<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Errors and Omissions Insurance - E & O  Insurance for Small Business | Hiscox USA</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<meta name="description" content="Hiscox tailors errors and omissions insurance to fit your specific field. Coverage from $22.50/month. Get a fast, online quote now." />
<meta name="keywords" content="errors and omissions insurance, errors and omissions insurance, errors and omissions coverage, professional liability insurance, professional errors and omissions, e & o insurance" />
		<link rel="canonical" href="http://www.hiscoxusa.com/small-business-insurance/errors-and-omissions-insurance/"/>	
 
	<link href="/resources/css/global-reset-sbl.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/ephox-dynamic-sbl.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/screenbuilder.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/rhythmyx-sbl.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/print-sbl.css" rel="stylesheet" type="text/css" media="print" />
 
	<!--[if lte IE 9]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
	<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
	<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
	<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
	<!--[if IE]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
	<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
 
	<script language="javascript" type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
	<script language="javascript" type="text/javascript" src="/resources/javascript/usacom_javascript.js"></script>
	
	<script src="/resources/javascript/jquery-1.7.1.min.js" type="text/javascript"></script>
	
	<script src="/resources/javascript/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
	
	<script src="/resources/javascript/jquery.selectbox-0.2.js" type="text/javascript"></script>
	<script src="/resources/javascript/jquery.bxSlider.min.js" type="text/javascript"></script>
	<script src="/resources/javascript/functions.js" type="text/javascript"></script>
	<script src="/resources/javascript/detectmobilebrowser.js" type="text/javascript"></script>
	
	<link rel="stylesheet" href="/resources/css/colorbox-sbl.css" />
	<script type="text/javascript" src="/resources/javascript/jquery.colorbox-min.js"></script>
	
		<script type="text/javascript">
		
		
		$(document).ready(function () {
		
		$('img').attr('draggable', false);
		
		$(".top-nav ul li").hover(  function () { 
            var li = $(this);
			var a = $(li).find("a");
			
			var left = $(a).position().left + ($(a).width() / 2);
 
            $(li).find('.panel-marker').css('margin-left', left);
			
          },function () {}
		);
		
		
		
      	$('#quote-and-buy-form-script').attr('style','display:inline;');
		
		$(".hl-col:first").addClass("first");
      	$(".hl-col:last").addClass("last");
		
			$('.sb-menu').hover(
				function(){
					$(this).toggleClass('sb-hover');
			});
			
			$('.sb-panel').mouseleave(function() { 
				var p = $(this).parent();
				if($(p).hasClass('sb-hover')) {
					$(this).attr('style','visibility:hidden;');
				}
			});
			
			$('.sb-menu').mouseenter(function() { 
				var p = $(this).find('.sb-panel');
				$(p).attr('style','visibility:visible;');
			});
			
			$('.search-text').focus(function() {           
			  if($(this).val() == 'Search') 
				$(this).val('');
			});
			$('.search-text').blur(function() {           
			  if($(this).val() == '') 
				$(this).val('Search');
			});
			
			$("a.youtube").colorbox({iframe:true, height:"300px", width:"400px", href: function(){
				return $(this).attr('href');
            }});
 
	
			$(function() {
				$( "ul.tooltips li a" ).tooltip({position: {my: "left-15 center", at: "right center", collision: "none"}});
			});
		});
      
		</script>
	<style>
.quote-box-content .selctbox-holder select{ border: 0;
    font-size: 1.2em;
    height:36px;
    *height:20px;
    width:273px;
    margin: 0 0 10px 0;
    *margin: 15px 0 10px 0;
    padding: 0;
    vertical-align: top;
    z-index:1;
    position:relative;
    opacity:0;
   -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
   *filter: alpha(opacity=0);
    cursor:pointer;
}
@media screen and (-webkit-min-device-pixel-ratio:0) 
{
  .quote-box-content .selctbox-holder select{height:22px;margin: 12px 0 10px 0;padding:3px 0;line-height:1.6em;}
}

.selctbox-holder{position:relative; margin-top:5px;}
.selctBox-bg{background:url("/resources/images/quote-select-landing-bg-sbl.png") no-repeat scroll left top transparent ;
height:40px; width:273px; position:absolute; top:-4px; left:0; 
}
.disaled-selct{
	opacity:0.6;
 -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=60)";
   *filter: alpha(opacity=60);
}
.enabled-selct{
	opacity:1;
 -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
   *filter: alpha(opacity=100);
}
.selc-text{color:#555; font-size:12px; font-weight:bold; 
padding:8px 0; display:block; padding:13px 8px; width:84%;}
.temp-quote-wrapper .temp-quote-inner .quote-box-content{height:196px;background:url("/resources/images/bg-quotebox-small-sbl-1.png") top left no-repeat;}
.selctBox-icon {
    background: url("/resources/images/quote-select-arrow-down-sbl.png") no-repeat scroll 0 0 transparent;
    height: 8px;
    position: absolute;
    right: 12px;
    top: 16px;
    width: 13px;
}
.selctBox-bg span.validation{color:red;display:inline;font-size:20px;line-height:5px;}
noscript .quote-box-content select{ font-size: 1.2em; margin: 0 0 10px 0;padding:1px 0}
</style>
<noscript><style>.hide{display:none;} 
.temp-quote-wrapper .temp-quote-inner .quote-box-content{height:150px;background:url("/resources/images/bg-quotebox-small-sbl.png") top left no-repeat;}
.quote-box-content select{ font-size: 1.2em; margin: 0 0 10px 0;padding:1px 0}
</style></noscript>	
	
	<!--BEGIN_GOMEZ--><script language="javascript">var gurl = document.location.href;var gomez={ gs: new Date().getTime(), acctId:'8ADB5A', pgId:gurl, grpId:'', wrate:'1', jbo:0, num:1 };</script><script language="javascript" type="text/javascript" src="/resources/javascript/minifirst.js"></script><!--END_GOMEZ-->
<!--BEGIN_GA-->
 <script type="text/javascript"> var _gaq = _gaq || [];  _gaq.push(['_setAccount', 'UA-21721659-10']); _gaq.push(['_setDomainName', '.nonprod.co.uk']); _gaq.push(['_trackPageview']); </script>
 <!--END_GA-->
<!--BEGIN_MAXYMISER-->
<script type="text/javascript" src="//service.maxymiser.net/cdn/hiscox/js/mmcore.js"> 
</script><!--END_MAXYMISER-->
</head>
	
	<body>
			
    		<div class="header">
    			<div class="header-inner">
    				<div class="logo-wrapper">
    					<div class="site-logo">
    						<a href="/" title="Hiscox USA"><img src="/resources/images/hiscox-logo-sbl.png" alt="Hiscox Logo" /></a>
    					</div>
    					<div class="sb-dropdown">
	<ul>
		<li class="sb-menu"><a href="/small-business-insurance/" title="Hiscox USA Small Business Insurance">Small Business</a>
			
	
    							
																<ul class="sb-panel"><li><a href="http://www.hiscoxusa.com" title="Hiscox">Homepage</a></li><li><a href="http://www.hiscoxbroker.co.uk/" title="Broker Center">Broker Center</a></li><li><a href="/small-business-insurance/why-choose-hiscox-insurance/">Hiscox Corporate</a></li></ul>
										
    	    		
		 				</li>
	</ul>
</div>    				</div>
    				
    				<div class="header-info">
							
	
    		<div id='site-search'>					
																
											<label for="gsc-i-id1">Search</label><div id="site-search-google">Loading</div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script> 
<script type="text/javascript"> 
	google.load('search', '1', {language : 'en'}); 
  	google.setOnLoadCallback(function() { 
  	var customSearchControl = new google.search.CustomSearchControl('004599377379842757095:ffvrv_nlocy'); 
  	customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
  	customSearchControl.setUserDefinedLabel('Search');
  	var options = new google.search.DrawOptions(); 
  	options.enableSearchboxOnly("http://systest.hiscoxusa.com/search/index.html"); 
  	customSearchControl.draw('site-search-google', options); }, true);
</script> 
 
<link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" /> 
<style type="text/css"> 
  input.gsc-input { border:none;width:150px;margin:-1px 0 0 10px;font-family:Arial, Helvetica, sans-serif; font-size:12px; background:none !important; height:19px; } 
  input.gsc-search-button { background:transparent url(/resources/images/icn-search-sbl.gif) no-repeat top right; margin-left: -225px; margin-top: -1px; width:15px; height:15px; border:0; font-size: 0px; color:transparent; *padding-left:76px !important; line-height:0px; cursor:pointer;margin-left:-241px\0;*position:relative; }
  :root input.gsc-search-button{margin-left:-225px \0/IE9} .gsc-control-searchbox-only div.gsc-clear-button { background:none; } 
</style>
															
    	    		</div>
		 							
						<div class='header-contact'>	
	
    							
																
											Call our licensed agents at <img src="/resources/images/icn-phone-sbl.gif" alt="" /> <span>1.800.456.1234</span>
															
    	    		
		 		</div>    				</div>
					
    				
						
		<div class="top-nav">
			<ul>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																											                                							    								<li>    									<a class="top-nav-link" href="#" title="Why Hiscox">Why Hiscox</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Coverage customized to the risks you face</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:210px;'>
					<p>See our approach</p><div>
<a href="http://youtube.com/embed/QR2fsK0bau8" class="youtube" shape="rect"><img id="Video Placeholder" src="/small-business-insurance/shared-images/sample-video.jpg" alt="Video Placeholder" title="Video Placeholder" border="0" height="99" width="165" /></a></div>
				</div>
            
												
				<div class='panel-col ' style='width:210px;'>
					<div class="panel-section"><p><strong>Lorem ipsum dolor sit</strong></p><p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p><p><a href="#" title="">See our awards and ratings</a></p><p><strong>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</strong></p><p><a href="#" title="">See our awards and ratings</a></p></div>
				</div>
            
												
				<div class='panel-col  last' style='width:210px;'>
					<div class="panel-section"><p><strong>Lorem ipsum dolor sit</strong></p><p>"Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet"</p><p><a href="#" title="">See what other customers say</a></p></div>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>
 
	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="#" title="Products">Products</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h1 class="red-call-to-action-large-h1">Insurance as unique as your small business</h1>							</div>
			
			
			
												
				<div class='panel-col ' style='width:210px;'>
					<h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Errors and omissions insurance</a></li><li><a href="#" title="">Errors and omissions insurance</a></li><li><a href="#" title="">Errors and omissions insurance</a></li><li><a href="#" title="">Errors and omissions insurance</a></li><li><a href="#" title="">Errors and omissions insurance</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:230px;'>
					<h3>Heading 3</h3><ul class="panel-icons"><li><img id="California" src="/small-business-insurance/shared-images/icn-california.gif" alt="California" title="California" border="0" height="25" width="25" /><a href="/small-business-insurance/california-business-insurance/">California</a></li><li><img id="Texas" src="/small-business-insurance/shared-images/icn-texas.gif" alt="Texas" title="Texas" border="0" height="25" width="25" /><a href="/small-business-insurance/texas-business-insurance/">Texas</a></li><li><img id="Florida" src="/small-business-insurance/shared-images/icn-florida.gif" alt="Florida" title="Florida" border="0" height="25" width="25" /><a href="/small-business-insurance/florida-business-insurance/">Florida</a></li><li><img id="Illinois" src="/small-business-insurance/shared-images/icn-illinois.gif" alt="Illinois" title="Illinois" border="0" height="25" width="25" /><a href="/small-business-insurance/illinois-business-insurance/">Illinois</a></li><li><img id="New York" src="/small-business-insurance/shared-images/icn-newyork.gif" alt="New York" title="New York" border="0" height="25" width="25" /><a href="/small-business-insurance/ny-business-insurance/">New York</a></li><li style=" list-style: none;"><a class="reg-link" href="/small-business-insurance/state/">View all states</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' style='width:210px;'>
					<h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Errors and omissions insurance</a></li><li><a href="#" title="">Errors and omissions insurance</a></li><li><a href="#" title="">Errors and omissions insurance</a></li><li><a href="#" title="">Errors and omissions insurance</a></li><li><a href="#" title="">Errors and omissions insurance</a></li></ul>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>
 
	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="#" title="Who we insure">Who we insure</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Coverage customized to the risks in your field</h2>				<p class="body-copy-grey-right-align">Don't see your profession? <a href="#">Click Here</a></p>			</div>
			
			
			
												
				<div class='panel-col ' style='width:170px;'>
					<h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a class="reg-link" href="#" title="">View all consulting</a></li></ul><br /><h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a class="reg-link" href="#" title="">View all consulting</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:170px;'>
					<h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a class="reg-link" href="#" title="">View all consulting</a></li></ul><br /><h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a class="reg-link" href="#" title="">View all consulting</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:170px;'>
					<h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a class="reg-link" href="#" title="">View all consulting</a></li></ul><br /><h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a class="reg-link" href="#" title="">View all consulting</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' style='width:170px;'>
					<h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a class="reg-link" href="#" title="">View all consulting</a></li></ul><br /><h3>Heading 3</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a href="#" title="">Business Consulting</a></li><li><a class="reg-link" href="#" title="">View all consulting</a></li></ul>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>
 
	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="#" title="Small business blog">Small business blog</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>We help make your business a huge success - not just with great insurance</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:340px;'>
					<h3>Recent blog posts</h3><ul class="mega-menu-red-arrow"><li><a href="http://www.hiscoxusa.com/small-business-insurance/blog/womens-history-month-is-a-great-time-to-recognize-women-business-owners/" title="">Women's History Month is a Great Time to...</a></li><li><a href="http://www.hiscoxusa.com/small-business-insurance/blog/tweet-to-win-and-leave-sxsw-with-an-ipad-mini-in-your-business-toolkit/" title="">Tweet to Win and Leave SXSW With an iPad Mini...</a></li><li><a href="http://www.hiscoxusa.com/small-business-insurance/blog/picture-this-why-photography-can-be-a-risky-business/" title="">Picture This: Why Photography Can Be a Risky Business</a></li><li><a href="http://www.hiscoxusa.com/small-business-insurance/blog/hiscox-receives-a-celent-model-insurer-award/" title="">Hiscox receives a Celent Model Insurer Award</a></li><li><a href="http://www.hiscoxusa.com/small-business-insurance/blog/show-your-local-community-you-care/" title="">Show Your Local Community You Care</a></li><li><a class="reg-link" href="http://www.hiscoxusa.com/small-business-insurance/blog/" title="">More posts</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' style='width:340px;'>
					<p>Our blog is designed to be a "go to" resource for your small business packed with information to make your business a success.</p><br /><h3>Join the conversation</h3><ul class="panel-icons-no-width"><li><a href="http://www.facebook.com/HiscoxSmallBiz" target="_blank" shape="rect"><img id="Meganav - Facebook" src="/small-business-insurance/shared-images/icn-facebook.gif" alt="Facebook" title="Facebook" border="0" height="29" width="29" /></a></li><li><a href="http://twitter.com/HiscoxSmallBiz" target="_blank" shape="rect"><img id="Meganav - Twitter" src="/small-business-insurance/shared-images/icn-twitter.gif" alt="Twitter" title="Twitter" border="0" height="29" width="29" /></a></li><li><a href="http://www.linkedin.com/company/hiscox-small-business-insurance" target="_blank" shape="rect"><img id="Meganav - Linked In" src="/small-business-insurance/shared-images/icn-linkedin.gif" alt="Linked In" title="Linked In" border="0" height="29" width="29" /></a></li><li><a href="http://www.youtube.com/user/hiscoxinsurance" target="_blank" shape="rect"><img id="Meganav - Youtube" src="/small-business-insurance/shared-images/icn-youtube.gif" alt="Youtube" title="Youtube" border="0" height="29" width="29" /></a></li><li><a href="http://www.hiscoxusa.com/small-business-insurance/blog/feed/" target="_blank" shape="rect"><img id="Meganav - RSS" src="/small-business-insurance/shared-images/icn-rss.gif" alt="RSS" title="RSS" border="0" height="29" width="29" /></a></li></ul>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>
 
	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="#" title="Customer support">Customer support</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Hiscox customer support - we're here to help</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:190px;'>
					<h3>Support services</h3><ul class="mega-menu-red-arrow"><li><a href="#" title="">Report a claim</a></li><li><a href="#" title="">Contact us</a></li><li><a href="#" title="">Customer reviews</a></li><li><a href="#" title="">Small business blog</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' >
					<div class="contact-details"><div>Call our licensed agents at</div><div class="tel-num">1.800.456.1234</div><div class="times">from 8am-10pm EST(Mon-Fri)</div></div>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>
 
	
      								</li>    																																									</ul>
		</div>
		
		
    
    			</div>
    		</div><!-- header -->
    		
			<div class="content-wrapper">
        		<div class="page-container">
        			
        				
	<div class="page-container">
		<!--Breadcrumb starts here-->
					<div class="breadcrumb">
				
	<div class="brdcrmb-sub">
			                    	                    					<div class="breadcrumb-link"><a href="/">Home page</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/Old">For small business owners</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-current"><span>Errors and Omissions Insurance</span></div>
			</div>
																						<div class="printbtnbrochureware-container">
    						<div class="submit-button grey-button print-icon">
    							<span class="submit-button-end">
    								<input type="button" value="Print" onClick="window.print()" class="submit-input">
        							<span class="button-icon"></span>
    							</span>
    						</div>
						</div>
													</div>
		<!--Breadcrumb ends here-->
		<!--Left nav starts here-->
		<div class="leftnav-gap clear-both">
						
							        	    		    									<div class="leftmenu-main">
																																																										<div class="leftmenu-mainitem">
					<div class="mainitem-topleft" ></div><div class="mainitem-topmiddle" ></div><div class="mainitem-topright"></div>
						<div class="mainitem-middle">
							<a href="/small-business-insurance/Old" target="_self" class="main-link">For small business owners</a>
						</div>
					<div class="mainitem-btmleft"></div><div class="mainitem-btmmiddle" ></div><div class="mainitem-btmright"></div>
				</div>
																	<div class="leftmenu">
					<div class="leftmenu-top"><div class="leftmenu-topleft"></div><div class="leftmenu-topmiddle"></div><div class="leftmenu-topright"></div></div>
						<div class="leftmenu-middle">
							<ul>
																	        																		        																		        																		            	    		    		    	    		    		    	    		    		    	    		    		    																																																												<li class="">
					<div class="leftmenu-listitem-sel"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/errors-and-omissions-insurance/" target="_self" class="leftmenulinks-sel">Errors and Omissions Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
																						  																                    																																																<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/errors-and-omissions-insurance/e-and-o-coverage/" target="_self" class="leftmenulinks-nopad">Coverage Details</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																																<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/errors-and-omissions-insurance/e-and-o-quotes/" target="_self" class="leftmenulinks-nopad">Example Quotes</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																																<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-liability-insurance/pl-faq/" target="_self" class="leftmenulinks-nopad">FAQs</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																																<li class="last-item">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/errors-and-omissions-insurance/e-and-o-state/" target="_self" class="leftmenulinks-nopad">Coverage by State</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    				   																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																</ul>
						</div>
					<div class="leftmenu-bottom"><div class="leftmenu-btmleft"></div><div class="leftmenu-btmmiddle"></div><div class="leftmenu-btmright"></div></div>
				</div>
			</div>
			    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    	    			
                                                                                                            
                                                                                                
            
            
                                    
            										        										        										        										    
            										    
            										    
            										        										        										        										        										        										        										    
                                                                                                    
            
            
                                    
            										        										        										        										    
            										    
            										    
            										        										        										        										        										        										        										    
    </div>
<div class="cnt-sctn no-bg">
                                                                                        		<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
        <h1 class="page-heading-h1">Errors and Omissions Insurance</h1>        </div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
        	
	
	
	<script type="text/javascript">
		$(document).ready(function () {
		
      		$('#quote-and-buy-form-script-26507').attr('style','display:inline;')
			
			if(!jQuery.browser.mobile) {
        		$('#state_dropdown_id-26507').selectmenu({ style: 'dropdown', menuWidth: ($(this).find('select.selectbox').width() + 20), transferClasses: true });
				$('#primarybusiness_dropdown_id-26507').selectmenu({ style: 'dropdown', menuWidth: ($(this).find('select.selectbox').width() + 20), transferClasses: true });
			} else {
        		$('#state_dropdown_id-26507').attr("style","-webkit-appearance:menulist;");
				$('#primarybusiness_dropdown_id-26507').attr("style","-webkit-appearance:menulist;");
			}
 
		});
		function passValues(ID,selct)
		{
		  var selctedText = $("#"+ID).children("option:selected").text();
		selctedText=selctedText.toUpperCase();
		  $("#"+selct+" .selc-text").html(selctedText);

		}
		function checkValues26507(submitbuttonid) {
            var ysValue = $("#state_dropdown_id").val(); 
            var bcValue = $("#primarybusiness_dropdown_id").val();
            var cValue=$("#category").val();
            
            if(!ysValue || !bcValue) {
              
              if(!jQuery.browser.mobile) {
              
            	  var ysText = $("#drop1 .selc-text").text();
                  var bsText = $("#drop3 .selc-text").text();
      
                  if(!ysValue && ysText.indexOf("*") == -1) {
                    $("#drop1 .selc-text").prepend("<span class='validation'>*</span> ");
                  }
                  
                  if(cValue!="NONE OF THE ABOVE")
  				{
  					
  				 if(!bcValue && bsText.indexOf("*") == -1) {
                    $("#drop3 .selc-text").prepend("<span class='validation'>*</span> ");
                  }
                 }
                
                $(".btn-getquote a#inline-content-button-26507").colorbox({speed:150, inline:true, title:true, href:"#inline-content-26507", innerWidth:420, opacity:0.5});
				$("#cboxTitle").removeAttr("style");
              } else {
              
                var ysText = $("#state_dropdown_id option").eq(0).text();
                var bsText = $("#primarybusiness_dropdown_id option").eq(0).text();
    
                if(!ysValue && ysText.indexOf("*") == -1) {
                  $("#state_dropdown_id option").eq(0).text("* " + ysText);
                }
                
                if(!bcValue && bsText.indexOf("*") == -1) {
                  $("#primarybusiness_dropdown_id option").eq(0).text("* " + bsText);
                }
                
                alert("Please fill out all the required fields");
              
              }
              
            } else {
              $(".btn-getquote a#inline-content-button-26507").colorbox.remove();
              document.getElementById(submitbuttonid).click();
            }
          }
      
		</script>
	
	
	<div>
			<form action="/small-business-insurance/quote-and-buy/brochureware/" method="post" name="ScreenBuilderForm" id="ScreenBuilderForm" class="hide">
				
				<div class="temp-quote-wrapper" style="float:none;">
					<div class="temp-quote-inner">
						<div class="quote-box-content">
						
							<div class="quote-col-one">
								<div class="quote-caption">
									<div class="quote-hr"></div>
									<div class="quote-hr-right"></div>
									<h2><span>It's quick and easy. Let's get started.</span></h2>								</div>
							</div>
							<div class="quote-col-two">
								<div class="selctbox-holder"><select  name="state" id="state_dropdown_id" onchange="passValues(this.id,'drop1');"> 
									<option value="" selected="selected">YOUR STATE</option>
																			<option value="AL">Alabama</option>
																			<option value="AK">Alaska</option>
																			<option value="AZ">Arizona</option>
																			<option value="AR">Arkansas</option>
																			<option value="CA">California</option>
																			<option value="CO">Colorado</option>
																			<option value="CT">Connecticut</option>
																			<option value="DE">Delaware</option>
																			<option value="DC">District Of Columbia</option>
																			<option value="FL">Florida</option>
																			<option value="GA">Georgia</option>
																			<option value="HI">Hawaii</option>
																			<option value="ID">Idaho</option>
																			<option value="IL">Illinois</option>
																			<option value="IN">Indiana</option>
																			<option value="IA">Iowa</option>
																			<option value="KS">Kansas</option>
																			<option value="KY">Kentucky</option>
																			<option value="LA">Louisiana</option>
																			<option value="ME">Maine</option>
																			<option value="MD">Maryland</option>
																			<option value="MA">Massachusetts</option>
																			<option value="MI">Michigan</option>
																			<option value="MN">Minnesota</option>
																			<option value="MS">Mississippi</option>
																			<option value="MO">Missouri</option>
																			<option value="MT">Montana</option>
																			<option value="NE">Nebraska</option>
																			<option value="NV">Nevada</option>
																			<option value="NH">New Hampshire</option>
																			<option value="NJ">New Jersey</option>
																			<option value="NM">New Mexico</option>
																			<option value="NY">New York</option>
																			<option value="NC">North Carolina</option>
																			<option value="ND">North Dakota</option>
																			<option value="OH">Ohio</option>
																			<option value="OK">Oklahoma</option>
																			<option value="OR">Oregon</option>
																			<option value="PA">Pennsylvania</option>
																			<option value="RI">Rhode Island</option>
																			<option value="SC">South Carolina</option>
																			<option value="SD">South Dakota</option>
																			<option value="TN">Tennessee</option>
																			<option value="TX">Texas</option>
																			<option value="UT">Utah</option>
																			<option value="VT">Vermont</option>
																			<option value="VA">Virginia</option>
																			<option value="WA">Washington</option>
																			<option value="WV">West Virginia</option>
																			<option value="WI">Wisconsin</option>
																			<option value="WY">Wyoming</option>
																	</select>
																	<div class="selctBox-bg" id="drop1"><span class="selc-text">YOUR STATE</span><span class="selctBox-icon"></span></div>	</div>
																	<div class="selctbox-holder"><select name="businesscategory" id="category" onchange="passValues(this.id,'drop2');updateprimarybusiness(this.selectedIndex);" >
									<option value="Not Selected" selected="selected">BUSINESS CATEGORY</option>
														<option value="Architecture, Engineering & Design">Architecture, Engineering & Design</option>
														<option value="Consulting">Consulting</option>
														<option value="Creative">Creative</option>
														<option value="Financial Services">Financial Services</option>
														<option value="Health, Beauty & Wellness">Health, Beauty & Wellness</option>
														<option value="Legal Services">Legal Services</option>
														<option value="Marketing/PR">Marketing/PR</option>
														<option value="Other Professional Services">Other Professional Services</option>
														<option value="Real Estate">Real Estate</option>
														<option value="Technology">Technology</option>
														<option value="NONE OF THE ABOVE">NONE OF THE ABOVE</option>
																		</select>
																	<div class="selctBox-bg" id="drop2"><span class="selc-text">BUSINESS CATEGORY</span><span class="selctBox-icon"></span></div>	</div>
								<div class="selctbox-holder"><select name="primarybusiness" id="primarybusiness_dropdown_id" onchange="passValues(this.id,'drop3');">
									<option selected="selected" value="">BUSINESS DESCRIPTION</option>
 <option value="Accounting">Accounting</option>
��������������� <option value="Actuarial services">Actuarial services</option>
��������������� <option value="Acupressure services">Acupressure services</option>
��������������� <option value="Acupuncture services">Acupuncture services</option>
��������������� <option value="Advertising">Advertising</option>
��������������� <option value="Answering/paging services">Answering/paging services</option>
��������������� <option value="Application development">Application development</option>
��������������� <option value="Application service provider">Application service provider</option>
��������������� <option value="Architecture">Architecture</option>
��������������� <option value="Art therapy">Art therapy</option>
��������������� <option value="Auctioneering">Auctioneering</option>
��������������� <option value="Audiology">Audiology</option>
��������������� <option value="Beautician/cosmetology services">Beautician/cosmetology services</option>
��������������� <option value="Bookkeeping">Bookkeeping</option>
��������������� <option value="Brand consultant">Brand consultant</option>
��������������� <option value="Building/construction inspection">Building inspection</option>
��������������� <option value="Business consulting">Business consulting</option>
��������������� <option value="Business manager services">Business manager services</option>
��������������� <option value="Civil engineering">Civil engineering</option>
��������������� <option value="Claims adjusting">Claims adjusting</option>
��������������� <option value="Computer consulting">Computer consulting</option>
��������������� <option value="Computer programming services">Computer programming services</option>
��������������� <option value="Computer system/network developer">Computer system/network developer</option>
��������������� <option value="Construction/project management">Project manager (architecture or engineering)</option>
��������������� <option value="Control systems integration/automation">Control systems integration/automation</option>
��������������� <option value="Court reporting">Court reporting</option>
��������������� <option value="Credit counseling">Credit counseling</option>
��������������� <option value="Dance therapy">Dance therapy</option>
��������������� <option value="Data processing">Data processing</option>
��������������� <option value="Database designer">Database designer</option>
��������������� <option value="Dietician/nutrition">Dietician/nutrition</option>
��������������� <option value="Digital marketing">Digital marketing</option>
��������������� <option value="Direct marketing">Direct marketing</option>
��������������� <option value="Document preparation">Document preparation</option>
��������������� <option value="Draftsman (including CAD/CAM)">Draftsman (including CAD/CAM)</option>
��������������� <option value="Drama therapy">Drama therapy</option>
��������������� <option value="Education consulting">Education consulting</option>
��������������� <option value="Electrical engineering">Electrical engineering</option>
��������������� <option value="Engineering">Engineering</option>
��������������� <option value="Environmental engineering">Environmental engineering</option>
��������������� <option value="Esthetician services">Esthetician services</option>
��������������� <option value="Event planning/promotion">Event planning/promotion</option>
��������������� <option value="Executive placement">Executive placement</option>
��������������� <option value="Expert witness services">Expert witness services</option>
��������������� <option value="Financial auditing or consulting">Financial auditing or consulting</option>
��������������� <option value="First aid and CPR training">First aid and CPR training</option>
��������������� <option value="Graphic design">Graphic design</option>
��������������� <option value="Barber/hair stylist services">Hair stylist/barber services</option>
��������������� <option value="Human Resources (HR) consulting">Human Resources (HR) consulting</option>
��������������� <option value="Hypnosis">Hypnosis</option>
��������������� <option value="Industrial engineering">Industrial engineering</option>
��������������� <option value="Interior design">Interior design</option>
��������������� <option value="Investment advice">Investment advice</option>
��������������� <option value="IT consulting">IT consulting</option>
��������������� <option value="IT project management">IT project management</option>
��������������� <option value="IT software/hardware training services">IT software/hardware training services</option>
��������������� <option value="Landscape architect">Landscape architect</option>
��������������� <option value="Legal services">Legal services</option>
��������������� <option value="Life/career/executive coaching">Life/career/executive coaching</option>
��������������� <option value="Management consulting">Management consulting</option>
��������������� <option value="Marketing/media consulting">Marketing/media consulting</option>
��������������� <option value="Market research">Market research</option>
��������������� <option value="Marriage and family therapy">Marriage and family therapy</option>
��������������� <option value="Massage therapy">Massage therapy</option>
��������������� <option value="Medical billing">Medical billing</option>
��������������� <option value="Mental health counseling">Mental health counseling</option>
��������������� <option value="Mortgage brokering/banking">Mortgage brokering/banking</option>
��������������� <option value="Music therapy">Music therapy</option>
��������������� <option value="Nail technician services">Nail technician services</option>
��������������� <option value="Notary services">Notary services</option>
��������������� <option value="Occupational therapy">Occupational therapy</option>
��������������� <option value="Personal concierge/assistant">Personal concierge/assistant</option>
��������������� <option value="Personal training (health and fitness)">Personal training (health and fitness)</option>
��������������� <option value="Photography">Photography</option>
��������������� <option value="Process engineering">Process engineering</option>
��������������� <option value="Process server">Process server</option>
��������������� <option value="Property management">Property management</option>
��������������� <option value="Project management">Project management</option>
��������������� <option value="Psychology">Psychology</option>
��������������� <option value="Public relations">Public relations</option>
��������������� <option value="Real estate agent/broker">Real estate agent/broker</option>
��������������� <option value="Recruiting (employment placements)">Recruiting (employment placements)</option>
��������������� <option value="Research consulting">Research consulting</option>
��������������� <option value="Resume consulting">Resume consulting</option>
��������������� <option value="Search engine services (SEO/SEM)">Search engine services (SEO/SEM)</option>
��������������� <option value="Social media consultant">Social media consultant</option>
��������������� <option value="Social work services">Social work services</option>
��������������� <option value="Software development">Software development</option>
��������������� <option value="Speech therapy">Speech therapy</option>
��������������� <option value="Stock brokering">Stock brokering</option>
��������������� <option value="Strategy consultant">Strategy consultant</option>
��������������� <option value="Substance abuse counseling">Substance abuse counseling</option>
��������������� <option value="Talent agency">Talent agency</option>
��������������� <option value="Tax preparation">Tax preparation</option>
��������������� <option value="Technology services">Technology services</option>
��������������� <option value="Training (business, vocational or life skills)">Training (business, vocational or life skills)</option>
��������������� <option value="Translating/interpreting">Translating/interpreting</option>
��������������� <option value="Transportation engineering">Transportation engineering</option>
��������������� <option value="Travel agency">Travel agency</option>
��������������� <option value="Trustee">Trustee</option>
��������������� <option value="Tutoring">Tutoring</option>
��������������� <option value="Value added reseller of computer hardware">Value added reseller of computer hardware</option>
��������������� <option value="Website design">Website design</option>
��������������� <option value="Yoga/pilates instruction">Yoga/pilates instruction</option>
��������������� <option value="Other architecture, engineering & design services">Other architecture, engineering & design services</option>
��������������� <option value="Other consulting services">Other consulting services</option>
��������������� <option value="Other creative services">Other creative services</option>
��������������� <option value="Other financial services">Other financial services</option>
��������������� <option value="Other health, beauty & wellness services">Other health, beauty & wellness services</option>
��������������� <option value="Other legal services">Other legal services</option>
��������������� <option value="Other marketing/PR services">Other marketing/PR services</option>
��������������� <option value="Other professional services">Other professional services</option>
��������������� <option value="Other real estate services">Other real estate services</option>
��������������� <option value="Other technology services">Other technology services</option>
																	</select><div class="selctBox-bg" id="drop3"><span class="selc-text" id="selct-text">BUSINESS DESCRIPTION</span><span class="selctBox-icon"></span></div>	</div>
							</div>
							
							<div class="quote-col-three">
							  <div class="slide-button btn-getquote"><a id="inline-content-button-26507" href="#" onclick="javascript:checkValues26507('startquote_button_id-26507');" title="GET A QUOTE"><span>GET A QUOTE</span></a></div>
							  <div class="slide-button btn-retquote"><a href="/small-business-insurance/quote-and-buy/retrieve-a-quote/" title="RETRIEVE YOUR SAVED QUOTE"><span>RETRIEVE YOUR SAVED QUOTE</span></a></div>
							</div>
							
							
						</div>
					</div>
				</div>
					<input type="submit" title="Start Quote" value="Get a quote" style="left:-9999px;" name="action_StartQuote_button" id="startquote_button_id-26507" class="submit-input" />
				</form>
				
				<noscript>
				
				<form action="/small-business-insurance/quote-and-buy/brochureware/" id="quote-and-buy-form-noscript-26507" method="post">
				
				<div class="temp-quote-wrapper" style="float:none;">
					<div class="temp-quote-inner">
						<div class="quote-box-content">
							
							<div class="quote-col-one">
								<div class="quote-caption">
									<div class="quote-hr"></div>
									<div class="quote-hr-right"></div>
									<h2>It's quick and easy. Let's get started.</h2>								</div>
							</div>
							<div class="quote-col-two">
								<select name="state" id="state_dropdown_id-26507">
									<option value="" selected="selected">YOUR STATE</option>
																			<option value="AL">Alabama</option>
																			<option value="AK">Alaska</option>
																			<option value="AZ">Arizona</option>
																			<option value="AR">Arkansas</option>
																			<option value="CA">California</option>
																			<option value="CO">Colorado</option>
																			<option value="CT">Connecticut</option>
																			<option value="DE">Delaware</option>
																			<option value="DC">District Of Columbia</option>
																			<option value="FL">Florida</option>
																			<option value="GA">Georgia</option>
																			<option value="HI">Hawaii</option>
																			<option value="ID">Idaho</option>
																			<option value="IL">Illinois</option>
																			<option value="IN">Indiana</option>
																			<option value="IA">Iowa</option>
																			<option value="KS">Kansas</option>
																			<option value="KY">Kentucky</option>
																			<option value="LA">Louisiana</option>
																			<option value="ME">Maine</option>
																			<option value="MD">Maryland</option>
																			<option value="MA">Massachusetts</option>
																			<option value="MI">Michigan</option>
																			<option value="MN">Minnesota</option>
																			<option value="MS">Mississippi</option>
																			<option value="MO">Missouri</option>
																			<option value="MT">Montana</option>
																			<option value="NE">Nebraska</option>
																			<option value="NV">Nevada</option>
																			<option value="NH">New Hampshire</option>
																			<option value="NJ">New Jersey</option>
																			<option value="NM">New Mexico</option>
																			<option value="NY">New York</option>
																			<option value="NC">North Carolina</option>
																			<option value="ND">North Dakota</option>
																			<option value="OH">Ohio</option>
																			<option value="OK">Oklahoma</option>
																			<option value="OR">Oregon</option>
																			<option value="PA">Pennsylvania</option>
																			<option value="RI">Rhode Island</option>
																			<option value="SC">South Carolina</option>
																			<option value="SD">South Dakota</option>
																			<option value="TN">Tennessee</option>
																			<option value="TX">Texas</option>
																			<option value="UT">Utah</option>
																			<option value="VT">Vermont</option>
																			<option value="VA">Virginia</option>
																			<option value="WA">Washington</option>
																			<option value="WV">West Virginia</option>
																			<option value="WI">Wisconsin</option>
																			<option value="WY">Wyoming</option>
																	</select>
								<select name="primarybusiness" id="primarybusiness_dropdown_id-26507">
									<option selected="selected" value="">BUSINESS DESCRIPTION</option>
 <option value="Accounting">Accounting</option>
��������������� <option value="Actuarial services">Actuarial services</option>
��������������� <option value="Acupressure services">Acupressure services</option>
��������������� <option value="Acupuncture services">Acupuncture services</option>
��������������� <option value="Advertising">Advertising</option>
��������������� <option value="Answering/paging services">Answering/paging services</option>
��������������� <option value="Application development">Application development</option>
��������������� <option value="Application service provider">Application service provider</option>
��������������� <option value="Architecture">Architecture</option>
��������������� <option value="Art therapy">Art therapy</option>
��������������� <option value="Auctioneering">Auctioneering</option>
��������������� <option value="Audiology">Audiology</option>
��������������� <option value="Beautician/cosmetology services">Beautician/cosmetology services</option>
��������������� <option value="Bookkeeping">Bookkeeping</option>
��������������� <option value="Brand consultant">Brand consultant</option>
��������������� <option value="Building/construction inspection">Building inspection</option>
��������������� <option value="Business consulting">Business consulting</option>
��������������� <option value="Business manager services">Business manager services</option>
��������������� <option value="Civil engineering">Civil engineering</option>
��������������� <option value="Claims adjusting">Claims adjusting</option>
��������������� <option value="Computer consulting">Computer consulting</option>
��������������� <option value="Computer programming services">Computer programming services</option>
��������������� <option value="Computer system/network developer">Computer system/network developer</option>
��������������� <option value="Construction/project management">Project manager (architecture or engineering)</option>
��������������� <option value="Control systems integration/automation">Control systems integration/automation</option>
��������������� <option value="Court reporting">Court reporting</option>
��������������� <option value="Credit counseling">Credit counseling</option>
��������������� <option value="Dance therapy">Dance therapy</option>
��������������� <option value="Data processing">Data processing</option>
��������������� <option value="Database designer">Database designer</option>
��������������� <option value="Dietician/nutrition">Dietician/nutrition</option>
��������������� <option value="Digital marketing">Digital marketing</option>
��������������� <option value="Direct marketing">Direct marketing</option>
��������������� <option value="Document preparation">Document preparation</option>
��������������� <option value="Draftsman (including CAD/CAM)">Draftsman (including CAD/CAM)</option>
��������������� <option value="Drama therapy">Drama therapy</option>
��������������� <option value="Education consulting">Education consulting</option>
��������������� <option value="Electrical engineering">Electrical engineering</option>
��������������� <option value="Engineering">Engineering</option>
��������������� <option value="Environmental engineering">Environmental engineering</option>
��������������� <option value="Esthetician services">Esthetician services</option>
��������������� <option value="Event planning/promotion">Event planning/promotion</option>
��������������� <option value="Executive placement">Executive placement</option>
��������������� <option value="Expert witness services">Expert witness services</option>
��������������� <option value="Financial auditing or consulting">Financial auditing or consulting</option>
��������������� <option value="First aid and CPR training">First aid and CPR training</option>
��������������� <option value="Graphic design">Graphic design</option>
��������������� <option value="Barber/hair stylist services">Hair stylist/barber services</option>
��������������� <option value="Human Resources (HR) consulting">Human Resources (HR) consulting</option>
��������������� <option value="Hypnosis">Hypnosis</option>
��������������� <option value="Industrial engineering">Industrial engineering</option>
��������������� <option value="Interior design">Interior design</option>
��������������� <option value="Investment advice">Investment advice</option>
��������������� <option value="IT consulting">IT consulting</option>
��������������� <option value="IT project management">IT project management</option>
��������������� <option value="IT software/hardware training services">IT software/hardware training services</option>
��������������� <option value="Landscape architect">Landscape architect</option>
��������������� <option value="Legal services">Legal services</option>
��������������� <option value="Life/career/executive coaching">Life/career/executive coaching</option>
��������������� <option value="Management consulting">Management consulting</option>
��������������� <option value="Marketing/media consulting">Marketing/media consulting</option>
��������������� <option value="Market research">Market research</option>
��������������� <option value="Marriage and family therapy">Marriage and family therapy</option>
��������������� <option value="Massage therapy">Massage therapy</option>
��������������� <option value="Medical billing">Medical billing</option>
��������������� <option value="Mental health counseling">Mental health counseling</option>
��������������� <option value="Mortgage brokering/banking">Mortgage brokering/banking</option>
��������������� <option value="Music therapy">Music therapy</option>
��������������� <option value="Nail technician services">Nail technician services</option>
��������������� <option value="Notary services">Notary services</option>
��������������� <option value="Occupational therapy">Occupational therapy</option>
��������������� <option value="Personal concierge/assistant">Personal concierge/assistant</option>
��������������� <option value="Personal training (health and fitness)">Personal training (health and fitness)</option>
��������������� <option value="Photography">Photography</option>
��������������� <option value="Process engineering">Process engineering</option>
��������������� <option value="Process server">Process server</option>
��������������� <option value="Property management">Property management</option>
��������������� <option value="Project management">Project management</option>
��������������� <option value="Psychology">Psychology</option>
��������������� <option value="Public relations">Public relations</option>
��������������� <option value="Real estate agent/broker">Real estate agent/broker</option>
��������������� <option value="Recruiting (employment placements)">Recruiting (employment placements)</option>
��������������� <option value="Research consulting">Research consulting</option>
��������������� <option value="Resume consulting">Resume consulting</option>
��������������� <option value="Search engine services (SEO/SEM)">Search engine services (SEO/SEM)</option>
��������������� <option value="Social media consultant">Social media consultant</option>
��������������� <option value="Social work services">Social work services</option>
��������������� <option value="Software development">Software development</option>
��������������� <option value="Speech therapy">Speech therapy</option>
��������������� <option value="Stock brokering">Stock brokering</option>
��������������� <option value="Strategy consultant">Strategy consultant</option>
��������������� <option value="Substance abuse counseling">Substance abuse counseling</option>
��������������� <option value="Talent agency">Talent agency</option>
��������������� <option value="Tax preparation">Tax preparation</option>
��������������� <option value="Technology services">Technology services</option>
��������������� <option value="Training (business, vocational or life skills)">Training (business, vocational or life skills)</option>
��������������� <option value="Translating/interpreting">Translating/interpreting</option>
��������������� <option value="Transportation engineering">Transportation engineering</option>
��������������� <option value="Travel agency">Travel agency</option>
��������������� <option value="Trustee">Trustee</option>
��������������� <option value="Tutoring">Tutoring</option>
��������������� <option value="Value added reseller of computer hardware">Value added reseller of computer hardware</option>
��������������� <option value="Website design">Website design</option>
��������������� <option value="Yoga/pilates instruction">Yoga/pilates instruction</option>
��������������� <option value="Other architecture, engineering & design services">Other architecture, engineering & design services</option>
��������������� <option value="Other consulting services">Other consulting services</option>
��������������� <option value="Other creative services">Other creative services</option>
��������������� <option value="Other financial services">Other financial services</option>
��������������� <option value="Other health, beauty & wellness services">Other health, beauty & wellness services</option>
��������������� <option value="Other legal services">Other legal services</option>
��������������� <option value="Other marketing/PR services">Other marketing/PR services</option>
��������������� <option value="Other professional services">Other professional services</option>
��������������� <option value="Other real estate services">Other real estate services</option>
��������������� <option value="Other technology services">Other technology services</option>
																	</select>
							</div>
							<div class="quote-col-three">
								<div class='slide-button'>
 
	
	<input type="image" src="/small-business-insurance/shared-buttons/btn-get-quote-large.jpg" name="action_StartQuote_button" value="Get a Quote NOSCRIPT button" name="Get a Quote NOSCRIPT button" />
 
  </div>								<div class="slide-button btn-retquote"><a href="/small-business-insurance/quote-and-buy/retrieve-a-quote/" title="RETRIEVE YOUR SAVED QUOTE"><span>RETRIEVE YOUR SAVED QUOTE</span></a></div>
							</div>
							
							<div class="quote-hr"></div>
							
						</div>
					</div>
				</div>
				
				</form>
				
				</noscript>
 
				<!-- This contains the hidden content for inline calls -->
				<div style='display:none'><div id='inline-content-26507' style='padding:10px; background:#fff; font-size:2em;'><p><span style=" font-size: 30px; color: red;"><strong>*</strong></span> <strong>Please fill out all the required fields</strong></p></div></div>
				</div>
				
				<script type="text/javascript">	
    				$(document).ready(function () {
						var h = $("#quote-and-buy-form-script-26507 .quote-caption h2").width();
						var s = $("#quote-and-buy-form-script-26507 .quote-caption h2 span").width();	
                		$("#quote-and-buy-form-script-26507 .quote-caption .quote-hr").css("width", function () { 
								return (((h - s) / 2) - 20);
                		});
						
						$("#quote-and-buy-form-script-26507 .quote-caption .quote-hr-right").css("width", function () {            				
								return (((h - s) / 2) - 20);
                		});
    				});
				</script>
 
	
  	
                                                                                        		<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
        <h2 class="red-call-to-action-medium-h2"><b>Why choose Hiscox?</b></h2><p> </p><p class="body-copy-grey">We specialize in errors and omissions insurance (E &amp; O insurance), also known as <a href="/small-business-insurance/professional-liability-insurance/">professional liability insurance</a> for professional services businesses. That's why we have made it easier for small business owners to buy E &amp; O coverage direct and online.</p><p> </p><ul class="list-red-tick-body-copy-grey"><li><b>Tailored coverage</b>: We specialize in businesses like yours and tailor coverage to the risks in your field.</li><li><b>Passion for service</b>: Knowledgeable agents provide exceptional service &ndash; 96% of people surveyed recommend us.</li><li><b>Coverage for contracts</b>: Our errors &amp; omissions coverage satisfies most <a href="/small-business-insurance/contract-insurance-requirements/">standard contract insurance requirements</a>.</li><li><b>Fast and simple</b>: Online quotes or speak to a licensed agent &ndash; immediate coverage.</li><li><b>Confidence</b>: Hiscox Insurance Company Inc. is &lsquo;A&rsquo; rated (Excellent) by A.M. Best.</li></ul><p> </p><p class="body-copy-grey">Learn more about the benefits of choosing <a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Hiscox insurance</a>.<br /><br /></p><h2 class="grey-heading-h2"><b>Why do you need errors and omissions insurance?</b></h2><p class="body-copy-grey">If you have a professional services business, having errors and omissions insurance coverage can be an integral part of protecting your business. Accusations of negligence or the failure to perform your professional services are things that any professional services business can be sued for, even if it hasn't made a mistake.<br /><br /></p><p class="body-copy-grey">You should seriously consider this coverage if your business:</p><ul class="list-bulleted-body-copy-grey"><li>provides a professional service.</li><li>regularly gives advice.</li><li>is required by its clients to have E &amp; O insurance.<br /><br /></li></ul><h2 class="red-call-to-action-medium-h2"><b>Additional coverage benefits:</b><br /><br /></h2><ul class="list-red-tick-body-copy-grey"><li><b>Monthly payment options</b>: Spread out your payments (with no fees) to help manage your cash flow.</li><li><b>Work completed by temporary staff is covered</b>: Hiscox professional liability coverage work done by full-time employees and temporary staff, as well as subcontractors.</li><li><b>Past work covered</b>: Unlike some of our competitors policies, our errors and omissions insurance covers unknown claims arising from work completed before you were even insured with us, back to an agreed date.</li><li><b>Worldwide coverage</b>: North, south, east or west, Hiscox will protect you as long as the claim is filed in the U.S., one of its territories or Canada.</li><li><b>Claims responsiveness</b>: In the world of professional businesses time is of the essence, especially when you need to make an insurance claim. If a covered claim is filed, Hiscox will immediately defend you even if the claim has no basis and, if necessary, appoint an attorney.<br /><br /></li></ul><div style="width:100%;align:center">
			<div class="addthis_toolbox addthis_default_style">
<a class="addthis_button_tweet"></a>
<a class="addthis_button_facebook_like"></a>
<a class="addthis_button_google_plusone"></a>
<a class="addthis_button_linkedin"></a>
<a class="addthis_button_delicious"></a>
<a class="addthis_button_digg"></a>
<a class="addthis_button_reddit"></a>
<a class="addthis_button_stumbleupon"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=ra-4d9dc1fc5e33b995">;</script>		</div>        </div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
    		
		
    <!--Secondary content section starts here-->
            <!--Secondary content section ends here-->
 
    <!--Centre bottom section starts here-->
                                                                                                
            
            
                                    
            										        										        										        										    
            										    
            										    
            										        										        										        										        										        										        										    
        <!--Centre bottom section ends here-->
    <!--Tertiary content section starts here-->
	
		
                                                                                            		<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
        <p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply that any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>        </div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
            <!--Tertiary content section ends here-->
	
	</div>
<!--Content section ends here-->
<!-- Right nav starts here-->
<div class="rightnav-gap showcntct-num">
    <!--Checklist section starts here-->                                                                                                    
            
            
                                    
            										        										        										        										    
            										    
            																                        <div class="section-container topright-container-small print" >
        <div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
                <div class="middle-content vertical-container">
                
																																				<div class="contact-child-item print">
						<div>
							<img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="140" width="167" />						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p><span class="grey-call-to-action-large"><b>866-283-7545</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p>						</div>
					</div>
										                </div>
                <div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
    </div>
    			 			    
            										        										        										        										        										        										        										    
                                                                                                                                                                                            
            
            
                                    
            								                                        <div class="section-container smallcontainer " >
        <div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
                <div class="middle-content vertical-container">
                
		<div itemscope="" itemtype="http://schema.org/InsuranceAgency"><h3 class="black-heading-h3"><span itemprop="name"><b>Customer Reviews</b></span></h3><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><img id="4 and a half star rating - small with padding" src="/shared-images/small-gold-stars-four-and-a-half.gif" alt="Hiscox Customer ratings" title="Hiscox Customer ratings" border="0" height="23" width="152" /><div itemprop="AggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating"><p class="body-copy-black"><b>Rated <span itemprop="ratingValue">4.9</span>/5 for overall</b><br /><b>customer service.</b></p><p class="body-copy-grey"><a href="http://www.hiscoxusa.com/small-business-insurance/business-insurance-reviews/"><span style=" text-decoration: underline; font-weight: bold;"><span itemprop="reviewCount">6,671</span> reviews</span><br /></a><br /></p></div></div>	                </div>
                <div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
    </div>
    			        										        										        										    
            										    
            										    
            										        										        										        										        										        										        										    
        <div class="verLogo-holder">
        <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications.">
            <tr>
                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hiscoxusa.com&size=L&use_flash=YES&use_transparent=YES&lang=en"></script><br />
                    <a href="http://www.verisign.com/ssl-certificate/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>
            </tr>
        </table>
    </div>
    <!--Checklist section ends here-->
</div>
<!-- Right nav ends here-->
</div>
 
        				
        				<div class="sb-footer">
        					<div class="sb-footer-inner">
        						
 
 
	
    						
			
									
											<div class='sb-footer-col'>
										
					<h3 class="box-heading-h3">Insurance by industry</h3><ul><li><a href="/small-business-insurance/why-choose-hiscox-insurance/">Marketing / Creative/ Design</a></li><li><a href="/small-business-insurance/why-choose-hiscox-insurance/">Lorem ipsum dolor sit</a></li><li><a href="/small-business-insurance/why-choose-hiscox-insurance/">Lorem ipsum dolor sit</a></li><li><a href="/small-business-insurance/why-choose-hiscox-insurance/">Lorem ipsum dolor sit</a></li><li><a href="/small-business-insurance/why-choose-hiscox-insurance/">Lorem ipsum dolor sit</a></li><li><a href="/small-business-insurance/why-choose-hiscox-insurance/">Lorem ipsum dolor sit</a></li></ul>
					</div>
							
    			
			
									
											<div class='sb-footer-col'>
										
					<h3>Small Business Insurance</h3><ul><li><a href="" title="">Marketing / Creative / Design</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li></ul>
					</div>
							
    			
			
									
											<div class='sb-footer-col'>
										
					<h3>Insurance by State</h3><ul><li><a href="" title="">Marketing / Creative / Design</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li><li><a href="" title="">Lorem ipsum dolor sit</a></li></ul>
					</div>
							
    			
			
									
											<div class='sb-footer-col' style='width:270px; margin-right:0;'>
										
					<h3>Helpful Links</h3><div class="help-left"><ul><li><a href="/small-business-insurance/why-choose-hiscox-insurance/">Report a claim</a></li><li><a href="#">Customer reviews</a></li><li><a href="#">Small business blog</a></li><li><a href="#">Report a claim</a></li><li><a href="#">Report a claim</a></li></ul></div><div class="help-right"><ul><li><table border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications." width="135"><tr><td align="center" valign="top" width="135"><script src="https://seal.verisign.com/getseal?host_name=www.hiscoxusa.com&amp;size=S&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en" type="text/javascript">/**/
/**/
/**/
/**/
/**/
/**/
 
/**/
/**/
/**/
/**/
/**/
/**/</script><br /><a href="http://www.symantec.com/ssl-certificates" style=" text-align: center; margin: 0px; color: #000000; padding: 0px; letter-spacing: .5px; font: bold 7px verdana,sans-serif; text-decoration: none;" target="_blank">ABOUT SSL CERTIFICATES</a></td></tr></table></li></ul></div>
					</div>
							
    	    	
	 		
<div class="footer-toolbar">
	
	
    		<div class='ft-partners'>					
																<ul><li><a href="http://www.facebook.com/HiscoxSmallBiz" target="_blank" shape="rect"><img id="Footer - Facebook" src="/small-business-insurance/shared-images/icn-social-facebook.gif" alt="Facebook" title="Facebook" border="0" height="20" width="20" /></a></li><li><a href="http://twitter.com/HiscoxSmallBiz" target="_blank" shape="rect"><img id="Footer - Twitter" src="/small-business-insurance/shared-images/icn-social-twitter.gif" alt="Twitter" title="Twitter" border="0" height="20" width="20" /></a></li><li><a href="http://www.linkedin.com/company/hiscox-small-business-insurance" target="_blank" shape="rect"><img id="Footer - Linked In" src="/small-business-insurance/shared-images/icn-social-linkedin.gif" alt="Linked In" title="Linked In" border="0" height="20" width="20" /></a></li><li><a href="http://www.youtube.com/user/hiscoxinsurance" target="_blank" shape="rect"><img id="Footer - Youtube" src="/small-business-insurance/shared-images/icn-social-youtube.gif" alt="Youtube" title="Youtube" border="0" height="20" width="20" /></a></li><li><a href="http://www.hiscoxusa.com/small-business-insurance/blog/feed/" target="_blank" shape="rect"><img id="Footer - RSS" src="/small-business-insurance/shared-images/icn-social-rss.gif" alt="RSS" title="RSS" border="0" height="20" width="20" /></a></li></ul>
										
    	    		</div>
		 			
	
    		<div class='ft-contact'>					
																<div itemscope="" itemtype="http://schema.org/Corporation"><div class="ft-contact">Need help? <a href="" title="">Contact</a> our licensed agents, or call us at <span itemprop="telephone">1.800.456.1234</span></div></div>
										
    	    		</div>
		 			
	
    		<div class='ft-social'>					
																<ul><li><a href="http://www.bbb.org/new-york-city/business-reviews/insurance-services/hiscox-inc-in-new-york-ny-123713/#bbbonlineclick" title="Hiscox Inc. BBB Business Review"><img alt="Hiscox Inc. BBB Business Review" src="http://seal-newyork.bbb.org/seals/blue-seal-96-50-hiscox-inc-123713.png" style=" border: 0;" /></a></li></ul>
										
    	    		</div>
		 		</div>
						
        					</div>
        				</div>
    					<div class="sb-footer-btm"></div>
        				<div class="sb-baseline">
        					<div class="sb-baseline-inner">
        						<div class="baseline-copyright">
        							
	
	
    							
																<p class="body-copy-grey-small">� 2013 Hiscox Inc. All rights reserved<br />Underwritten by Hiscox Insurance Company Inc.</p>
										
    	    		
		 		        						</div>
        						<div class="baseline-country">
        							
	
	
    							
							<div class="footer-dropdown-box">
                <label for="country">Not in the US?</label>
                <select name="country" id="country" onchange="if(this.options[this.selectedIndex].value!='')openChosenURL(this);">
                <option value="">Select your country</option>
                                                	                		<option value="http://www.hiscox.be">Belgium</option>
                	                		<option value="http://www.hiscox.bm">Bermuda</option>
                	                		<option value="http://www.hiscox.fr">France</option>
                	                		<option value="http://www.hiscox.de">Germany</option>
                	                		<option value="http://www.hiscox.ie">Ireland</option>
                	                		<option value="http://www.hiscox.nl">Netherlands</option>
                	                		<option value="http://www.hiscox.pt">Portugal</option>
                	                		<option value="http://www.hiscox.es">Spain</option>
                	                		<option value="http://www.hiscox.co.uk">United Kingdom</option>
                	                				</select></div>
						
    	    		
		 		        						</div>
        						<ul class='footer-links-home-box'>
	
	
	
    							
																  <li><p class="body-copy-grey">
	                    <a href="/accessibility/" title="" target="_self" >
							Accessibility</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/site-map.html" title="" target="_self" >
							Site map</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/terms-of-use/" title="" target="_self" >
							Terms of use</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/privacy-policy/" title="" target="_self" >
							Privacy policy</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/legal-notices/" title="" target="_self" >
							Legal notices</a></p>
					  </li>
										
    	    		
		 		</ul>        						
        					</div>
        				</div>
										</div>
			
			</div>
			
			
 
 
		<div style="width:100%;align:center">
			<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Professional Liability - E&O
URL of the webpage where the tag is expected to be placed: http://www.hiscoxusa.com/small-business-insurance/errors-and-omissions-insurance.htm
Creation Date: 05/06/2010
-->
<iframe src="https://fls.doubleclick.net/activityi;src=2723417;type=busin671;cat=profe332;qty=1;cost=[Revenue];u5=[Occupation];u2=[State];ord=[OrderID]?" width="1" height="1" frameborder="0"></iframe>
 
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->		</div>
	
		<script type="text/javascript">
		
		$(document).ready(function () {
 
			$(".top-nav ul li .panel-bottom-middle").css("width", function () { 
    				var t = $(this).find(".quote-link").width();
					return t + 10;
    		});
			
			$(".top-nav ul li .panel").css("top", function () {
				var top = 19;
				
    			$(".top-nav ul li a.top-nav-link").each(function () { 
    				if($(this).height() > top) {
    					top = $(this).height();
    				}
        		});
				$(".top-nav ul li a.top-nav-link").css("height", top);
				
				return top + 6;
			});
			
			
			$(".top-nav ul li").hover(function () {
				var panelHeight = 0;
				$(this).find(".panel-col").each(function () {
					if($(this).height() > panelHeight) {
						panelHeight = $(this).height();
					}
				});
				
				$(this).find(".panel-col").css("min-height", panelHeight);
			});
			
			var tcnHeight = 0;
			
			$(".help-left").each(function () {
				if($(this).height() > tcnHeight) {
					tcnHeight = $(this).height();
				}
			});
			$(".help-right").each(function () {
				if($(this).height() > tcnHeight) {
					tcnHeight = $(this).height();
				}
			});
			
			$(".help-left").css("height", tcnHeight);
			
			$(".home-lower .col-more").each(function () {
				$(this).css('width',$(this).parent().width());
				$(this).addClass("hl-col-moved");
				$(this).addClass("col-more-moved");
				$(this).addClass($(this).parent().attr('class'));
				$(this).removeClass('hl-col');
				$(this).removeClass('col-more');
				$(this).appendTo($(this).parent().parent());
			});
			
			var hlHeight = 0;
			
			$(".hl-col").each(function () {
				if($(this).height() > hlHeight) {
					hlHeight = $(this).height();
				}
			});
			
			$(".hl-col").css("height", hlHeight-10);
			
		});
		
		</script>
		
	<!--BEGIN_SPEEDTRAP--><SCRIPT language="JavaScript" type="text/javascript" src="/hdc.js" ></SCRIPT><noscript><img src='https://hdc.hiscox.com/OWCPZPDTRUV/noScript.gif' alt=''></noscript>
<!--END_SPEEDTRAP-->
<!--BEGIN_SESSIONCAM  -->
<script type="text/javascript"> 
//<![CDATA[
var ServiceTickDetection=function(){var g=(navigator.appVersion.indexOf("MSIE")!=-1)?true:false;var f=(navigator.appVersion.toLowerCase().indexOf("win")!=-1)?true:false;var e=(navigator.userAgent.indexOf("Opera")!=-1)?true:false;function b(){var i;var j;var k;try{j=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");i=j.GetVariable("$version")}catch(k){}if(!i){try{j=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");i="WIN 6,0,21,0";j.AllowScriptAccess="always";i=j.GetVariable("$version")}catch(k){}}if(!i){try{j=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");i=j.GetVariable("$version")}catch(k){}}if(!i){try{j=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");i="WIN 3,0,18,0"}catch(k){}}if(!i){try{j=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");i="WIN 2,0,0,11"}catch(k){i=-1}}return i}function d(){var o=-1;if(navigator.plugins!=null&&navigator.plugins.length>0){if(navigator.plugins["Shockwave Flash 2.0"]||navigator.plugins["Shockwave Flash"]){var n=navigator.plugins["Shockwave Flash 2.0"]?" 2.0":"";var i=navigator.plugins["Shockwave Flash"+n].description;var m=i.split(" ");var k=m[2].split(".");var p=k[0];var j=k[1];var l=m[3];if(l==""){l=m[4]}if(l[0]=="d"){l=l.substring(1)}else{if(l[0]=="r"){l=l.substring(1);if(l.indexOf("d")>0){l=l.substring(0,l.indexOf("d"))}}}var o=p+"."+j+"."+l}}else{if(navigator.userAgent.toLowerCase().indexOf("webtv/2.6")!=-1){o=4}else{if(navigator.userAgent.toLowerCase().indexOf("webtv/2.5")!=-1){o=3}else{if(navigator.userAgent.toLowerCase().indexOf("webtv")!=-1){o=2}else{if(g&&f&&!e){o=b()}}}}}return o}function h(n,l,k){versionStr=d();if(versionStr==-1){return false}else{if(versionStr!=0){if(g&&f&&!e){tempArray=versionStr.split(" ");tempString=tempArray[1];versionArray=tempString.split(",")}else{versionArray=versionStr.split(".")}var m=versionArray[0];var i=versionArray[1];var j=versionArray[2];if(m>parseFloat(n)){return true}else{if(m==parseFloat(n)){if(i>parseFloat(l)){return true}else{if(i==parseFloat(l)){if(j>=parseFloat(k)){return true}}}}}return false}}}function a(){if(g){window.attachEvent("onunload",function i(){try{var l=document.body.getElementsByTagName("object");for(var j=0;j<l.length;j++){if(l[j].parentNode){l[j].parentNode.removeChild(l[j])}}}catch(k){}})}}function c(){}return{DetectFlashVer:function(j,k,i){return h(j,k,i)},GetSwfVer:function(){return d()},Version:function(){return"4.1"},setUnloadHandlerForIE:function(){a()}}}();(function(d){ServiceTickDetection.setUnloadHandlerForIE();var e,f,a,b,c;e=ServiceTickDetection.DetectFlashVer(9,0,0);if(e){document.write('<object id="stflashob" style="display:inline;position:absolute;top:-100px;left:-100px;" class="ServiceTickHidden" width="1" height="1" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">');document.write(' <param name="movie" value="'+window.location.protocol+'//d2oh4tlt9mrke9.cloudfront.net/Record/swfhttprequest.swf" />');document.write(' <param name="allowScriptAccess" value="always" />');document.write(' <embed id="stflashembed" width="1" height="1" src="'+window.location.protocol+'//d2oh4tlt9mrke9.cloudfront.net/Record/swfhttprequest.swf" class="ServiceTickHidden" allowscriptaccess="always" type="application/x-shockwave-flash"></embed>');document.write("</object>")}f=document.createElement("script");f.type="text/javascript";f.src=window.location.protocol+"//d2oh4tlt9mrke9.cloudfront.net/Record/js/sessioncam.recorder.js";document.getElementsByTagName("head")[0].appendChild(f)})();
//]]>
</script>
<!--END_SESSIONCAM-->
<!--BEGIN_GA-->
<script type="text/javascript"> (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);  })();</script>
<!--END_GA-->
 <script type="text/javascript">

var businesscategorylist=document.ScreenBuilderForm.businesscategory;
var primarybusinesslist=document.ScreenBuilderForm.primarybusiness;
var primarybusiness=new Array();
     primarybusiness[0]=["BUSINESS DESCRIPTION|","Accounting|Accounting","Actuarial services|Actuarial services","Acupressure services|Acupressure services","Acupuncture services|Acupuncture services","Advertising|Advertising","Answering/paging services|Answering/paging services","Application development|Application development","Application service provider|Application service provider","Architecture|Architecture","Art therapy|Art therapy","Auctioneering|Auctioneering","Audiology|Audiology","Beautician/cosmetology services|Beautician/cosmetology services","Bookkeeping|Bookkeeping","Brand consultant|Brand consultant","Building inspection|Building/construction inspection","Business consulting|Business consulting","Business manager services|Business manager services","Civil engineering|Civil engineering","Claims adjusting|Claims adjusting","Computer consulting|Computer consulting","Computer programming services|Computer programming services","Computer system/network developer|Computer system/network developer","Project manager (architecture or engineering)|Construction/project management","Control systems integration/automation|Control systems integration/automation","Court reporting|Court reporting","Credit counseling|Credit counseling","Dance therapy|Dance therapy","Data processing|Data processing","Database designer|Database designer","Dietician/nutrition|Dietician/nutrition","Digital marketing|Digital marketing","Direct marketing|Direct marketing","Document preparation|Document preparation","Draftsman (including CAD/CAM)|Draftsman (including CAD/CAM)","Drama therapy|Drama therapy","Education consulting|Education consulting","Electrical engineering|Electrical engineering","Engineering|Engineering","Environmental engineering|Environmental engineering","Esthetician services|Esthetician services","Event planning/promotion|Event planning/promotion","Executive placement|Executive placement","Expert witness services|Expert witness services","Financial auditing or consulting|Financial auditing or consulting","First aid and CPR training|First aid and CPR training","Graphic design|Graphic design","Hair stylist/barber services|Barber/hair stylist services","Human Resources (HR) consulting|Human Resources (HR) consulting","Hypnosis|Hypnosis","Industrial engineering|Industrial engineering","Interior design|Interior design","Investment advice|Investment advice","IT consulting|IT consulting","IT project management|IT project management","IT software/hardware training services|IT software/hardware training services","Landscape architect|Landscape architect","Legal services|Legal services","Life/career/executive coaching|Life/career/executive coaching","Management consulting|Management consulting","Marketing/media consulting|Marketing/media consulting","Market research|Market research","Marriage and family therapy|Marriage and family therapy","Massage therapy|Massage therapy","Medical billing|Medical billing","Mental health counseling|Mental health counseling","Mortgage brokering/banking|Mortgage brokering/banking","Music therapy|Music therapy","Nail technician services|Nail technician services","Notary services|Notary services","Occupational therapy|Occupational therapy","Personal concierge/assistant|Personal concierge/assistant","Personal training (health and fitness)|Personal training (health and fitness)","Photography|Photography","Process engineering|Process engineering","Process server|Process server","Property management|Property management","Project management|Project management","Psychology|Psychology","Public relations|Public relations","Real estate agent/broker|Real estate agent/broker","Recruiting (employment placements)|Recruiting (employment placements)","Research consulting|Research consulting","Resume consulting|Resume consulting","Search engine services (SEO/SEM)|Search engine services (SEO/SEM)","Social media consultant|Social media consultant","Social work services|Social work services","Software development|Software development","Speech therapy|Speech therapy","Stock brokering|Stock brokering","Strategy consultant|Strategy consultant","Substance abuse counseling|Substance abuse counseling","Talent agency|Talent agency","Tax preparation|Tax preparation","Technology services|Technology services","Training (business, vocational or life skills)|Training (business, vocational or life skills)","Translating/interpreting|Translating/interpreting","Transportation engineering|Transportation engineering","Travel agency|Travel agency","Trustee|Trustee","Tutoring|Tutoring","Value added reseller of computer hardware|Value added reseller of computer hardware","Website design|Website design","Yoga/pilates instruction|Yoga/pilates instruction","Other architecture, engineering & design services|Other architecture, engineering & design services","Other consulting services|Other consulting services","Other creative services|Other creative services","Other financial services|Other financial services","Other health, beauty & wellness services|Other health, beauty & wellness services","Other legal services|Other legal services","Other marketing/PR services|Other marketing/PR services","Other professional services|Other professional services","Other real estate services|Other real estate services","Other technology services|Other technology services"];
      primarybusiness[1]=["Architecture|Architecture", "Building inspection|Building/construction inspection", "Civil engineering|Civil engineering", "Project manager (architecture or engineering)|Construction/project management", "Control systems integration/automation|Control systems integration/automation","Draftsman (including CAD/CAM)|Draftsman (including CAD/CAM)","Electrical engineering|Electrical engineering","Engineering|Engineering","Environmental engineering|Environmental engineering","Industrial engineering|Industrial engineering","Interior design|Interior design","Landscape architect|Landscape architect","Process engineering|Process engineering","Transportation engineering|Transportation engineering","Other architecture, engineering & design services|Other architecture, engineering & design services"];

 primarybusiness[2]=["Business consulting|Business consulting", "Education consulting|Education consulting", "Financial auditing or consulting|Financial auditing or consulting", "Human Resources (HR) consulting|Human Resources (HR) consulting", "IT consulting|IT consulting","IT project management|IT project management","IT software/hardware training services|IT software/hardware training services","Management consulting|Management consulting","Marketing/media consulting|Marketing/media consulting","Project management|Project management","Research consulting|Research consulting","Resume consulting|Resume consulting","Strategy consultant|Strategy consultant","Training (business, vocational or life skills)|Training (business, vocational or life skills)","Other consulting services|Other consulting services"];

    primarybusiness[3]=["Advertising|Advertising", "Application development|Application development", "Brand consultant|Brand consultant", "Event planning/promotion|Event planning/promotion","Graphic design|Graphic design","Interior design|Interior design","Photography|Photography","Search engine services (SEO/SEM)|Search engine services (SEO/SEM)","Social media consultant|Social media consultant","Website design|Website design","Other creative services|Other creative services"];

    primarybusiness[4]=["Accounting|Accounting", "Actuarial services|Actuarial services", "Auctioneering|Auctioneering", "Bookkeeping|Bookkeeping", "Claims adjusting|Claims adjusting","Credit counseling|Credit counseling","Financial auditing or consulting|Financial auditing or consulting","Investment advice|Investment advice","Medical billing|Medical billing","Mortgage brokering/banking|Mortgage brokering/banking","Notary services|Notary services","Stock brokering|Stock brokering","Tax preparation|Tax preparation","Trustee|Trustee","Other financial services|Other financial services"];

primarybusiness[5]=["Acupressure services|Acupressure services", "Acupuncture services|Acupuncture services", "Art therapy|Art therapy", "Audiology|Audiology", "Beautician/cosmetology services|Beautician/cosmetology services","Dance therapy|Dance therapy","Dietician/nutrition|Dietician/nutrition","Drama therapy|Drama therapy","Esthetician services|Esthetician services","First aid and CPR training|First aid and CPR training","Hair stylist/barber services|Barber/hair stylist services","Hypnosis|Hypnosis","Marriage and family therapy|Marriage and family therapy","Massage therapy|Massage therapy","Medical billing|Medical billing","Mental health counseling|Mental health counseling","Music therapy|Music therapy","Nail technician services|Nail technician services","Occupational therapy|Occupational therapy","Personal training (health and fitness)|Personal training (health and fitness)","Psychology|Psychology","Social work services|Social work services","Speech therapy|Speech therapy","Substance abuse counseling|Substance abuse counseling","Yoga/pilates instruction|Yoga/pilates instruction","Other health, beauty & wellness services|Other health, beauty & wellness services"];

primarybusiness[6]=["Claims adjusting|Claims adjusting", "Court reporting|Court reporting", "Document preparation|Document preparation", "Expert witness services|Expert witness services", "Legal services|Legal services","Notary services|Notary services","Process server|Process server","Tax preparation|Tax preparation","Trustee|Trustee","Other legal services|Other legal services"];

primarybusiness[7]=["Advertising|Advertising", "Answering/paging services|Answering/paging services", "Application development|Application development", "Brand consultant|Brand consultant", "Digital marketing|Digital marketing","Direct marketing|Direct marketing","Event planning/promotion|Event planning/promotion","Graphic design|Graphic design","Marketing/media consulting|Marketing/media consulting","Market research|Market research","Public relations|Public relations","Search engine services (SEO/SEM)|Search engine services (SEO/SEM)","Social media consultant|Social media consultant","Website design|Website design","Other marketing/PR services|Other marketing/PR services"];

primarybusiness[8]=["Answering/paging services|Answering/paging services", "Auctioneering|Auctioneering", "Business manager services|Business manager services", "Claims adjusting|Claims adjusting", "Court reporting|Court reporting","Credit counseling|Credit counseling","Document preparation|Document preparation","Event planning/promotion|Event planning/promotion","Executive placement|Executive placement","Expert witness services|Expert witness services","Financial auditing or consulting|Financial auditing or consulting","Life/career/executive coaching|Life/career/executive coaching","Medical billing|Medical billing","Notary services|Notary services","Personal concierge/assistant|Personal concierge/assistant","Photography|Photography","Project management|Project management","Recruiting (employment placements)|Recruiting (employment placements)","Resume consulting|Resume consulting","Talent agency|Talent agency","Training (business, vocational or life skills)|Training (business, vocational or life skills)","Translating/interpreting|Translating/interpreting","Travel agency|Travel agency","Trustee|Trustee","Tutoring|Tutoring","Other professional services|Other professional services"];

primarybusiness[9]=["Property management|Property management", "Real estate agent/broker|Real estate agent/broker", "Other real estate services|Other real estate services"];

primarybusiness[10]=["Application development|Application development", "Application service provider|Application service provider", "Computer consulting|Computer consulting", "Computer programming services|Computer programming services", "Computer system/network developer|Computer system/network developer","Data processing|Data processing","Database designer|Database designer","IT consulting|IT consulting","IT project management|IT project management","IT software/hardware training services|IT software/hardware training services","Software development|Software development","Technology services|Technology services","Value added reseller of computer hardware|Value added reseller of computer hardware","Website design|Website design","Other technology services|Other technology services"];
primarybusiness[11]=["BUSINESS DESCRIPTION|NONE OF THE ABOVE"];
function updateprimarybusiness(selectedcitygroup){
      primarybusinesslist.options.length=0
if (selectedcitygroup>=0){
	if (selectedcitygroup==11)
	{
	  primarybusinesslist.disabled=true;
	  document.getElementById('drop3').className="selctBox-bg disaled-selct";
	  primarybusinesslist.style.cursor="default";
	}
	else
	{
	primarybusinesslist.disabled=false;
	  document.getElementById('drop3').className="selctBox-bg enabled-selct";
	primarybusinesslist.style.cursor="pointer";
	}
for (var i=0; i<primarybusiness[selectedcitygroup].length; i++)
      primarybusinesslist.options[primarybusinesslist.options.length]=new Option(primarybusiness[selectedcitygroup][i].split("|")[0], primarybusiness[selectedcitygroup][i].split("|")[1]);

document.getElementById('selct-text').innerHTML=primarybusinesslist.options[0].text.toUpperCase();

}
}
</script>
</body>
</html>
