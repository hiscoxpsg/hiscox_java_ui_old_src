<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Important New York disclosure for claims-made policies | Hiscox USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print" />
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body>
  <div class="main-container-popup">
  <!-- Header starts here-->
	<div class="header-popup">
		<div class="popuplogo" id="print_rx_logo_id">
	
	
	
    							
			                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		
    <div class="popup-hdr-text">
	      <ol>
		<li class='red-button right-arrow-white float-right'>
        		</li>
    </ol>
       </div>
  </div>
  <!-- Header ends here-->
  <!-- Page container starts here-->
  <div class="page-container">
      <!--Screen builder section starts here-->
    <div class="cnt-sctn popup-centr-sctn" id="print_rx_cnt_id">
      <div class="top">
        <div class="right" >
          <div class="left">
            <div class="middle"> </div>
          </div>
        </div>
      </div>
      <div class="middle-content">
        <div class="page-heading page-heading-border">
        		    																					<usd:checkOccup occupContent="it-consulting" >
    	
    	<h1 class="page-heading-h1">Important New York disclosure for claims-made policies</h1>    	
		</usd:checkOccup>
	    																					<usd:checkOccup occupContent="consulting" >
    	
    	<h1 class="page-heading-h1">Important New York disclosure for claims-made policies</h1>    	
		</usd:checkOccup>
	    																					<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<h1 class="page-heading-h1">Important New York disclosure for claims-made policies</h1>    	
		</usd:checkOccup>
	    																					<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<h1 class="page-heading-h1">Important New York disclosure for claims-made policies</h1>    	
		</usd:checkOccup>
	    																							<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<h1 class="page-heading-h1">Important New York disclosure for claims-made policies</h1>    	
		</usd:checkOccup>
	    	        <div class="reference-number">
			<usd:checkInclude  includeContent="quoteRefID">
				<span>Reference # <usd:include sessionValue="quoteRefID"/></span>
			</usd:checkInclude>	
		</div>
        </div>
        <div class="clear"></div>
        <div class="topcnt-container">
                   																						<usd:checkOccup occupContent="it-consulting" >
    	
    	<p class="body-copy-grey">NEW YORK POLICYHOLDER NOTICE – ADDENDUM TO THE APPLICATION</p><p class="body-copy-grey">PROFESSIONAL LIABILITY – ERRORS AND OMISSIONS INSURANCE POLICY</p><p class="body-copy-grey">The following notice is being provided in accordance with New York Law:</p><p class="body-copy-grey">THIS POLICY CONTAINS COVERAGE THAT IS WRITTEN ON A CLAIMS-MADE BASIS.</p><p class="body-copy-grey">WITH RESPECT TO SUCH COVERAGE, THIS POLICY DOES NOT APPLY TO CLAIMS ARISING OUT OF INCIDENTS, OCCURRENCES OR ALLEGED WRONGFUL ACTS WHICH TOOK PLACE PRIOR TO THE APPLICABLE RETROACTIVE DATE (IF ANY).</p><p class="body-copy-grey">SUCH CLAIMS-MADE COVERAGE APPLIES ONLY TO THOSE CLAIMS ACTUALLY MADE AGAINST THE INSURED WHILE THE POLICY REMAINS IN EFFECT. ALL COVERAGE PROVIDED ON A CLAIMS-MADE BASIS UNDER THIS POLICY CEASES UPON TERMINATION OF THE POLICY, EXCEPT COVERAGE FOR CLAIMS REPORTED DURING THE AUTOMATIC EXTENDED REPORTING PERIOD OR DURING ONE OF THE OPTIONAL EXTENDED REPORTING PERIODS, IF PURCHASED.</p><p class="body-copy-grey">THE NATURE OF CLAIMS-MADE COVERAGE IS SUCH THAT DURING THE FIRST SEVERAL YEARS OF CONTINUING CLAIMS-MADE COVERAGE, CLAIMS-MADE PREMIUMS ARE COMPARATIVELY LOWER THAN OCCURRENCE COVERAGE PREMIUM. AN INSURED CAN EXPECT SUBSTANTIAL ANNUAL PREMIUM INCREASES, INDEPENDENT OF OVERALL RATE LEVEL INCREASES, UNTIL THE CLAIMS-MADE RELATIONSHIP REACHES MATURITY.</p><p class="body-copy-grey">THE FOLLOWING EXTENDED REPORTING PERIODS ARE AVAILABLE UPON TERMINATION OF COVERAGE:</p><ul class="list-bulleted-body-copy-grey"><li>60 Day Automatic Extended Reporting Period</li><li>1 Year Optional Extended Reporting Period</li><li>2 Year Optional Extended Reporting Period</li><li>3 Year Optional Extended Reporting Period</li></ul><p class="body-copy-grey"> </p><p class="body-copy-grey">POTENTIAL COVERAGE GAPS MAY ARISE UPON EXPIRATION OF THE EXTENDED REPORTING PERIODS.</p>    	
		</usd:checkOccup>
																							<usd:checkOccup occupContent="consulting" >
    	
    	<p class="body-copy-grey">NEW YORK POLICYHOLDER NOTICE – ADDENDUM TO THE APPLICATION</p><p class="body-copy-grey">PROFESSIONAL LIABILITY – ERRORS AND OMISSIONS INSURANCE POLICY</p><p class="body-copy-grey">The following notice is being provided in accordance with New York Law:</p><p class="body-copy-grey">THIS POLICY CONTAINS COVERAGE THAT IS WRITTEN ON A CLAIMS-MADE BASIS.</p><p class="body-copy-grey">WITH RESPECT TO SUCH COVERAGE, THIS POLICY DOES NOT APPLY TO CLAIMS ARISING OUT OF INCIDENTS, OCCURRENCES OR ALLEGED WRONGFUL ACTS WHICH TOOK PLACE PRIOR TO THE APPLICABLE RETROACTIVE DATE (IF ANY).</p><p class="body-copy-grey">SUCH CLAIMS-MADE COVERAGE APPLIES ONLY TO THOSE CLAIMS ACTUALLY MADE AGAINST THE INSURED WHILE THE POLICY REMAINS IN EFFECT. ALL COVERAGE PROVIDED ON A CLAIMS-MADE BASIS UNDER THIS POLICY CEASES UPON TERMINATION OF THE POLICY, EXCEPT COVERAGE FOR CLAIMS REPORTED DURING THE AUTOMATIC EXTENDED REPORTING PERIOD OR DURING ONE OF THE OPTIONAL EXTENDED REPORTING PERIODS, IF PURCHASED.</p><p class="body-copy-grey">THE NATURE OF CLAIMS-MADE COVERAGE IS SUCH THAT DURING THE FIRST SEVERAL YEARS OF CONTINUING CLAIMS-MADE COVERAGE, CLAIMS-MADE PREMIUMS ARE COMPARATIVELY LOWER THAN OCCURRENCE COVERAGE PREMIUM. AN INSURED CAN EXPECT SUBSTANTIAL ANNUAL PREMIUM INCREASES, INDEPENDENT OF OVERALL RATE LEVEL INCREASES, UNTIL THE CLAIMS-MADE RELATIONSHIP REACHES MATURITY.</p><p class="body-copy-grey">THE FOLLOWING EXTENDED REPORTING PERIODS ARE AVAILABLE UPON TERMINATION OF COVERAGE:</p><ul class="list-bulleted-body-copy-grey"><li>60 Day Automatic Extended Reporting Period</li><li>1 Year Optional Extended Reporting Period</li><li>2 Year Optional Extended Reporting Period</li><li>3 Year Optional Extended Reporting Period</li></ul><p class="body-copy-grey"> </p><p class="body-copy-grey">POTENTIAL COVERAGE GAPS MAY ARISE UPON EXPIRATION OF THE EXTENDED REPORTING PERIODS.</p>    	
		</usd:checkOccup>
																							<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<p class="body-copy-grey">NEW YORK POLICYHOLDER NOTICE – ADDENDUM TO THE APPLICATION</p><p class="body-copy-grey">PROFESSIONAL LIABILITY – ERRORS AND OMISSIONS INSURANCE POLICY</p><p class="body-copy-grey">The following notice is being provided in accordance with New York Law:</p><p class="body-copy-grey">THIS POLICY CONTAINS COVERAGE THAT IS WRITTEN ON A CLAIMS-MADE BASIS.</p><p class="body-copy-grey">WITH RESPECT TO SUCH COVERAGE, THIS POLICY DOES NOT APPLY TO CLAIMS ARISING OUT OF INCIDENTS, OCCURRENCES OR ALLEGED WRONGFUL ACTS WHICH TOOK PLACE PRIOR TO THE APPLICABLE RETROACTIVE DATE (IF ANY).</p><p class="body-copy-grey">SUCH CLAIMS-MADE COVERAGE APPLIES ONLY TO THOSE CLAIMS ACTUALLY MADE AGAINST THE INSURED WHILE THE POLICY REMAINS IN EFFECT. ALL COVERAGE PROVIDED ON A CLAIMS-MADE BASIS UNDER THIS POLICY CEASES UPON TERMINATION OF THE POLICY, EXCEPT COVERAGE FOR CLAIMS REPORTED DURING THE AUTOMATIC EXTENDED REPORTING PERIOD OR DURING ONE OF THE OPTIONAL EXTENDED REPORTING PERIODS, IF PURCHASED.</p><p class="body-copy-grey">THE NATURE OF CLAIMS-MADE COVERAGE IS SUCH THAT DURING THE FIRST SEVERAL YEARS OF CONTINUING CLAIMS-MADE COVERAGE, CLAIMS-MADE PREMIUMS ARE COMPARATIVELY LOWER THAN OCCURRENCE COVERAGE PREMIUM. AN INSURED CAN EXPECT SUBSTANTIAL ANNUAL PREMIUM INCREASES, INDEPENDENT OF OVERALL RATE LEVEL INCREASES, UNTIL THE CLAIMS-MADE RELATIONSHIP REACHES MATURITY.</p><p class="body-copy-grey">THE FOLLOWING EXTENDED REPORTING PERIODS ARE AVAILABLE UPON TERMINATION OF COVERAGE:</p><ul class="list-bulleted-body-copy-grey"><li>60 Day Automatic Extended Reporting Period</li><li>1 Year Optional Extended Reporting Period</li><li>2 Year Optional Extended Reporting Period</li><li>3 Year Optional Extended Reporting Period</li></ul><p class="body-copy-grey"> </p><p class="body-copy-grey">POTENTIAL COVERAGE GAPS MAY ARISE UPON EXPIRATION OF THE EXTENDED REPORTING PERIODS.</p>    	
		</usd:checkOccup>
																							<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<p class="body-copy-grey">NEW YORK POLICYHOLDER NOTICE – ADDENDUM TO THE APPLICATION</p><p class="body-copy-grey">PROFESSIONAL LIABILITY – ERRORS AND OMISSIONS INSURANCE POLICY</p><p class="body-copy-grey">The following notice is being provided in accordance with New York Law:</p><p class="body-copy-grey">THIS POLICY CONTAINS COVERAGE THAT IS WRITTEN ON A CLAIMS-MADE BASIS.</p><p class="body-copy-grey">WITH RESPECT TO SUCH COVERAGE, THIS POLICY DOES NOT APPLY TO CLAIMS ARISING OUT OF INCIDENTS, OCCURRENCES OR ALLEGED WRONGFUL ACTS WHICH TOOK PLACE PRIOR TO THE APPLICABLE RETROACTIVE DATE (IF ANY).</p><p class="body-copy-grey">SUCH CLAIMS-MADE COVERAGE APPLIES ONLY TO THOSE CLAIMS ACTUALLY MADE AGAINST THE INSURED WHILE THE POLICY REMAINS IN EFFECT. ALL COVERAGE PROVIDED ON A CLAIMS-MADE BASIS UNDER THIS POLICY CEASES UPON TERMINATION OF THE POLICY, EXCEPT COVERAGE FOR CLAIMS REPORTED DURING THE AUTOMATIC EXTENDED REPORTING PERIOD OR DURING ONE OF THE OPTIONAL EXTENDED REPORTING PERIODS, IF PURCHASED.</p><p class="body-copy-grey">THE NATURE OF CLAIMS-MADE COVERAGE IS SUCH THAT DURING THE FIRST SEVERAL YEARS OF CONTINUING CLAIMS-MADE COVERAGE, CLAIMS-MADE PREMIUMS ARE COMPARATIVELY LOWER THAN OCCURRENCE COVERAGE PREMIUM. AN INSURED CAN EXPECT SUBSTANTIAL ANNUAL PREMIUM INCREASES, INDEPENDENT OF OVERALL RATE LEVEL INCREASES, UNTIL THE CLAIMS-MADE RELATIONSHIP REACHES MATURITY.</p><p class="body-copy-grey">THE FOLLOWING EXTENDED REPORTING PERIODS ARE AVAILABLE UPON TERMINATION OF COVERAGE:</p><ul class="list-bulleted-body-copy-grey"><li>60 Day Automatic Extended Reporting Period</li><li>1 Year Optional Extended Reporting Period</li><li>2 Year Optional Extended Reporting Period</li><li>3 Year Optional Extended Reporting Period</li></ul><p class="body-copy-grey"> </p><p class="body-copy-grey">POTENTIAL COVERAGE GAPS MAY ARISE UPON EXPIRATION OF THE EXTENDED REPORTING PERIODS.</p>    	
		</usd:checkOccup>
																									<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<p class="body-copy-grey">NEW YORK POLICYHOLDER NOTICE – ADDENDUM TO THE APPLICATION</p><p class="body-copy-grey">PROFESSIONAL LIABILITY – ERRORS AND OMISSIONS INSURANCE POLICY</p><p class="body-copy-grey">The following notice is being provided in accordance with New York Law:</p><p class="body-copy-grey">THIS POLICY CONTAINS COVERAGE THAT IS WRITTEN ON A CLAIMS-MADE BASIS.</p><p class="body-copy-grey">WITH RESPECT TO SUCH COVERAGE, THIS POLICY DOES NOT APPLY TO CLAIMS ARISING OUT OF INCIDENTS, OCCURRENCES OR ALLEGED WRONGFUL ACTS WHICH TOOK PLACE PRIOR TO THE APPLICABLE RETROACTIVE DATE (IF ANY).</p><p class="body-copy-grey">SUCH CLAIMS-MADE COVERAGE APPLIES ONLY TO THOSE CLAIMS ACTUALLY MADE AGAINST THE INSURED WHILE THE POLICY REMAINS IN EFFECT. ALL COVERAGE PROVIDED ON A CLAIMS-MADE BASIS UNDER THIS POLICY CEASES UPON TERMINATION OF THE POLICY, EXCEPT COVERAGE FOR CLAIMS REPORTED DURING THE AUTOMATIC EXTENDED REPORTING PERIOD OR DURING ONE OF THE OPTIONAL EXTENDED REPORTING PERIODS, IF PURCHASED.</p><p class="body-copy-grey">THE NATURE OF CLAIMS-MADE COVERAGE IS SUCH THAT DURING THE FIRST SEVERAL YEARS OF CONTINUING CLAIMS-MADE COVERAGE, CLAIMS-MADE PREMIUMS ARE COMPARATIVELY LOWER THAN OCCURRENCE COVERAGE PREMIUM. AN INSURED CAN EXPECT SUBSTANTIAL ANNUAL PREMIUM INCREASES, INDEPENDENT OF OVERALL RATE LEVEL INCREASES, UNTIL THE CLAIMS-MADE RELATIONSHIP REACHES MATURITY.</p><p class="body-copy-grey">THE FOLLOWING EXTENDED REPORTING PERIODS ARE AVAILABLE UPON TERMINATION OF COVERAGE:</p><ul class="list-bulleted-body-copy-grey"><li>60 Day Automatic Extended Reporting Period</li><li>1 Year Optional Extended Reporting Period</li><li>2 Year Optional Extended Reporting Period</li><li>3 Year Optional Extended Reporting Period</li></ul><p class="body-copy-grey"> </p><p class="body-copy-grey">POTENTIAL COVERAGE GAPS MAY ARISE UPON EXPIRATION OF THE EXTENDED REPORTING PERIODS.</p>    	
		</usd:checkOccup>
				<div class="horz-line2"></div>
				</div>
		<usd:verifyToInclude>
		<usd:checkFormTag formContent="start"></usd:checkFormTag>
		<usd:questionTag  />  
		<usd:actionTag  />
	<usd:trackingTag />
	<usd:checkFormTag formContent="end"></usd:checkFormTag>
	</usd:verifyToInclude>
	     <div class="btmbtn-cntnr"> 
						        		                									<div class="clear-both"></div>
		 </div> 
    <!--Screen builder section ends here-->
    </div>
    <div class="bottom clear-both" ><div class="right"> <div class="left"><div class="middle" ></div></div></div></div></div>
 </div>
<!-- Page container ends here-->
		  </div>
<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
</body>
</html>