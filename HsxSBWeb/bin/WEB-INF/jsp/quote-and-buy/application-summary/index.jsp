<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Application summary - terms and conditions | Hiscox</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
						                							                								<meta name="robots" content="noindex,nofollow" />
    			    					<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print" />
<!--[if lte IE 9]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<noscript><!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-nojs.css";</style><![endif]--></noscript>

			<script type="text/javascript">
    var lpTag = lpTag || {}; lpTag.vars = lpTag.vars  || []; lpTag.dbs = lpTag.dbs || [];
    lpTag.section = 'sales'; 


    // ======================= VARIABLES ===================================
    // A LivePerson variable can be defined in one of three different scopes:
    // 'page' - exists on the current page only
    // 'session' - exists for the current visitor session
    // 'visitor' - persists between browser sessions

    // ----------------- 1: Sending when the page loads --------------------
    // Variables are JavaScript objects that are passed in an array
    // Each object must contain: scope, name, value
    // Values should be strings
	
    var arrLPvars = [ 
	
        { scope:'page', name:'unit', value:'sales' },
        { scope:'session', name:'language', value:'english' },
        // You can add as many other variables as you need
        { scope:'page', name:'OrderTotal', value:'VARIABLE_VALUE' },
        { scope:'page', name:'OrderNumber', value:'VARIABLE_VALUE' }
        ];
    lpTag.vars.push(arrLPvars);

  </script>
	</head>
<body>

	
	
	
    	<div class="main-container">
<!-- Header starts here-->
	<div class="header">
		<div class="logo" id="print_rx_logo_id">
	
	
	
    							
															                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		<form method="post">
	      <div class="header-btncontainer">
											<usd:checkLogin loginContent="logout">
						<div class="header-btns float-right"><div class="submit-button grey-button logout-icon">
								<span class='submit-button-end'><input class='submit-input' type='submit' title="Log Out" value='Log Out' name="action_Log Out_button"  /><span class="button-icon"></span></span>
						</div></div>
					</usd:checkLogin>						           </div>
		   </form>
	</div>
	<!-- Header ends here-->
	<!-- Progress bar starts here-->
			

    		    	<usd:checkPage pageContent ="coverage-options">
        <div class="progress-bar small" >
        <ul>
        	<li class="first-red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Choose Products</span></div></li>
        	<li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="about-you">
        <div class="progress-bar small" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Choose Products</span></div></li>
        	<li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="your-business">
        <div class="progress-bar small" >
        <ul>
			<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Choose Products</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="last-red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="your-quote">
        <div class="progress-bar" >
        <ul>
			<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="important-information">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="application-summary">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="payment-details">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="completed">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="none">
        <div class="progress-bar" >
        <ul>
        	<li class="first-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
			        <!-- Page container starts here-->
	<div class="page-container">
		<div class="breadcrumb">
						    		<usd:checkEligibility eligibleContent="stateVariant,primarybusiness">	
				<div class="occupation-details">
					<span class = "float-right"><object>
							<div class="occupation-info"><span>State:</span> <a href="/small-business-insurance/quote-and-buy/change-your-business-type-and-state"><usd:include sessionValue="stateVariant" /></a></div>
							<div class="occupation-info"><span>Type of business:</span> <a href="/small-business-insurance/quote-and-buy/change-your-business-type-and-state"><usd:include sessionValue="primarybusiness" /></a></div>
					</object></span>
				</div>
    		</usd:checkEligibility>
								</div>    			
    	 		
				    		    			                                                                                        																															<div></div>
			<div></div>			
						<!--Left nav starts here-->
			    			<div class="left-nav">
    				<usd:checkOccup occupContent="it-consulting" > 
    						                                 									    		    		    		    			    										<div class="section-container left-nav-corner grey-toplft " >
											<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3><img id="redline_whychoose_left" src="/shared-images/redline_whychoose_left.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="191" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Tailored coverage</b> - we specialize in businesses like yours and tailor coverage to the risks in your field.</li><li><b>Great value</b> - tailored coverage starts from just $22.50/mo.</li><li><b>Fast and simple</b> - online quotes or speak to a licensed agent – immediate coverage.</li><li><b>Confidence</b> - Hiscox Insurance Company Inc. is ‘A’ rated (Excellent) by A.M. Best.</li></ul>						</div>
									</div>	
    			        				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="consulting" > 
    						                                 									    		    		    		    			    										<div class="section-container left-nav-corner grey-toplft " >
											<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3><img id="redline_whychoose_left" src="/shared-images/redline_whychoose_left.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="191" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Tailored coverage</b> - we specialize in businesses like yours and tailor coverage to the risks in your field.</li><li><b>Great value</b> - tailored coverage starts from just $22.50/mo.</li><li><b>Fast and simple</b> - online quotes or speak to a licensed agent – immediate coverage.</li><li><b>Confidence</b> - Hiscox Insurance Company Inc. is ‘A’ rated (Excellent) by A.M. Best.</li></ul>						</div>
									</div>	
    			        				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="marketing-pr-consulting" > 
    						                                 									    		    		    		    			    										<div class="section-container left-nav-corner grey-toplft " >
											<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3><img id="redline_whychoose_left" src="/shared-images/redline_whychoose_left.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="191" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Tailored coverage</b> - we specialize in businesses like yours and tailor coverage to the risks in your field.</li><li><b>Great value</b> - tailored coverage starts from just $22.50/mo.</li><li><b>Fast and simple</b> - online quotes or speak to a licensed agent – immediate coverage.</li><li><b>Confidence</b> - Hiscox Insurance Company Inc. is ‘A’ rated (Excellent) by A.M. Best.</li></ul>						</div>
									</div>	
    			        				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-known" > 
    						                                 									    		    		    		    			    										<div class="section-container left-nav-corner grey-toplft " >
											<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3><img id="redline_whychoose_left" src="/shared-images/redline_whychoose_left.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="191" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Tailored coverage</b> - we specialize in businesses like yours and tailor coverage to the risks in your field.</li><li><b>Great value</b> - tailored coverage starts from just $22.50/mo.</li><li><b>Fast and simple</b> - online quotes or speak to a licensed agent – immediate coverage.</li><li><b>Confidence</b> - Hiscox Insurance Company Inc. is ‘A’ rated (Excellent) by A.M. Best.</li></ul>						</div>
									</div>	
    			        				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-not-known" > 
    						                                 														<div class="section-container left-nav-corner grey-toplft " >
											<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3><img id="redline_whychoose_left" src="/shared-images/redline_whychoose_left.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="191" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Tailored coverage</b> - we specialize in businesses like yours and tailor coverage to the risks in your field.</li><li><b>Great value</b> - tailored coverage starts from just $22.50/mo.</li><li><b>Fast and simple</b> - online quotes or speak to a licensed agent – immediate coverage.</li><li><b>Confidence</b> - Hiscox Insurance Company Inc. is ‘A’ rated (Excellent) by A.M. Best.</li></ul>						</div>
									</div>	
			    										    										    										            										            										            										    										    										    										    																																            				</usd:checkOccup>
    				<usd:checkOccup occupContent="it-consulting" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="consulting" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="marketing-pr-consulting" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-known" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-not-known" > 
    						                                 										    										    										    										            										            										            										    										    										    										    																																            				</usd:checkOccup>
    			</div>
						<!--Left nav ends here--> 
			<!--Screen builder section starts here-->  
			<div class="cnt-sctn " id="print_rx_cnt_id">
																<div class="middle-content">
					<div class="page-heading page-heading-border">
						
																					<usd:checkOccup occupContent="generic-page-error-heading">
																													<usd:checkOccup occupContent="it-consulting" >
		    	
    	<h1 class="page-heading-h1">Application summary</h1>    	
				</usd:checkOccup>
																																<usd:checkOccup occupContent="consulting" >
		    	
    	<h1 class="page-heading-h1">Application summary</h1>    	
				</usd:checkOccup>
																																<usd:checkOccup occupContent="marketing-pr-consulting" >
		    	
    	<h1 class="page-heading-h1">Application summary</h1>    	
				</usd:checkOccup>
																																<usd:checkOccup occupContent="default-occupation-known" >
		    	
    	<h1 class="page-heading-h1">Application summary</h1>    	
				</usd:checkOccup>
																																		<usd:checkOccup occupContent="default-occupation-not-known" >
		    	
    	<h1 class="page-heading-h1">Application summary</h1>    	
				</usd:checkOccup>
										</usd:checkOccup>
													
						<div class="reference-number">
							<usd:checkInclude  includeContent="quoteRefID">
								<span>Reference # <usd:include sessionValue="quoteRefID"/></span>
							</usd:checkInclude>	
						</div>
						</div>
						<div class="clear"></div>
																								<div class="topcnt-container">
							
																					    <usd:checkOccup occupContent="generic-top-center-error">
																													<usd:checkOccup occupContent="it-consulting" >
		    	
    	<p class="body-copy-grey">Below is a reminder of the information you have provided. Please check the box at the bottom of the screen to confirm it is correct.</p>    	
				</usd:checkOccup>
																																<usd:checkOccup occupContent="consulting" >
		    	
    	<p class="body-copy-grey">Below is a reminder of the information you have provided. Please check the box at the bottom of the screen to confirm it is correct.</p>    	
				</usd:checkOccup>
																																<usd:checkOccup occupContent="marketing-pr-consulting" >
		    	
    	<p class="body-copy-grey">Below is a reminder of the information you have provided. Please check the box at the bottom of the screen to confirm it is correct.</p>    	
				</usd:checkOccup>
																																<usd:checkOccup occupContent="default-occupation-known" >
		    	
    	<p class="body-copy-grey">Below is a reminder of the information you have provided. Please check the box at the bottom of the screen to confirm it is correct.</p>    	
				</usd:checkOccup>
																																		<usd:checkOccup occupContent="default-occupation-not-known" >
		    	
    	<p class="body-copy-grey">Below is a reminder of the information you have provided. Please check the box at the bottom of the screen to confirm it is correct.</p>    	
				</usd:checkOccup>
											</usd:checkOccup>
								<div class="horz-line2"></div>
														
						</div>
												<usd:verifyToInclude>
						<usd:checkFormTag formContent="Start"></usd:checkFormTag>
						<usd:questionTag />	
																								<usd:actionTag />
						<usd:checkFormTag formContent="End"></usd:checkFormTag>	
						</usd:verifyToInclude>
						<usd:trackingTag />
												        	<div class="btmbtn-cntnr"> 
        		        		            		                            		        		        		<div class="clear-both"></div>
        	</div>
		</div>
				</div> 
				<!--Screen builder section ends here--> 
				<!--Right nav starts here-->   
				<div class="right-nav showcntct-num">
					<!--Checklist section starts here-->   
					<usd:checkOccup occupContent="it-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        										<div class="section-container right-nav-corner tpright-container-large print" >
											<div class="middle-content vertical-container">
				
																																<div class="contact-child-item-last">
						<div>
							<img id="contact-man" src="/shared-images/contact-woman.png" alt="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" title="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" border="0" height="122" width="167" /><p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p>						</div>
					</div>
															</div>
									</div>	
        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" >
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        										<div class="section-container right-nav-corner tpright-container-large print" >
											<div class="middle-content vertical-container">
				
																																<div class="contact-child-item-last">
						<div>
							<img id="contact-man" src="/shared-images/contact-woman.png" alt="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" title="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" border="0" height="122" width="167" /><p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p>						</div>
					</div>
															</div>
									</div>	
        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        										<div class="section-container right-nav-corner tpright-container-large print" >
											<div class="middle-content vertical-container">
				
																																<div class="contact-child-item-last">
						<div>
							<img id="contact-man" src="/shared-images/contact-woman.png" alt="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" title="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" border="0" height="122" width="167" /><p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p>						</div>
					</div>
															</div>
									</div>	
        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        										<div class="section-container right-nav-corner tpright-container-large print" >
											<div class="middle-content vertical-container">
				
																																<div class="contact-child-item-last">
						<div>
							<img id="contact-man" src="/shared-images/contact-woman.png" alt="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" title="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" border="0" height="122" width="167" /><p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p>						</div>
					</div>
															</div>
									</div>	
        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                 										    										    										    										            										            																						<div class="section-container right-nav-corner tpright-container-large print" >
											<div class="middle-content vertical-container">
				
																																				<div class="contact-child-item print">
						<div>
							<img id="contact-man" src="/shared-images/contact-woman.png" alt="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" title="Contact Hiscox the small business insurance specialists, and speak to one of our licensed agents today!" border="0" height="122" width="167" />						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="grey-call-to-action-large">888-202-2973</p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p>						</div>
					</div>
															</div>
									</div>	
			 			            										    										    										    										    																																        					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                 										    										    										    										            										            										            										    										    										    										    																																        					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                 									    		    		    		    			    														<div class="section-container right-nav-corner btmright-cntainr-large " >
											<div class="middle-content vertical-container">
				
		<div id="lpchat"></div>						</div>
									</div>	
    			        				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                 									    		    		    		    			    														<div class="section-container right-nav-corner btmright-cntainr-large " >
											<div class="middle-content vertical-container">
				
		<div id="lpchat"></div>						</div>
									</div>	
    			        				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                 									    		    		    		    			    														<div class="section-container right-nav-corner btmright-cntainr-large " >
											<div class="middle-content vertical-container">
				
		<div id="lpchat"></div>						</div>
									</div>	
    			        				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                 									    		    		    		    			    														<div class="section-container right-nav-corner btmright-cntainr-large " >
											<div class="middle-content vertical-container">
				
		<div id="lpchat"></div>						</div>
									</div>	
    			        				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                 																		<div class="section-container right-nav-corner btmright-cntainr-large " >
											<div class="middle-content vertical-container">
				
		<div id="lpchat"></div>						</div>
									</div>	
			    										    										    										            										            										            										    										    										    										    																																        					</usd:checkOccup>					
					<div class="verLogo-holder">
						<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
							<tr>
								<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hiscox.com&amp;size=L&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script><br />
								<a href="http://www.symantec.com/verisign/ssl-certificates" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>
							</tr>	
						</table>
					</div>
				</div>
				<!--Right nav ends here--> 
				<!-- Page container ends here-->
		<div class="boiler-plate">
			 <div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>    
				<div class="middle-content"> 
					<div class="middlecnt-container">
							
	
	
	
	
    							
																<h2 class="red-call-to-action-medium-h2"><b>About Hiscox small business insurance</b></h2><ul class="list-bulleted-body-copy-grey"><li><b>Small business specialists</b> - focus on insuring professional businesses with less than 10 employees.</li><li><b>Customized coverage</b> - we help customize coverage to your specific needs. No more, no less.</li><li><b>Great value</b> - buy direct from Hiscox and enjoy excellent service and real value for money.</li><li><b>Money back guarantee</b> - be confident in your purchase with our 14 day guarantee.</li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.</li></ul>
										
    	    		
		 		</div>
				</div>
			<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div>
		</div>
		<!-- Start of new class for twocol page - confirmation page) !-->
						<!-- End of new class for twocol page - confirmation page) !-->
    	<div class="footer" id="print_rx_footer_id">
    		
	
    							
																<div class="footer-links">
                      <a href="/accessibility/" title="" target="_blank" onclick="return popup(this)">
						Accessibility</a>
                    </div>
										
    				
																<div class="footer-links">
                      <a href="/terms-of-use/" title="" target="_blank" onclick="return popup(this)">
						Terms of use</a>
                    </div>
										
    				
																<div class="footer-links">
                      <a href="/privacy-policy/" title="" target="_blank" onclick="return popup(this)">
						Privacy policy</a>
                    </div>
										
    				
																<div class="footer-links">
                      <a href="/legal-notices/" title="" target="_blank" onclick="return popup(this)">
						Legal notices</a>
                    </div>
										
    	    		
		 		    		<div class="copy-right">
    			
	
	
	
    							
																<p class="body-copy-grey-small">© 2015 Hiscox Inc. All rights reserved.<br />Underwritten by Hiscox Insurance Company Inc.</p>
										
    	    		
		 		    		</div>
    	</div>
	</div>  
</div>
<script language="javascript" type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script language="javascript" type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script language="javascript" type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script language="javascript" type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
<usd:invalidateSessionTag/>

		<div style="width:100%;align:center">
						<script type="text/javascript" src="/resources/javascript/le-mtagconfig.js"></script> 
		</div>
	</body>
</html>