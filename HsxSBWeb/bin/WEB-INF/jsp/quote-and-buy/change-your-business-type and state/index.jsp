<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Change your business type and state | Hiscox USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print"/>
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body>
<div class="main-container">
<!-- Header starts here-->
	<div class="header">
		<div class="logo" id="print_rx_logo_id"><img src="/resources/images/logo.gif" alt="" title="Logo" /></div>
		<form method="post">
	      <div class="header-btncontainer">
								<usd:checkLogin loginContent="logout">
						<div class="header-btns"><div class="submit-button grey-button logout-icon"><span class='submit-button-end'><input class='submit-input' type='submit' title="Log Out" value='Log Out' name="action_Log Out_button"  /><span class="button-icon"></span></span></div></div>
					</usd:checkLogin>
					<usd:checkLogin loginContent="retrieve">
						<div class='header-btns float-right' ><div class="submit-button grey-button save-icon header-button-gap"><span class='submit-button-end'><input class="submit-input" type="submit" title="Retrieve A quote" name ="action_RetrieveAQuote_button" value="Retrieve A Quote"/><span class="button-icon"></span></span></div></div>
					</usd:checkLogin>
			           </div>
		   </form>
	</div>
	<!-- Header ends here-->
	<!-- Progress bar starts here-->
    

	<!-- Page container starts here-->
	<div class="page-container">	
		    	<div class="breadcrumb">
    		<usd:checkEligibility eligibleContent="stateVariant,primarybusiness">	
    		<div class="occupation-details">
				<span class = "float-right"><object>
    			<div class="occupation-info"><span>State:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="stateVariant" /></a></div><div class="breadcrumb-separator"></div>
    			<div class="occupation-info"><span>Primary type of business:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="primarybusiness" /></a></div>
				</object></span>
    		</div>
    		</usd:checkEligibility>
    	</div>							
    	    	 		
				    		    				    	    		    				    	    		   			    		    		    		    																							<div></div>
			<div></div>			
						<!--Left nav starts here-->
			    			<div class="left-nav">
    				<usd:checkOccup occupContent="it-consulting" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    				<usd:checkOccup occupContent="consulting" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    				<usd:checkOccup occupContent="marketing-pr-consulting" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-known" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-not-known" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    				<usd:checkOccup occupContent="it-consulting" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    				<usd:checkOccup occupContent="consulting" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    				<usd:checkOccup occupContent="marketing-pr-consulting" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-known" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-not-known" > 
    						                                  								    								    								    								            								            								            								    								    								    								    																										            				</usd:checkOccup>
    			</div>
						<!--Left nav ends here--> 
			<!--Screen builder section starts here-->  
			<div class="cnt-sctn " id="print_rx_cnt_id">
				<div class="top"><div class="right" ><div class="left"><div class="middle"></div></div></div></div>
				<div class="middle-content">
					<div class="page-heading page-heading-border">
																																																																													<usd:checkOccup occupContent="default-occupation-not-known" > 
											<h1 class="page-heading-h1">Change your business type and state</h1>										</usd:checkOccup>
																																																															<usd:checkOccup occupContent="default-occupation-known" > 
											<h1 class="page-heading-h1">Change your business type and state</h1>										</usd:checkOccup>
																																																															<usd:checkOccup occupContent="marketing-pr-consulting" > 
											<h1 class="page-heading-h1">Change your business type and state</h1>										</usd:checkOccup>														
																																																															<usd:checkOccup occupContent="consulting" > 
											<h1 class="page-heading-h1">Change your business type and state</h1>										</usd:checkOccup>													
																																																															<usd:checkOccup occupContent="it-consulting" > 
											<h1 class="page-heading-h1">Change your business type and state</h1>										</usd:checkOccup>
																																																																		<div class="reference-number">
							<usd:checkInclude  includeContent="quoteRefID">
								<span>Reference # <usd:include sessionValue="quoteRefID"/></span>
							</usd:checkInclude>	
						</div></div>
						<div class="clear"></div>
																								<div class="topcnt-container">
																																																																													<usd:checkOccup occupContent="default-occupation-not-known" > 
											<p class="body-copy-grey">You can change your state and business type using the drop down menus below. Once you have done this you can continue your quote.</p><p class="body-copy-grey">Please note that changing this information could result in us no longer being able to offer you some of our products.</p>										</usd:checkOccup>
																																																															<usd:checkOccup occupContent="default-occupation-known" > 
											<p class="body-copy-grey">You can change your state and business type using the drop down menus below. Once you have done this you can continue your quote.</p><p class="body-copy-grey">Please note that changing this information could result in us no longer being able to offer you some of our products.</p>										</usd:checkOccup>													
																																																															<usd:checkOccup occupContent="marketing-pr-consulting" >
											<p class="body-copy-grey">You can change your state and business type using the drop down menus below. Once you have done this you can continue your quote.</p><p class="body-copy-grey">Please note that changing this information could result in us no longer being able to offer you some of our products.</p>										</usd:checkOccup>
																																																															<usd:checkOccup occupContent="consulting" > 
											<p class="body-copy-grey">You can change your state and business type using the drop down menus below. Once you have done this you can continue your quote.</p><p class="body-copy-grey">Please note that changing this information could result in us no longer being able to offer you some of our products.</p>										</usd:checkOccup>
																																																															<usd:checkOccup occupContent="it-consulting" >
											<p class="body-copy-grey">You can change your state and business type using the drop down menus below. Once you have done this you can continue your quote.</p><p class="body-copy-grey">Please note that changing this information could result in us no longer being able to offer you some of our products.</p>										</usd:checkOccup>
																																																																		</div>
												<usd:verifyToInclude>
						<usd:checkFormTag formContent="Start"></usd:checkFormTag>
						<usd:questionTag />	
																								<usd:actionTag />
						<usd:trackingTag />
						<usd:checkFormTag formContent="End"></usd:checkFormTag>	
						</usd:verifyToInclude>
													<div class="btmbtn-cntnr"> 
				    		            						<div class="clear-both"></div>
	</div>
						</div>
						<div class="bottom clear-both" ><div class="right"><div class="left"><div class="middle" ></div></div></div></div> 
				</div> 
				<!--Screen builder section ends here--> 
				<!--Right nav starts here-->   
				<div class="right-nav">					
					<!--Checklist section starts here-->   
					<usd:checkOccup occupContent="it-consulting" > 
							                                  								    								    								    								            								            																		<div class="section-container right-nav-corner tpright-container-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="http://www.google.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			 			            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" >
							                                  								    								    								    								            								            																		<div class="section-container right-nav-corner tpright-container-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="http://www.google.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			 			            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                  								    								    								    								            								            																		<div class="section-container right-nav-corner tpright-container-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="http://www.google.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			 			            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                  								    								    								    								            								            																		<div class="section-container right-nav-corner tpright-container-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="http://www.google.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			 			            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                  								    								    								    								            								            																		<div class="section-container right-nav-corner tpright-container-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="http://www.google.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			 			            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                  								    															<div class="section-container right-nav-corner btmright-cntainr-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3>		<div class="horz-line"></div> 
		<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for technology businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific technology business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul></div></div></div></div>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                  								    															<div class="section-container right-nav-corner btmright-cntainr-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3>		<div class="horz-line"></div> 
		<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for consulting businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your consulting business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul></div>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                  															<div class="section-container right-nav-corner btmright-cntainr-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3><img id="red-line-style" src="/shared-images/red-line-style.png" alt="red line" title="red line" border="0" height="2" width="166" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for marketing consultants with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific marketing business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    								    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                  								    															<div class="section-container right-nav-corner btmright-cntainr-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3>		<div class="horz-line"></div> 
		<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for professional services businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul></div>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                  								    															<div class="section-container right-nav-corner btmright-cntainr-large" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3>		<div class="horz-line"></div> 
		<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for professional services businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul></div></div>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                  								    								    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                  								    								    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                  								    								    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                  								    								    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                  								    								    								    								            								            								            								    								    								    								    																										        					</usd:checkOccup>					
					<div class="verLogo-holder"><img src="/resources/images/secured-icon.gif" alt=""  title="Verisign" /></div>
					<!--Checklist section ends here--> 
				</div>
				<!--Right nav ends here--> 
				<!-- Page container ends here-->
		<div class="boiler-plate">
    		
	
	
	
	
    			</div>
		<!-- Start of new class for twocol page - confirmation page) !-->
						<!-- End of new class for twocol page - confirmation page) !-->
    	<div class="footer" id="print_rx_footer_id">
    		
	
	
	
    	    		<div class="copy-right">
    			
	
	
	
    							
																<p class="body-copy-grey">© Hiscox Inc</p>
										
    	    		
		 		    		</div>
    	</div>
	</div>  
</div>
<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
</body>
</html>
		