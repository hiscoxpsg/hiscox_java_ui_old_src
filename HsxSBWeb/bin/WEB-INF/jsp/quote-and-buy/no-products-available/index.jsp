<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>No coverage available | Hiscox USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print" />
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body>
<div class="main-container">
<!-- Header starts here-->
	<div class="header">
		<div class="logo" id="print_rx_logo_id">
	
	
	
    							
			                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		<form method="post">
	      <div class="header-btncontainer">
								<usd:checkLogin loginContent="logout">
						<div class="header-btns"><div class="submit-button grey-button logout-icon"><span class='submit-button-end'><input class='submit-input' type='submit' title="Log Out" value='Log Out' name="action_Log Out_button"  /><span class="button-icon"></span></span></div></div>
					</usd:checkLogin>
					<usd:checkLogin loginContent="retrieve">
						<div class='header-btns float-right' ><div class="submit-button grey-button save-icon header-button-gap"><span class='submit-button-end'><input class="submit-input" type="submit" title="Retrieve A quote" name ="action_RetrieveAQuote_button" value="Retrieve A Quote"/><span class="button-icon"></span></span></div></div>
					</usd:checkLogin>
			           </div>
		   </form>
	</div>
	<!-- Header ends here-->
	<!-- Progress bar starts here-->
    

    		    	<usd:checkPage pageContent ="about-you">
        <div class="progress-bar" >
        <ul>
        	<li class="first-red-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your business</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important information</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="your-business">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your business</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important information</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="your-quote">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your business</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important information</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="important-information">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important information</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="application-summary">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important information</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application summary</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="payment-details">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important information</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application summary</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment details</span></div></li>
        	<li class="last-red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="completed">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important information</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application summary</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment details</span></div></li>
        	<li class="last-red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="none">
        <div class="progress-bar" >
        <ul>
        	<li class="first-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your business</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important information</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
				<!-- Page container starts here-->
	<div class="page-container">	
		    	<div class="breadcrumb">
    		<usd:checkEligibility eligibleContent="stateVariant,primarybusiness">	
    		<div class="occupation-details">
				<span class = "float-right"><object>
    			<div class="occupation-info"><span>State:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="stateVariant" /></a></div>
    			<div class="occupation-info"><span>Type of business:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="primarybusiness" /></a></div>
				</object></span>
    		</div>
    		</usd:checkEligibility>
    	</div>							
    	    	 		
				    		    				    	    		    				    	    		   			    		    		    		    																							<div></div>
			<div></div>			
						<!--Left nav starts here-->
			    			<div class="left-nav">
    				<usd:checkOccup occupContent="it-consulting" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="consulting" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="marketing-pr-consulting" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-known" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-not-known" > 
    						                                 										    										    										    										            										            										            										    										    										    										    																																            				</usd:checkOccup>
    				<usd:checkOccup occupContent="it-consulting" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="consulting" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="marketing-pr-consulting" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-known" > 
    						                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-not-known" > 
    						                                 										    										    										    										            										            										            										    										    										    										    																																            				</usd:checkOccup>
    			</div>
						<!--Left nav ends here--> 
			<!--Screen builder section starts here-->  
			<div class="cnt-sctn " id="print_rx_cnt_id">
				<div class="top"><div class="right" ><div class="left"><div class="middle"></div></div></div></div>
				<div class="middle-content">
					<div class="page-heading page-heading-border">
																																																																									<usd:checkOccup occupContent="it-consulting" >
    	
    	<h1 class="page-heading-h1">No Hiscox products available in your state</h1>    	
		</usd:checkOccup>
																																																												<usd:checkOccup occupContent="consulting" >
    	
    	<h1 class="page-heading-h1">No Hiscox products available in your state</h1>    	
		</usd:checkOccup>
																																																												<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<h1 class="page-heading-h1">No Hiscox products available in your state</h1>    	
		</usd:checkOccup>
																																																												<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<h1 class="page-heading-h1">No Hiscox products available in your state</h1>    	
		</usd:checkOccup>
																																																												<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<h1 class="page-heading-h1">No Hiscox products available in your state</h1>    	
		</usd:checkOccup>
														<div class="reference-number">
							<usd:checkInclude  includeContent="quoteRefID">
								<span>Reference # <usd:include sessionValue="quoteRefID"/></span>
							</usd:checkInclude>	
						</div>
						</div>
						<div class="clear"></div>
																								<div class="topcnt-container">
																																																																									<usd:checkOccup occupContent="it-consulting" >
    	
    	<p class="body-copy-grey">We don’t currently operate in your state, but we can still help.</p><p class="body-copy-grey">We have partnered with Business Insurance Now.</p><p class="body-copy-grey">They are a web based e-agent that specialize in insurance for professional services businesses across the whole of the US. Please visit their website to learn more about how they can customize a range of different policies for your business.</p>    	
		</usd:checkOccup>
																																																												<usd:checkOccup occupContent="consulting" >
    	
    	<p class="body-copy-grey">We don’t currently operate in your state, but we can still help.</p><p class="body-copy-grey">We have partnered with Business Insurance Now.</p><p class="body-copy-grey">They are a web based e-agent that specialize in insurance for professional services businesses across the whole of the US. Please visit their website to learn more about how they can customize a range of different policies for your business.</p>    	
		</usd:checkOccup>
																																																												<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<p class="body-copy-grey">We don’t currently operate in your state, but we can still help.</p><p class="body-copy-grey">We have partnered with Business Insurance Now.</p><p class="body-copy-grey">They are a web based e-agent that specialize in insurance for professional services businesses across the whole of the US. Please visit their website to learn more about how they can customize a range of different policies for your business.</p>    	
		</usd:checkOccup>
																																																												<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<p class="body-copy-grey">We don’t currently operate in your state, but we can still help.</p><p class="body-copy-grey">We have partnered with Business Insurance Now.</p><p class="body-copy-grey">They are a web based e-agent that specialize in insurance for professional services businesses across the whole of the US. Please visit their website to learn more about how they can customize a range of different policies for your business.</p>    	
		</usd:checkOccup>
																																																												<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<p class="body-copy-grey">We don’t currently operate in your state, but we can still help.</p><p class="body-copy-grey">We have partnered with Business Insurance Now.</p><p class="body-copy-grey">They are a web based e-agent that specialize in insurance for professional services businesses across the whole of the US. Please visit their website to learn more about how they can customize a range of different policies for your business.</p>    	
		</usd:checkOccup>
									<div class="horz-line2"></div>
													</div>
												<usd:verifyToInclude>
						<usd:checkFormTag formContent="Start"></usd:checkFormTag>
						<usd:questionTag />	
																								<usd:actionTag />
						<usd:checkFormTag formContent="End"></usd:checkFormTag>	
						</usd:verifyToInclude>
						<usd:trackingTag />
												        	<div class="btmbtn-cntnr"> 
        		        		            		                            		        		        		<div class="clear-both"></div>
        	</div>
		</div>
	<div class="bottom clear-both" ><div class="right"><div class="left"><div class="middle" ></div></div></div></div> 
	</div> 
				<!--Screen builder section ends here--> 
				<!--Right nav starts here-->   
				<div class="right-nav showcntct-num">
					<!--Checklist section starts here-->   
					<usd:checkOccup occupContent="it-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="grey-call-to-action-large"><b>866-425-1465</b></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" >
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="grey-call-to-action-large"><b>866-425-1465</b></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="grey-call-to-action-large"><b>866-425-1465</b></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="grey-call-to-action-large"><b>866-425-1465</b></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                 										    										    										    										            										            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="grey-call-to-action-large"><b>866-425-1465</b></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            										    										    										    										    																																        					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                 										    										    										    										            										            										            										    										    										    										    																																        					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                 									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                 										    										    										    										            										            										            										    										    										    										    																																        					</usd:checkOccup>					
					<div class="verLogo-holder">
						<span>
							<a href="http://www.verisign.com/ssl-certificate/" target="new"> 
								<span class="verisign-img">
									<script type="text/javascript" language="JavaScript" src="https://seal.verisign.com/getseal? host_name=www.hiscox.co.uk&amp;size=S&amp;use_flash=NO&amp;use_transparent=NO&amp;lang=en"></script>
								</span>
							</a> 
							<br />
						</span>
					</div>
				</div>
				<!--Right nav ends here--> 
				<!-- Page container ends here-->
		<div class="boiler-plate">
    		
	
	
	
	
    			</div>
		<!-- Start of new class for twocol page - confirmation page) !-->
						<!-- End of new class for twocol page - confirmation page) !-->
    	<div class="footer" id="print_rx_footer_id">
    		
	
	
	
    	    		<div class="copy-right">
    			
	
	
	
    							
																<p class="body-copy-grey">© Hiscox Inc</p>
										
    	    		
		 		    		</div>
    	</div>
	</div>  
</div>
<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
<usd:invalidateSessionTag/>
</body>
</html>
		