package com.hiscox.sbweb.resolver;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.servlet.view.AbstractUrlBasedView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * This class resolves the view based on the url by extending default
 * UrlBasedViewResolver Spring class.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebUrlBasedViewResolver extends UrlBasedViewResolver
	implements IHsxSBWebConstants {
    public static final  HsxWebLogger logger = HsxWebLogger.getLogger();

    /**
     * This method resolves the view based on the url.
     * @return view
     */

    @Override
    protected AbstractUrlBasedView buildView(String viewName) throws Exception {

	AbstractUrlBasedView view = (AbstractUrlBasedView) BeanUtils
		.instantiateClass(getViewClass());
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	String homeURL = STR_EMPTY;

	// small-business-insurance (root folder) is common for all static /
	// dynamic pages
	// All the jsp's will be named as index.jsp

	String folderType = webBean.getFolderType();
	String folderName = webBean.getFolder();
	String runtimeAddedFolder = webBean.getRequestedPage();
	logger.info(SBWEB, INFO, "In URL Resolver : - > webBean.getFolderType() :"
		+ folderType);
	logger.info(SBWEB, INFO, "In URL Resolver : - > webBean.getFolder :"
		+ folderName);

	if (StringUtils.isBlank(folderType) && StringUtils.isBlank(folderName)) {
	    folderName = STR_SYSTEM_ERROR;
	    folderType = QUOTE_AND_BUY;
	    // webBean.setInvalidateSessionFlag(true);
	}

	if (null == folderType
		&& SMALL_BUSINESS_INSURANCE.equalsIgnoreCase(folderName)) {
	    logger.info(SBWEB, INFO, "Redirecting to index.jsp under jsp folder");
	    homeURL = getPrefix() + INDEX_JSP + getSuffix();
	} else if (STR_RUNTIMEADDED.equalsIgnoreCase(folderName)
		&& StringUtils.isNotEmpty(runtimeAddedFolder)) {
	    logger.info(SBWEB, INFO, "Redirecting to runtime index.jsp under "
		    + runtimeAddedFolder + "folder");
	    homeURL = getPrefix() + runtimeAddedFolder + STR_FORWARD_SLASH
		    + INDEX_JSP + getSuffix();
	} else if (null == folderType
		&& !SMALL_BUSINESS_INSURANCE.equalsIgnoreCase(folderName)
		&& !STR_SYSTEM_ERROR.equalsIgnoreCase(folderName)) {
	    logger.info(SBWEB, INFO, "Redirecting to Level1 folder's index.jsp under "
			    + folderName + " folder");
	    homeURL = getPrefix() + folderName + STR_FORWARD_SLASH + INDEX_JSP
		    + getSuffix();
	} else {
	    logger.info(SBWEB, INFO, "Redirecting to Level2 under jsp (Quote-and-buy) folder's index.jsp under "
			    + folderName + " folder");
	    homeURL = getPrefix() + folderName + STR_FORWARD_SLASH + INDEX_JSP
		    + getSuffix();
	}
	view.setUrl(homeURL);
	logger.info(SBWEB, INFO, "In URL Resolver : - > Redirection URL :" + homeURL);
	return view;

    }

}

