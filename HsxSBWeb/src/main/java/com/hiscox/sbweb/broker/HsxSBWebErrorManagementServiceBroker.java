package com.hiscox.sbweb.broker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.ws.soap.SoapVersion;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.service.HsxWebServiceClient;
import com.hiscox.core.service.HsxWebServiceClientImpl;
import com.hiscox.core.vo.HsxCoreError;
import com.hiscox.core.vo.HsxCoreRequest;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;

/**
 * This calss constructs the request for error management service and concerts
 * the response into error object after logging the same into error management
 * service.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:20 AM
 */
public class HsxSBWebErrorManagementServiceBroker implements
	HsxSBWebBaseServiceBroker, IHsxSBWebConstants, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6687058398711674786L;

    private static volatile HsxWebServiceClient errorMngtWebServiceClient = null;

    private Properties appConfigProperties = null;

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setAppConfigProperties(Properties appConfigProperties) {
	this.appConfigProperties = appConfigProperties;
    }

    private synchronized HsxWebServiceClient getServiceClientInstance() {
	if (errorMngtWebServiceClient == null) {
	    errorMngtWebServiceClient = new HsxWebServiceClientImpl(
		    SoapVersion.SOAP_12);
	}
	return errorMngtWebServiceClient;
    }

    /*
     * constructs the request, calls the service
     */
    public HsxCoreError invokeErrorManagementService(HsxCoreRequest request)
	    throws HsxCoreException, HsxCoreRuntimeException {
	HsxCoreError error = null;
	if (null != request) {

	    constructRequestXML(request);
	    setDefaultURI(request);
	    if (request.getAttribute("RequestXml") != null) {
		String requestXML = (String) request.getAttribute("RequestXml");
		logger.error(SBWEB, ERROR,
			"Error Managment Request Xml Recieved :\n"
				+ StringEscapeUtils.unescapeXml(
					requestXML.toString()).replaceAll("><",
					">\n<"));
	    }
	    setServiceBinder(request);

	    StringBuilder responseXML = null;

	    responseXML = getServiceClientInstance().callSendRecieve(request);
	    if (responseXML != null) {
		logger.error(SBWEB, ERROR,
			"Error Managment Response Xml Recieved :\n"
				+ StringEscapeUtils.unescapeXml(
					responseXML.toString()).replaceAll(
					"><", ">\n<"));

	    }
	}

	return error;

    }

    public void setServiceBinder(HsxCoreRequest request) {
	request.setAttribute(SRVICE_BINDER,
		STR_ERROR_MANAGEMENT_SERVICE_BINDER_NAME);
    }

    public void setDefaultURI(HsxCoreRequest request) {
	// get webservice URL from property file
	String webserviceURL = appConfigProperties
		.getProperty(STR_ERROR_MANAGEMENT_SERVICE_URL);
	// "http://hxb31571.hiscox.nonprod:9000/";
	request.setAttribute(DEFAULT_URI, webserviceURL
		+ STR_ERROR_MANAGEMENT_SERVICE_DEFAULT_URI);
    }

    @SuppressWarnings("unchecked")
    private void constructRequestXML(HsxCoreRequest request) {
	StringBuilder requestXML = new StringBuilder(
		STR_ERROR_MANAGEMENT_REQUEST_SMALL_CREATE_ERROR_START);
	requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_CREATE_ERROR_START);
	requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_AREA_START);
	requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_MESSAGE_ID_START);
	if (request != null) {
	    requestXML.append(request.getAttribute(MESSAGE_ID));
	    // requestXML.append(EM_MESSAGE_ID);
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_MESSAGE_ID_END);
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_CREATION_DATE_TIME_START);
	    requestXML.append(getDateTimeForErrorManagement());
	    // requestXML.append(".718Z");
	    // requestXML.append("2010-09-24T12:15:40.361Z");
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_CREATION_DATE_TIME_END);
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_AREA_END);
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_DATA_AREA_START);
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_SERVER_NAME_START);
	    requestXML.append(request.getAttribute(APPLICATION_SERVER_NAME));
	    // requestXML.append(EM_APPLICATION_SERVER_NAME);
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_SERVER_NAME_END);
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_NAME_START);
	    requestXML.append(request.getAttribute(APPLICATION_NAME));
	    // requestXML.append(EM_APPLICATION_NAME);
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_NAME_END);
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_COMPONENT_NAME_START);
	    // requestXML.append(EM_COMPONENT_NAME);
	    requestXML.append(request.getAttribute(COMPONENT_NAME));
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_COMPONENT_NAME_END);
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_MAIN_SERVICE_START);
	    // requestXML.append(EM_MAIN_SERVICE);
	    requestXML.append(request.getAttribute(MAIN_SERVICE));
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_MAIN_SERVICE_END);
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_ERROR_CODE_START);
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_ERROR_TYPE_START);
	    // requestXML.append(EM_ERROR_TYPE);
	    requestXML.append(request.getAttribute(ERROR_TYPE));
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_ERROR_TYPE_END);
	    // requestXML.append(EM_ERROR_CODE);
	    requestXML.append(request.getAttribute(ERROR_CODE));
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_ERROR_CODE_END);
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_ERROR_MESSAGE_START);

	    if (request != null && request.getAttribute(ARRAY_ERROR) != null) {
		List<String> errors = (ArrayList<String>) request
			.getAttribute(ARRAY_ERROR);

		for (String errorElement : errors) {
		    requestXML
			    .append(STR_ERROR_MANAGEMENT_REQUEST_ARRAY_OF_STRING_ITEM_START);
		    // requestXML.append(EM_ERROR1);
		    requestXML.append(errorElement);
		    requestXML
			    .append(STR_ERROR_MANAGEMENT_REQUEST_ARRAY_OF_STRING_ITEM_END);
		}
	    }

	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_ERROR_MESSAGE_END);
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_ADDITIONAL_INFORMATION_START);
	    // requestXML.append(EM_ADDITIONAL_INFORMATION);
	    requestXML.append(request.getAttribute(ADDITIONAL_INFORMATION));
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_ADDITIONAL_INFORMATION_END);
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_DATA_AREA_END);
	    requestXML.append(STR_ERROR_MANAGEMENT_REQUEST_CREATE_ERROR_END);
	    requestXML
		    .append(STR_ERROR_MANAGEMENT_REQUEST_SMALL_CREATE_ERROR_END);

	    request.setAttribute(REQUEST_XML, requestXML.toString());
	}
    }

    private static StringBuilder getDateTimeForErrorManagement() {
	StringBuilder dateTime = new StringBuilder();
	Calendar calendar = Calendar.getInstance();
	dateTime.append(calendar.get(Calendar.YEAR));
	dateTime.append("-");
	dateTime.append(formatDateTime(calendar.get(Calendar.MONTH) + 1));
	dateTime.append("-");
	dateTime.append(formatDateTime(calendar.get(Calendar.DATE)));
	dateTime.append("T");
	dateTime.append(formatDateTime(calendar.get(Calendar.HOUR_OF_DAY)));
	dateTime.append(":");
	dateTime.append(formatDateTime(calendar.get(Calendar.MINUTE)));
	dateTime.append(":");
	dateTime.append(formatDateTime(calendar.get(Calendar.SECOND)));
	dateTime.append(".");
	dateTime.append(formatmilliSecond(calendar.get(Calendar.MILLISECOND)));
	dateTime.append("Z");
	return dateTime;
    }
/**
 * This method is to format the milli seconds.
 * @param i
 * @return value
 */
    private static String formatmilliSecond(int i) {
	String value = Integer.valueOf(i).toString();
	if (value != null) {
	    if (value.trim().length() == 1) {
		value = "00" + value;
	    } else if (value.trim().length() == 2) {
		value = "0" + value;
	    }
	}

	return value;

    }

    /**
     * This method is to format the Datetime.
     * @param a
     * @return value
     */
    private static String formatDateTime(int a) {
	String value = Integer.valueOf(a).toString();
	if (value != null && value.trim().length() == 1) {
	    value = "0" + value;
	}
	return value;
    }
}

