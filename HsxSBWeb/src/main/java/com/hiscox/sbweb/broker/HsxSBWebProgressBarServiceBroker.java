package com.hiscox.sbweb.broker;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.util.HsxSBResponseProcessor;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.util.HsxSBWebNavigationManager;
import com.hiscox.sbweb.util.HsxSBWebPageBeanUtil;
import com.hiscox.sbweb.util.HsxSBWebQuoteAndBuyControllerUtil;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * This class is to handle Get Page Orchestration service call.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:22 AM
 */
public class HsxSBWebProgressBarServiceBroker extends HsxSBGetPageServiceBroker
	implements IHsxSBWebConstants, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 6699844503529246506L;
    private Properties screenNameURLProperties = null;

    public void setScreenNameURLProperties(Properties screenNameURLProperties) {
	this.screenNameURLProperties = screenNameURLProperties;
    }

    public HsxSBWebProgressBarServiceBroker() {

    }

    /**
     * This method invokes the GetPage orchestration service and also sets the
     * viewname.
     *
     * @return viewName
     * @throws HsxCoreException
     */
    public String invokeGetPage() throws HsxCoreException {
	String viewName = STR_SYSTEM_ERROR;
	String folderType = null;
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	String requestType = webBean.getRequestType();
	boolean throbberIndicator = webBean.isThrobberIndicator();
	if (!throbberIndicator) {
	    // redirect to home page if throbber page is given directly
	    viewName = STR_EMPTY;
	    webBean.setThrobberIndicator(false);
	    webBean.setViewName(viewName);
	//}
	//added
	 /*if (STR_PAYMENT_DETAILS.equalsIgnoreCase(webBean.getNextScreenId())){
	     viewName = PAYMENT_ERROR;

		    viewName = invokeGetPageService(webBean, requestType);
		    logger.info(SBWEB, INFO,
			    "viewName :In HsxSBWebProgressBarServiceBroker :"
				    + viewName);
		    if (null != webBean.getSessionMap())
		    {
			HsxSBWebQuoteAndBuyControllerUtil.getSessionValues(webBean
				.getSessionMap(), webBean);
		    }
		    Map<String, HsxSBWebPageRuleBean> navigationRuleMap = null;
		    navigationRuleMap = HsxSBWebNavigationManager.getInstance()
			    .getNavigationMap();
		    HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap.get(viewName
			    .trim());
		    if (pageRuleBean != null)
		    {
			folderType = pageRuleBean.getFolderName();
		    }
		    if (StringUtils.isBlank(folderType))
		    {
			folderType = STR_EMPTY;
		    }
		    else
		    {
			folderType = folderType + STR_FORWARD_SLASH;
		    }
		    logger.info(SBWEB, INFO,
			    "folderName in HsxSBWebProgressBarServiceBroker :"
				    + folderType);
		    logger
			    .info(SBWEB, INFO,
				    "viewName in HsxSBWebProgressBarServiceBroker :"
					    + viewName);
		    webBean.setFolderType(folderType);

		    webBean.setViewName(viewName);

		    String pagesVisited = webBean.getPagesVisited();
		    if (STR_POST.equalsIgnoreCase(requestType) && pagesVisited != null)
		    {
			if (!pagesVisited.contains(viewName))
			{
			    pagesVisited = pagesVisited.concat(STR_COMA).concat(
				    viewName);
			}
			else
			{
			    pagesVisited = HsxSBWebPageBeanUtil.updatePageHistory(
				    viewName, pagesVisited);
			}

			webBean.setPagesVisited(pagesVisited);
		    }

		    webBean.setRequestType(STR_POST);

	 }*/
    }else {
	    viewName = invokeGetPageService(webBean, requestType);
	    logger.info(SBWEB, INFO,
		    "viewName :In HsxSBWebProgressBarServiceBroker :"
			    + viewName);
	    if (null != webBean.getSessionMap()) {
		HsxSBWebQuoteAndBuyControllerUtil.getSessionValues(webBean
			.getSessionMap(), webBean);
	    }
	    Map<String, HsxSBWebPageRuleBean> navigationRuleMap = null;
	    navigationRuleMap = HsxSBWebNavigationManager.getInstance()
		    .getNavigationMap();
	    HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap.get(viewName
		    .trim());
	    if (pageRuleBean != null) {
		folderType = pageRuleBean.getFolderName();
	    }
	    if (StringUtils.isBlank(folderType)) {
		folderType = STR_EMPTY;
	    } else {
		folderType = folderType + STR_FORWARD_SLASH;
	    }
	    logger.info(SBWEB, INFO,
		    "folderName in HsxSBWebProgressBarServiceBroker :"
			    + folderType);
	    logger
		    .info(SBWEB, INFO,
			    "viewName in HsxSBWebProgressBarServiceBroker :"
				    + viewName);
	    webBean.setFolderType(folderType);

	    webBean.setViewName(viewName);

	    String pagesVisited = webBean.getPagesVisited();
	    if (STR_POST.equalsIgnoreCase(requestType) && pagesVisited != null) {
		if (!pagesVisited.contains(viewName)) {
		    pagesVisited = pagesVisited.concat(STR_COMA).concat(
			    viewName);
		}
		/*else
		{
		    pagesVisited = HsxSBWebPageBeanUtil.updatePageHistory(
			    viewName, pagesVisited);
		}*/

		webBean.setPagesVisited(pagesVisited);
	    }

	    webBean.setRequestType(STR_POST);
	}

	return viewName;
    }

    /**
     * This method invokes the GetPage orchestration service.
     *
     * @param webBean
     * @return viewName
     */
    public String invokeGetPageService(HsxSBWebBean webBean, String requestType) {
	String viewName = STR_SYSTEM_ERROR;
	String screenXML = null;


	HsxSBGetPageServiceBroker serviceBroker = (HsxSBGetPageServiceBroker) HsxSBWebResourceManager
		.getResource("webServiceBroker");

	try {
	    logger.info(SBWEB, INFO,
		    "PAGE VISITED HISTORY IN THE POGRESS BAR SERVICE BOKER--"
			    + webBean.getPagesVisited());
	    logger.info(SBWEB, INFO,
		    "Request type in the progress bar service broker --"
			    + requestType);
	    if (STR_GET.equalsIgnoreCase(requestType)) {
		Map<String, HsxSBWebPageRuleBean> navigationRuleMap = HsxSBWebNavigationManager
			.getInstance().getNavigationMap();
		String previousscreen = webBean.getPreviousScreen();
		HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap
			.get(previousscreen);
		if (pageRuleBean != null) {

		    String throwberPage = pageRuleBean.getThrobberPage();
		    logger.info(SBWEB, INFO, "Previous Screen --"
			    + previousscreen);
		    logger.info(SBWEB, INFO, "Throber Page Flag --"
			    + throwberPage);
		    if (STR_YES.equalsIgnoreCase(throwberPage)) {
			Map<String, HsxSBWidget> widgetMap = webBean
				.getWidgetMap();
			String screenXmlContent = webBean.getQuestionsContent();
			Document document = HsxSBWebPageBeanUtil
				.getDocumentFromString(screenXmlContent);
			String requestXml = HsxSBResponseProcessor
				.generateScreenQuestionXML(document, widgetMap,
					STR_BACK);
			HsxSBWebQuoteAndBuyControllerUtil
				.resetWebBeanXmlContent(webBean);
			webBean.setRequestXml(requestXml);
		    }
		}

	    }
	    screenXML = serviceBroker.invokeGetPageService(
		    STR_PROGRESS_BAR_PAGE_REQUEST_TYPE, webBean.getRequestXml(), null,
		    webBean.getSessionIdentifierValue());
	    webBean.setResponseXml(screenXML);
	    webBean.setQuestionsContent(screenXML);
	    webBean.setActionsContent(screenXML);
	    webBean.setTrackingContent(screenXML);
	    String requestedPage = webBean.getNextScreenId();
	    if (screenNameURLProperties.containsKey(requestedPage)) {
		viewName = screenNameURLProperties.getProperty(requestedPage);
	    }
	    logger.info(SBWEB, INFO,
		    "viewName :In HsxSBWebProgressBarServiceBroker : invokeGetPageService"
			    + viewName);
	    webBean.setViewNameForProgressBar(viewName);

	} catch (HsxCoreRuntimeException e) {
	    viewName = STR_SYSTEM_ERROR;
	    webBean.setViewNameForProgressBar(viewName);
	    logger.error("HsxCoreRuntimeException", e);
	    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
		    webBean, "HsxCoreRuntimeException");
	    HsxSBWebQuoteAndBuyControllerUtil.resetSessionParamters(webBean);

	} catch (HsxCoreException e) {

	    // folderName = QUOTE_AND_BUY;
	    viewName = STR_SYSTEM_ERROR;
	    webBean.setViewNameForProgressBar(viewName);
	   logger.error("HsxCoreException", e);
	    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
		    webBean, e.getErrorDescription());
	    HsxSBWebQuoteAndBuyControllerUtil.resetSessionParamters(webBean);
	} catch (HsxSBUIBuilderRuntimeException e) {
	    // log it into log file
	    // return to error page
	    viewName = STR_SYSTEM_ERROR;
	    webBean.setViewNameForProgressBar(viewName);
	   logger.error("HsxSBUIBuilderRuntimeException", e);
	    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
		    webBean, "HsxSBUIBuilderRuntimeException");
	    HsxSBWebQuoteAndBuyControllerUtil.resetSessionParamters(webBean);

	} catch (Exception e) {
	    // folderName = QUOTE_AND_BUY;
	    viewName = STR_SYSTEM_ERROR;
	    webBean.setViewNameForProgressBar(viewName);
	   logger.error("Exception",e);
	    HsxSBWebPageBeanUtil.logException(webBean, e);
	    HsxSBWebQuoteAndBuyControllerUtil.resetSessionParamters(webBean);
	}

	return viewName;
    }
}

