package com.hiscox.sbweb.broker;

import com.hiscox.core.vo.HsxCoreRequest;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:19 AM
 */
public interface HsxSBWebBaseServiceBroker {

    void setServiceBinder(HsxCoreRequest request);

    void setDefaultURI(HsxCoreRequest request);

}

