package com.hiscox.sbweb.broker;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringEscapeUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.service.HsxWebServiceClient;
import com.hiscox.core.vo.HsxCoreRequest;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebPageBeanUtil;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;
import com.hiscox.sbweb.util.HsxSBWebTxtMgrConverter;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:22 AM
 */
public class HsxSBWebTextManagerServiceBroker implements
	HsxSBWebBaseServiceBroker, IHsxSBWebConstants, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7858378469964044691L;

    private Properties appConfigProperties = null;

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setAppConfigProperties(Properties appConfigProperties) {
	this.appConfigProperties = appConfigProperties;
    }

    /**
     *
     * @param textCodeTextItemsMap
     * @return
     */
    public void loadTextItems(Map<String, HsxSBTextManagerVO> textItemsMap) {
	HsxSBWebTxtMgrConverter txtMgrConverter = new HsxSBWebTxtMgrConverter();

	HsxWebServiceClient textMngrServiceClient = (HsxWebServiceClient) HsxSBWebResourceManager
		.getResource(TEXT_MNGR_SERVICE_CLIENT);
	HsxCoreRequest request = new HsxCoreRequest();
	setDefaultURI(request);
	setServiceBinder(request);
	String txtRequestXml = createTxtMgrRequestXml();
	request.setAttribute(REQUEST_XML, txtRequestXml);

	String responseXml = null;
	try {
	    // long a = Calendar.getInstance().getTimeInMillis();
	    responseXml = textMngrServiceClient.callSendRecieve(request).toString();
		//request object made eligible for garbage collection
	    request = null;
	    // long b = Calendar.getInstance().getTimeInMillis();
	    // logger.info(SBWEB, INFO, "Time Taken for the CTM Service Call :"
	    // + (b - a) * 0.001 + " Seconds");
	} catch (HsxCoreRuntimeException e) {
	  logger.error("HsxCoreRuntimeException", e);
	} catch (HsxCoreException e) {
	   logger.error("HsxCoreException", e);
	}
	if (responseXml != null) {
	    // long a = Calendar.getInstance().getTimeInMillis();
	    responseXml = responseXml.replace(XML_HEADER_ENC_STRING, STR_EMPTY);
	    responseXml = StringEscapeUtils.unescapeXml(responseXml);
	    responseXml = HsxSBWebPageBeanUtil
		    .encodeResponseXmlData(responseXml);
	    txtMgrConverter.convertXMLToTextItems(textItemsMap, responseXml);
	    // long b = Calendar.getInstance().getTimeInMillis();
	    // logger.info(SBWEB, INFO,
	    // "Time Taken to load the CTM Text Items Map :" + (b - a)
	    // * 0.001 + " Seconds");
	}

	// Testing ..Currently responseXml is loaded from local drive
	// String txtResponseXml = HsxSBWebPageBeanUtil.getTextXml();

    }

    public void setServiceBinder(HsxCoreRequest request) {
	request.setAttribute(SRVICE_BINDER,
		STR_TEXT_MANAGER_SERVICE_BINDER_NAME);

    }

    public void setDefaultURI(HsxCoreRequest request) {
	String webserviceURL = appConfigProperties
		.getProperty(STR_RETRIEVE_DETAILED_TEXT_SERVICE_URL);

	request.setAttribute(DEFAULT_URI, webserviceURL
		+ STR_TEXT_MANAGER_SERVICE_DEFAULT_URI);
    }

    public static String createTxtMgrRequestXml() {
	Document document = null;
	Element rootElement = null;

	DocumentBuilderFactory bdFactory = DocumentBuilderFactory.newInstance();
	try {
	    DocumentBuilder parser = bdFactory.newDocumentBuilder();
	    document = parser.newDocument();
	    String root = STR_RETRIEVETEXTINPUT;
	    rootElement = document.createElement(root);
	    document.appendChild(rootElement);
	} catch (ParserConfigurationException e) {
	    logger.error(SBWEB, ERROR, STR_ERROR_MESSAGE + e.getMessage());
	}

	Element inputElement = document.createElement(STR_INPUT);
	rootElement.appendChild(inputElement);

	final int txtInpEleLen = INPUT_ELEMENTS_ARRAY.length;
	for (int i = 0; i < txtInpEleLen; i++) {
	    Element inputChildElement = document
		    .createElement(INPUT_ELEMENTS_ARRAY[i]);
	    inputElement.appendChild(inputChildElement);
	}
	NodeList inputNodeList = document.getElementsByTagName(STR_INPUT);
	Element inputChildElement = (Element) inputNodeList.item(0);
	Element schemeElement = (Element) inputChildElement
		.getElementsByTagName(STR_SCHEME).item(0);
	schemeElement.setTextContent(STR_SCHEME_CONTENT);
	Element channelElement = (Element) inputChildElement
		.getElementsByTagName(STR_CHANNEL).item(0);
	channelElement.setTextContent(STR_CHANNEL_CONTENT);

	// HsxSBWebPageBeanUtil.printXmlData(document);
	String txMgrRequestXml = HsxSBWebPageBeanUtil
		.getStringFromDocument(document);
	txMgrRequestXml = txMgrRequestXml.replaceAll(XML_HEADER_STRING,
		STR_EMPTY);
	txMgrRequestXml = StringEscapeUtils.escapeXml(txMgrRequestXml);
	txMgrRequestXml = STR_LESSTHAN_SYMBOL + STR_RETRIEVE_INPUT
		+ STR_GREATERTHAN_SYMBOL + txMgrRequestXml
		+ STR_LESSTHAN_SYMBOL + STR_FORWARD_SLASH + STR_RETRIEVE_INPUT
		+ STR_GREATERTHAN_SYMBOL;
	logger.info(SBWEB, INFO, "Text Manager Request Xml Logged: "
		+ StringEscapeUtils.unescapeXml(txMgrRequestXml));
	return txMgrRequestXml;
    }

}

