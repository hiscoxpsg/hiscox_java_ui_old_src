package com.hiscox.sbweb.broker;

import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.service.HsxWebServiceClient;
import com.hiscox.core.vo.HsxCoreRequest;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebPageBeanUtil;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;
import com.hiscox.sbweb.util.HsxSBWebSessionManager;

/**
 * This class is to handle Get Page Orchestration service call.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:22 AM
 */

public class HsxSBGetPageServiceBroker implements HsxSBWebBaseServiceBroker,
	IHsxSBWebConstants, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7048429725543537604L;

    public HsxSBGetPageServiceBroker() {

    }

    public  static final HsxWebLogger logger = HsxWebLogger.getLogger();
    private Properties appConfigProperties = null;
    private Properties schemeIdListProperties = null;

    public void setSchemeIdListProperties(Properties schemeIdListProperties) {
	this.schemeIdListProperties = schemeIdListProperties;
    }

    public void setAppConfigProperties(Properties appConfigProperties) {
	this.appConfigProperties = appConfigProperties;
    }

    /**
     * This method invokes the GetPage orchestration service.
     *
     * @return String
     * @throws TransformerException
     * @throws TransformerFactoryConfigurationError
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws HsxCoreRuntimeException
     * @throws HsxCoreException
     */
    public String invokeGetPageService(String requestType, String requestXml,
	    String pageName, String sessionIdentifierValue)
	    throws TransformerFactoryConfigurationError, TransformerException,
	    ParserConfigurationException, SAXException, IOException,
	    HsxCoreException, HsxCoreRuntimeException {
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	requestXml = constructRequestXML(requestType, requestXml, pageName,
		sessionIdentifierValue, webBean);
	HsxWebServiceClient gettPageServiceClient = (HsxWebServiceClient) HsxSBWebResourceManager
		.getResource(GET_PAGE_SERVICE_CLIENT);
	HsxCoreRequest request = new HsxCoreRequest();
	setDefaultURI(request);
	setServiceBinder(request);
	request.setAttribute(REQUEST_XML, requestXml);
	return processResponseXML(requestType, webBean, gettPageServiceClient,
		request);
    }

    /**
     * @param requestType
     * @param webBean
     * @param gettPageServiceClient
     * @param request
     * @return
     * @throws HsxCoreException
     * @throws HsxCoreRuntimeException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @return responseXml
     */
    private String processResponseXML(String requestType, HsxSBWebBean webBean,
	    HsxWebServiceClient gettPageServiceClient, HsxCoreRequest request)
	    throws HsxCoreException, HsxCoreRuntimeException,
	    ParserConfigurationException, SAXException, IOException {
	String responseXml = null;
	StringBuilder responseBuilder = gettPageServiceClient
		.callSendRecieve(request);
	if (responseBuilder == null
		|| StringUtils.isBlank(responseBuilder.toString())) {
	    logger.error(SBWEB, ERROR, "Blank response recieved From WM");
	    throw new HsxCoreException("0050015",
		    "Blank response recieved From WM", "123456",
		    HsxSBWebPageBeanUtil.getCurrentTimeStamp(),
		    "Blank response recieved From WM");
	}
	responseXml = responseBuilder.toString();
	// long c = Calendar.getInstance().getTimeInMillis();
	responseXml = StringEscapeUtils.unescapeXml(responseXml);
	logger.info(SBWEB, INFO, "Quote Reference Id :"
		+ webBean.getErrMngtQuoteRefId() + "Session Id :"
		+ webBean.getSessionIdentifierValue());
	logger.info(SBWEB, INFO, "Response Xml Logged :" + responseXml);
	responseXml = responseXml.replace(XML_HEADER, STR_EMPTY);
	String requestXmlLogged = null;
	if (request.getAttribute(REQUEST_XML) != null) {
	    requestXmlLogged = (String) request.getAttribute(REQUEST_XML);
	}
	// requestXml passed to the below method to log the request xml in case
	// of any service errors
	String status = checkForGetPageServiceExceptions(requestXmlLogged,
		responseXml);
	// set the requestXmlLogged variable to null so that it gets garbage
	// collected.
	requestXmlLogged = null;
	Document showPageDocument = HsxSBWebPageBeanUtil
		.getDocumentFromString(responseXml.toString());
	/*showPageDocument = decryptCardPaymentData(showPageDocument,
		hsxSBWebEncryption, status);*/
	webBean.setServiceCallStatus(status);
	if (STR_BUSINESS_NAK.equalsIgnoreCase(status)) {
	    //Resetting DataCash Reference value on Card transaction failed at wM
	    webBean.setDataCashReference(null);
	    webBean.setNonJSDataCashIndicator(false);
	}
	responseXml = HsxSBWebPageBeanUtil
		.getStringFromDocument(showPageDocument);
	retrieveSessionIdentifier(webBean, showPageDocument);
	retrieveNextScreenId(webBean, showPageDocument);
	if (!HsxSBWebPageBeanUtil.isMapEmpty(webBean.getSavedQuotesMap())) {
	    webBean.getSavedQuotesMap().clear();
	}

	if (STR_SAVED_QUOTE.equalsIgnoreCase(webBean.getNextScreenId())) {
	    logger.info(SBWEB, INFO, "-- Retrieving the Saved Quotes Map --");
	    retrieveSavedQuotesmap(webBean, showPageDocument);
	}
	retrieveScreenConfigElements(webBean, showPageDocument);
	logger.info(SBWEB, INFO, "Request Type Logged For Action Map Load :"
		+ requestType);
	if (!STR_POP_UP_GET_REQUEST_TYPE.equalsIgnoreCase(requestType)) {
	    retrieveActionMap(showPageDocument, webBean);
	    retrieveScreenActionMap(showPageDocument, webBean);
	}
	if (STR_PAYMENT_DETAILS.equalsIgnoreCase(webBean.getNextScreenId())) {
	    logger.info(SBWEB, INFO, "-- Updating the DataCashValues in Session Map --");
	    updateConfigValuesForDataCash(showPageDocument, webBean);
	}
	/**Added for US25094-Add Accounting PL to DPD -start**/
	if (STR_ABOUT_YOU.equalsIgnoreCase(webBean.getNextScreenId())) {
	    logger.info(SBWEB, INFO, "-- Updating product values in Session Map for PL Products & Bookkeeping/tax prepation combination--");
	    updateConfigValuesForPL(showPageDocument, webBean);
	}/**Added for US25094-Add Accounting PL to DPD -end**/
	
	/**Added for DEFECT 5865&5887-Variable Text not coming for retrieve quote through mail**/
	if ((STR_COVERAGE_OPTIONS.equalsIgnoreCase(webBean.getNextScreenId())|| STR_ABOUT_YOU.equalsIgnoreCase(webBean.getNextScreenId()))
			&& STR_RETRIEVE_A_QUOTE_BUTTON_NAME.equals(webBean.getRequestedAction())) {
	    logger.info(SBWEB, INFO, "-- Updating occupation variant values in Session Map for retrieve quote through mail--");
	    updateSessionValuesRetrieveQuote(showPageDocument, webBean);
	}/**Added for DEFECT 5865&5887-Variable Text not coming for retrieve quote through mail-end**/
	
	// long d = Calendar.getInstance().getTimeInMillis();
	// logger.info(SBWEB, INFO,
	// "Time Taken for UI GetPage Service Response Reading :"
	// + (d - c) * 0.001 + " Seconds");
	return responseXml;
    }

    /**
     * @param requestType
     * @param requestXml
     * @param pageName
     * @param sessionIdentifierValue
     * @param webBean
     * @return requestXml
     * @throws ParserConfigurationException
     * @throws TransformerFactoryConfigurationError
     * @throws TransformerException
     * @throws HsxCoreException
     * @throws UnknownHostException
     */
    private String constructRequestXML(String requestType, String requestXml,
	    String pageName, String sessionIdentifierValue, HsxSBWebBean webBean)
	    throws ParserConfigurationException,
	    TransformerFactoryConfigurationError, TransformerException,
	    HsxCoreException, UnknownHostException {
	Document document = null;
	logger.info(SBWEB, INFO, "requestType in Service Broker :"
		+ requestType);
	// long a = Calendar.getInstance().getTimeInMillis();
	if (STR_LANDING_PAGE_TYPE.equalsIgnoreCase(requestType)) {
	    requestXml = createLandingPageReqXml(sessionIdentifierValue,
		    requestXml, pageName, webBean);
	} else if (STR_GET_REQUEST_TYPE.equalsIgnoreCase(requestType)
		|| STR_POP_UP_GET_REQUEST_TYPE.equalsIgnoreCase(requestType)) {
	    String currentScreenResp = webBean.getResponseXml();
	    // Attach the current screen response to getRequest
	    if (StringUtils.isNotBlank(currentScreenResp)) {
		document = HsxSBWebPageBeanUtil
			.getDocumentFromString(currentScreenResp);
		createGetpageRequestFromCurrentResponse(document, pageName,
			sessionIdentifierValue, webBean);
		document = populateScreenLevelConfigs(webBean, document);
		requestXml = HsxSBWebPageBeanUtil
			.getStringFromNodeList(document);
	    } else {
		document = createGetPageXML();
		document = createGetpageRequest(document, pageName,
			sessionIdentifierValue, webBean);
		document = populateScreenLevelConfigs(webBean, document);
		requestXml = HsxSBWebPageBeanUtil
			.getStringFromDocument(document);
	    }

	} else if (STR_NEXT_PAGE_REQUEST_TYPE.equalsIgnoreCase(requestType)) {
	    document = HsxSBWebPageBeanUtil.getDocumentFromString(requestXml);
	    document = createNextPageRequest(document, webBean, requestType);
	    document = populateScreenLevelConfigs(webBean, document);
	    requestXml = HsxSBWebPageBeanUtil.getStringFromNodeList(document);
	} else if (STR_PROGRESS_BAR_PAGE_REQUEST_TYPE.equalsIgnoreCase(requestType)) {
	    document = HsxSBWebPageBeanUtil.getDocumentFromString(requestXml);
	    document = createNextPageRequest(document, webBean, requestType);
	    document = populateScreenLevelConfigs(webBean, document);
	    requestXml = HsxSBWebPageBeanUtil.getStringFromNodeList(document);
	} else if (STR_ACEGI_TARGET_URL_GET_REQUEST_TYPE
		.equalsIgnoreCase(requestType)) {
	    document = HsxSBWebPageBeanUtil.getDocumentFromString(requestXml);
	    document = createGetpageRequestForAcegiTargetUrl(pageName,
		    sessionIdentifierValue, webBean);
	    document = populateScreenLevelConfigs(webBean, document);
	    requestXml = HsxSBWebPageBeanUtil.getStringFromDocument(document);
	} else if (STR_BROACHERWARE_TYPE.equalsIgnoreCase(requestType)) {
	    document = createGetPageXML();
	    final int partnerArrayLength = PARTNERS_AND_AGENT_ARRAY.length;
		for (int partnerIndex = 0; partnerIndex < partnerArrayLength; partnerIndex++) {
			document = resetScreenConfigsToRequestXml(document, PARTNERS_AND_AGENT_ARRAY[partnerIndex][0], STR_EMPTY);		    
		}
	    document = createBroacherPageRequest(document, sessionIdentifierValue,
		    webBean.getPagesVisited(),webBean);
	    requestXml = HsxSBWebPageBeanUtil.getStringFromDocument(document);
	}

	requestXml = StringEscapeUtils.escapeXml(requestXml);
	if (HsxSBUtil.isBlank(requestXml)) {
	    logger.info(SBWEB, INFO, "Request XML is empty on"
		    + webBean.getRequestedAction() + " action for the page : "
		    + webBean.getFromPage());
	    // setting to default case
	    pageName = HOME_PAGE_CODE;
	    requestXml = createLandingPageReqXml(sessionIdentifierValue,
		    requestXml, pageName, webBean);
	    requestXml = StringEscapeUtils.escapeXml(requestXml);
	}
	requestXml = STR_LESSTHAN_SYMBOL + STR_GET_PAGE_XML
		+ STR_GREATERTHAN_SYMBOL + requestXml + STR_LESSTHAN_SYMBOL
		+ STR_FORWARD_SLASH + STR_GET_PAGE_XML + STR_GREATERTHAN_SYMBOL;
	logger.info(SBWEB, INFO, "Request Xml Logged :"
		+ StringEscapeUtils.unescapeXml(requestXml).replaceAll("><",
			">\n<"));
	// long b = Calendar.getInstance().getTimeInMillis();
	// logger.info(SBWEB, INFO,
	// "Time Taken for UI GetPage Service Request Preparation :"
	// + (b - a) * 0.001 + " Seconds");
	return requestXml;
    }

    /**
     * To create landing page request xml on default case also.
     *
     * @param sessionIdentifierValue
     * @param webBean
     * @return requestXml
     * @throws HsxCoreException
     * @throws ParserConfigurationException
     * @throws UnknownHostException
     */
    private String createLandingPageReqXml(String sessionIdentifierValue,
	    String requestXml, String pageName, HsxSBWebBean webBean)
	    throws HsxCoreException, ParserConfigurationException,
	    UnknownHostException {

	Document document = null;
	// validate the Scheme ID entered by the user in the URL
	validateSchemeId(webBean.getSchemeID());
	document = createGetPageXML();
	// Including a new request Validation for all the Home pages to
	// ensure that a proper session identifier is set in all the home
	// page requests
	if (StringUtils.isBlank(sessionIdentifierValue)) {
	    StringBuilder newSessionId = new StringBuilder(STR_EMPTY);
	    newSessionId.append(webBean.getWebBeanId());
	    newSessionId.append(HsxSBWebPageBeanUtil
		    .generateUniqueSessionIdentifier());
	    sessionIdentifierValue = newSessionId.toString();
	    logger.info(SBWEB, INFO,
		    "New Session Created with session Identifier in the Service Broker -->:"
			    + sessionIdentifierValue);
	}
	document = createLandingPageRequest(document, sessionIdentifierValue,
		webBean.getPagesVisited(), pageName, webBean);
	document = populateScreenLevelConfigs(webBean, document);
	requestXml = HsxSBWebPageBeanUtil.getStringFromDocument(document);
	return requestXml;
    }

    /**
     * @param webBean
     * @param document
     * @return document
     */

    private Document populateScreenLevelConfigs(HsxSBWebBean webBean,
	    Document document) {
	document = populateScreenConfigsToRequestXml(document, STR_CLIENT_IP,
		webBean.getClientIP());
	document = populateScreenConfigsToRequestXml(document, STR_SCHEME_ID,
		webBean.getSchemeID());
	if(!HsxSBWebPageBeanUtil.isMapEmpty(webBean.getPartnerAndAgentMap())){
	for (String partnerkey : webBean.getPartnerAndAgentMap().keySet()){
		document = populateScreenConfigsToRequestXml(document, partnerkey,
				webBean.getPartnerAndAgentMap().get(partnerkey));
	}
	}
	document = populateScreenConfigsToRequestXml(document, STR_IS_JS_ENABLED,
		webBean.getIsJSEnabled());

	if (STR_BROACHERWARE_TYPE.equalsIgnoreCase(webBean.getRequestType())) {
	document = populateScreenConfigsToRequestXml(document, STR_BROACHERWARE_CONFIG,
		STR_YES);
	document = populateScreenConfigsToRequestXml(document, STR_IS_JS_ENABLED,
		STR_YES);
	}
	//ON JS disabled case, to show same page on IFrame back on Payment Details
	
	if (STR_NO.equalsIgnoreCase(webBean.getIsJSEnabled()) && STR_PAYMENT_DETAILS.equalsIgnoreCase(webBean.getNextScreenId()) && webBean.isNonJSDataCashCSSIncludeFlag()
		&& !STR_MAKE_PAYMENT_ACTION.equalsIgnoreCase(webBean.getRequestedAction())) {
		document = populateScreenConfigsToRequestXml(document, STR_PAYMENT_DETAILS_BACK,
			STR_YES);
		webBean.setNonJSDataCashCSSIncludeFlag(false);
	}
	
	if (StringUtils.isNotBlank(webBean.getLdapUserName())) {
	    document = populateScreenConfigsToRequestXml(document,
		    STR_EMAIL_ID, webBean.getLdapUserName());
	}
	if (StringUtils.isNotBlank(webBean.getAccLckUserName())) {
	    document = populateScreenConfigsToRequestXml(document,
		    STR_ACC_LCK_USER_NAME, webBean.getAccLckUserName());
	}
	// Reset The Account Locked User Name
	webBean.setAccLckUserName(null);
	if (StringUtils.isNotBlank(webBean.getUserName())) {
	    document = populateScreenConfigsToRequestXml(document,
		    STR_RESET_PASSWORD_USER_NAME, webBean.getUserName());
	}
	// Reset The User Name for Reset Password
	webBean.setUserName(null);
	if (StringUtils.isNotBlank(webBean.getGuidType())) {
	    document = populateScreenConfigsToRequestXml(document,
		    STR_GUID_TYPE, webBean.getGuidType());
	}
	// Reset The GuIdType for Reset Password
	webBean.setGuidType(null);
	return document;
    }

    /**
     * @param webBean
     * @param savedQuotesDcoument
     */
    private static void retrieveSavedQuotesmap(HsxSBWebBean webBean,
	    Document savedQuotesDcoument) {

	NodeList screeQuestionList = savedQuotesDcoument
		.getElementsByTagName(STR_SCREEN_QUESTION);
	int screenQuestionListLength = screeQuestionList.getLength();
	Map<String, String> savedQuotesMap = webBean.getSavedQuotesMap();
	for (int screenQuestionIndex = 0; screenQuestionIndex < screenQuestionListLength; screenQuestionIndex++) {
	    Node screenQuestionNode = screeQuestionList
		    .item(screenQuestionIndex);
	    Element screenQuestionElement = (Element) screenQuestionNode;
	    if (screenQuestionElement.hasAttribute(STR_CODE)
		    && STR_REFERENCE_NUMBER
			    .equalsIgnoreCase(screenQuestionElement
				    .getAttribute(STR_CODE))) {
		NodeList screenQuestionChildList = screenQuestionElement
			.getChildNodes();
		if (screenQuestionChildList != null) {
		    for (int configIndex = 0; configIndex < screenQuestionChildList
			    .getLength(); configIndex++) {
			Node screenChildNode = screenQuestionChildList
				.item(configIndex);
			if (screenChildNode != null
				&& screenChildNode.getNodeType() == Node.ELEMENT_NODE) {
			    String configNodeName = screenChildNode
				    .getNodeName();
			    if (configNodeName != null
				    && configNodeName.contains(XML_CONFIG)) {
				Element configElement = (Element) screenChildNode;
				if (configElement.hasAttribute(STR_NAME)
					&& configElement
						.hasAttribute(STR_VALUE)
					&& CONFIG_SAVED_VALUE
						.equalsIgnoreCase(configElement
							.getAttribute(STR_NAME))) {
				    String quoteReferenceId = configElement
					    .getAttribute(STR_VALUE);
				    if (StringUtils
					    .isNotBlank(quoteReferenceId)) {
					logger.info(SBWEB, INFO,
						"QuoteReferenceId :- "
							+ quoteReferenceId);
					savedQuotesMap.put(quoteReferenceId,
						quoteReferenceId);
				    }
				    break;
				}
			    }
			}
		    }
		}
	    }
	    webBean.setSavedQuotesMap(savedQuotesMap);
	}
    }

    public void setServiceBinder(HsxCoreRequest request)  {
	request.setAttribute(SRVICE_BINDER, STR_GET_PAGE_SERVICE_BINDER_NAME);
    }

    public void setDefaultURI(HsxCoreRequest request) {
	StringBuilder webserviceURL = new StringBuilder(STR_EMPTY);
	webserviceURL.append(appConfigProperties
		.getProperty(STR_GET_PAGE_SERVICE_URL));
	webserviceURL.append(STR_GET_PAGE_SERVICE_DEFAULT_URI);
	request.setAttribute(DEFAULT_URI, webserviceURL.toString());

    }

    /**
     * This method creates the request for the getPage service call when the
     * request is of Landing Page type.
     *
     * @param document
     * @param sessionIdentifierValue
     * @return document
     */
    private Document createLandingPageRequest(Document document,
	    String sessionIdentifierValue, String pagesVistedString,
	    String pageName, HsxSBWebBean webBean) {
	Element appAreaElement = (Element) document.getElementsByTagName(
		STR_APPLICATIONAREA).item(0);
	if (appAreaElement != null) {
	    updateGetPageElement(appAreaElement, STR_MESSAGE_ID,
		    STR_MESSAGE_ID_VALUE);
	    updateGetPageElement(appAreaElement, STR_TRANSACTIONID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_TRANSMISSION_ID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_SENDER_ID,
		    STR_LANDING_PAGE);
	}
	Element dataAreaElement = (Element) document.getElementsByTagName(
		STR_DATAAREA).item(0);
	if (dataAreaElement != null) {
	    updateUINavigationHistory(document, pagesVistedString,
		    dataAreaElement);
	    /*US15149 -Auto retrieve quote based on link: Start*/
	    if(webBean.getRetrieveQuoteGUID()!=null){
		    addConfigElement(document, dataAreaElement, STR_RETRIEVE_QUOTE_GUID,
					webBean.getRetrieveQuoteGUID());
		    webBean.setRetrieveQuoteGUID(null);
		    updateGetPageElement(dataAreaElement, STR_CURRENT_SCREEN_ID,
		    		STR_GET_RETRIEVE_QUOTE_SCREEN);
		    updateGetPageElement(dataAreaElement, STR_REQUESTED_ACTION,
		    		STR_RETRIEVE_A_QUOTE_BUTTON_NAME);
		    webBean.setRequestedAction(STR_RETRIEVE_A_QUOTE_BUTTON_NAME);
		    }else{
	    updateGetPageElement(dataAreaElement, STR_CURRENT_SCREEN_ID,
		    STR_BLANK_SCREEN);
	    updateGetPageElement(dataAreaElement, STR_REQUESTED_ACTION,
			    pageName);		    
		    }
	    /*US15149 -Auto retrieve quote based on link: End*/
	    updateGetPageElement(dataAreaElement, STR_SCHEME_CODE, STR_USDCWEB);
	    updateGetPageElement(dataAreaElement, STR_SESSION_IDENTIFIER,
		    sessionIdentifierValue);
	    updateGetPageElement(dataAreaElement, STR_NEXT_SCREEN_ID, STR_BLANK);
	    populatePreviousScreenIdToRequestXml(webBean, document);
	    Element tagElement = (Element) document.getElementsByTagName(
		    STR_TAG_LIST).item(0);
	    Element configElement = document.createElement(XML_CONFIG);
	    configElement.setAttribute(STR_NAME, STR_TAG_NAME);
	    configElement.setAttribute(STR_VALUE, STR_TAG_VALUE);
	    if (tagElement != null) {
		tagElement.appendChild(configElement);
	    }

	}
	return document;
    }
    /**
     * This method is to create element based on its element type.
     * @param document
     * @param parentElement
     * @param createElementType
     * @param codeValue questionElement
     * @return questionElement
     */
    private static Element createDynElement(Document document,
	    Element parentElement, String createElementType, String codeValue) {
	Element questionElement = document
		.createElement(createElementType);
	questionElement.setAttribute(STR_CODE, codeValue);
	parentElement.appendChild(questionElement);

	return questionElement;
    }

    /**
     * To update the broacherware page request with dynamic data as per request.
     *
     * @param document
     * @param sessionIdentifierValue
     * @param pagesVistedString
     * @param pageName
     * @param webBean
     * @return document
     */
    private Document createBroacherPageRequest(Document document,
	    String sessionIdentifierValue, String pagesVistedString,
	     HsxSBWebBean webBean) {
	Element appAreaElement = (Element) document.getElementsByTagName(
		STR_APPLICATIONAREA).item(0);
	if (appAreaElement != null) {
	    updateGetPageElement(appAreaElement, STR_MESSAGE_ID,
		    STR_MESSAGE_ID_VALUE);
	    updateGetPageElement(appAreaElement, STR_TRANSACTIONID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_TRANSMISSION_ID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_SENDER_ID,
		    STR_LANDING_PAGE);
	}
	Element dataAreaElement = (Element) document.getElementsByTagName(
		STR_DATAAREA).item(0);
	Map<String, String> sessionMap = webBean.getSessionMap();
	if (dataAreaElement != null) {
	    updateUINavigationHistory(document, pagesVistedString,
		    dataAreaElement);
	    updateGetPageElement(dataAreaElement, STR_SESSION_IDENTIFIER,
		    sessionIdentifierValue);
	    if (StringUtils.isNotBlank(webBean.getRequestedAction())) {
		updateGetPageElement(dataAreaElement, STR_REQUESTED_ACTION,
			webBean.getRequestedAction());
	    }
	    updateGetPageElement(dataAreaElement, STR_CURRENT_SCREEN_ID,
		    HOME_PAGE_CODE);
	    updateGetPageElement(dataAreaElement, STR_SCHEME_CODE, STR_USDCWEB);
	    updateGetPageElement(dataAreaElement, STR_NEXT_SCREEN_ID, HOME_PAGE_CODE);
	    Element formElement = (Element) document.getElementsByTagName(STR_FORM).item(0);
	    if (formElement != null) {
		    Element questionGroup1 = createDynElement(document, formElement, STR_QUESTION_GROUP,
				LANDING_PAGE_QUESTION_GROUP_ELEMENT_ARR[0][0]);
		    Element dummyQuestion = createDynElement(document, questionGroup1, STR_SCREEN_QUESTION, LANDING_PAGE_DUMMY_QG);
		    Element subQuestionGroup1 = createDynElement(document, questionGroup1, STR_QUESTION_GROUP,
				LANDING_PAGE_QUESTION_ELEMENT_ARR[0][0]);
		    String screenQuestionString = LANDING_PAGE_QUESTION_ELEMENT_ARR[0][1];
		    if (screenQuestionString != null) {
		    createScreenQuestion(document, dummyQuestion,
			    subQuestionGroup1, screenQuestionString);
		}
		}
	    populatePreviousScreenIdToRequestXml(webBean, document);
	    populateScreenLevelConfigs(webBean, document);
	    setScreenQuestionSavedValues(document, sessionMap);
	}

	return document;
    }
/**
 * This method is to construct screen questions for a given question group.
 * @param document
 * @param dummyQuestion
 * @param subQuestionGroup
 * @param screenQuestionString
 */
    private void createScreenQuestion(Document document, Element dummyQuestion,
	    Element subQuestionGroup, String screenQuestionString) {
	String[] screenQuestionArrray = screenQuestionString.split(STR_COMA);
		    final int screenQuestionArrayLength = screenQuestionArrray.length;
		for (int screenQuesitonIndex = 0; screenQuesitonIndex < screenQuestionArrayLength; screenQuesitonIndex++) {
		    String questionCode = screenQuestionArrray[screenQuesitonIndex];
		    Element screenQuestionElement = document.createElement(STR_SCREEN_QUESTION);
		    screenQuestionElement.setAttribute(STR_CODE, questionCode);
		    Map<String, String> xpathMap = HsxSBWebPageBeanUtil.createHashMap(LANDING_PAGE_QUESTION_XPATH_ELEMENT_ARR);
		    	if (xpathMap.get(questionCode) != null) {
			    screenQuestionElement.setAttribute(STR_XPATH, xpathMap.get(questionCode));
			    }

		    //Add screen question config
		    addConfigElement(document, screenQuestionElement, STR_SAVED_VALUE, STR_EMPTY);
		    subQuestionGroup.appendChild(screenQuestionElement);
		}
		dummyQuestion.appendChild(subQuestionGroup);
    }
/**
 * This method is set answered values from session Map to screen question saved value.
 * @param document
 * @param sessionMap
 */
    private void setScreenQuestionSavedValues(Document document,
	    Map<String, String> sessionMap) {
	NodeList screenQuestionList = document.getElementsByTagName(XML_SCREEN_QUESTION);
	final int noOfQuestions = screenQuestionList.getLength();
	for (int questionIndex = 0; questionIndex < noOfQuestions; questionIndex++) {
	Element screenQuestion = (Element) screenQuestionList
		.item(questionIndex);
	updateFormLevelConfigs(sessionMap, screenQuestion);
	}
    }

    /**
     * This method is to update saved values of screen question under form.
     *
     * @param sessionMap
     * @param screenQuestion
     */
    private void updateFormLevelConfigs(Map<String, String> sessionMap,
	    Element screenQuestion) {
	NodeList childElementsList = screenQuestion.getChildNodes();
	for (int configIndex = 0; configIndex < childElementsList.getLength(); configIndex++) {
	    String nodeName = childElementsList.item(configIndex).getNodeName();
	    if (childElementsList.item(configIndex).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null && nodeName.contains(XML_CONFIG)) {
		Element childElement = (Element) childElementsList
			.item(configIndex);
		if (CONFIG_SAVED_VALUE.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    String answerdValue = STR_EMPTY;
		    String screenQuestionCode = screenQuestion.getAttribute(
			    STR_CODE).toString();
		    if (sessionMap
			    .containsKey(screenQuestionCode.toLowerCase())) {
			answerdValue = sessionMap.get(screenQuestionCode
				.toLowerCase());
			childElement.setAttribute(STR_VALUE, answerdValue);
		    }

		}
	    }
	}
    }

    private void updateQuestionLevelConfigs(Document document, HsxSBWebBean webBean) {

	String hostURL = webBean.getHostURL();
	String successPageName = appConfigProperties.getProperty("datacashSuccessURL");
	String expiryPageName  = appConfigProperties.getProperty("datacashExpiryURL");
	String pageSetId  = appConfigProperties.getProperty("datacashPageSetId");
	String resourceProtocol  = appConfigProperties.getProperty("datacashResourcesProtocol");
	NodeList screenQuestionList = document.getElementsByTagNameNS(
		    STR_ASTERISK, XML_SCREEN_QUESTION);

	    final int noOfQuestions = screenQuestionList.getLength();
	    for (int questionIndex = 0; questionIndex < noOfQuestions; questionIndex++){
		Element screenQuestion = (Element) screenQuestionList
			.item(questionIndex);
		String screenQuestionCode = retrieveQuestionCode(screenQuestion);
		    if (STR_DATACASH_PO_QUESTIONCODE.equalsIgnoreCase(screenQuestionCode)) {
			updateMapwithConfigValuesForDataCash(screenQuestion, webBean);
		    }

	NodeList childElementsList = screenQuestion.getChildNodes();
	for (int configIndex = 0; configIndex < childElementsList.getLength(); configIndex++){
	    String nodeName = childElementsList.item(configIndex).getNodeName();
	    if (childElementsList.item(configIndex).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null && nodeName.contains(XML_CONFIG)) {
		Element childElement = (Element) childElementsList
			.item(configIndex);
		if (CONFIG_RETURN_URL.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
				if(webBean.getPartnerPaymentSuccessURL()!=null){
					childElement.setAttribute(STR_VALUE, webBean.getPartnerPaymentSuccessURL());
					webBean.setPartnerPaymentSuccessURL(null);
					}else{
					String successURL = HsxSBWebPageBeanUtil.getAbsoluteURL(successPageName, hostURL, webBean);
					childElement.setAttribute(STR_VALUE, successURL);
					}
		} else if (CONFIG_EXPIRY_URL.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
				if(webBean.getPartnerPaymentExpiryURL()!=null){
					childElement.setAttribute(STR_VALUE, webBean.getPartnerPaymentExpiryURL());
					webBean.setPartnerPaymentExpiryURL(null);
				}else{
					String expiryURL = HsxSBWebPageBeanUtil.getAbsoluteURL(expiryPageName, hostURL, webBean);
					childElement.setAttribute(STR_VALUE, expiryURL);
					}
		} else if (CONFIG_PAGESET_ID.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    childElement.setAttribute(STR_VALUE, pageSetId);

		} else if (CONFIG_REFERENCE.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    if (webBean.getDataCashReference() != null) {
		    childElement.setAttribute(STR_VALUE, webBean.getDataCashReference());
		    }
		} else if (CONFIG_DYNAMIC_DATA1.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    String resourcesProtocol = HsxSBWebPageBeanUtil.getAbsoluteResourcesProtocol(resourceProtocol, hostURL);
		    childElement.setAttribute(STR_VALUE, resourcesProtocol);

		} else if (CONFIG_DYNAMIC_DATA2.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    String  dataCashQuoteRef = webBean.getSessionMap().get(STR_DATACASH_QUESTIONREF);
		    childElement.setAttribute(STR_VALUE, dataCashQuoteRef);
		} else if (CONFIG_DYNAMIC_DATA3.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    String  dynData3 = webBean.getSessionMap().get(STR_DATACASH_PO_QUESTIONCODE_ANS);
		    childElement.setAttribute(STR_VALUE, dynData3);
		} else if (CONFIG_DYNAMIC_DATA4.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {

		    String  dynData4 = HsxSBWebPageBeanUtil.getDataCashBuildHTMLOne(webBean);
		    childElement.setAttribute(STR_VALUE, dynData4);
		} else if (CONFIG_DYNAMIC_DATA5.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {

		    String  dynData5 = webBean.getSessionMap().get(STR_DATACASH_PO_ADDTVALUE1);
		    childElement.setAttribute(STR_VALUE, dynData5);
		} else if (CONFIG_DYNAMIC_DATA6.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {

		    String  dynData6 = webBean.getSessionMap().get(STR_DATACASH_PO_ADDTVALUE2);
		    childElement.setAttribute(STR_VALUE, dynData6);
		} else if (CONFIG_DYNAMIC_DATA7.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    String  dynData7 = webBean.getSessionMap().get(STR_DATACASH_PO_ADDTVALUE3);
		    childElement.setAttribute(STR_VALUE, dynData7);
		} else if (CONFIG_DYNAMIC_DATA8.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    if (!STR_PV_MONTHLY.equalsIgnoreCase(webBean.getSessionMap().get(STR_DATACASH_PO_QUESTIONCODE_ANS))) {
		    String  dynData8 = STR_DYNAMIC_DATA_STYLE;
		    childElement.setAttribute(STR_VALUE, dynData8);
		    } else {
			String  dynData8 = STR_DYNAMIC_DATA_REVERSE_STYLE;
			childElement.setAttribute(STR_VALUE, dynData8);
		    }
		}
	    }
	}
    }
    }

    private void updateMapwithConfigValuesForDataCash(Element screenQuestion, HsxSBWebBean webBean) {
	NodeList childElementsList = screenQuestion.getChildNodes();
	for (int configIndex = 0; configIndex < childElementsList.getLength(); configIndex++) {
	    String nodeName = childElementsList.item(configIndex).getNodeName();
	    if (childElementsList.item(configIndex).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null && nodeName.contains(XML_CONFIG)) {
		Element childElement = (Element) childElementsList
			.item(configIndex);
		if (CONFIG_SAVED_VALUE.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    String childValue = (childElement.getAttribute(STR_VALUE)).toString();
		    webBean.getSessionMap().put(STR_DATACASH_PO_QUESTIONCODE_ANS, childValue);

		}
	    }
	    }
    }



    /**
     * This method creates the request for all the getPage service calls when
     * the request is of Get Page type.
     *
     * @param document
     * @param pageName
     * @param sessionIdentifierValue
     * @return document
     */
    private Document createGetpageRequest(Document document, String pageName,
	    String sessionIdentifierValue, HsxSBWebBean webBean) {
	Element appAreaElement = (Element) document.getElementsByTagName(
		STR_APPLICATIONAREA).item(0);
	if (appAreaElement != null) {
	    updateGetPageElement(appAreaElement, STR_MESSAGE_ID,
		    STR_MESSAGE_ID_VALUE);
	    updateGetPageElement(appAreaElement, STR_TRANSACTIONID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_TRANSMISSION_ID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_SENDER_ID, pageName);
	    removeErrorElement(appAreaElement);
	}
	Element dataAreaElement = (Element) document.getElementsByTagName(
		STR_DATAAREA).item(0);
	if (dataAreaElement != null) {
	    addConfigElement(document, dataAreaElement, STR_REQUEST_TYPE,
		    STR_GET);
	    addConfigElement(document, dataAreaElement, STR_REQUEST_PAGE,
		    pageName);
	    populatePreviousScreenIdToRequestXml(webBean, document);
	    populateLoginIndicator(document, webBean, dataAreaElement);

	    updateGetPageElement(dataAreaElement, STR_CURRENT_SCREEN_ID,
		    STR_GET_SCREEN);
	    updateGetPageElement(dataAreaElement, STR_SCHEME_CODE, STR_USDCWEB);

	    updateGetPageElement(dataAreaElement, STR_SESSION_IDENTIFIER,
		    sessionIdentifierValue);
	    updateUINavigationHistory(document, webBean.getPagesVisited(),
		    dataAreaElement);
	}
	return document;
    }

    /**
     * This method creates the request for all the getPage service calls when
     * the request is of Get Page type.
     *
     * @param document
     * @param pageName
     * @param sessionIdentifierValue
     * @return document
     */
    private Document createGetpageRequestFromCurrentResponse(Document document,
	    String pageName, String sessionIdentifierValue, HsxSBWebBean webBean) {
	NodeList showPageNodeList = document.getElementsByTagNameNS(
		STR_ASTERISK, STR_SHOWPAGE);
	for (int i = 0; i < showPageNodeList.getLength(); i++) {
	    Node showPageNode = showPageNodeList.item(i);
	    document.renameNode(showPageNode, STR_SHOWPAGE, STR_GETPAGE);
	}

	Element appAreaElement = (Element) document.getElementsByTagName(
		STR_APPLICATIONAREA).item(0);
	if (appAreaElement != null) {
	    updateGetPageElement(appAreaElement, STR_MESSAGE_ID,
		    STR_MESSAGE_ID_VALUE);
	    updateGetPageElement(appAreaElement, STR_TRANSACTIONID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_TRANSMISSION_ID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_SENDER_ID, pageName);
	    removeInvalidElement(appAreaElement, XML_STATUS);
	    removeInvalidElement(appAreaElement, XML_CORRELATION_ID);
	    removeErrorElement(appAreaElement);
	}
	Element dataAreaElement = (Element) document.getElementsByTagName(
		STR_DATAAREA).item(0);
	if (dataAreaElement != null) {
	    addConfigElement(document, dataAreaElement, STR_REQUEST_TYPE,
		    STR_GET);
	    addConfigElement(document, dataAreaElement, STR_REQUEST_PAGE,
		    pageName);
	    populatePreviousScreenIdToRequestXml(webBean, document);
	    populateLoginIndicator(document, webBean, dataAreaElement);

	    updateGetPageElement(dataAreaElement, STR_CURRENT_SCREEN_ID,
		    STR_GET_SCREEN);
	    updateGetPageElement(dataAreaElement, STR_SCHEME_CODE, STR_USDCWEB);

	    updateGetPageElement(dataAreaElement, STR_SESSION_IDENTIFIER,
		    sessionIdentifierValue);
	    updateUINavigationHistory(document, webBean.getPagesVisited(),
		    dataAreaElement);
	}
	return document;
    }

    /**
     * @param document
     * @param webBean
     * @param dataAreaElement
     */
    private void updateUINavigationHistory(Document document,
	    String pagesVisitedString, Element dataAreaElement) {
	if (StringUtils.isNotBlank(pagesVisitedString)) {
	    Element userAreaElement = document.createElement(STR_USER_AREA);
	    Element navigationHistoryelement = document
		    .createElement(STR_UI_NAVIGATION_HISTORY);
	    String[] pageVisitedArray = pagesVisitedString.split(STR_COMA);
	    for (String pageVisited : pageVisitedArray) {
		if (StringUtils.isNotBlank(pageVisited)) {
		    if (pageVisited.endsWith(STR_FORWARD_SLASH)) {
			pageVisited = StringUtils.substringBeforeLast(
				pageVisited, STR_FORWARD_SLASH);
		    }
		    if (pageVisited.contains(STR_FORWARD_SLASH)) {
			pageVisited = StringUtils.substringAfterLast(
				pageVisited, STR_FORWARD_SLASH);
		    }
		    Element pageVisitedElement = document
			    .createElement(STR_PAGE_VISITED);
		    pageVisitedElement.setTextContent(pageVisited);
		    navigationHistoryelement.appendChild(pageVisitedElement);
		}

	    }
	    userAreaElement.appendChild(navigationHistoryelement);
	    dataAreaElement.appendChild(userAreaElement);
	}
    }

    /**
     * @param document
     * @param dataAreaElement
     */
    private void addConfigElement(Document document, Element dataAreaElement,
	    String configName, String configValue) {
	Element configElement = document.createElement(XML_CONFIG);
	setConfigAttributes(configName, configValue, configElement);
	dataAreaElement.appendChild(configElement);
    }

    /**
     * @param configName
     * @param configValue
     * @param configElement
     */
    private void setConfigAttributes(String configName, String configValue,
	    Element configElement) {
	configElement.setAttribute(STR_NAME, configName);
	configElement.setAttribute(STR_VALUE, configValue);
    }

    private Document createGetpageRequestForAcegiTargetUrl(String pageName,
	    String sessionIdentifierValue, HsxSBWebBean webBean)
	    throws ParserConfigurationException {
	Document getPageDocument = createGetPageXML();

	Element dataAreaElement = (Element) getPageDocument
		.getElementsByTagName(STR_DATAAREA).item(0);
	Element appAreaElement = (Element) getPageDocument
		.getElementsByTagName(STR_APPLICATIONAREA).item(0);
	if (dataAreaElement != null) {
	    Element formElement = createFormElement(webBean, getPageDocument);
	    dataAreaElement.appendChild(formElement);
	    addConfigElement(getPageDocument, dataAreaElement,
		    STR_REQUEST_TYPE, STR_GET);
	    addConfigElement(getPageDocument, dataAreaElement,
		    STR_REQUEST_PAGE, pageName);
	    populatePreviousScreenIdToRequestXml(webBean, getPageDocument);

	    populateLoginIndicator(getPageDocument, webBean, dataAreaElement);

	    updateGetPageElement(dataAreaElement, STR_CURRENT_SCREEN_ID,
		    webBean.getNextScreenId());
	    updateGetPageElement(dataAreaElement, STR_NEXT_SCREEN_ID, webBean
		    .getNextScreenId());
	    updateGetPageElement(dataAreaElement, STR_REQUESTED_ACTION, webBean
		    .getRequestedAction());

	    updateGetPageElement(dataAreaElement, STR_SCHEME_CODE, STR_USDCWEB);
	    updateGetPageElement(dataAreaElement, STR_SESSION_IDENTIFIER,
		    sessionIdentifierValue);

	}

	logger.info(SBWEB, INFO, "Requested Action :"
		+ webBean.getRequestedAction());

	if (appAreaElement != null) {
	    updateGetPageElement(appAreaElement, STR_MESSAGE_ID,
		    STR_MESSAGE_ID_VALUE);
	    updateGetPageElement(appAreaElement, STR_TRANSACTIONID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_TRANSMISSION_ID,
		    STR_THOUSAND);
	    updateGetPageElement(appAreaElement, STR_SENDER_ID, "USDC");
	    removeInvalidElement(appAreaElement, XML_STATUS);
	    removeInvalidElement(appAreaElement, XML_CORRELATION_ID);
	    removeErrorElement(appAreaElement);
	}
	webBean.setServiceCallRequired(false);
	return getPageDocument;
    }

    /**
     * @param webBean
     * @param getPageDocument
     * @return formElement
     */
    private Element createFormElement(HsxSBWebBean webBean,
	    Document getPageDocument) {
	Element formElement = getPageDocument.createElement(XML_FORM);
	Element quesGroupElement = getPageDocument
		.createElement(STR_QUESTION_GROUP);
	Element screenQuesElement = createScreenQuesForLdapUser(webBean,
		getPageDocument);
	quesGroupElement.appendChild(screenQuesElement);
	formElement.appendChild(quesGroupElement);
	return formElement;
    }

    /**
     * @param webBean
     * @param getPageDocument
     * @return screenQuesElement
     */
    private Element createScreenQuesForLdapUser(HsxSBWebBean webBean,
	    Document getPageDocument) {
	Element screenQuesElement = getPageDocument
		.createElement(STR_SCREEN_QUESTION);
	screenQuesElement.setAttribute(STR_CODE, LDAP_USER_NAME);
	screenQuesElement.setAttribute(STR_XPATH, EMAIL_XPATH_VALUE);
	Element configElement = getPageDocument.createElement(XML_CONFIG);
	setConfigAttributes(CONFIG_SAVED_VALUE, webBean.getLdapUserName(),
		configElement);
	screenQuesElement.appendChild(configElement);
	return screenQuesElement;
    }

    /**
     * @param document
     * @param webBean
     * @param dataAreaElement
     */
    private void populateLoginIndicator(Document document,
	    HsxSBWebBean webBean, Element dataAreaElement) {

	if (dataAreaElement != null) {
	    Element loginIndicatorElement = document.createElement(XML_CONFIG);
	    String loginIndicator = webBean.getLoginIndicator();
	    if (StringUtils.isBlank(loginIndicator)) {
		loginIndicator = STR_NO;
	    }

	    if (STR_YES.equalsIgnoreCase(loginIndicator)) {
		loginIndicatorElement.setAttribute(STR_NAME,
			STR_LOGIN_INDICATOR);
		loginIndicatorElement.setAttribute(STR_VALUE, loginIndicator);
		dataAreaElement.appendChild(loginIndicatorElement);
	    }
	}
    }

    /**
     * This method creates the request for all the getPage service calls when
     * the request is given to retrieve the next page.
     *
     * @param showPageDocument
     * @param webBean
     * @return showPageDocument
     * @throws ParserConfigurationException
     */
    private Document createNextPageRequest(Document showPageDocument,
	    HsxSBWebBean webBean, String requestType) throws ParserConfigurationException {
	if (showPageDocument != null) {
	    NodeList showPageNodeList = showPageDocument
		    .getElementsByTagNameNS(STR_ASTERISK, STR_SHOWPAGE);
	    for (int i = 0; i < showPageNodeList.getLength(); i++) {
		Node showPageNode = showPageNodeList.item(i);
		showPageDocument.renameNode(showPageNode, STR_SHOWPAGE,
			STR_GETPAGE);
	    }
	} else {
	    showPageDocument = createGetPageXML();
	}
	String nextScreenId = null;
	logger.info(SBWEB, INFO, "NextScreenId in the Bean :"
		+ webBean.getNextScreenId());
	if (StringUtils.isNotBlank(webBean.getNextScreenId())) {
	    nextScreenId = webBean.getNextScreenId();
	}
	Element dataAreaElement = (Element) showPageDocument
		.getElementsByTagName(STR_DATAAREA).item(0);
	Element appAreaElement = (Element) showPageDocument
		.getElementsByTagName(STR_APPLICATIONAREA).item(0);

	if (STR_PROGRESS_BAR_PAGE_REQUEST_TYPE.equalsIgnoreCase(requestType)) {
	    // This is specifically written for Progress bar requests.
	    // Below Logic would copy the NextScreenId value to the
	    // CurrentScreenId.NextScreenId value would be retrieve from the
	    // showPageDocument. This ShowPageDocument ussually would be the copy of
	    // previous response xml.
	    Element nextScreenIdElement = (Element) showPageDocument
		    .getElementsByTagNameNS(STR_ASTERISK, STR_NEXT_SCREEN_ID)
		    .item(0);
	    if (nextScreenIdElement != null) {
		String showPageNextScreenId = nextScreenIdElement
			.getTextContent();
		logger.info(SBWEB, INFO, "showPageNextScreenId value retieved for Progress Bar ---> " + showPageNextScreenId);
		if (StringUtils.isNotBlank(showPageNextScreenId)) {
		    if (dataAreaElement != null) {
			updateGetPageElement(dataAreaElement,
				STR_CURRENT_SCREEN_ID, showPageNextScreenId);
		    }
		} else {
		    if (dataAreaElement != null) {
			updateGetPageElement(dataAreaElement,
				STR_CURRENT_SCREEN_ID, nextScreenId);
		    }
		}

	    }

	} else {
	    if (dataAreaElement != null) {
		updateGetPageElement(dataAreaElement, STR_CURRENT_SCREEN_ID,
			nextScreenId);
	    }
	}

	populateLoginIndicator(showPageDocument, webBean, dataAreaElement);
	updateUINavigationHistory(showPageDocument, webBean.getPagesVisited(),
		dataAreaElement);
	//To send DataCash Related Information
	if (STR_PAYMENT_DETAILS.equalsIgnoreCase(webBean.getNextScreenId())) {
	updateQuestionLevelConfigs(showPageDocument, webBean);
	}
	logger.info(SBWEB, INFO, "Requested Action :"
		+ webBean.getRequestedAction());

	if (appAreaElement != null) {
	    populateSenderId(webBean, nextScreenId, appAreaElement);
	}
	return showPageDocument;
    }

    /**
     * @param webBean
     * @param nextScreenId
     * @param appAreaElement
     */
    private void populateSenderId(HsxSBWebBean webBean, String nextScreenId,
	    Element appAreaElement) {
	StringBuilder senderId = new StringBuilder(STR_EMPTY);
	senderId.append(nextScreenId);
	if (!STR_FORGOTTEN_PASSWORD.equalsIgnoreCase(nextScreenId)
		&& !STR_RESET_PASSWORD.equalsIgnoreCase(nextScreenId)
		&& !STR_LOCK_ACCOUNT.equalsIgnoreCase(nextScreenId)) {
	    senderId.append(SYMBOL_COLON);
	    senderId.append(webBean.getRequestedAction());
	}
	updateGetPageElement(appAreaElement, STR_SENDER_ID, senderId.toString());
    }

    /**
     * This method will retrieve the sessionIdentifier from the response xml and
     * populates the same to Web bean.
     *
     * @param webBean
     * @param showPageDocument
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private void retrieveSessionIdentifier(HsxSBWebBean webBean,
	    Document showPageDocument) throws ParserConfigurationException,
	    SAXException, IOException {

	Element sessionIdentifierElement = (Element) showPageDocument
		.getElementsByTagNameNS(STR_ASTERISK, STR_SESSION_IDENTIFIER)
		.item(0);
	if (sessionIdentifierElement != null) {
	    String sessionIdentifierValue = sessionIdentifierElement
		    .getTextContent();
	    if (StringUtils.isNotBlank(sessionIdentifierValue)) {
		webBean.setSessionIdentifierValue(sessionIdentifierValue);
	    }
	}
    }

    /**
     * This method will retrieve the nextScreenId from the response xml and
     * populates the same to Web bean.
     *
     * @param webBean
     * @param showPageDocument
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     */
    private void retrieveNextScreenId(HsxSBWebBean webBean,
	    Document showPageDocument) throws ParserConfigurationException,
	    SAXException, IOException {

	Element nextScreenIdElement = (Element) showPageDocument
		.getElementsByTagNameNS(STR_ASTERISK, STR_NEXT_SCREEN_ID).item(
			0);
	if (nextScreenIdElement != null) {
	    String nextScreenIdValue = nextScreenIdElement.getTextContent();
	    logger.info(SBWEB, INFO, "nextScreenIdValue :" + nextScreenIdValue);
	    if (StringUtils.isNotBlank(nextScreenIdValue)) {
		webBean.setNextScreenId(nextScreenIdValue);
		// webBean.setLoginIndicator(null);
		if (STR_QUOTE_SAVED.equalsIgnoreCase(nextScreenIdValue)
			|| STR_SAVED_QUOTE.equalsIgnoreCase(nextScreenIdValue)) {
		    webBean.setLoginIndicator(STR_YES);
		}
	    } else {
		// Next Screen Id is defaulted to Home page.
		// This code will get executed when we get empty nextscreenId
		// value from Jrules.
		logger.info(SBWEB, INFO, "NextScreenId Value has been defaulted to Home");
		webBean.setNextScreenId(HOME_PAGE_CODE);
	    }
	}
    }

    /**
     * This method will retrieve all the config elements from the response xml
     * which are present under screen level and populates the same to Web bean.
     *
     * @param webBean
     * @param showPageDocument
     */
    private void retrieveScreenConfigElements(HsxSBWebBean webBean,
	    Document showPageDocument) {
	Element dataAreaElement = (Element) showPageDocument
		.getElementsByTagNameNS(STR_ASTERISK, STR_DATAAREA).item(0);
	if (dataAreaElement != null) {
	    NodeList dataAreaChildNodesList = dataAreaElement.getChildNodes();
	    int dataAreaChldNodesLstLength = dataAreaChildNodesList.getLength();
	    webBean.setCheckBoxValidationFlag(STR_NO);
	    webBean.setRadioButtonValidationFlag(STR_NO);
	    webBean.setGroupValidationFlag(STR_NO);
	    webBean.setDefaultNavigator(null);
	    for (int dataAreaChildNodesIndex = 0; dataAreaChildNodesIndex < dataAreaChldNodesLstLength; dataAreaChildNodesIndex++) {
		Node daNode = dataAreaChildNodesList
			.item(dataAreaChildNodesIndex);
		if (daNode != null && daNode.getNodeType() == Node.ELEMENT_NODE) {
		    String nodeName = daNode.getNodeName();
		    if (nodeName != null && nodeName.contains(XML_CONFIG)) {
			Element configElement = (Element) daNode;
			String configName = configElement
				.getAttribute(STR_NAME);
			if (StringUtils.isNotBlank(configName)) {
			    if (STR_QUOTE_REF_ID.equalsIgnoreCase(configName)) {
				String quoteRefID = configElement
					.getAttribute(STR_VALUE);
				logger.info(SBWEB, INFO,
					"Quote reference Id Recieved :"
						+ quoteRefID);
				if (StringUtils.isNotBlank(quoteRefID)) {
				    webBean.setQuoteRefID(quoteRefID);
				    // quote ref id ias also stored as a backup
				    // which will be used in logging the same to
				    // Error Management
				    webBean.setErrMngtQuoteRefId(quoteRefID);
				    //To populate in DataCash HTML
				    webBean.getSessionMap().put(STR_DATACASH_QUESTIONREF, quoteRefID);
				}
			    } else if (STR_CHECKBOX_VALIDATION_FLAG
				    .equalsIgnoreCase(configName)) {
				webBean.setCheckBoxValidationFlag(configElement
					.getAttribute(STR_VALUE));
			    } else if (STR_RADIO_BUTTON_VALIDATION_FLAG
				    .equalsIgnoreCase(configName)) {
				webBean
					.setRadioButtonValidationFlag(configElement
						.getAttribute(STR_VALUE));
			    } else if (STR_LOGIN_INDICATOR
				    .equalsIgnoreCase(configName)) {
				webBean.setLoginIndicator(configElement
					.getAttribute(STR_VALUE));
			    } else if (STR_GRP_VALIDTN
				    .equalsIgnoreCase(configName)) {
				webBean.setGroupValidationFlag(configElement
					.getAttribute(STR_VALUE));
			    } else if (STR_DEFAULT_NAVIGATOR
				    .equalsIgnoreCase(configName)) {
				webBean.setDefaultNavigator(configElement
					.getAttribute(STR_VALUE));
			    }
			}
		    }
		}
	    }

	}

    }

    private void retrieveActionMap(Document document, HsxSBWebBean webBean) {
	if (document != null) {
	    NodeList screenQuestionList = document
		    .getElementsByTagName(STR_SCREEN_QUESTION);
	    // Reset Action Map Before Reading
	    webBean.getActionMap().clear();
	    // Reset the ProgressBar Action Map.
	    webBean.getProgressbarActionMap().clear();
	    // Reset the Encryption Action Map.
	    webBean.getEncryptionActionMap().clear();

	    if (screenQuestionList != null) {
		for (int screenIndex = 0; screenIndex < screenQuestionList
			.getLength(); screenIndex++) {
		    readActionConfigs(webBean, screenQuestionList, screenIndex);
		}
	    }
	    NodeList actionList = document.getElementsByTagName(XML_ACTION);
	    if (actionList != null) {
		for (int actionIndex = 0; actionIndex < actionList.getLength(); actionIndex++) {
		    readActionConfigs(webBean, actionList, actionIndex);
		}
	    }

	}

    }

    private void retrieveScreenActionMap(Document document, HsxSBWebBean webBean) {
	webBean.getScreenActionMap().clear();
	if (document != null) {
	    NodeList screenQuestionList = document
		    .getElementsByTagName(STR_SCREEN_QUESTION);
	    Map<String, String> screenActionMap = new HashMap<String, String>();
	    if (screenQuestionList != null) {
		for (int screenIndex = 0; screenIndex < screenQuestionList
			.getLength(); screenIndex++) {
		    Element screenQuestionElement = (Element) screenQuestionList
			    .item(screenIndex);
		    if (screenQuestionElement != null) {
			NodeList configList = screenQuestionElement
				.getChildNodes();
			if (configList != null) {
			    final int configLength = configList.getLength();
			    for (int configIndex = 0; configIndex < configLength; configIndex++) {
				Node screenChildNode = configList
					.item(configIndex);
				if (screenChildNode != null
					&& screenChildNode.getNodeType() == Node.ELEMENT_NODE) {
				    String configNodeName = screenChildNode
					    .getNodeName();
				    if (configNodeName != null
					    && configNodeName
						    .contains(XML_CONFIG)) {
					Element configElement = (Element) screenChildNode;
					if (configElement
						.hasAttribute(STR_NAME)
						&& STR_TYPE
							.equalsIgnoreCase(configElement
								.getAttribute(STR_NAME))
						&& XML_ACTION
							.equalsIgnoreCase(configElement
								.getAttribute(STR_VALUE))) {
					    String questionCode = retrieveQuestionCode(screenQuestionElement);
					    if (screenActionMap != null) {
						screenActionMap.put(
							questionCode, STR_YES);
						webBean
							.setScreenActionMap(screenActionMap);
					    }

					}

				    }
				}
			    }
			}
		    }
		}
	    }
	    NodeList actionList = document.getElementsByTagName(XML_ACTION);
	    if (actionList != null) {
		final int actionLength = actionList.getLength();
		for (int actionIndex = 0; actionIndex < actionLength; actionIndex++) {
		    Element actionElement = (Element) actionList
			    .item(actionIndex);
		    String questionCode = retrieveQuestionCode(actionElement);

		    if (screenActionMap != null) {
			screenActionMap.put(questionCode, STR_YES);
			webBean.setScreenActionMap(screenActionMap);
		    }
		}
	    }
	}

    }

    /**
     * @param webBean
     * @param screenQuestionList
     * @param screenIndex
     */
    private void readActionConfigs(HsxSBWebBean webBean,
	    NodeList screenQuestionList, int screenIndex) {
	Element screenQuestionElement = (Element) screenQuestionList
		.item(screenIndex);
	if (screenQuestionElement != null) {
	    NodeList configList = screenQuestionElement.getChildNodes();
	    if (configList != null) {
		for (int configIndex = 0; configIndex < configList.getLength(); configIndex++){
		    Node screenChildNode = configList.item(configIndex);
		    if (screenChildNode != null
			    && screenChildNode.getNodeType() == Node.ELEMENT_NODE) {
			String configNodeName = screenChildNode.getNodeName();
			if (configNodeName != null
				&& configNodeName.contains(XML_CONFIG)) {
			    Element configElement = (Element) screenChildNode;
			    if (configElement.hasAttribute(STR_NAME)) {
				if (CONFIG_IS_VALIDATION_REQUIRED
					.equalsIgnoreCase(configElement
						.getAttribute(STR_NAME))) {
				    String questionCode = retrieveQuestionCode(screenQuestionElement);

				    webBean
					    .getActionMap()
					    .put(
						    questionCode,
						    configElement
							    .getAttribute(XML_QUESTION_VALUE));
				} else if (CONFIG_IS_PROGRESS_BAR_REQUIRED
					.equalsIgnoreCase(configElement
						.getAttribute(STR_NAME))) {
				    String questionCode = retrieveQuestionCode(screenQuestionElement);
				    webBean
					    .getProgressbarActionMap()
					    .put(
						    questionCode,
						    configElement
							    .getAttribute(XML_QUESTION_VALUE));
				} else if (CONFIG_IS_ENCRYPTION_REQUIRED
					.equalsIgnoreCase(configElement
						.getAttribute(STR_NAME))) {
				    String questionCode = retrieveQuestionCode(screenQuestionElement);
				    webBean
					    .getEncryptionActionMap()
					    .put(
						    questionCode,
						    configElement
							    .getAttribute(XML_QUESTION_VALUE));
				}

			    }

			}
		    }
		}
	    }
	}
    }

    /**
     * @param screenQuestionElement
     * @return questionCode
     */
    private String retrieveQuestionCode(Element screenQuestionElement) {
	String questionCode = null;
	if (STR_SCREEN_QUESTION.equalsIgnoreCase(screenQuestionElement
		.getNodeName())) {
	    if (screenQuestionElement.hasAttribute(STR_CODE)) {
		questionCode = screenQuestionElement.getAttribute(STR_CODE);
	    }
	} else if (XML_ACTION.equalsIgnoreCase(screenQuestionElement
		.getNodeName())&& screenQuestionElement.hasAttribute(STR_NAME)) {
		questionCode = screenQuestionElement.getAttribute(STR_NAME);
	    
	}
	return questionCode;
    }

    /**
     * This method will set the IpAddress from the response xml and populates
     * the same to Request Xml.
     *
     * @param webBean
     * @param showPageDocument
     * @return xmlDocument
     */
    private Document populateScreenConfigsToRequestXml(Document xmlDocument,
	    String screenConfigName, String screenConfigValue) {
	Element dataAreaElement = (Element) xmlDocument.getElementsByTagName(
		STR_DATAAREA).item(0);
	if (dataAreaElement != null) {
	    NodeList dataAreaChildNodesList = dataAreaElement.getChildNodes();
	    final int dataAreaChldNodesLstLength = dataAreaChildNodesList
		    .getLength();
	    boolean configExistsFlag = false;
	    for (int dataAreaChildNodesIndex = 0; dataAreaChildNodesIndex < dataAreaChldNodesLstLength; dataAreaChildNodesIndex++) {
		Node daNode = dataAreaChildNodesList
			.item(dataAreaChildNodesIndex);
		if (daNode != null && daNode.getNodeType() == Node.ELEMENT_NODE) {
		    String nodeName = daNode.getNodeName();
		    if (nodeName != null && nodeName.contains(XML_CONFIG)) {
			Element configElement = (Element) daNode;
			String configName = configElement
				.getAttribute(STR_NAME);

			if (screenConfigName.equalsIgnoreCase(configName)) {
			    String configValue = configElement
				    .getAttribute(STR_VALUE);
			    configExistsFlag = true;
			    if (StringUtils.isBlank(configValue)) {
				// config exists and value is empty hence update
				// the config value
				configElement.setAttribute(STR_VALUE,
					screenConfigValue);
			    }
			    break;
			}
		    }
		}
	    }
	    if (!configExistsFlag) {
		// config doesnt exists and hence create a new config element
		addConfigElement(xmlDocument, dataAreaElement,
			screenConfigName, screenConfigValue);
	    }

	}
	return xmlDocument;

    }

    /**
     * This method is to reset screen level config values
     * for every new session request.
     * @param xmlDocument
     * @param screenConfigName
     * @param screenConfigValue
     * @return xmlDocument
     */

    private Document resetScreenConfigsToRequestXml(Document xmlDocument,
	    String screenConfigName, String screenConfigValue) {
	Element dataAreaElement = (Element) xmlDocument.getElementsByTagName(
		STR_DATAAREA).item(0);
	if (dataAreaElement != null) {
	    NodeList dataAreaChildNodesList = dataAreaElement.getChildNodes();
	    final int dataAreaChldNodesLstLength = dataAreaChildNodesList
		    .getLength();
	    for (int dataAreaChildNodesIndex = 0; dataAreaChildNodesIndex < dataAreaChldNodesLstLength; dataAreaChildNodesIndex++) {
		Node daNode = dataAreaChildNodesList
			.item(dataAreaChildNodesIndex);
		if (daNode != null && daNode.getNodeType() == Node.ELEMENT_NODE) {
		    String nodeName = daNode.getNodeName();
		    if (nodeName != null && nodeName.contains(XML_CONFIG)) {
			Element configElement = (Element) daNode;
			String configName = configElement
				.getAttribute(STR_NAME);

			if (screenConfigName.equalsIgnoreCase(configName)) {
				// config exists and value is empty hence update
				// the config value
				configElement.setAttribute(STR_VALUE,
					screenConfigValue);
			    break;
			}
		    }
		}
	    }
	}
	return xmlDocument;

    }

    /**
     * This method will set the previousScreenId from the Web bean and populates
     * the same to Get Request xml.
     *
     * @param webBean
     * @param showPageDocument
     * @return xmlDocument
     */
    private Document populatePreviousScreenIdToRequestXml(HsxSBWebBean webBean,
	    Document xmlDocument) {
	Element dataAreaElement = (Element) xmlDocument.getElementsByTagName(
		STR_DATAAREA).item(0);
	if (dataAreaElement != null) {
	    NodeList dataAreaChildNodesList = dataAreaElement.getChildNodes();
	    final int dataAreaChldNodesLstLength = dataAreaChildNodesList
		    .getLength();
	    boolean previousScreenIdFlag = false;
	    String previousScreenName = webBean.getNextScreenId();
	    for (int dataAreaChildNodesIndex = 0; dataAreaChildNodesIndex < dataAreaChldNodesLstLength; dataAreaChildNodesIndex++){
		Node daNode = dataAreaChildNodesList
			.item(dataAreaChildNodesIndex);
		if (daNode != null && daNode.getNodeType() == Node.ELEMENT_NODE) {
		    String nodeName = daNode.getNodeName();
		    if (nodeName != null && nodeName.contains(XML_CONFIG)) {
			Element configElement = (Element) daNode;
			String configName = configElement
				.getAttribute(STR_NAME);

			if (STR_PREVIOUS_SCREEN_ID.equalsIgnoreCase(configName)) {
			    String previousScreenId = configElement
				    .getAttribute(STR_VALUE);
			    previousScreenIdFlag = true;
			    if (StringUtils.isBlank(previousScreenId)) {
				configElement.setAttribute(STR_VALUE,
					previousScreenName);
			    }
			    break;
			}
		    }
		}
	    }
	    if (!previousScreenIdFlag) {
		addConfigElement(xmlDocument, dataAreaElement,
			STR_PREVIOUS_SCREEN_ID, previousScreenName);
	    }

	}
	return xmlDocument;

    }

    /**
     * This method is responsible for handling any Exception occuring in service
     * layer and converting it to HsxCoreException. If status is Nak in response
     * XML , it is treated as an exception in service layer.
     *
     * @param responseXML
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws HsxCoreException
     * @return status
     */
    public static String checkForGetPageServiceExceptions(
	    String requestXmlLogged, String responseXML)
	    throws ParserConfigurationException, SAXException, IOException,
	    HsxCoreException {
	Document showPageDocument = HsxSBWebPageBeanUtil
		.getDocumentFromString(responseXML);
	String status = null;
	if (showPageDocument != null
		&& showPageDocument.getElementsByTagNameNS(STR_ASTERISK,
			STR_STATUS) != null) {
	    Element statusElement = (Element) showPageDocument
		    .getElementsByTagNameNS(STR_ASTERISK, STR_STATUS).item(0);

	    if (statusElement != null) {
		status = statusElement.getTextContent();
	    }
	    logger.info(SBWEB, INFO, "Get Page Service Call Status --------> :"
		    + status);
	    if (StringUtils.isNotBlank(status)
		    && !STR_ACK_STATUS.equalsIgnoreCase(status.trim())
		    && !STR_BUSINESS_NAK.equalsIgnoreCase(status.trim())) {
		if (StringUtils.isNotBlank(requestXmlLogged)) {
		    logger.error(SBWEB, ERROR,
			    "Get Page Request Xml Logged for this error : ---> "
				    + StringEscapeUtils.unescapeXml(
					    requestXmlLogged).replaceAll("><",
					    ">\n<"));
		}
		if (StringUtils.isNotBlank(responseXML)) {
		    logger.error(SBWEB, ERROR,
			    "Get Page Response Xml Logged for this error : ---> "
				    + responseXML);
		}
		// Throw Exception and log the Error to ErrorManagment Service.
		Element errorsElement = (Element) showPageDocument
			.getElementsByTagNameNS(STR_ASTERISK, STR_ERRORS).item(
				0);
		String errorCode = null;
		String errorDescription = null;
		String errorInstanceID = null;
		String dateTime = null;
		String additionalInformation = null;
		if (errorsElement != null) {

		    Element errorCodeElement = (Element) errorsElement
			    .getElementsByTagNameNS(STR_ASTERISK, ERROR_CODE)
			    .item(0);
		    if (errorCodeElement != null) {
			errorCode = errorCodeElement.getTextContent();
		    }
		    Element errorDescriptionElement = (Element) errorsElement
			    .getElementsByTagNameNS(STR_ASTERISK,
				    ERROR_DESCRIPTION).item(0);
		    if (errorDescriptionElement != null) {
			errorDescription = errorDescriptionElement
				.getTextContent();
		    }
		    Element errorInstanceIDElement = (Element) errorsElement
			    .getElementsByTagNameNS(STR_ASTERISK,
				    XML_ERROR_INSTANCE_ID).item(0);
		   
		    if (errorInstanceIDElement != null) {
			errorInstanceID = errorInstanceIDElement
				.getTextContent();
		    }
		    Element dateTimeElement = (Element) errorsElement
			    .getElementsByTagNameNS(STR_ASTERISK, DATE_TIME)
			    .item(0);
		  
		    if (dateTimeElement != null) {
			dateTime = dateTimeElement.getTextContent();
		    }
		    Element additionalInformationElement = (Element) errorsElement
			    .getElementsByTagNameNS(STR_ASTERISK,
				    ADDITIONAL_INFORMATION).item(0);
		 
		    if (additionalInformationElement != null) {
			additionalInformation = additionalInformationElement
				.getTextContent();
		    }
		    logger.error(SBWEB, ERROR, "GetPage Service Error Code :"
			    + errorCode);
		    logger.error(SBWEB, ERROR,
			    "GetPage Service Error Description :"
				    + errorDescription);
		    logger.error(SBWEB, ERROR,
			    "GetPage Service Error Instance ID :"
				    + errorInstanceID);
		    logger.error(SBWEB, ERROR,
			    "GetPage Service Error Occured at :" + dateTime);
		    logger.error(SBWEB, ERROR,
			    "GetPage Service Error Additional Information :"
				    + additionalInformation);

		}
		throw new HsxCoreException(errorCode, errorDescription,
			errorInstanceID, dateTime, additionalInformation);
	    }
	}
	return status;
    }

    /**
     * This method is responsible for preparing a blank xml document for
     * invoking getPage orchestration service.
     *
     * @return document
     * @throws ParserConfigurationException
     */
    public static Document createGetPageXML()
	    throws ParserConfigurationException {
	String root = STR_GETPAGE;
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	DocumentBuilder parser;
	Document document = null;

	factory.setNamespaceAware(true);
	parser = factory.newDocumentBuilder();
	document = parser.newDocument();
	Element parentrootElement = document.createElement(root);
	Element childRootElement = document.createElement(root);
	Element appAreaelement = document.createElement(STR_APPLICATIONAREA);
	final int appAreaArrayLength = APPLICATION_AREA_ELEMENT_ARR.length;
	for (int appAreaIndex = 0; appAreaIndex < appAreaArrayLength; appAreaIndex++) {
	    appAreaelement.appendChild(document
		    .createElement(APPLICATION_AREA_ELEMENT_ARR[appAreaIndex]));
	}
	childRootElement.appendChild(appAreaelement);
	Element dataAreaElement = document.createElement(STR_DATAAREA);
	final int dataAreaArrayLength = DATA_AREA_ELEMENT_ARRAY.length;
	for (int dataAreaIndex = 0; dataAreaIndex < dataAreaArrayLength; dataAreaIndex++) {
	    dataAreaElement.appendChild(document
		    .createElement(DATA_AREA_ELEMENT_ARRAY[dataAreaIndex]));
	}
	childRootElement.appendChild(dataAreaElement);
	parentrootElement.appendChild(childRootElement);
	document.appendChild(parentrootElement);

	return document;
    }

    /**
     * This method will be used to update any given element in the request xml
     * document.
     *
     * @param element
     * @param elementName
     * @param elementValue
     */
    private static void updateGetPageElement(Element element,
	    String elementName, String elementValue) {
	Element appChildElement = (Element) element.getElementsByTagName(
		elementName).item(0);
	if (appChildElement != null) {
	    appChildElement.setTextContent(elementValue);
	}
    }

    @SuppressWarnings("unused")
    private static void removeEmptyGroups(Document document, String groupName) {
	NodeList sqList = document.getElementsByTagNameNS(STR_ASTERISK,
		groupName);
	for (int index = 0; index < sqList.getLength(); index++) {
	    Node qgNode = sqList.item(index);
	    if (qgNode.getChildNodes().getLength() == 0) {
		Element el = (Element) qgNode;
		logger.info(SBWEB, INFO, "Empty " + el.getAttribute(STR_CODE)
			+ " " + el.getNodeName() + " removed");
		qgNode.getParentNode().removeChild(qgNode);
	    }
	}

    }

    /**
     * @param appAreaElement
     */
    private static void removeInvalidElement(Element appAreaElement,
	    String elementName) {
	if (appAreaElement.getElementsByTagNameNS(STR_ASTERISK, elementName) != null) {
	    Node invalidNode = appAreaElement.getElementsByTagNameNS(
		    STR_ASTERISK, elementName).item(0);
	    if (invalidNode != null) {
		appAreaElement.removeChild(invalidNode);
	    }
	}
    }

    // static String getXml()
    // {
    // String inputXML = "";
    // // logger.info(SBWEB, INFO,"fsgfl");
    // // replace the below code to retrieve IPEXML from rule engine
    // try
    // {
    // File f = new File("C:\\TestData\\Dev-CTM-Testing\\saved-quotes.xml");
    // RandomAccessFile rnd;
    // rnd = new RandomAccessFile(f, "rw");
    // String inter = rnd.readLine();
    // while (inter != null)
    // {
    // inputXML = inputXML + inter;
    // inter = rnd.readLine();
    // }
    // rnd.close();
    // } catch (Exception e)
    // {
    // e.printStackTrace();
    // }
    // // logger.info(SBWEB, INFO,"inputXML :" + inputXML);
    // return inputXML;
    // }

    /**
     * @param appAreaElement
     */
    private static void removeErrorElement(Element appAreaElement) {
	if (appAreaElement.getElementsByTagNameNS(STR_ASTERISK, STR_ERRORS) != null) {
	    Node errorsNode = appAreaElement.getElementsByTagNameNS(
		    STR_ASTERISK, STR_ERRORS).item(0);
	    if (errorsNode != null) {
		appAreaElement.removeChild(errorsNode);
	    }
	}
    }

    /**
     * Encrypt the credit card details before making the web service call Page
     * specific process - applicable only for card payment page.
     *
     * @param responseXml
     *            String
     * @param hsxSBWebEncryption
     *            HsxSBWebEncryption
     * @param widgetMap
     *            HsxSBWidget
     *
     * @return String
     * @throws HsxCoreException
     * @throws Exception
     */
    /*public Document decryptCardPaymentData(Document xmlDocument,
	    HsxSBWebEncryption hsxSBWebEncryption, String status)
	    throws HsxCoreException
    {
	if (xmlDocument != null && STR_BUSINESS_NAK.equalsIgnoreCase(status))
	{
	    String pageName = null;
	    Element dataAreaElement = (Element) xmlDocument
		    .getElementsByTagNameNS(STR_ASTERISK, STR_DATAAREA).item(0);
	    NodeList dataAreaChldNdLst = dataAreaElement.getChildNodes();
	    int dataAreaChldNdLstLength = dataAreaChldNdLst.getLength();
	    for (int daChldNdLstIndex = 0; daChldNdLstIndex < dataAreaChldNdLstLength; daChldNdLstIndex++)
	    {
		final Node daChildNode = dataAreaChldNdLst
			.item(daChldNdLstIndex);
		if (daChildNode != null
			&& daChildNode.getNodeType() == Node.ELEMENT_NODE)
		{
		    String nodeName = daChildNode.getNodeName();
		    if (nodeName != null && nodeName.contains(STR_CODE))
		    {
			pageName = daChildNode.getTextContent();
			break;
		    }
		}
	    }

	    if (STR_PAYMENT_DETAILS.equalsIgnoreCase(pageName))
	    {

		NodeList screenQuestionList = xmlDocument
			.getElementsByTagNameNS(STR_ASTERISK,
				XML_SCREEN_QUESTION);
		for (int questionIndex = 0; questionIndex < screenQuestionList
			.getLength(); questionIndex++)
		{
		    Element screenQuestion = (Element) screenQuestionList
			    .item(questionIndex);
		    String screenQuestionCode = screenQuestion.getAttribute(
			    STR_CODE).toString();
		    if (STR_CARD_NUMBER.equalsIgnoreCase(screenQuestionCode)
			    || STR_CARD_SECURITY_CODE
				    .equalsIgnoreCase(screenQuestionCode)
			    || STR_CARD_EXPIRY_DATE
				    .equalsIgnoreCase(screenQuestionCode))
		    {
			NodeList childElementsList = screenQuestion
				.getChildNodes();
			final int childElementsLength = childElementsList
				.getLength();
			for (int configIndex = 0; configIndex < childElementsLength; configIndex++)
			{
			    final Node sqChildElement = childElementsList
				    .item(configIndex);
			    if (sqChildElement != null)
			    {
				String nodeName = sqChildElement.getNodeName();
				if (sqChildElement.getNodeType() == Node.ELEMENT_NODE
					&& nodeName != null
					&& nodeName.contains(XML_CONFIG))
				{
				    Element childElement = (Element) sqChildElement;
				    if (CONFIG_SAVED_VALUE
					    .equalsIgnoreCase((childElement
						    .getAttribute(STR_NAME))
						    .toString()))
				    {
					String answerdValue = childElement
						.getAttribute(
							XML_QUESTION_VALUE)
						.toString();

					try
					{
					    if (StringUtils
						    .isNotBlank(answerdValue))
					    {
						answerdValue = hsxSBWebEncryption
							.doDataDecryptionAndDecoding(
								STR_CARD_PAYMENT_COMPONENT_NAME,
								answerdValue);
					    }
					} catch (Exception e)
					{
					    // TODO Auto-generated catch block
					    e.printStackTrace();
					    throw new HsxCoreException();
					}

					if (STR_CARD_EXPIRY_DATE
						.equalsIgnoreCase(screenQuestionCode))
					{
					    if (answerdValue != null
						    && answerdValue
							    .contains("/")
						    && answerdValue.trim()
							    .length() == 5)
					    {
						answerdValue = "01/"
							+ StringUtils
								.substringBefore(
									answerdValue,
									"/")
							+ "/20"
							+ StringUtils
								.substringAfter(
									answerdValue,
									"/");
						logger.info(SBWEB, INFO,
							answerdValue);
					    }
					}
					logger.info(SBWEB, INFO,
						"****Decpted***UI shown to user"
							+ answerdValue);

					childElement.setAttribute(
						XML_QUESTION_VALUE,
						answerdValue);
				    }
				}

			    }
			}
		    }
		}
	    }
	}

	return xmlDocument;
    }*/

    private void validateSchemeId(String schemeId) throws HsxCoreException {
	// Check if the Scheme read from the URL is valid
	logger.info(SBWEB, INFO, "SchemeID entered by the User :" + schemeId);
	if (StringUtils.isNotBlank(schemeId)) {
	    schemeId = schemeId.trim();
	    if (!schemeIdListProperties.containsKey(schemeId)) {
		logger.error(SBWEB, ERROR,
			"User is redirected to System Error page because of an invalid SchemeID :"
				+ schemeId);
		throw new HsxCoreException(STR_UI_ERROR_CODE,
			STR_IMPROPER_SCHEME_ID_ERROR_MESSAGE, "EM_MESSAGE_ID",
			HsxSBWebPageBeanUtil.getCurrentTimeStamp(),
			STR_IMPROPER_SCHEME_ID_ERROR_MESSAGE + EMPTY_SPACE
				+ schemeId);
	    }
	}

    }

    private void updateConfigValuesForDataCash(Document document, HsxSBWebBean webBean) {
	NodeList screenQuestionList = document.getElementsByTagNameNS(
		    STR_ASTERISK, XML_SCREEN_QUESTION);

	    final int noOfQuestions = screenQuestionList.getLength();
	    for (int questionIndex = 0; questionIndex < noOfQuestions; questionIndex++) {
		Element screenQuestion = (Element) screenQuestionList
			.item(questionIndex);
		String screenQuestionCode = retrieveQuestionCode(screenQuestion);
		    if (STR_DATACASH_PO_QUESTIONCODE.equalsIgnoreCase(screenQuestionCode)) {
			updateMapwithConfigValuesForDataCash(screenQuestion, webBean);

	NodeList childElementsList = screenQuestion.getChildNodes();
	for (int configIndex = 0; configIndex < childElementsList.getLength(); configIndex++) {
	    String nodeName = childElementsList.item(configIndex).getNodeName();
	    if (childElementsList.item(configIndex).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null && nodeName.contains(XML_CONFIG)) {
		Element childElement = (Element) childElementsList
			.item(configIndex);
		if (CONFIG_ADDITIONAL_VALUE1.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    String additionalValue1 = childElement.getAttribute(STR_VALUE);
		    webBean.getSessionMap().put(STR_DATACASH_PO_ADDTVALUE1, additionalValue1);

		} else if (CONFIG_ADDITIONAL_VALUE2.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    String additionalValue2 = childElement.getAttribute(STR_VALUE);
		    webBean.getSessionMap().put(STR_DATACASH_PO_ADDTVALUE2, additionalValue2);

		} else if (CONFIG_ADDITIONAL_VALUE3.equalsIgnoreCase((childElement
			.getAttribute(STR_NAME)).toString())) {
		    String additionalValue3 = childElement.getAttribute(STR_VALUE);
		    webBean.getSessionMap().put(STR_DATACASH_PO_ADDTVALUE3, additionalValue3);

		}
	    }
	}
	}
	    }
	    }

	
    /**
     * Added for US25094-Add Accounting PL to DPD 
     * Updating product values in Session Map for PL Products & Bookkeeping/tax preparation combination
     * @param document
     * @param webBean
     */
	private void updateConfigValuesForPL(Document document, HsxSBWebBean webBean) {
		NodeList tagList = document.getElementsByTagNameNS(STR_ASTERISK,
				XML_TAGLIST);
		webBean.getSessionMap().put(ENO_PRODUCT, null);
		final int noOfTags = tagList.getLength();
		for (int questionIndex = 0; questionIndex < noOfTags; questionIndex++) {
			Element element = (Element) tagList.item(questionIndex);

			NodeList childElementsList = element.getChildNodes();
			for (int configIndex = 0; configIndex < childElementsList
					.getLength(); configIndex++) {
				String nodeName = childElementsList.item(configIndex)
						.getNodeName();
				if (childElementsList.item(configIndex).getNodeType() == Node.ELEMENT_NODE
						&& nodeName != null && nodeName.contains(XML_CONFIG)) {
					Element childElement = (Element) childElementsList
							.item(configIndex);
					if (CONFIG_PRODUCT_NAME.equalsIgnoreCase((childElement
							.getAttribute(STR_NAME)).toString())) {
						String savedval = childElement.getAttribute(STR_VALUE);
						if (savedval.contains(STR_EO)) {

							webBean.getSessionMap()
									.put(ENO_PRODUCT, STR_ADDENO);

						}

					}
				}
			}

		}
	}
	
	 /**
     * Added for DEFECT 5865&5887-Variable Text not coming for retrieve quote through mail
     * Updating occupation variant values in Session Map for retrieve quote through mail
     * @param document
     * @param webBean
     */
	private void updateSessionValuesRetrieveQuote(Document document,
			HsxSBWebBean webBean) {
		NodeList tagList = document.getElementsByTagNameNS(STR_ASTERISK,
				XML_TAGLIST);
		
		final int noOfTags = tagList.getLength();
		for (int questionIndex = 0; questionIndex < noOfTags; questionIndex++) {
			Element element = (Element) tagList.item(questionIndex);

			NodeList childElementsList = element.getChildNodes();
			for (int configIndex = 0; configIndex < childElementsList
					.getLength(); configIndex++) {
				String nodeName = childElementsList.item(configIndex)
						.getNodeName();
				if (childElementsList.item(configIndex).getNodeType() == Node.ELEMENT_NODE
						&& nodeName != null && nodeName.contains(XML_CONFIG)) {
					Element childElement = (Element) childElementsList
							.item(configIndex);

					if (CONFIG_CLASS_OF_BUSINESS.equalsIgnoreCase((childElement
							.getAttribute(STR_NAME)).toString())) {

						String savedval = childElement.getAttribute(STR_VALUE);
						if (savedval != null && !STR_EMPTY.equals(savedval)) {
							webBean.getSessionMap().put(PRIMARY_BUSINESS_CODE,
									savedval);
							String businessGroup = HsxSBWebSessionManager.occupVariantMap
									.get(savedval);
							webBean.getSessionMap().put(OCCUPATION_VARIANT,
									businessGroup);
						}
					}

				}
			}

		}
	}	
	
	public String getNSLRedirectionUrl()
	{
		return appConfigProperties.getProperty("nslRedirectionUrl");
	}
	

}

