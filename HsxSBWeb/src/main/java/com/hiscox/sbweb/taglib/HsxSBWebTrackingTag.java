package com.hiscox.sbweb.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebTrackingTag implements Tag, IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    /**
     *
     * @param formContent
     * @return SKIP_BODY
     * @throws JspException
     */
    public int doStartTag() throws JspException {
	logger.info(SBWEB, INFO, "Tracking Tag Excecution Started");
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	try {
	    String trackingContent = webBean.getTrackingRenderContent();
	    if (StringUtils.isBlank(trackingContent)) {
		logger.info(SBWEB, INFO, "Tracking Tag Rendered Empty Content");
		trackingContent = STR_EMPTY;
	    }
	    pc.getOut().write(trackingContent);
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    logger.error("IOException", e);
	}
	logger.info(SBWEB, INFO, "Tracking Tag Excecution Completed");
	return SKIP_BODY;
    }
/**
 * @return EVAL_PAGE
 * @throws JspException
 */
    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
    }

}

