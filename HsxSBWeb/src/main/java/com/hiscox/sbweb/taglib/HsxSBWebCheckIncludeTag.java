package com.hiscox.sbweb.taglib;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebCheckIncludeTag implements Tag, IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;
    private String includeContent = STR_EMPTY;

    public String getIncludeContent() {
	return includeContent;
    }

    public PageContext getContext() {
	return this.pc;
    }

    public void setIncludeContent(String includeContent) {
	this.includeContent = includeContent;
    }

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    /**
     *
     * @param formContent
     * @return skip body constant
     */
    public int doStartTag() throws JspException {
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	if (webBean != null) {
	    if (null != webBean.getQuoteRefID()
		    && null != webBean.getFolderType() && StringUtils.isNotBlank(webBean.getQuoteRefID())
					&& QUOTE_AND_BUY.equalsIgnoreCase(webBean
							.getFolderType())) {
		    return EVAL_BODY_INCLUDE;
	    }
	    Map<String, String> sessionMap = new HashMap<String, String>();

	    if (null != webBean.getSessionMap()) {
		sessionMap = webBean.getSessionMap();

		if (sessionMap.containsKey(includeContent)
			&& StringUtils.isBlank(sessionMap.get(includeContent))) {
		    return EVAL_BODY_INCLUDE;
		} else {
		    return SKIP_BODY;
		}
	    } else if (webBean.getDynamicPageFlag()) {
		return EVAL_BODY_INCLUDE;
	    }
	}
	return SKIP_BODY;

    }

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
	includeContent = null;
    }

}

