package com.hiscox.sbweb.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebIncludeScriptTag implements Tag, IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;

    public PageContext getContext() {
	return this.pc;
    }

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    /**
     *
     * @param formContent
     * @return skip body constant
     */
    public int doStartTag() throws JspException {
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	StringBuilder renderContent = new StringBuilder(STR_EMPTY);
	if (null != webBean.getFolderType()
		&& QUOTE_AND_BUY.equalsIgnoreCase(webBean.getFolderType())) {
	    //renderContent.append(includescript(STR_FOCUS_JS));
	    /*if (webBean.getCalendarRequired()) {
		renderContent.append(includescript(STR_CALENDAR_JS));
	    }*/
	    if ((STR_NO.equalsIgnoreCase(webBean.getIsJSEnabled())) && STR_PAYMENT_DETAILS_FOLDER.equalsIgnoreCase(webBean.getFolder())) {
		renderContent.append(includeNonJSPaymentDetails());
	    }
	    if ((STR_NO.equalsIgnoreCase(webBean.getIsJSEnabled())) && webBean.isNonJSDataCashCSSIncludeFlag()
		    && STR_PAYMENT_DETAILS_FOLDER.equalsIgnoreCase(webBean.getFolder())) {
		renderContent.append(includeNonJSPaymentDetailsIframe());
	    }
	    if ((STR_NO.equalsIgnoreCase(webBean.getIsJSEnabled()))  && STR_MAKE_PAYMENT_ACTION.equalsIgnoreCase(webBean.getRequestedAction())) {
		renderContent.append(includeNonJSConfirmation());
	    }
	    try {
		if (renderContent != null) {
		    pc.getOut().write(renderContent.toString());
		}
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		logger.error("IOException", e);
	    }
	}
	return SKIP_BODY;

    }

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
    }

    public StringBuilder includescript(String scriptFileName) {
	StringBuilder renderContent = new StringBuilder();
	renderContent.append(STR_LESSTHAN_SYMBOL);
	renderContent.append(STR_SCRIPT);
	renderContent.append(EMPTY_SPACE);
	renderContent.append(STR_TYPE);
	renderContent.append(EQUAL_TO);
	renderContent.append(DOUBLE_QUOTE);
	renderContent.append(STR_TEXT);
	renderContent.append(STR_FORWARD_SLASH);
	renderContent.append(STR_JAVASCRIPT);
	renderContent.append(DOUBLE_QUOTE);
	renderContent.append(EMPTY_SPACE);
	renderContent.append(STR_SRC);
	renderContent.append(EQUAL_TO);
	renderContent.append(DOUBLE_QUOTE);
	renderContent.append(STR_FORWARD_SLASH);
	renderContent.append(STR_SYSTEST_RESOURCES_CONTEXTPATH);
	renderContent.append(STR_FORWARD_SLASH);
	renderContent.append(STR_JAVASCRIPT);
	renderContent.append(STR_FORWARD_SLASH);
	renderContent.append(scriptFileName);
	renderContent.append(DOUBLE_QUOTE);
	renderContent.append(STR_GREATERTHAN_SYMBOL);
	renderContent.append(STR_LESSTHAN_SYMBOL);
	renderContent.append(STR_FORWARD_SLASH);
	renderContent.append(STR_SCRIPT);
	renderContent.append(STR_GREATERTHAN_SYMBOL);

	return renderContent;

    }
    public StringBuilder includeNonJSPaymentDetailsIframe() {
	StringBuilder renderContent = new StringBuilder();
	renderContent.append("<noscript><style>.page-heading{display:none !important;}.page-heading+.clear{ display:none!important;}  .topcnt-container{display:none!important;}</style></noscript>");

	return renderContent;

    }
    public StringBuilder includeNonJSPaymentDetails() {
	StringBuilder renderContent = new StringBuilder();
	renderContent.append("<noscript><style>#confirmpaymentoptions_question_id{display:block !important;}</style></noscript>");
	return renderContent;

    }

    public StringBuilder includeNonJSConfirmation() {
	StringBuilder renderContent = new StringBuilder();
	renderContent.append("<noscript><link rel=\"stylesheet\" href=\"/resources/css/confirmation.css\" type=\"text/css\" media=\"all\" /></noscript>");

	return renderContent;

    }
}

