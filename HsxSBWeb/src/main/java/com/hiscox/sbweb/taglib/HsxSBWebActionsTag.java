package com.hiscox.sbweb.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * This tag to trigger the Action List processing and to generates XHTML.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:19 AM
 */
public class HsxSBWebActionsTag implements Tag, IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public Tag getParent() {
	return parent;
    }

    public void setParent(Tag parent) {
	this.parent = parent;
    }

    public void setPageContext(PageContext p) {
	pc = p;
    }

    /**
     * This method is called to build action content only.
     *
     * @return skip body constant
     */
    public int doStartTag() throws JspException {
	logger.info(SBWEB, INFO, "Action Tag Excecution Started");
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	try {
	    String actionContent = webBean.getActionRenderContent();
	    if (StringUtils.isBlank(actionContent)) {
		logger.info(SBWEB, INFO, "Action Tag Rendered Empty Content");
		actionContent = STR_EMPTY;
	    }
	    pc.getOut().write(actionContent);
	    // webBean.setActionRenderContent(STR_EMPTY);
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	   logger.error("IOException",e);
	}
	logger.info(SBWEB, INFO, "Action Tag Excecution Completed");
	return SKIP_BODY;
    }

    public void release() {
	pc = null;
	parent = null;
    }

    public int doEndTag() throws JspException {

	return EVAL_PAGE;
    }

}

