package com.hiscox.sbweb.taglib;

import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebNavigationManager;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebCheckLoginTag implements Tag, IHsxSBWebConstants {


    private PageContext pc = null;
    private Tag parent = null;
    private String loginContent = STR_EMPTY;

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public PageContext getContext() {
	return this.pc;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    public String getLoginContent() {
	return loginContent;
    }

    public void setLoginContent(String loginContent) {
	this.loginContent = loginContent;
    }

    /**
     *
     * @param formContent
     * @return skip body constant
     */
    public int doStartTag() throws JspException {
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	Map<String, HsxSBWebPageRuleBean> navigationRuleMap = HsxSBWebNavigationManager
		.getInstance().getNavigationMap();
	HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap.get(webBean
		.getFolder());
	if (null != pageRuleBean
		&& !STR_YES.equalsIgnoreCase(pageRuleBean
			.getIsEndOfJourneyPage())) {

	    if (LOGOUT.equalsIgnoreCase(loginContent) && STR_YES.equalsIgnoreCase(webBean.getLoginIndicator())) {
		    return EVAL_BODY_INCLUDE;
		}
	
	    if (RETRIEVE.equalsIgnoreCase(loginContent) && !(QUOTE_AND_BUY + STR_FORWARD_SLASH + RETRIEVE_A_QUOTE)
				.equalsIgnoreCase(webBean.getFolder())) {
		    return EVAL_BODY_INCLUDE;
		}
	   
	}
	return SKIP_BODY;
    }

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
	loginContent = null;
    }

}

