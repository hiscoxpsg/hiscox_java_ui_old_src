package com.hiscox.sbweb.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebCheckPageTag implements Tag, IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;
    private String pageContent = STR_EMPTY;

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public PageContext getContext() {
	return this.pc;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    public String getPageContent() {
	return pageContent;
    }

    public void setPageContent(String pageContent) {
	this.pageContent = pageContent;
    }

    /**
     *
     * @param formContent
     * @return skip body constant
     */
    public int doStartTag() throws JspException {
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	if (null != webBean.getPageBarItem()) {
		/*US15149 -Auto retrieve quote based on link: Start*/
		if(SAVED_QUOTE_EXCEPTION.equalsIgnoreCase(webBean.getNextScreenId())){
	    	webBean.setPageBarItem(STR_EMPTY);
	    }
		/*US15149 -Auto retrieve quote based on link: End*/
		if (pageContent.equalsIgnoreCase(webBean.getPageBarItem())) {
	    return EVAL_BODY_INCLUDE;
	    }
	    return SKIP_BODY;
	} else {
	    return SKIP_BODY;
	}

    }

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
	pageContent = null;
    }

}

