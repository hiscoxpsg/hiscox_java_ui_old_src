package com.hiscox.sbweb.taglib;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebCheckEligibilityTag implements Tag, IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;
    private String eligibleContent = STR_EMPTY;
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public PageContext getContext() {
	return this.pc;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    public String getEligibleContent() {
	return eligibleContent;
    }

    public void setEligibleContent(String eligibleContent) {
	this.eligibleContent = eligibleContent;
    }

    /**
     *
     * @param formContent
     * @return skip body constant
     */
    public int doStartTag() throws JspException {
	Map<String, String> sessionMap = new HashMap<String, String>();
	String[] eligibilityStringArr = null;
	int count = 0;
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	if (null != webBean.getSessionMap()) {
	    sessionMap = webBean.getSessionMap();

	    eligibilityStringArr = eligibleContent.split(STR_COMA);
	    for (String eligibleStr : eligibilityStringArr) {
		if (sessionMap.containsKey(eligibleStr) && StringUtils.isNotBlank(sessionMap
			    .get(eligibleStr))) {
			count++;
		}

	    }
	}
	if (count == 2) {
	    String pageName = webBean.getFolder();
		if (StringUtils.isNotBlank(pageName)) {
		    pageName = StringUtils.substringAfter(pageName,
			    QUOTE_AND_BUY_SLASH);

		    if (pageName.equalsIgnoreCase(PRODUCT_OPTIONS)
			    || pageName.equalsIgnoreCase(LIFESTYLE_QUESTIONS)
			    || pageName.equalsIgnoreCase(COVERAGE_OPTIONS)
			    || pageName
				    .equalsIgnoreCase(PRODUCT_OPTIONS_CHANGE_COVERAGE)
			    || pageName
				    .equalsIgnoreCase(COVERAGE_OPTIONS_CHANGE_COVERAGE)) {
		    	if(SAVED_QUOTE_EXCEPTION.equalsIgnoreCase(webBean.getNextScreenId())){
		    		return SKIP_BODY;		
		    	}
			return EVAL_BODY_INCLUDE;
		    } else {
			return SKIP_BODY;
		    }
		}


	}
	return SKIP_BODY;
    }

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
	eligibleContent = null;
    }

}

