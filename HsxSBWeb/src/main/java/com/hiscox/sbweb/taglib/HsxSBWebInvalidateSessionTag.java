package com.hiscox.sbweb.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebQuoteAndBuyControllerUtil;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebInvalidateSessionTag implements Tag, IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;

    public PageContext getContext() {
	return this.pc;
    }

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    /**
     *
     * @param formContent
     * @return SKIP_BODY
     */
    public int doStartTag() throws JspException {
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	HttpServletRequest requestObject = (HttpServletRequest) pc.getRequest();
	HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(requestObject,
		webBean);
	return SKIP_BODY;

    }

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;

    }

}

