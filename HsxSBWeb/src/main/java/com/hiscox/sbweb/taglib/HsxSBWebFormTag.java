package com.hiscox.sbweb.taglib;

import java.io.IOException;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:20 AM
 */
public class HsxSBWebFormTag implements Tag, IHsxSBWebConstants {

    private String formContent = STR_EMPTY;
    private PageContext pc = null;
    private Tag parent = null;
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public Tag getParent() {
	return parent;
    }

    public void setParent(Tag parent) {
	this.parent = parent;
    }

    public HsxSBWebFormTag() {

    }

    public void setPageContext(PageContext pc) {
	this.pc = pc;
    }

    public String getFormContent() {
	return formContent;
    }

    public void setFormContent(String formContent) {
	this.formContent = formContent;
    }

    /**
     * To return form content based on current page folder.
     *
     * @param string
     * @return skip body constant
     */
    public int doStartTag() {

	String renderCoponent = STR_EMPTY;
	String folderName = STR_EMPTY;
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	if (null != webBean.getFolder()) {

	    folderName = webBean.getFolder();

	    if (formContent.equalsIgnoreCase(STR_START)) {
		if (!(QUOTE_AND_BUY + STR_FORWARD_SLASH + SAVE_QUOTE)
			.equalsIgnoreCase(folderName)
			&& !(QUOTE_AND_BUY + STR_FORWARD_SLASH + RETRIEVE_A_QUOTE)
				.equalsIgnoreCase(folderName)) {

		    renderCoponent = HTML_FORM_CONTENT;
		}
	    } else if (formContent.equalsIgnoreCase(STR_END) && !(QUOTE_AND_BUY + STR_FORWARD_SLASH + SAVE_QUOTE)
				.equalsIgnoreCase(folderName)
				&& !(QUOTE_AND_BUY + STR_FORWARD_SLASH + RETRIEVE_A_QUOTE)
					.equalsIgnoreCase(folderName)) {
		    renderCoponent = HTML_FORM_END;
		
	    }
	}
	try {
	    pc.getOut().write(renderCoponent);
	} catch (IOException e) {
	    logger.error(SBWEB, ERROR, STR_ERROR_MESSAGE + e.getMessage());
	}

	return SKIP_BODY;
    }

    public int doEndTag() {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
	formContent = null;
    }

}

