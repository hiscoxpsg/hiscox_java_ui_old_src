package com.hiscox.sbweb.taglib;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebNavigationManager;
import com.hiscox.sbweb.util.HsxSBWebPageBeanUtil;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebCheckProductEligibilityTag implements Tag,
	IHsxSBWebConstants {

    private Tag parent = null;
    private String productContent = STR_EMPTY;

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    public String getProductContent() {
	return productContent;
    }

    public void setProductContent(String productContent) {
	this.productContent = productContent;
    }

    /**
     *
     * @param formContent
     * @return skip body constant
     */
    public int doStartTag() throws JspException {

	String[] eligibilityStringArr = null;

	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	Map<String, HsxSBWebPageRuleBean> navigationRuleMap = HsxSBWebNavigationManager
		.getInstance().getNavigationMap();
	HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap.get(webBean
		.getFolder());

	Map<String, String> sessionMap = new HashMap<String, String>();

	if (STR_ALL.equalsIgnoreCase(productContent)) {
	    return EVAL_BODY_INCLUDE;
	}
	if (!HsxSBWebPageBeanUtil.isMapEmpty(webBean.getSessionMap())) {
	    sessionMap = webBean.getSessionMap();
	    eligibilityStringArr = productContent.split(STR_COMA);

	    for (String eligString : eligibilityStringArr) {
		if (sessionMap.containsKey(eligString)) {
		    if (pageRuleBean != null
			    && HOME_PAGE_TYPE.equalsIgnoreCase(pageRuleBean
				    .getPageType())) {
			return EVAL_BODY_INCLUDE;
		    } else if (STR_YES.equalsIgnoreCase(sessionMap
			    .get(eligString))) {
			return EVAL_BODY_INCLUDE;
		    }

		}

	    }
	}
	if (!sessionMap.isEmpty() && StringUtils.isBlank(sessionMap.get(STATE))) {
	    return EVAL_BODY_INCLUDE;
	}

	return SKIP_BODY;
    }

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	parent = null;
	productContent = null;
    }

    public void setPageContext(PageContext arg) {
	// TODO Auto-generated method stub

    }
}

