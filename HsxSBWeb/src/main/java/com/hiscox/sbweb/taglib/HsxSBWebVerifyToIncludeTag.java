package com.hiscox.sbweb.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebVerifyToIncludeTag implements Tag, IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;

    public PageContext getContext() {
	return this.pc;
    }

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    /**
     *
     * @param formContent
     * @return SKIP_BODY or EVAL_BODY_INCLUDE
     * @throws JspException
     */
    public int doStartTag() throws JspException {
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	String pageType = webBean.getPageType();
	String pageName = webBean.getFolder();
	if (StringUtils.isNotBlank(pageType)
		&& (STATIC_ERROR_PAGE_TYPE.equalsIgnoreCase(pageType) || STATIC_PAGE_TYPE
			.equalsIgnoreCase(pageType))
		|| pageName.startsWith(QUOTE_AND_BUY_SLASH + READ_MORE)) {
	    return SKIP_BODY;
	} else {
	    return EVAL_BODY_INCLUDE;
	}

    }
/**
 * @return EVAL_PAGE
 * @throws JspException
 */
    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
    }

}

