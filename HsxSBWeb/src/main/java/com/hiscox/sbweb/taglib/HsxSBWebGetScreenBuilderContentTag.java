package com.hiscox.sbweb.taglib;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebPageBeanUtil;
import com.hiscox.sbweb.util.HsxSBWebQuoteAndBuyControllerUtil;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;
import com.hiscox.sbweb.util.HsxSBWebTextManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebGetScreenBuilderContentTag implements Tag,
	IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;
    private String questionContent = null;
    private String actionContent = null;
    private String trackingContent = null;
    public  static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public String getTrackingContent() {
	return trackingContent;
    }

    public void setTrackingContent(String trackingContent) {
	this.trackingContent = trackingContent;
    }

    public String getActionContent() {
	return actionContent;
    }

    public void setActionContent(String actionContent) {
	this.actionContent = actionContent;
    }

    public String getQuestionContent() {
	return questionContent;
    }

    public void setQuestionContent(String questionContent) {
	this.questionContent = questionContent;
    }

    public PageContext getContext() {
	return this.pc;
    }

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    /**
     *
     * @param formContent
     * @return skip body constant
     */
    public int doStartTag() throws JspException {
	String questionRenderContent = STR_EMPTY;
	String actionRenderContent = STR_EMPTY;
	String trackingRenderContent = STR_EMPTY;
	boolean excepionOccuredFlag = false;
	HsxSBUIBuilderControllerImpl uiBuilder = new HsxSBUIBuilderControllerImpl();
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	String pageName = webBean.getFolder();
	logger.info(SBWEB, INFO, "Jsp Execution Started For " + pageName
		+ " Page");
	logger.info(SBWEB, INFO,
		"GetScreenBuilderContent Tag Excecution Started For :"
			+ pageName + " Page");
	// long qsContTime;
	// long acContTime;
	// long tcContTime;

	try {

	    if (STATIC_ERROR_PAGE_TYPE.equalsIgnoreCase(webBean.getPageType())
		    || STATIC_PAGE_TYPE.equalsIgnoreCase(webBean.getPageType())
		    || pageName.startsWith(QUOTE_AND_BUY_SLASH + READ_MORE)) {
		// logger.info(SBWEB, INFO,"Page Type in verify tag:::"
		// + webBean.getPageType());
		uiBuilder = null;
		return SKIP_BODY;
	    } else  {
		String isJSEnabled = webBean.getIsJSEnabled();
		Map<String, String> sessionMap = webBean.getSessionMap();
		Map<String, String> additionalInfoMap = webBean
			.getAdditionalInfoMap();
		Properties widgetTypeProperties = (Properties) HsxSBWebResourceManager
			.getResource(STR_WIDGET_TYPE_PROPERTIES);
		Map<String, HsxSBTextManagerVO> textItemsMap = HsxSBWebTextManager
			.getInstance().textItemsMap;
		Document xmlDocument = null;
		// Question Content start
		if (POPUP_PAGE_TYPE.equalsIgnoreCase(webBean.getPageType())) {
		    questionContent = webBean.getPopUpQuestionsContent();
		    actionContent = webBean.getPopUpActionsContent();
		    trackingContent = webBean.getPopUpTrackingContent();

		}
		// long qsContStart;
		// long qsContEnd;
		boolean pageValidationFlag = webBean
			.getPageValidationFailedFlag();
		logger.info(SBWEB, INFO, pageName
			+ " Page Validated Failed Flag ----> :"
			+ pageValidationFlag);
		if (pageValidationFlag) {
		    // Above Condition says that the page Validation has failed
		    if (webBean.getWidget() != null
			    && !POPUP_PAGE_TYPE.equalsIgnoreCase(webBean
				    .getPageType())) {
			// Above Condition says that, since you have already
			// rendered the page, you have your main widget created
			// already
			// and there is no need to create this widget again so
			// get the rendered content with the help of the
			// existing widget
			// qsContStart =
			// Calendar.getInstance().getTimeInMillis();
			logger
				.info(SBWEB, INFO,
					"Rendered Content Prepared with the Existing Widget");
			questionRenderContent = uiBuilder
				.getHtmlContent(webBean.getWidget(), webBean
					.getUiContentType());
			// qsContEnd = Calendar.getInstance().getTimeInMillis();
			// qsContTime = qsContEnd - qsContStart;
		    } else {
			// Create new widgets from the response xml and then
			// form the render content
			// qsContStart =
			// Calendar.getInstance().getTimeInMillis();
			xmlDocument = getDocumentFromScreenContent(questionContent);
			logger
				.info(SBWEB, INFO,
					"Rendered Content Prepared with the New Widget with page validation");
			questionRenderContent = uiBuilder.generateHTML(
				xmlDocument, textItemsMap, isJSEnabled, webBean
					.getUiContentType(),
				widgetTypeProperties, additionalInfoMap,
				sessionMap, webBean.getPreviousScreen());
			// qsContEnd = Calendar.getInstance().getTimeInMillis();
			// qsContTime = qsContEnd - qsContStart;
			if (POPUP_PAGE_TYPE.equalsIgnoreCase(webBean
				.getPageType())) {
			    webBean.setPopUpWidget(uiBuilder.getWidget());
			    webBean.setPopUpWidgetMap(uiBuilder.getWidgetMap());
			} else {
			    webBean.setWidget(uiBuilder.getWidget());
			    webBean.setWidgetMap(uiBuilder.getWidgetMap());
			}
		    }

		} else {
		    // Create new widgets from the response xml and then
		    // form the render content
		    // qsContStart = Calendar.getInstance().getTimeInMillis();
		    xmlDocument = getDocumentFromScreenContent(questionContent);
		    logger
			    .info(SBWEB, INFO,
				    "Rendered Content Prepared with the New Widget without page validation");
		    questionRenderContent = uiBuilder.generateHTML(xmlDocument,
			    textItemsMap, isJSEnabled, webBean
				    .getUiContentType(), widgetTypeProperties,
			    additionalInfoMap, sessionMap, webBean
				    .getPreviousScreen());
		    // qsContEnd = Calendar.getInstance().getTimeInMillis();
		    // qsContTime = qsContEnd - qsContStart;
		    if (POPUP_PAGE_TYPE.equalsIgnoreCase(webBean.getPageType())) {
			webBean.setPopUpWidget(uiBuilder.getWidget());
			webBean.setPopUpWidgetMap(uiBuilder.getWidgetMap());
		    } else {
			webBean.setWidget(uiBuilder.getWidget());
			webBean.setWidgetMap(uiBuilder.getWidgetMap());
		    }
		}

		logger.info(SBWEB, INFO, "Rendererd component :"
			+ questionRenderContent);
		if (webBean.isBroucherValidationFlag()) {
		    logger.info(SBWEB, INFO,
			    "Redirecting from html to jsp page and Broucher validation flag is : "
				    + webBean.isBroucherValidationFlag());

		    Map<String, String> broucherReqMap = webBean
			    .getBroucherReqMap();
		    Map<String, HsxSBWidget> broucherWidgetMap = (HashMap<String, HsxSBWidget>) webBean
			    .getWidgetMap();
		    Collection<String> reqKeySet = broucherReqMap.keySet();
		    Iterator<String> keyIterator = reqKeySet.iterator();
		    while (keyIterator.hasNext()) {
			String key = (String) keyIterator.next();
			if (!HsxSBWebPageBeanUtil.isMapEmpty(broucherReqMap)
				&& broucherWidgetMap.containsKey(key)) {
			    String answerValue = broucherReqMap.get(key);
			    broucherWidgetMap.get(key).getWidgetInfo()
				    .setSavedValue(answerValue);
			    logger.info(SBWEB, INFO, "Key  :: "
				    + key
				    + "AnswerValue  :: "
				    + broucherWidgetMap.get(key)
					    .getWidgetInfo().getSavedValue());
			    if (!PRIMARY_BUSINESS_EXPLAIN_KEY
				    .equalsIgnoreCase(key)
				    && HsxSBUtil.isBlank(answerValue)) {
				enableErrorIndicator(broucherWidgetMap, key);
			    } else if (PRIMARY_BUSINESS_EXPLAIN_KEY
				    .equalsIgnoreCase(key)
				    && NONE_OF_THE_ABOVE_KEY
					    .equalsIgnoreCase(broucherReqMap
						    .get(PRIMARY_BUSINESS_CODE)) && HsxSBUtil.isBlank(broucherReqMap.get(key))) {
				    enableErrorIndicator(broucherWidgetMap, key);
			    }
			}
		    }
		    webBean.setWidgetMap(broucherWidgetMap);

		    questionRenderContent = uiBuilder.getHtmlContent(webBean
			    .getWidget(), webBean.getUiContentType());
		    webBean.setBroucherValidationFlag(false);
		}

		webBean.setQuestionRenderContent(questionRenderContent);

		// Action Content Starts

		// long acContStart;
		// long acContEnd;
		// acContStart = Calendar.getInstance().getTimeInMillis();
		xmlDocument = getDocumentFromScreenContent(actionContent);
		actionRenderContent = uiBuilder.generateActionListHTML(
			xmlDocument, textItemsMap, webBean.getUiContentType(),
			widgetTypeProperties, sessionMap, isJSEnabled);
		// acContEnd = Calendar.getInstance().getTimeInMillis();
		// acContTime = (acContEnd - acContStart);
		webBean.setActionRenderContent(actionRenderContent);

		// Tracking Content starts

		// long tcContStart;
		// long tcContEnd;
		// tcContStart = Calendar.getInstance().getTimeInMillis();
		xmlDocument = getDocumentFromScreenContent(trackingContent);
		trackingRenderContent = uiBuilder
			.generateFloodLightsHTML(xmlDocument);
		webBean.setTrackingRenderContent(trackingRenderContent);
		// tcContEnd = Calendar.getInstance().getTimeInMillis();
		// tcContTime = (tcContEnd - tcContStart);
		// logger
		// .info(
		// SBWEB,
		// INFO,
		// "Time Taken for UI ScreenBuilder Widgets(Question + Action + Tracking) Preparation :"
		// + (qsContTime + acContTime + tcContTime)
		// * 0.001 + " Seconds");

		// Generating Meta Tag
		// if (StringUtils.isNotEmpty(webBean.getFolderType()))
		// {
		// StringBuilder renderMetaTagContent = generateMetaTag();
		// if (renderMetaTagContent != null
		// && QUOTE_AND_BUY.equalsIgnoreCase(webBean
		// .getFolderType()))
		// {
		// pc.getOut().write(renderMetaTagContent.toString());
		// }
		// }
	    }
	} catch (HsxSBUIBuilderRuntimeException e) {
	    excepionOccuredFlag = true;
	    logger.error("HsxSBUIBuilderRuntimeException", e);
	    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
		    webBean, "HsxSBUIBuilderRuntimeException");
	} catch (HsxSBUIBuilderException e) {
	    excepionOccuredFlag = true;
	    logger.error("HsxSBUIBuilderException", e);
	    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
		    webBean, "HsxSBUIBuilderException");
	} catch (Exception e) {
	    excepionOccuredFlag = true;
	   logger.error("Exception", e);
	    HsxSBWebPageBeanUtil.logException(webBean, e);
	}
	logger.info(SBWEB, INFO, "Has Exception Occured in ScreenBuilder :"
		+ excepionOccuredFlag);
	if (excepionOccuredFlag) {
	    // redirect to the System Error Page
	    // logger.info(SBWEB, INFO,"Throwing new Exception");
	    logger
		    .info(
			    SBWEB,
			    INFO,
			    "Throwing a Runtime Exception in GetScreenBuilderContent Tag Which will handled by a Web Container and that will redirect to a System Error Page");
	    // clear the session...
	    webBean.setInvalidateSessionFlag(true);
	    HttpServletRequest request = (HttpServletRequest) pc.getRequest();
	    HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
		    webBean);
	    throw new HsxSBUIBuilderRuntimeException();

	}
	logger.info(SBWEB, INFO,
		"GetScreenBuilderContent Tag Excecution Completed");
	uiBuilder = null;
	return SKIP_BODY;

    }

    /**
     * To validate static broacher ware page for the first time.
     *
     * @param broucherWidgetMap
     * @param key
     */
    private void enableErrorIndicator(
	    Map<String, HsxSBWidget> broucherWidgetMap, String key) {
	broucherWidgetMap.get(key).getWidgetInfo().setErrorIndicator(true);
	String questionStyle = broucherWidgetMap.get(key + QUESTION_ID_SUFFIX)
		.getWidgetInfo().getStyleHint();
	questionStyle = questionStyle.concat(EMPTY_SPACE + CSS_ERROR);
	broucherWidgetMap.get(key + QUESTION_ID_SUFFIX).getWidgetInfo()
		.setStyleHint(questionStyle);
	if (broucherWidgetMap.get(HOME_PAGE_TYPE) != null) {
	    broucherWidgetMap.get(HOME_PAGE_TYPE).getWidgetInfo()
		    .setErrorIndicator(true);
	} else if (broucherWidgetMap.get(PROFESSIONAL_LIABILITY_KEY) != null) {
	    broucherWidgetMap.get(PROFESSIONAL_LIABILITY_KEY).getWidgetInfo()
		    .setErrorIndicator(true);
	}
    }

    /**
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @return xmlDocument
     */
    private Document getDocumentFromScreenContent(String screenContent)
	    throws ParserConfigurationException, SAXException, IOException {
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	factory.setNamespaceAware(true);
	Document xmlDocument = null;
	DocumentBuilder builder = factory.newDocumentBuilder();
	if (StringUtils.isNotBlank(screenContent)) {
	    xmlDocument = builder.parse(new InputSource(new StringReader(
		    screenContent)));
	}
	return xmlDocument;
    }

    /**
     * @return EVAL_PAGE
     */

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
    }

}

