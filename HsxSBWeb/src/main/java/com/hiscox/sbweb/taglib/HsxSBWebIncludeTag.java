package com.hiscox.sbweb.taglib;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * Custom tag library class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebIncludeTag implements Tag, IHsxSBWebConstants {

    private PageContext pc = null;
    private Tag parent = null;
    private String sessionValue = null;

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public String getSessionValue() {
	return sessionValue;
    }

    public void setSessionValue(String sessionValue) {
	this.sessionValue = sessionValue;
    }

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    /**
     *
     * @param formContent
     * @return skip body constant
     */
    public int doStartTag() throws JspException {
	Map<String, String> sessionMap = new HashMap<String, String>();
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	if (StringUtils.isNotBlank(sessionValue)) {
	    sessionValue = sessionValue.trim();
	}
	if (null != webBean.getSessionMap()) {
	    sessionMap = webBean.getSessionMap();
	}
	String renderCoponent = STR_EMPTY;
	if (STR_QUOTE_REF_ID.equalsIgnoreCase(sessionValue)) {
	    if (StringUtils.isNotBlank(webBean.getQuoteRefID())) {
		renderCoponent = webBean.getQuoteRefID();
	    }
	} else if (STR_HELPTEXTLABEL.equalsIgnoreCase(sessionValue)) {
	    if (StringUtils.isNotBlank(webBean.getHelpTextLabel())) {
		renderCoponent = webBean.getHelpTextLabel();
	    }
	} else if (STR_HELPTEXTCONTENT.equalsIgnoreCase(sessionValue)) {
	    if (StringUtils.isNotBlank(webBean.getHelpTextContent())) {
		renderCoponent = webBean.getHelpTextContent();
	    }
	} else if (sessionMap.containsKey(sessionValue)) {
	    renderCoponent = sessionMap.get(sessionValue);
	}
	try {
	    if (renderCoponent == null) {
		renderCoponent = STR_EMPTY;
	    }
	    pc.getOut().write(renderCoponent);

	} catch (IOException e) {
	    logger.error(SBWEB, ERROR, STR_ERROR_MESSAGE + e.getMessage());
	}
	return SKIP_BODY;
    }

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
	sessionValue = null;
    }

}

