package com.hiscox.sbweb.bean;

import java.io.Serializable;

import com.hiscox.sbweb.constants.IHsxSBWebConstants;

public class HsxSBWebPageRuleBean implements IHsxSBWebConstants, Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = 5850221892665512097L;
    private String pageName = null;
    private String pageType = null;
    private boolean isChildPage = false;
    private boolean isLandingPage = false;
    private String parentPage = null;
    private String pageBarItem = null;
    private boolean isLoginRequired = false;
    private String folderName = null;
    private String pageCode = null;
    private String isEndOfJourneyPage = null;
    private String ismultipleForm = null;
    private String previousPageStatus = null;
    private String regularNavigationAllowed = STR_NO;
    private String isCalendarRequired = STR_NO;
    private String throbberPage = STR_NO;
    private String updateProducts = STR_NO;
    private String throbberFrontPage = null;
    private String throbberRearPage = null;

    public String getThrobberRearPage(){
	return throbberRearPage;
    }

    public void setThrobberRearPage(String throbberRearPage){
	this.throbberRearPage = throbberRearPage;
    }

    public String getThrobberFrontPage(){
	return throbberFrontPage;
    }

    public void setThrobberFrontPage(String throbberFrontPage){
	this.throbberFrontPage = throbberFrontPage;
    }

    public String getUpdateProducts(){
	return updateProducts;
    }

    public void setUpdateProducts(String updateProducts){
	this.updateProducts = updateProducts;
    }

    public String getThrobberPage(){
	return throbberPage;
    }

    public void setThrobberPage(String throbberPage){
	this.throbberPage = throbberPage;
    }

    public String getIsCalendarRequired(){
	return isCalendarRequired;
    }

    public void setIsCalendarRequired(String isCalendarRequired){
	this.isCalendarRequired = isCalendarRequired;
    }

    public String getRegularNavigationAllowed(){
	return regularNavigationAllowed;
    }

    public void setRegularNavigationAllowed(String regularNavigationAllowed){
	this.regularNavigationAllowed = regularNavigationAllowed;
    }

    public String getPreviousPageStatus(){
	return previousPageStatus;
    }

    public void setPreviousPageStatus(String previousPageStatus){
	this.previousPageStatus = previousPageStatus;
    }

    public String getIsmultipleForm(){
	return ismultipleForm;
    }

    public void setIsmultipleForm(String ismultipleForm){
	this.ismultipleForm = ismultipleForm;
    }

    public String getIsEndOfJourneyPage(){
	return isEndOfJourneyPage;
    }

    public void setIsEndOfJourneyPage(String isEndOfJourneyPage){
	this.isEndOfJourneyPage = isEndOfJourneyPage;
    }

    public String getPageCode(){
	return pageCode;
    }

    public void setPageCode(String pageCode){
	this.pageCode = pageCode;
    }

    public String getFolderName(){
	return folderName;
    }

    public void setFolderName(String folderName){
	this.folderName = folderName;
    }

    public String getPageBarItem(){
	return pageBarItem;
    }

    public void setPageBarItem(String pageBarItem){
	this.pageBarItem = pageBarItem;
    }

    public String getPageName(){
	return pageName;
    }

    public void setPageName(String pageName){
	this.pageName = pageName;
    }

    public String getPageType(){
	return pageType;
    }

    public void setPageType(String pageType){
	this.pageType = pageType;
    }

    public boolean isChildPage(){
	return isChildPage;
    }

    public void setChildPage(boolean isChildPage){
	this.isChildPage = isChildPage;
    }

    public boolean isLandingPage(){
	return isLandingPage;
    }

    public void setLandingPage(boolean isLandingPage){
	this.isLandingPage = isLandingPage;
    }

    public String getParentPage(){
	return parentPage;
    }

    public void setParentPage(String parentPage){
	this.parentPage = parentPage;
    }

    public boolean isLoginRequired(){
	return isLoginRequired;
    }

    public void setLoginRequired(boolean isLoginRequired){
	this.isLoginRequired = isLoginRequired;
    }

}
