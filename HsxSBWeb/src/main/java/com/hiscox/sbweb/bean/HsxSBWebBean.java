package com.hiscox.sbweb.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.hiscox.sb.framework.core.HsxSBWidget;

/**
 * This is the main and single bean which will be holding application values.
 * 
 * @author Cognizant
 * @version 1.0
 * @created 25-Mar-2010 6:22:19 AM
 */
public class HsxSBWebBean implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -4019051504115809023L;
    private HsxSBWidget widget;
    private String questionsContent;
    private String popUpQuestionsContent;
    private String trackingContent;
    private String popUpTrackingContent;
    private String actionsContent;
    private String popUpActionsContent;
    private String homeURL = null;
    private String requestedPage = null;
    private String folderType = null;
    private String folder = null;
    private Map<String, HsxSBWidget> widgetMap = null;
    private Map<String, HsxSBWidget> popUpWidgetMap = null;
    private Map<String, String> navigationMap = null;
    private Map<String, String> sessionMap = null;
    private String pagesVisited = null;
    private String currentPage = null;
    private String nextPage = null;
    private String requestType = null;
    private boolean serviceCallRequired = false;
    private String isJSEnabled = null;
    private int uiContentType = 1;
    private String sessionIdentifierValue = null;
    private String nextScreenId = null;
    private String quoteRefID = null;
    private String errMngtQuoteRefId = null;
    private boolean isExceptionOccured = false;
    private String requestedAction = null;
    private String requestXml = null;
    private String viewNameForProgressBar = null;
    private String viewName = null;
    private String fromPage = null;
    private String loginIndicator = null;
    private String ldapUserName = null;
    private String clientIP = null;
    private String checkBoxValidationFlag = null;
    private String radioButtonValidationFlag = null;
    private String helpTextLabel = null;
    private String helpTextContent = null;
    private Map<String, String> additionalInfoMap = null;
    private String userName = null;
    private String pageType = null;
    private HsxSBWidget popUpWidget;
    private boolean invalidateSessionFlag = false;
    private String pageBarItem = null;
    private String questionRenderContent = null;
    private String actionRenderContent = null;
    private String trackingRenderContent = null;
    public Map<String, String> actionMap = null;
    public Map<String, String> progressbarActionMap = null;
    public Map<String, String> encryptionActionMap = null;
    public String previousScreen = null;
    public String schemeID = null;
    public String groupValidationFlag = null;
    public String responseXml = null;
    private String defaultNavigator = null;
    private String accLckUserName = null;
    private boolean dynamicPageFlag = false;
    private boolean sessionExpiryFlag = false;
    private boolean isCalendarRequired = false;
    private String guidType = null;
    private boolean pageValidationFailedFlag = false;
    // This Id will always have a backup of HttpSession Id
    private String webBeanId = null;

    private Map<String, String> savedQuotesMap = null;
    private String savedQuoteId = null;

    private Map<String, String> screenActionMap = null;
    private boolean throbberIndicator = false;
    private Map<String, String> broucherReqMap = null;
    private boolean broucherValidationFlag = false;
    private boolean retrieveQuoteSessionExpiryFlag = false;
    private String hostURL = null;
    private String dataCashReference = null;
    private String returnedViewName = null;
    private boolean nonJSDataCashIndicator = false;
    private boolean nonJSDataCashCSSIncludeFlag = false;
    private String serviceCallStatus = null;
    private String partnerPaymentSuccessURL = null;
    private String partnerPaymentExpiryURL = null;
    public Map<String, String> partnerAndAgentMap = null;
    private String retrieveQuoteGUID = null;
    
    
    public String getRetrieveQuoteGUID() {
		return retrieveQuoteGUID;
	}

	public void setRetrieveQuoteGUID(String retrieveQuoteGUID) {
		this.retrieveQuoteGUID = retrieveQuoteGUID;
	}

	public Map<String, String> getPartnerAndAgentMap() {
		return partnerAndAgentMap;
	}

	public void setPartnerAndAgentMap(Map<String, String> partnerAndAgentMap) {
		this.partnerAndAgentMap = partnerAndAgentMap;
	}

	
	public String getPartnerPaymentSuccessURL() {
		return partnerPaymentSuccessURL;
	}

	public void setPartnerPaymentSuccessURL(String partnerPaymentSuccessURL) {
		this.partnerPaymentSuccessURL = partnerPaymentSuccessURL;
	}

	public String getPartnerPaymentExpiryURL() {
		return partnerPaymentExpiryURL;
	}

	public void setPartnerPaymentExpiryURL(String partnerPaymentExpiryURL) {
		this.partnerPaymentExpiryURL = partnerPaymentExpiryURL;
	}

    public String getServiceCallStatus(){
        return serviceCallStatus;
    }

    public void setServiceCallStatus(String serviceCallStatus){
        this.serviceCallStatus = serviceCallStatus;
    }

    public boolean isNonJSDataCashCSSIncludeFlag(){
        return nonJSDataCashCSSIncludeFlag;
    }

    public void setNonJSDataCashCSSIncludeFlag(boolean nonJSDataCashCSSIncludeFlag){
        this.nonJSDataCashCSSIncludeFlag = nonJSDataCashCSSIncludeFlag;
    }

    public String getReturnedViewName(){
        return returnedViewName;
    }

    public void setReturnedViewName(String returnedViewName){
        this.returnedViewName = returnedViewName;
    }

    public boolean isNonJSDataCashIndicator(){
        return nonJSDataCashIndicator;
    }

    public void setNonJSDataCashIndicator(boolean nonJSDataCashIndicator){
        this.nonJSDataCashIndicator = nonJSDataCashIndicator;
    }

    public String getDataCashReference(){
        return dataCashReference;
    }

    public void setDataCashReference(String dataCashReference){
        this.dataCashReference = dataCashReference;
    }

    public String getHostURL(){
        return hostURL;
    }

    public void setHostURL(String hostURL){
        this.hostURL = hostURL;
    }

    public boolean isRetrieveQuoteSessionExpiryFlag(){
	return retrieveQuoteSessionExpiryFlag;
    }

    public void setRetrieveQuoteSessionExpiryFlag(
	    boolean retrieveQuoteSessionExpiryFlag){
	this.retrieveQuoteSessionExpiryFlag = retrieveQuoteSessionExpiryFlag;
    }

    public boolean isBroucherValidationFlag(){
	return broucherValidationFlag;
    }

    public void setBroucherValidationFlag(boolean broucherValidationFlag){
	this.broucherValidationFlag = broucherValidationFlag;
    }

    public Map<String, String> getBroucherReqMap(){
	return broucherReqMap;
    }

    public void setBroucherReqMap(Map<String, String> broucherReqMap){
	this.broucherReqMap = broucherReqMap;
    }

    public boolean isThrobberIndicator(){
	return throbberIndicator;
    }

    public void setThrobberIndicator(boolean throbberIndicator){
	this.throbberIndicator = throbberIndicator;
    }

    public Map<String, String> getScreenActionMap(){
	if (screenActionMap == null){
	    screenActionMap = new HashMap<String, String>();
	}
	return screenActionMap;
    }

    public void setScreenActionMap(Map<String, String> screenActionMap){
	this.screenActionMap = screenActionMap;
    }

    public String getSavedQuoteId(){
	return savedQuoteId;
    }

    public void setSavedQuoteId(String savedQuoteId){
	this.savedQuoteId = savedQuoteId;
    }

    public Map<String, String> getSavedQuotesMap(){
	if (this.savedQuotesMap == null){
	    this.savedQuotesMap = new HashMap<String, String>();
	}
	return savedQuotesMap;
    }

    public void setSavedQuotesMap(Map<String, String> savedQuotesMap){
	this.savedQuotesMap = savedQuotesMap;
    }

    public String getWebBeanId(){
	return webBeanId;
    }

    public void setWebBeanId(String webBeanId){
	this.webBeanId = webBeanId;
    }

    public void setPageValidationFailedFlag(boolean pageValidationFailedFlag){
	this.pageValidationFailedFlag = pageValidationFailedFlag;
    }

    private boolean invalidActionPressed = false;

    public boolean isInvalidActionPressed(){
    	return invalidActionPressed;
    }

    public void setInvalidActionPressed(boolean invalidActionPressed){
	this.invalidActionPressed = invalidActionPressed;
    }

    public boolean getPageValidationFailedFlag(){
	return pageValidationFailedFlag;
    }

    public String getGuidType(){
	return guidType;
    }

    public void setGuidType(String guidType){
	this.guidType = guidType;
    }

    public boolean getCalendarRequired(){
	return isCalendarRequired;
    }

    public void setCalendarRequired(boolean isCalendarRequired){
	this.isCalendarRequired = isCalendarRequired;
    }

    public boolean getSessionExpiryFlag(){
	return sessionExpiryFlag;
    }

    public void setSessionExpiryFlag(boolean sessionExpiryFlag){
	this.sessionExpiryFlag = sessionExpiryFlag;
    }

    public boolean getDynamicPageFlag(){
	return dynamicPageFlag;
    }

    public void setDynamicPageFlag(boolean dynamicPageFlag){
	this.dynamicPageFlag = dynamicPageFlag;
    }

    public String getAccLckUserName(){
	return accLckUserName;
    }

    public void setAccLckUserName(String accLckUserName){
	this.accLckUserName = accLckUserName;
    }

    public String getErrMngtQuoteRefId(){
	return errMngtQuoteRefId;
    }

    public void setErrMngtQuoteRefId(String errMngtQuoteRefId){
	this.errMngtQuoteRefId = errMngtQuoteRefId;
    }

    public String getDefaultNavigator(){
	return defaultNavigator;
    }

    public void setDefaultNavigator(String defaultNavigator){
	this.defaultNavigator = defaultNavigator;
    }

    public String getResponseXml(){
	return responseXml;
    }

    public void setResponseXml(String responseXml){
	this.responseXml = responseXml;
    }

    public String getGroupValidationFlag(){
	return groupValidationFlag;
    }

    public void setGroupValidationFlag(String groupValidationFlag){
	this.groupValidationFlag = groupValidationFlag;
    }

    public String getSchemeID(){
	return schemeID;
    }

    public void setSchemeID(String schemeID){
	this.schemeID = schemeID;
    }

    public String getPreviousScreen(){
	return previousScreen;
    }

    public void setPreviousScreen(String previousScreen){
	this.previousScreen = previousScreen;
    }

    public Map<String, String> getEncryptionActionMap(){
	if (encryptionActionMap == null){
	    encryptionActionMap = new HashMap<String, String>();
	}
	return encryptionActionMap;
    }

    public void setEncryptionActionMap(Map<String, String> encryptionActionMap){
	this.encryptionActionMap = encryptionActionMap;
    }

    public Map<String, String> getProgressbarActionMap(){
	if (progressbarActionMap == null){
	    progressbarActionMap = new HashMap<String, String>();
	}
	return progressbarActionMap;
    }

    public void setProgressbarActionMap(Map<String, String> progressbarActionMap){
	this.progressbarActionMap = progressbarActionMap;
    }

    public Map<String, String> getActionMap(){
	if (actionMap == null){
	    actionMap = new HashMap<String, String>();
	}
	return actionMap;
    }

    public void setActionMap(Map<String, String> actionMap){
	this.actionMap = actionMap;
    }

    public String getQuestionRenderContent(){
	return questionRenderContent;
    }

    public void setQuestionRenderContent(String questionRenderContent){
	this.questionRenderContent = questionRenderContent;
    }

    public String getActionRenderContent(){
	return actionRenderContent;
    }

    public void setActionRenderContent(String actionRenderContent){
	this.actionRenderContent = actionRenderContent;
    }

    public String getTrackingRenderContent(){
	return trackingRenderContent;
    }

    public void setTrackingRenderContent(String trackingRenderContent){
	this.trackingRenderContent = trackingRenderContent;
    }

    public String getPageBarItem(){
	return pageBarItem;
    }

    public void setPageBarItem(String pageBarItem){
	this.pageBarItem = pageBarItem;
    }

    public boolean isInvalidateSessionFlag(){
	return invalidateSessionFlag;
    }

    public void setInvalidateSessionFlag(boolean invalidateSessionFlag){
	this.invalidateSessionFlag = invalidateSessionFlag;
    }

    public HsxSBWidget getPopUpWidget(){
	return popUpWidget;
    }

    public void setPopUpWidget(HsxSBWidget popUpWidget){
	this.popUpWidget = popUpWidget;
    }

    public String getPageType(){
	return pageType;
    }

    public void setPageType(String pageType){
	this.pageType = pageType;
    }

    public String getUserName(){
	return userName;
    }

    public void setUserName(String userName){
	this.userName = userName;
    }

    public Map<String, String> getAdditionalInfoMap(){
	return additionalInfoMap;
    }

    public void setAdditionalInfoMap(Map<String, String> additionalInfoMap){
	this.additionalInfoMap = additionalInfoMap;
    }

    public String getHelpTextLabel(){
	return helpTextLabel;
    }

    public void setHelpTextLabel(String helpTextLabel){
	this.helpTextLabel = helpTextLabel;
    }

    public String getPopUpTrackingContent(){
	return popUpTrackingContent;
    }

    public void setPopUpTrackingContent(String popUpTrackingContent){
	this.popUpTrackingContent = popUpTrackingContent;
    }

    public String getHelpTextContent(){
	return helpTextContent;
    }

    public void setHelpTextContent(String helpTextContent){
	this.helpTextContent = helpTextContent;
    }

    public String getPopUpQuestionsContent(){
	return popUpQuestionsContent;
    }

    public void setPopUpQuestionsContent(String popUpQuestionsContent){
	this.popUpQuestionsContent = popUpQuestionsContent;
    }

    public String getPopUpActionsContent(){
	return popUpActionsContent;
    }

    public void setPopUpActionsContent(String popUpActionsContent){
	this.popUpActionsContent = popUpActionsContent;
    }

    public Map<String, HsxSBWidget> getPopUpWidgetMap(){
	return popUpWidgetMap;
    }

    public void setPopUpWidgetMap(Map<String, HsxSBWidget> popUpWidgetMap){
	this.popUpWidgetMap = popUpWidgetMap;
    }

    public String getCheckBoxValidationFlag(){
	return checkBoxValidationFlag;
    }

    public void setCheckBoxValidationFlag(String checkBoxValidationFlag){
	this.checkBoxValidationFlag = checkBoxValidationFlag;
    }

    public String getClientIP(){
	return clientIP;
    }

    public void setClientIP(String clientIP){
	this.clientIP = clientIP;
    }

    public String getLdapUserName(){
	return ldapUserName;
    }

    public void setLdapUserName(String ldapUserName){
	this.ldapUserName = ldapUserName;
    }

    public String getLoginIndicator(){
	return loginIndicator;
    }

    public void setLoginIndicator(String loginIndicator){
	this.loginIndicator = loginIndicator;
    }

    public String getFromPage(){
	return fromPage;
    }

    public void setFromPage(String fromPage){
	this.fromPage = fromPage;
    }

    public String getViewName(){
	return viewName;
    }

    public void setViewName(String viewName){
	this.viewName = viewName;
    }

    public String getViewNameForProgressBar(){
	return viewNameForProgressBar;
    }

    public void setViewNameForProgressBar(String viewNameForProgressBar){
	this.viewNameForProgressBar = viewNameForProgressBar;
    }

    public String getRequestXml(){
	return requestXml;
    }

    public void setRequestXml(String requestXml){
	this.requestXml = requestXml;
    }

    public String getRequestedAction(){
	return requestedAction;
    }

    public void setRequestedAction(String requestedAction){
	this.requestedAction = requestedAction;
    }

    public boolean isExceptionOccured(){
	return isExceptionOccured;
    }

    public void setExceptionOccured(boolean isExceptionOccured){
	this.isExceptionOccured = isExceptionOccured;
    }

    public String getQuoteRefID(){
	return quoteRefID;
    }

    public void setQuoteRefID(String quoteRefID){
	this.quoteRefID = quoteRefID;
    }

    public String getNextScreenId(){
	return nextScreenId;
    }

    public void setNextScreenId(String nextScreenId){
	this.nextScreenId = nextScreenId;
    }

    public String getSessionIdentifierValue(){
	return sessionIdentifierValue;
    }

    public void setSessionIdentifierValue(String sessionIdentifierValue){
	this.sessionIdentifierValue = sessionIdentifierValue;
    }

    public int getUiContentType(){
	return uiContentType;
    }

    public void setUiContentType(int uiContentType){
	this.uiContentType = uiContentType;
    }

    public String getIsJSEnabled(){
	return isJSEnabled;
    }

    public void setIsJSEnabled(String isJSEnabled){
	this.isJSEnabled = isJSEnabled;
    }

    public boolean isServiceCallRequired(){
	return serviceCallRequired;
    }

    public void setServiceCallRequired(boolean serviceCallRequired){
	this.serviceCallRequired = serviceCallRequired;
    }

    public String getRequestType(){
	return requestType;
    }

    public void setRequestType(String requestType){
	this.requestType = requestType;
    }

    public String getNextPage(){
	return nextPage;
    }

    public void setNextPage(String nextPage){
	this.nextPage = nextPage;
    }

    public String getCurrentPage(){
	return currentPage;
    }

    public void setCurrentPage(String currentPage){
	this.currentPage = currentPage;
    }

    public String getPagesVisited(){
	return pagesVisited;
    }

    public void setPagesVisited(String pagesVisited){
	this.pagesVisited = pagesVisited;
    }

    public Map<String, String> getNavigationMap(){
	return navigationMap;
    }

    public void setNavigationMap(Map<String, String> navigationMap){
	this.navigationMap = navigationMap;
    }

    public Map<String, HsxSBWidget> getWidgetMap(){
	return widgetMap;
    }

    public void setWidgetMap(Map<String, HsxSBWidget> widgetMap){
	this.widgetMap = widgetMap;
    }

    public String getFolder(){
	return folder;
    }

    public void setFolder(String folder){
	this.folder = folder;
    }

    public String getFolderType(){
	return folderType;
    }

    public void setFolderType(String folderType){
	this.folderType = folderType;
    }

    public String getRequestedPage(){
	return requestedPage;
    }

    public void setRequestedPage(String requestedPage){
	this.requestedPage = requestedPage;
    }

    public String getHomeURL(){
	return homeURL;
    }

    public void setHomeURL(String homeURL){
	this.homeURL = homeURL;
    }

    public String getQuestionsContent(){
	return questionsContent;
    }

    public void setQuestionsContent(String questionsContent){
	this.questionsContent = questionsContent;
    }

    public String getTrackingContent(){
	return trackingContent;
    }

    public void setTrackingContent(String trackingContent){
	this.trackingContent = trackingContent;
    }

    public String getActionsContent(){
	return actionsContent;
    }

    public void setActionsContent(String actionsContent){
	this.actionsContent = actionsContent;
    }

    public HsxSBWidget getWidget(){
	return widget;
    }

    /**
     * 
     * @param widget
     */
    public void setWidget(HsxSBWidget widget){
	this.widget = widget;
    }

    public Map<String, String> getSessionMap(){
	return sessionMap;
    }

    public void setSessionMap(Map<String, String> sessionMap){
	this.sessionMap = sessionMap;
    }

    public String getRadioButtonValidationFlag(){
	return radioButtonValidationFlag;
    }

    public void setRadioButtonValidationFlag(String radioButtonValidationFlag){
	this.radioButtonValidationFlag = radioButtonValidationFlag;
    }

}

