package com.hiscox.sbweb.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * Servlet Filter implementation class DisableUrlSessionFilter
 */
public class DisableUrlSessionFilter implements Filter {

    /**
     * Default constructor.
     */
    public DisableUrlSessionFilter() {
    }

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response,
	    FilterChain chain) throws IOException, ServletException {
	// skip non-http requests
	if (!(request instanceof HttpServletRequest)) {
	    chain.doFilter(request, response);
	    return;
	}

	HttpServletRequest httpRequest = (HttpServletRequest) request;
	HttpServletResponse httpResponse = (HttpServletResponse) response;

	// Redirect requests with JSESSIONID in URL to clean version
	// This is ONLY triggered if the request did not also contain a
	// JSESSIONID cookie!
	if (httpRequest.isRequestedSessionIdFromURL()) {
	    String url = httpRequest.getRequestURL().append(
		    httpRequest.getQueryString() != null ? "?"
			    + httpRequest.getQueryString() : "").toString();

	    // The url is clean, at least in Tomcat, which strips out the
	    // JSESSIONID path parameter automatically (Jetty does not?!)
	    httpResponse.setHeader("Location", url);
	    httpResponse.sendError(HttpServletResponse.SC_MOVED_PERMANENTLY);
	    return;
	}

	// Prevent rendering of JSESSIONID in URLs for all outgoing links
	HttpServletResponseWrapper wrappedResponse = new HttpServletResponseWrapper(
		httpResponse) {
	    @Override
	    public String encodeRedirectUrl(String url) {
		return url;
	    }

	    @Override
	    public String encodeRedirectURL(String url) {
		return url;
	    }

	    @Override
	    public String encodeUrl(String url) {
		return url;
	    }

	    @Override
	    public String encodeURL(String url) {
		return url;
	    }
	};

	// pass the request along the filter chain
	chain.doFilter(request, wrappedResponse);
    }

    /**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
    }

}

