package com.hiscox.sbweb.controller;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import org.xml.sax.SAXException;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.broker.HsxSBGetPageServiceBroker;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebNavigationManager;
import com.hiscox.sbweb.util.HsxSBWebPageBeanUtil;
import com.hiscox.sbweb.util.HsxSBWebQuoteAndBuyControllerUtil;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebBroacherwareController extends SimpleFormController
	implements IHsxSBWebConstants {

    private Map<String, HsxSBWebPageRuleBean> navigationRuleMap = null;
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();
    private Properties sessionMgrProperties = null;

    public void setSessionMgrProperties(Properties sessionMgrProperties) {
	this.sessionMgrProperties = sessionMgrProperties;
    }

    private Properties screenNameURLProperties = null;

    public void setScreenNameURLProperties(Properties screenNameURLProperties) {
	this.screenNameURLProperties = screenNameURLProperties;
    }

    private HsxSBGetPageServiceBroker webServiceBroker = null;

    public void setWebServiceBroker(HsxSBGetPageServiceBroker webServiceBroker) {
	this.webServiceBroker = webServiceBroker;
    }

    /**
     * @param request
     * @param webBean
     */
    private void populateSchemeID(HttpServletRequest request,
	    HsxSBWebBean webBean, String pageType) {
	if (BROACHER_TYPE.equalsIgnoreCase(pageType)) {
	    String schemeID = request.getParameter(STR_SCHEMEID_QUERY_STRING);
	    if (StringUtils.isBlank(schemeID)) {
		schemeID = STR_USDCWEB;
	    }
	    logger
		    .info(SBWEB, INFO,
			    "SchemeId set to webBean through BroacherType :"
				    + schemeID);
	    webBean.setSchemeID(schemeID);
	}
    }

    /**
     * @param webBean
     * @throws TransformerFactoryConfigurationError
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws HsxCoreException
     * @throws HsxCoreRuntimeException
     */
    private void invokeGetPageServiceForBroacherwarePage(HsxSBWebBean webBean,
	    String pageCode) throws TransformerFactoryConfigurationError,
	    TransformerException, ParserConfigurationException, SAXException,
	    IOException, HsxCoreException, HsxCoreRuntimeException {
	String screenXml;
	logger.info(SBWEB, INFO,
		"Session Identifier Sent to getPage Broacherware Page Request:"
			+ webBean.getSessionIdentifierValue());

	screenXml = webServiceBroker.invokeGetPageService(
		STR_BROACHERWARE_TYPE, null, pageCode, webBean
			.getSessionIdentifierValue());
	webBean.setQuestionsContent(screenXml);
	webBean.setActionsContent(screenXml);
	webBean.setTrackingContent(screenXml);
    }

    /**
     * @param webBean
     * @param session
     * @throws UnknownHostException
     */
    private void createSessionIdentifier(HsxSBWebBean webBean,
	    HttpSession session) throws UnknownHostException {
	if (StringUtils.isBlank(webBean.getSessionIdentifierValue())) {
	    HsxSBWebPageBeanUtil.resetWebBean(webBean);
	    StringBuilder sessionIdentifier = new StringBuilder(STR_EMPTY);
	    sessionIdentifier.append(session.getId());
	    sessionIdentifier.append(HsxSBWebPageBeanUtil
		    .generateUniqueSessionIdentifier());
	    logger.info(SBWEB, INFO,
		    "New Session Created with session Identifier in Broachurware-->:"
			    + sessionIdentifier);
	    webBean.setSessionIdentifierValue(sessionIdentifier.toString());
	}
    }

    /**
     * This method will be executed after form submission and before validation.
     * To handle custom binding this method retrieves the values from request
     * and set the map in USDC bean.
     *
     * @param request
     */
    /*
     * @Override protected boolean suppressBinding(HttpServletRequest request) {
     *
     * logger .info( SBWEB, INFO,
     * "---------------Supress Bindings Execution Started------------------------------"
     * );
     *
     * HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
     * .getResource(WEB_BEAN);
     *
     * Enumeration<String> paramNames = request.getParameterNames();
     * HashMap<String, String> requestValueMap = new HashMap<String, String>();
     * Map<String, String> sessionMap = new HashMap<String, String>();
     * sessionMap = HsxSBWebPageBeanUtil.getMapFromProperty(
     * sessionMgrProperties, sessionMap); webBean.setSavedQuoteId(null);
     *
     * while (paramNames.hasMoreElements()) { String questionName =
     * paramNames.nextElement(); if (HsxSBUtil.isNotBlank(questionName)) {
     * questionName = questionName.toString(); String answerValue =
     * request.getParameter(questionName); if
     * (sessionMap.containsKey(questionName)) { logger.info(SBWEB, INFO,
     * "QuestionName :" + questionName + " answerValue :" + answerValue);
     * requestValueMap.put(questionName, answerValue); } }
     *
     * }
     *
     * sessionMap.putAll(requestValueMap); webBean.setSessionMap(sessionMap);
     * webBean.setBroucherReqMap(requestValueMap); return false; }
     */
    /**
     * This is an overridden method which is called when the page gets submitted.
     *
     * @param request
     * @param response
     * @param response
     * @return ModelAndView
     */
    @SuppressWarnings("unchecked")
    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
	    HttpServletResponse response, Object command, BindException errors)
	    throws Exception {
    	 logger.info(SBWEB, INFO,
    			    "onSubmit in Broachurware-->:"
    				   );
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	String viewName = STR_EMPTY;
	String currentUrl = request.getRequestURI();
	String requestedPage = lookUpScreenName(currentUrl);
	// Clear the web session identifier whenever brochure ware pages are
	// visited in the existing session
	webBean.setSessionIdentifierValue(null);
	navigationRuleMap = HsxSBWebNavigationManager.getInstance()
		.getNavigationMap();
	HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap
		.get(requestedPage);
	HttpSession session = request.getSession(true);
	createSessionIdentifier(webBean, session);
	webBean.setWebBeanId(session.getId());
	Enumeration<String> paramNames = request.getParameterNames();
	Map<String, String> requestValueMap = new HashMap<String, String>();
	Map<String, String> sessionMap = new HashMap<String, String>();
	sessionMap = HsxSBWebPageBeanUtil.getMapFromProperty(
		sessionMgrProperties, sessionMap);
	webBean.setSavedQuoteId(null);

	requestValueMap = populateRequestValueMap(request, webBean, paramNames,
		requestValueMap, sessionMap);

	sessionMap.putAll(requestValueMap);
	webBean.setSessionMap(sessionMap);
	webBean.setBroucherReqMap(requestValueMap);

	if (requestedPage != null
		&& pageRuleBean != null
		&& HsxSBWebQuoteAndBuyControllerUtil
			.isRequestedPageValid(requestedPage)) {
	    requestedPage = requestedPage.trim();
	    String pageType = pageRuleBean.getPageType();
	    populateSchemeID(request, webBean, pageType);
	    webBean.setBroucherValidationFlag(false);
	    // check whether static broacher ware page is valid
	    checkForStaticErrors(webBean);
	    // if error exists execute below if block which will direct to
	    // standard error page
	    if (webBean.isBroucherValidationFlag()) {
		webBean.setFolder(BROACHER_WARE_ERROR_PAGE);
		webBean.setServiceCallRequired(true);
		webBean.setRequestType(STR_GET);
		// redirecting to standard broacher ware error page with
		// forward slash(help text working)
		return new ModelAndView(new RedirectView(STR_FORWARD_SLASH
			+ BROACHER_WARE_ERROR_PAGE + STR_FORWARD_SLASH, true));
	    } else {
		HsxSBWebQuoteAndBuyControllerUtil.getSessionValues(webBean
			.getSessionMap(), webBean);
		webBean.setClientIP(request.getRemoteAddr());
		webBean.setServiceCallRequired(true);
		webBean.setRequestType(STR_BROACHERWARE_TYPE);
		webBean.setPagesVisited(BROACHER_WARE_PAGE);

		invokeGetPageServiceForBroacherwarePage(webBean, pageRuleBean
			.getPageCode());
		requestedPage = webBean.getNextScreenId();
		viewName = getViewNameFromScreenId(viewName, requestedPage);
		webBean.setPagesVisited(BROACHER_WARE_PAGE + STR_COMA
			+ viewName);

		if (StringUtils.isBlank(viewName)) {
		    viewName = STR_EMPTY;
		}
		viewName = STR_FORWARD_SLASH + viewName;
	    }

	}
	//NSL Redirection
	return new ModelAndView(new RedirectView(webServiceBroker.getNSLRedirectionUrl()+requestValueMap.get("state")
			+"&cob="+requestValueMap.get("primarybusiness")+"&agentId="+requestValueMap.get("partnerAgent")
			+"&quoteOrigin="+requestValueMap.get("quoteOrigin")+"&restoreforms=false", true));
    }

	private Map<String, String> populateRequestValueMap(
	    HttpServletRequest request, HsxSBWebBean webBean,
	    Enumeration<String> paramNames,
	    Map<String, String> requestValueMap,
	    Map<String, String> sessionMap) {
	    HashMap<String, String> partnerMap = HsxSBWebPageBeanUtil.createHashMap(PARTNERS_AND_AGENT_ARRAY);	    
	while (paramNames.hasMoreElements()) {
	    String questionName = paramNames.nextElement();
	    if (HsxSBUtil.isNotBlank(questionName)) {
		if (questionName.startsWith(STR_ACTION)
			&& questionName.endsWith(STR_BUTTON)) {
		    if (WebUtils.hasSubmitParameter(request, questionName)) {
			String buttonPressed = questionName;
			String[] buttonNameArray = null;
			buttonNameArray = HsxSBWebPageBeanUtil.getStringTokens(
				buttonPressed, STR_UNDERSCORE);
			if (buttonNameArray.length >= 2) {
			    buttonPressed = buttonNameArray[1];
			    if (STR_PRINT.equalsIgnoreCase(buttonPressed)) {
				continue;
			    }
			    logger.info(SBWEB, INFO,
				    "requestedAction in BroacherwareController  : ---> "
					    + buttonPressed);

			    webBean.setRequestedAction(buttonPressed);
			}

		    }
		} else {
		    String answerValue = request.getParameter(questionName);
		    if (sessionMap.containsKey(questionName)) {
			logger
				.info(SBWEB, INFO, "QuestionName :"
					+ questionName + " answerValue :"
					+ answerValue);
			requestValueMap.put(questionName, answerValue);
			if(questionName.equalsIgnoreCase(SECONDARY_STREET) && StringUtils.isBlank(requestValueMap.get(questionName))){
	    		requestValueMap.remove(SECONDARY_STREET);
	    		}
		    } else {
		    if (partnerMap.get(questionName) != null) {
			   partnerMap.put(questionName, answerValue);
			  }		
			logger
			.info(SBWEB, INFO, "QuestionName not in sesssion:"
				+ questionName + " answerValue :"
				+ answerValue);		
		    }
		}
	    }
	    webBean.setPartnerAndAgentMap(partnerMap);
	}
	return requestValueMap;
    }

    /**
     * To enable indicator if any error exists.
     *
     * @param webBean
     */
    private void checkForStaticErrors(HsxSBWebBean webBean) {
	if (!HsxSBWebPageBeanUtil.isMapEmpty(webBean.getBroucherReqMap())) {
	    Map<String, String> brocherReqMap = webBean.getBroucherReqMap();
	    Collection<String> brocherReqKeySet = brocherReqMap.keySet();
	    Iterator<String> keyList = brocherReqKeySet.iterator();
	    while (keyList.hasNext()) {
		String key = (String) keyList.next();
		String answerValue = brocherReqMap.get(key);
		if (!PRIMARY_BUSINESS_EXPLAIN_KEY.equalsIgnoreCase(key)
			&& HsxSBUtil.isBlank(answerValue)) {
		    webBean.setBroucherValidationFlag(true);
		    break;
		} else if (PRIMARY_BUSINESS_EXPLAIN_KEY.equalsIgnoreCase(key)
			&& NONE_OF_THE_ABOVE_KEY.equalsIgnoreCase(brocherReqMap
				.get(PRIMARY_BUSINESS_CODE)) && HsxSBUtil.isBlank(brocherReqMap.get(key))) {
			webBean.setBroucherValidationFlag(true);
			break;
		    
		}

	    }

	}
    }

    /**
     * @param viewName
     * @param requestedPage
     * @return viewName
     */
    private String getViewNameFromScreenId(String viewName, String requestedPage) {
	if (screenNameURLProperties.containsKey(requestedPage)) {
	    viewName = screenNameURLProperties.getProperty(requestedPage);
	    logger.info(SBWEB, INFO, "ViewName Retrieved From Prop File :"
		    + viewName);
	} else {
	    logger.info(SBWEB, INFO, "Requested page: " + requestedPage
		    + " is not found in screenNameURLProperties");
	}
	return viewName;
    }

    /**
     * This method retrieves the requested page from the url.
     *
     * @param url
     * @return pageName
     */
    private String lookUpScreenName(String url) {
	logger.info(SBWEB, INFO, "Current URL Recieved " + url);
	url = removeLastCharecters(url, STR_FORWARD_SLASH);
	String[] tokens = url.split("/");
	String pageName = null;
	final int tokLen = tokens.length;
	int tokenLength = tokLen;
	if (tokenLength > 1) {
	    if (tokenLength == 2 || tokenLength == 3) {
		pageName = tokens[tokenLength - 1];
	    } else {
		StringBuilder requestedPageName = new StringBuilder();
		for (int i = 2; i < tokLen; i++) {
		    requestedPageName.append(tokens[i]);
		    requestedPageName.append(STR_FORWARD_SLASH);
		}
		pageName = requestedPageName.toString();
	    }

	}

	if (pageName != null && pageName.contains(STR_QUESTION_SYMBOL)) {
	    String[] pageTokens = pageName.split(STR_QUESTION_REG_EXP);
	    pageName = pageTokens[0];
	}
	pageName = removeLastCharecters(pageName, STR_FORWARD_SLASH);
	logger.info(SBWEB, INFO, "Current URL Modified :" + pageName);
	return pageName;
    }

    private String removeLastCharecters(String url, String seperator) {
	if (StringUtils.isNotBlank(url)) {
	    while (url.endsWith(seperator)) {
		url = StringUtils.substringBeforeLast(url, seperator);
	    }
	}
	return url;
    }

}

