package com.hiscox.sbweb.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebNavigationManager;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebProgressBarFormController extends SimpleFormController
	implements IHsxSBWebConstants {
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    /**
     * Retrieve a backing object for the current form from the given request.
     * Override this method for custom implementation.
     *
     * @param request
     * @return webBean
     */
    protected Object formBackingObject(HttpServletRequest request) {

	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	String requestedPage = lookUpScreenName(request.getRequestURI());
	webBean.setFolder(requestedPage);
	Map<String, HsxSBWebPageRuleBean> navigationRuleMap = null;
	navigationRuleMap = HsxSBWebNavigationManager.getInstance()
		.getNavigationMap();
	HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap.get(requestedPage
		.trim());
	String folderType = pageRuleBean.getFolderName();

	webBean.setFolderType(folderType);
	String frontPage = pageRuleBean.getThrobberFrontPage();
	String rearPage = pageRuleBean.getThrobberRearPage();
	String requestType = webBean.getRequestType();
	if (StringUtils.isBlank(requestType)) {
	    requestType = STR_GET;
	}

	logger.info(SBWEB, INFO, "RequestType :" + requestType);
	String nextScreenId = webBean.getNextScreenId();
	if (StringUtils.isNotBlank(nextScreenId)) {
	    if (STR_GET.equalsIgnoreCase(requestType)) {
		updateThrobberIndicator(webBean, rearPage, nextScreenId);
	    } else if (STR_POST.equalsIgnoreCase(requestType)) {
		updateThrobberIndicator(webBean, frontPage, nextScreenId);
	    }
	} else {
	    webBean.setThrobberIndicator(false);
	}

	logger.info(SBWEB, INFO,
		"The requested page in the progress bar controller "
			+ requestedPage);
	logger.info(SBWEB, INFO,
		"The from page in the progress bar controller "
			+ webBean.getFromPage());
	return webBean;
    }

    private static void updateThrobberIndicator(HsxSBWebBean webBean,
	    String pageName, String nextScreenId) {
	if (StringUtils.isNotBlank(pageName)) {
	    String[] tokens = pageName.split(",");
	    final int tokLen = tokens.length;
	    for (int i = 0; i < tokLen; i++) {
		pageName = tokens[i];
		if (StringUtils.isNotBlank(nextScreenId)
			&& nextScreenId.equalsIgnoreCase(pageName.trim())) {
		    webBean.setThrobberIndicator(true);
		    break;
		} else {
		    webBean.setThrobberIndicator(false);
		    logger.info(SBWEB, INFO,
			    "Invalid Action taken using Throbber Page");
		}
	    }
	}
    }

    /**
     * This is an overridden method which is called when the page gets submitted.
     *
     * @param request
     * @param response
     * @param response
     * @return modelAndView
     */
    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
	    HttpServletResponse response, Object command, BindException errors) {

	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	webBean.setPreviousScreen(lookUpScreenName(request.getRequestURI()));
	String viewName = webBean.getViewName();

	logger.info(SBWEB, INFO, "Relative Url View Name :" + viewName);
	if (STR_EMPTY.equalsIgnoreCase(viewName)) {
	    logger.info(SBWEB, INFO, "Redirecting to the Home Page:");
	    webBean.setRequestType(STR_GET);
	} else {
	    webBean.setRequestType(STR_POST);
	}
	viewName = STR_FORWARD_SLASH + viewName;
	ModelAndView modelAndView = null;
	modelAndView = new ModelAndView(new RedirectView(viewName, true));
	return modelAndView;

    }

    /**
     * This method retrieves the requested page from the url.
     *
     * @param url
     * @return pageName
     */
    private String lookUpScreenName(String url) {
	logger.info(SBWEB, INFO, "Current URL Recieved " + url);
	url = removeLastCharecters(url, STR_FORWARD_SLASH);
	String[] tokens = url.split("/");
	String pageName = null;
	final int tokLen = tokens.length;
	int tokenLength = tokLen;
	if (tokenLength > 1) {
	    if (tokenLength == 2 || tokenLength == 3) {
		pageName = tokens[tokenLength - 1];
	    } else {
		StringBuilder requestedPageName = new StringBuilder();
		for (int i = 2; i < tokLen; i++) {
		    requestedPageName.append(tokens[i]);
		    requestedPageName.append(STR_FORWARD_SLASH);
		}
		pageName = requestedPageName.toString();
	    }

	}

	if (pageName != null && pageName.contains(STR_QUESTION_SYMBOL)) {
	    String[] pageTokens = pageName.split(STR_QUESTION_REG_EXP);
	    pageName = pageTokens[0];
	}
	pageName = removeLastCharecters(pageName, STR_FORWARD_SLASH);
	logger.info(SBWEB, INFO, "Current URL Modified :" + pageName);
	return pageName;
    }

    private String removeLastCharecters(String url, String seperator) {
	if (StringUtils.isNotBlank(url)) {
	    while (url.endsWith(seperator)) {
		url = StringUtils.substringBeforeLast(url, seperator);
	    }
	}
	return url;
    }

    /**
     * This method retrieves the requested page type from the url.
     *
     * @param url
     * @return folderType
     */
    @SuppressWarnings("unused")
    private String getRequestedFolderType(String url) {
	url = removeLastCharecters(url, STR_FORWARD_SLASH);
	String[] tokens = url.split("/");
	String folderType = null;
	if (tokens.length > 3) {
	    folderType = tokens[2];
	}
	logger.info(SBWEB, INFO, "Folder Type :" + folderType);
	return folderType;
    }
}

