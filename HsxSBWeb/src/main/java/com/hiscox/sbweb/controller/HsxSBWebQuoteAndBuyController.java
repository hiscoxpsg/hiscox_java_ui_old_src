package com.hiscox.sbweb.controller;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.util.HsxCoreLDAPUtil;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.util.HsxSBResponseProcessor;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.broker.HsxSBGetPageServiceBroker;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebNavigationManager;
import com.hiscox.sbweb.util.HsxSBWebPageBeanUtil;
import com.hiscox.sbweb.util.HsxSBWebQuoteAndBuyControllerUtil;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebQuoteAndBuyController extends SimpleFormController
	implements IHsxSBWebConstants {

    private Map<String, HsxSBWebPageRuleBean> navigationRuleMap = null;
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();
    private Properties additionalInfoProperties = null;
    private Properties sessionMgrProperties = null;

    public void setSessionMgrProperties(Properties sessionMgrProperties) {
	this.sessionMgrProperties = sessionMgrProperties;
    }

    public void setAdditionalInfoProperties(Properties additionalInfoProperties) {
	this.additionalInfoProperties = additionalInfoProperties;
    }

    private Properties screenNameURLProperties = null;

    public void setScreenNameURLProperties(Properties screenNameURLProperties) {
	this.screenNameURLProperties = screenNameURLProperties;
    }

    private Properties appConfigProperties = null;

    public void setAppConfigProperties(Properties appConfigProperties) {
	this.appConfigProperties = appConfigProperties;
    }

    private HsxSBGetPageServiceBroker webServiceBroker = null;

    public void setWebServiceBroker(HsxSBGetPageServiceBroker webServiceBroker) {
	this.webServiceBroker = webServiceBroker;
    }

    /**
     * Retrieve a backing object for the current form from the given request.
     * Override this method for custom implementation.
     *
     * @param request
     * @return webBean
     */
    @Override
    protected Object formBackingObject(HttpServletRequest request) {


	// logger
	// .info(
	// SBWEB,
	// INFO,
	// "---------------Form Backing Object Execution Started------------------------------");
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	// Reset the retrieve Quote Session Expiry flag
	webBean.setRetrieveQuoteSessionExpiryFlag(false);
	// Check for Invalid Submits and Redirect User to the Current Page

	logger.info(SBWEB, INFO,
		"Is Invalid Action Pressed in Form Backing Object:"
			+ webBean.isInvalidActionPressed());

	if (webBean.isInvalidActionPressed()) {
	    logger
		    .info(
			    SBWEB,
			    INFO,
			    "---------------Form Backing Object Execution Ended because of invalid Action Submitt------------------------------");
	    return webBean;
	}

	String currentUrl = request.getRequestURI();
    
	/**US32240 -DEF: HPPv2 -Card Validation Error - Issues with Payment on payment with invalid card data*/
    //To redirect service returned view in DataCash IFrame 
    if ((webBean.isNonJSDataCashIndicator() || STR_BUSINESS_NAK.equalsIgnoreCase(webBean.getServiceCallStatus()))
           && STR_NO.equalsIgnoreCase(webBean.getIsJSEnabled()) && STR_PAYMENT_DETAILS.equalsIgnoreCase(webBean.getNextScreenId())) {    	
           currentUrl = STR_FORWARD_SLASH + SMALL_BUSINESS_INSURANCE + webBean.getReturnedViewName();
           logger.info(SBWEB, INFO, "currentUrl  in formBackingObject  in Fix for US32240 1st--> :"
        			+ currentUrl);
           webBean.setServiceCallStatus(null);
           webBean.setNonJSDataCashCSSIncludeFlag(true);
    }
    /**US32240 - fix end*/

	//To redirect service returned view in DataCash IFrame
	if ((webBean.isNonJSDataCashIndicator() || STR_BUSINESS_NAK.equalsIgnoreCase(webBean.getServiceCallStatus()))
			&& STR_NO.equalsIgnoreCase(webBean.getIsJSEnabled()) && CONFIRMATION.equalsIgnoreCase(webBean.getNextScreenId())) {
	    currentUrl = STR_FORWARD_SLASH + SMALL_BUSINESS_INSURANCE + webBean.getReturnedViewName();
	    webBean.setServiceCallStatus(null);
	    webBean.setNonJSDataCashCSSIncludeFlag(true);
	 }
	String requestedPage = lookUpScreenName(currentUrl);
	String requestedFolderType = getRequestedFolderType(currentUrl);
	webBean.setFolderType(requestedFolderType);
	logger.info(SBWEB, INFO, "Requested Page in formBackingObject --> :"
		+ requestedPage);
	logger.info(SBWEB, INFO, "Folder Type in formBackingObject --> :"
		+ webBean.getFolderType());
	webBean.setFolder(requestedPage);
	if (HsxSBWebQuoteAndBuyControllerUtil
		.isRequestedPageValid(requestedPage)
		&& (!requestedPage.startsWith(QUOTE_AND_BUY_SLASH + READ_MORE))) {
	    HttpSession session = request.getSession(true);
	    logger.info(SBWEB, INFO, "Remote Machine Ip Address :"
		    + request.getRemoteAddr());
	    webBean.setClientIP(request.getRemoteAddr());
	    Map<String, String> additionalInfoMap = new HashMap<String, String>();
	    HsxCoreLDAPUtil ldapsUtil = new HsxCoreLDAPUtil();

	    try {

		logger.info(SBWEB, INFO, "Requested Page -------> :"
			+ requestedPage);
		HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap
			.get(requestedPage);
		
		if (pageRuleBean != null) {
		    String pageType = pageRuleBean.getPageType();
		    String requestType = getRequestType(request);
		    String pageName = pageRuleBean.getPageName();
		    
		    // reset calendar required flag...
		    webBean.setCalendarRequired(false);
		    logger.info(SBWEB, INFO,
			    "Page Rule Bean Is Calendar Required  "
				    + pageRuleBean.getIsCalendarRequired());
		    if (STR_YES.equalsIgnoreCase(pageRuleBean
			    .getIsCalendarRequired())) {
			webBean.setCalendarRequired(true);
		    }

		    populateInvalidSessionFlag(webBean, pageRuleBean);
		    webBean.setPageType(pageRuleBean.getPageType());

		    webBean.setFolder(requestedPage);
		    if (!STATIC_ERROR_PAGE_TYPE.equalsIgnoreCase(pageType)
			    && !STATIC_PAGE_TYPE.equalsIgnoreCase(pageType)) {

			logger.info(SBWEB, INFO,
				"Session Identifier Before Setting :"
					+ webBean.getSessionIdentifierValue());
			createSessionIdentifier(webBean, session);
			populateSchemeID(request, webBean, pageType);
			populateAdditionalInfoMap(webBean, additionalInfoMap);
			session.setAttribute(WIDGET, webBean.getWidget());
			HsxSBWebQuoteAndBuyControllerUtil.retrieveSessionMap(
				webBean, sessionMgrProperties);
			invokeGetPageServiceForAcegiTargetUrl(request, webBean,
				pageRuleBean);

			authorizationUtil(request, webBean, session);

			processResetPasswordRequest(request, webBean,
				ldapsUtil, pageRuleBean.getPageCode());
			/*US15149 -Auto retrieve quote based on link: Start*/
			if(webBean.getRetrieveQuoteGUID()!=null && COVERAGE_OPTIONS.equalsIgnoreCase(pageName)
					&& STR_GET.equalsIgnoreCase(requestType)){ 
				
				pageType = HOME_PAGE_TYPE;
		      }
			/*US15149 -Auto retrieve quote based on link: End*/
			if (!STR_POST
				.equalsIgnoreCase(webBean.getRequestType())
				&& webBean.isServiceCallRequired()
				&& !STR_BROACHERWARE_TYPE
					.equalsIgnoreCase(webBean
						.getRequestType())) {

			    if (!POPUP_PAGE_TYPE.equalsIgnoreCase(pageType)) {
				HsxSBWebQuoteAndBuyControllerUtil
					.resetWebBeanXmlContent(webBean);
			    }

			    logger.info(SBWEB, INFO, "Page Type :" + pageType);
			    if (HOME_PAGE_TYPE.equalsIgnoreCase(pageType)) {
				populatePreviousScreen(request, webBean,
					pageRuleBean);

				invokeGetPageServiceForHomePage(webBean,
					pageRuleBean.getPageCode());
				if(SAVED_QUOTE_EXCEPTION.equalsIgnoreCase(webBean.getNextScreenId())){
				webBean.setInvalidateSessionFlag(true);
				}
			    } else if (POPUP_PAGE_TYPE.equalsIgnoreCase(pageType)) {
				invokeGetPageServiceForPopupPage(webBean,
					pageRuleBean);
			    } else {
				populatePreviousScreen(request, webBean,
					pageRuleBean);
				invokeGetPageServiceForGetRequest(webBean,
					pageRuleBean);

			    }

			}

		    } else {
			logger.info(SBWEB, INFO,
				"Either Static or Static-error occurred ::"
					+ pageType);
			if (HOME_PAGE_TYPE.equalsIgnoreCase(webBean
				.getNextScreenId())) {
			    logger
				    .info(SBWEB, INFO,
					    "Resetting previous screen to Landing page");
			    webBean.setPreviousScreen(requestedPage);
			}

			if (webBean.getDataCashReference() != null && (STR_NO.equalsIgnoreCase(webBean.getIsJSEnabled())) && !webBean.isNonJSDataCashIndicator()) {
			    //Changing Action based on DataCashReferenc received from SuccessURL from DataCash
			    if (STR_BUSINESS_NAK.equalsIgnoreCase(webBean.getServiceCallStatus())) {
				webBean.setDataCashReference(null);
			    }
			    if (STR_CONFIRM_PAYMENT_OPTIONS_ACTION.equalsIgnoreCase(webBean.getRequestedAction()) && webBean.getDataCashReference() != null) {
				webBean.setRequestedAction(STR_MAKE_PAYMENT_ACTION);
			    }
			  webBean.setNonJSDataCashIndicator(true);
	      	onSubmit(request, null, null, null);
			handleRequestInternal(request, null);
			}
		    }
		} else {

		    // check if page got Added Dynamically

		    if (webBean.getDynamicPageFlag()) {
			// create session identifier for the dynamically added
			// pages inside small-business-insurance
			createSessionIdentifier(webBean, session);
			populateSchemeID(request, webBean, HOME_PAGE_TYPE);
			populateAdditionalInfoMap(webBean, additionalInfoMap);
			HsxSBWebQuoteAndBuyControllerUtil.retrieveSessionMap(
				webBean, sessionMgrProperties);
			invokeGetPageServiceForHomePage(webBean,
				PROFESSIONAL_LIABILITY_HOME);
		    } else {
			// forward to page not found
			webBean.setFolderType(QUOTE_AND_BUY);
			webBean.setFolder(STR_404_ERROR_PAGE);
		    }
		}
		webBean.setRequestType(STR_GET);
		webBean.setExceptionOccured(false);
		// Logic of updating Session Map was moved from Custom Tag to
		// Controller.
		// Read the Response and update SessionMap Accordingly..
		HsxSBWebQuoteAndBuyControllerUtil.updateSessionMapWithProducts(
			webBean, pageRuleBean);



	    } catch (HsxCoreRuntimeException e) {
	    	logger.error("HsxCoreRuntimeException",e);
		

		HsxSBWebQuoteAndBuyControllerUtil
			.logUiExceptionToErrorManagement(webBean,
				"HsxCoreRuntimeException");
	    } catch (HsxCoreException e) {
	    	logger.error("HsxCoreException",e);

		HsxSBWebQuoteAndBuyControllerUtil
			.logUiExceptionToErrorManagement(webBean, e
				.getErrorDescription());
	    } catch (Exception e) {

	    logger.error("Exception",e);
		
		HsxSBWebPageBeanUtil.logException(webBean, e);

	    }
	}
	// reset the Page Validation Flag
	webBean.setPageValidationFailedFlag(false);
	return webBean;
    }

    /**
     * @param request
     * @param webBean
     */
    private void populatePreviousScreen(HttpServletRequest request,
	    HsxSBWebBean webBean, HsxSBWebPageRuleBean pageRuleBean) {
	if (pageRuleBean != null) {
	    String currentPage = lookUpScreenName(request.getRequestURI());
	    if (!STR_NO.equalsIgnoreCase(pageRuleBean.getPreviousPageStatus())) {
		webBean.setPreviousScreen(currentPage);
	    }
	}
    }

    /**
     * @param request
     * @param webBean
     */
    private void populateSchemeID(HttpServletRequest request,
	    HsxSBWebBean webBean, String pageType) {
	if (HOME_PAGE_TYPE.equalsIgnoreCase(pageType)) {
	    String schemeID = request.getParameter(STR_SCHEMEID_QUERY_STRING);
	    if (StringUtils.isBlank(schemeID)) {
		schemeID = STR_USDCWEB;
	    }
	    logger.info(SBWEB, INFO, "SchemeId read from the URL :" + schemeID);
	    webBean.setSchemeID(schemeID);
	}
    }

    /**
     * @param webBean
     * @param pageRuleBean
     * @throws TransformerFactoryConfigurationError
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws HsxCoreException
     * @throws HsxCoreRuntimeException
     */
    private void invokeGetPageServiceForGetRequest(HsxSBWebBean webBean,
	    HsxSBWebPageRuleBean pageRuleBean)
	    throws TransformerFactoryConfigurationError, TransformerException,
	    ParserConfigurationException, SAXException, IOException,
	    HsxCoreException, HsxCoreRuntimeException {
	String screenXml;
	logger.info(SBWEB, INFO, "PageName :" + pageRuleBean.getPageCode());
	logger.info(SBWEB, INFO,
		"Session Identifier Sent to getPage Service-Get Page Request:"
			+ webBean.getSessionIdentifierValue());
	screenXml = webServiceBroker.invokeGetPageService(STR_GET_REQUEST_TYPE,
		null, pageRuleBean.getPageCode(), webBean
			.getSessionIdentifierValue());
	webBean.setQuestionsContent(screenXml);
	webBean.setActionsContent(screenXml);
	webBean.setTrackingContent(screenXml);
    }

    /**
     * @param webBean
     * @param pageRuleBean
     * @throws TransformerFactoryConfigurationError
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws HsxCoreException
     * @throws HsxCoreRuntimeException
     */
    private void invokeGetPageServiceForPopupPage(HsxSBWebBean webBean,
	    HsxSBWebPageRuleBean pageRuleBean)
	    throws TransformerFactoryConfigurationError, TransformerException,
	    ParserConfigurationException, SAXException, IOException,
	    HsxCoreException, HsxCoreRuntimeException {
	logger.info(SBWEB, INFO,
		"Session Identifier Sent to getPage Service-Get Page Pop upRequest:"
			+ webBean.getSessionIdentifierValue());
	String nextScreenId = webBean.getNextScreenId();
	String popUpScreenXml = webServiceBroker.invokeGetPageService(
		STR_POP_UP_GET_REQUEST_TYPE, null, pageRuleBean.getPageCode(),
		webBean.getSessionIdentifierValue());
	// restore the next screen ID of the parent page
	webBean.setNextScreenId(nextScreenId);
	webBean.setPopUpQuestionsContent(popUpScreenXml);
	webBean.setPopUpActionsContent(popUpScreenXml);
	webBean.setPopUpTrackingContent(popUpScreenXml);
	webBean.setPopUpWidget(null);
    }

    /**
     * @param webBean
     * @throws TransformerFactoryConfigurationError
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws HsxCoreException
     * @throws HsxCoreRuntimeException
     */
    private void invokeGetPageServiceForHomePage(HsxSBWebBean webBean,
	    String pageCode) throws TransformerFactoryConfigurationError,
	    TransformerException, ParserConfigurationException, SAXException,
	    IOException, HsxCoreException, HsxCoreRuntimeException {
	String screenXml;
	logger.info(SBWEB, INFO,
		"Session Identifier Sent to getPage Service-Landing Page Request:"
			+ webBean.getSessionIdentifierValue());
	screenXml = webServiceBroker.invokeGetPageService(
		STR_LANDING_PAGE_TYPE, null, pageCode, webBean
			.getSessionIdentifierValue());
	webBean.setQuestionsContent(screenXml);
	webBean.setActionsContent(screenXml);
	webBean.setTrackingContent(screenXml);
    }

    /**
     * @param request
     * @param webBean
     * @param pageRuleBean
     * @param screenXml
     * @return
     * @throws TransformerFactoryConfigurationError
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws HsxCoreException
     * @throws HsxCoreRuntimeException
     */
    private void invokeGetPageServiceForAcegiTargetUrl(
	    HttpServletRequest request, HsxSBWebBean webBean,
	    HsxSBWebPageRuleBean pageRuleBean)
	    throws TransformerFactoryConfigurationError, TransformerException,
	    ParserConfigurationException, SAXException, IOException,
	    HsxCoreException, HsxCoreRuntimeException {
	Principal userPrincipal = request.getUserPrincipal();

	logger.info(SBWEB, INFO, "userPrincipal :" + userPrincipal);
	logger.info(SBWEB, INFO, "Is Service Call Required ---->:"
		+ webBean.isServiceCallRequired());
	logger.info(SBWEB, INFO, "Request Type :" + webBean.getRequestType());
	if (!STR_POST.equalsIgnoreCase(webBean.getRequestType())
		&& userPrincipal != null && webBean.isServiceCallRequired()) {
	    String pageCode = pageRuleBean.getPageCode();
	    logger.info(SBWEB, INFO, "Page Code Requested in the Acegi Call :"
		    + pageCode);
	    if (StringUtils.isNotBlank(pageCode)
		    && (QUOTE_SAVED_ACEGI_PAGE.equalsIgnoreCase(pageCode) || SAVED_QUOTE_ACEGI_PAGE
			    .equalsIgnoreCase(pageCode))) {

		// temp Fix
		if (QUOTE_SAVED_ACEGI_PAGE.equalsIgnoreCase(pageCode)) {
		    webBean.setRequestedAction("SignIn");
		} else if (SAVED_QUOTE_ACEGI_PAGE.equalsIgnoreCase(pageCode)) {
		    webBean.setRequestedAction("RetrieveAQuote");
		}

		String userName = userPrincipal.getName();

		webBean.setLdapUserName(userName);
		webBean.setLoginIndicator(STR_YES);

		String xmlString = webBean.getQuestionsContent();
		HsxSBWebQuoteAndBuyControllerUtil
			.resetWebBeanXmlContent(webBean);
		logger.info(SBWEB, INFO, "PROCESSING ACEGI SERVICE CALL  For :"
			+ pageCode);
		String screenXml = null;
		screenXml = webServiceBroker.invokeGetPageService(
			STR_ACEGI_TARGET_URL_GET_REQUEST_TYPE, xmlString,
			pageCode, webBean.getSessionIdentifierValue());

		webBean.setServiceCallRequired(false);
		webBean.setQuestionsContent(screenXml);
		webBean.setActionsContent(screenXml);
		webBean.setTrackingContent(screenXml);
	    }

	}
    }

    /**
     * @param webBean
     * @param additionalInfoMap
     */
    private void populateAdditionalInfoMap(HsxSBWebBean webBean,
	    Map<String, String> additionalInfoMap) {
	if (null == webBean.getAdditionalInfoMap()) {
	    additionalInfoMap = HsxSBWebPageBeanUtil.getMapFromProperty(
		    additionalInfoProperties, additionalInfoMap);
	    webBean.setAdditionalInfoMap(additionalInfoMap);
	}
    }

    /**
     * @param webBean
     * @param session
     * @throws UnknownHostException
     */
    private void createSessionIdentifier(HsxSBWebBean webBean,
	    HttpSession session) throws UnknownHostException {
	if (StringUtils.isBlank(webBean.getSessionIdentifierValue())) {
	    HsxSBWebPageBeanUtil.resetWebBean(webBean);
	    StringBuilder sessionIdentifier = new StringBuilder(STR_EMPTY);
	    sessionIdentifier.append(session.getId());
	    sessionIdentifier.append(HsxSBWebPageBeanUtil
		    .generateUniqueSessionIdentifier());
	    logger.info(SBWEB, INFO,
		    "New Session Created with session Identifier-->:"
			    + sessionIdentifier);
	    webBean.setSessionIdentifierValue(sessionIdentifier.toString());
	}
    }

    /**
     * @param webBean
     * @param pageRuleBean
     */
    private void populateInvalidSessionFlag(HsxSBWebBean webBean,
	    HsxSBWebPageRuleBean pageRuleBean) {
	String isEndOfJourneyPage = pageRuleBean.getIsEndOfJourneyPage();
	// logger.info(SBWEB, INFO,"isEndOfJourneyPage :"+isEndOfJourneyPage);
	webBean.setInvalidateSessionFlag(false);
	if (STR_YES.equalsIgnoreCase(isEndOfJourneyPage)) {
	    webBean.setInvalidateSessionFlag(true);
	}
	logger.info(SBWEB, INFO, "Invalidate Session Flag: "
		+ webBean.isInvalidateSessionFlag());
    }

    /**
     * @param request
     * @param webBean
     * @param ldapsUtil
     * @throws HsxCoreRuntimeException
     * @throws HsxCoreException
     */
    private void processResetPasswordRequest(HttpServletRequest request,
	    HsxSBWebBean webBean, HsxCoreLDAPUtil ldapsUtil, String pageCode)
	    throws HsxCoreRuntimeException, HsxCoreException {
	if (STR_RESET_PASSWORD.equalsIgnoreCase(pageCode)) {
	    String unlockAccountGuid = null;
	    String resetPasswordGuid = null;
	    unlockAccountGuid = request.getParameter(STR_UN_LOCK_ACCOUNT_GUID);
	    resetPasswordGuid = request.getParameter(STR_RESET_PASSWORD_GUID);
	    // Reset the User Name and GuidType
	    webBean.setUserName(null);
	    webBean.setGuidType(null);
	    if (unlockAccountGuid != null) {
		HsxSBWebPageBeanUtil.getUsernameFromLDAP(webBean,
			STR_UN_LOCK_ACCOUNT_GUID, unlockAccountGuid, ldapsUtil,
			appConfigProperties);
	    } else if (resetPasswordGuid != null) {
		HsxSBWebPageBeanUtil.getUsernameFromLDAP(webBean,
			STR_RESET_PASSWORD_GUID, resetPasswordGuid, ldapsUtil,
			appConfigProperties);
	    }
	}

    }

    /**
     * @param request
     * @param webBean
     * @param session
     * @throws HsxSBUIBuilderException
     */
    private void authorizationUtil(HttpServletRequest request,
	    HsxSBWebBean webBean, HttpSession session)
	    throws HsxSBUIBuilderException {
	String authFailed = request.getParameter(AUTHORIZATION_FAILED);
	if (authFailed != null && session != null && authFailed.equalsIgnoreCase(STR_TRUE)) {

		webBean.setPageValidationFailedFlag(true);
		HsxSBWebPageBeanUtil.checkBadCredentials(webBean, session);
		// No service call is required if the acegi fails
		webBean.setServiceCallRequired(false);
	    
	}
	// reset the Page Validation Flag
	webBean.setPageValidationFailedFlag(false);
    }

    /**
     * This method will be responsible for redirecting the user to Proper Error
     * Page in case of any service level exceptions.
     *
     * @param response
     * @param request
     * @return showForm
     */

    @Override
    protected ModelAndView showForm(HttpServletRequest request,
	    HttpServletResponse response, BindException errors)
	    throws Exception {
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	logger.info(SBWEB, INFO, "In the showForm isExceptionOccured Value :"
		+ webBean.isExceptionOccured());
	if (webBean.isExceptionOccured()) {
	    return new ModelAndView(HsxSBWebQuoteAndBuyControllerUtil
		    .getErrorPageUrl(request, webBean));
	} else {

	    if (STR_SYSTEM_ERROR.equalsIgnoreCase(webBean.getFolder())) {

		webBean.setInvalidateSessionFlag(true);
		HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
			webBean);
		logger
			.info(SBWEB, INFO,
				"Redirected to System Error Page in the Show Form Method");

	    }


	    /*//added
	    if (PAYMENT_TIME_OUT.equalsIgnoreCase(webBean.getFolder()))
	    {

		webBean.setInvalidateSessionFlag(true);
		HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
			webBean);
		logger
			.info(SBWEB, INFO,
				"Redirected to System Error Page in the Show Form Method");
	    }*/
	    logger.info(SBWEB, INFO, "Retrieve Quote Session Expiry Flag :"
		    + webBean.isRetrieveQuoteSessionExpiryFlag());
	    if (webBean.isRetrieveQuoteSessionExpiryFlag()) {
		HsxSBWebQuoteAndBuyControllerUtil
			.resetSessionParamters(webBean);
		return new ModelAndView(new RedirectView(SESSION_TIME_OUT_URL,
			true));
	    }
	    // On DataCash Payment in JS disabled, success folder is set
	    if (webBean.isNonJSDataCashIndicator()) {
		if (webBean.getReturnedViewName() != null) {
		webBean.setFolder(webBean.getReturnedViewName());
		}
		webBean.setNonJSDataCashIndicator(false);
	    }

	    return super.showForm(request, response, errors);
	}

    }

    /**
     * This method is responsible of passing control to either a configured
     * validator class or directly to onSubmitt method based on the action
     * requested by the user.
     *
     * @param response
     * @param request
     * @return boolean
     */

    @Override
    protected boolean suppressValidation(HttpServletRequest request,
	    Object command) {
	HsxSBWebBean webBean = null;
	if (command instanceof HsxSBWebBean) {
	    webBean = (HsxSBWebBean) command;
	}
	if (webBean != null) {
	    // reset the Page Validation Flag
	    webBean.setPageValidationFailedFlag(false);
	    // reset the invalid Action flag
	    webBean.setInvalidActionPressed(false);
	    String requestedAction = retrieveActionName(request);
	    if (StringUtils.isBlank(requestedAction)) {
		// if requested action is blank treat that the main buton is
		// pressed
		// below fix is made for IE in corner cases
		logger.info(SBWEB, INFO,
			"DefaultNavigator For the Current Page :"
				+ webBean.getDefaultNavigator());
		requestedAction = webBean.getDefaultNavigator();
	    }
	    logger
		    .info(SBWEB, INFO, "Requested Action set :"
			    + requestedAction);
	    webBean.setRequestedAction(requestedAction);
	    //Changing Action based on DataCashReferenc received from SuccessURL from DataCash
	    if (STR_CONFIRM_PAYMENT_OPTIONS_ACTION.equalsIgnoreCase(requestedAction) && webBean.getDataCashReference() != null) {
		webBean.setRequestedAction(STR_MAKE_PAYMENT_ACTION);
	    }
	    Map<String, String> actionMap = webBean.getActionMap();
	    Map<String, String> screenActionMap = webBean.getScreenActionMap();
	    if (actionMap != null && HsxSBUtil.isNotBlank(requestedAction)) {
		if (actionMap.containsKey(requestedAction)) {
		    if (STR_YES
			    .equalsIgnoreCase(actionMap.get(requestedAction))) {
			// reset the invalid Action flag
			webBean.setInvalidActionPressed(false);
			return false;
		    }
		} else if (!STR_RETRIEVE_A_QUOTE_BUTTON_NAME
			.equalsIgnoreCase(requestedAction)
			&& !STR_LOGOUT_BUTTON_NAME
				.equalsIgnoreCase(requestedAction)) {
		    if (screenActionMap != null
			    && screenActionMap.containsKey(requestedAction)) {
			webBean.setInvalidActionPressed(false);
		    } else {
			logger
				.info(SBWEB, INFO,
					"<------------Invalid Action Submitted by the User------------->");
			webBean.setInvalidActionPressed(true);
		    }

		}

	    }
	}
	return true;
    }

    /**
     * Handles two cases: form submissions and showing a new form. Delegates the
     * decision between the two to isFormSubmission, always treating requests
     * without existing form session attribute as new form when using session
     * form mode.
     *
     * @param response
     * @param request
     * @return handleInternalRequest
     * @throws Exception
     */
    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
	    HttpServletResponse response) throws Exception {
	System.out.println();
	// logger
	// .info(
	// SBWEB,
	// INFO,
	// "---------------Handle Internal Execution Started------------------------------");
	// logger.info("Has form submitted ---> :"+isFormSubmission(request));
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	String currentUrl = request.getRequestURI();
	//set default isJSEnabled to 'NO'
	if (!STR_YES.equalsIgnoreCase(webBean.getIsJSEnabled()) && !STR_NO.equalsIgnoreCase(webBean.getIsJSEnabled())) {
	    webBean.setIsJSEnabled(STR_NO);
	    logger.info(SBWEB, INFO, "isJSEnabled resetted to :" + webBean.getIsJSEnabled());
	}
	//To redirect service returned view in DataCash IFrame
	if ((webBean.isNonJSDataCashIndicator() || STR_BUSINESS_NAK.equalsIgnoreCase(webBean.getServiceCallStatus()))
		&& STR_NO.equalsIgnoreCase(webBean.getIsJSEnabled()) && STR_PAYMENT_DETAILS.equalsIgnoreCase(webBean.getNextScreenId())) {
		currentUrl = STR_FORWARD_SLASH + SMALL_BUSINESS_INSURANCE + webBean.getReturnedViewName();
	 }

	String requestedPage = lookUpScreenName(currentUrl);
	String requestedFolderType = getRequestedFolderType(currentUrl);
	webBean.setFolderType(requestedFolderType);
	webBean.setFolder(requestedPage);
	HttpSession session = request.getSession(true);
	webBean.setWebBeanId(session.getId());
	logger.info(SBWEB, INFO, "Pages Visited :" + webBean.getPagesVisited());
	logger
		.info(SBWEB, INFO,
			"Requested Page in handleInternalRequest --> :"
				+ requestedPage);
	logger.info(SBWEB, INFO, "Folder Type in handleInternalRequest --> :"
		+ requestedFolderType);

	// Reset the dynamic Page Flag
	webBean.setDynamicPageFlag(false);
	logger.info(SBWEB, INFO, "is Service Call Required :"
		+ webBean.isServiceCallRequired());
	if (HsxSBWebQuoteAndBuyControllerUtil
		.isRequestedPageValid(requestedPage)) {
	    // get the navigation rule map which contains the key as the
	    // pageName
	    // and the value it the object to the pageRuleBean
	    navigationRuleMap = HsxSBWebNavigationManager.getInstance()
		    .getNavigationMap();
	    String hostURL = HsxSBWebPageBeanUtil.getHostURL(request.getRequestURL().toString());
	    webBean.setHostURL(hostURL);
	    // reset Session Lost Flag
	    webBean.setSessionExpiryFlag(false);
	    if (requestedPage != null) {
		// based on the requestedPage get the corresponding pageRuleBean
		// from
		// the navigationRuleMap
		requestedPage = requestedPage.trim();
		HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap
			.get(requestedPage);
		if (pageRuleBean != null) {
		    if (StringUtils
			    .isBlank(webBean.getSessionIdentifierValue())) {
			webBean.setPagesVisited(null);
			HsxSBWebPageBeanUtil.resetWebBean(webBean);
			// set Session session lost flag to true so that in an
			// onSubmit method
			// we identify that use will have to be redirect user to
			// Home
			// Page directly
			logger.info(SBWEB, INFO, "Session Expiry Set to True");
			webBean.setSessionExpiryFlag(true);
		    }
		    String authFailed = request
			    .getParameter(AUTHORIZATION_FAILED);
		    HsxCoreLDAPUtil ldapsUtil = new HsxCoreLDAPUtil();

		    String pagesVisited = webBean.getPagesVisited();
		    // returns to the account locked page if the user tries to
		    // login
		    // with bad password for more that 4 times
		    if (authFailed != null
			    && authFailed.equalsIgnoreCase(STR_TRUE)) {
			ModelAndView acegiView = super.handleRequestInternal(
				request, response);
			webBean.setPageValidationFailedFlag(true);

			return HsxSBWebPageBeanUtil.checkAccountLocked(webBean,
				authFailed, ldapsUtil, request.getSession(),
				pagesVisited, acegiView, appConfigProperties);
		    } else {
			// reset the Page Validation Flag
			webBean.setPageValidationFailedFlag(false);
		    }
		    if (PAYMENT_TIME_OUT.equalsIgnoreCase(requestedPage)) {
			 String paymentException = PAYMENT_TIME_EXCEPTION;
			    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
				    webBean, paymentException);
			    webBean.setInvalidateSessionFlag(true);
			    HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
				    webBean);
		    }

		    return checkNavigation(request, response, webBean,
			    pageRuleBean, pagesVisited);

		} else {

		    if (!requestedPage.startsWith(QUOTE_AND_BUY
			    + STR_FORWARD_SLASH)) {
			logger.info(SBWEB, INFO, "Folder Type :"
				+ webBean.getFolderType());

			if (StringUtils.isNotBlank(webBean.getPagesVisited())) {
			    updatePageVisitedString(requestedPage, webBean,
				    webBean.getPagesVisited());
			} else {
			    webBean.setPagesVisited(requestedPage);
			}
			webBean.setDynamicPageFlag(true);

			// Redirect to dynamically added page at runtime
			// HsxSBWebQuoteAndBuyControllerUtil
			// .resetWebBeanXmlContent(webBean);
			webBean.setRequestedPage(requestedPage);
			// Get the absolute file path :- web-inf
			String filepath = HsxSBWebNavigationManager
				.getFileAbsolutePath();
			File file = new File(filepath
				+ STR_BACKWARD_SLASH_SYMBOL + STR_JSP
				+ STR_BACKWARD_SLASH_SYMBOL + requestedPage
				+ STR_BACKWARD_SLASH_SYMBOL + INDEX_JSP
				+ STR_DOT + STR_JSP);
			if (file.exists()) {
			    // To identify when a folder was added dynamically
			    // in
			    // view resolver.
			    logger.info(SBWEB, INFO,
				    "Dynamically added page found :: "
					    + requestedPage);
			    webBean.setFolder(STR_RUNTIMEADDED);

			    return super.handleRequestInternal(request,
				    response);
			} else {

			    webBean.setDynamicPageFlag(false);
			    response
				    .sendError(HttpServletResponse.SC_NOT_FOUND);
			    return super.handleRequestInternal(request,
				    response);
			}
		    } else {

			webBean.setDynamicPageFlag(false);
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return super.handleRequestInternal(request, response);
		    }
		}
	    } else {
		// Redirect to Page not found error page
		HsxSBWebQuoteAndBuyControllerUtil
			.resetWebBeanXmlContent(webBean);

		webBean.setDynamicPageFlag(false);
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
		return super.handleRequestInternal(request, response);
	    }

	} else {

	    return super.handleRequestInternal(request, response);
	}
    }

    /**
     * This method to check the navigation rules.
     *
     * @param request
     * @param response
     * @param webBean
     * @param pageRuleBean
     * @param pagesVisited
     * @return handleRequestInternal
     * @throws Exception
     */
    private ModelAndView checkNavigation(HttpServletRequest request,
	    HttpServletResponse response, HsxSBWebBean webBean,
	    HsxSBWebPageRuleBean pageRuleBean, String pagesVisited)
	    throws Exception {
	logger.info(SBWEB, INFO,
		"PageVisted String in Handle Request Internal  ---> :"
			+ pagesVisited);
	String pageType = pageRuleBean.getPageType();
	String pageName = pageRuleBean.getPageName();
	String pageBarItem = pageRuleBean.getPageBarItem();
	webBean.setPageType(pageType);
	webBean.setPageBarItem(pageBarItem);
	int requestParamSize = 0;
	if (request != null && request.getParameterMap() != null) {
	    requestParamSize = request.getParameterMap().size();
	}
	logger.info(SBWEB, INFO,
		"No of request Parameters in Handle Request Internal  ---> :"
			+ requestParamSize);
	String requestType = getRequestType(request);
	logger
		.info(SBWEB, INFO,
			"Request Type in Handle Request Internal  ---> :"
				+ requestType);

	/*US15149 -Auto retrieve quote based on link: End*/
		String retrieveguid = request.getParameter(STR_RETRIEVE_QUOTE_GUID);
		if (retrieveguid != null && COVERAGE_OPTIONS.equalsIgnoreCase(pageName)
				&& STR_GET.equalsIgnoreCase(requestType)) {
			logger.info(SBWEB, INFO, "Retrievequoteguid :" + retrieveguid);
			webBean.setRetrieveQuoteGUID(retrieveguid);
			pageType = HOME_PAGE_TYPE;
			webBean.setPageType(pageType);
		}
    /*US15149 -Auto retrieve quote based on link: End*/
	
	if (STATIC_PAGE_TYPE.equalsIgnoreCase(pageType)
		|| STATIC_ERROR_PAGE_TYPE.equalsIgnoreCase(pageType)) {
	    webBean.setServiceCallRequired(false);
	    logger.info(SBWEB, INFO, "static Page ::: " + pageName);
	    if (pageName.equalsIgnoreCase(DATACASH_CONFIRMATION)) {
		String dataCashReference = request.getParameter(STR_URL_DATACASH_REFERENCE);
		webBean.setDataCashReference(dataCashReference);
		logger.info("DataCash Reference ::" + dataCashReference);
		/*if (StringUtils.isNotBlank(webBean.getPagesVisited()))
		{
		    String requestedPage = webBean.getRequestedPage();
		    updatePageVisitedString(requestedPage, webBean,
			    webBean.getPagesVisited());
		}*/
	    }
	    return super.handleRequestInternal(request, response);
	} else if (HOME_PAGE_TYPE.equalsIgnoreCase(pageType)) {
	    webBean.setServiceCallRequired(true);
	    logger.info(SBWEB, INFO, "Home Page ::: " + pageName);
	    if (pagesVisited == null) {
		if (requestParamSize > 0
			&& STR_POST.equalsIgnoreCase(requestType)) {
		    // This will mean that user has submitted the page but still
		    // he has lost his page visited history. This case should be
		    // redirected to Session Timeout page
		    return new ModelAndView(new RedirectView(
			    SESSION_TIME_OUT_URL, true));
		}

		pagesVisited = pageName;
		// start of the navigation history string (pagesVisited)
		webBean.setPagesVisited(pagesVisited);
		return super.handleRequestInternal(request, response);
	    }
	} else if (LANDING_PAGE_TYPE.equalsIgnoreCase(pageType)) {
	    webBean.setServiceCallRequired(true);
	    logger.info(SBWEB, INFO, "landing Page ::: " + pageName);
	    if (pagesVisited == null) {
		return redirectToSessionExpiryPage(webBean, requestParamSize,
			requestType);
	    } else {
		if (!pagesVisited.contains(pageName)) {
		    pagesVisited = pagesVisited.concat(STR_COMA).concat(
			    pageName);
		}
		// start of the navigation history string (pagesVisited)
		webBean.setPagesVisited(pagesVisited);
		return super.handleRequestInternal(request, response);
	    }
	} else if (DYNAMIC_PAGE_TYPE.equalsIgnoreCase(pageType)) {
	    webBean.setServiceCallRequired(true);
	    logger.info(SBWEB, INFO, "dynamic Page ::: " + pageName);
	    logger.info(SBWEB, INFO, "Pages visited :: " + pagesVisited);
	    logger.info(SBWEB, INFO, "is child page :: "
		    + pageRuleBean.isChildPage());
	    // Start --On browser back of pay by phone
	    if (pagesVisited != null && pagesVisited.contains(PAY_BY_PHONE)
		    && STR_GET.equalsIgnoreCase(webBean.getRequestType())
		    && !(pagesVisited.contains(QUOTE_PASSED_TO_AGENT))
		    && !(PAY_BY_PHONE.contains(pageName))) {

		String quotePassedUrl = STR_FORWARD_SLASH
			+ QUOTE_PASSED_TO_AGENT + STR_FORWARD_SLASH;
		pagesVisited = pagesVisited.concat(STR_COMA).concat(
			QUOTE_PASSED_TO_AGENT);
		webBean.setPagesVisited(pagesVisited);
		return new ModelAndView(new RedirectView(quotePassedUrl, true));
	    }
	    // End --On browser back of pay by phone
	    if (pageRuleBean.isChildPage()) {
		if (pagesVisited == null) {
		    return redirectToSessionExpiryPage(webBean,
			    requestParamSize, requestType);
		}

		String[] parentPages = pageRuleBean.getParentPage().split("##");
		boolean present = false;
		for (String parent : parentPages) {
		    logger.info(SBWEB, INFO, "Parent Pages :: " + parent);
		    if (pagesVisited != null
			    && pagesVisited.contains(STR_COMA + parent)) {
			present = true;
			break;
		    }
		}

		if (!present
			&& STR_YES.equalsIgnoreCase(pageRuleBean
				.getRegularNavigationAllowed())
			&& (pagesVisited != null && pagesVisited
				.contains(pageName))) {
		    present = true;
		}
		if (!present) {
		    logger.info(SBWEB, INFO, "Redirecting to Error Page");
		    return new ModelAndView(HsxSBWebQuoteAndBuyControllerUtil
			    .redirectToInvalidPageUrl());
		}

	    } else {
		if (pagesVisited == null) {
		    return redirectToSessionExpiryPage(webBean,
			    requestParamSize, requestType);
		} else if (!pagesVisited.contains(pageName)) {
		    // redirect to invalid navigation page
		    HsxSBWebQuoteAndBuyControllerUtil
			    .resetWebBeanXmlContent(webBean);
		    return new ModelAndView(HsxSBWebQuoteAndBuyControllerUtil
			    .redirectToInvalidPageUrl());
		}
	    }
	} else if (POPUP_PAGE_TYPE.equalsIgnoreCase(pageType)) {
	    webBean.setServiceCallRequired(true);
	    logger.info(SBWEB, INFO, "dynamic popup Page ::: " + pageName);
	    String parentPage = pageRuleBean.getParentPage();
	    if (STR_GET.equalsIgnoreCase(webBean.getRequestType())
		    && pagesVisited.contains(STR_COMA + parentPage)) {
		return super.handleRequestInternal(request, response);
	    } else {
		// redirect to invalid navigation page
		HsxSBWebQuoteAndBuyControllerUtil
			.resetWebBeanXmlContent(webBean);
		return new ModelAndView(HsxSBWebQuoteAndBuyControllerUtil
			.redirectToInvalidPageUrl());
	    }
	}

	logger.info(SBWEB, INFO, "Page History after Navigation Check :-"
		+ webBean.getPagesVisited());
	return super.handleRequestInternal(request, response);
    }

    private ModelAndView redirectToSessionExpiryPage(HsxSBWebBean webBean,
	    int requestParamSize, String requestType) {
	logger.info(SBWEB, INFO,
		"Sesseion Identifier in Case of Session Expiry :"
			+ webBean.getSessionIdentifierValue());
	if (requestParamSize == 0 || STR_GET.equalsIgnoreCase(requestType)) {
	    logger
		    .info(SBWEB, INFO,
			    "Redirecting to Home Page as this was an invalid navigation");
	    // If the no of request parameters are equal to 0 then this
	    // is a Get request and hence redirect to home page
	    HsxSBWebQuoteAndBuyControllerUtil.resetSessionParamters(webBean);
	    return new ModelAndView(new RedirectView(STR_FORWARD_SLASH, true));
	} else {
	    logger
		    .info(
			    SBWEB,
			    INFO,
			    "Redirecting to Session timeout Page as the page  was submitted after the session expiry");
	    // If the no of request parameters are greater than 0 then
	    // this
	    // is a POST request and hence redirect to Session Timeout
	    // page
	    HsxSBWebQuoteAndBuyControllerUtil.resetSessionParamters(webBean);
	    return new ModelAndView(
		    new RedirectView(SESSION_TIME_OUT_URL, true));
	}
    }

    /**
     * This is an overridden method which is called when the page gets submitted.
     *
     * @param request
     * @param response
     * @param response
     * @return modelAndView
     */
    @Override
    protected ModelAndView onSubmit(HttpServletRequest request,
	    HttpServletResponse response, Object command, BindException errors) {


	ModelAndView modelAndView = null;
	String viewName = STR_SYSTEM_ERROR;
	String currentURL = null;
	currentURL = request.getRequestURL().toString();

	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	// Check for both WidgetMap and sessionLostFlag and redirectUser to
	// Home Page with new Session

	if (webBean.getWidgetMap() == null && webBean.getSessionExpiryFlag()) {
	    // Reset the User session if any in case of session expiry
	    logger.info(SBWEB, INFO,
		    "Sesseion Identifier in Case of Session Expiry :"
			    + webBean.getSessionIdentifierValue());
	    webBean.setInvalidateSessionFlag(true);
	    HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
		    webBean);
	    // Redirects User to Home page
	    modelAndView = new ModelAndView(new RedirectView(
		    SESSION_TIME_OUT_URL, true));
	    return modelAndView;
	}

	String currentPage = null;
	try {


	    // Check for Invalid Submits and Redirect User to the Current Page

	    logger.info(SBWEB, INFO, "Is Invalid Action Pressed :"
		    + webBean.isInvalidActionPressed());
	    if (webBean.isInvalidActionPressed()) {
		logger
			.info(SBWEB, INFO,
				"Redirect User to Current Page because of Invalid Submitts");
		viewName = STR_EMPTY;
		String requestedPage = webBean.getNextScreenId();


		if (StringUtils.isNotBlank(requestedPage)) {
		    viewName = getViewNameFromScreenId(viewName, requestedPage);
		}
		viewName = STR_FORWARD_SLASH + viewName;
		modelAndView = new ModelAndView(
			new RedirectView(viewName, true));
		logger
			.info(SBWEB, INFO,
				"---------------On Submit Execution Ended------------------------------");
		webBean.setRequestType(STR_POST);
		return modelAndView;
	    }

	    // Below logic is written to prevent hackers to view the un
	    // authorized quotes.
	    Map<String, String> savedQuoteMap = webBean.getSavedQuotesMap();
	    String savedQuoteId = webBean.getSavedQuoteId();
	    String requestedAction = webBean.getRequestedAction();
	    logger.info(SBWEB, INFO, "Next Screen Id :"
		    + webBean.getNextScreenId());

	    if (STR_SAVED_QUOTE.equalsIgnoreCase(webBean.getNextScreenId())
		    && !HsxSBWebPageBeanUtil.isMapEmpty(savedQuoteMap)
		    && StringUtils.isNotBlank(savedQuoteId)
		    && STR_RETRIEVE_A_QUOTE_BUTTON_NAME
			    .equalsIgnoreCase(requestedAction)) {

		logger.info(SBWEB, INFO, "No of Saved Quotes :"
			+ savedQuoteMap.size());
		logger.info(SBWEB, INFO, "Quote Ref Id selected by the user :"
			+ savedQuoteId);
		// HsxSBWebPageBeanUtil.printMap(savedQuoteMap);
		if (savedQuoteMap.containsValue(savedQuoteId)) {
		    logger
			    .info(SBWEB, INFO,
				    "Quote selected by the user is valid and can be retrieved");
		} else {
		    logger
			    .error(SBWEB, ERROR,
				    "Quote selected by the user is invalid and hence ending the user session");
		    webBean.setInvalidateSessionFlag(true);
		    HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(
			    request, webBean);
		    // Redirects User to Home page
		    modelAndView = new ModelAndView(new RedirectView(
			    STR_FORWARD_SLASH, true));
		    return modelAndView;
		}
	    }

	    logger.info(SBWEB, INFO, "In Submit currentURL:  :: " + currentURL);

	    Map<String, HsxSBWidget> widgetMap = webBean.getWidgetMap();
	    String screenXmlContent = webBean.getQuestionsContent();

	    Document document = HsxSBWebPageBeanUtil
		    .getDocumentFromString(screenXmlContent);
	    String requestXml = HsxSBResponseProcessor
		    .generateScreenQuestionXML(document, widgetMap,
			    requestedAction);
	    if (null != webBean.getSessionMap()) {
		HsxSBWebQuoteAndBuyControllerUtil.getSessionValues(webBean
			.getSessionMap(), webBean);
	    }
	    // logger.info(SBWEB, INFO,"requestXml :" + requestXml);
	    /*if (StringUtils.isNotBlank(requestXml))
	    {
		requestXml = HsxSBWebQuoteAndBuyControllerUtil
			.encryptCardPaymentData(requestXml, hsxSBWebEncryption,
				webBean.getEncryptionActionMap(),
				requestedAction);
	    }*/
	    HsxSBWebQuoteAndBuyControllerUtil.resetWebBeanXmlContent(webBean);
	    String screenXML = null;
	    currentPage = lookUpScreenName(request.getRequestURI());

	    webBean.setFromPage(currentPage);
	    webBean.setRequestXml(requestXml);
	    String isJSEnabled = webBean.getIsJSEnabled();
	    // String isJSEnabled = request.getParameter("isJSEnabled");
	    logger.info(SBWEB, INFO,
		    "The current page name in the controller : on submit  is : "
			    + currentPage);

	    HsxSBWebPageRuleBean currentPageRuleBean = navigationRuleMap
		    .get(currentPage);
	    populatePreviousScreen(request, webBean, currentPageRuleBean);
	    currentPageRuleBean = null;

	    Map<String, String> progressbarActionMap = webBean
		    .getProgressbarActionMap();

	    if (progressbarActionMap != null
		    && progressbarActionMap.containsKey(requestedAction)
		    && STR_YES.equalsIgnoreCase(progressbarActionMap
			    .get(requestedAction))
		    && STR_YES.equalsIgnoreCase(isJSEnabled)) {

		String currentScreenName = webBean.getNextScreenId();
		viewName = PROCESSING_YOUR_PAYMENT_URL;
		if (!STR_PAYMENT_DETAILS.equalsIgnoreCase(currentScreenName)) {
		    viewName = CALCULATING_YOUR_QUOTE_URL;
		}

	    } else {
		screenXML = webServiceBroker.invokeGetPageService(
			STR_NEXT_PAGE_REQUEST_TYPE, requestXml, null, webBean
				.getSessionIdentifierValue());
		webBean.setResponseXml(screenXML);
		webBean.setQuestionsContent(screenXML);
		webBean.setActionsContent(screenXML);
		webBean.setTrackingContent(screenXML);
		String requestedPage = webBean.getNextScreenId();
		viewName = getViewNameFromScreenId(viewName, requestedPage);

	    }

	    logger.info(SBWEB, INFO, "On Submitt Quote Ref Id :"
		    + webBean.getQuoteRefID());
	    //Injecting nonJS CSS in IFrame page after this action
	    webBean.setNonJSDataCashCSSIncludeFlag(false);
	    if (STR_CONFIRM_PAYMENT_OPTIONS_ACTION.equalsIgnoreCase(requestedAction)) {
		webBean.setNonJSDataCashCSSIncludeFlag(true);
	    }

	    if (StringUtils.isNotBlank(viewName)) {
		viewName = viewName.trim();
	    }
	    logger.info(SBWEB, INFO, "viewName :" + viewName);



	} catch (HsxCoreRuntimeException e) {
		viewName = STR_SYSTEM_ERROR;
		   logger.error("HsxCoreRuntimeException", e);
		    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
			    webBean, "HsxCoreRuntimeException");
		    webBean.setInvalidateSessionFlag(true);
		    HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
			    webBean);



	} catch (HsxCoreException e) {

	    // folderName = QUOTE_AND_BUY;

		viewName = STR_SYSTEM_ERROR;
		logger.error("HsxCoreException", e);
		    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
			    webBean, e.getErrorDescription());
		    webBean.setInvalidateSessionFlag(true);
		    HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
			    webBean);


	} catch (HsxSBUIBuilderRuntimeException e) {
	    // log it into log file
	    // return to error page
	    //viewName = STR_SYSTEM_ERROR;

		viewName = STR_SYSTEM_ERROR;
		logger.error("HsxSBUIBuilderRuntimeException", e);
		    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
			    webBean, "HsxSBUIBuilderRuntimeException");
		    webBean.setInvalidateSessionFlag(true);
		    HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
			    webBean);


	} catch (HsxSBUIBuilderException e) {
	    // log it into log file
	    // log it into error management service
	    // return to error page


		viewName = STR_SYSTEM_ERROR;
		logger.error("HsxSBUIBuilderException", e);
		    HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
			    webBean, "HsxSBUIBuilderException");
		    webBean.setInvalidateSessionFlag(true);
		    HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
			    webBean);


	} catch (Exception e) {

		viewName = STR_SYSTEM_ERROR;
		 logger.error("Exception",e);
		    HsxSBWebPageBeanUtil.logException(webBean, e);
		    webBean.setInvalidateSessionFlag(true);
		    HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request,
			    webBean);

	}
	viewName = STR_FORWARD_SLASH + viewName;
	//Set view name to retrieve in Show Form
	webBean.setReturnedViewName(viewName);
	logger.info(SBWEB, INFO, "Relative Url View Name :" + viewName);
	modelAndView = new ModelAndView(new RedirectView(viewName, true));

	viewName = StringUtils.substringAfter(viewName, STR_FORWARD_SLASH);
	String pagesVisited = webBean.getPagesVisited();
	if (pagesVisited != null) {

	    updatePageVisitedHistory(viewName, webBean, currentPage,
		    pagesVisited);
	}
	// to handle href link(dynamic page) in static home page on back submit
	if (HOME_PAGE_CODE.equalsIgnoreCase(webBean.getNextScreenId())) {
	    webBean.setRequestType(STR_GET);
	} else {
	    webBean.setRequestType(STR_POST);
	}
	// reset the Page Validation Flag
	webBean.setPageValidationFailedFlag(false);

	return modelAndView;

    }

    /**
     * @param viewName
     * @param requestedPage
     * @return viewName
     */
    private String getViewNameFromScreenId(String viewName, String requestedPage) {
	logger.info(SBWEB, INFO, "Requested Page ---> :"
		    + requestedPage);
	if (screenNameURLProperties.containsKey(requestedPage)) {
	    viewName = screenNameURLProperties.getProperty(requestedPage);
	    logger.info(SBWEB, INFO, "ViewName Retrieved From Prop File :"
		    + viewName);
	} else {
	    logger.info(SBWEB, INFO, "Requested page: " + requestedPage
		    + " is not found in screenNameURLProperties");
	}
	return viewName;
    }

    /**
     * @param viewName
     * @param webBean
     * @param currentPage
     * @param pagesVisited
     */
    private void updatePageVisitedHistory(String viewName,
	    HsxSBWebBean webBean, String currentPage, String pagesVisited) {
	updatePageVisitedString(viewName, webBean, pagesVisited);
	if (StringUtils.isNotBlank(currentPage)) {
	    HsxSBWebPageRuleBean currentPageRuleBean = navigationRuleMap
		    .get(currentPage);

		if (currentPageRuleBean != null && currentPageRuleBean.isChildPage()
			&& DYNAMIC_PAGE_TYPE
				.equalsIgnoreCase(currentPageRuleBean
					.getPageType())
			&& !STR_YES.equalsIgnoreCase(currentPageRuleBean
				.getRegularNavigationAllowed())) {
		    // Add the Current Page to PageHistory in the Case if
		    // that
		    // particular page being the Child Page
		    logger.info(SBWEB, INFO,
			    "The current page name set to PageVisited History : "
				    + currentPage);

		    updatePageVisitedStringForChildPage(currentPage, webBean,
			    pagesVisited);
		}
	    
	}
    }

    /**
     * @param viewName
     * @param webBean
     * @param pagesVisited
     */
    private void updatePageVisitedString(String viewName, HsxSBWebBean webBean,
	    String pagesVisited) {
	// logger.info(SBWEB, INFO,"check the view Name :" + viewName);
	if (viewName != null && !pagesVisited.contains(viewName)) {
	    pagesVisited = pagesVisited.concat(STR_COMA).concat(viewName);
	} else {
	    String navigationViewName = viewName;
	    if (navigationViewName != null
		    && navigationViewName.endsWith(STR_FORWARD_SLASH)) {
		navigationViewName = StringUtils.substringBeforeLast(
			navigationViewName, STR_FORWARD_SLASH);

	    }
	    /*HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap
		    .get(navigationViewName);*/
	    // logger.info(SBWEB, INFO,"Check this: " + navigationViewName + " "
	    // + pageRuleBean.getPageType());
	    /*if (pageRuleBean != null
		    && DYNAMIC_PAGE_TYPE.equalsIgnoreCase(pageRuleBean
			    .getPageType()))
	    {
		pagesVisited = HsxSBWebPageBeanUtil.updatePageHistory(viewName,
			pagesVisited);
	    }*/
	}
	webBean.setPagesVisited(pagesVisited);
    }

    /**
     * @param viewName
     * @param webBean
     * @param pagesVisited
     */
    private void updatePageVisitedStringForChildPage(String viewName,
	    HsxSBWebBean webBean, String pagesVisited) {
	if (StringUtils.isNotBlank(pagesVisited)
		&& !pagesVisited.contains(viewName)) {
	    StringBuilder pageVisitedString = new StringBuilder(STR_EMPTY);
	    pageVisitedString.append(StringUtils.substringBefore(pagesVisited,
		    STR_COMA));
	    pageVisitedString.append(STR_COMA);
	    pageVisitedString.append(viewName);
	    pageVisitedString.append(STR_COMA);
	    pageVisitedString.append(StringUtils.substringAfter(pagesVisited,
		    STR_COMA));
	    webBean.setPagesVisited(pageVisitedString.toString());
	}
    }

    /**
     * This method will be executed after form submission and before validation.
     * To handle custom binding this method retrieves the values from request
     * and set the map in USDC bean.
     *
     * @param request
     * @return boolean value
     */
    @SuppressWarnings("unchecked")
    @Override
	protected boolean suppressBinding(HttpServletRequest request) {
		HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
				.getResource(WEB_BEAN);
		if (webBean != null) {
			try {
				Enumeration<String> paramNames = request.getParameterNames();
				HashMap<String, String> requestValueMap = new HashMap<String, String>();
				webBean.setSavedQuoteId(null);
				while (paramNames.hasMoreElements()) {
					String questionName = paramNames.nextElement().toString();
					String answerValue = request.getParameter(questionName);
					requestValueMap.put(questionName, answerValue);
					if (STR_SAVED_QUOTE_QUESTION_CODE
							.equalsIgnoreCase(questionName)) {
						logger.info(SBWEB, INFO,
								"Saved Quote Value Set to Bean as :"
										+ answerValue);
						webBean.setSavedQuoteId(answerValue);
					} else if (STR_PARTNER_DATACASH_SUCCESS_URL
							.equalsIgnoreCase(questionName)) {
						webBean.setPartnerPaymentSuccessURL(answerValue);
						logger.info(SBWEB, INFO,
								"Partner DataCash Success URL recieved :"
										+ answerValue);
					} else if (STR_PARTNER_DATACASH_EXPIRY_URL
							.equalsIgnoreCase(questionName)) {
						webBean.setPartnerPaymentExpiryURL(answerValue);
						logger.info(SBWEB, INFO,
								"Partner DataCash Expiry URL recieved :"
										+ answerValue);
					}					
				}

				HsxSBUIBuilderControllerImpl builderControllerImpl = new HsxSBUIBuilderControllerImpl();
				if (webBean.getWidgetMap() != null) {

					builderControllerImpl.bindValueToWidget(
							webBean.getWidgetMap(), requestValueMap);

				}
				builderControllerImpl = null;
			} catch (HsxSBUIBuilderRuntimeException e) {
				// log it into log file
				// return to error page
				logger.error("HsxSBUIBuilderRuntimeException",e);
				HsxSBWebQuoteAndBuyControllerUtil
						.logUiExceptionToErrorManagement(webBean,
								"HsxSBUIBuilderRuntimeException");

			} catch (HsxSBUIBuilderException e) {
				// log it into log file
				// log it into error management service
				// return to error page
				logger.error("HsxSBUIBuilderException", e);
				HsxSBWebQuoteAndBuyControllerUtil
						.logUiExceptionToErrorManagement(webBean,
								"HsxSBUIBuilderException");
			} catch (Exception e) {
				logger.error("Exception", e);
				HsxSBWebPageBeanUtil.logException(webBean, e);
			}
		}
		return false;
	}

    /**
     * This method retrieves the requested page from the url.
     *
     * @param url
     * @return pageName
     */
    private String lookUpScreenName(String url) {
	logger.info(SBWEB, INFO, "Current URL Recieved " + url);
	url = removeLastCharecters(url, STR_FORWARD_SLASH);
	String[] tokens = url.split("/");
	String pageName = null;
	final int tokLen = tokens.length;
	int tokenLength = tokLen;
	if (tokenLength > 1) {
	    if (tokenLength == 2 || tokenLength == 3) {
		pageName = tokens[tokenLength - 1];
	    } else {
		StringBuilder requestedPageName = new StringBuilder();
		for (int i = 2; i < tokLen; i++) {
		    requestedPageName.append(tokens[i]);
		    requestedPageName.append(STR_FORWARD_SLASH);
		}
		pageName = requestedPageName.toString();
	    }

	}

	if (pageName != null && pageName.contains(STR_QUESTION_SYMBOL)) {
	    String[] pageTokens = pageName.split(STR_QUESTION_REG_EXP);
	    pageName = pageTokens[0];
	}
	pageName = removeLastCharecters(pageName, STR_FORWARD_SLASH);
	logger.info(SBWEB, INFO, "Current URL Modified :" + pageName);
	return pageName;
    }
/**
 *
 * @param url
 * @param seperator
 * @return url
 */
    private String removeLastCharecters(String url, String seperator) {
	if (StringUtils.isNotBlank(url)) {
	    while (url.endsWith(seperator)) {
		url = StringUtils.substringBeforeLast(url, seperator);
	    }
	}
	return url;
    }

    /**
     * This method retrieves the requested page type from the url.
     *
     * @param url
     * @return folderType
     */
    private String getRequestedFolderType(String url) {
	url = removeLastCharecters(url, STR_FORWARD_SLASH);
	String[] tokens = url.split("/");
	String folderType = null;
	if (tokens.length > 3) {
	    folderType = tokens[2];
	}
	logger.info(SBWEB, INFO, "Folder Type :" + folderType);
	return folderType;
    }

    /**
     *This is to identify the action performed the user.
     *
     * @param request
     *            HttpServletRequest
     * @param enumeration
     *            Enumeration
     *
     * @return buttonPressed
     */
    @SuppressWarnings("unchecked")
    private String retrieveActionName(HttpServletRequest request) {
	Enumeration<String> enumeration = request.getParameterNames();
	String buttonPressed = null;
	while (enumeration.hasMoreElements()) {
	    String requestParamName = enumeration.nextElement();
	    if (requestParamName != null
		    && requestParamName.startsWith(STR_ACTION)
		    && requestParamName.endsWith(STR_BUTTON) && WebUtils.hasSubmitParameter(request, requestParamName)) {
		    buttonPressed = requestParamName;
		    String[] buttonNameArray = null;
		    if (buttonPressed != null) {
			buttonNameArray = HsxSBWebPageBeanUtil.getStringTokens(
				buttonPressed, STR_UNDERSCORE);
			if (buttonNameArray.length >= 2) {
			    buttonPressed = buttonNameArray[1];
			    if (STR_PRINT.equalsIgnoreCase(buttonPressed)) {
				continue;
			    }
			    break;
			}
		    }

		
	    }

	}
	return buttonPressed;
    }
/**
 *
 * @param request
 * @return methodName
 */
    @SuppressWarnings("unchecked")
    private String getRequestType(HttpServletRequest request) {

	String methodName = STR_GET;
	if (request != null) {
	    Enumeration<String> enumeration = request.getParameterNames();
	    while (enumeration.hasMoreElements()) {
		String requestParamName = enumeration.nextElement();
		if (requestParamName != null
			&& requestParamName.startsWith(STR_ACTION)
			&& requestParamName.endsWith(STR_BUTTON)) {
		    // Condition return above will filter all the
		    // requestParameters
		    // with Submit Actions. Since all the Action button button
		    // names
		    // in USDC start with Action and end with Button, if we have
		    // any
		    // such request parameters will mean that this particular
		    // request has submit parameters and is a Post request
		    methodName = STR_POST;
		    break;
		}
	    }
	}
	return methodName;
    }

}

