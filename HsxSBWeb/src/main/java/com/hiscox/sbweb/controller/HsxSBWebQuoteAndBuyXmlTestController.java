package com.hiscox.sbweb.controller;

import org.springframework.web.servlet.mvc.SimpleFormController;

import com.hiscox.sbweb.constants.IHsxSBWebConstants;

/**
 * @author Cognizant
 * @version 1.0
 * @created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebQuoteAndBuyXmlTestController extends SimpleFormController
	implements IHsxSBWebConstants{
//
//    private Map<String, HsxSBWebPageRuleBean> navigationRuleMap = null;
//    public static HsxWebLogger logger = HsxWebLogger.getLogger();
//    private String currentPageBar = null;
//    private String PAGEVISITED_STRING = "small-business-insurance,errors-and-omissions-insurance,workers-compensation-insurance,business-owner-insurance-policy,general-liability-insurance,no-products-available,coverage-options,coverage-options-change-coverage,lifestyle-questions,product-options,product-options-change-coverage,about-you,your-business,your-business-continued,additional-questions,additional-questions-two,your-quote,important-information,important-information-disagree,application-summary,application-summary-terms-and-conditions,payment-details,card-payment,confirmation,retrieve-a-quote,saved-quotes,save-quote,locked-account,errors-and-omissions,general-liability,business-owners-policy,calculating-your-quote,processing-your-payment,we-need-to-talk-to-you-about-your-quote,quote-details-saved,let-us-call-you,system-error,404-error,unable-to-navigate-back";
//
//    public void setHsxSBWebEncryption(HsxSBWebEncryption hsxSBWebEncryption)
//    {
//    }
//
//    private Properties appConfigProperties = null;
//    private Properties additionalInfoProperties = null;
//
//    public void setAdditionalInfoProperties(Properties additionalInfoProperties)
//    {
//	this.additionalInfoProperties = additionalInfoProperties;
//    }
//
//    public void setScreenNameURLProperties(Properties screenNameURLProperties)
//    {
//    }
//
//    public void setAppConfigProperties(Properties appConfigProperties)
//    {
//	this.appConfigProperties = appConfigProperties;
//    }
//
//    public void setWebServiceBroker(HsxSBGetPageServiceBroker webServiceBroker)
//    {
//    }
//
//    /**
//     * Retrieve a backing object for the current form from the given request.
//     * Override this method for custom implementation.
//     * 
//     * @param request
//     */
//    @SuppressWarnings("unchecked")
//    @Override
//    protected Object formBackingObject(HttpServletRequest request)
//    {
//
//	String currentUrl = request.getRequestURI();
//	String requestedPage = lookUpScreenName(currentUrl);
//	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
//		.getResource(WEB_BEAN);
//	if (!requestedPage.contains(".gif"))
//	{
//	    logger.info(SBWEB, INFO, "Form Backing Object Method");
//
//	    HttpSession session = request.getSession(true);
//	    HashMap<String, String> sessionMap = new HashMap<String, String>();
//
//	    Map<String, String> additionalInfoMap = new HashMap<String, String>();
//
//	    additionalInfoMap = HsxSBWebPageBeanUtil.getMapFromProperty(
//		    additionalInfoProperties, additionalInfoMap);
//	    Set<Map.Entry<String, String>> additionalInfoSet = additionalInfoMap.entrySet();
//	    Iterator<Map.Entry<String, String>> addInfoIterator = additionalInfoSet.iterator();
////	  
//
//	    webBean.setAdditionalInfoMap(additionalInfoMap);
//
//	    session.setAttribute(WIDGET, webBean.getWidget());
//
//	    String requestedFolderType = getRequestedFolderType(currentUrl);
//	    logger.info(SBWEB, INFO, "session.getId() :" + session.getId());
//	    logger.info(SBWEB, INFO, "webBean.getSessionIdentifierValue() :"
//		    + webBean.getSessionIdentifierValue());
//	    if (StringUtils.isBlank(webBean.getSessionIdentifierValue()))
//	    {
//		webBean.setSessionIdentifierValue(session.getId());
//	    }
//	    logger.info(SBWEB, INFO, "webBean.getSessionIdentifierValue() :"
//		    + webBean.getSessionIdentifierValue());
//	    // If the folder type changes for differnt urls then get this folder
//	    // type from the url.
//	    webBean.setFolderType(requestedFolderType);
//	    webBean.setFolder(requestedPage);
//	    HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap
//		    .get(requestedPage);
//	    logger.info(SBWEB, INFO, "Request Page: " + requestedPage);
//
//	    if (null != session.getAttribute(STR_SESSION_MANAGER))
//	    {
//		sessionMap = (HashMap<String, String>) session
//			.getAttribute(STR_SESSION_MANAGER);
//		if (null != pageRuleBean)
//		{
//		    currentPageBar = pageRuleBean.getPageBarItem();
//		    logger.info(SBWEB, INFO, "current pagebarItem: "
//			    + currentPageBar);
//		    sessionMap.put("currentPageBar", currentPageBar);
//		}
//		webBean.setSessionMap(sessionMap);
//	    }
//	    else
//	    {
//		// sessionMap = (HashMap<String, String>)
//		// HsxSBWebQuoteAndBuyControllerUtil.getSessionInstance();
//		// webBean.setSessionMap(sessionMap);
//	    }
//	    // implement some stub ver service call
//	    logger.info(SBWEB, INFO, "Request Type :"
//		    + webBean.getRequestType());
//	    logger.info(SBWEB, INFO, "Service Call Required :"
//		    + webBean.isServiceCallRequired());
//	    if ((STR_GET.equalsIgnoreCase(webBean.getRequestType()) || webBean
//		    .getRequestType() == null)
//		    && webBean.isServiceCallRequired())
//	    {
//		webBean.setWidget(null);
//		webBean.setActionsContent(null);
//		webBean.setQuestionsContent(null);
//		webBean.setTrackingContent(null);
//		// service call is required here to get the screen content
//		String requestXml = getXml(requestedPage);
//		// pageXml;
//		Document showPageDocument = HsxSBWebPageBeanUtil
//			.getDocumentFromString(requestXml.toString());
//		retrieveScreenConfigElements(webBean, showPageDocument);
//		webBean.setQuestionsContent(requestXml);
//		webBean.setActionsContent(requestXml);
//		webBean.setTrackingContent(requestXml);
//
//	    }
//	    else
//	    {
//		logger.info(SBWEB, INFO, "If Failed");
//	    }
//
//	    String authFailed = request.getParameter(AUTHORIZATION_FAILED);
//
//	    // if there is any login errors then updated the error messages in
//	    // the
//	    // page
//	    if (authFailed != null && authFailed.equalsIgnoreCase(STR_TRUE))
//	    {
//		try
//		{
//		    HsxSBWebPageBeanUtil.checkBadCredentials(webBean, session);
//		    webBean.setExceptionOccured(false);
//		} catch (HsxSBUIBuilderRuntimeException e)
//		{
//		    // TODO Auto-generated catch block
//		    webBean.setExceptionOccured(true);
//		    e.printStackTrace();
//		} catch (HsxSBUIBuilderException e)
//		{
//		    // TODO Auto-generated catch block
//		    webBean.setExceptionOccured(true);
//		    e.printStackTrace();
//		}
//	    }
//
//	    webBean.setRequestType(STR_GET);
//	}
//	return webBean;
//    }
//
//    @Override
//    protected ModelAndView showForm(HttpServletRequest request,
//	    HttpServletResponse response, BindException errors)
//	    throws Exception
//    {
//
//	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
//		.getResource(WEB_BEAN);
//	logger.info(SBWEB, INFO, "In the showForm isExceptionOccured Value :"
//		+ webBean.isExceptionOccured());
//	if (webBean.isExceptionOccured())
//	{
//
//	    return new ModelAndView(new RedirectView(STR_SYSTEM_ERROR,true)); 
//	}
//	else
//	{
//	    return super.showForm(request, response, errors);
//	}
//
//    }
//
//    @SuppressWarnings("unchecked")
//    @Override
//    protected boolean suppressValidation(HttpServletRequest request,
//	    Object command)
//    {
//	HsxSBWebBean webBean = null;
//	if (command instanceof HsxSBWebBean)
//	{
//	    webBean = (HsxSBWebBean) command;
//	}
//	if (webBean != null)
//	{
//	    Enumeration<String> enumeration = request.getParameterNames();
//	    String requestedAction = getRequestedAction(request, enumeration);
//	    webBean.setRequestedAction(requestedAction);
//	    logger.info(SBWEB, INFO, "Requested Action in the XML TEST ---> :"
//		    + requestedAction);
//	    Map<String, String> actionMap = (HashMap<String, String>) webBean
//		    .getActionMap();
//	    logger.info(SBWEB, INFO, "In Supress Validation");
//	    Collection<String> c = actionMap.keySet();
//	    for (Iterator<String> iterator = c.iterator(); iterator.hasNext();)
//	    {
//		String key = iterator.next();
//		String value = actionMap.get(key);
//		logger.info(SBWEB, INFO, "Key :" + key + " value :" + value);
//	    }
//	    if (actionMap != null && actionMap.containsKey(requestedAction))
//	    {
//		if (STR_YES.equalsIgnoreCase(actionMap.get(requestedAction)))
//		{
//		    return false;
//		}
//	    }
//	}
//	return false;
//    }
//
//    protected ModelAndView handleRequestInternal(HttpServletRequest request,
//	    HttpServletResponse response) throws Exception
//    {
//
//	String requestedPage = lookUpScreenName(request.getRequestURI());
//	if (HsxSBWebQuoteAndBuyControllerUtil
//		.isRequestedPageValid(requestedPage))
//	{
//
//	    HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
//		    .getResource(WEB_BEAN);
//	    // get the navigation rule map which contains the key as the
//	    // pageName
//	    // and the value it the object to the pageRuleBean
//	    navigationRuleMap = HsxSBWebNavigationManager.getInstance()
//		    .getNavigationMap();
//	    String currentURL = null;
//	    
//	    currentURL = request.getRequestURL().toString();
//	    logger.info(SBWEB, INFO, "Current URL  :" + currentURL);
//	    if (requestedPage != null)
//	    {
//		// based on the requestedPage get the corresponding pageRuleBean
//		// from
//		// the navigationRuleMap
//		HsxSBWebPageRuleBean pageRuleBean = navigationRuleMap
//			.get(requestedPage);
//		if (pageRuleBean != null)
//		{
//		    if (StringUtils
//			    .isBlank(webBean.getSessionIdentifierValue()))
//		    {
//			webBean.setPagesVisited(null);
//			HsxSBWebPageBeanUtil.resetWebBean(webBean);
//		    }
//		    String authFailed = request
//			    .getParameter(AUTHORIZATION_FAILED);
//		    HsxCoreLDAPUtil ldapsUtil = new HsxCoreLDAPUtil();
//		    HttpSession session = request.getSession();
//
//		    String pagesVisited = PAGEVISITED_STRING;
//
//		    // returns to the account locked page if the user tries to
//		    // login
//		    // with bad password for more that 4 times
//		    if (authFailed != null
//			    && authFailed.equalsIgnoreCase(STR_TRUE))
//		    {
//			ModelAndView acegiView = super.handleRequestInternal(
//				request, response);
//			return HsxSBWebPageBeanUtil.checkAccountLocked(webBean,
//				authFailed, ldapsUtil, session,
//				pagesVisited, acegiView, appConfigProperties);
//		    }
//		    // the navigation rules will get executed
//		    return checkNavigation(request, response, webBean,
//			    pageRuleBean, pagesVisited);
//
//		}
//		else
//		{
//
//		    // Redirect to Page not found error page
//		    HsxSBWebQuoteAndBuyControllerUtil
//			    .resetWebBeanXmlContent(webBean);
//		    return new ModelAndView(HsxSBWebQuoteAndBuyControllerUtil
//			    .redirectToPageNotFoundUrl());
//		}
//	    }
//	    else
//	    {
//		// Redirect to Page not found error page
//		HsxSBWebQuoteAndBuyControllerUtil
//			.resetWebBeanXmlContent(webBean);
//		return new ModelAndView(HsxSBWebQuoteAndBuyControllerUtil
//			.redirectToPageNotFoundUrl());
//	    }
//	}
//	else
//	{
//	    return super.handleRequestInternal(request, response);
//	}
//
//    }
//
//    /**
//     * This method to check the navigation rules.
//     * 
//     * @param request
//     * @param response
//     * @param webBean
//     * @param pageRuleBean
//     * @param pagesVisited
//     * @return
//     * @throws Exception
//     */
//    private ModelAndView checkNavigation(HttpServletRequest request,
//	    HttpServletResponse response, HsxSBWebBean webBean,
//	    HsxSBWebPageRuleBean pageRuleBean, String pagesVisited) throws Exception
//    {
//
//	String pageType = pageRuleBean.getPageType();
//	String pageName = pageRuleBean.getPageName();
//	boolean isChildPage = pageRuleBean.isChildPage();
//
//	if (STATIC_PAGE_TYPE.equalsIgnoreCase(pageType))
//	{
//	    webBean.setServiceCallRequired(false);
//	    logger.info(SBWEB, INFO, "static Page ::: " + pageName);
//	    return super.handleRequestInternal(request, response);
//	}
//	else if (LANDING_PAGE_TYPE.equalsIgnoreCase(pageType)
//		|| HOME_PAGE_TYPE.equalsIgnoreCase(pageType))
//	{
//	    webBean.setServiceCallRequired(true);
//	    logger.info(SBWEB, INFO, "landing Page ::: " + pageName);
//	    if (pagesVisited == null)
//	    {
//		pagesVisited = pageName;
//		// start of the navigation history string (pagesVisited)
//		webBean.setPagesVisited(pagesVisited);
//		return super.handleRequestInternal(request, response);
//	    }
//	}
//	else if (DYNAMIC_PAGE_TYPE.equalsIgnoreCase(pageType))
//	{
//	    webBean.setServiceCallRequired(true);
//	    logger.info(SBWEB, INFO, "dynamic Page ::: " + pageName);
//	    logger.info(SBWEB, INFO, "Pages visited :: " + pagesVisited);
//	    if (pagesVisited == null || !pagesVisited.contains(pageName))
//	    {
//		// redirect to invalid navigation page
//		HsxSBWebQuoteAndBuyControllerUtil
//			.resetWebBeanXmlContent(webBean);
//		return new ModelAndView(HsxSBWebQuoteAndBuyControllerUtil
//			.redirectToInvalidPageUrl());
//	    }
//	    else if (isChildPage)
//	    {
//		String parentPage = pageRuleBean.getParentPage();
//		if (STR_POST.equalsIgnoreCase(webBean.getRequestType())
//			&& parentPage != null
//			&& pagesVisited.contains(parentPage))
//		{
//		    return super.handleRequestInternal(request, response);
//		}
//		else
//		{
//		    // redirect to invalid navigation page
//		    HsxSBWebQuoteAndBuyControllerUtil
//			    .resetWebBeanXmlContent(webBean);
//		    return new ModelAndView(HsxSBWebQuoteAndBuyControllerUtil
//			    .redirectToInvalidPageUrl());
//		}
//
//	    }
//	}
//	else if (POPUP_PAGE_TYPE.equalsIgnoreCase(pageType))
//	{
//	    webBean.setServiceCallRequired(true);
//	    logger.info(SBWEB, INFO, "dynamic popup Page ::: " + pageName);
//	    String parentPage = pageRuleBean.getParentPage();
//	    if (STR_GET.equalsIgnoreCase(webBean.getRequestType())
//		    && parentPage != null && pagesVisited.contains(parentPage))
//	    {
//		return super.handleRequestInternal(request, response);
//	    }
//	    else
//	    {
//		// redirect to invalid navigation page
//		HsxSBWebQuoteAndBuyControllerUtil
//			.resetWebBeanXmlContent(webBean);
//		return new ModelAndView(HsxSBWebQuoteAndBuyControllerUtil
//			.redirectToInvalidPageUrl());
//	    }
//
//	}
//	return super.handleRequestInternal(request, response);
//    }
//
//    /**
//     * This is an overridden method which is called when the page gets submitted
//     * 
//     * @param request
//     * @param response
//     * @param response
//     */
//    @Override
//    protected ModelAndView onSubmit(HttpServletRequest request,
//	    HttpServletResponse response, Object command, BindException errors)
//    {
//
//	logger.info(SBWEB, INFO, "On Submitt Method");
//	ModelAndView modelAndView = null;
//	String viewName = null;
//	String currentURL = null;
//	currentURL = request.getRequestURL().toString();
//	HttpSession session = request.getSession(true);
//	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
//		.getResource(WEB_BEAN);
//	logger.info(SBWEB, INFO, "currentURL: in On Submit :: " + currentURL);
//	//HsxSBWebQuoteAndBuyControllerUtil.getSessionValues(null, null);
//	session.setAttribute(STR_SESSION_MANAGER, webBean.getSessionMap());
//	try
//	{
//	 
//	    Map<String, HsxSBWidget> widgetMap = webBean.getWidgetMap();
//	    String screenXmlContent = webBean.getQuestionsContent();
//	    String requestedAction = webBean.getRequestedAction();
//	    Document document = HsxSBWebPageBeanUtil
//		    .getDocumentFromString(screenXmlContent);
//	    String requestXml = HsxSBResponseProcessor
//		    .generateScreenQuestionXML(document, widgetMap,
//			    requestedAction);
//
//	    logger.info(SBWEB, INFO,
//		    "Request Xml Logged (Response Processor)After Saved-Quotes Page  :"
//			    + requestXml);
//
//	    String currentPage = lookUpScreenName(request.getRequestURI());
//	    logger.info(SBWEB, INFO, "Is Js Enabled :"
//		    + webBean.getIsJSEnabled());
//	    if (ABOUT_YOU.equalsIgnoreCase(currentPage))
//	    {
//		webBean.setQuoteRefID("QRF987654");
//		viewName = "quote-and-buy/" + "your-business-two";
//	    }
//	    else if (YOUR_BUSINESS.equalsIgnoreCase(currentPage))
//	    {
//		viewName = "quote-and-buy/" + YOUR_QUOTE;
//	    }
//	    else if (SMALL_BUSINESS_INSURANCE.equalsIgnoreCase(currentPage))
//	    {
//		viewName = QUOTE_AND_BUY + STR_FORWARD_SLASH + ABOUT_YOU;
//	    }
//	    else
//	    {
//		viewName = ABOUT_YOU;
//	    }
//
//	} catch (Exception e)
//	{
//	    request.getSession().invalidate();
//	    e.printStackTrace();
//	}
//	// service call is required to display the page
//	webBean.setWidget(null);
//	String requestXml = HsxSBWebPageBeanUtil.getXml(viewName); // pageXml;
//
//	webBean.setQuestionsContent(requestXml);
//	webBean.setActionsContent(requestXml);
//
//	String pagesVisited = webBean.getPagesVisited();
//	if (pagesVisited != null)
//	{
//	    if (!pagesVisited.contains(viewName))
//	    {
//		pagesVisited = pagesVisited.concat(STR_COMA).concat(viewName);
//	    }
//	    else
//	    {
//		pagesVisited = HsxSBWebPageBeanUtil.updatePageHistory(viewName,
//			pagesVisited);
//	    }
//
//	    webBean.setPagesVisited(pagesVisited);
//	}
//	webBean.setRequestType(STR_POST);
//	modelAndView = new ModelAndView(new RedirectView(viewName,true)); 
//
//	return modelAndView;
//
//    }
//
//    private void retrieveScreenConfigElements(HsxSBWebBean webBean,
//	    Document showPageDocument)
//    {
//
//	Element dataAreaElement = (Element) showPageDocument
//		.getElementsByTagNameNS(STR_ASTERISK, STR_DATAAREA).item(0);
//	if (dataAreaElement != null)
//	{
//	    NodeList dataAreaChildNodesList = dataAreaElement.getChildNodes();
//	    int dataAreaChldNodesLstLength = dataAreaChildNodesList.getLength();
//	    webBean.setCheckBoxValidationFlag(STR_NO);
//	    webBean.setRadioButtonValidationFlag(STR_NO);
//	    webBean.setGroupValidationFlag(STR_NO);
//	    webBean.setDefaultNavigator(null);
//	    for (int dataAreaChildNodesIndex = 0; dataAreaChildNodesIndex < dataAreaChldNodesLstLength; dataAreaChildNodesIndex++)
//	    {
//		Node daNode = dataAreaChildNodesList
//			.item(dataAreaChildNodesIndex);
//		if (daNode != null)
//		{
//		    String nodeName = daNode.getNodeName();
//		    if (nodeName != null && nodeName.contains(XML_CONFIG))
//		    {
//			Element configElement = (Element) daNode;
//			String configName = configElement
//				.getAttribute(STR_NAME);
//			if (StringUtils.isNotBlank(configName))
//			{
//			    if (STR_QUOTE_REF_ID.equalsIgnoreCase(configName))
//			    {
//				String quoteRefID = configElement
//					.getAttribute(STR_VALUE);
//				logger.info(SBWEB, INFO,
//					"Quote reference Id Recieved :"
//						+ quoteRefID);
//				if (StringUtils.isNotBlank(quoteRefID))
//				{
//				    webBean.setQuoteRefID(quoteRefID);
//				    // quote ref id ias also stored as a backup
//				    // which will be used in logging the same to
//				    // Error Management
//				    webBean.setErrMngtQuoteRefId(quoteRefID);
//				}
//			    }
//			    else if (STR_CHECKBOX_VALIDATION_FLAG
//				    .equalsIgnoreCase(configName))
//			    {
//				webBean.setCheckBoxValidationFlag(configElement
//					.getAttribute(STR_VALUE));
//			    }
//			    else if (STR_RADIO_BUTTON_VALIDATION_FLAG
//				    .equalsIgnoreCase(configName))
//			    {
//				webBean
//					.setRadioButtonValidationFlag(configElement
//						.getAttribute(STR_VALUE));
//			    }
//			    else if (STR_LOGIN_INDICATOR
//				    .equalsIgnoreCase(configName))
//			    {
//				webBean.setLoginIndicator(configElement
//					.getAttribute(STR_VALUE));
//			    }
//			    else if (STR_GRP_VALIDTN
//				    .equalsIgnoreCase(configName))
//			    {
//				System.out
//					.println("validation falg :: "
//						+ configElement
//							.getAttribute(STR_VALUE));
//				webBean.setGroupValidationFlag(configElement
//					.getAttribute(STR_VALUE));
//			    }
//			    else if (STR_DEFAULT_NAVIGATOR
//				    .equalsIgnoreCase(configName))
//			    {
//				webBean.setDefaultNavigator(configElement
//					.getAttribute(STR_VALUE));
//			    }
//			}
//		    }
//		}
//	    }
//
//	}
//
//    }
//
//    /**
//     * This method will be executed after form submission and before validation.
//     * To handle custom binding this method retrieves the values from request
//     * and set the map in USDC bean.
//     * 
//     * @param request
//     */
//    @SuppressWarnings("unchecked")
//    @Override
//    protected boolean suppressBinding(HttpServletRequest request)
//    {
//	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
//		.getResource(WEB_BEAN);
//	Enumeration<String> paramNames = request.getParameterNames();
//	HashMap<String, String> requestValueMap = new HashMap<String, String>();
//
//	while (paramNames.hasMoreElements())
//	{
//	    String questionName = paramNames.nextElement().toString();
//	    String answerValue = request.getParameter(questionName);
//	    requestValueMap.put(questionName, answerValue);
//
//	}
//
//	HsxSBUIBuilderControllerImpl builderControllerImpl = new HsxSBUIBuilderControllerImpl();
//	if (webBean != null && webBean.getWidgetMap() != null)
//	{
//	    try
//	    {
//		builderControllerImpl.bindValueToWidget(webBean.getWidgetMap(),
//			requestValueMap);
//	    } catch (HsxSBUIBuilderRuntimeException e)
//	    {
//		// log it into log file
//		// return to error page
//
//	    } catch (HsxSBUIBuilderException e)
//	    {
//		// log it into log file
//		// log it into error management service
//		// return to error page
//	    }
//	}
//
//	return false;
//    }
//
//    /**
//     * This method will be moved to Web Utility class later. This is to identify
//     * the action performed the user.
//     * 
//     * @param request
//     *            HttpServletRequest
//     * @param enumeration
//     *            Enumeration
//     * 
//     * @return buttonPressed
//     */
//    private String getRequestedAction(HttpServletRequest request,
//	    Enumeration<String> enumeration)
//    {
//	String buttonPressed = null;
//	while (enumeration.hasMoreElements())
//	{
//	    String requestParamName = enumeration.nextElement();
//	    if (requestParamName != null
//		    && requestParamName.startsWith(STR_ACTION)
//		    && requestParamName.endsWith(STR_BUTTON))
//	    {
//		if (WebUtils.hasSubmitParameter(request, requestParamName))
//		{
//		    buttonPressed = requestParamName;
//		    String[] buttonNameArray = null;
//		    if (buttonPressed != null)
//		    {
//			buttonNameArray = HsxSBWebPageBeanUtil.getStringTokens(
//				buttonPressed, STR_UNDERSCORE);
//			if (buttonNameArray.length >= 2)
//			{
//			    buttonPressed = buttonNameArray[1];
//			}
//		    }
//
//		}
//	    }
//
//	}
//	return buttonPressed;
//    }
//
//    public static String getXml(String pageName)
//    {
//	StringBuilder inputXML = new StringBuilder();
//	// replace the below code to retrieve IPEXML from rule engine
//	try
//	{
//	    String[] tokens = pageName.split("/");
//	    int tokenLength = tokens.length;
//	    if (tokenLength > 0)
//	    {
//		// logger.info(SBWEB, INFO,tokenLength);
//		pageName = tokens[tokenLength - 1];
//	    }
//	    File f = new File("C:\\TestData\\" + pageName.trim() + ".xml");
//	    RandomAccessFile rnd;
//	    rnd = new RandomAccessFile(f, "rw");
//	    String inter = rnd.readLine();
//	    while (inter != null)
//	    {
//		inputXML.append(inter);
//		inter = rnd.readLine();
//	    }
//	    rnd.close();
//	} catch (Exception e)
//	{
//	    e.printStackTrace();
//	}
//	return inputXML.toString();
//
//    }
//
//    /**
//     * This method retrieves the requested page from the url
//     * 
//     * @param url
//     * @return
//     */
//    private String lookUpScreenName(String url)
//    {
//	logger.info(SBWEB, INFO, "Current URL Recieved " + url);
//	url = removeLastCharecters(url, STR_FORWARD_SLASH);
//	String[] tokens = url.split("/");
//	String pageName = null;
//	final int tokLen = tokens.length;
//	int tokenLength = tokLen;
//	if (tokenLength > 1)
//	{
//	    if (tokenLength == 2 || tokenLength == 3)
//	    {
//		pageName = tokens[tokenLength - 1];
//	    }
//	    else
//	    {
//		StringBuilder requestedPageName = new StringBuilder();
//		for (int i = 2; i < tokLen; i++)
//		{
//		    requestedPageName.append(tokens[i]);
//		    requestedPageName.append(STR_FORWARD_SLASH);
//		}
//		pageName = requestedPageName.toString();
//	    }
//
//	}
//
//	if (pageName!=null && pageName.contains(STR_QUESTION_SYMBOL)) 
//	{
//	    String[] pageTokens = pageName.split(STR_QUESTION_REG_EXP);
//	    pageName = pageTokens[0];
//	}
//	pageName = removeLastCharecters(pageName, STR_FORWARD_SLASH);
//	logger.info(SBWEB, INFO, "Current URL Modified :" + pageName);
//	return pageName;
//    }
//
//    private String removeLastCharecters(String url, String seperator)
//    {
//	if (StringUtils.isNotBlank(url))
//	{
//	    while (url.endsWith(seperator))
//	    {
//		url = StringUtils.substringBeforeLast(url, seperator);
//	    }
//	}
//	return url;
//    }
//
//    /**
//     * This method retrieves the requested page type from the url
//     * 
//     * @param url
//     * @return
//     */
//    private String getRequestedFolderType(String url)
//    {
//	url = removeLastCharecters(url, STR_FORWARD_SLASH);
//	String[] tokens = url.split("/");
//	String folderType = null;
//	if (tokens.length > 3)
//	{
//	    folderType = tokens[2];
//	}
//	logger.info(SBWEB, INFO, "Folder Type :" + folderType);
//	return folderType;
//    }
//
}

