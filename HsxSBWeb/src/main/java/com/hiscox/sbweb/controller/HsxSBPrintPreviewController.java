package com.hiscox.sbweb.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.mvc.SimpleFormController;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;
/**
 * @author cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBPrintPreviewController extends SimpleFormController implements
	IHsxSBWebConstants {
    @Override
   protected Object formBackingObject(HttpServletRequest request) {

	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
	.getResource(WEB_BEAN);
	webBean.setFolderType("quote-and-buy");
	webBean.setFolder("quote-and-buy/print-preview");
       return webBean;
   }

  /*  @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
	    HttpServletResponse response) throws Exception
    {
	return new ModelAndView(new RedirectView("http://10.204.7.176:8020/small-business-insurance/quote-and-buy/print-preview"));
    }*/
}

