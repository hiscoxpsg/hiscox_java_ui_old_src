package com.hiscox.sbweb.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;
import com.hiscox.sbweb.util.HsxSBWebTextManager;

public class HsxSBWebHelpFormController extends SimpleFormController implements
	IHsxSBWebConstants {
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    @Override
    protected Object formBackingObject(HttpServletRequest request)
	    throws Exception {
	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);
	// webBean.setFolderType(QUOTE_AND_BUY);
	webBean.setFolder(QUOTE_AND_BUY + STR_FORWARD_SLASH + "help-text");
	String pageName = request.getParameter("pageName");
	String questionCode = request.getParameter("questionCode");
	logger.info(SBWEB, INFO, "Page Name :" + pageName);
	logger.info(SBWEB, INFO, "Question Code :" + questionCode);
	String textCode = pageName + STR_UNDERSCORE + questionCode;
	logger.info(SBWEB, INFO, "Text Item Code :" + textCode);
	Map<String, HsxSBTextManagerVO> textItemsMap = HsxSBWebTextManager.getInstance().textItemsMap;
	if (textItemsMap.containsKey(textCode)) {
	    HsxSBTextManagerVO textItem = textItemsMap.get(textCode);
	    if (textItem != null) {
		String helpText = textItem.getHelpText();
		String label = textItem.getLabelText();
		webBean.setHelpTextContent(STR_EMPTY);
		webBean.setHelpTextLabel(STR_EMPTY);
		logger.info(SBWEB, INFO, "helpText :" + helpText);
		logger.info(SBWEB, INFO, "label :" + label);
		if (StringUtils.isNotBlank(helpText)) {
		    webBean.setHelpTextContent(helpText);
		}
		if (StringUtils.isNotBlank(label)) {
		    webBean.setHelpTextLabel(label);
		}
	    }

	}
	return webBean;
    }
}

