package com.hiscox.sbweb.constants;

/**
 * IHsxSBWebConstants interface for all web String Constants declaration.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:22 AM
 */

public interface IHsxSBWebConstants {
    // String Constants
    String STR_GET_PAGE_SERVICE_BINDER_NAME = "hsx_USDCOrch_Synchronous_WSProviders_getPageProvider_Binder_getPage";
    String STR_ERROR_MANAGEMENT_SERVICE_BINDER_NAME = "hsx_ErrorMgmt_Agent_WSProviders_SOAP12_ErrorManagement_Binder_handleApplicationException";
    String STR_ERROR_MANAGEMENT_SERVICE_DEFAULT_URI = "ws/hsx.ErrorMgmt_Agent.WSProviders.SOAP12:ErrorManagement";
    String STR_GET_PAGE_SERVICE_DEFAULT_URI = "ws/hsx.USDCOrch.Synchronous.WSProviders:getPageProvider";
    String STR_TEXT_MANAGER_SERVICE_DEFAULT_URI = "ws/hsx.ctm.WSProviders:retieveDetailedText";
    String STR_TEXT_MANAGER_SERVICE_BINDER_NAME = "hsx_ctm_WSProviders_retieveDetailedText_Binder_retrieveDetailedText";
    String STR_GET_PAGE_SERVICE_URL = "getPageServiceURL";
    String STR_RETRIEVE_DETAILED_TEXT_SERVICE_URL = "retrieveDetailedTextServiceURL";
    String STR_ERROR_MANAGEMENT_SERVICE_URL = "errorManagementServiceURL";
    String STR_RETRIEVETEXTINPUT = "RetrieveTextInput";
    String STR_RETRIEVE_INPUT = "retrieveTextInput";
    String STR_INPUT = "Input";
    String STR_SCHEME = "Scheme";
    String STR_CHANNEL = "Channel";
    String STR_CHANNEL_CONTENT = "D";
    String STR_SCHEME_CONTENT = "USDC";
    String STR_APPLICATIONAREA = "ApplicationArea";
    String STR_DATAAREA = "DataArea";
    String STR_TRANSACTIONID = "TransactionID";
    String STR_FORM = "Form";
    String STR_CODE = "code";
    String STR_SAVED_VALUE = "savedValue";
    String STR_NULL = "null";
    String STR_CURRENT_SCREEN_ID = "currentScreenId";
    String STR_GET_PAGE_XML = "GetPageXML";
    String STR_GETPAGE = "GetPage";
    String STR_ASTERISK = "*";
    //String STR_CARD_DETAILS = "CardDetails";
    String STR_PAYMENT_DETAILS = "PaymentDetails";
    String XML_SCREEN_QUESTION = "ScreenQuestion";
    String STR_CARD_NUMBER = "CardNumber";
    String STR_CARD_SECURITY_CODE = "SecurityCode";
    String STR_CARD_EXPIRY_DATE = "ExprtnDate";
    String XML_CONFIG = "Config";
    String XML_TAGLIST = "TagList";
    String CONFIG_SAVED_VALUE = "savedValue";
    String CONFIG_RETURN_URL = "returnUrl";
    String CONFIG_EXPIRY_URL = "expiryUrl";
    String CONFIG_PAGESET_ID = "pageSetId";
    String CONFIG_REFERENCE = "datacashReference";
    String CONFIG_DYNAMIC_DATA1 = "dynData1";
    String CONFIG_DYNAMIC_DATA2 = "dynData2";
    String CONFIG_DYNAMIC_DATA3 = "dynData3";
    String CONFIG_DYNAMIC_DATA4 = "dynData4";
    String CONFIG_DYNAMIC_DATA5 = "dynData5";
    String CONFIG_DYNAMIC_DATA6 = "dynData6";
    String CONFIG_DYNAMIC_DATA7 = "dynData7";
    String CONFIG_DYNAMIC_DATA8 = "dynData8";
    String CONFIG_ADDITIONAL_VALUE1 = "additionalValue1";
    String CONFIG_ADDITIONAL_VALUE2 = "additionalValue2";
    String CONFIG_ADDITIONAL_VALUE3 = "additionalValue3";
    String CONFIG_EMAIL_SUBTYPE = "Email";
    String STR_PAYMENT_DETAILS_BACK = "PaymentBack";
    String STR_DYNAMIC_DATA_STYLE = ".additional-text{display:none;}";
    String STR_DYNAMIC_DATA_REVERSE_STYLE = ".additional-text{display:block;}";

    String STR_NAME = "name";
    String XML_QUESTION_VALUE = "value";
    String STR_CARD_PAYMENT_COMPONENT_NAME = "CARDPAYMENT-US-DIRECTCOMMERCIAL";
    String STR_ACTION = "action";
    String STR_BUTTON = "button";
    String STR_UNDERSCORE = "_";
    String STR_DECRYPT = "DECRYPT";
    String STR_ENCRYPT = "ENCRYPT";
    String STR_USER_DN = "userDn";
    String STR_PASSWORD = "password";
    String STR_CARD_PAYMENT_PASSWORD = "cardPaymentPassword";
    String STR_ACEGI_PASSWORD = "acegiPassword";
    String STR_BASE = "base";
    String STR_DNS_NAME = "dnsName";
    String STR_APP_DN = "appDn";
    String STR_KEY = "Key";
    String STR_SESSION_IDENTIFIER = "sessionIdentifier";
    String STR_MESSAGE_ID = "MessageID";
    String STR_MESSAGE_ID_VALUE = "123456789";
    String STR_TRANSMISSION_ID = "TransmissionID";
    String STR_SCHEME_CODE = "schemeCode";
    String STR_REQUESTED_ACTION = "requestedAction";
    String STR_NEXT_SCREEN_ID = "nextScreenId";
    String STR_PREVIOUS_SCREEN_ID = "previousScreenId";
    String STR_USDCWEB = "USDCWEB";
    String STR_BLANK_SCREEN = "BLANK_SCREEN";
    String STR_REQUEST_TYPE = "RequestedType";
    String STR_REQUEST_PAGE = "requestedpage";
    String STR_GET_PAGE = "GET_PAGE";
    String STR_BLANK = "BLANK";
    String STR_LANDING_PAGE_TYPE = "LandingPageRequest";
    String STR_BROACHERWARE_TYPE = "BroacherwareRequest";
    String STR_BROACHERWARE_CONFIG = "broacherwareReq";
    String STR_GET_REQUEST_TYPE = "GetRequest";
    String STR_POP_UP_GET_REQUEST_TYPE = "PopUpGetRequest";
    String STR_NEXT_PAGE_REQUEST_TYPE = "NextPageRequest";
    String STR_PROGRESS_BAR_PAGE_REQUEST_TYPE = "ProgressBarPageRequest";
    String STR_ACEGI_TARGET_URL_GET_REQUEST_TYPE = "AcegiTargetUrlGetRequest";
    String STR_SESSION_MANAGER = "sessionManager";
    String STR_DEFAULT = "Default";
    String STR_PRIMARY_BUSINESS = "primarybusiness";
    String STR_U2 = "U2";
    String STR_U5 = "U5";
    String STR_PARTNER_DATACASH_SUCCESS_URL = "paymentSuccessURL";
    String STR_PARTNER_DATACASH_EXPIRY_URL = "paymentExpiryURL";
    String STR_GET_RETRIEVE_QUOTE_SCREEN ="GET_RETRIEVE_QUOTE_SCREEN";
    String STR_RETRIEVE_QUOTE_GUID="retrievequoteguid";
    
    // special Charecters
    String STR_LESSTHAN_SYMBOL = "<";
    String STR_GREATERTHAN_SYMBOL = ">";
    String STR_BACKWARD_SLASH_SYMBOL = "\\";
    String STR_QUESTION_SYMBOL = "?";
    String STR_QUESTION_REG_EXP = "\\?";
    String STR_DOT = ".";
    String XML_CDATA_START_TAG = "<![CDATA[";
    String XML_CDATA_END_TAG = "]]>";

    // String Array Constants

    String[] APPLICATION_AREA_ELEMENT_ARR =
    {"MessageID", "TransactionID", "TransmissionID", "ReferenceID",
	    "CreationDateTime", "Verb", "Noun", "SenderID", "ReceiverID",
	    "Filter", "OutputOrder", "SecurityCredentials" };

    String[] DATA_AREA_ELEMENT_ARRAY =
    {"Form", "TagList", "currentScreenId", "schemeCode", "requestedAction",
	    "sessionIdentifier", "transmissionId", "nextScreenId", "code" };
    
    String[][] PARTNERS_AND_AGENT_ARRAY =
        {{"partner",""},{"partnerAgent",""},{"agentName",""}, {"agentPhoneNumber",""}, {"agentEmail",""},
    		{"agencyName",""}, {"partnerRepresentative",""},{"quoteOrigin",""}};

    String[][] LANDING_PAGE_QUESTION_GROUP_ELEMENT_ARR =
    {{"HM_1", "HM_1.1"}};
    String[][] LANDING_PAGE_QUESTION_ELEMENT_ARR =
    {{"HM_1.1", "State,BusinessCategory,PrimaryBusiness,PrimaryBusinessExplain,Title,ApplicantFirstName,ApplicantLastName,BusinessName,Street,SecondaryStreet,BusinessCity,ZipCode,TelephoneNumber,EmailAddress,PlCoverage,GlCoverage,BopCoverage"}};
    String LANDING_PAGE_DUMMY_QG = "Dummy_EG_1_1";
    String[][] LANDING_PAGE_QUESTION_XPATH_ELEMENT_ARR =
    {
	    {"State", "Policy/Client/AddressGBR[@addressType='main']/@state" },
	    {"BusinessCategory", "Policy/Transaction/Question[@code='BusinessCategory']/Response/@value" },
	    {"PrimaryBusiness", "Policy/Transaction/Question[@code='Industry']/Response/@value" },
	    {"PrimaryBusinessExplain", "Policy/Transaction/Question[@code='IndustryNone']/Response/@value"},
	    {"Title","Policy/Client/Individual/@title"},
	    {"ApplicantFirstName", "Policy/Client/Individual/@forename" },
	    {"ApplicantLastName", "Policy/Client/Individual/@surname"},
	    {"BusinessName","Policy/Client/Company/@companyName"},
	    {"Street","Policy/Client/AddressGBR[@addressType='main']/@address1"},
	    {"SecondaryStreet","Policy/Client/AddressGBR[@addressType='main']/@address2"},
	    {"BusinessCity","Policy/Client/AddressGBR[@addressType='main']/@cityTown"},
	    {"ZipCode", "Policy/Client/AddressGBR[@addressType='main']/@postCode"},
	    {"TelephoneNumber","Policy/Client/Individual/@mainTelephone"},
	    {"EmailAddress", "Policy/Client/Individual/@mainEmail"},
	    {"PlCoverage", "Dummy1"},
	    {"GlCoverage", "Dummy2"},
	    {"BopCoverage", "Dummy3"},
	    };

    String[] INPUT_ELEMENTS_ARRAY =
    {"Scheme", "Channel", "Language", "ActiveDate", "EffectiveDate",
	    "TextCode", "TextType", "Format" };

    String SMALL_BUSINESS_INSURANCE = "small-business-insurance";
    String QUOTE_AND_BUY = "quote-and-buy";
    String QUOTE_AND_BUY_SLASH = "quote-and-buy/";
    String STR_FORWARD_SLASH = "/";
    String INDEX_JSP = "index";
    String WEB_BEAN = "webBean";
    String ERROR_MANAGEMENT_SERVICE_BROKER = "errorManagementServiceBroker";
    String STR_EMPTY = "";
    String STR_GET = "get";
    String STR_POST = "Post";
    String WIDGET = "widget";
    String AUTHORIZATION_FAILED = "authorizationFailed";
    String STR_TRUE = "true";
    String SBWEB = "SBWEB";
    String INFO = "INFO";
    String DEBUG = "DEBUG";
    String ERROR = "ERROR";
    String PROPERTIES_LOGGER = "loggerProperties";
    String WEB_LOGGER_CLASS = "HsxWebLogger";
    String STR_BAD_CREDENTIALS = "Bad credentials";
    String STR_COMA = ",";
    String STR_ERROR_MESSAGE = "Error Message :";
    String STR_EMPTY_USERNAME = "Empty Username";
    String STR_EMPTY_PASSWORD = "Empty Password";
    String TEXTMANAGER = "textManager";

    String STATIC_PAGE_TYPE = "static";
    String DYNAMIC_PAGE_TYPE = "dynamic";
    String LANDING_PAGE_TYPE = "landing";
    String STATIC_ERROR_PAGE_TYPE = "static-error";
    String HOME_PAGE_TYPE = "home";
    String HOME_PAGE_CODE = "Home";
    String PROFESSIONAL_LIABILITY_HOME = "ProfessionalLiability";
    String POPUP_PAGE_TYPE = "popup";

    String STR_OUTPUT = "Output";
    String TEXT_ITEM = "TextItem";
    String TEXT_ITEM_HELP_TEXT = "HelpText";
    String TEXT_ITEM_TEXT_CODE = "TextCode";
    String TEXT_ITEM_LABEL = "Label";
    String TEXT_ITEM_ERROR_TEXT = "ErrorText";
    String TEXT_ITEM_STYLE = "Style";
    String TEXT_ITEM_PURPOSE = "Purpose";
    String TEXT_ITEM_HELP_TITLE = "HelpTitle";
    String TEXT_ITEM_DEFAULT_VALUE = "DefaultValue";
    String TEXT_ITEM_ADD_TEXT1 = "AdditionalText1";
    String TEXT_ITEM_ADD_TEXT2 = "AdditionalText2";
    String TEXT_ITEM_ADD_TEXT3 = "AdditionalText3";
    String TEXT_ITEM_ADD_TEXT4 = "AdditionalText4";
    String TEXT_ITEM_ADD_TEXT5 = "AdditionalText5";
    String TEXT_ITEM_ADD_TEXT6 = "AdditionalText6";
    String TEXT_ITEM_ADD_TEXT7 = "AdditionalText7";
    String TEXT_ITEM_ADD_TEXT8 = "AdditionalText8";
    String TEXT_ITEM_ADD_TEXT9 = "AdditionalText9";

    String SYS_ERROR_PAGE = "http://www.hiscox.com";
    String NAVIGATION_RULES_PATH = "WEB-INF/navigation-rules.xml";
    String BROCHERWARE_REQ_PATH = "WEB-INF/broacher-request.xml";

    // pages
    String ABOUT_YOU = "about-you";
    String YOUR_BUSINESS = "your-business";
    String YOUR_QUOTE = "your-quote";
    String PROCESSING_YOUR_PAYMENT_URL = "quote-and-buy/processing-your-payment/";
    String CALCULATING_YOUR_QUOTE_URL = "quote-and-buy/calculating-your-quote/";
    String CARD_PAYMENT = "card-payment";
    String SAVE_QUOTE = "save-quote";
    String RETRIEVE_A_QUOTE = "retrieve-a-quote";
    String PAYMENT_DETAILS = "quote-and-buy/payment-details/";
    //String SESSION_TIME_OUT = "session-timeout";
    String SESSION_TIME_OUT_URL = "/quote-and-buy/session-timeout/";
    String QUOTE_PASSED_TO_AGENT = "quote-and-buy/quote-passed-to-agent";
    String PAY_BY_PHONE = "quote-and-buy/pay-by-phone";
    String DATACASH_CONFIRMATION = "datacash-confirmation";
    String CONFIRMATION = "Confirmation";

    // navigation rules tag names
    String STR_NAVIGATION = "navigation";
    String STR_PAGE = "page";
    String STR_VALUE = "value";
    String STR_TYPE = "type";
    String STR_BARITEM = "bar-item";
    String STR_RULE = "rule";
    String STR_IS_CHILD_PAGE = "isChildPage";
    String STR_IS_LANDING_PAGE = "isLandingPage";
    String STR_IS_LOGIN_REQUIRED = "isLoginRequired";
    String STR_FOLDER_NAME = "folderName";
    String STR_LOCKED_ACCOUNT = "quote-and-buy/locked-account";
    String STR_LDAP_URL = "ldapsName";
    String STR_MANAGER_DN = "managerDn";
    String STR_ACEGY_ERROR_MSG = " Error: Please review and amend the fields below. If you are having problems logging in or have forgotten your password, please use the 'Forgotten your password?' link below.";
    String STR_SENDER_ID = "SenderID";
    String STR_LANDING_PAGE = "LandingPage";
    String STR_NAK_STATUS = "Nak";
    String STR_BUSINESS_NAK = "BusinessNak";
    String STR_ACK_STATUS = "Ack";
    String STR_STATUS = "Status";
    String STR_ERRORS = "Errors";
    String STR_SYSTEM_ERROR = "quote-and-buy/system-error";
    String STR_404_ERROR_PAGE_URL = "quote-and-buy/404-error";
    String STR_404_ERROR_PAGE = "404-error";
    String STR_UNABLE_TO_NAVIGATE_BACK_PAGE = "unable-to-navigate-back";
    String STR_ERROR_MANAGEMENT_REQUEST_SMALL_CREATE_ERROR_START = "<createError>";
    String STR_ERROR_MANAGEMENT_REQUEST_SMALL_CREATE_ERROR_END = "</createError>";
    String STR_ERROR_MANAGEMENT_REQUEST_CREATE_ERROR_START = "<CreateError>";
    String STR_ERROR_MANAGEMENT_REQUEST_CREATE_ERROR_END = "</CreateError>";
    String STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_AREA_START = "<ApplicationArea>";
    String STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_AREA_END = "</ApplicationArea>";
    String STR_ERROR_MANAGEMENT_REQUEST_MESSAGE_ID_START = "<MessageID>";
    String STR_ERROR_MANAGEMENT_REQUEST_MESSAGE_ID_END = "</MessageID>";
    String STR_ERROR_MANAGEMENT_REQUEST_CREATION_DATE_TIME_START = "<CreationDateTime>";
    String STR_ERROR_MANAGEMENT_REQUEST_CREATION_DATE_TIME_END = "</CreationDateTime>";
    String STR_ERROR_MANAGEMENT_REQUEST_DATA_AREA_START = "<DataArea>";
    String STR_ERROR_MANAGEMENT_REQUEST_DATA_AREA_END = "</DataArea>";
    String STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_SERVER_NAME_START = "<ApplicationServerName>";
    String STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_SERVER_NAME_END = "</ApplicationServerName>";
    String STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_NAME_START = "<ApplicationName>";
    String STR_ERROR_MANAGEMENT_REQUEST_APPLICATION_NAME_END = "</ApplicationName>";
    String STR_ERROR_MANAGEMENT_REQUEST_COMPONENT_NAME_START = "<ComponentName>";
    String STR_ERROR_MANAGEMENT_REQUEST_COMPONENT_NAME_END = "</ComponentName>";
    String STR_ERROR_MANAGEMENT_REQUEST_MAIN_SERVICE_START = "<MainService>";
    String STR_ERROR_MANAGEMENT_REQUEST_MAIN_SERVICE_END = "</MainService>";
    String STR_ERROR_MANAGEMENT_REQUEST_ERROR_CODE_START = "<ErrorCode ";
    String STR_ERROR_MANAGEMENT_REQUEST_ERROR_CODE_END = "</ErrorCode>";
    String STR_ERROR_MANAGEMENT_REQUEST_ERROR_TYPE_START = "errorType='";
    String STR_ERROR_MANAGEMENT_REQUEST_ERROR_TYPE_END = "'>";
    String STR_ERROR_MANAGEMENT_REQUEST_ERROR_MESSAGE_START = "<ErrorMessage>";
    String STR_ERROR_MANAGEMENT_REQUEST_ERROR_MESSAGE_END = "</ErrorMessage>";
    String STR_ERROR_MANAGEMENT_REQUEST_ARRAY_OF_STRING_ITEM_START = "<ArrayOfstringItem>";
    String STR_ERROR_MANAGEMENT_REQUEST_ARRAY_OF_STRING_ITEM_END = "</ArrayOfstringItem>";
    String STR_ERROR_MANAGEMENT_REQUEST_ADDITIONAL_INFORMATION_START = "<AdditionalInformation>";
    String STR_ERROR_MANAGEMENT_REQUEST_ADDITIONAL_INFORMATION_END = "</AdditionalInformation>";
    String STR_QUOTE_REF_ID = "quoteRefID";
    String STR_CLIENT_IP = "clientIP";
    String STR_SCHEME_ID = "schemeID";
    String STR_EMAIL_ID = "emailID";
    String STR_ACC_LCK_USER_NAME = "AccLckUserName";
    String STR_RESET_PASSWORD_USER_NAME = "userName";
    String STR_GUID_TYPE = "guidType";
    String STR_YES = "Yes";
    String XML_ERROR_LIST = "errorList";
    String XML_ARRAY_OF_ERROR_ITEM = "ArrayOfErrorItem";
    String STR_CALCULATING_QUOTE = "Calculating your quote";
    String STR_PROCESSING_PAYMENT = "Processing your payment";

    String XML_ERROR_CODE = "ErrorCode";
    String XML_ERROR_TYPE = "type";
    String XML_ERROR_DESCRIPTION = "ErrorDescription";
    String XML_ERROR_INSTANCE_ID = "ErrorInstanceID";
    String XML_DATE_TIME = "DateTime";
    String XML_ADDITIONAL_INFORMATION = "AdditionalInformation";

    String SRVICE_BINDER = "ServiceBinder";
    String DEFAULT_URI = "DefaultUri";
    String REQUEST_XML = "RequestXml";
    String APPLICATION_SERVER_NAME = "ApplicationServerName";
    String MAIN_SERVICE = "MainService";
    String MESSAGE_ID = "MessageId";
    String APPLICATION_NAME = "ApplicationName";
    String COMPONENT_NAME = "ComponentName";
    String ARRAY_ERROR = "ArrayOfError";
    String ERROR_CODE = "ErrorCode";
    String STR_UI_ERROR_CODE = "0050015";
    String ERROR_TYPE = "errorType";
    String ERROR_DESCRIPTION = "ErrorDescription";
    String ERROR_INSTANCE_ID = "ErrorInstanceID";
    String DATE_TIME = "DateTime";
    String ADDITIONAL_INFORMATION = "AdditionalInformation";
    String STR_THOUSAND = "1000";
    String STR_TAG_LIST = "TagList";
    String STR_TAG_NAME = "tagName";
    String STR_TAG_VALUE = "tagValue";
    String STR_GET_SCREEN = "GET_SCREEN";
    String STR_SHOWPAGE = "ShowPage";
    String XML_FORM = "Form";
    String XML_HEADER = "<?xml version=\"1.0\"?>";
    String XML_HEADER_STRING = "&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;";
    String XML_HEADER_ENC_STRING = "&lt;?xml version=\"1.0\"?&gt;";
    String STR_NO = "NO";
    String STR_NEITHER = "Neither";
    String STR_LOGIN_INDICATOR = "loggedIn";
    String STR_CHECKBOX_VALIDATION_FLAG = "checkBoxValidationFlag";
    String STR_RADIO_BUTTON_VALIDATION_FLAG = "radioButtonValidationFlag";

    // constants used in Tags
    String HTML_FORM_CONTENT = "<form name = \"ScreenBuilderForm\"  id = \"ScreenBuilderForm\" method=\"post\" commandName=\"screen\">";
    String HTML_FORM_END = "</form>";
    String STR_START = "start";
    String STR_END = "end";
    String STR_EMAIL_ADDRESS = "EmailAddress";
    String STR_CHECKBOX = "checkbox";
    String STR_RADIO = "radio";
    String STR_SCRIPT = "script";
    String STR_TEXT = "text";
    String EMPTY_SPACE = " ";
    String SYMBOL_COLON = ":";
    String STR_JAVASCRIPT = "javascript";
    String STR_DEV_RESOURCES_CONTEXTPATH = "resources-usdirect";
    String STR_SYSTEST_RESOURCES_CONTEXTPATH = "resources";
    String STR_CALENDAR_JS = "calendar.js";
    String STR_SRC = "src";
    String EQUAL_TO = "=";
    String DOUBLE_QUOTE = "\"";
    String STR_HELPTEXTLABEL = "helpTextLabel";
    String STR_HELPTEXTCONTENT = "helpTextContent";
    String STR_RX_DEFAULT_OCCUPATION = "default-occupation-not-known";
    String STR_ALL = "all";
    String PRIMARY_BUSINESS_CODE = "primarybusiness";
    String STR_FOCUS_JS = "focus.js";
    String LOGOUT = "logout";
    String STATE = "state";
    String PRODUCT_OPTIONS_CHANGE_COVERAGE = "product-options-change-coverage";
    String PRODUCT_OPTIONS = "product-options";
    String LIFESTYLE_QUESTIONS = "lifestyle-questions";
    String READ_MORE = "read-more-";
    String QUOTE_AND_BUY_INFORMATION_PAGES = "quote-and-buy-information-pages";
    String COVERAGE_OPTIONS = "coverage-options";
    String COVERAGE_OPTIONS_CHANGE_COVERAGE = "coverage-options-change-coverage";
    String PAYMENT_TIME_OUT = "quote-and-buy/payment-problem";
    String PAYMENT_TIME_EXCEPTION = "PaymentTimeOutException";
    String SAVED_QUOTE_EXCEPTION ="SavedQuoteException";
    String STR_RX_PAGE_ERROR_HEADING ="generic-page-error-heading";
    String STR_RX_PAGE_ERROR_TOP_CENTER ="generic-top-center-error";


    // file name constants
    String APPCONFIG_PROPERTIES = "appConfigProperties";
    // end
    String ENVIRONMENT = "Environment";
    String QUARTZJOB_TRIGGER = "QuartzJobTrigger";
    String TRIGGER_INTERVAL1 = "triggerInterval1";
    String TRIGGER_INTERVAL2 = "triggerInterval2";
    String QUARTZJOB = "Quartzjob";
    String GUID = "guid";
    String STR_UN_LOCK_ACCOUNT_GUID = "unlockaccountguid";
    String STR_RESET_PASSWORD_GUID = "resetpasswordguid";
    //String STR_CARD_DETAILS_PAGE_NAME = "CardDetails";
    String STR_IS_END_OF_JOURNEY_PAGE = "isEndOfJourneyPage";
    String STR_PARENT_PAGE = "parentPage";
    String STR_IS_MULTIPLE_FORM = "ismultipleForm";
    String STR_PREVIOUS_PAGE_STATUS = "previousPageStatus";
    String STR_REG_NAV = "regularNavigationAllowed";

    String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";
    String DATE_FORMAT_MMYY = "MM/yy";
    // to be moved to app config
    String EM_DATE_TIME = "2010-08-22 12:00:00";
    String EM_APPLICATION_SERVER_NAME = "HSXUSDCUI";
    String EM_APPLICATION_NAME = "HSXUSDIRECT";
    String EM_COMPONENT_NAME = "ScreenBuilder";
    String EM_MAIN_SERVICE = "yyy";
    String EM_ERROR_TYPE = "Application";
    String EM_ERROR_CODE = "0050015";
    String EM_ERROR1 = "Error1";
    String EM_ADDITIONAL_INFORMATION = "Additional Information";
    String EM_MESSAGE_ID = "123456";

    String STR_QUESTION_GROUP = "QuestionGroup";
    String STR_SCREEN_QUESTION = "ScreenQuestion";
    String LDAP_USER_NAME = "j_username";
    String STR_WIDGET_TYPE_PROPERTIES = "widgetTypeProperties";
    String[] STR_IMPROPER_FILE_EXTENSION_URL_ARR =
    {".gif", ".css", ".js", ".jpg", ".png", ".htc",
	    "_https://seal.verisign.com/getseal" };
    String XML_STATUS = "Status";
    String XML_CORRELATION_ID = "CorrelationID";
    String STR_USER_AREA = "UserArea";
    String STR_UI_NAVIGATION_HISTORY = "UINavigationHistory";
    String STR_PAGE_VISITED = "PageVisited";
    String CONFIG_IS_VALIDATION_REQUIRED = "isValidationRequired";
    String CONFIG_IS_PROGRESS_BAR_REQUIRED = "isProgrssbarRequired";
    String CONFIG_IS_ENCRYPTION_REQUIRED = "isEncryptionRequired";
    String XML_ACTION = "Action";
    String STR_FORGOTTEN_PASSWORD = "ForgottenPassword";
    String STR_SAVE_QUOTE ="SaveQuote";
    String STR_RESET_PASSWORD = "ResetPassword";
    String STR_LOCK_ACCOUNT = "LockedAccount";
    String STR_QUOTE_SAVED = "QuoteSaved";
    String STR_SAVED_QUOTE = "SavedQuote";
    String QUOTE_SAVED_ACEGI_PAGE = "QuoteSaved";
    String SAVED_QUOTE_ACEGI_PAGE = "SavedQuote";
    String EMAIL_XPATH_VALUE = "Policy/Client/Individual/@mainEmail";
    String STR_XPATH = "xpath";
    String STR_JSP = "jsp";
    String STR_RUNTIMEADDED = "runtimeAdded";
    String STR_SCHEMEID_QUERY_STRING = "schemeID";
    String STR_GRP_VALIDTN = "groupValidationFlag";
    String STR_DEFAULT_NAVIGATOR = "defaultNavigator";
    String QUESTION_ID_SUFFIX = "_question_id";
    String STR_ADDGL = "AddGL";
    String STR_ADDENO = "AddEnO";
    String STR_REQUESTCALLBACK = "RequestCallBack";
    String STR_EMAILADDRESS_NEW = "EmailAddress_New";
    String STR_IS_CALENDAR_REQUIRED = "isCalendarRequired";
    String STR_IS_THROBBER_PAGE = "isThrobberPage";
    String STR_UPDATE_PRODUCTS = "updateProducts";
    String STR_BACK = "Back";
    String STR_RETRIEVE_A_QUOTE_BUTTON_NAME = "RetrieveAQuote";
    String STR_LOGOUT_BUTTON_NAME = "Log Out";
    String STR_REFERENCE_NUMBER = "ReferenceNumber";
    String STR_SAVED_QUOTE_QUESTION_CODE = "savedQuote";
    String STR_PRINT = "Print";
    String STR_IMPROPER_SCHEME_ID_ERROR_MESSAGE = "Improper schemeID entered by the user";
    String STR_TIMESTAMP_FORMAT = "dd-MM-yyyy hh:mm:ss";
    String STR_SESSION_IDENTOFIER_TIMESTAMP_FORMAT = "ddMMyyyyhhmmss";
    String NONE_OF_THE_ABOVE_KEY = "NONE OF THE ABOVE; Please explain";
    String PROFESSIONAL_LIABILITY_KEY = "professionalliability";
    String PRIMARY_BUSINESS_EXPLAIN_KEY = "primarybusinessexplain";
    String CSS_ERROR = "error";
    String BROACHER_WARE_ERROR_PAGE = "small-business-insurance-error";
    String BROACHER_WARE_PAGE = "broacherware";
    String BROACHER_TYPE = "broacher";
    String OCCUPATION_PROPERTIES = "occupationProperties";
    String OCCUPATION_VARIANT_PROPERTIES = "occupVariantProperties";
    String STATE_VARIANT_PROPERTIES = "stateVariantProperties";
    String STATE_VARIANT = "stateVariant";
    String OCCUPATION_VARIANT = "occupationVariant";
    String RETRIEVE = "retrieve";
    String GET_PAGE_SERVICE_CLIENT = "getPageServiceClient";
    String TEXT_MNGR_SERVICE_CLIENT = "textMngrServiceClient";
    String SECONDARY_STREET = "secondarystreet";
    String STR_DOUBLE_FORWARD_SLASH = "//";
    String STR_COLON = ":";
    String STR_SECURE_APPENDER = "s";

    //PCI changes
    String STR_URL_DATACASH_REFERENCE = "dts_reference";
    String STR_IS_JS_ENABLED = "isJSEnabled";
    String STR_CONFIRM_PAYMENT_OPTIONS_ACTION = "ConfirmPaymentOptions";
    String STR_MAKE_PAYMENT_ACTION = "MakePayment";
    String STR_PAYMENT_DETAILS_FOLDER = "quote-and-buy/payment-details";
    String STR_DATACASH_PO_QUESTIONCODE = "PayMnthlyAnnual";
    String STR_DATACASH_QUESTIONREF = "dataCashQuoteRefId";
    String STR_DATACASH_PO_QUESTIONCODE_ANS = "dataCashPaymentOption";
    String STR_DATACASH_PO_ADDTVALUE1 = "dataCashPOAdditionalValue1";
    String STR_DATACASH_PO_ADDTVALUE2 = "dataCashPOAdditionalValue2";
    String STR_DATACASH_PO_ADDTVALUE3 = "dataCashPOAdditionalValue3";
    String STR_PV_MONTHLY = "Monthly";
    String STR_PV_ANNUALLY = "Annually";
    String HTML_LESSTHAN_SYMBOL = "<";
    String HTML_SPAN = "span";
    String HTML_EMPTYSPACE = " ";
    String HTML_CLASS = "class";
    String HTML_EQUALS_SYMBOL = "=";
    String HTML_SINGLEQUOTE = "'";
    String HTML_CLASSNAME_ANSFX = "ans-fx";
    String HTML_CLASSNAME_ANSIMP = "ans-imp";
    String HTML_GREATERTHAN_SYMBOL = ">";
    String HTML_SPAN_ENDTAG = "</span>";
    String HTML_PO_CONSTANT1 = "Initial payment of ";
    String HTML_US_CURRENCY = "$";
    String HTML_PO_CONSTANT2 = "(two months premium) followed by 10 monthly payments of ";
    String HTML_FULL_STOP = ".";
    String HTML_PO_CONSTANT3 = "Total payment over 12 months is ";
    
	/**Added for US25094-Add Accounting PL to DPD **/
    String ENO_PRODUCT = "EnoProduct";    
    String STR_ABOUT_YOU="AboutYou";
    String CONFIG_PRODUCT_NAME="productName";
    String STR_EO="EO";
    String CONFIG_CLASS_OF_BUSINESS="classOfBusiness";
    String STR_COVERAGE_OPTIONS="UserSpecificBusinessInsurance";
    

}

