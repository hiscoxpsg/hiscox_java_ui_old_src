package com.hiscox.sbweb.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.CharacterIterator;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.acegisecurity.AuthenticationException;
import org.acegisecurity.ui.AbstractProcessingFilter;
import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.util.HsxCoreLDAPUtil;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;

/**
 * @author Cognizant
 *
 */
public class HsxSBWebPageBeanUtil implements IHsxSBWebConstants {

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    // /**
    // * This methods creates a base home URl from the requested Url
    // *
    // * @param aURL
    // * @return
    // * @throws Exception
    // */
    // public static String getHomeURL(String aURL) throws Exception
    // {
    //
    // URL requestURL;
    // StringBuilder path = new StringBuilder();
    // requestURL = new URL(aURL);
    // String urlPath = requestURL.getPath();
    // path.append(requestURL.getProtocol());
    // path.append("://");
    // path.append(requestURL.getAuthority());
    //
    // StringTokenizer stringTokenizer = new StringTokenizer(urlPath, "/");
    // String[] stringArray = new String[stringTokenizer.countTokens()];
    // int stringArrayLength = stringArray.length;
    // for (int i = 0; stringTokenizer.hasMoreTokens()
    // && i < stringArrayLength; i++)
    // {
    // stringArray[i] = "/" + stringTokenizer.nextToken();
    //
    // if (i == 1)
    // {
    // stringArray[i] = "";
    // break;
    // }
    // path.append(stringArray[i]);
    // }
    // path.append("/");
    // return path.toString();
    // }

    /**
     * This methods converts given xml document to String.
     *
     * @param doc
     * @return writer or null
     */
    public static String getStringFromDocument(Document doc) {
	try {
	    DOMSource domSource = new DOMSource(doc);
	    StringWriter writer = new StringWriter();
	    StreamResult result = new StreamResult(writer);
	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    transformer.transform(domSource, result);
	    return writer.toString();
	} catch (TransformerException ex) {
	    logger.error("TransformerException", ex);
	    return null;
	}

    }

    /**
     * This method prints the xml data with a cleaner indentation.
     *
     * @param document
     */
    public static void printXmlData(Document document) {
	TransformerFactory transformerFactory = TransformerFactory
		.newInstance();
	Transformer transformer;
	try {
	    transformer = transformerFactory.newTransformer();
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty(
		    "{http://xml.apache.org/xslt}indent-amount", "2");
	    DOMSource source = new DOMSource(document);
	    StreamResult result = new StreamResult(System.out);

	    // logger.info(SBWEB, INFO,"samp :"+samp);
	    transformer.transform(source, result);
	    // logger.info(SBWEB, INFO,"xml :"+result.toString());

	} catch (TransformerConfigurationException e) {
	    // TODO Auto-generated catch block
	    logger.error("TransformerConfigurationException",e);
	} catch (TransformerException e) {
	    // TODO Auto-generated catch block
	   logger.error("TransformerException", e);
	}
    }

    /**
     * This methods encodes a given String.
     *
     * @param strToEncode
     * @return result or strToEncode
     */
    public static String encode(String strToEncode) {
	final StringBuilder result = new StringBuilder();

	if (strToEncode != null && !"".equalsIgnoreCase(strToEncode)) {

	    final StringCharacterIterator iterator = new StringCharacterIterator(
		    strToEncode);
	    char character = iterator.current();

	    while (character != CharacterIterator.DONE) {
		if (character == '<') {
		    result.append("&lt;");
		} else if (character == '>') {
		    result.append("&gt;");
		//}
		// else if (character == '\"')
		// {
		// result.append("&quot;");
		 }else if (character == '\'') {
		    result.append("&#039;");
		} else if (character == '&') {
		    result.append("&amp;");
		} else {
		    // the char is not a special one
		    // add it to the result as is
		    result.append(character);
		}
		character = iterator.next();
	    }
	    return result.toString();
	} else {
	    return strToEncode;
	}

    }

    /**
     * This method decode a given String.
     *
     * @param strToDecode
     * @return decodedString or strToDecode
     */
    public static String decode(String strToDecode) {

	String decodedString = strToDecode; // URLDecoder.decode(strToDecode,
	// "UTF-8");

	if (strToDecode != null && !"".equalsIgnoreCase(strToDecode)) {
	    if (decodedString.contains("&amp;")) {
		decodedString = decodedString.replaceAll("&amp;", "&");
	    }

	    if (decodedString.contains("&lt;")) {
		decodedString = decodedString.replaceAll("&lt;", "<");
	    }
	    if (decodedString.contains("&gt;")) {
		decodedString = decodedString.replaceAll("&gt;", ">");
	    }
	    if (decodedString.contains("&quot;")) {
		decodedString = decodedString.replaceAll("&quot;", "\"");
	    }
	    if (decodedString.contains("&#039;")) {
		decodedString = decodedString.replaceAll("&#039;", "\'");
	    }
	    return decodedString;
	} else {
	    return strToDecode;
	}

    }

    public static String getXml(String pageName) {
	StringBuilder inputXML = new StringBuilder();
	// replace the below code to retrieve IPEXML from rule engine
	try {
	    String[] tokens = pageName.split("/");
	    int tokenLength = tokens.length;
	    if (tokenLength > 0) {
		// logger.info(SBWEB, INFO,tokenLength);
		pageName = tokens[tokenLength - 1];
	    }
	    File f = new File("C:\\TestData\\" + pageName.trim() + ".xml");
	    RandomAccessFile rnd;
	    rnd = new RandomAccessFile(f, "rw");
	    String inter = rnd.readLine();
	    while (inter != null) {
		inputXML.append(inter);
		inter = rnd.readLine();
	    }
	    rnd.close();
	} catch (Exception e) {
		logger.error("Exception", e);
	}
	return inputXML.toString();

    }

    /**
     * Only for Testing to retrieve the Text manager data.
     *
     * @return inputXML
     */
    public static String getTextXml() {
	StringBuilder inputXML = new StringBuilder();
	// replace the below code to retrieve IPEXML from rule engine
	try {
	    File f = new File("\\TestData\\TextResponse.xml");
	    RandomAccessFile rnd;
	    rnd = new RandomAccessFile(f, "rw");
	    String inter = rnd.readLine();
	    while (inter != null) {
		inputXML.append(inter);
		inter = rnd.readLine();
	    }
	    rnd.close();
	} catch (Exception e) {
		logger.error("Exception", e);
	}
	return inputXML.toString();
    }

    /**
     * This method will update the pageHistory string if the previous page is
     * edited and submitted then the later pages in the pageHistory string will
     * be removed.
     *
     * @param pageName
     * @param pageHistory
     * @return updatedPageHisory
     */
    public static String updatePageHistory(String pageName, String pageHistory) {

	String updatedPageHisory = pageName;
	if (!pageHistory.equalsIgnoreCase(pageName)) {
	    String[] pageItems = pageHistory.split(pageName);
	    if (pageItems.length >= 1) {
		updatedPageHisory = pageItems[0] + pageName;
	    }
	}
	return updatedPageHisory;
    }

    public static Document getDocumentFromString(String xmlSource) {
	if (StringUtils.isNotBlank(xmlSource)) {
	    xmlSource = xmlSource.replaceAll(XML_HEADER, STR_EMPTY);
	    DocumentBuilderFactory factory = DocumentBuilderFactory
		    .newInstance();
	    factory.setNamespaceAware(true);
	    DocumentBuilder builder = null;
	    try {
		builder = factory.newDocumentBuilder();
		InputSource inputSource = new InputSource(new StringReader(
			xmlSource));
		if (inputSource != null) {
		    return builder.parse(inputSource);
		}
	    } catch (ParserConfigurationException e) {
		// TODO Auto-generated catch block
	    	logger.error("ParserConfigurationException", e);
	    } catch (SAXException e) {
		// TODO Auto-generated catch block
		logger.error("SAXException", e);
	    } catch (IOException e) {
		// TODO Auto-generated catch block
	    	logger.error("IOException", e);
	    }

	}
	return null;

    }

    /**
     * This mehtod returns an array of strings by tokenizing the input string.
     * @return tokenArray
     */
    public static String[] getStringTokens(String string, String seperator) {
	StringTokenizer stringTokenizer = new StringTokenizer(string, seperator);
	String[] tokenArray = new String[stringTokenizer.countTokens()];
	int tokenArrayLength = tokenArray.length;
	for (int i = 0; stringTokenizer.hasMoreTokens() && i < tokenArrayLength; i++) {
	    tokenArray[i] = stringTokenizer.nextToken();
	}

	return tokenArray;

    }

    /**
     * This method retrieves the username from the LDAP and sets it to the
     * webBean.
     *
     * @param webBean
     * @param guid
     * @param ldapsUtil
     * @param session
     * @param appConfigProperties
     * @throws HsxCoreRuntimeException
     * @throws HsxCoreException
     */
    public static void getUsernameFromLDAP(HsxSBWebBean webBean,
	    String guidType, String guid, HsxCoreLDAPUtil ldapsUtil,
	    Properties appConfigProperties) throws HsxCoreRuntimeException,
	    HsxCoreException {
	String ldapName = appConfigProperties.getProperty(STR_LDAP_URL).trim();
	String managerDn = appConfigProperties.getProperty(STR_MANAGER_DN)
		.trim();
	String password = appConfigProperties.getProperty(STR_ACEGI_PASSWORD)
		.trim();
	String username = ldapsUtil.getVerificationDetailsBasedonGuidType(
		guidType, guid, ldapName, managerDn, password);
	logger.info(SBWEB, INFO, "User name : " + username);
	logger.info(SBWEB, INFO, "GuId Type: " + guidType);
	webBean.setGuidType(guidType);
	webBean.setUserName(username);
    }

    /**
     * This method returns to the account locked page if the user tries to login
     * with bad password for more that 4 times.
     *
     * @param webBean
     * @param homeURL
     * @param authFailed
     * @param ldapsUtil
     * @param session
     * @param pagesVisited
     * @param acegiView
     * @param appConfigProperties
     * @return acegiView
     * @throws HsxCoreException
     * @throws HsxCoreRuntimeException
     */
    public static ModelAndView checkAccountLocked(HsxSBWebBean webBean,
	    String authFailed, HsxCoreLDAPUtil ldapsUtil, HttpSession session,
	    String pagesVisited, ModelAndView acegiView,
	    Properties appConfigProperties) throws HsxCoreRuntimeException,
	    HsxCoreException {
	if (authFailed != null && authFailed.equalsIgnoreCase(STR_TRUE)) {

	    String ldapName = appConfigProperties.getProperty(STR_LDAP_URL)
		    .trim();
	    String managerDn = appConfigProperties.getProperty(STR_MANAGER_DN)
		    .trim();
	    String password = appConfigProperties.getProperty(
		    STR_ACEGI_PASSWORD).trim();
	    String authenticationException = null;
	    if (session != null
		    && session
			    .getAttribute(AbstractProcessingFilter.ACEGI_SECURITY_LAST_EXCEPTION_KEY) != null) {
		authenticationException = ((AuthenticationException) session
			.getAttribute(AbstractProcessingFilter.ACEGI_SECURITY_LAST_EXCEPTION_KEY))
			.getMessage();
	    }

	    if (authenticationException != null
		    && authenticationException.contains(STR_BAD_CREDENTIALS)) {
		authenticationException = STR_BAD_CREDENTIALS;

		if (session != null
			&& session
				.getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY) != null) {
		    String emailId = (String) session
			    .getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
		    if (emailId != null) {
			String badCount = ldapsUtil.getBadCredential(emailId,
				ldapName, managerDn, password);
			logger.info(SBWEB, INFO, "badCount :" + badCount
				+ "for email id :" + emailId);
			// Reset AcountLocked User Name
			webBean.setAccLckUserName(null);
			if (badCount != null
				&& !STR_EMPTY.equalsIgnoreCase(badCount)) {
			    int badCountInt = Integer.valueOf(badCount);
			    if (badCountInt > 4) {
				logger.info(SBWEB, INFO,
					"The Bad Password Count has exceeded 4 times."
						+ emailId);
				// redirect to the Unlock account page
				String pageName = webBean.getPagesVisited();
				if (pagesVisited == null) {
				    pagesVisited = pageName;
				    webBean.setPagesVisited(pagesVisited);
				} else {
				    pagesVisited = pagesVisited
					    .concat(STR_COMA).concat(
						    STR_LOCKED_ACCOUNT);
				    webBean.setPagesVisited(pagesVisited);

				}
				// EmailId For the Account Locked User is stored
				// to
				// Transmit the email id in the Request which
				// will
				// be sent to Jrules
				webBean.setAccLckUserName(emailId);
				String lockedAccountViewName = STR_FORWARD_SLASH
					+ STR_LOCKED_ACCOUNT;
				logger.info(SBWEB, INFO,
					"Relative Url View Name For the Locked Account :"
						+ STR_LOCKED_ACCOUNT);
				acegiView = new ModelAndView(new RedirectView(
					lockedAccountViewName, true));
			    }
			}
		    }
		}
	    }
	}

	return acegiView;
    }

    /**
     * This method checks if there is any login errors and then it updates the
     * error messages in the page.
     *
     * @param webBean
     * @param session
     * @throws HsxSBUIBuilderException
     * @throws HsxSBUIBuilderRuntimeException
     */
    public static void checkBadCredentials(HsxSBWebBean webBean,
	    HttpSession session) throws HsxSBUIBuilderRuntimeException,
	    HsxSBUIBuilderException {
	if (session != null
		&& session
			.getAttribute(AbstractProcessingFilter.ACEGI_SECURITY_LAST_EXCEPTION_KEY) != null) {
	    String authenticationException = ((AuthenticationException) session
		    .getAttribute(AbstractProcessingFilter.ACEGI_SECURITY_LAST_EXCEPTION_KEY))
		    .getMessage();
	    logger.error(SBWEB, ERROR, "The error message from the LDAP is :"
		    + authenticationException);
	    String customErrMsg = STR_EMPTY;
	    if (authenticationException.contains(STR_BAD_CREDENTIALS)) {
		authenticationException = STR_BAD_CREDENTIALS;
	    } else if (authenticationException.contains(STR_EMPTY_USERNAME)) {
		authenticationException = STR_EMPTY_USERNAME;
	    } else if (authenticationException.contains(STR_EMPTY_PASSWORD)) {
		authenticationException = STR_EMPTY_PASSWORD;
	    }

	    if (null != authenticationException
		    && authenticationException.trim().length() > 0) {
		authenticationException = authenticationException
			.concat(STR_EMPTY);
		customErrMsg = STR_ACEGY_ERROR_MSG;
		logger.info(SBWEB, INFO, "The Error Message is  :"
			+ customErrMsg);
	    }
	    logger.info(SBWEB, INFO, "Widget for Retrieve Quote  ---> :"
		    + session.getAttribute(WIDGET));
	    webBean.setRetrieveQuoteSessionExpiryFlag(false);
	    if (session.getAttribute(WIDGET) == null) {
		// This will mean that Session would have expired and we would
		// not have anything to display and hence redirect this user to
		// SessionTime out page
		webBean.setRetrieveQuoteSessionExpiryFlag(true);
	    }

	    if (webBean.getWidget() == null) {
		webBean.setWidget((HsxSBWidget) session.getAttribute(WIDGET));
	    }

	    HsxSBUIBuilderControllerImpl ui = new HsxSBUIBuilderControllerImpl();
	    if (webBean.getWidget() != null) {
		ui.updateQuestionsForAcegy(webBean.getWidget(), customErrMsg);
	    }
	}
    }

    /**
     * To encode textData having special characters.
     *
     * @param strToDecode
     * @return strToDecode
     */
    public static String decodeTxData(String strToDecode) {
	if (StringUtils.isNotBlank(strToDecode)) {
	    strToDecode = strToDecode.replaceAll("&amp;", "&");
	}

	return strToDecode;
    }

    /**
     * To decode responseXml Data having special characters.
     *
     * @param strToDecode
     * @return strToDecode
     */

    public static String encodeResponseXmlData(String strToDecode) {
	if (strToDecode != null) {
	    strToDecode = strToDecode.replaceAll("&", "&amp;");
	}
	return strToDecode;
    }

    /**
     * To decode text Data having special characters.
     *
     * @param strToDecode
     * @return strToDecode
     */
    public static String encodeTextData(String strToDecode) {
	if (StringUtils.isNotBlank(strToDecode)) {
	    strToDecode = strToDecode.replaceAll("�", "&#180;");
	    strToDecode = strToDecode.replaceAll("�", "&#45;");
	    strToDecode = strToDecode.replaceAll("\"", "&#34;");

	}
	return strToDecode;
    }

    public static Map<String, String> getMapFromProperty(
	    Properties propertyfile, Map<String, String> propertyMap) {

	Set<Object> keySet = propertyfile.keySet();

	for (Iterator<Object> iterator = keySet.iterator(); iterator.hasNext();) {
	    String key = (String) iterator.next();
	    String value = propertyfile.getProperty(key);
	    propertyMap.put(key, value.trim());

	}
	return propertyMap;

    }

    /**
     * This methods converts given xml document's childNode to String.
     *
     * @param doc
     * @return string writer
     */
    public static String getStringFromNodeList(Document showPageDocument)
	    throws TransformerFactoryConfigurationError, TransformerException {
	showPageDocument.normalizeDocument();
	TransformerFactory tFactory = TransformerFactory.newInstance();
	Transformer transformer = null;
	transformer = tFactory.newTransformer();
	transformer.setOutputProperty(OutputKeys.INDENT, "no");
	StringWriter sw = new StringWriter();
	StreamResult result = new StreamResult(sw);
	Element showPageElement = (Element) showPageDocument
		.getElementsByTagNameNS(STR_ASTERISK, "ShowPageXML").item(0);
	if (showPageElement != null) {
	    NodeList nl = showPageElement.getChildNodes();
	    DOMSource source = null;
	    for (int x = 0; x < nl.getLength(); x++) {
		Node e = nl.item(x);
		if (e instanceof Element) {
		    source = new DOMSource(e);
		    break;
		}
	    }

	    // Do the transformation and output

	    transformer.transform(source, result);
	}
	return sw.toString();
    }

    public static String generateUniqueSessionIdentifier()
	    throws UnknownHostException {
	StringBuilder sessionIdentifier = new StringBuilder();
	Calendar calendar = Calendar.getInstance();
	sessionIdentifier.append(getHostName());
	sessionIdentifier.append(getCurrentTimeStampForSessionIdentifier());
	sessionIdentifier.append(calendar.get(Calendar.MILLISECOND));

	Random random = new Random();
	int randomNumber = random.nextInt(100000000);
	sessionIdentifier.append(randomNumber);
	return sessionIdentifier.toString();
    }

    /**
     * @param webBean
     */
    public static void resetWebBean(HsxSBWebBean webBean) {
	webBean.setSessionIdentifierValue(null);
	if (webBean.getSessionMap() != null) {
	    webBean.getSessionMap().clear();
	}
	webBean.setLoginIndicator(null);
	webBean.setQuoteRefID(null);
	webBean.setWidget(null);
	webBean.setWidgetMap(null);
    }

    /**
     *
     * @param map
     * @return boolean value
     */
    public static boolean isMapEmpty(Map<String, String> map) {
    	boolean result;

	if (null != map && !map.isEmpty()) {
		result= false;
	} else {
		result= true;
	}
    	return result;
    }
    public static void printMap(Map<String, String> map) {
	Set<Map.Entry<String, String>> set = map.entrySet();
	Iterator<Map.Entry<String, String>> iterator = set.iterator();
	while (iterator.hasNext()) {
	    final Entry<String, String> entry = iterator.next();
	    logger.info(SBWEB, INFO, entry.getKey() + " = " + entry.getValue());
	}
    }

    public static void logException(HsxSBWebBean webBean, Exception e) {
	String exceptionName = "Exception";
	if (e.getClass() != null) {
	    exceptionName = e.getClass().getCanonicalName();
	}
	HsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(
		webBean, exceptionName);
    }

    public static String getCurrentTimeStamp() {
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat(STR_TIMESTAMP_FORMAT);
	String currentTimeStamp = STR_BLANK;
	if (sdf != null && cal != null && cal.getTime() != null) {
	    currentTimeStamp = sdf.format(cal.getTime());
	}
	return currentTimeStamp;
    }

    public static String getHostName() throws UnknownHostException {
	String hostName = STR_EMPTY;
	if (InetAddress.getLocalHost() != null
		&& InetAddress.getLocalHost().getHostName() != null) {
	    hostName = InetAddress.getLocalHost().getHostName();
	}

	return hostName;

    }

    public static String getCurrentTimeStampForSessionIdentifier() {
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat sdf = new SimpleDateFormat(
		STR_SESSION_IDENTOFIER_TIMESTAMP_FORMAT);
	String currentTimeStamp = STR_BLANK;
	if (sdf != null && cal != null && cal.getTime() != null) {
	    currentTimeStamp = sdf.format(cal.getTime());
	}
	return currentTimeStamp;
    }

    public static String getHostURL(String aURL) {

	URL requestURL;
	StringBuilder path = new StringBuilder();
	try {
	    requestURL = new URL(aURL);
	    path.append(requestURL.getProtocol());
	    if (!requestURL.getProtocol().endsWith(STR_SECURE_APPENDER)) {
	    path.append(STR_SECURE_APPENDER);
	    }
	    path.append(STR_COLON);
	    path.append(STR_DOUBLE_FORWARD_SLASH);
	    path.append(requestURL.getAuthority());
	    path.append(STR_FORWARD_SLASH);

	} catch (MalformedURLException e) {
	    logger.error("MalformedURLException", e);
	}
	return path.toString();
    }

    public static String getAbsoluteURL(String pageName, String bURL, HsxSBWebBean webBean) {

	StringBuilder absolutePath = new StringBuilder();
	absolutePath.append(XML_CDATA_START_TAG);
	absolutePath.append(bURL);
	absolutePath.append(SMALL_BUSINESS_INSURANCE);
	absolutePath.append(STR_FORWARD_SLASH);
	absolutePath.append(pageName);
	if (STR_YES.equalsIgnoreCase(webBean.getIsJSEnabled())) {
	    absolutePath.append(STR_FORWARD_SLASH);
	}
	absolutePath.append(XML_CDATA_END_TAG);
	return absolutePath.toString();
    }

    public static String getAbsoluteResourcesProtocol(String httpProtocol, String bURL) {

	StringBuilder absolutePath = new StringBuilder();

	URL requestURL;
	try {
	    requestURL = new URL(bURL);
	absolutePath.append(XML_CDATA_START_TAG);
	absolutePath.append(httpProtocol);
	absolutePath.append(STR_COLON);
	absolutePath.append(STR_DOUBLE_FORWARD_SLASH);
	absolutePath.append(requestURL.getAuthority());
	absolutePath.append(XML_CDATA_END_TAG);
	} catch (MalformedURLException e) {
	   logger.error("MalformedURLException", e);
	}
	return absolutePath.toString();
    }
    public static String getDataCashBuildHTMLOne(HsxSBWebBean webBean) {

	//StringBuilder htmlBuildOne = new StringBuilder();
	String additionalValue = STR_EMPTY;
	String savedValue = STR_EMPTY;
	if (webBean.getSessionMap().get(STR_DATACASH_PO_QUESTIONCODE_ANS) != null) {
	    savedValue = webBean.getSessionMap().get(STR_DATACASH_PO_QUESTIONCODE_ANS);
	    if (STR_PV_MONTHLY.equalsIgnoreCase(savedValue)) {
		additionalValue = webBean.getSessionMap().get(STR_DATACASH_PO_ADDTVALUE2);
	    } else if (STR_PV_ANNUALLY.equalsIgnoreCase(savedValue)) {
		additionalValue = webBean.getSessionMap().get(STR_DATACASH_PO_ADDTVALUE3);
	    }
	}
	/*htmlBuildOne.append(XML_CDATA_START_TAG);
	htmlBuildOne.append(HTML_LESSTHAN_SYMBOL);
	htmlBuildOne.append(HTML_SPAN);
	htmlBuildOne.append(HTML_EMPTYSPACE);
	htmlBuildOne.append(HTML_CLASS);
	htmlBuildOne.append(HTML_EQUALS_SYMBOL);
	htmlBuildOne.append(HTML_SINGLEQUOTE);
	htmlBuildOne.append(HTML_CLASSNAME_ANSFX);
	htmlBuildOne.append(HTML_SINGLEQUOTE);
	htmlBuildOne.append(HTML_GREATERTHAN_SYMBOL);
	htmlBuildOne.append(savedValue);
	htmlBuildOne.append(HTML_SPAN_ENDTAG);

	htmlBuildOne.append(HTML_LESSTHAN_SYMBOL);
	htmlBuildOne.append(HTML_SPAN);
	htmlBuildOne.append(HTML_EMPTYSPACE);
	htmlBuildOne.append(HTML_CLASS);
	htmlBuildOne.append(HTML_EQUALS_SYMBOL);
	htmlBuildOne.append(HTML_SINGLEQUOTE);
	htmlBuildOne.append(HTML_CLASSNAME_ANSIMP);
	htmlBuildOne.append(HTML_SINGLEQUOTE);
	htmlBuildOne.append(HTML_GREATERTHAN_SYMBOL);
	//htmlBuildOne.append(HTML_US_CURRENCY);
	htmlBuildOne.append(additionalValue);
	htmlBuildOne.append(HTML_SPAN_ENDTAG);
	htmlBuildOne.append(XML_CDATA_END_TAG);*/

	return additionalValue;

    }
    /**
     * This method creates HashMap for the given two dimensional array and returns the HashMap object.
     *
     * @param twoDStringArray
     * @return generatedHashMap
     */
    public static HashMap<String, String> createHashMap(String[][] twoDStringArray) {
        HashMap<String, String> generatedHashMap = new HashMap<String, String>();
        int twoDStringArrayLength = twoDStringArray.length;
        for (int i = 0; i < twoDStringArrayLength; i++) {
            generatedHashMap.put(twoDStringArray[i][0], twoDStringArray[i][1]);
        }
        return generatedHashMap;
    }

/*    public static String getDataCashBuildHTMLTwo(HsxSBWebBean webBean){

	StringBuilder htmlBuildTwo = new StringBuilder();
	String additionalValue1 = webBean.getSessionMap().get(STR_DATACASH_PO_ADDTVALUE1);
	String additionalValue2 = webBean.getSessionMap().get(STR_DATACASH_PO_ADDTVALUE2);
	String additionalValue3 = webBean.getSessionMap().get(STR_DATACASH_PO_ADDTVALUE3);


	htmlBuildTwo.append(XML_CDATA_START_TAG);

	htmlBuildTwo.append(HTML_LESSTHAN_SYMBOL);
	htmlBuildTwo.append(HTML_SPAN);
	htmlBuildTwo.append(HTML_GREATERTHAN_SYMBOL);

	htmlBuildTwo.append(HTML_PO_CONSTANT1);
	//htmlBuildTwo.append(HTML_US_CURRENCY);
	htmlBuildTwo.append(additionalValue1);
	htmlBuildTwo.append(HTML_EMPTYSPACE);
	htmlBuildTwo.append(HTML_PO_CONSTANT2);
	//htmlBuildTwo.append(HTML_US_CURRENCY);
	htmlBuildTwo.append(additionalValue2);
	htmlBuildTwo.append(HTML_FULL_STOP);
	htmlBuildTwo.append(HTML_EMPTYSPACE);

	htmlBuildTwo.append(HTML_PO_CONSTANT3);
	//htmlBuildTwo.append(HTML_US_CURRENCY);
	htmlBuildTwo.append(additionalValue3);
	htmlBuildTwo.append(HTML_SPAN_ENDTAG);

	htmlBuildTwo.append(XML_CDATA_END_TAG);

	System.out.println("%%%%Second STRING%%% "+htmlBuildTwo.toString());
	return htmlBuildTwo.toString();

    }
*/
}

