package com.hiscox.sbweb.util;

import java.io.StringReader;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;

/**
 * Converter Class responsible for conversion from Value Objects to Web service
 * Connector objects and vice versa.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:17 AM
 */
public class HsxSBWebTxtMgrConverter implements IHsxSBWebConstants {

    public HsxSBWebTxtMgrConverter() {

    }

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    /**
     *
     * This method creates the request object for the text manager call.
     *
     * @param textCodeTextItemsMap
     * @param textItemsMap
     * @param showText
     * @return empty string
     */
    public String convertToWSCB() {
	return STR_EMPTY;
    }

    /**
     *
     * This method converts the response from the text manager to the textItems
     * map.
     *
     * @param textItemsMap
     * @param txtResponseXml
     * @return textItemsMap
     */
    public Map<String, HsxSBTextManagerVO> convertXMLToTextItems(
	    Map<String, HsxSBTextManagerVO> textItemsMap, String txtResponseXml) {
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	Document xmlDocument = null;
	try {
	    DocumentBuilder txtBuilder = factory.newDocumentBuilder();
	    xmlDocument = txtBuilder.parse(new InputSource(new StringReader(
		    txtResponseXml)));
	    Element retrieveElement = xmlDocument.getDocumentElement();
	    NodeList textItemList = retrieveElement
		    .getElementsByTagName(TEXT_ITEM);
	    final int textItemLen = textItemList.getLength();
	    for (int i = 0; i < textItemLen; i++) {
		Element textItemElement = (Element) textItemList.item(i);
		NodeList textItemChildList = textItemElement.getChildNodes();
		HsxSBTextManagerVO textItem = new HsxSBTextManagerVO();
		String textItemCode = null;
		final int childTxtItemLen = textItemChildList.getLength();
		for (int j = 0; j < childTxtItemLen; j++) {
		    Node childItem = textItemChildList.item(j);
		    final String nodeName = childItem.getNodeName();
		    final String textContent = childItem.getTextContent();
		    if (Node.ELEMENT_NODE == childItem.getNodeType()
			    && StringUtils.isNotBlank(nodeName)
			    && StringUtils.isNotBlank(textContent)) {
			if (nodeName.contains(TEXT_ITEM_HELP_TEXT)) {
			    String helpText = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    helpText = HsxSBWebPageBeanUtil
				    .encodeTextData(helpText);
			    textItem.setHelpText(helpText);
			} else if (nodeName.contains(TEXT_ITEM_TEXT_CODE)) {
			    textItemCode = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    textItem.setTextCode(textItemCode);
			} else if (nodeName.contains(TEXT_ITEM_LABEL)) {
			    String labelText = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    labelText = HsxSBWebPageBeanUtil
				    .encodeTextData(labelText);
			    labelText = StringEscapeUtils
				    .unescapeHtml(labelText);
			    textItem.setLabelText(labelText);
			} else if (nodeName.contains(TEXT_ITEM_ERROR_TEXT)) {
			    String errorText = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    errorText = HsxSBWebPageBeanUtil
				    .encodeTextData(errorText);
			    textItem.setErrorText(errorText);
			} else if (nodeName.contains(TEXT_ITEM_STYLE)) {
			    textItem.setStyle(textContent);
			//}
			// else if (nodeName.contains(TEXT_ITEM_PURPOSE))
			// {
			// textItem.setPurpose(textContent);
			}else if (nodeName.contains(TEXT_ITEM_HELP_TITLE)) {
			    String helpTitleText = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    helpTitleText = HsxSBWebPageBeanUtil
				    .encodeTextData(helpTitleText);
			    textItem.setHelpTitle(helpTitleText);
			} else if (nodeName.contains(TEXT_ITEM_DEFAULT_VALUE)) {

			    String defaultValueText = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    if (defaultValueText == null) {
				defaultValueText = STR_EMPTY;
			    }
			    defaultValueText = HsxSBWebPageBeanUtil
				    .encodeTextData(defaultValueText);
			    textItem.setDefaultValue(defaultValueText);
			} else if (nodeName.contains(TEXT_ITEM_ADD_TEXT1)) {
			    String additionText1 = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    additionText1 = HsxSBWebPageBeanUtil
				    .encodeTextData(additionText1);
			    additionText1 = StringEscapeUtils
				    .unescapeHtml(additionText1);
			    textItem.setAdditionalText1(additionText1);
			} else if (nodeName.contains(TEXT_ITEM_ADD_TEXT2)) {
			    String additionText2 = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    additionText2 = HsxSBWebPageBeanUtil
				    .encodeTextData(additionText2);
			    textItem.setAdditionalText2(additionText2);
			} else if (nodeName.contains(TEXT_ITEM_ADD_TEXT3)) {
			    String additionText3 = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    additionText3 = HsxSBWebPageBeanUtil
				    .encodeTextData(additionText3);
			    textItem.setAdditionalText3(additionText3);
			} else if (nodeName.contains(TEXT_ITEM_ADD_TEXT4)) {
			    String additionText4 = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    additionText4 = HsxSBWebPageBeanUtil
				    .encodeTextData(additionText4);
			    textItem.setAdditionalText4(additionText4);
			} else if (nodeName.contains(TEXT_ITEM_ADD_TEXT5)) {
			    String additionText5 = HsxSBWebPageBeanUtil
				    .decodeTxData(textContent);
			    additionText5 = HsxSBWebPageBeanUtil
				    .encodeTextData(additionText5);
			    textItem.setAdditionalText5(HsxSBWebPageBeanUtil
				    .decodeTxData(additionText5));
			} else if (nodeName.contains(TEXT_ITEM_ADD_TEXT6)) {
			    textItem.setAdditionalText6(HsxSBWebPageBeanUtil
				    .decodeTxData(textContent));
			}
			// else if (nodeName.contains(TEXT_ITEM_ADD_TEXT7))
			// {
			// textItem.setAdditionalText7(HsxSBWebPageBeanUtil
			// .decodeTxData(textContent));
			// }
			// else if (nodeName.contains(TEXT_ITEM_ADD_TEXT8))
			// {
			// textItem.setAdditionalText8(HsxSBWebPageBeanUtil
			// .decodeTxData(textContent));
			// }
			// else if (nodeName.contains(TEXT_ITEM_ADD_TEXT9))
			// {
			// textItem.setAdditionalText9(HsxSBWebPageBeanUtil
			// .decodeTxData(textContent));
			// }

		    }
		} 
		if (null != textItemCode) {
		    textItemsMap.put(textItemCode.trim(), textItem);
		}

	    }
	} catch (Exception e) {
	    logger
		    .error(SBWEB, ERROR,
			    "Exception Occured During CTM Data Load");
	   logger.error("Exception", e);

	    Map<String, HsxSBTextManagerVO> dummyTextItemsMap = HsxSBWebTextManager
		    .getInstance().dummyTextItemsMap;
	    if (dummyTextItemsMap != null) {
		dummyTextItemsMap.clear();
	    }
	}

	return textItemsMap;

    }

}

