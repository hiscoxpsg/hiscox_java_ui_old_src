package com.hiscox.sbweb.util;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;

/**
 * This class will be called on server start up.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:20 AM
 */
public class HsxSBWebContextLoaderServlet extends HttpServlet implements
	IHsxSBWebConstants {

    private static final long serialVersionUID = 1L;
    protected transient ContextLoader contextLoader;
    private Properties appConfigProperties = null;

    @Override
    public void init() throws ServletException {

	HsxSBWebResourceManager.setContext(WebApplicationContextUtils
		.getRequiredWebApplicationContext(getServletContext()));
	HsxSBWebNavigationManager.setContext(WebApplicationContextUtils
		.getRequiredWebApplicationContext(getServletContext()));
	appConfigProperties = (Properties) HsxSBWebResourceManager
		.getResource(APPCONFIG_PROPERTIES);
	addQuartzScheduler();
	HsxSBWebNavigationManager.getInstance();
	HsxSBWebSessionManager.getInstance();

    }

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    /**
     * This method will be inside init() method.
     */
    public void addQuartzScheduler() {
	SchedulerFactory shedFactory = new StdSchedulerFactory();
	long startTime;
	long endTime;
	long repeatInterval;
	Scheduler sched;

	try {
	    sched = shedFactory.getScheduler();
	    sched.start();
	    JobDetail jobDetail = new JobDetail(QUARTZJOB,
		    Scheduler.DEFAULT_GROUP, HsxSBWebQuartzJob.class);
	    startTime = Long.parseLong(appConfigProperties.getProperty(
		    TRIGGER_INTERVAL1).trim());
	    endTime = Long.parseLong(appConfigProperties.getProperty(
		    TRIGGER_INTERVAL2).trim());
	    repeatInterval = startTime * endTime;

	    SimpleTrigger simpleTrigger = new SimpleTrigger(QUARTZJOB_TRIGGER,
		    Scheduler.DEFAULT_GROUP, new Date(), null,
		    SimpleTrigger.REPEAT_INDEFINITELY, repeatInterval);
	    sched.scheduleJob(jobDetail, simpleTrigger);

	} catch (SchedulerException e) {
	    // TODO Auto-generated catch block
	    	logger.error("SchedulerException", e);
	}

    }

    /**
     * This method closes the web application context.
     */
    @Override
    public void destroy() {
	if (this.contextLoader != null) {
	    this.contextLoader.closeWebApplicationContext(getServletContext());
	}
    }

 /**
  *  This is the servlet's service method.
  *  @throws IOException which throws input/output exception
  *  @param request which is input request parameter
  *  @param response which is input response parameter
  */

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response)
	    throws IOException {
	getServletContext().log(
		"Attempt to call service method on ContextLoaderServlet as ["
			+ request.getRequestURI() + "] was ignored");
	response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    }

}

