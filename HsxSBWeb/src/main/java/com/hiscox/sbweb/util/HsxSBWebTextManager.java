package com.hiscox.sbweb.util;

import java.util.HashMap;
import java.util.Map;

import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;

/**
 * This is a singleton class which is used to load the text manager data in
 * application scope.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:22 AM
 */
public final class HsxSBWebTextManager implements IHsxSBWebConstants {

    private static volatile HsxSBWebTextManager textManager = null;
    public Map<String, HsxSBTextManagerVO> textItemsMap = new HashMap<String, HsxSBTextManagerVO>();
    public Map<String, HsxSBTextManagerVO> dummyTextItemsMap = new HashMap<String, HsxSBTextManagerVO>();



    /**
     * Private constructor to make sure this class is initialized with in this
     * class only.
     */
    private HsxSBWebTextManager() {
    }

    /**
     * This method returns a single instance of the HsxUSDCTextManager class.
     * @return textManager
     */
    public static synchronized HsxSBWebTextManager getInstance() {
	if (null == textManager) {
	    textManager = new HsxSBWebTextManager();
	}
	return textManager;
    }

}

