package com.hiscox.sbweb.util;

/**
 * This class is used to Reset the reload flag for the text manager at every
 * interval.
 *
 * @author Cognizant
 *
 */
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sbweb.broker.HsxSBWebTextManagerServiceBroker;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;

public class HsxSBWebQuartzJob implements Job, IHsxSBWebConstants {

    /**
     * Resets the reload flag for the text manager at every interval.
     */
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void execute(JobExecutionContext arg0) throws JobExecutionException {
	getTextMangerData();

    }

    /**
     *
     */
    private void getTextMangerData() {
	logger.info(SBWEB, INFO, "Job schedular executed For CTM Data Load");
	HsxSBWebTextManagerServiceBroker txtMgrBroker = null;
	txtMgrBroker = (HsxSBWebTextManagerServiceBroker) HsxSBWebResourceManager
		.getResource("txtMgrServiceBroker");
	Map<String, HsxSBTextManagerVO> dummyTextItemsMap = HsxSBWebTextManager
		.getInstance().dummyTextItemsMap;
	Map<String, HsxSBTextManagerVO> textItemsMap = HsxSBWebTextManager
		.getInstance().textItemsMap;
	logger.info(SBWEB, INFO, "Service Call Made to Load CTM Data");
	txtMgrBroker.loadTextItems(dummyTextItemsMap);
	if (dummyTextItemsMap != null && !dummyTextItemsMap.isEmpty()) {
	    textItemsMap.putAll(dummyTextItemsMap);
	    dummyTextItemsMap.clear();
	    logger.info(SBWEB, INFO, "CTM Data Refresh Completed with :"
		    + textItemsMap.size() + " text items");
	} else {
	    logger.info(SBWEB, INFO,
		    "CTM Data Refresh Failed Because Of An Improper Response");
	}
    }

}

