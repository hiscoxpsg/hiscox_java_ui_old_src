package com.hiscox.sbweb.util;

import java.io.File;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.context.ApplicationContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
/**
 *
 * @author cognizant
 * @version 1.0
 */
public final class HsxSBWebNavigationManager implements IHsxSBWebConstants {
    private static HsxSBWebNavigationManager navigatonManager = null;
    private Map<String, HsxSBWebPageRuleBean> navigationMap = new HashMap<String, HsxSBWebPageRuleBean>();
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();
    private static ApplicationContext context = null;
    private static String fileAbsolutePath = null;
    public static Document broacherReqDocument = null;

    public static String getFileAbsolutePath() {
	return fileAbsolutePath;
    }

    public static void setBroacherReqDocument(Document broacherReqDocument) {
	HsxSBWebNavigationManager.broacherReqDocument = broacherReqDocument;
    }

    public Map<String, HsxSBWebPageRuleBean> getNavigationMap() {
	return navigationMap;
    }

    public void setNavigationMap(
	    Map<String, HsxSBWebPageRuleBean> navigationMap) {
	this.navigationMap = navigationMap;
    }

    /**
     * This method returns a single instance of the HsxSBWebNavigationManager
     * class.
     *
     * @return manager
     */
    public static HsxSBWebNavigationManager getInstance() {
	HsxSBWebNavigationManager manager = navigatonManager;
	try {
	    if (manager == null) {
		navigatonManager = new HsxSBWebNavigationManager();
		manager = getPageRules();
	    }
	   // manager = getPageRules();

	} catch (Exception e) {
	    logger.error("Exception", e);
	    logger.error(SBWEB, ERROR, STR_ERROR_MESSAGE + e.getMessage());
	}
	return manager;
    }

    // this is to make sure only this class can create this class instance
    private HsxSBWebNavigationManager() {

    }

    public static synchronized HsxSBWebNavigationManager getPageRules()
	    throws HsxCoreException, HsxCoreRuntimeException, Exception {
	loadPageRulesMap(navigatonManager.getNavigationMap());
	// To load request xml for broacherware page
	loadBroacherReq();
	return navigatonManager;
    }

    public static void main(String[] args) throws Exception {
	// loadPageRulesMap(new HashMap<String, HsxSBWebPageRuleBean>());
    }

    /**
     * This method is to load broacherware request xml.
     *
     * @throws Exception
     */
    private static void loadBroacherReq() throws Exception {
	Document broacherReqDocument = convertXMLtoDocument(BROCHERWARE_REQ_PATH);

	HsxSBWebNavigationManager.broacherReqDocument = broacherReqDocument;
    }

    private static void loadPageRulesMap(
	    Map<String, HsxSBWebPageRuleBean> navigationRuleMap)
	    throws HsxCoreException, HsxCoreRuntimeException, Exception {
	Document document = convertXMLtoDocument(NAVIGATION_RULES_PATH);

	Element navigationElement = (Element) document.getElementsByTagName(
		STR_NAVIGATION).item(0);

	NodeList pageRules = navigationElement.getChildNodes();

	for (int i = 0; i < pageRules.getLength(); i++) {
	    String nodeName = pageRules.item(i).getNodeName();
	    if (pageRules.item(i).getNodeType() == Node.ELEMENT_NODE
		    && (nodeName != null) && nodeName.contains(STR_PAGE)) {
		Element pageElement = (Element) pageRules.item(i);
		HsxSBWebPageRuleBean hsxSBWebPageRuleBean = new HsxSBWebPageRuleBean();
		String pageNameKey = pageElement.getElementsByTagName(STR_NAME)
			.item(0).getTextContent();
		// logger.info(SBWEB, INFO,"Page Name :"+pageNameKey);
		String[] pageNameArray = HsxSBWebPageBeanUtil.getStringTokens(
			pageNameKey, "/");
		if (pageNameArray.length > 1) {
		    String pageName = pageNameArray[1];
		    String folderName = pageNameArray[0];
		    hsxSBWebPageRuleBean.setPageName(pageName);
		    hsxSBWebPageRuleBean.setFolderName(folderName);
		} else {
		    hsxSBWebPageRuleBean.setPageName(pageNameKey);
		}
		// hsxSBWebPageRuleBean.setPageRuleBeanKey(pageNameKey);

		String pageType = pageElement.getElementsByTagName(STR_TYPE)
			.item(0).getTextContent();
		hsxSBWebPageRuleBean.setPageType(pageType);
		String pageBarItem = pageElement.getElementsByTagName(
			STR_BARITEM).item(0).getTextContent();
		hsxSBWebPageRuleBean.setPageBarItem(pageBarItem);
		Element pageCodeElement = (Element) pageElement
			.getElementsByTagNameNS(STR_ASTERISK, STR_CODE).item(0);
		if (pageCodeElement != null) {
		    hsxSBWebPageRuleBean.setPageCode(pageCodeElement
			    .getTextContent());
		}

		NodeList rules = pageElement.getElementsByTagNameNS(
			STR_ASTERISK, STR_RULE);
		// logger.info(SBWEB,
		// INFO,"rules.getLength() :"+rules.getLength());
		for (int ruleIndex = 0; ruleIndex < rules.getLength(); ruleIndex++) {
		    boolean value = false;
		    String ruleNodeName = rules.item(ruleIndex).getNodeName();
		    // logger.info(SBWEB, INFO,"ruleNodeName :"+ruleNodeName);
		    // logger.info(SBWEB, INFO,"ruleNodeName :"+ruleNodeName);
		    // logger.info(SBWEB,
		    // INFO,"Result 1: "+(rules.item(ruleIndex).getNodeType()
		    // == Node.ELEMENT_NODE));
		    // logger.info(SBWEB, INFO,"Result 2: "+(ruleNodeName !=
		    // null));
		    // logger.info(SBWEB,
		    // INFO,"Result 3: "+ruleNodeName.contains(STR_RULE));
		    if (rules.item(ruleIndex).getNodeType() == Node.ELEMENT_NODE
			    && (ruleNodeName != null)
			    && ruleNodeName.contains(STR_RULE)) {
			// logger.info(SBWEB, INFO,"If Passed");
			Element ruleElement = (Element) rules.item(ruleIndex);
			String ruleElementName = ruleElement
				.getAttribute(STR_NAME);
			// logger.info(SBWEB,
			// INFO,"ruleElement.getAttribute(STR_NAME) :"+ruleElementName);
			if (STR_IS_CHILD_PAGE.equalsIgnoreCase(ruleElement
				.getAttribute(STR_NAME))) {
			    value = Boolean.parseBoolean(ruleElement
				    .getAttribute(STR_VALUE));

			    hsxSBWebPageRuleBean.setChildPage(value);

			} else if (STR_IS_LANDING_PAGE
				.equalsIgnoreCase(ruleElement
					.getAttribute(STR_NAME))) {
			    value = Boolean.parseBoolean(ruleElement
				    .getAttribute(STR_VALUE));
			    hsxSBWebPageRuleBean.setLandingPage(value);

			} else if (STR_IS_LOGIN_REQUIRED
				.equalsIgnoreCase(ruleElement
					.getAttribute(STR_NAME))) {
			    value = Boolean.parseBoolean(ruleElement
				    .getAttribute(STR_VALUE));
			    hsxSBWebPageRuleBean.setLoginRequired(value);

			 /*}
			 * else if (ruleElementName != null && STR_FOLDER_NAME
			 * .equalsIgnoreCase(ruleElementName .trim())) { String
			 * folderName = ruleElement .getAttribute(STR_VALUE);
			 * hsxSBWebPageRuleBean.setFolderName(folderName);
			 *
			 */}else if (ruleElementName != null
				&& STR_IS_END_OF_JOURNEY_PAGE
					.equalsIgnoreCase(ruleElementName
						.trim())) {
			    String isEndOfJourneyPage = ruleElement
				    .getAttribute(STR_VALUE);
			    hsxSBWebPageRuleBean
				    .setIsEndOfJourneyPage(isEndOfJourneyPage);

			} else if (ruleElementName != null
				&& STR_PARENT_PAGE
					.equalsIgnoreCase(ruleElementName
						.trim())) {
			    String parentPage = ruleElement
				    .getAttribute(STR_VALUE);
			    hsxSBWebPageRuleBean.setParentPage(parentPage);

			} else if (ruleElementName != null
				&& STR_IS_MULTIPLE_FORM
					.equalsIgnoreCase(ruleElementName
						.trim())) {
			    String ismultipleForm = ruleElement
				    .getAttribute(STR_VALUE);
			    hsxSBWebPageRuleBean
				    .setIsmultipleForm(ismultipleForm);

			} else if (ruleElementName != null
				&& STR_PREVIOUS_PAGE_STATUS
					.equalsIgnoreCase(ruleElementName
						.trim())) {
			    String previousPageStatus = ruleElement
				    .getAttribute(STR_VALUE);
			    hsxSBWebPageRuleBean
				    .setPreviousPageStatus(previousPageStatus);

			} else if (ruleElementName != null
				&& STR_REG_NAV.equalsIgnoreCase(ruleElementName
					.trim())) {
			    hsxSBWebPageRuleBean
				    .setRegularNavigationAllowed(ruleElement
					    .getAttribute(STR_VALUE));

			} else if (ruleElementName != null
				&& STR_IS_CALENDAR_REQUIRED
					.equalsIgnoreCase(ruleElementName
						.trim())) {
			    hsxSBWebPageRuleBean
				    .setIsCalendarRequired(ruleElement
					    .getAttribute(STR_VALUE));

			} else if (ruleElementName != null
				&& STR_IS_THROBBER_PAGE
					.equalsIgnoreCase(ruleElementName
						.trim())) {
			    hsxSBWebPageRuleBean.setThrobberPage(ruleElement
				    .getAttribute(STR_VALUE));
			} else if (ruleElementName != null
				&& STR_UPDATE_PRODUCTS
					.equalsIgnoreCase(ruleElementName
						.trim())) {
			    hsxSBWebPageRuleBean.setUpdateProducts(ruleElement
				    .getAttribute(STR_VALUE));
			} else if (ruleElementName != null
				&& "frontPage".equalsIgnoreCase(ruleElementName
					.trim())) {
			    hsxSBWebPageRuleBean
				    .setThrobberFrontPage(ruleElement
					    .getAttribute(STR_VALUE));
			} else if (ruleElementName != null
				&& "rearPage".equalsIgnoreCase(ruleElementName
					.trim())) {
			    hsxSBWebPageRuleBean
				    .setThrobberRearPage(ruleElement
					    .getAttribute(STR_VALUE));
			}
		    }
		}
		navigationRuleMap.put(pageNameKey, hsxSBWebPageRuleBean);

	    }

	}

    }

    /**
     * This method loads xml and coverts to document.
     *
     * @param path
     * @return document
     * @throws Exception
     */
    public static Document convertXMLtoDocument(String path) throws Exception {
	StringBuilder inputXML = new StringBuilder(STR_EMPTY);

	File rulesFile = context.getResource(path).getFile();

	// Start: to find file path
	fileAbsolutePath = rulesFile.getPath();
	fileAbsolutePath = fileAbsolutePath.substring(0, fileAbsolutePath
		.lastIndexOf(STR_BACKWARD_SLASH_SYMBOL) + 1);
	// logger.info(SBWEB, INFO, "Navigation file location : :: "
	// + fileAbsolutePath);
	// End : to find file path
	// rulesFile = new File("C:\\TestData\\navigation-rules.xml");
	RandomAccessFile rnd;

	rnd = new RandomAccessFile(rulesFile, "rw");
	String inter = rnd.readLine();
	while (inter != null) {
	    inputXML = inputXML.append(inter);
	    inter = rnd.readLine();
	}
	rnd.close();

	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	factory.setNamespaceAware(true);
	DocumentBuilder builder = factory.newDocumentBuilder();
	Document document = builder.parse(new InputSource(new StringReader(
		inputXML.toString())));

	return document;
    }

    /**
     *
     * @param context
     */
    public static void setContext(ApplicationContext context) {
	HsxSBWebNavigationManager.context = context;
    }

}

