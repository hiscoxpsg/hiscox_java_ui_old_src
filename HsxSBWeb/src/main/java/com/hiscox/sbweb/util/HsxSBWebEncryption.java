package com.hiscox.sbweb.util;

import java.io.Serializable;

import java.util.Properties;
import org.apache.commons.lang.StringUtils;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.security.crypto.HsxCoreAESEngine;
import com.hiscox.core.security.crypto.HsxCoreCryptoInterface;
import com.hiscox.core.security.crypto.HsxCoreKeyGenerator;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:20 AM
 */
public class HsxSBWebEncryption implements IHsxSBWebConstants, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6498983350458238476L;
    /**
     * This is used to retrieve the data (like userDn, password, dnsName of the
     * LDAP) from the property file.
     */
    public static final HsxWebLogger logger = HsxWebLogger.getLogger();
    private Properties appConfigProperties;

    public void setAppConfigProperties(Properties appConfigProperties) {
	this.appConfigProperties = appConfigProperties;
    }

    /**
     *
     * @param componentName
     * @param rawData
     * @throws Exception
     * @return decryptedDecodedData
     */
    public String doDataDecryptionAndDecoding(String componentName,
	    String rawData) throws Exception {
	String decryptedDecodedData = null;

	String encodedKey = getKey(componentName);
	HsxCoreCryptoInterface aesEngine = new HsxCoreAESEngine();
	decryptedDecodedData = aesEngine.processBlock(STR_DECRYPT, rawData
		.getBytes(), encodedKey);

	return decryptedDecodedData;
    }

    /**
     *
     * @param componentName
     * @param rawData
     * @throws Exception
     * @return encryptedEncodedData
     */
    public String doDataEncryptionAndEncoding(String componentName,
	    String rawData) throws HsxCoreException {
	String encryptedEncodedData = null;

	try {

	    String encodedKey = getKey(componentName);
	    HsxCoreCryptoInterface aesEngine = new HsxCoreAESEngine();

	    encryptedEncodedData = aesEngine.processBlock(STR_ENCRYPT, rawData
		    .getBytes(), encodedKey);
	    if (null != encryptedEncodedData) {
		logger
			.info(
				SBWEB,
				INFO,
				"****Encryption***Encrypted encoded data is successsful and encrypted data is ::"
					+ encryptedEncodedData);
	    } else {
		logger.info(SBWEB, INFO,
			"****Encryption***Encryption failed");
	    }
	} catch (Exception e) {
	    logger.error("Exception", e);
	    throw new HsxCoreException();
	}
	return encryptedEncodedData;
    }

    /**
     *
     * @param componentName
     * @throws HsxCoreException
     * @throws CertificateException
     * @throws NoSuchAlgorithmException
     * @return key
     */
    public String getKey(String componentName) throws HsxCoreException {
	String key = STR_EMPTY;
	String appDn = STR_EMPTY;

	String userDn = appConfigProperties.getProperty(STR_USER_DN); // "CN=APPADMIN,OU=ADMINISTRATORS,DC=HISCOXAPPS";
	String password = appConfigProperties
		.getProperty(STR_CARD_PAYMENT_PASSWORD); // "password-1";
	String base = appConfigProperties.getProperty(STR_BASE); // "DC=HISCOXAPPS";
	String dnsName = appConfigProperties.getProperty(STR_DNS_NAME); // "ldap://HXS32226.hiscox.nonprod:389";
	logger.info(SBWEB, INFO, "****Encryption***userDn ::" + userDn);
	logger.info(SBWEB, INFO, "****Encryption***password::" + password);
	logger.info(SBWEB, INFO, "****Encryption***base::" + base);
	logger.info(SBWEB, INFO, "****Encryption***dnsName::" + dnsName);

	if (componentName.equals(STR_CARD_PAYMENT_COMPONENT_NAME)) {
	    appDn = appConfigProperties.getProperty(STR_APP_DN); // "CN=CARDPAYMENT-US-DIRECTCOMMERCIAL,OU=APPS_CONFIG,DC=HISCOXAPPS";
	    logger.info(SBWEB, INFO, "****Encryption***appDn ::" + appDn);
	}

	try {

	    HsxCoreKeyGenerator keyGenerator = new HsxCoreKeyGenerator();

	    keyGenerator.setCredentials(userDn, password, base, dnsName);
	    logger.info(SBWEB, INFO, "****Encryption*** LDAP call Start");
	    key = keyGenerator.retrieveKey(appDn, STR_KEY);
	    if (StringUtils.isNotEmpty(key)) {
		logger.info(SBWEB, INFO,
			"****Encryption***Successfully retrieved the key from LDAP and Key is ::"
				+ key);
	    } else {
		logger
			.info(SBWEB, INFO,
				"****Encryption***Failed to Retreive the key from LDAP**");
	    }

	} catch (Exception e) {
		logger.error("Exception", e);
	    throw new HsxCoreException();
	}
	return key;
    }

}

