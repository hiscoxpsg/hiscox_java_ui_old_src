package com.hiscox.sbweb.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.vo.HsxCoreRequest;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.broker.HsxSBWebErrorManagementServiceBroker;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;

/**
 * @author Cognizant
 *
 */

public class HsxSBWebQuoteAndBuyControllerUtil implements IHsxSBWebConstants {

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    /**
     * Encrypt the credit card details before making the web service call Page
     * specific process - applicable only for card payment page.
     *
     * @param responseXml String
     * @param hsxSBWebEncryption HsxSBWebEncryptio
     * @return encryptedResponseXml
     * @throws HsxCoreException
     */
    public static String encryptCardPaymentData(String responseXml,
	    HsxSBWebEncryption hsxSBWebEncryption,
	    Map<String, String> encryptionActionMap, String requestedAction)
	    throws HsxCoreException {
	Document xmlDocument = HsxSBWebPageBeanUtil
		.getDocumentFromString(responseXml);
	String pageName = null;
	Element dataAreaElement = (Element) xmlDocument.getElementsByTagNameNS(
		STR_ASTERISK, STR_DATAAREA).item(0);
	NodeList dataAreaChldNdLst = dataAreaElement.getChildNodes();
	int dataAreaChldNdLstLength = dataAreaChldNdLst.getLength();
	for (int daChldNdLstIndex = 0; daChldNdLstIndex < dataAreaChldNdLstLength; daChldNdLstIndex++) {
	    final Node daChildNode = dataAreaChldNdLst.item(daChldNdLstIndex);
	    if (daChildNode != null
		    && daChildNode.getNodeType() == Node.ELEMENT_NODE) {
		String nodeName = daChildNode.getNodeName();
		if (nodeName != null && nodeName.contains(STR_CODE)) {
		    pageName = daChildNode.getTextContent();
		    break;
		}
	    }
	}

	if (STR_PAYMENT_DETAILS.equalsIgnoreCase(pageName)) {
	    boolean encryptionFlag = false;
	    if (encryptionActionMap != null
		    && encryptionActionMap.containsKey(requestedAction)
		    && STR_YES.equalsIgnoreCase(encryptionActionMap
			    .get(requestedAction))) {
		encryptionFlag = true;
	    }

	    NodeList screenQuestionList = xmlDocument.getElementsByTagNameNS(
		    STR_ASTERISK, XML_SCREEN_QUESTION);
	    final int screenQuesLen = screenQuestionList.getLength();
	    for (int questionIndex = 0; questionIndex < screenQuesLen; questionIndex++) {
		Element screenQuestion = (Element) screenQuestionList
			.item(questionIndex);
		String screenQuestionCode = screenQuestion.getAttribute(
			STR_CODE).toString();

		if (STR_CARD_NUMBER.equalsIgnoreCase(screenQuestionCode)
			|| STR_CARD_SECURITY_CODE
				.equalsIgnoreCase(screenQuestionCode)
			|| STR_CARD_EXPIRY_DATE
				.equalsIgnoreCase(screenQuestionCode)) {
		    NodeList childElementsList = screenQuestion.getChildNodes();
		    final int childElemLen = childElementsList.getLength();
		    for (int configIndex = 0; configIndex < childElemLen; configIndex++) {
			String nodeName = childElementsList.item(configIndex)
				.getNodeName();
			if (childElementsList.item(configIndex).getNodeType() == Node.ELEMENT_NODE
				&& nodeName != null
				&& nodeName.contains(XML_CONFIG)) {
			    Element childElement = (Element) childElementsList
				    .item(configIndex);
			    if (CONFIG_SAVED_VALUE
				    .equalsIgnoreCase((childElement
					    .getAttribute(STR_NAME)).toString())) {
				String answerdValue = childElement
					.getAttribute(XML_QUESTION_VALUE)
					.toString();

				if (STR_CARD_EXPIRY_DATE
					.equalsIgnoreCase(screenQuestionCode)) {
				    answerdValue = HsxSBUtil.convertDateFormat(
					    answerdValue, DATE_FORMAT_DDMMYYYY,
					    DATE_FORMAT_MMYY);
				} else if (STR_CARD_SECURITY_CODE
					.equalsIgnoreCase(screenQuestionCode)) {
				    if (StringUtils.isNotBlank(answerdValue)) {
					answerdValue = answerdValue.trim();
					if (answerdValue.length() == 1) {
					    logger
						    .error(SBWEB, ERROR,
							    "Single Digit CVV Number Is being Encrypted and sent to WM");
					} else if (answerdValue.length() == 2) {
					    logger
						    .error(SBWEB, ERROR,
							    "Double Digit CVV Number Is being Encrypted and sent to WM");
					}

				    } else {
					logger
						.error(SBWEB, ERROR,
							"Blank CVV Number Is being Encrypted and sent to WM");
				    }
				}
				if (encryptionFlag) {
				    logger.info(SBWEB, INFO,
					    "Encryption required For The Answered Value :"
						    + answerdValue);
				    answerdValue = hsxSBWebEncryption
					    .doDataEncryptionAndEncoding(
						    STR_CARD_PAYMENT_COMPONENT_NAME,
						    answerdValue);
				    logger.info(SBWEB, INFO,
					    "Encrypted value sent to WM is "
						    + answerdValue);
				} else {
				    answerdValue = STR_EMPTY;
				}
				childElement.setAttribute(XML_QUESTION_VALUE,
					answerdValue);
			    }
			}

		    }
		}
	    }
	}
	String encryptedResponseXml = HsxSBWebPageBeanUtil
		.getStringFromDocument(xmlDocument);

	return encryptedResponseXml;
    }

    /**
     * This method will reset the xml parameters like screenxml content and
     * quote reference id which are present in the web bean before making any
     * service calls.
     *
     */
    public static void resetWebBeanXmlContent(HsxSBWebBean webBean) {
	logger.info(SBWEB, INFO, "Resetting WebBean");
	webBean.setWidget(null);
	webBean.setQuestionsContent(null);
	webBean.setActionsContent(null);
	webBean.setQuoteRefID(null);
    }

    /**
     * This method will generate proper system error page url based on the error
     * code returned by the service in case of Nak.
     *
     * @param request
     * @return Redirectview
     * @throws Exception
     */
    public static RedirectView getErrorPageUrl(HttpServletRequest request,
	    HsxSBWebBean webBean) throws Exception {
	StringBuilder redirectionURL = new StringBuilder(STR_EMPTY);
	redirectionURL.append(STR_FORWARD_SLASH);
	redirectionURL.append(STR_SYSTEM_ERROR);
	webBean.setInvalidateSessionFlag(true);
	HsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request, webBean);
	logger.info(SBWEB, INFO,
		"Relative Url View Name For The System Error Page :"
			+ redirectionURL);
	return new RedirectView(redirectionURL.toString(), true);
    }

    public static RedirectView redirectToInvalidPageUrl() throws Exception {
	StringBuilder redirectionURL = new StringBuilder(STR_EMPTY);
	redirectionURL.append(STR_FORWARD_SLASH);
	redirectionURL.append(QUOTE_AND_BUY);
	redirectionURL.append(STR_FORWARD_SLASH);
	redirectionURL.append(STR_UNABLE_TO_NAVIGATE_BACK_PAGE);
	redirectionURL.append(STR_FORWARD_SLASH);
	logger.info(SBWEB, INFO,
		"Relative Url View Name For The Invalid Navigation Page :"
			+ redirectionURL);
	return new RedirectView(redirectionURL.toString(), true);
    }

    public static RedirectView redirectToPageNotFoundUrl() throws Exception {
	StringBuilder redirectionURL = new StringBuilder(STR_EMPTY);
	redirectionURL.append(STR_FORWARD_SLASH);
	redirectionURL.append(QUOTE_AND_BUY);
	redirectionURL.append(STR_FORWARD_SLASH);
	redirectionURL.append(STR_404_ERROR_PAGE);
	redirectionURL.append(STR_FORWARD_SLASH);
	logger.info(SBWEB, INFO,
		"Relative Url View Name For The 404 Error Page :"
			+ redirectionURL);
	return new RedirectView(redirectionURL.toString(), true);
    }

    public static synchronized void retrieveSessionMap(HsxSBWebBean webBean,
	    Properties sessionMgrProperties) {
	Map<String, String> sessionMap;
	if (HsxSBWebPageBeanUtil.isMapEmpty(webBean.getSessionMap())) {
	    sessionMap = getSessionInstance(sessionMgrProperties, webBean);
	    webBean.setSessionMap(sessionMap);
	}
	sessionMap = webBean.getSessionMap();
	if (HsxSBWebPageBeanUtil.isMapEmpty(sessionMap)) {
	    sessionMap = new HashMap<String, String>();
	    sessionMap.put(PRIMARY_BUSINESS_CODE, STR_DEFAULT);
	    webBean.setSessionMap(sessionMap);
	} else {
	    if (!sessionMap.containsKey(PRIMARY_BUSINESS_CODE)) {
		sessionMap.put(PRIMARY_BUSINESS_CODE, STR_DEFAULT);
		webBean.setSessionMap(sessionMap);
	    }
	}

    }

    public static boolean isRequestedPageValid(String requestedPage) {
	boolean validUrlFlag = true;
	if (requestedPage != null) {
	    int arrLength = STR_IMPROPER_FILE_EXTENSION_URL_ARR.length;
	    for (int i = 0; i < arrLength; i++) {
		if (requestedPage
			.contains(STR_IMPROPER_FILE_EXTENSION_URL_ARR[i])) {
		    validUrlFlag = false;
		    logger
			    .info(SBWEB, INFO,
				    "Invalid Url Reaching Controller :"
					    + requestedPage);
		    break;
		}
	    }
	} else {
	    validUrlFlag = false;
	}
	return validUrlFlag;

    }

    /**
     * This method invalidates the user session on system error.
     *
     * @param request
     * @param webBean
     */
    public static void inValidateSession(HttpServletRequest request,
	    HsxSBWebBean webBean) {

	if (webBean.isInvalidateSessionFlag()) {
	    resetSessionParamters(webBean);
	    HttpSession userSession = null;
	    userSession = request.getSession(false);
	    if (userSession != null) {
		userSession.invalidate();
	    }
	    logger.info(SBWEB, INFO,
		    "<----- User Session Invalidated Completely ------>");
	}

    }

    /**
     * @param webBean
     */
    public static void resetSessionParamters(HsxSBWebBean webBean) {
	logger.info(SBWEB, INFO, "User pertaining to sessionidentifier ->: "
		+ webBean.getSessionIdentifierValue() + " and QuoteRefId ->: "
		+ webBean.getErrMngtQuoteRefId()
		+ "'S Session is being invalidated");
	HsxSBWebPageBeanUtil.resetWebBean(webBean);
	HsxSBWebQuoteAndBuyControllerUtil.resetWebBeanXmlContent(webBean);
    }
/**
 * This method is to prepare the request for web exception.
 * @param sessionIdentifier
 * @param quoteRefId
 * @param exceptionType
 * @return request
 */
    public static HsxCoreRequest prepareRequestForWebException(
	    String sessionIdentifier, String quoteRefId, String exceptionType) {
	HsxCoreRequest request = new HsxCoreRequest();
	request.setAttribute(MESSAGE_ID, EM_MESSAGE_ID);
	request.setAttribute(ERROR_CODE, STR_UI_ERROR_CODE);
	request.setAttribute(APPLICATION_SERVER_NAME,
		EM_APPLICATION_SERVER_NAME);
	request.setAttribute(APPLICATION_NAME, EM_APPLICATION_NAME);
	request.setAttribute(COMPONENT_NAME, STR_USDCWEB);
	request.setAttribute(MAIN_SERVICE, STR_GETPAGE);
	request.setAttribute(ERROR_TYPE, EM_ERROR_TYPE);
	List<String> error = new ArrayList<String>();
	error.add("User with Sess Identifier :" + sessionIdentifier
		+ " Quote Reference Id :" + quoteRefId);
	error.add("USDC UI Application");
	request.setAttribute(ARRAY_ERROR, error);
	request
		.setAttribute(
			ADDITIONAL_INFORMATION,
			"User with Session Identifier :"
				+ sessionIdentifier
				+ " and of Quote Reference Id :"
				+ quoteRefId
				+ " is not allowed to access the USDC System because Of :"
				+ exceptionType);
	return request;
    }

    /**
     * @param webBean
     * @param errorManagementServiceBroker
     * @param exceptionType
     */
    public static void logUiExceptionToErrorManagement(HsxSBWebBean webBean,
	    String exceptionType) {
	webBean.setExceptionOccured(true);
	logger.error(SBWEB, ERROR, "Exception after Page: "
		+ webBean.getPreviousScreen());
	logger.error(SBWEB, ERROR, "User with Session Identifier :"
		+ webBean.getSessionIdentifierValue()
		+ " and of Quote Reference Id :"
		+ webBean.getErrMngtQuoteRefId()
		+ " is not allowed to access the USDC System because Of :"
		+ exceptionType);
	HsxSBWebErrorManagementServiceBroker errorManagementServiceBroker = (HsxSBWebErrorManagementServiceBroker) HsxSBWebResourceManager
		.getResource(ERROR_MANAGEMENT_SERVICE_BROKER);
	try {
	    errorManagementServiceBroker
		    .invokeErrorManagementService(HsxSBWebQuoteAndBuyControllerUtil
			    .prepareRequestForWebException(webBean
				    .getSessionIdentifierValue(), webBean
				    .getErrMngtQuoteRefId(), exceptionType));
	} catch (Exception e) {
	    logger.error("Exception", e);
	}
    }

    private static Map<String, String> getSessionInstance(
	    Properties sessionMgrProperties, HsxSBWebBean webBean) {
	Map<String, String> sessionMap = new HashMap<String, String>();
	sessionMap = (HashMap<String, String>) getMapValues(sessionMap,
		sessionMgrProperties, webBean);

	return sessionMap;
    }
/**
 * This method is useful to get the map values.
 * @param sessionMap
 * @param sessionMgrProperties
 * @param webBean
 * @return sessionMap
 */
    private static Map<String, String> getMapValues(
	    Map<String, String> sessionMap, Properties sessionMgrProperties,
	    HsxSBWebBean webBean) {
	sessionMap = HsxSBWebPageBeanUtil.getMapFromProperty(
		sessionMgrProperties, sessionMap);
	sessionMap = getSessionValues(sessionMap, webBean);

	return sessionMap;
    }

    private static synchronized Map<String, String> getOccupationVariant(
	    Map<String, String> sessionMap, String key,
	    Map<String, String> fixMap) {
	if (HsxSBWebSessionManager.occupVariantMap.containsKey(key)) {
	    // logger.info(SBWEB, INFO,"Business occupation Match found::");
	    String businessGroup = HsxSBWebSessionManager.occupVariantMap
		    .get(key);
	    fixMap.put(OCCUPATION_VARIANT, businessGroup);
	    logger.info(SBWEB, INFO, "sessionKey in  Occupation variant :"
		    + key);

	    logger.info(SBWEB, INFO, "sessionValue in  Occupation variant :"
		    + businessGroup);
	} else {
	    // to reset back from from variable text to generic text
	    fixMap.put(OCCUPATION_VARIANT, STR_EMPTY);
	}
	return sessionMap;
    }

    public static synchronized Map<String, String> getSessionValues(
	    Map<String, String> sessionMap, HsxSBWebBean webBean) {

	Map<String, HsxSBWidget> widgetMap = webBean.getWidgetMap();
	if (!HsxSBWebPageBeanUtil.isMapEmpty(webBean.getSessionMap())) {
	    sessionMap = webBean.getSessionMap();
	}
	updateSessionMapWithUserValues(sessionMap, webBean, widgetMap);
	webBean.setSessionMap(sessionMap);
	return sessionMap;
    }

    /**
     * @param sessionMap
     * @param webBean
     * @param widgetMap
     */
    private static void updateSessionMapWithUserValues(
	    Map<String, String> sessionMap, HsxSBWebBean webBean,
	    Map<String, HsxSBWidget> widgetMap) {
	logger.info(SBWEB, INFO, "Updating Session Map with User Values ....");
	Map<String, String> tempSessionMap = new HashMap<String, String>();

	if (null != widgetMap && null != sessionMap) {
	    Collection<String> sessionKeySet = sessionMap.keySet();
	    for (Iterator<String> sessionIterator = sessionKeySet.iterator(); sessionIterator
		    .hasNext();) {
		String sessionKey = sessionIterator.next();
		if (widgetMap.containsKey(sessionKey)) {
		    HsxSBWidget widget = widgetMap.get(sessionKey);
		    if (widget != null && widget.getWidgetInfo() != null) {
			String sessionValue = widget.getWidgetInfo()
				.getSavedValue();
			if (StringUtils.isNotBlank(sessionValue)) {
			    if (STR_EMAILADDRESS_NEW
				    .equalsIgnoreCase(sessionKey)) {
//				logger.info(SBWEB, INFO,
//					"sessionKey Stored for Ldap UserName :"
//						+ sessionKey);
//				logger.info(SBWEB, INFO,
//					"sessionValue Stored for Ldap UserName :"
//						+ sessionValue);
				webBean.setLdapUserName(sessionValue);
				tempSessionMap.put(sessionKey, sessionValue);
			    } else {
//				logger.info(SBWEB, INFO, "sessionKey :"
//					+ sessionKey);
//				logger.info(SBWEB, INFO, "sessionValue :"
//					+ sessionValue);
				tempSessionMap.put(sessionKey, sessionValue);
			    }
			}
		    }

		}

	    }

	    // uppdate Quote reference id to session map
	    String quoterefId = webBean.getQuoteRefID();
	    if (StringUtils.isNotBlank(quoterefId)
		    && !"0".equalsIgnoreCase(quoterefId)) {
		sessionMap.put(STR_QUOTE_REF_ID, quoterefId);
	    }
	    sessionMap.putAll(tempSessionMap);
	}
	if (null != sessionMap) {
	    // To replace state code with state Name and update variants of COB
	    getOccupationVariant(sessionMap, sessionMap.get(PRIMARY_BUSINESS_CODE),
		    tempSessionMap);
	    getStateVariant(sessionMap, sessionMap.get(STATE), tempSessionMap);
	    sessionMap.putAll(tempSessionMap);
	}
    }

    private static synchronized Map<String, String> getStateVariant(
	    Map<String, String> sessionMap, String key,
	    Map<String, String> fixMap) {

	if (HsxSBWebSessionManager.stateVariantMap.containsKey(key)) {
	    // logger.info(SBWEB, INFO,"Business state Match found::");
	    String stateValue = HsxSBWebSessionManager.stateVariantMap.get(key);
	    fixMap.put(STATE_VARIANT, stateValue);
//	    logger.info(SBWEB, INFO, "sessionKey in  State variant :" + key);
//
//	    logger.info(SBWEB, INFO, "sessionValue in  State variant : :"
//		    + stateValue);
	}
	return sessionMap;
    }

    /**
     * @param webBean
     * @param pageRuleBean
     */
    public static void updateSessionMapWithProducts(HsxSBWebBean webBean,
	    HsxSBWebPageRuleBean pageRuleBean) {
	if (pageRuleBean != null && STR_YES.equalsIgnoreCase(pageRuleBean.getUpdateProducts())) {
		// loop through all the Screen Questions and Update
		// Session Map accordingly.
		String responseXml = webBean.getQuestionsContent();
		if (responseXml != null) {
		    Document responseDocument = HsxSBWebPageBeanUtil
			    .getDocumentFromString(responseXml);
		    if (responseDocument != null) {
			NodeList screenQuestionList = responseDocument
				.getElementsByTagNameNS(STR_ASTERISK,
					STR_SCREEN_QUESTION);
			final int screenQuestionLength = screenQuestionList
				.getLength();
			Map<String, String> sessionMap = webBean
				.getSessionMap();
			if (!HsxSBWebPageBeanUtil.isMapEmpty(sessionMap)) {
			    for (int screenQuestionIndex = 0; screenQuestionIndex < screenQuestionLength; screenQuestionIndex++) {
				Node ScreenQuestionNode = screenQuestionList
					.item(screenQuestionIndex);
				if (ScreenQuestionNode != null) {
				    Element screenQuestionElement = (Element) ScreenQuestionNode;
				    if (screenQuestionElement
					    .hasAttribute(STR_CODE)) {
					String questionCode = screenQuestionElement
						.getAttribute(STR_CODE);
					if (StringUtils
						.isNotBlank(questionCode)) {
					    questionCode = questionCode.trim();
					}
					if (STR_ADDGL
						.equalsIgnoreCase(questionCode)
						&& sessionMap
							.containsKey(STR_ADDGL)) {

					    sessionMap.put(STR_ADDGL, STR_YES);

					} else if (STR_ADDENO
						.equalsIgnoreCase(questionCode)
						&& sessionMap
							.containsKey(STR_ADDENO)) {

					    sessionMap.put(STR_ADDENO, STR_YES);

					} else if (STR_REQUESTCALLBACK
						.equalsIgnoreCase(questionCode)
						&& sessionMap
							.containsKey(STR_REQUESTCALLBACK)) {

					    sessionMap.put(STR_REQUESTCALLBACK,
						    STR_YES);

					}

				    }
				}

			    }
			}
		    }
		}

	    }

	}
    

}

