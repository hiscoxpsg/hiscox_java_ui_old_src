package com.hiscox.sbweb.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;

public final class HsxSBWebSessionManager implements IHsxSBWebConstants {

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();
    public static volatile Map<String, String> occupationMap = new HashMap<String, String>();
    public static volatile Map<String, String> occupVariantMap = new HashMap<String, String>();
    public static volatile Map<String, String> stateVariantMap = new HashMap<String, String>();


/**
 * This method is used to get the instance.
 */
    public static synchronized void getInstance() {
	if (null != occupationMap) {
	    Properties occupationProperties = (Properties) HsxSBWebResourceManager.getResource(OCCUPATION_PROPERTIES);
	    occupationMap = HsxSBWebPageBeanUtil.getMapFromProperty(
		    occupationProperties, occupationMap);
	    logger.info(SBWEB, INFO, "occupationMap created with size :"
		    + occupationMap.size());
	}
	if (null != occupVariantMap) {
	    Properties occupVariantProperties = (Properties) HsxSBWebResourceManager.getResource(OCCUPATION_VARIANT_PROPERTIES);
	    occupVariantMap = HsxSBWebPageBeanUtil.getMapFromProperty(
		    occupVariantProperties, occupVariantMap);
	    logger.info(SBWEB, INFO, "occupationVariantMap created with size :"
		    + occupVariantMap.size());

	}
	if (null != stateVariantMap) {
	    Properties stateVariantProperties = (Properties) HsxSBWebResourceManager.getResource(STATE_VARIANT_PROPERTIES);
	    stateVariantMap = HsxSBWebPageBeanUtil.getMapFromProperty(
		    stateVariantProperties, stateVariantMap);
	    logger.info(SBWEB, INFO, "stateVariantMap created with size :"
		    + stateVariantMap.size());

	}
    }

}

