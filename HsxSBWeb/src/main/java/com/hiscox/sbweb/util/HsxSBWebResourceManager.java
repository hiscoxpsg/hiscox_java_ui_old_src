package com.hiscox.sbweb.util;

import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.logger.HsxWebLogger;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebResourceManager {
    private static ApplicationContext context = null;

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    /**
     *
     * @param name
     * @return context bean name
     */
    public static Object getResource(String name) {
	return context.getBean(name);
    }

    /**
     *
     * @param context
     */
    public static void setContext(ApplicationContext context) {
	HsxSBWebResourceManager.context = context;
    }

}

