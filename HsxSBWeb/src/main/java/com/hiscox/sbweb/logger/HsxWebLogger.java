package com.hiscox.sbweb.logger;

import java.util.Enumeration;
import java.util.Properties;

import org.jboss.logging.Logger;

import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * This Class houses all the logging related activities. Log4j will be used for
 * logging activities in the USDC. Log4j enables logging at runtime without
 * modifying the application binary. Log4j needs to be configured up front using
 * a properties file.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:16 AM
 */
public class HsxWebLogger extends Logger implements IHsxSBWebConstants {

    /**
     *
     */
    private static final long serialVersionUID = 2159172442314752271L;

    public HsxWebLogger(String name) {
	super(name);
    }

    public static final Logger LOGGER = Logger.getLogger(HsxWebLogger.class);
    public boolean loggerFlag;

    /**
     *
     * This method carries the application name, the method to be called i.e
     * debug and the message as the input parameters. The properties file will
     * be read and the value of the key will be retrieved. Based on the Key
     * Value, the message will be displayed.
     *
     * @param packageName
     * @param methodName
     * @param message
     */
    @SuppressWarnings("deprecation")
    public void debug(String packageName, String methodName, String message) {
	boolean loggerValue;
	loggerValue = loggerCheck(packageName, methodName);
	if (loggerValue && LOGGER.isDebugEnabled()) {
	    LOGGER.debug(message);
	}
    }

    /**
     *
     * This method carries the application name, the method to be called i.e
     * error and the message as the input parameters. The properties file will
     * be read and the value of the key will be retrieved. Based on the Key
     * Value, the message will be displayed.
     *
     * @param packageName
     * @param methodName
     * @param message
     */
    public void error(String packageName, String methodName, String message) {
	boolean loggerValue;
	loggerValue = loggerCheck(packageName, methodName);
	if (loggerValue) {
	    LOGGER.error(message);
	}
	}

    /**
     *
     * This method carries the application name, the method to be called i.e
     * info and the message as the input parameters. The properties file will be
     * read and the value of the key will be retrieved. Based on the Key Value,
     * the message will be displayed.
     *
     * @param packageName
     * @param methodName
     * @param message
     */
    @SuppressWarnings("deprecation")
    public void info(String packageName, String methodName, String message) {
	boolean loggerValue;
	loggerValue = loggerCheck(packageName, methodName);
	if (loggerValue && LOGGER.isInfoEnabled()) {
	    LOGGER.info(message);
	}
    }

    /**
     *
     * This method reads form the properties file and compares it with the input
     * parameters. the Boolean value is returned.
     *
     * @param packageName
     * @param methodName
     * @return loggerFlag
     */
    public boolean loggerCheck(String packageName, String methodName) {

	Properties labels = null;
	labels = (Properties) HsxSBWebResourceManager
		.getResource(PROPERTIES_LOGGER);
	// ResourceBundle labels = ResourceBundle.getBundle(LOG_PROPERTIES);
	Enumeration<Object> bundleKeys = labels.keys();

	while (bundleKeys.hasMoreElements()) {
	    String loggerKey = (String) bundleKeys.nextElement();
	    String loggerVal = labels.getProperty(loggerKey);
	    String name = packageName.concat(STR_DOT).concat(methodName);

	    if (loggerKey.contains(name)) {
		if (loggerVal.equalsIgnoreCase(STR_YES)) {
		    loggerFlag = true;

		} else {
		    loggerFlag = false;
		}
	    }
	}
	return loggerFlag;
    }

    /**
     *
     * stack trace to determine the caller's class.
     *
     * @param packageName
     * @param methodName
     * @param message
     * @return webLogger or null
     */
    public static HsxWebLogger getLogger() {
	StackTraceElement[] stack = Thread.currentThread().getStackTrace();
	final int length = stack.length;
	for (int i = 0; i < length; i++) {
	    if (stack[i].getClassName().endsWith(WEB_LOGGER_CLASS)) {
		return new HsxWebLogger(stack[i + 1].getClassName());
	    }
	}
	return null;
    }

}

