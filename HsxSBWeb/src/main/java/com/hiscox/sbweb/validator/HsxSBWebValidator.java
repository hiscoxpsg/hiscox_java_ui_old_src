package com.hiscox.sbweb.validator;

import java.util.HashMap;

import org.springframework.validation.Errors;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebPageBeanUtil;
import com.hiscox.sbweb.util.HsxSBWebQuoteAndBuyControllerUtil;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:23 AM
 */
public class HsxSBWebValidator extends HsxSBWebBaseValidator {

    public HsxSBWebBean hsxSBWebBean;
    public HsxSBWebBaseValidator hsxSBWebBaseValidator;

    @SuppressWarnings("rawtypes")
	public boolean supports(Class aClass) {
	return HsxSBWebBean.class.equals(aClass);
	// return false;
    }

    /**
     * This method is responsible for validating the data entered in any of the
     * ScreenBuilder Web Pages.
     */

    @Override
    public void validate(Object obj, Errors errors) {
	if (obj instanceof HsxSBWebBean) {
	    hsxSBWebBean = (HsxSBWebBean) obj;
	    HashMap<String, HsxSBWidget> dynamicQuestionMap = (HashMap<String, HsxSBWidget>) hsxSBWebBean
		    .getWidgetMap();
	    boolean excepionOccuredFlag = false;
	    try {
		if (dynamicQuestionMap != null) {
		    validateScreenQuestions(hsxSBWebBean, errors,
			    dynamicQuestionMap);
		}
	    } catch (HsxSBUIBuilderRuntimeException e) {
		excepionOccuredFlag = true;
		logger.error("HsxSBUIBuilderRuntimeException",e);
		HsxSBWebQuoteAndBuyControllerUtil
			.logUiExceptionToErrorManagement(hsxSBWebBean,
				"HsxSBUIBuilderRuntimeException");
	    } catch (HsxSBUIBuilderException e) {
		excepionOccuredFlag = true;
		logger.error("HsxSBUIBuilderException",e);
		HsxSBWebQuoteAndBuyControllerUtil
			.logUiExceptionToErrorManagement(hsxSBWebBean,
				"HsxSBUIBuilderException");
	    } catch (Exception e) {
		excepionOccuredFlag = true;
		logger.error("Exception",e);
		
		HsxSBWebPageBeanUtil.logException(hsxSBWebBean, e);
	    }
	    if (excepionOccuredFlag) {
		// redirect to the System Error Page
		// logger.info(SBWEB, INFO,"Throwing new Exception");
		logger
			.info(
				SBWEB,
				INFO,
				"Throwing a Runtime Exception in Web Validator Which will handled by a Web Container and that will redirect to a System Error Page");
		// clear the session...

		HsxSBWebQuoteAndBuyControllerUtil
			.resetSessionParamters(hsxSBWebBean);
		throw new HsxSBUIBuilderRuntimeException();

	    }

	}
    }

}

