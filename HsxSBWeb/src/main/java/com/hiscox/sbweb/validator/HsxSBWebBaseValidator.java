package com.hiscox.sbweb.validator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.util.HsxCoreLDAPUtil;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

/**
 * This class holds the common validation methods required for the page
 * validation.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:19 AM
 */
public class HsxSBWebBaseValidator implements Validator, IHsxSBWebConstants {

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    /**
     *
     * @param validationHint
     * @return empty string
     */
    public String getValidationHintValue(String validationHint) {
	return STR_EMPTY;
    }

    /**
     * This method checks all mandatory questions based on
     * mandatoryQuestionsString and returns an error if exists.
     *
     * @param mandatoryQuestionsString
     * @param errors
     * @param sbwebBean
     */
    public void validateMandatory(String mandatoryQuestionsString,
	    Errors errors, HsxSBWebBean hsxSBWebBean) {

    }

    /**
     *
     * @param sbwebBean
     * @param errors
     * @param hideUnhideString
     * @throws HsxSBUIBuilderRuntimeException
     * @throws HsxSBUIBuilderException
     */
    public void validateScreenQuestions(HsxSBWebBean hsxSBWebBean,
	    Errors errors, HashMap<String, HsxSBWidget> dynamicQuestionMap)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {
	if (hsxSBWebBean != null) {
	    Boolean hasErrors = false;
	    HsxSBUIBuilderControllerImpl builderControllerImpl = new HsxSBUIBuilderControllerImpl();
	    Properties validationHintProperties = (Properties) HsxSBWebResourceManager
		    .getResource("validationHintProperties");
	    // HsxSBUIBuilderControllerImpl.sessionMap =
	    // hsxSBWebBean.getSessionMap();
	    Properties appProperties = null;
	    appProperties = (Properties) HsxSBWebResourceManager
		    .getResource(APPCONFIG_PROPERTIES);

	    String env = STR_EMPTY;
	    if (appProperties != null) {
		env = appProperties.getProperty(ENVIRONMENT);

	    }
	    if (env == null) {
		env = STR_EMPTY;
	    }
	    validationHintProperties.setProperty(ENVIRONMENT, env);
	    hasErrors = builderControllerImpl.validateScreenBuilder(
		    hsxSBWebBean.getWidget(), dynamicQuestionMap, hsxSBWebBean
			    .getIsJSEnabled(), validationHintProperties,
		    hsxSBWebBean.getRequestedAction(), hsxSBWebBean
			    .getGroupValidationFlag());
	    	
	    if (STR_YES.equalsIgnoreCase(hsxSBWebBean
		    .getCheckBoxValidationFlag())
		    && !hasErrors) {
		hasErrors = validateProductOptionsPage(hsxSBWebBean,
			dynamicQuestionMap);
	    } else if (STR_YES.equalsIgnoreCase(hsxSBWebBean
		    .getRadioButtonValidationFlag())
		    && !hasErrors) {
		hasErrors = validateCoverageOptionsPage(hsxSBWebBean,
			dynamicQuestionMap);
	    }
	    //US 14815 Start
	    /**
	     * Condition to check whether user already saved the quote or not.If already saved
	     * with same email id,display the error message.
	     */
	    else if(hsxSBWebBean.getNextScreenId().equalsIgnoreCase(
	    		STR_SAVE_QUOTE)){
	    	String genericPageErrorText = hsxSBWebBean.getWidget()
					.getWidgetInfo().getAdditionalText2();
			String customPageErrorText = hsxSBWebBean.getWidget()
					.getWidgetInfo().getAdditionalText3();
	    	if (!hasErrors) {
  				Boolean isUserExit = validateEmailAddressWithLDAP(dynamicQuestionMap, appProperties);
  				if (!isUserExit) {
  					hasErrors = true;
  					hsxSBWebBean.getWidget().getWidgetInfo()
  							.setErrorIndicator(true);
  					hsxSBWebBean.getWidget().getWidgetInfo()
  							.setPageErrorText(customPageErrorText);
  				}
  			} else {
  				hsxSBWebBean.getWidget().getWidgetInfo()
  						.setPageErrorText(genericPageErrorText);
  			}
	    	
	    }
	    //US 14815 End
		//US 10736 Start
	    else if (hsxSBWebBean.getNextScreenId().equalsIgnoreCase(
  				STR_FORGOTTEN_PASSWORD)) {
	    	String genericPageErrorText = hsxSBWebBean.getWidget()
					.getWidgetInfo().getAdditionalText2();
			String customPageErrorText = hsxSBWebBean.getWidget()
					.getWidgetInfo().getAdditionalText3();
  			if (!hasErrors) {
  				hasErrors = validateEmailAddressWithLDAP(dynamicQuestionMap, appProperties);
  				if (hasErrors) {
  					hsxSBWebBean.getWidget().getWidgetInfo()
  							.setErrorIndicator(true);
  					hsxSBWebBean.getWidget().getWidgetInfo()
  							.setPageErrorText(customPageErrorText);
  				}
  			} else {
  				hsxSBWebBean.getWidget().getWidgetInfo()
  						.setPageErrorText(genericPageErrorText);
  			}
  		}
	  	//US 10736 End
	    logger.info(SBWEB, INFO, "hasErrors :" + hasErrors);
	    // reset the Page Validation Flag
	    hsxSBWebBean.setPageValidationFailedFlag(false);
	    if (hasErrors) {
		errors.reject(STR_EMPTY);
		hsxSBWebBean.setPageValidationFailedFlag(true);
		if (hsxSBWebBean.getWidget() != null) {
		    HsxSBWidgetInfo pageWidgetInfo = hsxSBWebBean.getWidget()
			    .getWidgetInfo();
		    // logger.info(SBWEB, INFO, "Page Widget Info :" +
		    // pageWidgetInfo);
		    if (pageWidgetInfo != null) {
			// logger.info(SBWEB, INFO, "Page Widget Info Id :"
			// + pageWidgetInfo.getId());
			hsxSBWebBean.getWidget().getWidgetInfo()
				.setErrorIndicator(true);
		    }
		}
	    } else {
		if (hsxSBWebBean != null && hsxSBWebBean.getWidget() != null) {
		    HsxSBWidgetInfo pageWidgetInfo = hsxSBWebBean.getWidget()
			    .getWidgetInfo();
		    // logger.info(SBWEB, INFO, "Page Widget Info :" +
		    // pageWidgetInfo);
		    if (pageWidgetInfo != null) {
			// logger.info(SBWEB, INFO, "Page Widget Info Id :"
			// + pageWidgetInfo.getId());
			hsxSBWebBean.getWidget().getWidgetInfo()
				.setErrorIndicator(false);
		    }
		}
	    }
	    builderControllerImpl = null;
	}
    }

    @SuppressWarnings("rawtypes")
	public boolean supports(Class aClass) {
	return HsxSBWebBaseValidator.class.equals(aClass);
    }

    public void validate(Object arg0, Errors arg1) {
	logger.info(SBWEB, INFO, "In the base validator class");

    }

    private boolean validateProductOptionsPage(HsxSBWebBean hsxSBWebBean,
	    HashMap<String, HsxSBWidget> dynamicQuestionMap) {
	Collection<String> questionList = dynamicQuestionMap.keySet();
	boolean validSavedValueFlag = true;
	for (Iterator<String> questionIterator = questionList.iterator(); questionIterator
		.hasNext();) {
	    String quesitonId = questionIterator.next();
	    HsxSBWidget hsxSBWidget = dynamicQuestionMap.get(quesitonId);
	    String widgetType = hsxSBWidget.getWidgetInfo().getType();
	    if (STR_CHECKBOX.equalsIgnoreCase(widgetType) && STR_YES.equalsIgnoreCase(hsxSBWidget.getWidgetInfo()
				.getSavedValue())) {
		    validSavedValueFlag = false;
		    break;
		
	    }
	}
	if (validSavedValueFlag) {
	    hsxSBWebBean.getWidget().getWidgetInfo().setErrorIndicator(true);
	}

	return validSavedValueFlag;
    }

    private boolean validateCoverageOptionsPage(HsxSBWebBean hsxSBWebBean,
	    HashMap<String, HsxSBWidget> dynamicQuestionMap) {
	Collection<String> questionList = dynamicQuestionMap.keySet();
	boolean validSavedValueFlag = true;
	for (Iterator<String> questionIterator = questionList.iterator(); questionIterator
		.hasNext();) {
	    String quesitonId = questionIterator.next();
	    HsxSBWidget hsxSBWidget = dynamicQuestionMap.get(quesitonId);
	    String widgetType = hsxSBWidget.getWidgetInfo().getType();
	    if (STR_RADIO.equalsIgnoreCase(widgetType)) {
		String radioSavedValue = hsxSBWidget.getWidgetInfo()
			.getSavedValue();
		if (StringUtils.isNotBlank(radioSavedValue)
			&& !STR_NO.equalsIgnoreCase(radioSavedValue)
			&& !STR_NEITHER.equalsIgnoreCase(radioSavedValue)) {
		    validSavedValueFlag = false;
		    break;
		}
	    }
	}
	    if (validSavedValueFlag && hsxSBWebBean != null && hsxSBWebBean.getWidget() != null
		    && hsxSBWebBean.getWidget().getWidgetInfo() != null) {
		hsxSBWebBean.getWidget().getWidgetInfo()
			.setErrorIndicator(true);
	    }
	

	return validSavedValueFlag;

    }
    //US 10736 Start
	/*
	 * This method is used to validate the email address field with the LDAP
	 * registry on The forgot password page.
	 */
	private boolean validateEmailAddressWithLDAP(HashMap<String, HsxSBWidget> dynamicQuestionMap,
			Properties appConfigProperties) {

		boolean isNewUser = true;
		String ldapName = appConfigProperties.getProperty(STR_LDAP_URL)
			    .trim();
		String managerDn = appConfigProperties.getProperty(STR_MANAGER_DN)
			    .trim();
		String password = appConfigProperties.getProperty(
			    STR_ACEGI_PASSWORD).trim();
		Collection<String> questionList = dynamicQuestionMap.keySet();

		for (Iterator<String> questionIterator = questionList.iterator(); questionIterator
				.hasNext();) {

			String quesitonId = questionIterator.next();
			HsxSBWidget hsxSBWidget = dynamicQuestionMap.get(quesitonId);
			String widgetSubType = hsxSBWidget.getWidgetInfo().getSubType();

			if (CONFIG_EMAIL_SUBTYPE.equalsIgnoreCase(widgetSubType)) {

				String emailSavedValue = hsxSBWidget
						.getWidgetInfo().getSavedValue();
				try {
					HsxCoreLDAPUtil ldapsUtil = new HsxCoreLDAPUtil();
					isNewUser = ldapsUtil.checkUserNameExistsInLDAP(
							emailSavedValue, ldapName, managerDn, password);
				} catch (HsxCoreRuntimeException e) {
					logger.error(SBWEB,ERROR,"HsxCoreRuntimeException occurred in LDAP Validation: "
									+ e.getMessage());
					logger.error("HsxCoreRuntimeException", e);
				} catch (HsxCoreException e) {
					logger.error(SBWEB,ERROR,"HsxCoreException occurred in LDAP Validation: "
									+ e.getMessage());
					logger.error("HsxCoreException",e);
				}
				break;
			}
		}
		return isNewUser;
	}
    //US 10736 Ends

}

