<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>BOP Insurance - FAQs about Business Owner Insurance | Hiscox USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
<meta name="description" content="Hiscox business owner insurance policies offer a range of protection at competitive rates. Visit Hiscox for answers to questions about BOP insurance." />
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print"/>
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body onload="rotatingtext()">
	<!--main container starts here-->
	<div class="main-container">
		<!-- Header starts here-->
		<div class="header">			
		<div class="logo-home">
	
	
	
    							
			                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		<div class="topmenu-container">
			<div class="utitlitylinks-hlder">
				<ul>
	
	
	
    							
							    				    				<li><p class="body-copy-grey">
						<a href="/about-hiscox-insurance/" title="" target="_blank" >
						About Hiscox</a></p>
					</li>
										
    				
							    				    				<li><p class="body-copy-grey">
						<a href="/contact-us/" title="" target="_blank" >
						Contact us</a></p>
					</li>
										
    	    		
		 		</ul>			</div>		
					
											<ul class="topmenu">					
																																																																																																									<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist firstitem">
					<div class="leftcurve"></div><div class="leftmiddle">
						<a href="/" target="_self" class="topmenu-links">Home page </a> 
					</div>
				</li>							 
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																													<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist-sel">
					<a href="/small-business-insurance/" target="_self" class="topmenu-links-twoline">Small business insurance direct</a>																																																																																												            <usd:checkProductEligibility productContent="RequestCallBack,AddEnO,AddGL">
						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																																																							<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/why-choose-hiscox-insurance/" target="_self" class='menulinks'>Why choose Hiscox Insurance?</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/quotes/" target="_self" class='menulinks'>Great value insurance quotes</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/professional-liability-insurance/" target="_self" class='menulinks'>Professional Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddEnO"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/errors-and-omissions-insurance/" target="_self" class='menulinks'>Errors and Omissions Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddGL"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/general-liability-insurance/" target="_self" class='menulinks'>General Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddGL,AddBOP"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/business-owner-insurance/" target="_self" class='menulinks'>Business Owner Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="RequestCallBack"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/workers-compensation-insurance/" target="_self" class='menulinks'>Workers' Compensation Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																																																																																																																																																																												</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
					</usd:checkProductEligibility>
									</li>	
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																								<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist">
					<a href="/broker/" target="_self" class="topmenu-links-twoline">Business insurance through brokers</a>																																            <usd:checkProductEligibility productContent="RequestCallBack,AddEnO,AddGL">
						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																												<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_professional_insurance.htm" target="_self" class='menulinks'>Professional insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_property_insurance.htm" target="_self" class='menulinks'>Property insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_specialty.htm" target="_self" class='menulinks'>Specialty insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_broker.htm" target="_self" class='menulinks'>Broker center</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																						</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
					</usd:checkProductEligibility>
									</li>	
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																								<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist lastitem">
					<div class="rightmiddle">
						<a href="/insurance-products/" target="_self" class="topmenu-links">Products</a>					</div>
					<div class="rightcurve"></div>
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																														</ul>
								</div>
    		<div class="header-hp-right">
    			
	
	
	
    							
																<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="red-call-to-action-small"><a href="http://www.hiscox.com"><span class="icon-open-in-new-window"><b>Group site</b></span></a> </p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/en/careers.aspx">Careers</a></p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/investors.aspx">Investors</a></p></div>
										
    	    		
		 		    		</div>
		</div>
		<!-- Header ends here-->
		<!-- Page container starts here-->
		<div class="page-container">
		<!--Breadcrumb starts here-->
					<div class="breadcrumb">
				
	<div class="brdcrmb-sub">
			                    	                    	                    					<div class="breadcrumb-link"><a href="/">Home page</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/">Small business insurance direct</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/business-owner-insurance/">Business Owner Insurance</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-current"><span>BOP Insurance - FAQs</span></div>
			</div>
									<usd:checkEligibility eligibleContent="stateVariant,primarybusiness">
					<div class="occupation-details">
						<span class = "float-right"><object>
						<div class="occupation-info"><span>State:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="stateVariant" /></a></div>
						<div class="occupation-info"><span>Type of business:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="primarybusiness" /></a></div>
						</object></span>
					</div>
					</usd:checkEligibility>
							</div>
		<!--Breadcrumb ends here-->
		<!--Left nav starts here-->
		<div class="leftnav-gap clear-both">
						
							        	    		    									<div class="leftmenu-main">
																																																					<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="all">							<div class="leftmenu-mainitem">
					<div class="mainitem-topleft" ></div><div class="mainitem-topmiddle" ></div><div class="mainitem-topright"></div>
						<div class="mainitem-middle">
							<a href="/small-business-insurance/" target="_self" class="main-link">Small business insurance direct</a>
						</div>
					<div class="mainitem-btmleft"></div><div class="mainitem-btmmiddle" ></div><div class="mainitem-btmright"></div>
				</div>
					</usd:checkProductEligibility>	</usd:checkOccup>													<div class="leftmenu">
					<div class="leftmenu-top"><div class="leftmenu-topleft"></div><div class="leftmenu-topmiddle"></div><div class="leftmenu-topright"></div></div>
						<div class="leftmenu-middle">
							<ul>
																	        																		        																		        																		        																		        																		            	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    																																																					<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem-sel"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/" target="_self" class="leftmenulinks-sel">Business Owner Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>																		  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/bop-quotes/" target="_self" class="leftmenulinks-nopad">Business Owner Insurance Quotes</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/office-insurance/" target="_self" class="leftmenulinks-nopad">Office Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/commercial-property/" target="_self" class="leftmenulinks-nopad">Commercial Property Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/gl-insurance-and-bop/" target="_self" class="leftmenulinks-nopad">BOP Insurance and General Liability Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/non-owned-auto-liability/" target="_self" class="leftmenulinks-nopad">Non-Owned Auto Liability Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/commercial-crime/" target="_self" class="leftmenulinks-nopad">Commercial Crime Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/data-loss-insurance/" target="_self" class="leftmenulinks-nopad">Electronic Data Loss Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																																																												<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="last-item">
					<div class="leftmenu-listitem-sel2"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/bop-faq/" target="_self" class="leftmenulinks-sel">BOP Insurance - FAQs</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>																	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="last-item">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/bop-state/" target="_self" class="leftmenulinks-nopad">Business Owner Insurance in my state?</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    				   																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																</ul>
						</div>
					<div class="leftmenu-bottom"><div class="leftmenu-btmleft"></div><div class="leftmenu-btmmiddle"></div><div class="leftmenu-btmright"></div></div>
				</div>
			</div>
			    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    	    					
				    		    				    	    		    				    	    		    				    	    		   			    		    		    		    		    		    		    								<!--Left nav starts here and the div starts in the global template-->
					<usd:checkOccup occupContent="it-consulting" > 
					                                  															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  																							<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    		    															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
					<div class="horz-line"></div> 
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    										    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  										    										    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup> 
		</div>
		<!--Left nav ends here-->
		<!--Content section starts here-->
				<div class="cnt-sctn no-bg">			
																													<usd:checkOccup occupContent="it-consulting" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">BOP Insurance - FAQs</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																								<usd:checkOccup occupContent="consulting" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">BOP Insurance - FAQs</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																								<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">BOP Insurance - FAQs</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																								<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">BOP Insurance - FAQs</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																										<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">BOP Insurance - FAQs</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																														<usd:checkOccup occupContent="it-consulting" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="grey-heading-h2"><b>Common Questions About Business Owners Insurance</b></h2><p class="body-copy-grey">Below are some common questions about our business owners policy, also known as business owners insurance. If you have other questions or wish to discuss our policy, please contact one of our licensed advisors either by phone 888-202-3007 (Mon-Fri, 8am-10pm EST), live chat or email us at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a>.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why should I choose a Hiscox business owners insurance policy?</b></h2><p class="body-copy-grey">A Hiscox business owners policy provides you with general liability coverage and also protects your business equipment. You should also consider this policy if you own your office building as this can be covered as well. Even better, you can customize your policy with our policy upgrades, which gives you the right coverage at a great price as you only pay for what you really need. No more, no less.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why is the Hiscox online process so much simpler?</b></h2><p class="body-copy-grey">We understand that your time is money. We specialize in insurance for professional services businesses, so we understand the risks you face. This lets us simplify our quote-and-buy process and focus on the questions that only apply to your business. Other insurers applications are typically designed to cover all types of industries outside of professional services (e.g., restaurants, shops, mechanics etc.), all of which have very different risks and exposures.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We only ask you questions relevant to your business, making the process quicker and more straightforward than anyone else, whether online, or over the phone. Get started on your <a href="/small-business-insurance/" title="">business owners insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What are the benefits of buying BOP insurance directly from Hiscox?</b></h2><p class="body-copy-grey">When you buy direct, you deal directly with Hiscox. Our licensed small business insurance advisors only focus on professional services businesses like yours. This means you speak to a knowledgeable specialist who can deliver personalized recommendations about the insurance you need in your specific profession.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">With Hiscox you get great value as well as excellent service. Buying direct is a simple process that allows you to get the right coverage at a competitive price.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What types of businesses can you offer business owners insurance to?</b></h2><p class="body-copy-grey">Hiscox BOP insurance covers most types of professional services businesses with 10 employees or less. This includes IT/technology consultants, marketing consultants, and business or management consultants. We’ll also happily insure recruiters, PR specialists, graphic designers, photographers, interior designers and more types of service professionals. If you don’t see your specific business type on this list, contact us to find out if we can provide business owners insurance coverage for your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What do I need to get a BOP insurance quote?</b></h2><p class="body-copy-grey">The quote process is simple whether you go online or call a licensed advisor 888-202-3007 (Mon-Fri, 8am-10pm EST). We’ll ask you for some basic information about your business, previous claims, type of work, etc.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Learn more and start a <a href="/small-business-insurance/" title="">small business insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How can I save money on my business owners insurance?</b></h2><p class="body-copy-grey">Buying this type of insurance direct from Hiscox means you enjoy greater overall value. We like to reward businesses that actively manage their risk. Depending on your specific business, you will be asked questions about the types of risk management practices you employ, allowing us to offer rate reductions to those who actively reduce their chances of suffering a loss.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We also offer customers up to a 5% rate reduction if you buy two or more Hiscox insurance products. Also, by choosing a higher deductible (the initial amount you pay for each loss), you can also lower your premium. It’s only fair, as you’re taking on more of the risk.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How much does a BOP insurance cost?</b></h2><p class="body-copy-grey">Business owners insurance policy premiums vary depending on the location and size of your business, your occupation, the types of activities you conduct and the risk management practices you employ. However, our minimum premiums start from as little as $400 per year. See this page for more <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">business</a> owners policy quote examples or get a simple, no-obligation quote for your business now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Is BOP insurance available in my state?</b></h2><p class="body-copy-grey">Hiscox is currently licensed to offer our business owners insurance in various states across the U.S. View our <a href="/small-business-insurance/business-owner-insurance/bop-state/" title="">business owners insurance</a> state insurance page for details. However, if we don’t currently operate in your state we may still be able to help. We have access to specialist insurance brokers across the U.S. who may be able to assist you. Contact one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Find out if Hiscox BOP insurance is offered in your state and start a quote today.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What should I do if I need to make a claim?</b></h2><p class="body-copy-grey">You can report a claim 24 hours a day, 7 days a week. Just call us on 866-424-8508 or send us an email at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a> and an advisor will get back to you as quickly as possible. We’ll advise you on what to do and, if necessary, appoint a lawyer on your behalf.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What payment options exist for my business owners policy?</b></h2><p class="body-copy-grey">Hiscox offers a range of payment options, including debit and credit card (Visa, MasterCard and American Express). You can also choose to pay the full amount in one lump sum each year or pay monthly (with no fees added) to help spread your costs.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What deductible options do you offer?</b></h2><p class="body-copy-grey">For our business owners insurance, we offer a range of deductible options. This is the amount you must pay on each claim before the policy pays out. These range from $500 to $10,000 for any building or business equipment related loss. The higher the deductible, the lower your premium, since you take on more of the risk. Get a quote online and select different deductible amounts to see how it affects your premium. Any claim relating to the general liability insurance section of your policy has no deductible.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What happens if I cancel my policy?</b></h2><p class="body-copy-grey">You can cancel your Hiscox policy at any time and we will issue you a refund. However, as soon as you cancel your policy you will no longer have any protection in place for occurrences after your policy is canceled.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Does a Hiscox business owners policy cover work completed temporary staff?</b></h2><p class="body-copy-grey">Yes. Claims arising from actions by full-time and temporary staff are covered.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">Be confident that you've made the right choice. Hiscox gives you 14 days to review the policy. If you are not satisfied for any reason and have not had a claim or loss, you can cancel and receive a full refund.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="consulting" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="grey-heading-h2"><b>Common Questions About Business Owners Insurance</b></h2><p class="body-copy-grey">Below are some common questions about our business owners policy, also known as business owners insurance. If you have other questions or wish to discuss our policy, please contact one of our licensed advisors either by phone 888-202-3007 (Mon-Fri, 8am-10pm EST), live chat or email us at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a>.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why should I choose a Hiscox business owners insurance policy?</b></h2><p class="body-copy-grey">A Hiscox business owners policy provides you with general liability coverage and also protects your business equipment. You should also consider this policy if you own your office building as this can be covered as well. Even better, you can customize your policy with our policy upgrades, which gives you the right coverage at a great price as you only pay for what you really need. No more, no less.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why is the Hiscox online process so much simpler?</b></h2><p class="body-copy-grey">We understand that your time is money. We specialize in insurance for professional services businesses, so we understand the risks you face. This lets us simplify our quote-and-buy process and focus on the questions that only apply to your business. Other insurers applications are typically designed to cover all types of industries outside of professional services (e.g., restaurants, shops, mechanics etc.), all of which have very different risks and exposures.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We only ask you questions relevant to your business, making the process quicker and more straightforward than anyone else, whether online, or over the phone. Get started on your <a href="/small-business-insurance/" title="">business owners insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What are the benefits of buying BOP insurance directly from Hiscox?</b></h2><p class="body-copy-grey">When you buy direct, you deal directly with Hiscox. Our licensed small business insurance advisors only focus on professional services businesses like yours. This means you speak to a knowledgeable specialist who can deliver personalized recommendations about the insurance you need in your specific profession.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">With Hiscox you get great value as well as excellent service. Buying direct is a simple process that allows you to get the right coverage at a competitive price.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What types of businesses can you offer business owners insurance to?</b></h2><p class="body-copy-grey">Hiscox BOP insurance covers most types of professional services businesses with 10 employees or less. This includes IT/technology consultants, marketing consultants, and business or management consultants. We’ll also happily insure recruiters, PR specialists, graphic designers, photographers, interior designers and more types of service professionals. If you don’t see your specific business type on this list, contact us to find out if we can provide business owners insurance coverage for your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What do I need to get a BOP insurance quote?</b></h2><p class="body-copy-grey">The quote process is simple whether you go online or call a licensed advisor 888-202-3007 (Mon-Fri, 8am-10pm EST). We’ll ask you for some basic information about your business, previous claims, type of work, etc.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Learn more and start a <a href="/small-business-insurance/" title="">small business insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How can I save money on my business owners insurance?</b></h2><p class="body-copy-grey">Buying this type of insurance direct from Hiscox means you enjoy greater overall value. We like to reward businesses that actively manage their risk. Depending on your specific business, you will be asked questions about the types of risk management practices you employ, allowing us to offer rate reductions to those who actively reduce their chances of suffering a loss.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We also offer customers up to a 5% rate reduction if you buy two or more Hiscox insurance products. Also, by choosing a higher deductible (the initial amount you pay for each loss), you can also lower your premium. It’s only fair, as you’re taking on more of the risk.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How much does a BOP insurance cost?</b></h2><p class="body-copy-grey">Business owners insurance policy premiums vary depending on the location and size of your business, your occupation, the types of activities you conduct and the risk management practices you employ. However, our minimum premiums start from as little as $400 per year. See this page for more <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">business</a> owners policy quote examples or get a simple, no-obligation quote for your business now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Is BOP insurance available in my state?</b></h2><p class="body-copy-grey">Hiscox is currently licensed to offer our business owners insurance in various states across the U.S. View our <a href="/small-business-insurance/business-owner-insurance/bop-state/" title="">business owners insurance</a> state insurance page for details. However, if we don’t currently operate in your state we may still be able to help. We have access to specialist insurance brokers across the U.S. who may be able to assist you. Contact one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Find out if Hiscox BOP insurance is offered in your state and start a quote today.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What should I do if I need to make a claim?</b></h2><p class="body-copy-grey">You can report a claim 24 hours a day, 7 days a week. Just call us on 866-424-8508 or send us an email at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a> and an advisor will get back to you as quickly as possible. We’ll advise you on what to do and, if necessary, appoint a lawyer on your behalf.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What payment options exist for my business owners policy?</b></h2><p class="body-copy-grey">Hiscox offers a range of payment options, including debit and credit card (Visa, MasterCard and American Express). You can also choose to pay the full amount in one lump sum each year or pay monthly (with no fees added) to help spread your costs.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What deductible options do you offer?</b></h2><p class="body-copy-grey">For our business owners insurance, we offer a range of deductible options. This is the amount you must pay on each claim before the policy pays out. These range from $500 to $10,000 for any building or business equipment related loss. The higher the deductible, the lower your premium, since you take on more of the risk. Get a quote online and select different deductible amounts to see how it affects your premium. Any claim relating to the general liability insurance section of your policy has no deductible.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What happens if I cancel my policy?</b></h2><p class="body-copy-grey">You can cancel your Hiscox policy at any time and we will issue you a refund. However, as soon as you cancel your policy you will no longer have any protection in place for occurrences after your policy is canceled.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Does a Hiscox business owners policy cover work completed temporary staff?</b></h2><p class="body-copy-grey">Yes. Claims arising from actions by full-time and temporary staff are covered.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">Be confident that you've made the right choice. Hiscox gives you 14 days to review the policy. If you are not satisfied for any reason and have not had a claim or loss, you can cancel and receive a full refund.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="grey-heading-h2"><b>Common Questions About Business Owners Insurance</b></h2><p class="body-copy-grey">Below are some common questions about our business owners policy, also known as business owners insurance. If you have other questions or wish to discuss our policy, please contact one of our licensed advisors either by phone 888-202-3007 (Mon-Fri, 8am-10pm EST), live chat or email us at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a>.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why should I choose a Hiscox business owners insurance policy?</b></h2><p class="body-copy-grey">A Hiscox business owners policy provides you with general liability coverage and also protects your business equipment. You should also consider this policy if you own your office building as this can be covered as well. Even better, you can customize your policy with our policy upgrades, which gives you the right coverage at a great price as you only pay for what you really need. No more, no less.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why is the Hiscox online process so much simpler?</b></h2><p class="body-copy-grey">We understand that your time is money. We specialize in insurance for professional services businesses, so we understand the risks you face. This lets us simplify our quote-and-buy process and focus on the questions that only apply to your business. Other insurers applications are typically designed to cover all types of industries outside of professional services (e.g., restaurants, shops, mechanics etc.), all of which have very different risks and exposures.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We only ask you questions relevant to your business, making the process quicker and more straightforward than anyone else, whether online, or over the phone. Get started on your <a href="/small-business-insurance/" title="">business owners insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What are the benefits of buying BOP insurance directly from Hiscox?</b></h2><p class="body-copy-grey">When you buy direct, you deal directly with Hiscox. Our licensed small business insurance advisors only focus on professional services businesses like yours. This means you speak to a knowledgeable specialist who can deliver personalized recommendations about the insurance you need in your specific profession.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">With Hiscox you get great value as well as excellent service. Buying direct is a simple process that allows you to get the right coverage at a competitive price.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What types of businesses can you offer business owners insurance to?</b></h2><p class="body-copy-grey">Hiscox BOP insurance covers most types of professional services businesses with 10 employees or less. This includes IT/technology consultants, marketing consultants, and business or management consultants. We’ll also happily insure recruiters, PR specialists, graphic designers, photographers, interior designers and more types of service professionals. If you don’t see your specific business type on this list, contact us to find out if we can provide business owners insurance coverage for your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What do I need to get a BOP insurance quote?</b></h2><p class="body-copy-grey">The quote process is simple whether you go online or call a licensed advisor 888-202-3007 (Mon-Fri, 8am-10pm EST). We’ll ask you for some basic information about your business, previous claims, type of work, etc.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Learn more and start a <a href="/small-business-insurance/" title="">small business insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How can I save money on my business owners insurance?</b></h2><p class="body-copy-grey">Buying this type of insurance direct from Hiscox means you enjoy greater overall value. We like to reward businesses that actively manage their risk. Depending on your specific business, you will be asked questions about the types of risk management practices you employ, allowing us to offer rate reductions to those who actively reduce their chances of suffering a loss.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We also offer customers up to a 5% rate reduction if you buy two or more Hiscox insurance products. Also, by choosing a higher deductible (the initial amount you pay for each loss), you can also lower your premium. It’s only fair, as you’re taking on more of the risk.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How much does a BOP insurance cost?</b></h2><p class="body-copy-grey">Business owners insurance policy premiums vary depending on the location and size of your business, your occupation, the types of activities you conduct and the risk management practices you employ. However, our minimum premiums start from as little as $400 per year. See this page for more <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">business</a> owners policy quote examples or get a simple, no-obligation quote for your business now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Is BOP insurance available in my state?</b></h2><p class="body-copy-grey">Hiscox is currently licensed to offer our business owners insurance in various states across the U.S. View our <a href="/small-business-insurance/business-owner-insurance/bop-state/" title="">business owners insurance</a> state insurance page for details. However, if we don’t currently operate in your state we may still be able to help. We have access to specialist insurance brokers across the U.S. who may be able to assist you. Contact one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Find out if Hiscox BOP insurance is offered in your state and start a quote today.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What should I do if I need to make a claim?</b></h2><p class="body-copy-grey">You can report a claim 24 hours a day, 7 days a week. Just call us on 866-424-8508 or send us an email at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a> and an advisor will get back to you as quickly as possible. We’ll advise you on what to do and, if necessary, appoint a lawyer on your behalf.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What payment options exist for my business owners policy?</b></h2><p class="body-copy-grey">Hiscox offers a range of payment options, including debit and credit card (Visa, MasterCard and American Express). You can also choose to pay the full amount in one lump sum each year or pay monthly (with no fees added) to help spread your costs.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What deductible options do you offer?</b></h2><p class="body-copy-grey">For our business owners insurance, we offer a range of deductible options. This is the amount you must pay on each claim before the policy pays out. These range from $500 to $10,000 for any building or business equipment related loss. The higher the deductible, the lower your premium, since you take on more of the risk. Get a quote online and select different deductible amounts to see how it affects your premium. Any claim relating to the general liability insurance section of your policy has no deductible.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What happens if I cancel my policy?</b></h2><p class="body-copy-grey">You can cancel your Hiscox policy at any time and we will issue you a refund. However, as soon as you cancel your policy you will no longer have any protection in place for occurrences after your policy is canceled.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Does a Hiscox business owners policy cover work completed temporary staff?</b></h2><p class="body-copy-grey">Yes. Claims arising from actions by full-time and temporary staff are covered.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">Be confident that you've made the right choice. Hiscox gives you 14 days to review the policy. If you are not satisfied for any reason and have not had a claim or loss, you can cancel and receive a full refund.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="grey-heading-h2"><b>Common Questions About Business Owners Insurance</b></h2><p class="body-copy-grey">Below are some common questions about our business owners policy, also known as business owners insurance. If you have other questions or wish to discuss our policy, please contact one of our licensed advisors either by phone 888-202-3007 (Mon-Fri, 8am-10pm EST), live chat or email us at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a>.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why should I choose a Hiscox business owners insurance policy?</b></h2><p class="body-copy-grey">A Hiscox business owners policy provides you with general liability coverage and also protects your business equipment. You should also consider this policy if you own your office building as this can be covered as well. Even better, you can customize your policy with our policy upgrades, which gives you the right coverage at a great price as you only pay for what you really need. No more, no less.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why is the Hiscox online process so much simpler?</b></h2><p class="body-copy-grey">We understand that your time is money. We specialize in insurance for professional services businesses, so we understand the risks you face. This lets us simplify our quote-and-buy process and focus on the questions that only apply to your business. Other insurers applications are typically designed to cover all types of industries outside of professional services (e.g., restaurants, shops, mechanics etc.), all of which have very different risks and exposures.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We only ask you questions relevant to your business, making the process quicker and more straightforward than anyone else, whether online, or over the phone. Get started on your <a href="/small-business-insurance/" title="">business owners insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What are the benefits of buying BOP insurance directly from Hiscox?</b></h2><p class="body-copy-grey">When you buy direct, you deal directly with Hiscox. Our licensed small business insurance advisors only focus on professional services businesses like yours. This means you speak to a knowledgeable specialist who can deliver personalized recommendations about the insurance you need in your specific profession.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">With Hiscox you get great value as well as excellent service. Buying direct is a simple process that allows you to get the right coverage at a competitive price.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What types of businesses can you offer business owners insurance to?</b></h2><p class="body-copy-grey">Hiscox BOP insurance covers most types of professional services businesses with 10 employees or less. This includes IT/technology consultants, marketing consultants, and business or management consultants. We’ll also happily insure recruiters, PR specialists, graphic designers, photographers, interior designers and more types of service professionals. If you don’t see your specific business type on this list, contact us to find out if we can provide business owners insurance coverage for your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What do I need to get a BOP insurance quote?</b></h2><p class="body-copy-grey">The quote process is simple whether you go online or call a licensed advisor 888-202-3007 (Mon-Fri, 8am-10pm EST). We’ll ask you for some basic information about your business, previous claims, type of work, etc.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Learn more and start a <a href="/small-business-insurance/" title="">small business insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How can I save money on my business owners insurance?</b></h2><p class="body-copy-grey">Buying this type of insurance direct from Hiscox means you enjoy greater overall value. We like to reward businesses that actively manage their risk. Depending on your specific business, you will be asked questions about the types of risk management practices you employ, allowing us to offer rate reductions to those who actively reduce their chances of suffering a loss.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We also offer customers up to a 5% rate reduction if you buy two or more Hiscox insurance products. Also, by choosing a higher deductible (the initial amount you pay for each loss), you can also lower your premium. It’s only fair, as you’re taking on more of the risk.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How much does a BOP insurance cost?</b></h2><p class="body-copy-grey">Business owners insurance policy premiums vary depending on the location and size of your business, your occupation, the types of activities you conduct and the risk management practices you employ. However, our minimum premiums start from as little as $400 per year. See this page for more <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">business</a> owners policy quote examples or get a simple, no-obligation quote for your business now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Is BOP insurance available in my state?</b></h2><p class="body-copy-grey">Hiscox is currently licensed to offer our business owners insurance in various states across the U.S. View our <a href="/small-business-insurance/business-owner-insurance/bop-state/" title="">business owners insurance</a> state insurance page for details. However, if we don’t currently operate in your state we may still be able to help. We have access to specialist insurance brokers across the U.S. who may be able to assist you. Contact one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Find out if Hiscox BOP insurance is offered in your state and start a quote today.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What should I do if I need to make a claim?</b></h2><p class="body-copy-grey">You can report a claim 24 hours a day, 7 days a week. Just call us on 866-424-8508 or send us an email at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a> and an advisor will get back to you as quickly as possible. We’ll advise you on what to do and, if necessary, appoint a lawyer on your behalf.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What payment options exist for my business owners policy?</b></h2><p class="body-copy-grey">Hiscox offers a range of payment options, including debit and credit card (Visa, MasterCard and American Express). You can also choose to pay the full amount in one lump sum each year or pay monthly (with no fees added) to help spread your costs.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What deductible options do you offer?</b></h2><p class="body-copy-grey">For our business owners insurance, we offer a range of deductible options. This is the amount you must pay on each claim before the policy pays out. These range from $500 to $10,000 for any building or business equipment related loss. The higher the deductible, the lower your premium, since you take on more of the risk. Get a quote online and select different deductible amounts to see how it affects your premium. Any claim relating to the general liability insurance section of your policy has no deductible.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What happens if I cancel my policy?</b></h2><p class="body-copy-grey">You can cancel your Hiscox policy at any time and we will issue you a refund. However, as soon as you cancel your policy you will no longer have any protection in place for occurrences after your policy is canceled.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Does a Hiscox business owners policy cover work completed temporary staff?</b></h2><p class="body-copy-grey">Yes. Claims arising from actions by full-time and temporary staff are covered.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">Be confident that you've made the right choice. Hiscox gives you 14 days to review the policy. If you are not satisfied for any reason and have not had a claim or loss, you can cancel and receive a full refund.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																									<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="grey-heading-h2"><b>Common Questions About Business Owners Insurance</b></h2><p class="body-copy-grey">Below are some common questions about our business owners policy, also known as business owners insurance. If you have other questions or wish to discuss our policy, please contact one of our licensed advisors either by phone 888-202-3007 (Mon-Fri, 8am-10pm EST), live chat or email us at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a>.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why should I choose a Hiscox business owners insurance policy?</b></h2><p class="body-copy-grey">A Hiscox business owners policy provides you with general liability coverage and also protects your business equipment. You should also consider this policy if you own your office building as this can be covered as well. Even better, you can customize your policy with our policy upgrades, which gives you the right coverage at a great price as you only pay for what you really need. No more, no less.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Why is the Hiscox online process so much simpler?</b></h2><p class="body-copy-grey">We understand that your time is money. We specialize in insurance for professional services businesses, so we understand the risks you face. This lets us simplify our quote-and-buy process and focus on the questions that only apply to your business. Other insurers applications are typically designed to cover all types of industries outside of professional services (e.g., restaurants, shops, mechanics etc.), all of which have very different risks and exposures.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We only ask you questions relevant to your business, making the process quicker and more straightforward than anyone else, whether online, or over the phone. Get started on your <a href="/small-business-insurance/" title="">business owners insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What are the benefits of buying BOP insurance directly from Hiscox?</b></h2><p class="body-copy-grey">When you buy direct, you deal directly with Hiscox. Our licensed small business insurance advisors only focus on professional services businesses like yours. This means you speak to a knowledgeable specialist who can deliver personalized recommendations about the insurance you need in your specific profession.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">With Hiscox you get great value as well as excellent service. Buying direct is a simple process that allows you to get the right coverage at a competitive price.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What types of businesses can you offer business owners insurance to?</b></h2><p class="body-copy-grey">Hiscox BOP insurance covers most types of professional services businesses with 10 employees or less. This includes IT/technology consultants, marketing consultants, and business or management consultants. We’ll also happily insure recruiters, PR specialists, graphic designers, photographers, interior designers and more types of service professionals. If you don’t see your specific business type on this list, contact us to find out if we can provide business owners insurance coverage for your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What do I need to get a BOP insurance quote?</b></h2><p class="body-copy-grey">The quote process is simple whether you go online or call a licensed advisor 888-202-3007 (Mon-Fri, 8am-10pm EST). We’ll ask you for some basic information about your business, previous claims, type of work, etc.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Learn more and start a <a href="/small-business-insurance/" title="">small business insurance quote</a> now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How can I save money on my business owners insurance?</b></h2><p class="body-copy-grey">Buying this type of insurance direct from Hiscox means you enjoy greater overall value. We like to reward businesses that actively manage their risk. Depending on your specific business, you will be asked questions about the types of risk management practices you employ, allowing us to offer rate reductions to those who actively reduce their chances of suffering a loss.</p><p class="body-copy-grey"> </p><p class="body-copy-grey">We also offer customers up to a 5% rate reduction if you buy two or more Hiscox insurance products. Also, by choosing a higher deductible (the initial amount you pay for each loss), you can also lower your premium. It’s only fair, as you’re taking on more of the risk.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>How much does a BOP insurance cost?</b></h2><p class="body-copy-grey">Business owners insurance policy premiums vary depending on the location and size of your business, your occupation, the types of activities you conduct and the risk management practices you employ. However, our minimum premiums start from as little as $400 per year. See this page for more <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">business</a> owners policy quote examples or get a simple, no-obligation quote for your business now.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Is BOP insurance available in my state?</b></h2><p class="body-copy-grey">Hiscox is currently licensed to offer our business owners insurance in various states across the U.S. View our <a href="/small-business-insurance/business-owner-insurance/bop-state/" title="">business owners insurance</a> state insurance page for details. However, if we don’t currently operate in your state we may still be able to help. We have access to specialist insurance brokers across the U.S. who may be able to assist you. Contact one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</p><p class="body-copy-grey"> </p><p class="body-copy-grey">Find out if Hiscox BOP insurance is offered in your state and start a quote today.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What should I do if I need to make a claim?</b></h2><p class="body-copy-grey">You can report a claim 24 hours a day, 7 days a week. Just call us on 866-424-8508 or send us an email at <a href="mailto:contactus@hiscoxusa.com">contactus@hiscoxusa.com</a> and an advisor will get back to you as quickly as possible. We’ll advise you on what to do and, if necessary, appoint a lawyer on your behalf.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What payment options exist for my business owners policy?</b></h2><p class="body-copy-grey">Hiscox offers a range of payment options, including debit and credit card (Visa, MasterCard and American Express). You can also choose to pay the full amount in one lump sum each year or pay monthly (with no fees added) to help spread your costs.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What deductible options do you offer?</b></h2><p class="body-copy-grey">For our business owners insurance, we offer a range of deductible options. This is the amount you must pay on each claim before the policy pays out. These range from $500 to $10,000 for any building or business equipment related loss. The higher the deductible, the lower your premium, since you take on more of the risk. Get a quote online and select different deductible amounts to see how it affects your premium. Any claim relating to the general liability insurance section of your policy has no deductible.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>What happens if I cancel my policy?</b></h2><p class="body-copy-grey">You can cancel your Hiscox policy at any time and we will issue you a refund. However, as soon as you cancel your policy you will no longer have any protection in place for occurrences after your policy is canceled.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Does a Hiscox business owners policy cover work completed temporary staff?</b></h2><p class="body-copy-grey">Yes. Claims arising from actions by full-time and temporary staff are covered.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">Be confident that you've made the right choice. Hiscox gives you 14 days to review the policy. If you are not satisfied for any reason and have not had a claim or loss, you can cancel and receive a full refund.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
			<!--Secondary content section starts here-->
									<!--Secondary content section ends here-->
			<!--eligibility container starts here-->
								<div class="cnt-subsctn white-all-corner">
			<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
			<div class="middle-content vertical-container">
			<div class="elgibltybox-heading"><h2>Get an accurate quote and buy online in minutes</h2></div>
			<usd:checkInclude includeContent="state">
				<usd:checkFormTag formContent="Start"></usd:checkFormTag>
				<usd:questionTag  />
				<usd:actionTag  />
				<usd:trackingTag />
				<usd:checkFormTag formContent="End"></usd:checkFormTag>	
			 </usd:checkInclude>
			  </div>
			  <div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
			</div>
					<!--eligibility container ends here-->
			<!--Centre bottom section starts here-->			
			<usd:checkOccup occupContent="it-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        									        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        									        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        									        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        									        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  										    										    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup>
			<!--Centre bottom section ends here-->
			<!--Tertiary content section starts here-->
																										<usd:checkOccup occupContent="it-consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																									<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
						<!--Tertiary content section ends here-->
		</div>  
		<!--Content section ends here-->
		<!-- Right nav starts here-->
		<div class="rightnav-gap showcntct-num">
			<!--Checklist section starts here-->
			<usd:checkOccup occupContent="it-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        											<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        											<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        											<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        											<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  										    										    										    										            										            																							<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><b><span class="grey-call-to-action-large">888-202-3007</span></b></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				 			            										    										    										    										    																																        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        															<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        															<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        															<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        															<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  										    										    																											<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/get-a-quote-for-your-small-business" title="Get a quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				 			    										            										            										            										    										    										    										    																																        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b><span style=" text-decoration: underline;">Why choose Hiscox?</span></b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for technology businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific technology business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b>Why choose Hiscox?</b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for consulting businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your consulting business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b>Why choose Hiscox?</b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for marketing consultants with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific marketing business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b>Why choose Hiscox?</b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for professional services businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b>Why choose Hiscox?</b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for professional services businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    										    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup>			
			<div class="verLogo-holder"><img src="/resources/images/secured-icon.gif" alt=""  title="Verisign" /></div>
			<!--Checklist section ends here-->
		</div>
		<!-- Right nav ends here-->
		<!-- the closing div for left nav is present in the page templates-->
		<div class="boiler-plate">
    		
	
	
	
	
    			</div>
		<!--Footer starts here -->
		<div class="footer-home">
			<div class="copy-right-home-box">
				
	
	
	
    							
																<p class="body-copy-grey">© Hiscox Inc</p>
										
    	    		
		 					</div>			
							
	
	
	
    							
							<div class="footer-dropdown-box">
                <label for="country">Not in the US?</label>
                <select name="country" id="country" onchange="if(this.options[this.selectedIndex].value!='')openChosenURL(this);">
                <option value="">Select your country</option>
                                                	                		<option value="http://www.hiscox.be">Belgium</option>
                	                		<option value="http://www.hiscox.fr">France</option>
                	                		<option value="http://www.hiscox.de">Germany</option>
                	                		<option value="http://www.hiscox.ie">Ireland</option>
                	                		<option value="http://www.hiscox.nl">Netherlands</option>
                	                		<option value="http://www.hiscox.pt">Portugal</option>
                	                		<option value="http://www.hiscox.es">Spain</option>
                	                		<option value="http://www.hiscox.co.uk">United Kingdom</option>
                	                				</select></div>
						
    	    		
		 								<ul class='footer-links-home-box'>
	
	
	
	
    							
																  <li><p class="body-copy-grey">
	                    <a href="/legal-notices/" title="" target="_blank" >
							Legal notices</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/site-map.html" title="" target="_blank" >
							Site map</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/privacy-policy/" title="" target="_blank" >
							Privacy policy</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/accessibility/" title="" target="_blank" >
							Accessibility</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/terms-of-use/" title="" target="_blank" >
							Terms of use</a></p>
					  </li>
										
    	    		
		 		</ul>		</div>
		<!--Footer ends here -->
		</div>
		<!-- Page container ends here-->
	</div>	<!--main container ends here-->	
<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/landingpageprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
</body>
</html>