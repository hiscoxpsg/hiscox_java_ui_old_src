<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Electronic Data Liability Insurance - Data Loss Liability Protection | Hiscox USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
<meta name="description" content="Do you work with large amounts of client data? Click through to find Hiscox electronic data liability insurance that will protect your small business." />
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print"/>
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body onload="rotatingtext()">
	<!--main container starts here-->
	<div class="main-container">
		<!-- Header starts here-->
		<div class="header">			
		<div class="logo-home"><img src="/resources/images/logo.gif" alt="" title="Logo" /></div>
		<div class="topmenu-container">
			<div class="utitlitylinks-hlder">
				<ul>
	
	
	
    							
							    				    				<li><p class="body-copy-grey">
						<a href="/about-hiscox-insurance/index.html" title="" target="_blank" >
						About Hiscox</a></p>
					</li>
										
    				
							    				    				<li><p class="body-copy-grey">
						<a href="/contact-us/index.html" title="" target="_blank" >
						Contact us</a></p>
					</li>
										
    	    		
		 		</ul>			</div>		
					
											<ul class="topmenu">					
																																																																																																													<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist firstitem">
					<div class="leftcurve"></div><div class="leftmiddle">
						<a href="/index.html" target="_self" class="topmenu-links">Home page </a> 
					</div>
				</li>							 
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																													<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist-sel">
					<a href="/small-business-insurance/" target="_self" class="topmenu-links-twoline">Small business insurance direct</a>																																																																																							            <usd:checkProductEligibility productContent="RequestCallBack,AddEnO,AddGL">
						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																																																							<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/why-choose-hiscox-insurance/" target="_self" class='menulinks'>Why choose Hiscox Insurance?</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/quotes/" target="_self" class='menulinks'>Great value insurance quotes</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/professional-liability-insurance/" target="_self" class='menulinks'>Professional Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddEnO"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/errors-and-omissions-insurance/" target="_self" class='menulinks'>Errors and Omissions Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddGL"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/general-liability-insurance/" target="_self" class='menulinks'>General Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddGL,AddBOP"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/business-owner-insurance/" target="_self" class='menulinks'>Business Owner Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="RequestCallBack"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/workers-compensation-insurance/" target="_self" class='menulinks'>Workers Compensation Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																																																																																																																																																						</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
					</usd:checkProductEligibility>
									</li>	
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																								<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist">
					<a href="/broker/" target="_self" class="topmenu-links-twoline">Business insurance through brokers</a>																																            <usd:checkProductEligibility productContent="RequestCallBack,AddEnO,AddGL">
						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																												<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_property_insurance.htm" target="_self" class='menulinks'>Property insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_professional_insurance.htm" target="_self" class='menulinks'>Professional insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_specialty.htm" target="_self" class='menulinks'>Specialty insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_broker.htm" target="_self" class='menulinks'>Broker center</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																						</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
					</usd:checkProductEligibility>
									</li>	
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																								<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist lastitem">
					<div class="rightmiddle">
						<a href="/insurance-products/index.html" target="_self" class="topmenu-links">Products</a>					</div>
					<div class="rightcurve"></div>
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																	</ul>
								</div>
    		<div class="header-hp-right">
    			
	
	
	
    							
																<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="red-call-to-action-small"><a href="http://www.hiscox.com" target="_blank"><b>Group site</b></a><span class="icon-open-in-new-window"> </span></p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/en/careers.aspx">Careers</a></p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/investors.aspx">Investors</a></p></div>
										
    	    		
		 		    		</div>
		</div>
		<!-- Header ends here-->
		<!-- Page container starts here-->
		<div class="page-container">
		<!--Breadcrumb starts here-->
					<div class="breadcrumb">
				
	<div class="brdcrmb-sub">
			                    	                    	                    					<div class="breadcrumb-link"><a href="/index.html">Home page</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/">Small business insurance direct</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/business-owner-insurance/">Business Owner Insurance</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-current"><span>Electronic Data Liability Insurance</span></div>
			</div>
									<usd:checkEligibility eligibleContent="stateVariant,primarybusiness">
					<div class="occupation-details">
						<span class = "float-right"><object>
						<div class="occupation-info"><span>State:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="stateVariant" /></a></div><div class="breadcrumb-separator"></div>
						<div class="occupation-info"><span>Primary type of business:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="primarybusiness" /></a></div>
						</object></span>
					</div>
					</usd:checkEligibility>
							</div>
		<!--Breadcrumb ends here-->
		<!--Left nav starts here-->
		<div class="leftnav-gap clear-both">
						
							        	    		    									<div class="leftmenu-main">
																																																					<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="all">							<div class="leftmenu-mainitem">
					<div class="mainitem-topleft" ></div><div class="mainitem-topmiddle" ></div><div class="mainitem-topright"></div>
						<div class="mainitem-middle">
							<a href="/small-business-insurance/" target="_self" class="main-link">Small business insurance direct</a>
						</div>
					<div class="mainitem-btmleft"></div><div class="mainitem-btmmiddle" ></div><div class="mainitem-btmright"></div>
				</div>
					</usd:checkProductEligibility>	</usd:checkOccup>													<div class="leftmenu">
					<div class="leftmenu-top"><div class="leftmenu-topleft"></div><div class="leftmenu-topmiddle"></div><div class="leftmenu-topright"></div></div>
						<div class="leftmenu-middle">
							<ul>
																	        																		        																		        																		        																		        																		            	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    			    				    				    					    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem-sel"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/" target="_self" class="leftmenulinks-sel">Business Owner Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>    					    													  			    										  			    										  			    										  			    										  			    										  			    										  			    										  			    										  			    										  																																																												<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="last-item">
					<div class="leftmenu-listitem-sel2"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/data-loss-protection./" target="_self" class="leftmenulinks-sel">Electronic Data Liability Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>										      				    																								        																		        																		        																		        																		        																		        																		        																		        																		        																</ul>
						</div>
					<div class="leftmenu-bottom"><div class="leftmenu-btmleft"></div><div class="leftmenu-btmmiddle"></div><div class="leftmenu-btmright"></div></div>
				</div>			
			</div>
			    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    	    					
				    		    				    	    		    				    	    		    				    	    		   			    		    		    		    		    		    		    								<!--Left nav starts here and the div starts in the global template-->
					<usd:checkOccup occupContent="it-consulting" > 
					                                        								    													<div class="section-container grey-toplft" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3">Insurance by occupation</h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                        								    													<div class="section-container grey-toplft" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3">Insurance by occupation</h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                        								    													<div class="section-container grey-toplft" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3">Insurance by occupation</h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                        								    													<div class="section-container grey-toplft" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3">Insurance by occupation</h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                        								    													<div class="section-container grey-toplft" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3">Insurance by occupation</h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup> 
		</div>
		<!--Left nav ends here-->
		<!--Content section starts here-->
				<div class="cnt-sctn no-bg">			
																																																<usd:checkOccup occupContent="default-occupation-not-known" > 
        						<div class="cnt-subsctn">
        							<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
        							<div class="middle-content">
        								<div class="page-heading2">       									
        									<h1 class="page-heading-h1">Electronic Data Loss Protection Insurance - Coverage Details</h1>        								</div>
        								<div class="clear"></div>
        							</div>
        							<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
        						</div>
    						</usd:checkOccup>
																																																<usd:checkOccup occupContent="default-occupation-known" > 
        						<div class="cnt-subsctn">
        							<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
        							<div class="middle-content">
        								<div class="page-heading2">       									
        									<h1 class="page-heading-h1">Electronic Data Loss Protection Insurance - Coverage Details</h1>        								</div>
        								<div class="clear"></div>
        							</div>
        							<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
        						</div>
        					</usd:checkOccup>
																																																<usd:checkOccup occupContent="marketing-pr-consulting" > 
        						<div class="cnt-subsctn">
        							<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
        							<div class="middle-content">
        								<div class="page-heading2">       									
        									<h1 class="page-heading-h1">Electronic Data Loss Protection Insurance - Coverage Details</h1>        								</div>
        								<div class="clear"></div>
        							</div>
        							<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
        						</div> 
							</usd:checkOccup>	
																																																<usd:checkOccup occupContent="consulting" > 
        						<div class="cnt-subsctn">
        							<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
        							<div class="middle-content">
        								<div class="page-heading2">       									
        									<h1 class="page-heading-h1">Electronic Data Loss Protection Insurance - Coverage Details</h1>        								</div>
        								<div class="clear"></div>
        							</div>
        							<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
        						</div> 
    						</usd:checkOccup>
																																																<usd:checkOccup occupContent="it-consulting" > 
        						<div class="cnt-subsctn">
        							<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
        							<div class="middle-content">
        								<div class="page-heading2">
        									<h1 class="page-heading-h1">Electronic Data Loss Protection Insurance - Coverage Details</h1>        								</div>
        								<div class="clear"></div>
        							</div>
        							<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
        						</div> 
    						</usd:checkOccup>
    																																																																											<usd:checkOccup occupContent="default-occupation-not-known" >
							<div class="cnt-subsctn white-corner-btm">
								<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
								<div class="middle-content">
									<div class="middlecnt-container">								 
										<h2 class="grey-heading-h2"><b>Your Business Owners Policy – electronic data loss protection insurance does cover:</b></h2><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Lost or damaged electronic data</b></h2><p class="body-copy-grey">Data loss protection insurance is important. You never know when you may lose all your important data, which is why you must be protected. Our standard Business Owners Policy coverage includes up to $10,000 of coverage to replace or restore your electronic data that is lost as a result of an insured loss e.g., as a result of a fire which destroys your computer system or by a virus. With this upgrade, we increase your electronic data loss limit to $25,000. This coverage is recommended for professional services businesses as we understand that your data is the life blood of your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Interruption of computer operations</b></h2><p class="body-copy-grey">Data loss can severely impact a business. That’s why our standard Business Owners Policy coverage includes up to $10,000 of coverage for the actual income your business loses and extra expenses you incur if you cannot operate your business due to the data loss. By purchasing this upgrade, we also increase this limit to $25,000.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>E-commerce coverage</b></h2><p class="body-copy-grey">This coverage doesn’t exist in our standard Business Owners Policy. However, our electronic data loss protection insurance upgrade also includes up to $25,000 in coverage for loss of electronic data used in e-commerce activities and the lost business income associated with that loss. This coverage is recommended for businesses that trade over the internet and through other means of e-commerce.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Hiscox – data loss insurance does not cover:</b></h2><p class="body-copy-grey">We want you to understand our electronic data loss liability coverage. That’s why we’re completely open about what we do and don’t cover. The following is a list of key items we don’t cover.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your liability due to data loss (not a third party)</b></h2><p class="body-copy-grey">The Hiscox electronic data loss liability upgrade package only covers your losses due to lost electronic data. It does not provide any coverage for any liability you may have to a third-party due to such losses.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your mistakes</b></h2><p class="body-copy-grey">We won’t cover any losses due to your mistakes in processing electronic data, mistakes made in the design, implementation, or support of your computer systems or networks, unless those mistakes result in a fire that subsequently destroys the data.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Employee actions</b></h2><p class="body-copy-grey">Hiscox won’t cover any losses to electronic data if those losses are a result of your employees’ actions, whether intentional or not.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Business Owners Policy Claims Examples:</b></h2><h2 class="grey-heading-h2"><b>Fire</b></h2><p class="body-copy-grey">A small fire in your building destroys or damages your server. Your Hiscox electronic data loss protection insurance will cover the cost (up to your limit) to restore that data and lost income if your business can’t trade as a result of the insured loss.</p><p class="body-copy-grey"> </p><h3 class="box-heading-h3"><b>Get your simple <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">online insurance quote</a> now. Or call one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</b></h3>									</div>
								</div>
								<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
							</div>
						</usd:checkOccup>
																																							<usd:checkOccup occupContent="default-occupation-known" > 
							<div class="cnt-subsctn white-corner-btm">
								<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
								<div class="middle-content">
									<div class="middlecnt-container">									
										<h2 class="grey-heading-h2"><b>Your Business Owners Policy – electronic data loss protection insurance does cover:</b></h2><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Lost or damaged electronic data</b></h2><p class="body-copy-grey">Data loss protection insurance is important. You never know when you may lose all your important data, which is why you must be protected. Our standard Business Owners Policy coverage includes up to $10,000 of coverage to replace or restore your electronic data that is lost as a result of an insured loss e.g., as a result of a fire which destroys your computer system or by a virus. With this upgrade, we increase your electronic data loss limit to $25,000. This coverage is recommended for professional services businesses as we understand that your data is the life blood of your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Interruption of computer operations</b></h2><p class="body-copy-grey">Data loss can severely impact a business. That’s why our standard Business Owners Policy coverage includes up to $10,000 of coverage for the actual income your business loses and extra expenses you incur if you cannot operate your business due to the data loss. By purchasing this upgrade, we also increase this limit to $25,000.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>E-commerce coverage</b></h2><p class="body-copy-grey">This coverage doesn’t exist in our standard Business Owners Policy. However, our electronic data loss protection insurance upgrade also includes up to $25,000 in coverage for loss of electronic data used in e-commerce activities and the lost business income associated with that loss. This coverage is recommended for businesses that trade over the internet and through other means of e-commerce.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Hiscox – data loss insurance does not cover:</b></h2><p class="body-copy-grey">We want you to understand our electronic data loss liability coverage. That’s why we’re completely open about what we do and don’t cover. The following is a list of key items we don’t cover.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your liability due to data loss (not a third party)</b></h2><p class="body-copy-grey">The Hiscox electronic data loss liability upgrade package only covers your losses due to lost electronic data. It does not provide any coverage for any liability you may have to a third-party due to such losses.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your mistakes</b></h2><p class="body-copy-grey">We won’t cover any losses due to your mistakes in processing electronic data, mistakes made in the design, implementation, or support of your computer systems or networks, unless those mistakes result in a fire that subsequently destroys the data.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Employee actions</b></h2><p class="body-copy-grey">Hiscox won’t cover any losses to electronic data if those losses are a result of your employees’ actions, whether intentional or not.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Business Owners Policy Claims Examples:</b></h2><h2 class="grey-heading-h2"><b>Fire</b></h2><p class="body-copy-grey">A small fire in your building destroys or damages your server. Your Hiscox electronic data loss protection insurance will cover the cost (up to your limit) to restore that data and lost income if your business can’t trade as a result of the insured loss.</p><p class="body-copy-grey"> </p><h3 class="box-heading-h3"><b>Get your simple <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">online insurance quote</a> now. Or call one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</b></h3>									</div>
								</div>
								<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
								</div>
						</usd:checkOccup>
																																							<usd:checkOccup occupContent="marketing-pr-consulting" >
							<div class="cnt-subsctn white-corner-btm">
								<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
								<div class="middle-content">
									<div class="middlecnt-container">								
										<h2 class="grey-heading-h2"><b>Your Business Owners Policy – electronic data loss protection insurance does cover:</b></h2><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Lost or damaged electronic data</b></h2><p class="body-copy-grey">Data loss protection insurance is important. You never know when you may lose all your important data, which is why you must be protected. Our standard Business Owners Policy coverage includes up to $10,000 of coverage to replace or restore your electronic data that is lost as a result of an insured loss e.g., as a result of a fire which destroys your computer system or by a virus. With this upgrade, we increase your electronic data loss limit to $25,000. This coverage is recommended for professional services businesses as we understand that your data is the life blood of your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Interruption of computer operations</b></h2><p class="body-copy-grey">Data loss can severely impact a business. That’s why our standard Business Owners Policy coverage includes up to $10,000 of coverage for the actual income your business loses and extra expenses you incur if you cannot operate your business due to the data loss. By purchasing this upgrade, we also increase this limit to $25,000.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>E-commerce coverage</b></h2><p class="body-copy-grey">This coverage doesn’t exist in our standard Business Owners Policy. However, our electronic data loss protection insurance upgrade also includes up to $25,000 in coverage for loss of electronic data used in e-commerce activities and the lost business income associated with that loss. This coverage is recommended for businesses that trade over the internet and through other means of e-commerce.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Hiscox – data loss insurance does not cover:</b></h2><p class="body-copy-grey">We want you to understand our electronic data loss liability coverage. That’s why we’re completely open about what we do and don’t cover. The following is a list of key items we don’t cover.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your liability due to data loss (not a third party)</b></h2><p class="body-copy-grey">The Hiscox electronic data loss liability upgrade package only covers your losses due to lost electronic data. It does not provide any coverage for any liability you may have to a third-party due to such losses.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your mistakes</b></h2><p class="body-copy-grey">We won’t cover any losses due to your mistakes in processing electronic data, mistakes made in the design, implementation, or support of your computer systems or networks, unless those mistakes result in a fire that subsequently destroys the data.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Employee actions</b></h2><p class="body-copy-grey">Hiscox won’t cover any losses to electronic data if those losses are a result of your employees’ actions, whether intentional or not.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Business Owners Policy Claims Examples:</b></h2><h2 class="grey-heading-h2"><b>Fire</b></h2><p class="body-copy-grey">A small fire in your building destroys or damages your server. Your Hiscox electronic data loss protection insurance will cover the cost (up to your limit) to restore that data and lost income if your business can’t trade as a result of the insured loss.</p><p class="body-copy-grey"> </p><h3 class="box-heading-h3"><b>Get your simple <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">online insurance quote</a> now. Or call one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</b></h3>									</div>
								</div>
								<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
							</div>
						</usd:checkOccup>
																																							<usd:checkOccup occupContent="consulting" > 
							<div class="cnt-subsctn white-corner-btm">
								<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
								<div class="middle-content">
									<div class="middlecnt-container">								
										<h2 class="grey-heading-h2"><b>Your Business Owners Policy – electronic data loss protection insurance does cover:</b></h2><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Lost or damaged electronic data</b></h2><p class="body-copy-grey">Data loss protection insurance is important. You never know when you may lose all your important data, which is why you must be protected. Our standard Business Owners Policy coverage includes up to $10,000 of coverage to replace or restore your electronic data that is lost as a result of an insured loss e.g., as a result of a fire which destroys your computer system or by a virus. With this upgrade, we increase your electronic data loss limit to $25,000. This coverage is recommended for professional services businesses as we understand that your data is the life blood of your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Interruption of computer operations</b></h2><p class="body-copy-grey">Data loss can severely impact a business. That’s why our standard Business Owners Policy coverage includes up to $10,000 of coverage for the actual income your business loses and extra expenses you incur if you cannot operate your business due to the data loss. By purchasing this upgrade, we also increase this limit to $25,000.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>E-commerce coverage</b></h2><p class="body-copy-grey">This coverage doesn’t exist in our standard Business Owners Policy. However, our electronic data loss protection insurance upgrade also includes up to $25,000 in coverage for loss of electronic data used in e-commerce activities and the lost business income associated with that loss. This coverage is recommended for businesses that trade over the internet and through other means of e-commerce.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Hiscox – data loss insurance does not cover:</b></h2><p class="body-copy-grey">We want you to understand our electronic data loss liability coverage. That’s why we’re completely open about what we do and don’t cover. The following is a list of key items we don’t cover.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your liability due to data loss (not a third party)</b></h2><p class="body-copy-grey">The Hiscox electronic data loss liability upgrade package only covers your losses due to lost electronic data. It does not provide any coverage for any liability you may have to a third-party due to such losses.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your mistakes</b></h2><p class="body-copy-grey">We won’t cover any losses due to your mistakes in processing electronic data, mistakes made in the design, implementation, or support of your computer systems or networks, unless those mistakes result in a fire that subsequently destroys the data.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Employee actions</b></h2><p class="body-copy-grey">Hiscox won’t cover any losses to electronic data if those losses are a result of your employees’ actions, whether intentional or not.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Business Owners Policy Claims Examples:</b></h2><h2 class="grey-heading-h2"><b>Fire</b></h2><p class="body-copy-grey">A small fire in your building destroys or damages your server. Your Hiscox electronic data loss protection insurance will cover the cost (up to your limit) to restore that data and lost income if your business can’t trade as a result of the insured loss.</p><p class="body-copy-grey"> </p><h3 class="box-heading-h3"><b>Get your simple <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">online insurance quote</a> now. Or call one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</b></h3>									</div>
								</div>
								<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
							</div>
						</usd:checkOccup>
																																							<usd:checkOccup occupContent="it-consulting" >
        					<div class="cnt-subsctn white-corner-btm">
        						<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
        						<div class="middle-content">
        							<div class="middlecnt-container">								
        								<h2 class="grey-heading-h2"><b>Your Business Owners Policy – electronic data loss protection insurance does cover:</b></h2><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Lost or damaged electronic data</b></h2><p class="body-copy-grey">Data loss protection insurance is important. You never know when you may lose all your important data, which is why you must be protected. Our standard Business Owners Policy coverage includes up to $10,000 of coverage to replace or restore your electronic data that is lost as a result of an insured loss e.g., as a result of a fire which destroys your computer system or by a virus. With this upgrade, we increase your electronic data loss limit to $25,000. This coverage is recommended for professional services businesses as we understand that your data is the life blood of your business.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Interruption of computer operations</b></h2><p class="body-copy-grey">Data loss can severely impact a business. That’s why our standard Business Owners Policy coverage includes up to $10,000 of coverage for the actual income your business loses and extra expenses you incur if you cannot operate your business due to the data loss. By purchasing this upgrade, we also increase this limit to $25,000.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>E-commerce coverage</b></h2><p class="body-copy-grey">This coverage doesn’t exist in our standard Business Owners Policy. However, our electronic data loss protection insurance upgrade also includes up to $25,000 in coverage for loss of electronic data used in e-commerce activities and the lost business income associated with that loss. This coverage is recommended for businesses that trade over the internet and through other means of e-commerce.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Hiscox – data loss insurance does not cover:</b></h2><p class="body-copy-grey">We want you to understand our electronic data loss liability coverage. That’s why we’re completely open about what we do and don’t cover. The following is a list of key items we don’t cover.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your liability due to data loss (not a third party)</b></h2><p class="body-copy-grey">The Hiscox electronic data loss liability upgrade package only covers your losses due to lost electronic data. It does not provide any coverage for any liability you may have to a third-party due to such losses.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Your mistakes</b></h2><p class="body-copy-grey">We won’t cover any losses due to your mistakes in processing electronic data, mistakes made in the design, implementation, or support of your computer systems or networks, unless those mistakes result in a fire that subsequently destroys the data.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Employee actions</b></h2><p class="body-copy-grey">Hiscox won’t cover any losses to electronic data if those losses are a result of your employees’ actions, whether intentional or not.</p><p class="body-copy-grey"> </p><h2 class="grey-heading-h2"><b>Business Owners Policy Claims Examples:</b></h2><h2 class="grey-heading-h2"><b>Fire</b></h2><p class="body-copy-grey">A small fire in your building destroys or damages your server. Your Hiscox electronic data loss protection insurance will cover the cost (up to your limit) to restore that data and lost income if your business can’t trade as a result of the insured loss.</p><p class="body-copy-grey"> </p><h3 class="box-heading-h3"><b>Get your simple <a href="/small-business-insurance/business-owner-insurance/bop-quotes/" title="">online insurance quote</a> now. Or call one of our licensed advisors at 888-202-3007 (Mon-Fri, 8am-10pm EST).</b></h3>        							</div>
        						</div>
        						<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
        					</div>
    					</usd:checkOccup>
																														<!--Secondary content section starts here-->
												<!--Secondary content section ends here-->
			<!--eligibility container starts here-->
								<div class="cnt-subsctn white-all-corner">
			<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
			<div class="middle-content vertical-container">
			<div class="elgibltybox-heading"><h2>Get an accurate quote and buy online in minutes</h2></div>
			<usd:checkInclude includeContent="state">
				<usd:checkFormTag formContent="Start"></usd:checkFormTag>
				<usd:questionTag  />
				<usd:actionTag  />
				<usd:trackingTag />
				<usd:checkFormTag formContent="End"></usd:checkFormTag>	
			 </usd:checkInclude>
			  </div>
			  <div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
			</div>
					<!--eligibility container ends here-->
			<!--Centre bottom section starts here-->			
			<usd:checkOccup occupContent="it-consulting" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<!--Centre bottom section ends here-->
			<!--Tertiary content section starts here-->
																																																																										<usd:checkOccup occupContent="default-occupation-not-known" > 
										<div class="cnt-subsctn white-all-corner">
											<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
											<div class="middle-content">
												<div class="middlecnt-container">	
												<p class="body-copy-grey">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverage is subject to underwriting and may not be available in all states.</p>												</div>
											</div>
											<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
										</div> 
										</usd:checkOccup>
																																																																												<usd:checkOccup occupContent="default-occupation-known" > 
										<div class="cnt-subsctn white-all-corner">
											<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
											<div class="middle-content">
												<div class="middlecnt-container">
												<p class="body-copy-grey">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverage is subject to underwriting and may not be available in all states.</p>												</div>
											</div>
											<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
										</div> 
										</usd:checkOccup>
																																																																												<usd:checkOccup occupContent="marketing-pr-consulting" >
										<div class="cnt-subsctn white-all-corner">
											<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
											<div class="middle-content">
												<div class="middlecnt-container">
												<p class="body-copy-grey">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverage is subject to underwriting and may not be available in all states.</p>												</div>
											</div>
											<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
										</div> 
										</usd:checkOccup>
																																																																											<usd:checkOccup occupContent="consulting" > 
										<div class="cnt-subsctn white-all-corner">
											<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
											<div class="middle-content">
												<div class="middlecnt-container">	
												<p class="body-copy-grey">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverage is subject to underwriting and may not be available in all states.</p>												</div>
											</div>
											<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
										</div> 
										</usd:checkOccup>
																																																																												<usd:checkOccup occupContent="it-consulting" >
										<div class="cnt-subsctn white-all-corner">
											<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
											<div class="middle-content">
												<div class="middlecnt-container">									
												<p class="body-copy-grey">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverage is subject to underwriting and may not be available in all states.</p>												</div>
											</div>
											<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
										</div> 
										</usd:checkOccup>
																																																																	<!--Tertiary content section ends here-->
		</div>  
		<!--Content section ends here-->
		<!-- Right nav starts here-->
		<div class="rightnav-gap">
			<!--Checklist section starts here-->
			<usd:checkOccup occupContent="it-consulting" > 
					                                        								    								    								    								            								            																			<div class="section-container topright-container-small" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				 			            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                        								    								    								    								            								            																			<div class="section-container topright-container-small" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				 			            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                        								    								    								    								            								            																			<div class="section-container topright-container-small" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				 			            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                        								    								    								    								            								            																			<div class="section-container topright-container-small" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				 			            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                        								    								    								    								            								            																			<div class="section-container topright-container-small" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Call our licensed advisors" title="Call our licensed advisors" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-mouse">Chat online now</span></a></p>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				 			            								    								    								    								    																										        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                        								    																	<div class="section-container smallcontainer" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3>		<div class="horz-line"></div> 
		<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for technology businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific technology business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul></div></div></div></div>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                        								    																	<div class="section-container smallcontainer" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3>		<div class="horz-line"></div> 
		<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for consulting businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your consulting business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul></div>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                        																	<div class="section-container smallcontainer" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3><img id="red-line-style" src="/shared-images/red-line-style.png" alt="red line" title="red line" border="0" height="2" width="166" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for marketing consultants with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific marketing business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                        								    																	<div class="section-container smallcontainer" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3>		<div class="horz-line"></div> 
		<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for professional services businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul></div>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                        								    																	<div class="section-container smallcontainer" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Why choose Hiscox?</b></h3>		<div class="horz-line"></div> 
		<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for professional services businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul></div></div>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                        								    								    								    								            								            								            								    								    								    								    																										        			</usd:checkOccup>			
			<div class="verLogo-holder"><img src="/resources/images/secured-icon.gif" alt=""  title="Verisign" /></div>
			<!--Checklist section ends here-->
		</div>
		<!-- Right nav ends here-->
		<!-- the closing div for left nav is present in the page templates-->
		<div class="boiler-plate">
    		
	
	
	
	
    			</div>
		<!--Footer starts here -->
		<div class="footer-home">
			<div class="copy-right-home-box">
				
	
	
	
    							
																<p class="body-copy-grey">© Hiscox Inc</p>
										
    	    		
		 					</div>			
							
	
	
	
    							
							<div class="footer-dropdown-box">
                <label for="country">Not in the US?</label>
                <select name="country" id="country" onchange="if(this.options[this.selectedIndex].value!='')openChosenURL(this);">
                <option value="">Select your country</option>
                                                	                		<option value="http://www.hiscox.be">Belgium</option>
                	                		<option value="http://www.hiscox.fr">France</option>
                	                		<option value="http://www.hiscox.de">Germany</option>
                	                		<option value="http://www.hiscox.ie">Ireland</option>
                	                		<option value="http://www.hiscox.nl">Netherlands</option>
                	                		<option value="http://www.hiscox.pt">Portugal</option>
                	                		<option value="http://www.hiscox.es">Spain</option>
                	                		<option value="http://www.hiscox.co.uk">United Kingdom</option>
                	                				</select></div>
						
    	    		
		 								<ul class='footer-links-home-box'>
	
	
	
	
    							
																  <li><p class="body-copy-grey-small">
	                    <a href="/legal-notices/index.html" title="" target="_blank" >
							Legal notices</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey-small">
	                    <a href="/site-map.html" title="" target="_blank" >
							Site map</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey-small">
	                    <a href="/privacy-policy/index.html" title="" target="_blank" >
							Privacy policy</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey-small">
	                    <a href="/accessibility/index.html" title="" target="_blank" >
							Accessibility</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey-small">
	                    <a href="/terms-of-use/index.html" title="" target="_blank" >
							Terms of use</a></p>
					  </li>
										
    	    		
		 		</ul>		</div>
		<!--Footer ends here -->
		</div>
		<!-- Page container ends here-->
	</div>	<!--main container ends here-->	
<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/landingpageprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
</body>
</html>