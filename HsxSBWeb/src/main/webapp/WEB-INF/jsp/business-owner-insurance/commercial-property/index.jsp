<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Commercial Property Insurance - Equipment and Property Insurance for Small Business | Hiscox  USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
<meta name="description" content="Visit Hiscox to learn more about commercial property insurance and business equipment insurance.  Quote and buy now." />
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print"/>
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body onload="rotatingtext()">
	<!--main container starts here-->
	<div class="main-container">
		<!-- Header starts here-->
		<div class="header">			
		<div class="logo-home">
	
	
	
    							
			                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		<div class="topmenu-container">
			<div class="utitlitylinks-hlder">
				<ul>
	
	
	
    							
							    				    				<li><p class="body-copy-grey">
						<a href="/about-hiscox-insurance/" title="" target="_blank" >
						About Hiscox</a></p>
					</li>
										
    				
							    				    				<li><p class="body-copy-grey">
						<a href="/contact-us/" title="" target="_blank" >
						Contact us</a></p>
					</li>
										
    	    		
		 		</ul>			</div>		
					
											<ul class="topmenu">					
																																																																																																									<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist firstitem">
					<div class="leftcurve"></div><div class="leftmiddle">
						<a href="/" target="_self" class="topmenu-links">Home page </a> 
					</div>
				</li>							 
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																													<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist-sel">
					<a href="/small-business-insurance/" target="_self" class="topmenu-links-twoline">Small business insurance direct</a>																																																																																												            <usd:checkProductEligibility productContent="RequestCallBack,AddEnO,AddGL">
						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																																																							<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/why-choose-hiscox-insurance/" target="_self" class='menulinks'>Why choose Hiscox Insurance?</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/quotes/" target="_self" class='menulinks'>Great value insurance quotes</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/professional-liability-insurance/" target="_self" class='menulinks'>Professional Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddEnO"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/errors-and-omissions-insurance/" target="_self" class='menulinks'>Errors and Omissions Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddGL"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/general-liability-insurance/" target="_self" class='menulinks'>General Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddGL,AddBOP"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/business-owner-insurance/" target="_self" class='menulinks'>Business Owner Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="RequestCallBack"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/workers-compensation-insurance/" target="_self" class='menulinks'>Workers' Compensation Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																																																																																																																																																																												</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
					</usd:checkProductEligibility>
									</li>	
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																								<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist">
					<a href="/broker/" target="_self" class="topmenu-links-twoline">Business insurance through brokers</a>																																            <usd:checkProductEligibility productContent="RequestCallBack,AddEnO,AddGL">
						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																												<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_professional_insurance.htm" target="_self" class='menulinks'>Professional insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_property_insurance.htm" target="_self" class='menulinks'>Property insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_specialty.htm" target="_self" class='menulinks'>Specialty insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_broker.htm" target="_self" class='menulinks'>Broker center</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																						</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
					</usd:checkProductEligibility>
									</li>	
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																								<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist lastitem">
					<div class="rightmiddle">
						<a href="/insurance-products/" target="_self" class="topmenu-links">Products</a>					</div>
					<div class="rightcurve"></div>
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																														</ul>
								</div>
    		<div class="header-hp-right">
    			
	
	
	
    							
																<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="red-call-to-action-small"><a href="http://www.hiscox.com"><span class="icon-open-in-new-window"><b>Group site</b></span></a> </p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/en/careers.aspx">Careers</a></p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/investors.aspx">Investors</a></p></div>
										
    	    		
		 		    		</div>
		</div>
		<!-- Header ends here-->
		<!-- Page container starts here-->
		<div class="page-container">
		<!--Breadcrumb starts here-->
					<div class="breadcrumb">
				
	<div class="brdcrmb-sub">
			                    	                    	                    					<div class="breadcrumb-link"><a href="/">Home page</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/">Small business insurance direct</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/business-owner-insurance/">Business Owner Insurance</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-current"><span>Commercial Property Insurance</span></div>
			</div>
									<usd:checkEligibility eligibleContent="stateVariant,primarybusiness">
					<div class="occupation-details">
						<span class = "float-right"><object>
						<div class="occupation-info"><span>State:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="stateVariant" /></a></div>
						<div class="occupation-info"><span>Type of business:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="primarybusiness" /></a></div>
						</object></span>
					</div>
					</usd:checkEligibility>
							</div>
		<!--Breadcrumb ends here-->
		<!--Left nav starts here-->
		<div class="leftnav-gap clear-both">
						
							        	    		    									<div class="leftmenu-main">
																																																					<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="all">							<div class="leftmenu-mainitem">
					<div class="mainitem-topleft" ></div><div class="mainitem-topmiddle" ></div><div class="mainitem-topright"></div>
						<div class="mainitem-middle">
							<a href="/small-business-insurance/" target="_self" class="main-link">Small business insurance direct</a>
						</div>
					<div class="mainitem-btmleft"></div><div class="mainitem-btmmiddle" ></div><div class="mainitem-btmright"></div>
				</div>
					</usd:checkProductEligibility>	</usd:checkOccup>													<div class="leftmenu">
					<div class="leftmenu-top"><div class="leftmenu-topleft"></div><div class="leftmenu-topmiddle"></div><div class="leftmenu-topright"></div></div>
						<div class="leftmenu-middle">
							<ul>
																	        																		        																		        																		        																		        																		            	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    																																																					<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem-sel"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/" target="_self" class="leftmenulinks-sel">Business Owner Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>																		  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/bop-quotes/" target="_self" class="leftmenulinks-nopad">Business Owner Insurance Quotes</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/office-insurance/" target="_self" class="leftmenulinks-nopad">Office Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																																																												<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="last-item">
					<div class="leftmenu-listitem-sel2"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/commercial-property/" target="_self" class="leftmenulinks-sel">Commercial Property Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>																	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/gl-insurance-and-bop/" target="_self" class="leftmenulinks-nopad">BOP Insurance and General Liability Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/non-owned-auto-liability/" target="_self" class="leftmenulinks-nopad">Non-Owned Auto Liability Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/commercial-crime/" target="_self" class="leftmenulinks-nopad">Commercial Crime Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/data-loss-insurance/" target="_self" class="leftmenulinks-nopad">Electronic Data Loss Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/bop-faq/" target="_self" class="leftmenulinks-nopad">BOP Insurance - FAQs</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    											  																                    																																									<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="AddGL,AddBOP">							<li class="last-item">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/business-owner-insurance/bop-state/" target="_self" class="leftmenulinks-nopad">Business Owner Insurance in my state?</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
					</usd:checkProductEligibility>	</usd:checkOccup>                                    	         	    				   																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																</ul>
						</div>
					<div class="leftmenu-bottom"><div class="leftmenu-btmleft"></div><div class="leftmenu-btmmiddle"></div><div class="leftmenu-btmright"></div></div>
				</div>
			</div>
			    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    	    					
				    		    				    	    		    				    	    		    				    	    		   			    		    		    		    		    		    		    								<!--Left nav starts here and the div starts in the global template-->
					<usd:checkOccup occupContent="it-consulting" > 
					                                  															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  																							<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    		    															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
					<div class="horz-line"></div> 
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  															<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><b>Insurance by profession</b></p><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey"><a href="/small-business-insurance/it-insurance/" title="">IT consultants &gt;</a><br /><a href="/small-business-insurance/marketing-consultant-insurance/" title="">Marketing consultants &gt;</a><br /><a href="/small-business-insurance/business-consultant-insurance/" title="">Business consultants &gt;</a><br /><a href="/small-business-insurance/home-based-business-insurance/" title="">Home based businesses &gt;</a><br /><a href="/small-business-insurance/professional-business-insurance/" title="">Other professions &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    										    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  										    										    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup> 
		</div>
		<!--Left nav ends here-->
		<!--Content section starts here-->
				<div class="cnt-sctn no-bg">			
																													<usd:checkOccup occupContent="it-consulting" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">Commercial Property Insurance</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																								<usd:checkOccup occupContent="consulting" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">Commercial Property Insurance</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																								<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">Commercial Property Insurance</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																								<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">Commercial Property Insurance</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																										<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
    	<h1 class="page-heading-h1">Commercial Property Insurance</h1>    	</div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																														<usd:checkOccup occupContent="it-consulting" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey"> </p><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td width="45%"><h2 class="grey-heading-h2">        <b>What is covered?</b></h2></td><td width="5%"> </td><td width="45%"><h2 class="grey-heading-h2"><b>What is not covered?</b></h2></td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Business equipment</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Earthquakes, floods and volcanoes</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Tenant improvements</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Dishonesty</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Coverage un and away from the office</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Utilities</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accidental damage</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Errors or omissions</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>No deductions for depreciation</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Computer testing, installation or repair</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Personal effects</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Lost business income and extra expense</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Electronic data</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Interruption of computer operations</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Valuable papers and records</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Forgery or alteration</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accounts receivable</li></ul></td><td> </td><td> </td></tr></tbody></table><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Hiscox business property and equipment insurance does cover:</b></h2><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business equipment</b></p><p class="body-copy-grey">Our commercial property insurance provides coverage for loss or damage to your business equipment (e.g. computers, printers, servers, office furniture).</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Tenant improvements</b></p><p class="body-copy-grey">With business property insurance, we cover improvements you’ve made to your office space that you can’t legally remove.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Coverage in and away from the office</b></p><p class="body-copy-grey">You’re covered whether you stay put or on the go. Your business equipment is insured when physically in your office. We also provide up to $10,000 in coverage for equipment you take away from your office such as laptops, projectors or other insured equipment.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accidental damage</b></p><p class="body-copy-grey">Not only do we provide insurance for equipment against risks like fire, we also cover losses due to accidental damage.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>No deductions for depreciation</b></p><p class="body-copy-grey">Most property insurance for small business under this policy is valued at its replacement cost. There is no deduction for depreciation so you can replace damaged items for equipment of like kind and quality.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Personal effects</b></p><p class="body-copy-grey">Your Hiscox commercial property insurance, provides up to $2,500 of coverage, per premise, for personal items that are damaged in an insured loss such as a large fire.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Lost business income and extra expense</b></p><p class="body-copy-grey">Hiscox business equipment insurance will pay the actual income your business loses and extra expenses you incur for up to 12 months if you cannot operate your business as a result of damage to your insured equipment. This includes up to 60 days of the ongoing payroll expenses of your employees.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Electronic data</b></p><p class="body-copy-grey">We provide up to $10,000 of business property insurance coverage to replace or restore your electronic data that is lost as a result of an insured loss. This includes losses caused by a virus or malicious code. If you work with large amounts of valuable data, this limit can be increased by purchasing our <a href="/small-business-insurance/business-owner-insurance/data-loss-insurance/" title="">electronic data loss insurance</a> upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Interruption of computer operations</b></p><p class="body-copy-grey">If you suffer an electronic data loss as above, we will also cover up to $10,000 for the actual income your business loses and extra expenses you incur if you cannot operate your business as a result of that data loss. If you work with large amounts of valuable data this limit can be increased by purchasing our electronic data loss upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Valuable papers and records</b></p><p class="body-copy-grey">Our commercial property insurance will pay for loss or damage to your business’s valuable papers and records at your business premises up to $10,000 and up to $5,000 if the incident occurs away from your office.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Forgery or alteration</b></p><p class="body-copy-grey">Hiscox will pay up to $2,500 for losses that result from the forgery or alteration of a check or similar financial document. We may also defend you, at our expense, if you’re sued for failing to honor a financial document you believe was forged. You can increase this limit by purchasing our <a href="/small-business-insurance/business-owner-insurance/commercial-crime/" title="">commercial crime insurance package</a> upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accounts receivable</b></p><p class="body-copy-grey">We’ll pay for lost receivables you cannot collect from your customers and extra collection expenses you incur as a result of damage to your records (e.g., invoices). We provide up to $10,000 of coverage for losses to records at your business premises and up to $5,000 for losses to records away from your premises.</p><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Business owner insurance – business equipment insurance does not cover:</b></h2><p> </p><p class="body-copy-grey">We want you to feel confident you understand the coverage in our business property insurance policy. That’s why we’re open about what we do and don’t cover. The following is a list of what we don’t cover:</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Earthquakes, floods and volcanoes</b></p><p class="body-copy-grey">Business property insurance doesn’t cover you for losses caused by earthquakes, floods, or volcanoes.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Dishonesty</b></p><p class="body-copy-grey">We won’t cover you for dishonest or criminal acts committed by you or your employees. Destruction of property by your employees may be covered, but theft by your employees is not. You may obtain coverage for employee theft by purchasing our commercial crime package upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Utilities</b></p><p class="body-copy-grey">Your commercial property insurance will not cover losses due to the failure of a power, communication or other utility service supplier.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Errors or omissions</b></p><p class="body-copy-grey">Hiscox won’t cover any losses due to mistakes in the programming, processing or storage of electronic data or processing valuable papers and records.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Computer testing, installation or repair</b></p><p class="body-copy-grey">Any losses due to mistakes in the design, installation, testing, maintenance or modification of your computer systems are not covered.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="consulting" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey"> </p><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td width="45%"><h2 class="grey-heading-h2">        <b>What is covered?</b></h2></td><td width="5%"> </td><td width="45%"><h2 class="grey-heading-h2"><b>What is not covered?</b></h2></td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Business equipment</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Earthquakes, floods and volcanoes</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Tenant improvements</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Dishonesty</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Coverage un and away from the office</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Utilities</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accidental damage</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Errors or omissions</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>No deductions for depreciation</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Computer testing, installation or repair</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Personal effects</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Lost business income and extra expense</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Electronic data</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Interruption of computer operations</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Valuable papers and records</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Forgery or alteration</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accounts receivable</li></ul></td><td> </td><td> </td></tr></tbody></table><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Hiscox business property and equipment insurance does cover:</b></h2><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business equipment</b></p><p class="body-copy-grey">Our commercial property insurance provides coverage for loss or damage to your business equipment (e.g. computers, printers, servers, office furniture).</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Tenant improvements</b></p><p class="body-copy-grey">With business property insurance, we cover improvements you’ve made to your office space that you can’t legally remove.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Coverage in and away from the office</b></p><p class="body-copy-grey">You’re covered whether you stay put or on the go. Your business equipment is insured when physically in your office. We also provide up to $10,000 in coverage for equipment you take away from your office such as laptops, projectors or other insured equipment.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accidental damage</b></p><p class="body-copy-grey">Not only do we provide insurance for equipment against risks like fire, we also cover losses due to accidental damage.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>No deductions for depreciation</b></p><p class="body-copy-grey">Most property insurance for small business under this policy is valued at its replacement cost. There is no deduction for depreciation so you can replace damaged items for equipment of like kind and quality.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Personal effects</b></p><p class="body-copy-grey">Your Hiscox commercial property insurance, provides up to $2,500 of coverage, per premise, for personal items that are damaged in an insured loss such as a large fire.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Lost business income and extra expense</b></p><p class="body-copy-grey">Hiscox business equipment insurance will pay the actual income your business loses and extra expenses you incur for up to 12 months if you cannot operate your business as a result of damage to your insured equipment. This includes up to 60 days of the ongoing payroll expenses of your employees.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Electronic data</b></p><p class="body-copy-grey">We provide up to $10,000 of business property insurance coverage to replace or restore your electronic data that is lost as a result of an insured loss. This includes losses caused by a virus or malicious code. If you work with large amounts of valuable data, this limit can be increased by purchasing our <a href="/small-business-insurance/business-owner-insurance/data-loss-insurance/" title="">electronic data loss insurance</a> upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Interruption of computer operations</b></p><p class="body-copy-grey">If you suffer an electronic data loss as above, we will also cover up to $10,000 for the actual income your business loses and extra expenses you incur if you cannot operate your business as a result of that data loss. If you work with large amounts of valuable data this limit can be increased by purchasing our electronic data loss upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Valuable papers and records</b></p><p class="body-copy-grey">Our commercial property insurance will pay for loss or damage to your business’s valuable papers and records at your business premises up to $10,000 and up to $5,000 if the incident occurs away from your office.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Forgery or alteration</b></p><p class="body-copy-grey">Hiscox will pay up to $2,500 for losses that result from the forgery or alteration of a check or similar financial document. We may also defend you, at our expense, if you’re sued for failing to honor a financial document you believe was forged. You can increase this limit by purchasing our <a href="/small-business-insurance/business-owner-insurance/commercial-crime/" title="">commercial crime insurance package</a> upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accounts receivable</b></p><p class="body-copy-grey">We’ll pay for lost receivables you cannot collect from your customers and extra collection expenses you incur as a result of damage to your records (e.g., invoices). We provide up to $10,000 of coverage for losses to records at your business premises and up to $5,000 for losses to records away from your premises.</p><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Business owner insurance – business equipment insurance does not cover:</b></h2><p> </p><p class="body-copy-grey">We want you to feel confident you understand the coverage in our business property insurance policy. That’s why we’re open about what we do and don’t cover. The following is a list of what we don’t cover:</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Earthquakes, floods and volcanoes</b></p><p class="body-copy-grey">Business property insurance doesn’t cover you for losses caused by earthquakes, floods, or volcanoes.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Dishonesty</b></p><p class="body-copy-grey">We won’t cover you for dishonest or criminal acts committed by you or your employees. Destruction of property by your employees may be covered, but theft by your employees is not. You may obtain coverage for employee theft by purchasing our commercial crime package upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Utilities</b></p><p class="body-copy-grey">Your commercial property insurance will not cover losses due to the failure of a power, communication or other utility service supplier.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Errors or omissions</b></p><p class="body-copy-grey">Hiscox won’t cover any losses due to mistakes in the programming, processing or storage of electronic data or processing valuable papers and records.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Computer testing, installation or repair</b></p><p class="body-copy-grey">Any losses due to mistakes in the design, installation, testing, maintenance or modification of your computer systems are not covered.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey"> </p><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td width="45%"><h2 class="grey-heading-h2">        <b>What is covered?</b></h2></td><td width="5%"> </td><td width="45%"><h2 class="grey-heading-h2"><b>What is not covered?</b></h2></td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Business equipment</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Earthquakes, floods and volcanoes</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Tenant improvements</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Dishonesty</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Coverage un and away from the office</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Utilities</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accidental damage</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Errors or omissions</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>No deductions for depreciation</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Computer testing, installation or repair</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Personal effects</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Lost business income and extra expense</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Electronic data</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Interruption of computer operations</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Valuable papers and records</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Forgery or alteration</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accounts receivable</li></ul></td><td> </td><td> </td></tr></tbody></table><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Hiscox business property and equipment insurance does cover:</b></h2><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business equipment</b></p><p class="body-copy-grey">Our commercial property insurance provides coverage for loss or damage to your business equipment (e.g. computers, printers, servers, office furniture).</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Tenant improvements</b></p><p class="body-copy-grey">With business property insurance, we cover improvements you’ve made to your office space that you can’t legally remove.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Coverage in and away from the office</b></p><p class="body-copy-grey">You’re covered whether you stay put or on the go. Your business equipment is insured when physically in your office. We also provide up to $10,000 in coverage for equipment you take away from your office such as laptops, projectors or other insured equipment.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accidental damage</b></p><p class="body-copy-grey">Not only do we provide insurance for equipment against risks like fire, we also cover losses due to accidental damage.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>No deductions for depreciation</b></p><p class="body-copy-grey">Most property insurance for small business under this policy is valued at its replacement cost. There is no deduction for depreciation so you can replace damaged items for equipment of like kind and quality.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Personal effects</b></p><p class="body-copy-grey">Your Hiscox commercial property insurance, provides up to $2,500 of coverage, per premise, for personal items that are damaged in an insured loss such as a large fire.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Lost business income and extra expense</b></p><p class="body-copy-grey">Hiscox business equipment insurance will pay the actual income your business loses and extra expenses you incur for up to 12 months if you cannot operate your business as a result of damage to your insured equipment. This includes up to 60 days of the ongoing payroll expenses of your employees.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Electronic data</b></p><p class="body-copy-grey">We provide up to $10,000 of business property insurance coverage to replace or restore your electronic data that is lost as a result of an insured loss. This includes losses caused by a virus or malicious code. If you work with large amounts of valuable data, this limit can be increased by purchasing our <a href="/small-business-insurance/business-owner-insurance/data-loss-insurance/" title="">electronic data loss insurance</a> upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Interruption of computer operations</b></p><p class="body-copy-grey">If you suffer an electronic data loss as above, we will also cover up to $10,000 for the actual income your business loses and extra expenses you incur if you cannot operate your business as a result of that data loss. If you work with large amounts of valuable data this limit can be increased by purchasing our electronic data loss upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Valuable papers and records</b></p><p class="body-copy-grey">Our commercial property insurance will pay for loss or damage to your business’s valuable papers and records at your business premises up to $10,000 and up to $5,000 if the incident occurs away from your office.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Forgery or alteration</b></p><p class="body-copy-grey">Hiscox will pay up to $2,500 for losses that result from the forgery or alteration of a check or similar financial document. We may also defend you, at our expense, if you’re sued for failing to honor a financial document you believe was forged. You can increase this limit by purchasing our <a href="/small-business-insurance/business-owner-insurance/commercial-crime/" title="">commercial crime insurance package</a> upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accounts receivable</b></p><p class="body-copy-grey">We’ll pay for lost receivables you cannot collect from your customers and extra collection expenses you incur as a result of damage to your records (e.g., invoices). We provide up to $10,000 of coverage for losses to records at your business premises and up to $5,000 for losses to records away from your premises.</p><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Business owner insurance – business equipment insurance does not cover:</b></h2><p> </p><p class="body-copy-grey">We want you to feel confident you understand the coverage in our business property insurance policy. That’s why we’re open about what we do and don’t cover. The following is a list of what we don’t cover:</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Earthquakes, floods and volcanoes</b></p><p class="body-copy-grey">Business property insurance doesn’t cover you for losses caused by earthquakes, floods, or volcanoes.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Dishonesty</b></p><p class="body-copy-grey">We won’t cover you for dishonest or criminal acts committed by you or your employees. Destruction of property by your employees may be covered, but theft by your employees is not. You may obtain coverage for employee theft by purchasing our commercial crime package upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Utilities</b></p><p class="body-copy-grey">Your commercial property insurance will not cover losses due to the failure of a power, communication or other utility service supplier.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Errors or omissions</b></p><p class="body-copy-grey">Hiscox won’t cover any losses due to mistakes in the programming, processing or storage of electronic data or processing valuable papers and records.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Computer testing, installation or repair</b></p><p class="body-copy-grey">Any losses due to mistakes in the design, installation, testing, maintenance or modification of your computer systems are not covered.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey"> </p><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td width="45%"><h2 class="grey-heading-h2">        <b>What is covered?</b></h2></td><td width="5%"> </td><td width="45%"><h2 class="grey-heading-h2"><b>What is not covered?</b></h2></td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Business equipment</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Earthquakes, floods and volcanoes</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Tenant improvements</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Dishonesty</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Coverage un and away from the office</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Utilities</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accidental damage</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Errors or omissions</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>No deductions for depreciation</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Computer testing, installation or repair</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Personal effects</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Lost business income and extra expense</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Electronic data</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Interruption of computer operations</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Valuable papers and records</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Forgery or alteration</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accounts receivable</li></ul></td><td> </td><td> </td></tr></tbody></table><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Hiscox business property and equipment insurance does cover:</b></h2><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business equipment</b></p><p class="body-copy-grey">Our commercial property insurance provides coverage for loss or damage to your business equipment (e.g. computers, printers, servers, office furniture).</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Tenant improvements</b></p><p class="body-copy-grey">With business property insurance, we cover improvements you’ve made to your office space that you can’t legally remove.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Coverage in and away from the office</b></p><p class="body-copy-grey">You’re covered whether you stay put or on the go. Your business equipment is insured when physically in your office. We also provide up to $10,000 in coverage for equipment you take away from your office such as laptops, projectors or other insured equipment.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accidental damage</b></p><p class="body-copy-grey">Not only do we provide insurance for equipment against risks like fire, we also cover losses due to accidental damage.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>No deductions for depreciation</b></p><p class="body-copy-grey">Most property insurance for small business under this policy is valued at its replacement cost. There is no deduction for depreciation so you can replace damaged items for equipment of like kind and quality.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Personal effects</b></p><p class="body-copy-grey">Your Hiscox commercial property insurance, provides up to $2,500 of coverage, per premise, for personal items that are damaged in an insured loss such as a large fire.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Lost business income and extra expense</b></p><p class="body-copy-grey">Hiscox business equipment insurance will pay the actual income your business loses and extra expenses you incur for up to 12 months if you cannot operate your business as a result of damage to your insured equipment. This includes up to 60 days of the ongoing payroll expenses of your employees.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Electronic data</b></p><p class="body-copy-grey">We provide up to $10,000 of business property insurance coverage to replace or restore your electronic data that is lost as a result of an insured loss. This includes losses caused by a virus or malicious code. If you work with large amounts of valuable data, this limit can be increased by purchasing our <a href="/small-business-insurance/business-owner-insurance/data-loss-insurance/" title="">electronic data loss insurance</a> upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Interruption of computer operations</b></p><p class="body-copy-grey">If you suffer an electronic data loss as above, we will also cover up to $10,000 for the actual income your business loses and extra expenses you incur if you cannot operate your business as a result of that data loss. If you work with large amounts of valuable data this limit can be increased by purchasing our electronic data loss upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Valuable papers and records</b></p><p class="body-copy-grey">Our commercial property insurance will pay for loss or damage to your business’s valuable papers and records at your business premises up to $10,000 and up to $5,000 if the incident occurs away from your office.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Forgery or alteration</b></p><p class="body-copy-grey">Hiscox will pay up to $2,500 for losses that result from the forgery or alteration of a check or similar financial document. We may also defend you, at our expense, if you’re sued for failing to honor a financial document you believe was forged. You can increase this limit by purchasing our <a href="/small-business-insurance/business-owner-insurance/commercial-crime/" title="">commercial crime insurance package</a> upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accounts receivable</b></p><p class="body-copy-grey">We’ll pay for lost receivables you cannot collect from your customers and extra collection expenses you incur as a result of damage to your records (e.g., invoices). We provide up to $10,000 of coverage for losses to records at your business premises and up to $5,000 for losses to records away from your premises.</p><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Business owner insurance – business equipment insurance does not cover:</b></h2><p> </p><p class="body-copy-grey">We want you to feel confident you understand the coverage in our business property insurance policy. That’s why we’re open about what we do and don’t cover. The following is a list of what we don’t cover:</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Earthquakes, floods and volcanoes</b></p><p class="body-copy-grey">Business property insurance doesn’t cover you for losses caused by earthquakes, floods, or volcanoes.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Dishonesty</b></p><p class="body-copy-grey">We won’t cover you for dishonest or criminal acts committed by you or your employees. Destruction of property by your employees may be covered, but theft by your employees is not. You may obtain coverage for employee theft by purchasing our commercial crime package upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Utilities</b></p><p class="body-copy-grey">Your commercial property insurance will not cover losses due to the failure of a power, communication or other utility service supplier.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Errors or omissions</b></p><p class="body-copy-grey">Hiscox won’t cover any losses due to mistakes in the programming, processing or storage of electronic data or processing valuable papers and records.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Computer testing, installation or repair</b></p><p class="body-copy-grey">Any losses due to mistakes in the design, installation, testing, maintenance or modification of your computer systems are not covered.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																									<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey"> </p><table cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td width="45%"><h2 class="grey-heading-h2">        <b>What is covered?</b></h2></td><td width="5%"> </td><td width="45%"><h2 class="grey-heading-h2"><b>What is not covered?</b></h2></td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Business equipment</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Earthquakes, floods and volcanoes</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Tenant improvements</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Dishonesty</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Coverage un and away from the office</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Utilities</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accidental damage</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Errors or omissions</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>No deductions for depreciation</li></ul></td><td> </td><td class="body-copy-grey"><img id="grey_cross" src="/shared-images/grey_cross.gif" alt="" title="" border="0" height="14" width="14" /> Computer testing, installation or repair</td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Personal effects</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Lost business income and extra expense</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Electronic data</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Interruption of computer operations</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Valuable papers and records</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Forgery or alteration</li></ul></td><td> </td><td> </td></tr><tr><td><ul class="list-red-tick-body-copy-grey"><li>Accounts receivable</li></ul></td><td> </td><td> </td></tr></tbody></table><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Hiscox business property and equipment insurance does cover:</b></h2><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business equipment</b></p><p class="body-copy-grey">Our commercial property insurance provides coverage for loss or damage to your business equipment (e.g. computers, printers, servers, office furniture).</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Tenant improvements</b></p><p class="body-copy-grey">With business property insurance, we cover improvements you’ve made to your office space that you can’t legally remove.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Coverage in and away from the office</b></p><p class="body-copy-grey">You’re covered whether you stay put or on the go. Your business equipment is insured when physically in your office. We also provide up to $10,000 in coverage for equipment you take away from your office such as laptops, projectors or other insured equipment.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accidental damage</b></p><p class="body-copy-grey">Not only do we provide insurance for equipment against risks like fire, we also cover losses due to accidental damage.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>No deductions for depreciation</b></p><p class="body-copy-grey">Most property insurance for small business under this policy is valued at its replacement cost. There is no deduction for depreciation so you can replace damaged items for equipment of like kind and quality.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Personal effects</b></p><p class="body-copy-grey">Your Hiscox commercial property insurance, provides up to $2,500 of coverage, per premise, for personal items that are damaged in an insured loss such as a large fire.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Lost business income and extra expense</b></p><p class="body-copy-grey">Hiscox business equipment insurance will pay the actual income your business loses and extra expenses you incur for up to 12 months if you cannot operate your business as a result of damage to your insured equipment. This includes up to 60 days of the ongoing payroll expenses of your employees.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Electronic data</b></p><p class="body-copy-grey">We provide up to $10,000 of business property insurance coverage to replace or restore your electronic data that is lost as a result of an insured loss. This includes losses caused by a virus or malicious code. If you work with large amounts of valuable data, this limit can be increased by purchasing our <a href="/small-business-insurance/business-owner-insurance/data-loss-insurance/" title="">electronic data loss insurance</a> upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Interruption of computer operations</b></p><p class="body-copy-grey">If you suffer an electronic data loss as above, we will also cover up to $10,000 for the actual income your business loses and extra expenses you incur if you cannot operate your business as a result of that data loss. If you work with large amounts of valuable data this limit can be increased by purchasing our electronic data loss upgrade package.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Valuable papers and records</b></p><p class="body-copy-grey">Our commercial property insurance will pay for loss or damage to your business’s valuable papers and records at your business premises up to $10,000 and up to $5,000 if the incident occurs away from your office.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Forgery or alteration</b></p><p class="body-copy-grey">Hiscox will pay up to $2,500 for losses that result from the forgery or alteration of a check or similar financial document. We may also defend you, at our expense, if you’re sued for failing to honor a financial document you believe was forged. You can increase this limit by purchasing our <a href="/small-business-insurance/business-owner-insurance/commercial-crime/" title="">commercial crime insurance package</a> upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Accounts receivable</b></p><p class="body-copy-grey">We’ll pay for lost receivables you cannot collect from your customers and extra collection expenses you incur as a result of damage to your records (e.g., invoices). We provide up to $10,000 of coverage for losses to records at your business premises and up to $5,000 for losses to records away from your premises.</p><p class="body-copy-grey"> </p><h2 class="red-call-to-action-medium-h2"><b>Business owner insurance – business equipment insurance does not cover:</b></h2><p> </p><p class="body-copy-grey">We want you to feel confident you understand the coverage in our business property insurance policy. That’s why we’re open about what we do and don’t cover. The following is a list of what we don’t cover:</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Earthquakes, floods and volcanoes</b></p><p class="body-copy-grey">Business property insurance doesn’t cover you for losses caused by earthquakes, floods, or volcanoes.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Dishonesty</b></p><p class="body-copy-grey">We won’t cover you for dishonest or criminal acts committed by you or your employees. Destruction of property by your employees may be covered, but theft by your employees is not. You may obtain coverage for employee theft by purchasing our commercial crime package upgrade.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Utilities</b></p><p class="body-copy-grey">Your commercial property insurance will not cover losses due to the failure of a power, communication or other utility service supplier.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Errors or omissions</b></p><p class="body-copy-grey">Hiscox won’t cover any losses due to mistakes in the programming, processing or storage of electronic data or processing valuable papers and records.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Computer testing, installation or repair</b></p><p class="body-copy-grey">Any losses due to mistakes in the design, installation, testing, maintenance or modification of your computer systems are not covered.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
			<!--Secondary content section starts here-->
																													<usd:checkOccup occupContent="it-consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="red-call-to-action-medium-h2"><b>Business Owners Policy Claims Examples:</b></h2><p class="body-copy-grey"><b>Stolen property</b> - A break-in results in stolen business property as well as theft of client’s property in your possession. Your Hiscox small business property insurance policy will cover the loss up to your applicable limits for that premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Equipment lost away from the office</b> – You take the train to visit a client and your laptop is stolen on the way. We offer up to $10,000 coverage for business personal property away from the office premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business interruption</b> – A fire in your office destroys your computer network and equipment, that you insured as business personal property. Now your business can’t operate for 2 weeks – the lost income and extra expenses resulting from the loss are covered.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Insuring for full value – If you insure your property for less than 80% of the full replacement cost, you may not receive the full amount of your loss</b></p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
	    																						<usd:checkOccup occupContent="consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="red-call-to-action-medium-h2"><b>Business Owners Policy Claims Examples:</b></h2><p class="body-copy-grey"><b>Stolen property</b> - A break-in results in stolen business property as well as theft of client’s property in your possession. Your Hiscox small business property insurance policy will cover the loss up to your applicable limits for that premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Equipment lost away from the office</b> – You take the train to visit a client and your laptop is stolen on the way. We offer up to $10,000 coverage for business personal property away from the office premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business interruption</b> – A fire in your office destroys your computer network and equipment, that you insured as business personal property. Now your business can’t operate for 2 weeks – the lost income and extra expenses resulting from the loss are covered.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Insuring for full value – If you insure your property for less than 80% of the full replacement cost, you may not receive the full amount of your loss</b></p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
	    																						<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="red-call-to-action-medium-h2"><b>Business Owners Policy Claims Examples:</b></h2><p class="body-copy-grey"><b>Stolen property</b> - A break-in results in stolen business property as well as theft of client’s property in your possession. Your Hiscox small business property insurance policy will cover the loss up to your applicable limits for that premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Equipment lost away from the office</b> – You take the train to visit a client and your laptop is stolen on the way. We offer up to $10,000 coverage for business personal property away from the office premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business interruption</b> – A fire in your office destroys your computer network and equipment, that you insured as business personal property. Now your business can’t operate for 2 weeks – the lost income and extra expenses resulting from the loss are covered.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Insuring for full value – If you insure your property for less than 80% of the full replacement cost, you may not receive the full amount of your loss</b></p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
	    																						<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="red-call-to-action-medium-h2"><b>Business Owners Policy Claims Examples:</b></h2><p class="body-copy-grey"><b>Stolen property</b> - A break-in results in stolen business property as well as theft of client’s property in your possession. Your Hiscox small business property insurance policy will cover the loss up to your applicable limits for that premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Equipment lost away from the office</b> – You take the train to visit a client and your laptop is stolen on the way. We offer up to $10,000 coverage for business personal property away from the office premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business interruption</b> – A fire in your office destroys your computer network and equipment, that you insured as business personal property. Now your business can’t operate for 2 weeks – the lost income and extra expenses resulting from the loss are covered.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Insuring for full value – If you insure your property for less than 80% of the full replacement cost, you may not receive the full amount of your loss</b></p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
	    																								<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<h2 class="red-call-to-action-medium-h2"><b>Business Owners Policy Claims Examples:</b></h2><p class="body-copy-grey"><b>Stolen property</b> - A break-in results in stolen business property as well as theft of client’s property in your possession. Your Hiscox small business property insurance policy will cover the loss up to your applicable limits for that premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Equipment lost away from the office</b> – You take the train to visit a client and your laptop is stolen on the way. We offer up to $10,000 coverage for business personal property away from the office premises.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Business interruption</b> – A fire in your office destroys your computer network and equipment, that you insured as business personal property. Now your business can’t operate for 2 weeks – the lost income and extra expenses resulting from the loss are covered.</p><p class="body-copy-grey"> </p><p class="body-copy-grey"><b>Insuring for full value – If you insure your property for less than 80% of the full replacement cost, you may not receive the full amount of your loss</b></p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
							<!--Secondary content section ends here-->
			<!--eligibility container starts here-->
								<div class="cnt-subsctn white-all-corner">
			<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
			<div class="middle-content vertical-container">
			<div class="elgibltybox-heading"><h2>Get an accurate quote and buy online in minutes</h2></div>
			<usd:checkInclude includeContent="state">
				<usd:checkFormTag formContent="Start"></usd:checkFormTag>
				<usd:questionTag  />
				<usd:actionTag  />
				<usd:trackingTag />
				<usd:checkFormTag formContent="End"></usd:checkFormTag>	
			 </usd:checkInclude>
			  </div>
			  <div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
			</div>
					<!--eligibility container ends here-->
			<!--Centre bottom section starts here-->			
			<usd:checkOccup occupContent="it-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        									        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        									        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        									        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        									        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  										    										    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup>
			<!--Centre bottom section ends here-->
			<!--Tertiary content section starts here-->
																										<usd:checkOccup occupContent="it-consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																							<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																									<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the coverage we offer and does not modify the terms and conditions of any insurance policy, nor does it imply any claim is covered. Coverages are subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
						<!--Tertiary content section ends here-->
		</div>  
		<!--Content section ends here-->
		<!-- Right nav starts here-->
		<div class="rightnav-gap showcntct-num">
			<!--Checklist section starts here-->
			<usd:checkOccup occupContent="it-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        											<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        											<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        											<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        											<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-3007</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  										    										    										    										            										            																							<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><b><span class="grey-call-to-action-large">888-202-3007</span></b></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				 			            										    										    										    										    																																        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        															<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        															<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        															<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		        			        															<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
	        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  										    										    																											<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/get-a-quote-for-your-small-business" title="Get a quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				 			    										            										            										            										    										    										    										    																																        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b><span style=" text-decoration: underline;">Why choose Hiscox?</span></b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for technology businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific technology business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b>Why choose Hiscox?</b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for consulting businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your consulting business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b>Why choose Hiscox?</b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for marketing consultants with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific marketing business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b>Why choose Hiscox?</b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for professional services businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                  																			<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<p class="body-copy-grey"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title=""><b>Why choose Hiscox?</b></a></p><img id="redline_whychoose_right" src="/shared-images/redline_whychoose_right.png" alt="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." title="Why choose Hiscox? We provide customized small business insurance at competitive rates when you buy direct." border="0" height="2" width="168" /><ul class="list-red-tick-body-copy-grey-small"><li><b>Specialists</b> - in insurance for professional services businesses with 10 employees or less.<br /></li><li><b>Customized coverage</b> - we customize coverage for your specific business. No more, no less.<br /></li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.<br /></li><li><b>Great value</b> - competitive rates and great service when you buy direct from Hiscox.</li></ul><p class="red-call-to-action-small" style=" margin-left: 80px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Learn more &gt;</a></p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
				    										    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup>			
			<div class="verLogo-holder"><img src="/resources/images/secured-icon.gif" alt=""  title="Verisign" /></div>
			<!--Checklist section ends here-->
		</div>
		<!-- Right nav ends here-->
		<!-- the closing div for left nav is present in the page templates-->
		<div class="boiler-plate">
    		
	
	
	
	
    			</div>
		<!--Footer starts here -->
		<div class="footer-home">
			<div class="copy-right-home-box">
				
	
	
	
    							
																<p class="body-copy-grey">© Hiscox Inc</p>
										
    	    		
		 					</div>			
							
	
	
	
    							
							<div class="footer-dropdown-box">
                <label for="country">Not in the US?</label>
                <select name="country" id="country" onchange="if(this.options[this.selectedIndex].value!='')openChosenURL(this);">
                <option value="">Select your country</option>
                                                	                		<option value="http://www.hiscox.be">Belgium</option>
                	                		<option value="http://www.hiscox.fr">France</option>
                	                		<option value="http://www.hiscox.de">Germany</option>
                	                		<option value="http://www.hiscox.ie">Ireland</option>
                	                		<option value="http://www.hiscox.nl">Netherlands</option>
                	                		<option value="http://www.hiscox.pt">Portugal</option>
                	                		<option value="http://www.hiscox.es">Spain</option>
                	                		<option value="http://www.hiscox.co.uk">United Kingdom</option>
                	                				</select></div>
						
    	    		
		 								<ul class='footer-links-home-box'>
	
	
	
	
    							
																  <li><p class="body-copy-grey">
	                    <a href="/legal-notices/" title="" target="_blank" >
							Legal notices</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/site-map.html" title="" target="_blank" >
							Site map</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/privacy-policy/" title="" target="_blank" >
							Privacy policy</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/accessibility/" title="" target="_blank" >
							Accessibility</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/terms-of-use/" title="" target="_blank" >
							Terms of use</a></p>
					  </li>
										
    	    		
		 		</ul>		</div>
		<!--Footer ends here -->
		</div>
		<!-- Page container ends here-->
	</div>	<!--main container ends here-->	
<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/landingpageprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
</body>
</html>