<!Doctype html>
<html>
<head>
<title>Partner Testing Page</title>
</head>
<body>
<center>
<form method="post" action="/small-business-insurance/quote-and-buy/brochureware/">
  <table border="0" style="width:50%">
  
     <tr>
    <td><img src="https://www.hiscox.com/shared-images/emails-images/eml_spacer.gif"> </td>
    <td> </td> 
  </tr>
  <tr>
    <td><img src="https://www.hiscox.com/shared-images/emails-images/eml_logo.gif" ></td>
    <td></td> 
  </tr>
     <tr>
    <td><img src="https://www.hiscox.com/shared-images/emails-images/eml_spacer.gif"> </td>
    <td> </td> 
  </tr>
  <tr>
	<td>Partner</td>
	<td>
		<select name="partner">
			<option value="" selected="selected"></option>
			<option value="GEICO">GEICO</option>
			<option value="Progressive">Progressive</option>
			<option value="Combined Group">Combined Group</option>
			<option value="Geico web & phone">Geico web & phone</option>
			<option value="Insureon">Insureon</option>
			<option value="US Direct">US Direct</option>
		</select>
	</td>
  </tr>
  <tr>
    <td>State</td>
    <td><select name="state" >
									<option value="" selected="selected"></option>
																			<option value="AL">Alabama</option>
																			<option value="AK">Alaska</option>
																			<option value="AZ">Arizona</option>
																			<option value="AR">Arkansas</option>
																			<option value="CA">California</option>
																			<option value="CO">Colorado</option>
																			<option value="CT">Connecticut</option>
																			<option value="DE">Delaware</option>
																			<option value="DC">District Of Columbia</option>
																			<option value="FL">Florida</option>
																			<option value="GA">Georgia</option>
																			<option value="HI">Hawaii</option>
																			<option value="ID">Idaho</option>
																			<option value="IL">Illinois</option>
																			<option value="IN">Indiana</option>
																			<option value="IA">Iowa</option>
																			<option value="KS">Kansas</option>
																			<option value="KY">Kentucky</option>
																			<option value="LA">Louisiana</option>
																			<option value="ME">Maine</option>
																			<option value="MD">Maryland</option>
																			<option value="MA">Massachusetts</option>
																			<option value="MI">Michigan</option>
																			<option value="MN">Minnesota</option>
																			<option value="MS">Mississippi</option>
																			<option value="MO">Missouri</option>
																			<option value="MT">Montana</option>
																			<option value="NE">Nebraska</option>
																			<option value="NV">Nevada</option>
																			<option value="NH">New Hampshire</option>
																			<option value="NJ">New Jersey</option>
																			<option value="NM">New Mexico</option>
																			<option value="NY">New York</option>
																			<option value="NC">North Carolina</option>
																			<option value="ND">North Dakota</option>
																			<option value="OH">Ohio</option>
																			<option value="OK">Oklahoma</option>
																			<option value="OR">Oregon</option>
																			<option value="PA">Pennsylvania</option>
																			<option value="RI">Rhode Island</option>
																			<option value="SC">South Carolina</option>
																			<option value="SD">South Dakota</option>
																			<option value="TN">Tennessee</option>
																			<option value="TX">Texas</option>
																			<option value="UT">Utah</option>
																			<option value="VT">Vermont</option>
																			<option value="VA">Virginia</option>
																			<option value="WA">Washington</option>
																			<option value="WV">West Virginia</option>
																			<option value="WI">Wisconsin</option>
																			<option value="WY">Wyoming</option>
																	</select>
	</td> 
  </tr>
  <tr>
    <td>Primary Service</td>
    <td>
																	<select name="primarybusiness">
																			<option value="" selected="selected"></option>
																			<option value="Accounting">Accounting</option>
																			<option value="Actuarial services">Actuarial services</option>
																			<option value="Acupressure services">Acupressure services</option>
																			<option value="Acupuncture services">Acupuncture services</option>
																			<option value="Advertising">Advertising</option>
																			<option value="Air conditioning systems installation/repair">Air conditioning systems installation/repair</option>
																			<option value="Answering/paging services">Answering/paging services</option>
																			<option value="Appliance/electronic stores">Appliance/electronic stores</option>
																			<option value="Application development">Application development</option>
																			<option value="Application service provider">Application service provider</option>
																			<option value="Appliance and accessories installation/repair">Appliance and accessories installation/repair</option>
																			<option value="Architecture">Architecture</option>
																			<option value="Art therapy">Art therapy</option>
																			<option value="Auctioneering">Auctioneering</option>
																			<option value="Audiology">Audiology</option>
																			<option value="Beautician/cosmetology services">Beautician/cosmetology services</option>
																			<option value="Bookkeeping">Bookkeeping</option>
																			<option value="Brand consultant">Brand consultant</option>
																			<option value="Building/construction inspection">Building inspection</option>
																			<option value="Business consulting">Business consulting</option>
																			<option value="Business manager services">Business manager services</option>
																			<option value="Carpentry (interior only)">Carpentry (interior only)</option>
																			<option value="Carpet/furniture/upholstery cleaning(offsite only)">Carpet/furniture/upholstery cleaning(offsite only)</option>
																			<option value="Civil engineering">Civil engineering</option>
																			<option value="Claims adjusting">Claims adjusting</option>
																			<option value="Clock making/repair">Clock making/repair</option>
																			<option value="Clothing/apparel stores">Clothing/apparel stores</option>
																			<option value="Computer consulting">Computer consulting</option>
																			<option value="Computer programming services">Computer programming services</option>
																			<option value="Computer system/network developer">Computer system/network developer</option>
																			<option value="Control systems integration/automation">Control systems integration/automation</option>
																			<option value="Court reporting">Court reporting</option>
																			<option value="Credit counseling">Credit counseling</option>
																			<option value="Dance therapy">Dance therapy</option>
																			<option value="Data processing">Data processing</option>
																			<option value="Database designer">Database designer</option>
																			<option value="Dietician/nutrition">Diet/nutrition services</option>
																			<option value="Digital marketing">Digital marketing</option>
																			<option value="Direct marketing">Direct marketing</option>
																			<option value="Document preparation">Document preparation</option>
																			<option value="Door or window installation/repair">Door or window installation/repair</option>
																			<option value="Draftsman (including CAD/CAM)">Draftsman (including CAD/CAM)</option>
																			<option value="Drama therapy">Drama therapy</option>
																			<option value="Driveway or sidewalk paving/repaving">Driveway or sidewalk paving/repaving</option>
																			<option value="Drywall or wallboard installation/repair">Drywall or wallboard installation/repair</option>
																			<option value="Education consulting">Education consulting</option>
																			<option value="Electrical engineering">Electrical engineering</option>
																			<option value="Electrical work (interior only)">Electrical work (interior only)</option>
																			<option value="Engineering">Engineering</option>
																			<option value="Environmental engineering">Environmental engineering</option>
																			<option value="Esthetician services">Esthetician services</option>
																			<option value="Event planning/promotion">Event planning/promotion</option>
																			<option value="Executive placement">Executive placement</option>
																			<option value="Expert witness services">Expert witness services</option>
																			<option value="Exterior cleaning services">Exterior cleaning services</option>
																			<option value="Fence installation/repair">Fence installation/repair</option>
																			<option value="Financial auditing or consulting">Financial auditing or consulting</option>
																			<option value="First aid and CPR training">First aid and CPR training</option>
																			<option value="Floor covering installation(no ceramic tile/stone)">Floor covering installation(no ceramic tile/stone)</option>
																			<option value="Florists">Florists</option>
																			<option value="Glass installation/repair (no auto work)">Glass installation/repair (no auto work)</option>
																			<option value="Graphic design">Graphic design</option>
																			<option value="Handyperson (no roof work)">Handyperson (no roof work)</option>
																			<option value="Barber/hair stylist services">Hair stylist/barber services</option>
																			<option value="Heating/air conditioning install/repair(no LPG)">Heating/air conditioning install/repair(no LPG)</option>
																			<option value="Home furnishing stores">Home furnishing stores</option>
																			<option value="Home healthcare aide">Home healthcare aide</option>
																			<option value="Human Resources (HR) consulting">Human Resources (HR) consulting</option>
																			<option value="Hypnosis">Hypnosis</option>
																			<option value="Industrial engineering">Industrial engineering</option>
																			<option value="Insurance agent">Insurance agent</option>
																			<option value="Insurance inspector">Insurance inspector</option>
																			<option value="Interior design">Interior Design</option>
																			<option value="Interior finishing work">Interior finishing work</option>
																			<option value="Investment advice">Investment advice</option>
																			<option value="IT consulting">IT consulting</option>
																			<option value="IT project management">IT project management</option>
																			<option value="IT software/hardware training services">IT software/hardware training services</option>
																			<option value="Janitorial/cleaning services">Janitorial/cleaning services</option>
																			<option value="Jewelry stores">Jewelry stores</option>
																			<option value="Landscape architect">Landscape architect</option>
																			<option value="Landscaping/gardening services">Landscaping/gardening services</option>
																			<option value="Land Surveyor">Land Surveyor</option>
																			<option value="Lawn care services">Lawn care services</option>
																			<option value="Legal services">Legal services</option>
																			<option value="Life/career/executive coaching">Life/career/executive coaching</option>
																			<option value="Locksmiths">Locksmiths</option>
																			<option value="Masonry work">Masonry work</option>
																			<option value="Management consulting">Management consulting</option>
																			<option value="Manufacturer sales representative">Manufacturer sales representative</option>
																			<option value="Market research">Market research</option>
																			<option value="Marketing/media consulting">Marketing/media consulting</option>
																			<option value="Marriage and family therapy">Marriage and family therapy</option>
																			<option value="Massage therapy">Massage therapy</option>
																			<option value="Medical billing">Medical billing</option>
																			<option value="Mental health counseling">Mental health counseling</option>
																			<option value="Mortgage brokering/banking">Mortgage brokering/banking</option>
																			<option value="Music therapy">Music therapy</option>
																			<option value="Nail technician services">Nail technician services</option>
																			<option value="Notary services">Notary services</option>
																			<option value="Occupational therapy">Occupational therapy</option>
																			<option value="Other stores (with food/drinks)">Other stores (with food/drinks)</option>
																			<option value="Other stores (without food/drinks)">Other stores (without food/drinks)</option>
																			<option value="Painting (interior only)">Painting (interior only)</option>
																			<option value="Personal care assistant">Personal care assistant</option>
																			<option value="Personal concierge/assistant">Personal concierge/assistant</option>
																			<option value="Personal training (health and fitness)">Personal training (health and fitness)</option>
																			<option value="Photography">Photography</option>
																			<option value="Plastering or stucco work">Plastering or stucco work</option>
																			<option value="Plumbing (commercial/industrial)">Plumbing (commercial/industrial)</option>
																			<option value="Plumbing (residential/domestic)">Plumbing (residential/domestic)</option>
																			<option value="Process engineering">Process engineering</option>
																			<option value="Process server">Process server</option>
																			<option value="Project management">Project management</option>
																			<option value="Construction/project management">Project manager (architecture or engineering)</option>
																			<option value="Property management">Property management</option>
																			<option value="Psychology">Psychology</option>
																			<option value="Public relations">Public relations</option>
																			<option value="Real estate agent/broker">Real estate agent/broker</option>
																			<option value="Recruiting (employment placements)">Recruiting (employment placements)</option>
																			<option value="Research consulting">Research consulting</option>
																			<option value="Resume consulting">Resume consulting</option>
																			<option value="Safety consultant">Safety consultant</option>
																			<option value="Search engine services (SEO/SEM)">Search engine services (SEO/SEM)</option>
																			<option value="Sign painting/lettering (interior only)">Sign painting/lettering (interior only)</option>
																			<option value="Sign painting/lettering (exterior only)">Sign painting/lettering (exterior only)</option>
																			<option value="Snow blowing and removal (no auto coverage)">Snow blowing and removal (no auto coverage)</option>
																			<option value="Social media consultant">Social media consultant</option>
																			<option value="Social work services">Social work services</option>
																			<option value="Software development">Software development</option>
																			<option value="Speech therapy">Speech therapy</option>
																			<option value="Stock brokering">Stock brokering</option>
																			<option value="Strategy consultant">Strategy consultant</option>
																			<option value="Substance abuse counseling">Substance abuse counseling</option>
																			<option value="Talent agency">Talent agency</option>
																			<option value="Tax preparation">Tax preparation</option>
																			<option value="Technology services">Technology services</option>
																			<option value="Tile/stone/marble/mosaic/terrazzo work(int. only)">Tile/stone/marble/mosaic/terrazzo work(int. only)</option>
																			<option value="Training (business, vocational or life skills)">Training (business, vocational or life skills)</option>
																			<option value="Translating/interpreting">Translating/interpreting</option>
																			<option value="Transportation engineering">Transportation engineering</option>
																			<option value="Travel agency">Travel agency</option>
																			<option value="Trustee">Trustee services</option>
																			<option value="Tutoring">Tutoring</option>
																			<option value="Upholstery work">Upholstery work</option>
																			<option value="Value added reseller of computer hardware">Value added reseller of computer hardware</option>
																			<option value="Website design">Website design</option>
																			<option value="Window cleaning (nothing above 15 feet)">Window cleaning (nothing above 15 feet)</option>
																			<option value="Yoga/pilates instruction">Yoga/pilates instruction</option>
																			<option value="Other consulting services">Other consulting services</option>
																			<option value="Other marketing/PR services">Other marketing/PR services</option>
																			<option value="Other technology services">Other technology services</option>
																			<option value="NONE OF THE ABOVE">NONE OF THE ABOVE</option>
																	</select>
	
	</td> 
  </tr>
  <tr>
	<td><input type="hidden" name="partnerAgent" id="partnerAgent" value="88"/></td>
	<td><input type="hidden" name="quoteOrigin" id="quoteOrigin" value="Online"/></td>
	<td><input type="hidden" name="action_StartQuote_button" value="Get a quote"/></td>
  </tr>
     <tr>
    <td><img src="https://www.hiscox.com/shared-images/emails-images/eml_spacer.gif"> </td>
    <td> </td> 
  </tr>
  <tr>
    <td></td>
    <td><input type="submit" value="GET A QUOTE"></td> 
  </tr>
</table>
 </form>  
 </center> 
</body>
</html>