<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DataCash Confirmation</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"
	name="description" />
<link href="/resources/css/global-reset.css" rel="stylesheet"
	type="text/css" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet"
	type="text/css" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css"
	type="text/css" />
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie.css";</style><![endif]-->
<!--[if IE]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/datacashconfirmation.js"></script>
<noscript><link rel="stylesheet" href="/resources/css/confirmation.css" type="text/css" /></noscript>
</head>
<body>
<form:form method="post" commandName="screen" name="datacashConfirmation">
<div class="cnt-sctn ">
<div class="top">
<div class="right">
<div class="left">
<div class="middle"></div>
</div>
</div>
</div>
    <div class="middle-content">
              <div class="page-heading page-heading-border">
                 <h1 class="red-call-to-action-large-h1">Pick up where you left off</h1></div>
                  <div class="clear"></div>
                   <div class="topcnt-container">
                        <p class="body-copy-grey">On this occasion we have been unable to produce an online quote for you, but we&rsquo;re confident that we can still help you get the right coverage for your business.</p>
                        <p class="body-copy-grey">If you were in the middle of a quote, our experienced team of licensed advisors will be able to <b>pick up where you left off &ndash; please call us at 888-202-2973 (8am &ndash; 10pm EST&sbquo; Mon-Fri).</b></p>
                        <p class="body-copy-grey">Regards&sbquo;</p><p class="body-copy-grey">Kevin Kerridge<br />Head of Direct Business</p>																						</div>
                        <div class="btmbtn-cntnr"> <div class="clear-both"></div>
          </div></div>
<div class="bottom clear-both">
<div class="right">
<div class="left">
<div class="middle"></div>
</div>
</div>
</div>
</div>
</form:form>
</body>
</html>
