<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Waiver of subrogation text|Hiscox</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
						                							                								<meta name="robots" content="noindex,nofollow" />
    			    					<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print" />
<!--[if lte IE 9]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body>
  <div class="main-container-popup">
  <!-- Header starts here-->
	<div class="header-popup">
		<div class="popuplogo" id="print_rx_logo_id">
	
	
	
    							
															                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		
    <div class="popup-hdr-text">
	      <ol>
		<li class='red-button right-arrow-white float-right'>
        		</li>
    </ol>
       </div>
  </div>
  <!-- Header ends here-->
  <!-- Page container starts here-->
  <div class="page-container">
      <!--Screen builder section starts here-->
    <div class="cnt-sctn popup-centr-sctn" id="print_rx_cnt_id">
	        	        <div class="middle-content">
        <div class="page-heading page-heading-border">
        		    																							<usd:checkOccup occupContent="it-consulting" >
		    	
    	<h1 class="page-heading-h1">Modified Waiver of Transfer of Rights of Recovery Against Others to Us (Waiver of Subrogation)</h1>    	
				</usd:checkOccup>
			    																							<usd:checkOccup occupContent="consulting" >
		    	
    	<h1 class="page-heading-h1">Modified Waiver of Transfer of Rights of Recovery Against Others to Us (Waiver of Subrogation)</h1>    	
				</usd:checkOccup>
			    																							<usd:checkOccup occupContent="marketing-pr-consulting" >
		    	
    	<h1 class="page-heading-h1">Modified Waiver of Transfer of Rights of Recovery Against Others to Us (Waiver of Subrogation)</h1>    	
				</usd:checkOccup>
			    																							<usd:checkOccup occupContent="default-occupation-known" >
		    	
    	<h1 class="page-heading-h1">Modified Waiver of Transfer of Rights of Recovery Against Others to Us (Waiver of Subrogation)</h1>    	
				</usd:checkOccup>
			    																									<usd:checkOccup occupContent="default-occupation-not-known" >
		    	
    	<h1 class="page-heading-h1">Modified Waiver of Transfer of Rights of Recovery Against Others to Us (Waiver of Subrogation)</h1>    	
				</usd:checkOccup>
			    	        <div class="reference-number">
			<usd:checkInclude  includeContent="quoteRefID">
				<span>Reference # <usd:include sessionValue="quoteRefID"/></span>
			</usd:checkInclude>	
		</div>
        </div>
        <div class="clear"></div>
        <div class="topcnt-container">
                   																								<usd:checkOccup occupContent="it-consulting" >
		    	
    	<p class="body-copy-grey">Some client contracts or landlord lease agreements require your Commercial General Liability insurance to include a waiver of subrogation. If your contract or lease agreement requires this coverage modification, you can include it on your policy for an additional fee.</p><p class="body-copy-grey">This endorsement allows you to waive your rights against another party so long as you do so in writing prior to:</p><p class="body-copy-grey">• An offense arising out of your business that caused a “personal and advertising injury” or an "occurrence" that caused "bodily injury" or "property damage".</p>    	
				</usd:checkOccup>
																											<usd:checkOccup occupContent="consulting" >
		    	
    	<p class="body-copy-grey">Some client contracts or landlord lease agreements require your Commercial General Liability insurance to include a waiver of subrogation. If your contract or lease agreement requires this coverage modification, you can include it on your policy for an additional fee.</p><p class="body-copy-grey">This endorsement allows you to waive your rights against another party so long as you do so in writing prior to:</p><p class="body-copy-grey">• An offense arising out of your business that caused a “personal and advertising injury” or an "occurrence" that caused "bodily injury" or "property damage".</p>    	
				</usd:checkOccup>
																											<usd:checkOccup occupContent="marketing-pr-consulting" >
		    	
    	<p class="body-copy-grey">Some client contracts or landlord lease agreements require your Commercial General Liability insurance to include a waiver of subrogation. If your contract or lease agreement requires this coverage modification, you can include it on your policy for an additional fee.</p><p class="body-copy-grey">This endorsement allows you to waive your rights against another party so long as you do so in writing prior to:</p><p class="body-copy-grey">• An offense arising out of your business that caused a “personal and advertising injury” or an "occurrence" that caused "bodily injury" or "property damage".</p>    	
				</usd:checkOccup>
																											<usd:checkOccup occupContent="default-occupation-known" >
		    	
    	<p class="body-copy-grey">Some client contracts or landlord lease agreements require your Commercial General Liability insurance to include a waiver of subrogation. If your contract or lease agreement requires this coverage modification, you can include it on your policy for an additional fee.</p><p class="body-copy-grey">This endorsement allows you to waive your rights against another party so long as you do so in writing prior to:</p><p class="body-copy-grey">• An offense arising out of your business that caused a “personal and advertising injury” or an "occurrence" that caused "bodily injury" or "property damage".</p>    	
				</usd:checkOccup>
																													<usd:checkOccup occupContent="default-occupation-not-known" >
		    	
    	<p class="body-copy-grey">Some client contracts or landlord lease agreements require your Commercial General Liability insurance to include a waiver of subrogation. If your contract or lease agreement requires this coverage modification, you can include it on your policy for an additional fee.</p><p class="body-copy-grey">This endorsement allows you to waive your rights against another party so long as you do so in writing prior to:</p><p class="body-copy-grey">• An offense arising out of your business that caused a “personal and advertising injury” or an "occurrence" that caused "bodily injury" or "property damage".</p>    	
				</usd:checkOccup>
						<div class="horz-line2"></div>
				</div>
		<usd:verifyToInclude>
		<usd:checkFormTag formContent="start"></usd:checkFormTag>
		<usd:questionTag  />  
		<usd:actionTag  />
	<usd:checkFormTag formContent="end"></usd:checkFormTag>
	</usd:verifyToInclude>
	<usd:trackingTag />
	     <div class="btmbtn-cntnr"> 
						        		                									<div class="clear-both"></div>
		 </div> 
    <!--Screen builder section ends here-->
    </div>
	    	 </div>
<!-- Page container ends here-->
				<div class="boiler-plate">
			 <div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>    
				<div class="middle-content"> 
					<div class="middlecnt-container">
							
	
	
	
	
    							
																<h2 class="red-call-to-action-medium-h2"><b>About Hiscox small business insurance</b></h2><ul class="list-bulleted-body-copy-grey"><li><b>Small business specialists</b> - focus on insuring professional businesses with less than 10 employees.</li><li><b>Customized coverage</b> - we help customize coverage to your specific needs. No more, no less.</li><li><b>Great value</b> - buy direct from Hiscox and enjoy excellent service and real value for money.</li><li><b>Money back guarantee</b> - be confident in your purchase with our 14 day guarantee.</li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.</li></ul>
										
    	    		
		 		</div>
				</div>
			<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div>
		</div>
  </div>
<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
</body>
</html>