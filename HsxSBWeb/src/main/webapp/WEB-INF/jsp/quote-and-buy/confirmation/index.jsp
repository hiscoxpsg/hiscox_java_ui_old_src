<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Thank you for choosing Hiscox Insurance | Hiscox USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print" />
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body>
<div class="main-container">
<!-- Header starts here-->
	<div class="header">
		<div class="logo" id="print_rx_logo_id">
	
    	</div>
		<form method="post">
	      <div class="header-btncontainer">
											<usd:checkLogin loginContent="logout">
						<div class="header-btns float-right"><div class="submit-button grey-button logout-icon">
								<span class='submit-button-end'><input class='submit-input' type='submit' title="Log Out" value='Log Out' name="action_Log Out_button"  /><span class="button-icon"></span></span>
						</div></div>
					</usd:checkLogin>						           </div>
		   </form>
	</div>
	<!-- Header ends here-->
	<!-- Progress bar starts here-->
			

    		    	<usd:checkPage pageContent ="coverage-options">
        <div class="progress-bar small" >
        <ul>
        	<li class="first-red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Choose Products</span></div></li>
        	<li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="about-you">
        <div class="progress-bar small" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Choose Products</span></div></li>
        	<li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="your-business">
        <div class="progress-bar small" >
        <ul>
			<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Choose Products</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="last-red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="your-quote">
        <div class="progress-bar" >
        <ul>
			<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="important-information">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="application-summary">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="payment-details">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="completed">
        <div class="progress-bar" >
        <ul>
        	<li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
    	<usd:checkPage pageContent ="none">
        <div class="progress-bar" >
        <ul>
        	<li class="first-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
        	<li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Business</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important Information</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application Summary</span></div></li>
            <li class="white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment Details</span></div></li>
        	<li class="last-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
        </ul>
        </div>
    	</usd:checkPage>
			        <!-- Page container starts here-->
	<div class="page-container">
		<div class="breadcrumb">
						    		<usd:checkEligibility eligibleContent="stateVariant,primarybusiness">	
				<div class="occupation-details">
					<span class = "float-right"><object>
							<div class="occupation-info"><span>State:</span> <a href="/small-business-insurance/quote-and-buy/change-your-business-type-and-state"><usd:include sessionValue="stateVariant" /></a></div>
							<div class="occupation-info"><span>Type of business:</span> <a href="/small-business-insurance/quote-and-buy/change-your-business-type-and-state"><usd:include sessionValue="primarybusiness" /></a></div>
					</object></span>
				</div>
    		</usd:checkEligibility>
								</div>    			
    	 		
				    		    				    	    		    				    	    		   			    		    		    		    																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																<div></div>
			<div></div>			
						<!--Left nav starts here-->
						<!--Left nav ends here--> 
			<!--Screen builder section starts here-->  
			<div class="cnt-sctn two-column-center" id="print_rx_cnt_id">
				<div class="top"><div class="right" ><div class="left"><div class="middle"></div></div></div></div>
				<div class="middle-content">
					<div class="page-heading page-heading-border">
																																									<usd:checkOccup occupContent="it-consulting" >
    	
    	<h1 class="page-heading-h1">Thank you for choosing Hiscox</h1>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="consulting" >
    	
    	<h1 class="page-heading-h1">Thank you for choosing Hiscox</h1>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<h1 class="page-heading-h1">Thank you for choosing Hiscox</h1>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<h1 class="page-heading-h1">Thank you for choosing Hiscox</h1>    	
		</usd:checkOccup>
																														<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<h1 class="page-heading-h1">Thank you for choosing Hiscox</h1>    	
		</usd:checkOccup>
														<div class="reference-number">
							<usd:checkInclude  includeContent="quoteRefID">
								<span>Reference # <usd:include sessionValue="quoteRefID"/></span>
							</usd:checkInclude>	
						</div>
						</div>
						<div class="clear"></div>
																								<div class="topcnt-container">
																																									<usd:checkOccup occupContent="it-consulting" >
    	
    	<p class="body-copy-grey">Your payment has been successfully processed and you now have a Hiscox insurance policy.</p><p class="body-copy-grey">If you requested your coverage to start today, your policy is now active. You can print this screen as confirmation of your purchase. If you specified starting at a future date, your policy will start on the requested date.</p><p class="body-copy-grey">Your insurance documents, including copies of the full policy wordings, insurance certificates, product summaries and payment details, will be emailed to you shortly.</p>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="consulting" >
    	
    	<p class="body-copy-grey">Your payment has been successfully processed and you now have a Hiscox insurance policy.</p><p class="body-copy-grey">If you requested your coverage to start today, your policy is now active. You can print this screen as confirmation of your purchase. If you specified starting at a future date, your policy will start on the requested date.</p><p class="body-copy-grey">Your insurance documents, including copies of the full policy wordings, insurance certificates, product summaries and payment details, will be emailed to you shortly.</p>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<p class="body-copy-grey">Your payment has been successfully processed and you now have a Hiscox insurance policy.</p><p class="body-copy-grey">If you requested your coverage to start today, your policy is now active. You can print this screen as confirmation of your purchase. If you specified starting at a future date, your policy will start on the requested date.</p><p class="body-copy-grey">Your insurance documents, including copies of the full policy wordings, insurance certificates, product summaries and payment details, will be emailed to you shortly.</p>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<p class="body-copy-grey">Your payment has been successfully processed and you now have a Hiscox insurance policy.</p><p class="body-copy-grey">If you requested your coverage to start today, your policy is now active. You can print this screen as confirmation of your purchase. If you specified starting at a future date, your policy will start on the requested date.</p><p class="body-copy-grey">Your insurance documents, including copies of the full policy wordings, insurance certificates, product summaries and payment details, will be emailed to you shortly.</p>    	
		</usd:checkOccup>
																														<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<p class="body-copy-grey">Your payment has been successfully processed and you now have a Hiscox insurance policy.</p><p class="body-copy-grey">If you requested your coverage to start today, your policy is now active. You can print this screen as confirmation of your purchase. If you specified starting at a future date, your policy will start on the requested date.</p><p class="body-copy-grey">Your insurance documents, including copies of the full policy wordings, insurance certificates, product summaries and payment details, will be emailed to you shortly.</p>    	
		</usd:checkOccup>
									<div class="horz-line2"></div>
													</div>
												<usd:verifyToInclude>
						<usd:checkFormTag formContent="Start"></usd:checkFormTag>
						<usd:questionTag />	
																																						<usd:checkOccup occupContent="it-consulting" >
    	<div class='topcnt-container'>
    	<h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">We want to make sure you’re confident you've made the right choice. We give you 14 days to review your policy and if you’re not satisfied for any reason and have not had a claim or loss, you can cancel your policy and receive a full refund.</p><p class="body-copy-grey">Thank you again for choosing Hiscox to help protect your business.</p><p class="body-copy-grey">Click to return to the <a href="/small-business-insurance/" title="">Hiscox home page</a>.</p>    	</div>
		</usd:checkOccup>
																											<usd:checkOccup occupContent="consulting" >
    	<div class='topcnt-container'>
    	<h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">We want to make sure you’re confident you've made the right choice. We give you 14 days to review your policy and if you’re not satisfied for any reason and have not had a claim or loss, you can cancel your policy and receive a full refund.</p><p class="body-copy-grey">Thank you again for choosing Hiscox to help protect your business.</p><p class="body-copy-grey">Click to return to the <a href="/small-business-insurance/" title="">Hiscox home page</a>.</p>    	</div>
		</usd:checkOccup>
																											<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='topcnt-container'>
    	<h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">We want to make sure you’re confident you've made the right choice. We give you 14 days to review your policy and if you’re not satisfied for any reason and have not had a claim or loss, you can cancel your policy and receive a full refund.</p><p class="body-copy-grey">Thank you again for choosing Hiscox to help protect your business.</p><p class="body-copy-grey">Click to return to the <a href="/small-business-insurance/" title="">Hiscox home page</a>.</p>    	</div>
		</usd:checkOccup>
																											<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='topcnt-container'>
    	<h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">We want to make sure you’re confident you've made the right choice. We give you 14 days to review your policy and if you’re not satisfied for any reason and have not had a claim or loss, you can cancel your policy and receive a full refund.</p><p class="body-copy-grey">Thank you again for choosing Hiscox to help protect your business.</p><p class="body-copy-grey">Click to return to the <a href="/small-business-insurance/" title="">Hiscox home page</a>.</p>    	</div>
		</usd:checkOccup>
																													<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='topcnt-container'>
    	<h2 class="grey-heading-h2"><b>Money back guarantee</b></h2><p class="body-copy-grey">We want to make sure you’re confident you've made the right choice. We give you 14 days to review your policy and if you’re not satisfied for any reason and have not had a claim or loss, you can cancel your policy and receive a full refund.</p><p class="body-copy-grey">Thank you again for choosing Hiscox to help protect your business.</p><p class="body-copy-grey">Click to return to the <a href="/small-business-insurance/" title="">Hiscox home page</a>.</p>    	</div>
		</usd:checkOccup>
																			<usd:actionTag />
						<usd:checkFormTag formContent="End"></usd:checkFormTag>	
						</usd:verifyToInclude>
						<usd:trackingTag />
												        	<div class="btmbtn-cntnr"> 
        		        		            		                            		        		        		<div class="clear-both"></div>
        	</div>
		</div>
	<div class="bottom clear-both" ><div class="right"><div class="left"><div class="middle" ></div></div></div></div> 
	</div> 
				<!--Screen builder section ends here--> 
				<!--Right nav starts here-->   
				<div class="right-nav showcntct-num">
					<!--Checklist section starts here-->   
					<usd:checkOccup occupContent="it-consulting" > 
							                                         									            		            		            		            		            									            		            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        										<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" >
							                                         		            									            		            		            		            		            									            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        										<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                         		            		            									            		            		            		            		            									            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        										<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                         		            		            		            									            		            		            		            		            									            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		        			        										<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
        			             							            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                         		            		            		            		            									            		            		            		            		            										            										    										    										    										            										            																						<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			 			            										    										    										    										    																																        					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                         									            		            		            		            		            									            		            		            		            			            																		<div class="section-container right-nav-corner btmright-cntainr-large " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Still have some questions?</b></h3><img id="redline_quoteBuy_right" src="/shared-images/redline_quoteBuy_right.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey">We want you to be 100% confident that you understand the coverage you have purchased. If you have any other questions please contact one of our licensed advisors.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                         		            									            		            		            		            		            									            		            		            			            																		<div class="section-container right-nav-corner btmright-cntainr-large " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Still have some questions?</b></h3><img id="redline_quoteBuy_right" src="/shared-images/redline_quoteBuy_right.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey">We want you to be 100% confident that you understand the coverage you have purchased. If you have any other questions please contact one of our licensed advisors.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                         		            		            									            		            		            		            		            									            		            			            																		<div class="section-container right-nav-corner btmright-cntainr-large " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Still have some questions?</b></h3><img id="redline_quoteBuy_right" src="/shared-images/redline_quoteBuy_right.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey">We want you to be 100% confident that you understand the coverage you have purchased. If you have any other questions please contact one of our licensed advisors.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                         		            		            		            									            		            		            		            		            									            			            																		<div class="section-container right-nav-corner btmright-cntainr-large " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Still have some questions?</b></h3><img id="redline_quoteBuy_right" src="/shared-images/redline_quoteBuy_right.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey">We want you to be 100% confident that you understand the coverage you have purchased. If you have any other questions please contact one of our licensed advisors.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                         		            		            		            		            									            		            		            		            		            										            																		<div class="section-container right-nav-corner btmright-cntainr-large " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Still have some questions?</b></h3><img id="redline_quoteBuy_right" src="/shared-images/redline_quoteBuy_right.png" alt="" title="" border="0" height="2" width="191" /><p class="body-copy-grey">We want you to be 100% confident that you understand the coverage you have purchased. If you have any other questions please contact one of our licensed advisors.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    										    										    										            										            										            										    										    										    										    																																        					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                         									            		            		            		            		            									            		            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                         		            									            		            		            		            		            									            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                         		            		            									            		            		            		            		            									            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                         		            		            		            									            		            		            		            		            									            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                         		            		            		            		            									            		            		            		            		            										            										    										    										    										            										            										            										    										    										    										    																																        					</usd:checkOccup>					
					<div class="verLogo-holder">
<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications.">
<tr>
<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hiscoxusa.com&size=L&use_flash=YES&use_transparent=YES&lang=en"></script><br />
<a href="http://www.verisign.com/ssl-certificate/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>
</tr>
</table>
					</div>
				</div>
				<!--Right nav ends here--> 
				<!-- Page container ends here-->
		<div class="boiler-plate">
			 <div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>    
				<div class="middle-content"> 
					<div class="middlecnt-container">
							
	
	
    	</div>
				</div>
			<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div>
		</div>
		<!-- Start of new class for twocol page - confirmation page) !-->
						<!-- End of new class for twocol page - confirmation page) !-->
    	<div class="footer-twocol" id="print_rx_footer_id">
    		
	
    							
																<div class="footer-links">
                      <a href="/accessibility/" title="" target="_blank" onclick="return popup(this)">
						Accessibility</a>
                    </div>
										
    				
																<div class="footer-links">
                      <a href="/terms-of-use/" title="" target="_blank" onclick="return popup(this)">
						Terms of use</a>
                    </div>
										
    				
																<div class="footer-links">
                      <a href="/privacy-policy/" title="" target="_blank" onclick="return popup(this)">
						Privacy policy</a>
                    </div>
										
    				
																<div class="footer-links">
                      <a href="/legal-notices/" title="" target="_blank" onclick="return popup(this)">
						Legal notices</a>
                    </div>
										
    	    		
		 		    		<div class="copy-right">
    			
	
    	    		</div>
    	</div>
	</div>  
</div>
			<div id="stElement"></div>
		<script type="text/javascript">
 			var SWFfilename = 'st-transport.swf';
 			var AccountCode = 'HiscoxUS-1';
		</script>
		<script type="text/javascript" src="/resources/javascript/servicetickbase.js"></script>
	<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
<usd:invalidateSessionTag/>

		<div style="width:100%;align:center">
			<!-- Google Code for Sales Conversion Page --> <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1016710726;
var google_conversion_language = "en";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "4GBpCPq9qAIQxozn5AM"; var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript"
src="https://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""
src="https://www.googleadservices.com/pagead/conversion/1016710726/?label=4GBpCPq9qAIQxozn5AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
		</div>
	
		<div style="width:100%;align:center">
			<img src="https://www.shmktpl.com/Outcome/?O=388" width="1" height="1" alt="" />		</div>
	</body>
</html>