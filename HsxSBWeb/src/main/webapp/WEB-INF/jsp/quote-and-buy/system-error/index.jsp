<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page isErrorPage="true" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>System error</title>
<link href="/resources/css/global-reset.css" rel="stylesheet"
	type="text/css" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet"
	type="text/css" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css"
	type="text/css" />
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->

<!--[if IE]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body>
<div class="main-container"><!-- Header starts here-->
<div class="header">
<div class="logo"><img src="/resources/images/logo.gif" alt=""
	title="Logo" /></div>
<form method="post">
<div class="header-btncontainer">

<div class='header-btns float-right'>
<div class="submit-button grey-button save-icon header-button-gap"><span
	class='submit-button-end'><input class='submit-input'
	type='submit' title="Retrieve Quote" value='Retrieve a quote' /><span
	class="button-icon"></span></span></div>
</div>
</div>

</form>
</div>
<!-- Header ends here--> <!-- Progress bar starts here-->










<div class="progress-bar">
<ul>
	<li class="first-white-bar">
	<div class="left-bg"></div>
	<div class="middle-bg"><span>About You</span></div>
	</li>
	<li class="white-bar">
	<div class="left-bg"></div>
	<div class="middle-bg"><span>Your business</span></div>
	</li>

	<li class="white-bar">
	<div class="left-bg"></div>
	<div class="middle-bg"><span>Your Quote</span></div>
	</li>
	<li class="white-bar">
	<div class="left-bg"></div>
	<div class="middle-bg"><span>Important information</span></div>
	</li>
	<li class="white-bar">
	<div class="left-bg"></div>
	<div class="middle-bg"><span>Application summary</span></div>
	</li>
	<li class="white-bar">
	<div class="left-bg"></div>
	<div class="middle-bg"><span>Payment details</span></div>
	</li>
	<li class="last-white-bar">
	<div class="left-bg"></div>
	<div class="middle-bg"><span>Completed</span></div>
	<div class="right-bg-last"></div>
	</li>
</ul>

</div>

<!-- Page container starts here-->
<div class="page-container">
<div class="breadcrumb"></div>

<div></div>
<div></div>
<!--Left nav starts here-->

<div class="left-nav">

<div class="section-container left-nav-corner grey-toplft">

<div class="top">
<div class="right">
<div class="left">
<div class="middle"></div>
</div>
</div>
</div>
<div class="middle-content vertical-container">

<h3 class="black-heading-h3"><b>Helpful Information</b></h3>
<p class="body-copy-black-small">Click the <span class="icon-help">
</span>icon where shown, it will display useful and helpful information about
the question</p>
<p class="grey-call-to-action-medium"><b>Why choose Hiscox
Insurance?</b></p>
<ul class="list-red-tick-body-copy-black-small">
	<li>
	<p class="body-text"><a href="http://"><b>14 day guarantee</b></a>
	<span class="body-text">If for any reason you want to cancel we
	will refund the premium paid in full.</span></p>
	</li>
	<li><a href="http://"><b>Customized coverage</b></a> <span
		class="body-text">If for any reason you want to cancel we will
	refund the premium paid in full.</span></li>
	<li><a href="http://"><b>Financial strength</b></a> <span
		class="body-text">If for any reason you want to cancel we will
	refund the premium paid in full.</span></li>
</ul>
</div>

<div class="bottom clear-both">
<div class="right">
<div class="left">
<div class="middle"></div>
</div>
</div>
</div>
</div>


</div>

<!--Left nav ends here--> <!--Screen builder section starts here-->
<div class="cnt-sctn ">
<div class="top">
<div class="right">
<div class="left">
<div class="middle"></div>
</div>
</div>
</div>
<div class="middle-content">
<div class="page-heading page-heading-border">
<h1 class="page-heading-h1">Sorry, an error has occurred.</h1>
<div class="reference-number"></div>
</div>
<div class="clear"></div>
<div class="topcnt-container">
<p class="body-copy-grey">We have been unable to provide you with a
quote at this time. This is due to a system error.</p>
<p class="body-copy-grey">Please call us on 1-800-841-5555 (Monday
to Fridat, 8am - 10pm) to get a quote over the phone or if you have any
queries you would like to discuss.</p>
<p class="body-copy-grey">We apologise for any inconvenience caused.</p>
</div>
<div class="btmbtn-cntnr">
<div class="header-btns float-right">
<div class="submit-button red-button close-icon"><span
	class="submit-button-end"><a class='submit-input'
	href='http://www.hiscoxusa.com/' title="Close" target="_self">Close</a><span
	class="button-icon"></span></span></div>
</div>

<div class="clear-both"></div>
</div>
</div>
<div class="bottom clear-both">
<div class="right">
<div class="left">
<div class="middle"></div>
</div>
</div>
</div>
</div>
<!--Screen builder section ends here--> <!--Right nav starts here-->
<div class="right-nav"><!--Checklist section starts here-->
<div class="section-container right-nav-corner tpright-container-large">
<div class="top">
<div class="right">
<div class="left">
<div class="middle"></div>
</div>
</div>
</div>
<div class="middle-content vertical-container">

<div class="contact-child-item">

<div>
<p class="grey-call-to-action-large"><b>(800) 841 2222</b></p>
<p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p>
</div>
</div>
<div class="contact-child-item">
<div><img id="contact-img-text"
	src="/resources/images/contact-img-text-2.gif" alt="contact-img-text"
	title="contact-img-text" border="0" height="103" width="190" /></div>
</div>

<div class="contact-child-item">
<div>
<p class="contact-box-link"><a
	href="/small-business-insurance/quote-and-buy/let-us-call-you"><span
	class="icon-phone">Let us call you</span></a></p>
</div>
</div>
<div class="contact-child-item">
<div>
<p class="contact-box-link"><a href="http://www.google.com"><span
	class="icon-mouse">Chat online now</span></a></p>
</div>

</div>
<div class="contact-child-item-last">
<div>
<p class="contact-box-link"><a href="http://www.google.com"><span
	class="icon-email">Email</span></a></p>
</div>
</div>
</div>
<div class="bottom clear-both">
<div class="right">
<div class="left">
<div class="middle"></div>
</div>
</div>
</div>
</div>













<div class="verLogo-holder"><img
	src="/resources/images/secured-icon.gif" alt="" title="Verisign" /></div>

<!--Checklist section ends here--></div>
<!--Right nav ends here--> <!-- Page container ends here-->
<div class="boiler-plate"></div>
<!-- Start of new class for twocol page - confirmation page) !--> <!-- End of new class for twocol page - confirmation page) !-->
<div class="footer">



<div class="footer-links"><a
	href="/small-business-insurance/quote-and-buy/terms-and-conditions/"
	title=""> Terms and conditions</a></div>


<div class="footer-links"><a
	href="/small-business-insurance/quote-and-buy/privacy-policy/" title="">
Privacy policy</a></div>


<div class="footer-links"><a
	href="/small-business-insurance/quote-and-buy/accessibility/" title="">
Accessibility</a></div>


<div class="copy-right">




<p class="body-copy-grey">©2010 Hiscox Limited</p>


</div>
</div>
</div>

</div>

</body>
</html>
