package com.hiscox.sbweb.broker;


import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBWebProgressBarServiceBrokerTest {
	private HsxSBWebProgressBarServiceBroker hsxSBWebProgressBarServiceBroker;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;

	@Before
	public void setUp() throws Exception {
		hsxSBWebProgressBarServiceBroker = new HsxSBWebProgressBarServiceBroker();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		webBean.setRequestType("payment");
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		webBean = null;
		hsxSBWebProgressBarServiceBroker = null;
		hsxSBWebResourceManager = null;
		mockery = null;
	}

	@Test
	public void testInvokeGetPage() throws HsxCoreException {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
			}
			
		});
		
		Object result = hsxSBWebProgressBarServiceBroker.invokeGetPage();
		Assert.assertNotNull(result);
	}

}
