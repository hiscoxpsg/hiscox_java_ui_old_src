package com.hiscox.sbweb.broker;

import java.util.Properties;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.core.vo.HsxCoreRequest;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBWebTextManagerServiceBrokerTest {
	
	private HsxSBWebTextManagerServiceBroker hsxSBWebTextManagerServiceBroker;
	private HsxCoreRequest request;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Properties properties;
	@Before
	public void setUp() throws Exception {
		hsxSBWebTextManagerServiceBroker = new HsxSBWebTextManagerServiceBroker();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		properties = new Properties();
		request = new HsxCoreRequest();
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBWebResourceManager = null;
		hsxSBWebTextManagerServiceBroker = null;
		mockery = null;
		properties = null;
		request = null;
		
	}

	@Test
	public void testServiceBinder() {
		
		hsxSBWebTextManagerServiceBroker.setServiceBinder(request);
		Assert.assertNotNull(request);
	}

	
	@Test
		public void testCreateTxtMgrRequestXml() {
		mockery.checking(new Expectations(){
			{
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
			}
			
		});
		Object result =	hsxSBWebTextManagerServiceBroker.createTxtMgrRequestXml();
		Assert.assertNotNull(result);
		}
}
