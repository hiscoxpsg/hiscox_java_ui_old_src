package com.hiscox.sbweb.broker;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.vo.HsxCoreRequest;


import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class HsxSBGetPageServiceBrokerTest {
	private HsxSBGetPageServiceBroker hsxSBGetPageServiceBroker;
	private HsxCoreRequest request;

	@Before
	public void setUp() throws Exception {
		hsxSBGetPageServiceBroker = new HsxSBGetPageServiceBroker();
		request = new HsxCoreRequest();
	}

	@After
	public void tearDown() throws Exception {
		hsxSBGetPageServiceBroker = null;
		request = null;
	}

	@Test
	public void testServiceBinder() {
		hsxSBGetPageServiceBroker.setServiceBinder(request);
		Assert.assertNotNull(request);
	}
	
	
	@Test
	public void testCreateGetPageXML() throws ParserConfigurationException {
		Document document = hsxSBGetPageServiceBroker.createGetPageXML();
		Assert.assertNotNull(document);
		
	}
	
	@Test
		public void tesWithResponseXMlNull() throws ParserConfigurationException, 
		SAXException, IOException, HsxCoreException {
			Object result = hsxSBGetPageServiceBroker.checkForGetPageServiceExceptions(null, null);
			Assert.assertNull(result);
			
		}

}
