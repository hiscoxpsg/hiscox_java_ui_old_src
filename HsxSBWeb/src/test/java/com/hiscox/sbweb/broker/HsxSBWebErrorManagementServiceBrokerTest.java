package com.hiscox.sbweb.broker;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.core.vo.HsxCoreRequest;

public class HsxSBWebErrorManagementServiceBrokerTest {
	
	private HsxSBWebErrorManagementServiceBroker hsxSBWebErrorManagementServiceBroker;
	private HsxCoreRequest request;
	

	@Before
	public void setUp() throws Exception {
		hsxSBWebErrorManagementServiceBroker = new HsxSBWebErrorManagementServiceBroker();
		request = new HsxCoreRequest();
	}

	@After
	public void tearDown() throws Exception {
		hsxSBWebErrorManagementServiceBroker = null;
		request = null;
		
	}

	@Test
	public void testServiceBinder() {
		
		hsxSBWebErrorManagementServiceBroker.setServiceBinder(request);
		Assert.assertNotNull(request);
	}

}
