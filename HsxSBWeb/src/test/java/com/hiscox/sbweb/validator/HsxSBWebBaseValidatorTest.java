package com.hiscox.sbweb.validator;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBWebBaseValidatorTest {
	
	private HsxSBWebBaseValidator hsxSBWebBaseValidator;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Errors errors;
	private HsxSBScreenWidget hsxSBScreenWidget;
	private HashMap<String, HsxSBWidget> dynamicQuestionMap;
	private Properties validationHintProperties;
	private HsxSBWidgetInfo hsxSBWidgetInfo;  
	private Properties properties;
	@Before
	public void setUp() throws Exception {
		hsxSBWebBaseValidator = new HsxSBWebBaseValidator();
		webBean = new HsxSBWebBean();
		dynamicQuestionMap = new HashMap<String, HsxSBWidget>();
		hsxSBScreenWidget = new HsxSBScreenWidget();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		properties = new Properties();
		validationHintProperties = new Properties();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setSavedValue("test");
		hsxSBWidgetInfo.setSubType("test");
		hsxSBWidgetInfo.setType("test");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setErrorStyle("test");
		webBean.setCheckBoxValidationFlag("Yes");
		hsxSBScreenWidget.setWidgetInfo(hsxSBWidgetInfo);
		webBean.setWidget(hsxSBScreenWidget);
		dynamicQuestionMap.put("", hsxSBScreenWidget);
		errors = new Errors() {
			
			@Override
			public void setNestedPath(String nestedPath) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void rejectValue(String field, String errorCode, Object[] errorArgs,
					String defaultMessage) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void rejectValue(String field, String errorCode,
					String defaultMessage) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void rejectValue(String field, String errorCode) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void reject(String errorCode, Object[] errorArgs,
					String defaultMessage) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void reject(String errorCode, String defaultMessage) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void reject(String errorCode) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void pushNestedPath(String subPath) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void popNestedPath() throws IllegalStateException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public boolean hasGlobalErrors() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean hasFieldErrors(String field) {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean hasFieldErrors() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean hasErrors() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public String getObjectName() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getNestedPath() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List getGlobalErrors() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getGlobalErrorCount() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public ObjectError getGlobalError() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getFieldValue(String field) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Class getFieldType(String field) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List getFieldErrors(String field) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public List getFieldErrors() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getFieldErrorCount(String field) {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public int getFieldErrorCount() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public FieldError getFieldError(String field) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public FieldError getFieldError() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getErrorCount() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public List getAllErrors() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public void addAllErrors(Errors errors) {
				// TODO Auto-generated method stub
				
			}
		};

	}

	@After
	public void tearDown() throws Exception {
		context = null;
		dynamicQuestionMap = null;
		errors = null;
		hsxSBScreenWidget = null;
		hsxSBWebBaseValidator = null;
		hsxSBWebResourceManager = null;
		hsxSBWidgetInfo = null;
		mockery = null;
		properties = null;
		validationHintProperties = null;
		webBean = null;
	}

	@Test
	public void testValidateScreenQuestions() throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {
		mockery.checking(new Expectations(){
			{
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(validationHintProperties));
			}
			
		});
		hsxSBWebBaseValidator.validateScreenQuestions(webBean, errors, dynamicQuestionMap);
		Assert.assertNotNull(hsxSBWebBaseValidator);
	}
	
	@Test
	public void testSupports(){
		hsxSBWebBaseValidator.supports(HsxSBWebBaseValidator.class);
		Assert.assertNotNull(hsxSBWebBaseValidator);
	}
	
	@Test
	public void testValidate(){
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
			
		});
		hsxSBWebBaseValidator.validate(null, errors);
		Assert.assertNotNull(hsxSBWebBaseValidator);
	}
	
	@Test
	public void testGetValidationHintValue(){
		String result = hsxSBWebBaseValidator.getValidationHintValue("test");
		Assert.assertEquals("", result);
	}
}
