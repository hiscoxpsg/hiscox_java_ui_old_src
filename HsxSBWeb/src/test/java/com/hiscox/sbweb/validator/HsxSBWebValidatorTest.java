package com.hiscox.sbweb.validator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.Errors;

import com.hiscox.sbweb.bean.HsxSBWebBean;

public class HsxSBWebValidatorTest {
	private HsxSBWebValidator hsxSBWebValidator;
	private HsxSBWebBean hsxSBWebBean;
	private Errors errors;
	@Before
	public void setUp() throws Exception {
		hsxSBWebValidator = new HsxSBWebValidator();
		hsxSBWebBean = new HsxSBWebBean();
	}

	@After
	public void tearDown() throws Exception {
		hsxSBWebBean = null;
		hsxSBWebValidator = null;
	}

	@Test
	public void testWithQuestionMapNull() {
		hsxSBWebValidator.validate(hsxSBWebBean, errors);
		Assert.assertNull(hsxSBWebBean.getWidgetMap());
	}

}
