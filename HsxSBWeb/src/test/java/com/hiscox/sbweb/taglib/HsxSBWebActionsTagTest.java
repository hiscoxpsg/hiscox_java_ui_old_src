package com.hiscox.sbweb.taglib;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.el.ELContext;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.el.ExpressionEvaluator;
import javax.servlet.jsp.el.VariableResolver;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;
@Ignore
/**
 * This Junit is incomplete becuase we can't set
 * getOut() method.Its implicit object defined by JSP.
 *
 */
public class HsxSBWebActionsTagTest {
	
	private HsxSBWebActionsTag hsxSBWebActionsTag;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Properties properties;
	private PageContext pageContext;
	JspWriter out = null;
	
	

	@Before
	public void setUp() throws Exception {
			hsxSBWebActionsTag = new HsxSBWebActionsTag();
			webBean = new HsxSBWebBean();
			mockery = new Mockery();
			context=mockery.mock(ApplicationContext.class);
			hsxSBWebResourceManager.setContext(context);
			properties = new Properties();
			
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws JspException {
		//JspWriter out = super.pageContext.getOut(); 
		
		pageContext = new PageContext() {
			
			@Override
			public void setAttribute(String arg0, Object arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setAttribute(String arg0, Object arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void removeAttribute(String arg0, int arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void removeAttribute(String arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public VariableResolver getVariableResolver() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public JspWriter getOut() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ExpressionEvaluator getExpressionEvaluator() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ELContext getELContext() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getAttributesScope(String arg0) {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Enumeration<String> getAttributeNamesInScope(int arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getAttribute(String arg0, int arg1) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getAttribute(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object findAttribute(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public void release() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void initialize(Servlet arg0, ServletRequest arg1,
					ServletResponse arg2, String arg3, boolean arg4, int arg5,
					boolean arg6) throws IOException, IllegalStateException,
					IllegalArgumentException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void include(String arg0, boolean arg1) throws ServletException,
					IOException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void include(String arg0) throws ServletException, IOException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void handlePageException(Throwable arg0) throws ServletException,
					IOException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void handlePageException(Exception arg0) throws ServletException,
					IOException {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public HttpSession getSession() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ServletContext getServletContext() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ServletConfig getServletConfig() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ServletResponse getResponse() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ServletRequest getRequest() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getPage() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Exception getException() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public void forward(String arg0) throws ServletException, IOException {
				// TODO Auto-generated method stub
				
			}
		};
		
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
				
			}
			
		});
		hsxSBWebActionsTag.setPageContext(pageContext);
		hsxSBWebActionsTag.doStartTag();
		


	}
	
}
