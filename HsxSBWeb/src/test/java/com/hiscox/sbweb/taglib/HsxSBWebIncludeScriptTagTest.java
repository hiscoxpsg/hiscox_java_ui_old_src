package com.hiscox.sbweb.taglib;

import javax.servlet.jsp.JspException;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBWebIncludeScriptTagTest {
	
	private HsxSBWebIncludeScriptTag hsxSBWebIncludeScriptTag;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;

	@Before
	public void setUp() throws Exception {
		hsxSBWebIncludeScriptTag = new HsxSBWebIncludeScriptTag();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBWebIncludeScriptTag = null;
		hsxSBWebResourceManager = null;
		mockery = null;
		webBean = null;
	}

	@Test
	public void testIncludeScript() throws JspException {
		String scriptFileName = "focus.js";
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
			}
			
		});
		Object result =	hsxSBWebIncludeScriptTag.includescript(scriptFileName);
		Assert.assertNotNull(result);
		
	}
	@Test
	public void testIncludeNonJSPaymentDetailsIframe() throws JspException {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
			}
			
		});
		Object result =	hsxSBWebIncludeScriptTag.includeNonJSPaymentDetailsIframe();;
		Assert.assertNotNull(result);
		
	}
	
	@Test
	public void testIncludeNonJSPaymentDetails() throws JspException {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
			}
			
		});
		Object result =	hsxSBWebIncludeScriptTag.includeNonJSPaymentDetails();
		Assert.assertNotNull(result);
		
	}
	
	@Test
	public void testIncludeNonJSConfirmation() throws JspException {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
			}
			
		});
		Object result =	hsxSBWebIncludeScriptTag.includeNonJSConfirmation();
		Assert.assertNotNull(result);
		
	}
	
	@Test
	public void testDoEndTag() throws JspException {
		Object result = hsxSBWebIncludeScriptTag.doEndTag();
		Assert.assertNotNull(result);
	}
}
