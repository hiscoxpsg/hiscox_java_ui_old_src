package com.hiscox.sbweb.taglib;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBWebCheckOccupationTagTest {
	private HsxSBWebCheckOccupationTag hsxSBWebCheckOccupationTag;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Map<String, String> sessionMap;

	

	@Before
	public void setUp() throws Exception {
		hsxSBWebCheckOccupationTag = new HsxSBWebCheckOccupationTag();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		sessionMap = new HashMap<String, String>();
		sessionMap.put("test", "test");
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBWebCheckOccupationTag = null;
		hsxSBWebResourceManager = null;
		mockery = null;
		sessionMap = null;
		webBean = null;
	}

	@Ignore
	public void test() throws JspException {
		webBean.setSessionMap(sessionMap);
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
			}
			
		});
		
		Object result = hsxSBWebCheckOccupationTag.doStartTag();
		Assert.assertNotNull(result);
	}
		
	@Test
	public void testDoEndTag() throws JspException {
		Object result = hsxSBWebCheckOccupationTag.doEndTag();
		Assert.assertNotNull(result);
	}
	

}
