package com.hiscox.sbweb.taglib;

import javax.servlet.jsp.JspException;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBWebCheckPageTagTest {

	private HsxSBWebCheckPageTag hsxSBWebCheckPageTag;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	
	@Before
	public void setUp() throws Exception {
		hsxSBWebCheckPageTag= new HsxSBWebCheckPageTag();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDoStartTag() throws JspException {
		webBean.setPageBarItem("");
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
			}
			
		});
		
		Object result = hsxSBWebCheckPageTag.doStartTag();
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testDoEndTag() throws JspException {
		Object result = hsxSBWebCheckPageTag.doEndTag();
		Assert.assertNotNull(result);
	}
}
