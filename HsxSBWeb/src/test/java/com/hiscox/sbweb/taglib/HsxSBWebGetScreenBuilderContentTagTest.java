package com.hiscox.sbweb.taglib;

import java.util.Properties;

import javax.servlet.jsp.JspException;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBWebGetScreenBuilderContentTagTest {
	
	private HsxSBWebGetScreenBuilderContentTag hsxSBWebGetScreenBuilderContentTag;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Properties properties;
	

	@Before
	public void setUp() throws Exception {
		hsxSBWebGetScreenBuilderContentTag = new HsxSBWebGetScreenBuilderContentTag();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		properties = new Properties();
		
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBWebGetScreenBuilderContentTag = null;
		hsxSBWebResourceManager = null;
		mockery = null;
		properties = null;
		webBean = null;
				
	}

	@Test
	public void testDoStartTagWithError() throws JspException {
		webBean.setPageType("static-error");
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
			}
			
		});
		Object result =	hsxSBWebGetScreenBuilderContentTag.doStartTag();
		Assert.assertNotNull(result);

	}
	
	@Test
	public void testDoStartTagWithPageValidationFail() throws JspException{
		webBean.setPageType("popup");
		webBean.setFolder("payment");
		webBean.setPageValidationFailedFlag(true);
		
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
			}
			
		});
		Object result =	hsxSBWebGetScreenBuilderContentTag.doStartTag();
		Assert.assertNotNull(result);

	}
	
	@Test
	public void testDoStartWithPageValidationPass() throws JspException{
		webBean.setPageType("popup");
		webBean.setFolder("payment");
		
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
			}
			
		});
		Object result =	hsxSBWebGetScreenBuilderContentTag.doStartTag();
		Assert.assertNotNull(result);

	}
	
	@Test
	public void testDoEndTag() throws JspException {
		Object result = hsxSBWebGetScreenBuilderContentTag.doEndTag();
		Assert.assertNotNull(result);
	}


}
