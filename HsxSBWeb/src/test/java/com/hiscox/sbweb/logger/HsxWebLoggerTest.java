package com.hiscox.sbweb.logger;

import java.util.Properties;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxWebLoggerTest {
	
	private HsxWebLogger hsxWebLogger;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Properties properties;
	private String packageName="SBWEB";
	private String message="dummy message for JUnit";
	
	@Before
	public void setUp() throws Exception {
		hsxWebLogger = new HsxWebLogger("");
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		properties = new Properties();
		
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBWebResourceManager = null;
		hsxWebLogger = null;
		message = null;
		mockery = null;
		packageName = null;
		properties = null;
		
	}

	@Test
	public void testDebug() {
		properties.setProperty("SBWEB.DEBUG", "Yes");
		String methodName="DEBUG";
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
			
		});
		hsxWebLogger.debug(packageName, methodName, message);
		Assert.assertTrue(hsxWebLogger.loggerFlag);
	}
	
	@Test
	public void testError() {
		properties.setProperty("SBWEB.ERROR", "Yes");
		String methodName="ERROR";
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
			
		});
		hsxWebLogger.error(packageName, methodName, message);
		Assert.assertTrue(hsxWebLogger.loggerFlag);
	}

	@Test
	public void testInfo() {
		properties.setProperty("SBWEB.ERROR", "Yes");
		String methodName="ERROR";
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
			
		});
		hsxWebLogger.info(packageName, methodName, message);
		Assert.assertTrue(hsxWebLogger.loggerFlag);
	}
}
