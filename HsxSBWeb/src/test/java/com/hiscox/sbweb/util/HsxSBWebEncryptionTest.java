package com.hiscox.sbweb.util;

import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.hiscox.core.exception.HsxCoreException;
@Ignore
/**
 * This Junit is incomplete because we can't mock the 
 * Properties.
 * Mocking is allowed only for Interface.
 */
public class HsxSBWebEncryptionTest {
	
	private HsxSBWebEncryption hsxSBWebEncryption;
	private Properties appConfigProperties;

	@Before
	public void setUp() throws Exception {
		hsxSBWebEncryption = new HsxSBWebEncryption();
		appConfigProperties = new Properties();
		appConfigProperties.setProperty("STR_USER_DN", "");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws HsxCoreException {
		String componentName="CARDPAYMENT-US-DIRECTCOMMERCIAL";
		hsxSBWebEncryption.getKey(componentName);
	}

}
