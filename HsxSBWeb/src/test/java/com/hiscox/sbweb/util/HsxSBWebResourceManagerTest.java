package com.hiscox.sbweb.util;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

public class HsxSBWebResourceManagerTest {
	
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private String name = "webBean";
	private ApplicationContext context;
	private Mockery mockery;

	@Before
	public void setUp() throws Exception {
		hsxSBWebResourceManager = new HsxSBWebResourceManager();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBWebResourceManager = null;
		mockery = null;
		name = null;
		
	}

	@Test
	public void test() {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(name));
			}
			
		});
		Object result = hsxSBWebResourceManager.getResource(name);
		Assert.assertNotNull(result);
	}

}
