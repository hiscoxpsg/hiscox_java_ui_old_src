package com.hiscox.sbweb.util;

import java.util.Properties;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

public class HsxSBWebSessionManagerTest {
	private HsxSBWebSessionManager hsxSBWebSessionManager;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Properties properties;

	@Before
	public void setUp() throws Exception {
		hsxSBWebSessionManager = new HsxSBWebSessionManager();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		properties = new Properties();
		
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBWebResourceManager = null; 
		hsxSBWebSessionManager = null;
		mockery = null;
		properties = null;
	}

	@Test
	public void test() {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
			
		});
		hsxSBWebSessionManager.getInstance();
		Assert.assertNotNull(properties);
	}

}
