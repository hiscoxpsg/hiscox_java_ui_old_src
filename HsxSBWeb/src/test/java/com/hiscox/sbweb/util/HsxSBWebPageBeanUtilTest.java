package com.hiscox.sbweb.util;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.acegisecurity.AuthenticationException;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.w3c.dom.Document;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sbweb.bean.HsxSBWebBean;

public class HsxSBWebPageBeanUtilTest {
	private HsxSBWebPageBeanUtil hsxSBWebPageBeanUtil;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Properties properties;
	private HttpSession session;
	
	

	@Before
	public void setUp() throws Exception {
		hsxSBWebPageBeanUtil = new HsxSBWebPageBeanUtil();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		properties = new Properties();
		session=mockery.mock(HttpSession.class);
		
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBWebPageBeanUtil = null;
		hsxSBWebResourceManager = null;
		mockery = null;
		properties = null;
		session = null;
		webBean = null;
		
	}

	@Test
	public void testGetStringFromDocument() {
		Document doc = null;
		Object result = hsxSBWebPageBeanUtil.getStringFromDocument(doc);
		Assert.assertNotNull(result);
		
	}
	
	@Test
	public void testPrintXmlData() {
		Document doc = null;
		hsxSBWebPageBeanUtil.printXmlData(doc);
		Assert.assertNotNull(hsxSBWebPageBeanUtil); 
		
	}
	
	@Test
	public void testEncode() {
		String strToEncode="<?xml version=\"1.0\"?>";
		Object result = hsxSBWebPageBeanUtil.encode(strToEncode);
		Assert.assertNotNull(result); 
		
	}
	@Test
	public void testDecode() {
		String strToDecode="&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;";
		Object result = hsxSBWebPageBeanUtil.decode(strToDecode);
		Assert.assertNotNull(result); 
		
	}
	
	@Test
	public void testUpdateHistory(){
		String pageName ="paymentDetails";
		String pageHistory = "applicationSummary";
		Object result = hsxSBWebPageBeanUtil.updatePageHistory(pageName, pageHistory);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testGetDocumentFromString(){
		String xmlSource="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"+
                "<Emp id=\"1\"><name>Pankaj</name><age>25</age>\n"+
                "<role>Developer</role><gen>Male</gen></Emp>";
		hsxSBWebPageBeanUtil.getDocumentFromString(xmlSource);
		Assert.assertNotNull(hsxSBWebPageBeanUtil);
	}
	
	@Test
	public void testGetStringTokens(){
		String value="action_page";
		String seperator="_";
		Object result = hsxSBWebPageBeanUtil.getStringTokens(value, seperator);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testCheckBadCredentials() throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException{
		final HsxSBWidget hsxSBWidget = null;
		final AuthenticationException authenticationException = new AuthenticationException("Bad credentials") {
		};
		mockery.checking(new Expectations(){
			{
				
				one(session).getAttribute(with(any(String.class)));
				will(returnValue("ACEGI_SECURITY_LAST_EXCEPTION"));
				
				one(session).getAttribute(with(any(String.class)));
				will(returnValue(authenticationException));
				
				one(session).getAttribute(with(any(String.class)));
				will(returnValue("widget"));
				
				one(session).getAttribute(with(any(String.class)));
				will(returnValue("widget"));
				
				one(session).getAttribute(with(any(String.class)));
				will(returnValue(hsxSBWidget));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
			}
		});
		hsxSBWebPageBeanUtil.checkBadCredentials(webBean, session);
		Assert.assertNull(webBean.getWidget());
	}
	
	@Test
	public void testDecodeTxData(){
		String strToDecode="&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;";
		Object result = hsxSBWebPageBeanUtil.decodeTxData(strToDecode);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testEncodeResponseXmlData(){
		String strToEncode="&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;";
		Object result = hsxSBWebPageBeanUtil.encodeResponseXmlData(strToEncode);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testEncodeTextData(){
		String strToEncode="&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;";
		Object result = hsxSBWebPageBeanUtil.encodeTextData(strToEncode);
		Assert.assertNotNull(result);
		}
	
	@Test
	public void testGetMapFromProperty(){
		Properties propertyfile = new Properties();
		propertyfile.setProperty("test", "test");
		Map<String, String> propertyMap = new HashMap<String, String>();
		propertyMap.put("test", "test");
		Object result = hsxSBWebPageBeanUtil.getMapFromProperty(propertyfile, propertyMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void  testGenerateUniqueSessionIdentifier() throws UnknownHostException{
		Object result = hsxSBWebPageBeanUtil.generateUniqueSessionIdentifier();
		Assert.assertNotNull(result);
		
	}
	@Test
	public void testResetWebBean(){
		hsxSBWebPageBeanUtil.resetWebBean(webBean);
		Assert.assertNull(webBean.getSessionIdentifierValue());
		Assert.assertNull(webBean.getLoginIndicator());
		Assert.assertNull(webBean.getQuoteRefID());
		Assert.assertNull(webBean.getWidget());
		Assert.assertNull(webBean.getWidgetMap());
	}
	
	@Test
	public void testPrintMap(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("test", "test");
		mockery.checking(new Expectations(){
			{
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
			}
		});
		hsxSBWebPageBeanUtil.printMap(map);
		Assert.assertNotNull(map);
	}
	
	@Test
	public void testGetCurrentTimeStamp(){
		Object result = hsxSBWebPageBeanUtil.getCurrentTimeStamp();
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testGetCurrentTimeStampForSessionIdentifier(){
		Object result = hsxSBWebPageBeanUtil.getCurrentTimeStampForSessionIdentifier();
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testGetHostURL(){
		String URL = "https://dev.hiscox.com/small-business-insurance/";
		Object result = hsxSBWebPageBeanUtil.getHostURL(URL);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testGetAbsoluteURL(){
		String pageName="paymentDetails";
		String URL = "https://dev.hiscox.com/small-business-insurance/";
		Object result = hsxSBWebPageBeanUtil.getAbsoluteURL(pageName, URL, webBean);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testGetAbsoluteResourcesProtocol(){
		String httpProtocol="https";
		String URL = "https://dev.hiscox.com/small-business-insurance/";
		Object result = hsxSBWebPageBeanUtil.getAbsoluteResourcesProtocol(httpProtocol, URL);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testGetDataCashBuildHTMLOne(){
		final Map<String, String> sessionMap = new HashMap<String, String>();
		sessionMap.put("dataCashPaymentOption", "Monthly");
		sessionMap.put("dataCashPOAdditionalValue2", "Monthly");
		webBean.setSessionMap(sessionMap);
		Object result = hsxSBWebPageBeanUtil.getDataCashBuildHTMLOne(webBean);
		Assert.assertNotNull(result);
	}
}	


