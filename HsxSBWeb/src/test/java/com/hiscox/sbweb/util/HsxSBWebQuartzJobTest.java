package com.hiscox.sbweb.util;

import java.util.Properties;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.broker.HsxSBWebTextManagerServiceBroker;

@Ignore
/**
 * This Junit is incomplete because we can't mock the 
 * Properties class used in setDefaultURI() method.
 * Mocking is allowed only for Interface.
 */
public class HsxSBWebQuartzJobTest {
	
	private HsxSBWebQuartzJob hsxSBWebQuartzJob;	
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Properties properties;
	private HsxSBWebTextManagerServiceBroker hsxSBWebTextManagerServiceBroker;

	@Before
	public void setUp() throws Exception {
		hsxSBWebQuartzJob = new HsxSBWebQuartzJob();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		properties = new Properties();
		hsxSBWebTextManagerServiceBroker = new HsxSBWebTextManagerServiceBroker();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws JobExecutionException {
		mockery.checking(new Expectations(){
			{
				
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
				one(context).getBean(with(any(String.class)));
				will(returnValue(hsxSBWebTextManagerServiceBroker));
				
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
			
		});
		hsxSBWebQuartzJob.execute(null);
	}

}
