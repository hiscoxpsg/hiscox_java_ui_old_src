package com.hiscox.sbweb.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.broker.HsxSBWebErrorManagementServiceBroker;

public class HsxSBWebQuoteAndBuyControllerUtilTest {
	
	private HsxSBWebQuoteAndBuyControllerUtil hsxSBWebQuoteAndBuyControllerUtil;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private Properties properties;
	private HttpServletRequest request;
	private HttpSession session;
	private Map<String, String> sessionMap;
	private HsxSBWidget hsxSBWidget;
	private Map<String, HsxSBWidget> widgetMap;
	private HsxSBWebPageRuleBean pageRuleBean;
	private HsxSBWebErrorManagementServiceBroker errorManagementServiceBroker;

	@Before
	public void setUp() throws Exception {
		hsxSBWebQuoteAndBuyControllerUtil = new HsxSBWebQuoteAndBuyControllerUtil();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		request=mockery.mock(HttpServletRequest.class);
		properties = new Properties();
		sessionMap = new HashMap<String, String>();
		widgetMap = new HashMap<String, HsxSBWidget>();
		pageRuleBean = new HsxSBWebPageRuleBean();
		errorManagementServiceBroker = new HsxSBWebErrorManagementServiceBroker();
		
		session = new HttpSession() {
			
			@Override
			public void setMaxInactiveInterval(int arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setAttribute(String arg0, Object arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void removeValue(String arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void removeAttribute(String arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void putValue(String arg0, Object arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public boolean isNew() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public void invalidate() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public String[] getValueNames() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getValue(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public HttpSessionContext getSessionContext() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ServletContext getServletContext() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getMaxInactiveInterval() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public long getLastAccessedTime() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public String getId() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public long getCreationTime() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Enumeration getAttributeNames() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getAttribute(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		errorManagementServiceBroker = null;
		hsxSBWebQuoteAndBuyControllerUtil = null;
		hsxSBWebResourceManager = null;
		hsxSBWidget = null;
		mockery = null;
		pageRuleBean = null;
		properties = null;
		request = null;
		session = null;
		sessionMap = null;
		webBean = null;
		widgetMap = null;
		
	}

	@Test
	public void testResetWebBeanXmlContent() throws HsxCoreException {
		mockery.checking(new Expectations(){
			{
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
			}
		});
		hsxSBWebQuoteAndBuyControllerUtil.resetWebBeanXmlContent(webBean);
		Assert.assertNull(webBean.getWidget());
		Assert.assertNull(webBean.getQuestionsContent());
		Assert.assertNull(webBean.getActionsContent());
		Assert.assertNull(webBean.getQuoteRefID());
	}

	@Test
	public void testGetErrorPageUrl() throws Exception{
		mockery.checking(new Expectations(){
			{
				
				one(request).getSession(with(false));
				will(returnValue(session));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
			}
		});
		Object result = hsxSBWebQuoteAndBuyControllerUtil.getErrorPageUrl(request, webBean);
		Assert.assertNotNull(result);
	}
	
	@Test
		public void testRedirectToInvalidPageUrl() throws Exception{
			mockery.checking(new Expectations(){
				{
					
					atLeast(1).of(context).getBean(with(any(String.class)));
					will(returnValue(properties));
					
				}
			});
			Object result = hsxSBWebQuoteAndBuyControllerUtil.redirectToInvalidPageUrl();
			Assert.assertNotNull(result);
		}
		
	@Test
	  public void testRedirectToPageNotFoundUrl() throws Exception{
			mockery.checking(new Expectations(){
				{
					atLeast(1).of(context).getBean(with(any(String.class)));
					will(returnValue(properties));
							
				}
			});
			Object result = hsxSBWebQuoteAndBuyControllerUtil.redirectToPageNotFoundUrl();
			Assert.assertNotNull(result);
		}
	  
	 @Test
	  	public void testRetrieveSessionMap(){
		  Properties sessionMgrProperties = new Properties();
		  mockery.checking(new Expectations(){
				{
					atLeast(1).of(context).getBean(with(any(String.class)));
					will(returnValue(properties));
							
				}
			});
		  hsxSBWebQuoteAndBuyControllerUtil.retrieveSessionMap(webBean, sessionMgrProperties);
		  Assert.assertNotNull(webBean.getSessionMap());
	  }
	  
	@Test
	  public void testIsRequestedPageValid(){
		String requestedPage="paymentDetails";
		 mockery.checking(new Expectations(){
				{
					atLeast(1).of(context).getBean(with(any(String.class)));
					will(returnValue(properties));
							
				}
			});
		  Boolean result = hsxSBWebQuoteAndBuyControllerUtil.isRequestedPageValid(requestedPage);
		  Assert.assertTrue(result);
	}
	  
	@Test
	 	public void testInValidateSession(){
		 webBean.setInvalidateSessionFlag(true);
		 mockery.checking(new Expectations(){
				{
					
					one(request).getSession(with(false));
					will(returnValue(session));
					
					atLeast(1).of(context).getBean(with(any(String.class)));
					will(returnValue(properties));
					
				}
			});
		hsxSBWebQuoteAndBuyControllerUtil.inValidateSession(request, webBean);
		Assert.assertNull(webBean.getWidget());
		Assert.assertNull(webBean.getQuestionsContent());
		Assert.assertNull(webBean.getActionsContent());
		Assert.assertNull(webBean.getQuoteRefID());
	}
	 	
	@Test
	   public void testPrepareRequestForWebException(){
		 String sessionIdentifier = "test";
		 String quoteRefId = "test";
		 String exceptionType = "test";
		 mockery.checking(new Expectations(){
				{
					atLeast(1).of(context).getBean(with(any(String.class)));
					will(returnValue(properties));
					
				}
			});	 
	 		Object result = hsxSBWebQuoteAndBuyControllerUtil.prepareRequestForWebException(sessionIdentifier, quoteRefId, exceptionType);
	 		Assert.assertNotNull(result);
	 	}
	   
	  @Test
	    public void testGetSessionValues(){
		  sessionMap.put("test", "test");
		  widgetMap.put("test", hsxSBWidget);
		  webBean.setSessionMap(sessionMap);
		  webBean.setWidgetMap(widgetMap);
		  mockery.checking(new Expectations(){
				{
					atLeast(1).of(context).getBean(with(any(String.class)));
					will(returnValue(properties));
					
				}
			});	 
		 Object result =  hsxSBWebQuoteAndBuyControllerUtil.getSessionValues(sessionMap, webBean);
		 Assert.assertNotNull("result");
	  }
	    
	 @Test
	  public void testUpdateSessionMapWithProducts(){
		 String xmlSource="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"+
	                "<Emp id=\"1\"><name>Pankaj</name><age>25</age>\n"+
	                "<role>Developer</role><gen>Male</gen></Emp>";
		 pageRuleBean.setUpdateProducts("Yes");
		 webBean.setQuestionsContent(xmlSource);
		 sessionMap.put("test", "test");
		 webBean.setSessionMap(sessionMap);
		 hsxSBWebQuoteAndBuyControllerUtil.updateSessionMapWithProducts(webBean, pageRuleBean);
		 Assert.assertNotNull(webBean.getSessionMap());
	 }
	 
	@Test
	 public void testLogUiExceptionToErrorManagement(){
		String exceptionType = "NullPointerException";
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
				one(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
				one(context).getBean(with(any(String.class)));
				will(returnValue(errorManagementServiceBroker)); 
				
				
			}
		});	
		hsxSBWebQuoteAndBuyControllerUtil.logUiExceptionToErrorManagement(webBean, exceptionType);
		Assert.assertNotNull(errorManagementServiceBroker);
	}
}

