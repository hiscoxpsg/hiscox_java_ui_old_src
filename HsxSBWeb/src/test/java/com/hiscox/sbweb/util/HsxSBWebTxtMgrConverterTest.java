package com.hiscox.sbweb.util;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HsxSBWebTxtMgrConverterTest {
	
	private HsxSBWebTxtMgrConverter hsxSBWebTxtMgrConverter;
	
	
	@Before
	public void setUp() throws Exception {
		hsxSBWebTxtMgrConverter = new HsxSBWebTxtMgrConverter();
	}

	@After
	public void tearDown() throws Exception {
		hsxSBWebTxtMgrConverter = null;
	}

	@Test
	public void testWithOnlyOneChildElement() {
		String responseXML="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"+
                "<test><TextItem>test</TextItem></test>";
		Object result = hsxSBWebTxtMgrConverter.convertXMLToTextItems(null, responseXML);
		Assert.assertNull(result);
	}

}
