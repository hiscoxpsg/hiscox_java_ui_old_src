package com.hiscox.sbweb.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBWebHelpFormControllerTest {
	
	private HsxSBWebHelpFormController hsxSBWebHelpFormController;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private HttpServletRequest request;
	private Properties properties;
	private HsxSBTextManagerVO hsxSBTextManagerVO;
	private Map<String, HsxSBTextManagerVO> textItemsMap; 
	
	

	@Before
	public void setUp() throws Exception {
		hsxSBWebHelpFormController = new HsxSBWebHelpFormController();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		request=mockery.mock(HttpServletRequest.class);
		properties = new Properties();
		hsxSBTextManagerVO = new HsxSBTextManagerVO();
		hsxSBTextManagerVO.setDefaultValue("1000");
		textItemsMap = new HashMap<String, HsxSBTextManagerVO>();
		textItemsMap.put("1", hsxSBTextManagerVO);
		
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBTextManagerVO = null;
		hsxSBWebHelpFormController = null;
		hsxSBWebResourceManager = null;
		mockery = null;
		properties = null;
		request = null;
		textItemsMap = null;
		webBean = null;
	}

	@Test
	public void test() throws Exception {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				one(request).getParameter(with(any(String.class)));
				will(returnValue("aboutYou"));
				
				one(request).getParameter(with(any(String.class)));
				will(returnValue("1"));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
				
			}
			
		});
		Object result = hsxSBWebHelpFormController.formBackingObject(request);
		Assert.assertNotNull(result);
	}

}
