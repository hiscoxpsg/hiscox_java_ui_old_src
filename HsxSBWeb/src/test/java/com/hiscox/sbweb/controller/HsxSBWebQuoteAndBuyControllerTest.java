package com.hiscox.sbweb.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.ModelAndView;


import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.bean.HsxSBWebPageRuleBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBWebQuoteAndBuyControllerTest {
	private HsxSBWebQuoteAndBuyController hsxSBWebQuoteAndBuyController;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private HttpServletRequest request;
	private Properties properties;
	private HttpSession session;
	private Map<String, HsxSBWebPageRuleBean> navigationRuleMap;
	private HsxSBWebPageRuleBean hsxSBWebPageRuleBean;
	
	

	@Before
	public void setUp() throws Exception {
		hsxSBWebQuoteAndBuyController = new HsxSBWebQuoteAndBuyController();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		request=mockery.mock(HttpServletRequest.class);
		properties = new Properties();
		
		session = new HttpSession() {
			
			@Override
			public void setMaxInactiveInterval(int arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setAttribute(String arg0, Object arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void removeValue(String arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void removeAttribute(String arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void putValue(String arg0, Object arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public boolean isNew() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public void invalidate() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public String[] getValueNames() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getValue(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public HttpSessionContext getSessionContext() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public ServletContext getServletContext() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public int getMaxInactiveInterval() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public long getLastAccessedTime() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public String getId() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public long getCreationTime() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public Enumeration getAttributeNames() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public Object getAttribute(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		navigationRuleMap = new HashMap<String, HsxSBWebPageRuleBean>();
		hsxSBWebPageRuleBean = new HsxSBWebPageRuleBean();
		hsxSBWebPageRuleBean.setChildPage(true);
		navigationRuleMap.put("dev.hiscox.com/small-business-insurance",hsxSBWebPageRuleBean);
		
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBWebPageRuleBean = null;
		hsxSBWebQuoteAndBuyController = null;
		hsxSBWebResourceManager = null;
		mockery = null;
		navigationRuleMap = null;
		properties = null;
		request = null;
		session = null;
		webBean = null;
		
	}

	@Test
	public void testInvalidFormBackingObject() {
		webBean.setInvalidActionPressed(true);

		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				one(request).getParameter(with(any(String.class)));
				will(returnValue("aboutYou"));
				
				one(request).getParameter(with(any(String.class)));
				will(returnValue("1"));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
		});
		Object result = hsxSBWebQuoteAndBuyController.formBackingObject(request);
		Assert.assertTrue(((HsxSBWebBean) result).isInvalidActionPressed());
	}

	/*@Test
	public void testWithValidAction() {
		
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				atLeast(1).of(request).getRequestURI();
				will(returnValue("https://dev.hiscox.com/small-business-insurance/"));
				
				
				
				atLeast(1).of(request).getSession(with(true));
				will(returnValue(session));
				
				atLeast(1).of(request).getRemoteAddr();
				will(returnValue("10.204.32.100"));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
		});
		Object result = hsxSBWebQuoteAndBuyController.formBackingObject(request);
		//Assert.assertTrue(((HsxSBWebBean) result).isInvalidActionPressed());
	}
*/
	@Test
	public void testCheckExceptionOccured() throws Exception{
		webBean.setExceptionOccured(true);
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				one(request).getSession(with(false));
				will(returnValue(session));
				
				
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
		});
		ModelAndView result = hsxSBWebQuoteAndBuyController.showForm(request, null, null);
		Assert.assertNotNull(result.getView());
	}
	
	@Test
	public void testSessionExpire() throws Exception{
		webBean.setRetrieveQuoteSessionExpiryFlag(true);
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				one(request).getSession(with(false));
				will(returnValue(session));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			}
		});
		ModelAndView result = hsxSBWebQuoteAndBuyController.showForm(request, null, null);
		Assert.assertNotNull(result.getView());
	}
	
	@Test
	public void testInvalidActionSubmittedByUser()throws Exception{
	  final Enumeration<String> enumeration = new Enumeration<String>() {
			
			@Override
			public String nextElement() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean hasMoreElements() {
				// TODO Auto-generated method stub
				return false;
			}
		};
		webBean.setDefaultNavigator("submit");
			mockery.checking(new Expectations(){
				{
					one(request).getParameterNames();
					will(returnValue(enumeration));
					
					atLeast(1).of(context).getBean(with(any(String.class)));
					will(returnValue(properties));
				}
			});
			Boolean result = hsxSBWebQuoteAndBuyController.suppressValidation(request, webBean);
			Assert.assertTrue(result);
			Assert.assertTrue(webBean.isInvalidActionPressed());
			
		}
		
	@Test
	public void testValidActionSubmittedByUser()throws Exception{
		final Enumeration<String> enumeration = new Enumeration<String>() {
					
					@Override
					public String nextElement() {
						// TODO Auto-generated method stub
						return null;
					}
					
					@Override
					public boolean hasMoreElements() {
						// TODO Auto-generated method stub
						return false;
					}
				};
				webBean.setDefaultNavigator("submit");
				Map<String, String> screenActionMap = new HashMap<String, String>();
				screenActionMap.put("submit", "");
				webBean.setScreenActionMap(screenActionMap);
					mockery.checking(new Expectations(){
						{
							
							one(request).getParameterNames();
							will(returnValue(enumeration));
							
							atLeast(1).of(context).getBean(with(any(String.class)));
							will(returnValue(properties));
						}
					});
					Boolean result = hsxSBWebQuoteAndBuyController.suppressValidation(request, webBean);
					Assert.assertTrue(result);
					Assert.assertFalse(webBean.isInvalidActionPressed());
				}
	
		@Test
		public void testSessionExpireOnSubmit()throws Exception{
			final StringBuffer buffer = new StringBuffer();
		
			webBean.setSessionExpiryFlag(true);
						mockery.checking(new Expectations(){
							{
								one(context).getBean(with(any(String.class)));
								will(returnValue(webBean));
								
								atLeast(1).of(request).getRequestURL();
								will(returnValue(buffer));
								
								one(request).getSession(with(false));
								will(returnValue(session));
								
								
								atLeast(1).of(context).getBean(with(any(String.class)));
								will(returnValue(properties));
							}
						});
						ModelAndView result =hsxSBWebQuoteAndBuyController.onSubmit(request, null, null, null);
						Assert.assertNotNull(result.getView());
						
					}
		
		@Test
		public void testInvalidSubmitByUser()throws Exception{
			final StringBuffer buffer = new StringBuffer();
		webBean.setInvalidActionPressed(true);
		
			
						mockery.checking(new Expectations(){
							{
								one(context).getBean(with(any(String.class)));
								will(returnValue(webBean));
								
								atLeast(1).of(request).getRequestURL();
								will(returnValue(buffer));
								
								one(request).getSession(with(false));
								will(returnValue(session));
								
								
								atLeast(1).of(context).getBean(with(any(String.class)));
								will(returnValue(properties));
							}
						});
						ModelAndView result = hsxSBWebQuoteAndBuyController.onSubmit(request, null, null, null);
						Assert.assertNotNull(result.getView());
						
					}
		
		@Test
		public void testInvalidQuoteOnSubmit()throws Exception{
			final StringBuffer buffer = new StringBuffer();
		
			Map<String, String> savedQuoteMap = new HashMap<String, String>();
			savedQuoteMap.put("", "");
			webBean.setSavedQuotesMap(savedQuoteMap);
			webBean.setSavedQuoteId("GHH990KL");
			webBean.setNextScreenId("SavedQuote");
			webBean.setRequestedAction("RetrieveAQuote");
						mockery.checking(new Expectations(){
							{
								one(context).getBean(with(any(String.class)));
								will(returnValue(webBean));
								
								atLeast(1).of(request).getRequestURL();
								will(returnValue(buffer));
								
								one(request).getSession(with(false));
								will(returnValue(session));
								
								atLeast(1).of(request).getRequestURI();
								will(returnValue("https://dev.hiscox.com/small-business-insurance/"));
								
								atLeast(1).of(context).getBean(with(any(String.class)));
								will(returnValue(properties));
							}
						});
						ModelAndView result = hsxSBWebQuoteAndBuyController.onSubmit(request, null, null, null);
						Assert.assertNotNull(result.getView());
						
					}
		
		@Test
		public void testSuppressBinding()throws Exception{
		
			final Enumeration<String> enumeration = new Enumeration<String>() {
				
				@Override
				public String nextElement() {
					// TODO Auto-generated method stub
					return null;
				}
				
				@Override
				public boolean hasMoreElements() {
					// TODO Auto-generated method stub
					return false;
				}
			};
			
						mockery.checking(new Expectations(){
							{
								one(context).getBean(with(any(String.class)));
								will(returnValue(webBean));
								
								one(request).getParameterNames();
								will(returnValue(enumeration));
								
								atLeast(1).of(context).getBean(with(any(String.class)));
								will(returnValue(properties));
							}
						});
						Boolean result = hsxSBWebQuoteAndBuyController.suppressBinding(request);
						Assert.assertFalse(result);
						
					}
}
