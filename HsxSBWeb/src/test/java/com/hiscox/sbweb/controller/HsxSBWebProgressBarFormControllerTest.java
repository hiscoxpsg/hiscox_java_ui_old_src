package com.hiscox.sbweb.controller;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;
@Ignore
/**
 * This Junit is incomplete becuase we can't set the file.
 */
public class HsxSBWebProgressBarFormControllerTest {
	
	private HsxSBWebProgressBarFormController  hsxSBWebProgressBarFormController;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	private HttpServletRequest request;
	private Properties properties;

	@Before
	public void setUp() throws Exception {
		hsxSBWebProgressBarFormController = new HsxSBWebProgressBarFormController();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		request=mockery.mock(HttpServletRequest.class);
		properties = new Properties();
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				one(request).getRequestURI();
				will(returnValue("https://dev.hiscox.com/small-business-insurance/"));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
				
				
				
			}
		});
		hsxSBWebProgressBarFormController.formBackingObject(request);
	}

}
