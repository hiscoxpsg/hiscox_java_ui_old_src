package com.hiscox.sbweb.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import com.hiscox.sbweb.bean.HsxSBWebBean;

@Ignore
/**
 * This Junit is incomplete becuase we can't set the file.
 */
public class HsxSBWebBroacherwareControllerTest {
	
	private HsxSBWebBroacherwareController hsxSBWebBroacherwareController;
	private HsxSBWebBean webBean;
	private ApplicationContext context;
	private Mockery mockery;
	private HttpServletRequest request;
	private Properties properties;
	private Resource file;
	
	
	@Before
	public void setUp() throws Exception {
		hsxSBWebBroacherwareController = new HsxSBWebBroacherwareController();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		properties = new Properties();
		file = new Resource() {
			
			@Override
			public InputStream getInputStream() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public long lastModified() throws IOException {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public boolean isReadable() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean isOpen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public URL getURL() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public URI getURI() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getFilename() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public File getFile() throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public String getDescription() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean exists() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Resource createRelative(String relativePath) throws IOException {
				// TODO Auto-generated method stub
				return null;
			}
		};
		

		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
				
				one(request).getRequestURI();
				will(returnValue("https://dev.hiscox.com/small-business-insurance/"));
				
				atLeast(1).of(context).getBean(with(any(String.class)));
				will(returnValue(properties));
			
				//"C:\"JBOSS\"jboss-5.1.0.GA\"server\"USDC\"deploy\"small-business-insurance.war\"WEB-INF\"navigation-rules.xml"
				atLeast(1).of(context).getResource(with(any(String.class))).getFile();
				will(returnValue(file));

				
				
			}
			
		});
		
		hsxSBWebBroacherwareController.onSubmit(request, null, null, null);
	}

}
