package com.hiscox.sbweb.controller;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;

public class HsxSBPrintPreviewControllerTest  {
	
	private HsxSBPrintPreviewController hsxSBPrintPreviewController;
	private HsxSBWebBean webBean;
	private HsxSBWebResourceManager hsxSBWebResourceManager;
	private ApplicationContext context;
	private Mockery mockery;
	

	@Before
	public void setUp() throws Exception {
		hsxSBPrintPreviewController = new HsxSBPrintPreviewController();
		webBean = new HsxSBWebBean();
		mockery = new Mockery();
		context=mockery.mock(ApplicationContext.class);
		hsxSBWebResourceManager.setContext(context);
		
		
	}

	@After
	public void tearDown() throws Exception {
		context = null;
		hsxSBPrintPreviewController = null;
		hsxSBWebResourceManager = null;
		mockery = null;
		webBean = null;
	}

	@Test
	public void test() {
		mockery.checking(new Expectations(){
			{
				one(context).getBean(with(any(String.class)));
				will(returnValue(webBean));
			}
			
		});
		
		Object result = hsxSBPrintPreviewController.formBackingObject(null);
		Assert.assertNotNull(result);
	}

}
