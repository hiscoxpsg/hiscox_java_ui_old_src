package com.hiscox.sbweb.resolver;

import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.web.servlet.view.AbstractUrlBasedView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
@Ignore

/**
 * This Junit is incomplete because we can not set the class inside
 * view class method.
 */
public class HsxSBWebUrlBasedViewResolverTest extends UrlBasedViewResolver{
	
	private HsxSBWebUrlBasedViewResolver hsxSBWebUrlBasedViewResolver;
	private UrlBasedViewResolver urlBasedViewResolver;
	private Mockery mockery;
	private BeanUtils beanUtils;
	

	@Before
	public void setUp() throws Exception {
		
		hsxSBWebUrlBasedViewResolver = new HsxSBWebUrlBasedViewResolver();
		urlBasedViewResolver = new UrlBasedViewResolver();
		urlBasedViewResolver.setViewClass(AbstractUrlBasedView.class);
		beanUtils =new BeanUtils() {
		};
		beanUtils.instantiateClass(HsxSBWebUrlBasedViewResolver.class);
		AbstractUrlBasedView view = (AbstractUrlBasedView) BeanUtils
				.instantiateClass(getViewClass());
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		String viewName="payment";
		hsxSBWebUrlBasedViewResolver.buildView(viewName);
	}

	
}
