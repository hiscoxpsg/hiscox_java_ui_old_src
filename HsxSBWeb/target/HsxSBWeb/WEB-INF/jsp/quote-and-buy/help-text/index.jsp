<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" />
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
<title>Help Text</title>
</head>
<body>
<div class="hlp-popup-div">
<h2> <usd:include sessionValue="helpTextLabel"/></h2>

<div class="hlp-popup-txt"><usd:include sessionValue="helpTextContent"/></div>
</div>
</body>
</html>