<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Processing your payment</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"
	name="description" />
<link href="/resources/css/global-reset.css" rel="stylesheet"
	type="text/css" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet"
	type="text/css" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css"
	type="text/css" />
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie.css";</style><![endif]-->
<!--[if IE]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/progressbar.js"></script>
<script type='text/javascript' src='/small-business-insurance/dwr/engine.js'></script>
<script type='text/javascript' src='/small-business-insurance/dwr/interface/ServiceBroker.js'></script>

</head>
<body>
<form:form method="post" commandName="screen" name="progressbar">
    <div class="main-container">
<!-- Header starts here-->
<div class="header">
<div class="logo"><img src="/resources/images/logo.gif" alt="Logo" title="Logo" /></div>


</div>
<!-- Header ends here-->

<!-- Progress bar starts here-->
<div class="progress-bar" >
    <ul>
            <li class="first-grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>About You</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your business</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Your Quote</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Important information</span></div></li>
            <li class="grey-bar"><div class="left-bg"></div><div class="middle-bg"><span>Application summary</span></div></li>
            <li class="red-bar"><div class="left-bg"></div><div class="middle-bg"><span>Payment details</span></div></li>
            <li class="last-red-white-bar"><div class="left-bg"></div><div class="middle-bg"><span>Completed</span></div><div class="right-bg-last"></div></li>
    </ul>
    </div>
<!-- Progress bar starts here-->
<!-- Page container starts here-->
<div class="page-container">

<div class="section-container throbber-box">
    <div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
    <div class="middle-content vertical-container">
    <div class="throbber-heading">Hiscox US Direct</div>
    <div class="animated-img"><img	src="/resources/images/pod1_hiscox_ball-circle_20060618.gif" alt="" title="" /></div>
    <div class="throbber-text">Processing your payment</div>
   </div>
   <div class="bottom"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
    </div>
 </div>
 </div>
 </form:form>
</body>
</html>
