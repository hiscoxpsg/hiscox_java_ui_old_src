<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page language="java" isErrorPage="false"
	errorPage="/WEB-INF/jsp/system-error/index.jsp"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Print Preview</title>


<link media="all" href="/resources/css/global-reset.css"
	rel="stylesheet" type="text/css" />
<link media="all" href="/resources/css/rhythmyx.css"
	rel="stylesheet" type="text/css" />
<link media="all" href="/resources/css/ephox-dynamic.css"
	rel="stylesheet" type="text/css" />
<link media="all" href="/resources/css/print.css"
	rel="stylesheet" type="text/css" />
<link media="all" rel="stylesheet"
	href="/resources/css/screenbuilder.css" type="text/css" />
<!-- <link media ="print" id="id" rel="stylesheet" href="/resources/css/print.css" type="text/css" /> -->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body>
<div class="main-container">
<div class="header">
<div id="print_rx_logo_id" class="logo"></div>
</div>
<div class="bottom clear-both"></div>
<div id="print_rx_cnt_id" class="cnt-sctn"></div>
<div id="print_rx_footer_id" class="footer"></div>
</div>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/printer.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
</body>
</html>