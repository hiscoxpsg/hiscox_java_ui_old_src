<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Coverage options | Hiscox USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print" />
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body>
<div class="main-container">
<!-- Header starts here-->
	<div class="header">
		<div class="logo" id="print_rx_logo_id">
	
	
    							
			                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		<form method="post">
	      <div class="header-btncontainer">
											<usd:checkLogin loginContent="logout">
						<div class="header-btns"><div class="submit-button grey-button logout-icon">
								<span class='submit-button-end'><input class='submit-input' type='submit' title="Log Out" value='Log Out' name="action_Log Out_button"  /><span class="button-icon"></span></span>
						</div></div>
					</usd:checkLogin>
					<usd:checkLogin loginContent="retrieve">
						<div class='header-btns float-right' ><div class="submit-button grey-button save-icon header-button-gap"><span class='submit-button-end'><input class="submit-input" type="submit" title="Retrieve A quote" name ="action_RetrieveAQuote_button" value="Retrieve A Quote"/><span class="button-icon"></span></span></div></div>
					</usd:checkLogin>
						           </div>
		   </form>
	</div>
	<!-- Header ends here-->
	<!-- Progress bar starts here-->
			

        <!-- Page container starts here-->
	<div class="page-container">
		<div class="breadcrumb">
						    		<usd:checkEligibility eligibleContent="stateVariant,primarybusiness">	
				<div class="occupation-details">
					<span class = "float-right"><object>
							<div class="occupation-info"><span>State:</span> <a href="/small-business-insurance/quote-and-buy/change-your-business-type-and-state"><usd:include sessionValue="stateVariant" /></a></div>
							<div class="occupation-info"><span>Type of business:</span> <a href="/small-business-insurance/quote-and-buy/change-your-business-type-and-state"><usd:include sessionValue="primarybusiness" /></a></div>
					</object></span>
				</div>
    		</usd:checkEligibility>
								</div>    			
    	 		
				    		    				    	    		    				    	    		   			    		    		    		    																																																																																																																																																																																																																																																																												<div></div>
			<div></div>			
						<!--Left nav starts here-->
			    			<div class="left-nav">
    				<usd:checkOccup occupContent="it-consulting" > 
    						                                         									            		            		            		            			            														<div class="section-container left-nav-corner grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Customized coverage for technology businesses</b></h3><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="190" /><p class="body-copy-grey">Because we understand the specific risks you face we customize our coverage for you. Our Professional Liability Policy includes $200,000 of software copyright infringement coverage for most technology businesses.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="consulting" > 
    						                                         		            									            		            		            			            														<div class="section-container left-nav-corner grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Customized coverage for small businesses</b></h3><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="190" /><p class="body-copy-grey">Because we understand the specific risks you face we customize your coverage. This means you get the right protection and only pay for what you really need.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="marketing-pr-consulting" > 
    						                                         		            		            									            		            			            														<div class="section-container left-nav-corner grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Customized coverage for marketing consultants</b></h3><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="190" /><p class="body-copy-grey">Because we understand the specific risks you face, we customize our coverage for you. Our Professional Liability Policy includes $200,000 of copyright infringement coverage for marketing consultants.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-known" > 
    						                                         		            		            		            									            			            														<div class="section-container left-nav-corner grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Customized coverage for small businesses</b></h3><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="190" /><p class="body-copy-grey">Because we understand the specific risks you face we customize your coverage. This means you get the right protection and only pay for what you really need.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-not-known" > 
    						                                         		            		            		            		            										            														<div class="section-container left-nav-corner grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
		<h3 class="box-heading-h3"><b>Customized coverage for small businesses</b></h3><img id="redline_left" src="/shared-images/redline_left.png" alt="" title="" border="0" height="2" width="190" /><p class="body-copy-grey">Because we understand the specific risks you face we customize your coverage. This means you get the right protection and only pay for what you really need.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			    										    										    										            										            										            										    										    										    										    																																            				</usd:checkOccup>
    				<usd:checkOccup occupContent="it-consulting" > 
    						                                         									            		            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="consulting" > 
    						                                         		            									            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="marketing-pr-consulting" > 
    						                                         		            		            									            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-known" > 
    						                                         		            		            		            									            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	            				</usd:checkOccup>
    				<usd:checkOccup occupContent="default-occupation-not-known" > 
    						                                         		            		            		            		            										            										    										    										    										            										            										            										    										    										    										    																																            				</usd:checkOccup>
    			</div>
						<!--Left nav ends here--> 
			<!--Screen builder section starts here-->  
			<div class="cnt-sctn " id="print_rx_cnt_id">
				<div class="top"><div class="right" ><div class="left"><div class="middle"></div></div></div></div>
				<div class="middle-content">
					<div class="page-heading page-heading-border">
																																									<usd:checkOccup occupContent="it-consulting" >
    	
    	<h1 class="page-heading-h1">Coverage options</h1>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="consulting" >
    	
    	<h1 class="page-heading-h1">Coverage options</h1>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<h1 class="page-heading-h1">Coverage options</h1>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<h1 class="page-heading-h1">Coverage options</h1>    	
		</usd:checkOccup>
																														<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<h1 class="page-heading-h1">Coverage options</h1>    	
		</usd:checkOccup>
														<div class="reference-number">
							<usd:checkInclude  includeContent="quoteRefID">
								<span>Reference # <usd:include sessionValue="quoteRefID"/></span>
							</usd:checkInclude>	
						</div>
						</div>
						<div class="clear"></div>
																								<div class="topcnt-container">
																																									<usd:checkOccup occupContent="it-consulting" >
    	
    	<div><p class="body-copy-grey">If you know what insurance coverage your business needs, please select from the options below to start a quote. If you need help, just give one of our licensed advisors a call.<br /><br />Our process is quick and easy – it only takes a few simple steps to get a customized online quote which is tailored to your business.</p></div>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="consulting" >
    	
    	<div><p class="body-copy-grey">If you know what insurance coverage your business needs, please select from the options below to start a quote. If you need help, just give one of our licensed advisors a call.<br /><br />Our process is quick and easy – it only takes a few simple steps to get a customized online quote which is tailored to your business.</p></div>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<div><p class="body-copy-grey">If you know what insurance coverage your business needs, please select from the options below to start a quote. If you need help, just give one of our licensed advisors a call.<br /><br />Our process is quick and easy – it only takes a few simple steps to get a customized online quote which is tailored to your business.</p></div>    	
		</usd:checkOccup>
																												<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<div><p class="body-copy-grey">If you know what insurance coverage your business needs, please select from the options below to start a quote. If you need help, just give one of our licensed advisors a call.<br /><br />Our process is quick and easy – it only takes a few simple steps to get a customized online quote which is tailored to your business.</p></div>    	
		</usd:checkOccup>
																														<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<div><p class="body-copy-grey">If you know what insurance coverage your business needs, please select from the options below to start a quote. If you need help, just give one of our licensed advisors a call.<br /><br />Our process is quick and easy – it only takes a few simple steps to get a customized online quote which is tailored to your business.</p></div>    	
		</usd:checkOccup>
									<div class="horz-line2"></div>
													</div>
												<usd:verifyToInclude>
						<usd:checkFormTag formContent="Start"></usd:checkFormTag>
						<usd:questionTag />	
																								<usd:actionTag />
						<usd:checkFormTag formContent="End"></usd:checkFormTag>	
						</usd:verifyToInclude>
						<usd:trackingTag />
																																						<usd:checkOccup occupContent="it-consulting" >
    	<div class='topcnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div>
		</usd:checkOccup>
																											<usd:checkOccup occupContent="consulting" >
    	<div class='topcnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div>
		</usd:checkOccup>
																											<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='topcnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div>
		</usd:checkOccup>
																											<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='topcnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div>
		</usd:checkOccup>
																													<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='topcnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div>
		</usd:checkOccup>
							        	<div class="btmbtn-cntnr"> 
        		        		            		                            		        		        		<div class="clear-both"></div>
        	</div>
		</div>
	<div class="bottom clear-both" ><div class="right"><div class="left"><div class="middle" ></div></div></div></div> 
	</div> 
				<!--Screen builder section ends here--> 
				<!--Right nav starts here-->   
				<div class="right-nav showcntct-num">
					<!--Checklist section starts here-->   
					<usd:checkOccup occupContent="it-consulting" > 
							                                         									            		            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><b><span class="grey-call-to-action-large">888-202-2973</span></b></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST IT default</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" >
							                                         		            									            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><b><span class="grey-call-to-action-large">888-202-2973</span></b></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST Consulting default</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                         		            		            									            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><b><span class="grey-call-to-action-large">888-202-2973</span></b></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST Marketing default</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                         		            		            		            									            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																												<div class="contact-child-item print">
						<div>
							<p><b><span class="grey-call-to-action-large">888-202-2973</span></b></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST Occupation known default</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                         		            		            		            		            										            										    										    										    										            										            														<div class="section-container right-nav-corner tpright-container-large print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><p class="red-call-to-action-small">Default snippet</p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><script type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script> <p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>	
			            										    										    										    										    																																        					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                         													            		            		            		            			    					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                         		            													            		            		            			    					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                         		            		            													            		            			    					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                         		            		            		            													            			    					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                         		            		            		            		            														    					</usd:checkOccup>
					<usd:checkOccup occupContent="it-consulting" > 
							                                         									            		            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="consulting" > 
							                                         		            									            		            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="marketing-pr-consulting" > 
							                                         		            		            									            		            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-known" > 
							                                         		            		            		            									            			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        					</usd:checkOccup>
					<usd:checkOccup occupContent="default-occupation-not-known" > 
							                                         		            		            		            		            										            										    										    										    										            										            										            										    										    										    										    																																        					</usd:checkOccup>					
					<div class="verLogo-holder">
<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications.">
<tr>
<td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hiscoxusa.com&size=L&use_flash=YES&use_transparent=YES&lang=en"></script><br />
<a href="http://www.verisign.com/ssl-certificate/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>
</tr>
</table>
					</div>
				</div>
				<!--Right nav ends here--> 
				<!-- Page container ends here-->
		<div class="boiler-plate">
			 <div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>    
				<div class="middle-content"> 
					<div class="middlecnt-container">
							
	
	
	
    							
																<h2 class="red-call-to-action-medium-h2"><b>About Hiscox small business insurance</b></h2><ul class="list-bulleted-body-copy-grey"><li><b>Small business specialists</b> - focus on insuring professional businesses with less than 10 employees.</li><li><b>Customized coverage</b> - we help customize coverage to your specific needs. No more, no less.</li><li><b>Great value</b> - buy direct from Hiscox and enjoy excellent service and real value for money.</li><li><b>Money back guarantee</b> - be confident in your purchase with our 14 day guarantee.</li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.</li></ul>
										
    	    		
		 		</div>
				</div>
			<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div>
		</div>
		<!-- Start of new class for twocol page - confirmation page) !-->
						<!-- End of new class for twocol page - confirmation page) !-->
    	<div class="footer" id="print_rx_footer_id">
    		
	
	
    	    		<div class="copy-right">
    			
	
	
    							
																<p class="body-copy-grey">© 2011 Hiscox Inc. All rights reserved</p>
										
    	    		
		 		    		</div>
    	</div>
	</div>  
</div>
	<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
<usd:invalidateSessionTag/>

		<div style="width:100%;align:center">
			<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Product selection page (I know what I want)
URL of the webpage where the tag is expected to be placed: http://www.hiscoxusa.com/small-business-insurance/quote-and-buy/coverage-options
Creation Date: 06/07/2010
-->
<iframe src="https://fls.doubleclick.net/activityi;src=2723417;type=iknow804;cat=produ766;qty=1;cost=[Revenue];u4=[Products];ord=[OrderID]?" width="1" height="1" frameborder="0"></iframe>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->		</div>
	</body>
</html>