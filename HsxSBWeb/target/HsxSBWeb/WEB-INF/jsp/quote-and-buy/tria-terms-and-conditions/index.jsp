<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Optional terrorism coverage terms and conditions | Hiscox USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print" />
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body>
  <div class="main-container-popup">
  <!-- Header starts here-->
	<div class="header-popup">
		<div class="popuplogo" id="print_rx_logo_id">
	
	
	
    							
			                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		
    <div class="popup-hdr-text">
	      <ol>
		<li class='red-button right-arrow-white float-right'>
        		</li>
    </ol>
       </div>
  </div>
  <!-- Header ends here-->
  <!-- Page container starts here-->
  <div class="page-container">
      <!--Screen builder section starts here-->
    <div class="cnt-sctn popup-centr-sctn" id="print_rx_cnt_id">
      <div class="top">
        <div class="right" >
          <div class="left">
            <div class="middle"> </div>
          </div>
        </div>
      </div>
      <div class="middle-content">
        <div class="page-heading page-heading-border">
        		    																					<usd:checkOccup occupContent="it-consulting" >
    	
    	<h1 class="page-heading-h1">Optional terrorism coverage terms and conditions</h1>    	
		</usd:checkOccup>
	    																					<usd:checkOccup occupContent="consulting" >
    	
    	<h1 class="page-heading-h1">Optional terrorism coverage terms and conditions</h1>    	
		</usd:checkOccup>
	    																					<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<h1 class="page-heading-h1">Optional terrorism coverage terms and conditions</h1>    	
		</usd:checkOccup>
	    																					<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<h1 class="page-heading-h1">Optional terrorism coverage terms and conditions</h1>    	
		</usd:checkOccup>
	    																							<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<h1 class="page-heading-h1">Optional terrorism coverage terms and conditions</h1>    	
		</usd:checkOccup>
	    	        <div class="reference-number">
			<usd:checkInclude  includeContent="quoteRefID">
				<span>Reference # <usd:include sessionValue="quoteRefID"/></span>
			</usd:checkInclude>	
		</div>
        </div>
        <div class="clear"></div>
        <div class="topcnt-container">
                   																						<usd:checkOccup occupContent="it-consulting" >
    	
    	<h2 class="grey-heading-h2">Disclosure Notice of Terrorism Insurance Coverage</h2><p class="body-copy-grey">You are hereby notified that under the Terrorism Risk Insurance Act of 2002, as amended ("TRIA"), that you now have a right to purchase insurance coverage for losses arising out of acts of terrorism, <b>as defined in Section 102(1) of the Act, as amended:</b> The term “act of terrorism” means any act that is certified by the Secretary of the Treasury, in concurrence with the Secretary of State, and the Attorney General of the United States-to be an act of terrorism; to be a violent act or an act that is dangerous to human life, property, or infrastructure; to have resulted in damage within the United States, or outside the United States in the case of an air carrier or vessel or the premises of a United States mission; and to have been committed by an individual or individuals, as part of an effort to coerce the civilian population of the United States or to influence the policy or affect the conduct of the United States Government by coercion. Any coverage you purchase for "acts of terrorism" shall expire at 12:00 midnight December 31, 2014, the date on which the TRIA Program is scheduled to terminate or the expiry date of the policy whichever occurs first, and shall not cover any losses or events which arise after the earlier of these dates.</p><p class="body-copy-grey">YOU SHOULD KNOW THAT COVERAGE PROVIDED BY THIS POLICY FOR LOSSES CAUSED BY CERTIFIED ACTS OF TERRORISM IS PARTIALLY REIMBURSED BY THE UNITED STATES UNDER A FORMULA ESTABLISHED BY FEDERAL LAW. HOWEVER, YOUR POLICY MAY CONTAIN OTHER EXCLUSIONS WHICH MIGHT AFFECT YOUR COVERAGE, SUCH AS AN EXCLUSION FOR NUCLEAR EVENTS. UNDER THIS FORMULA, THE UNITED STATES PAYS 85% OF COVERED TERRORISM LOSSES EXCEEDING THE STATUTORILY ESTABLISHED DEDUCTIBLE PAID BY THE INSURER(S) PROVIDING THE COVERAGE. YOU SHOULD ALSO KNOW THAT THE TERRORISM RISK INSURANCE ACT, AS AMENDED, CONTAINS A USD100 BILLION CAP THAT LIMITS U.S. GOVERNMENT REIMBURSEMENT AS WELL AS INSURERS' LIABILITY FOR LOSSES RESULTING FROM CERTIFIED ACTS OF TERRORISM WHEN THE AMOUNT OF SUCH LOSSES IN ANY ONE CALENDAR YEAR EXCEEDS USD100 BILLION. IF THE AGGREGATE INSURED LOSSES FOR ALL INSURERS EXCEED USD100 BILLION, YOUR COVERAGE MAY BE REDUCED.</p><p class="body-copy-grey">THE PREMIUM CHARGED FOR THIS COVERAGE IS PROVIDED BELOW AND DOES NOT INCLUDE ANY CHARGES FOR THE PORTION OF LOSS COVERED BY THE FEDERAL GOVERNMENT UNDER THE ACT.</p>    	
		</usd:checkOccup>
																							<usd:checkOccup occupContent="consulting" >
    	
    	<h2 class="grey-heading-h2">Disclosure Notice of Terrorism Insurance Coverage</h2><p class="body-copy-grey">You are hereby notified that under the Terrorism Risk Insurance Act of 2002, as amended ("TRIA"), that you now have a right to purchase insurance coverage for losses arising out of acts of terrorism, <b>as defined in Section 102(1) of the Act, as amended:</b> The term “act of terrorism” means any act that is certified by the Secretary of the Treasury, in concurrence with the Secretary of State, and the Attorney General of the United States-to be an act of terrorism; to be a violent act or an act that is dangerous to human life, property, or infrastructure; to have resulted in damage within the United States, or outside the United States in the case of an air carrier or vessel or the premises of a United States mission; and to have been committed by an individual or individuals, as part of an effort to coerce the civilian population of the United States or to influence the policy or affect the conduct of the United States Government by coercion. Any coverage you purchase for "acts of terrorism" shall expire at 12:00 midnight December 31, 2014, the date on which the TRIA Program is scheduled to terminate or the expiry date of the policy whichever occurs first, and shall not cover any losses or events which arise after the earlier of these dates.</p><p class="body-copy-grey">YOU SHOULD KNOW THAT COVERAGE PROVIDED BY THIS POLICY FOR LOSSES CAUSED BY CERTIFIED ACTS OF TERRORISM IS PARTIALLY REIMBURSED BY THE UNITED STATES UNDER A FORMULA ESTABLISHED BY FEDERAL LAW. HOWEVER, YOUR POLICY MAY CONTAIN OTHER EXCLUSIONS WHICH MIGHT AFFECT YOUR COVERAGE, SUCH AS AN EXCLUSION FOR NUCLEAR EVENTS. UNDER THIS FORMULA, THE UNITED STATES PAYS 85% OF COVERED TERRORISM LOSSES EXCEEDING THE STATUTORILY ESTABLISHED DEDUCTIBLE PAID BY THE INSURER(S) PROVIDING THE COVERAGE. YOU SHOULD ALSO KNOW THAT THE TERRORISM RISK INSURANCE ACT, AS AMENDED, CONTAINS A USD100 BILLION CAP THAT LIMITS U.S. GOVERNMENT REIMBURSEMENT AS WELL AS INSURERS' LIABILITY FOR LOSSES RESULTING FROM CERTIFIED ACTS OF TERRORISM WHEN THE AMOUNT OF SUCH LOSSES IN ANY ONE CALENDAR YEAR EXCEEDS USD100 BILLION. IF THE AGGREGATE INSURED LOSSES FOR ALL INSURERS EXCEED USD100 BILLION, YOUR COVERAGE MAY BE REDUCED.</p><p class="body-copy-grey">THE PREMIUM CHARGED FOR THIS COVERAGE IS PROVIDED BELOW AND DOES NOT INCLUDE ANY CHARGES FOR THE PORTION OF LOSS COVERED BY THE FEDERAL GOVERNMENT UNDER THE ACT.</p>    	
		</usd:checkOccup>
																							<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<h2 class="grey-heading-h2">Disclosure Notice of Terrorism Insurance Coverage</h2><p class="body-copy-grey">You are hereby notified that under the Terrorism Risk Insurance Act of 2002, as amended ("TRIA"), that you now have a right to purchase insurance coverage for losses arising out of acts of terrorism, <b>as defined in Section 102(1) of the Act, as amended:</b> The term “act of terrorism” means any act that is certified by the Secretary of the Treasury, in concurrence with the Secretary of State, and the Attorney General of the United States-to be an act of terrorism; to be a violent act or an act that is dangerous to human life, property, or infrastructure; to have resulted in damage within the United States, or outside the United States in the case of an air carrier or vessel or the premises of a United States mission; and to have been committed by an individual or individuals, as part of an effort to coerce the civilian population of the United States or to influence the policy or affect the conduct of the United States Government by coercion. Any coverage you purchase for "acts of terrorism" shall expire at 12:00 midnight December 31, 2014, the date on which the TRIA Program is scheduled to terminate or the expiry date of the policy whichever occurs first, and shall not cover any losses or events which arise after the earlier of these dates.</p><p class="body-copy-grey">YOU SHOULD KNOW THAT COVERAGE PROVIDED BY THIS POLICY FOR LOSSES CAUSED BY CERTIFIED ACTS OF TERRORISM IS PARTIALLY REIMBURSED BY THE UNITED STATES UNDER A FORMULA ESTABLISHED BY FEDERAL LAW. HOWEVER, YOUR POLICY MAY CONTAIN OTHER EXCLUSIONS WHICH MIGHT AFFECT YOUR COVERAGE, SUCH AS AN EXCLUSION FOR NUCLEAR EVENTS. UNDER THIS FORMULA, THE UNITED STATES PAYS 85% OF COVERED TERRORISM LOSSES EXCEEDING THE STATUTORILY ESTABLISHED DEDUCTIBLE PAID BY THE INSURER(S) PROVIDING THE COVERAGE. YOU SHOULD ALSO KNOW THAT THE TERRORISM RISK INSURANCE ACT, AS AMENDED, CONTAINS A USD100 BILLION CAP THAT LIMITS U.S. GOVERNMENT REIMBURSEMENT AS WELL AS INSURERS' LIABILITY FOR LOSSES RESULTING FROM CERTIFIED ACTS OF TERRORISM WHEN THE AMOUNT OF SUCH LOSSES IN ANY ONE CALENDAR YEAR EXCEEDS USD100 BILLION. IF THE AGGREGATE INSURED LOSSES FOR ALL INSURERS EXCEED USD100 BILLION, YOUR COVERAGE MAY BE REDUCED.</p><p class="body-copy-grey">THE PREMIUM CHARGED FOR THIS COVERAGE IS PROVIDED BELOW AND DOES NOT INCLUDE ANY CHARGES FOR THE PORTION OF LOSS COVERED BY THE FEDERAL GOVERNMENT UNDER THE ACT.</p>    	
		</usd:checkOccup>
																							<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<h2 class="grey-heading-h2">Disclosure Notice of Terrorism Insurance Coverage</h2><p class="body-copy-grey">You are hereby notified that under the Terrorism Risk Insurance Act of 2002, as amended ("TRIA"), that you now have a right to purchase insurance coverage for losses arising out of acts of terrorism, <b>as defined in Section 102(1) of the Act, as amended:</b> The term “act of terrorism” means any act that is certified by the Secretary of the Treasury, in concurrence with the Secretary of State, and the Attorney General of the United States-to be an act of terrorism; to be a violent act or an act that is dangerous to human life, property, or infrastructure; to have resulted in damage within the United States, or outside the United States in the case of an air carrier or vessel or the premises of a United States mission; and to have been committed by an individual or individuals, as part of an effort to coerce the civilian population of the United States or to influence the policy or affect the conduct of the United States Government by coercion. Any coverage you purchase for "acts of terrorism" shall expire at 12:00 midnight December 31, 2014, the date on which the TRIA Program is scheduled to terminate or the expiry date of the policy whichever occurs first, and shall not cover any losses or events which arise after the earlier of these dates.</p><p class="body-copy-grey">YOU SHOULD KNOW THAT COVERAGE PROVIDED BY THIS POLICY FOR LOSSES CAUSED BY CERTIFIED ACTS OF TERRORISM IS PARTIALLY REIMBURSED BY THE UNITED STATES UNDER A FORMULA ESTABLISHED BY FEDERAL LAW. HOWEVER, YOUR POLICY MAY CONTAIN OTHER EXCLUSIONS WHICH MIGHT AFFECT YOUR COVERAGE, SUCH AS AN EXCLUSION FOR NUCLEAR EVENTS. UNDER THIS FORMULA, THE UNITED STATES PAYS 85% OF COVERED TERRORISM LOSSES EXCEEDING THE STATUTORILY ESTABLISHED DEDUCTIBLE PAID BY THE INSURER(S) PROVIDING THE COVERAGE. YOU SHOULD ALSO KNOW THAT THE TERRORISM RISK INSURANCE ACT, AS AMENDED, CONTAINS A USD100 BILLION CAP THAT LIMITS U.S. GOVERNMENT REIMBURSEMENT AS WELL AS INSURERS' LIABILITY FOR LOSSES RESULTING FROM CERTIFIED ACTS OF TERRORISM WHEN THE AMOUNT OF SUCH LOSSES IN ANY ONE CALENDAR YEAR EXCEEDS USD100 BILLION. IF THE AGGREGATE INSURED LOSSES FOR ALL INSURERS EXCEED USD100 BILLION, YOUR COVERAGE MAY BE REDUCED.</p><p class="body-copy-grey">THE PREMIUM CHARGED FOR THIS COVERAGE IS PROVIDED BELOW AND DOES NOT INCLUDE ANY CHARGES FOR THE PORTION OF LOSS COVERED BY THE FEDERAL GOVERNMENT UNDER THE ACT.</p>    	
		</usd:checkOccup>
																									<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<h2 class="grey-heading-h2">Disclosure Notice of Terrorism Insurance Coverage</h2><p class="body-copy-grey">You are hereby notified that under the Terrorism Risk Insurance Act of 2002, as amended ("TRIA"), that you now have a right to purchase insurance coverage for losses arising out of acts of terrorism, <b>as defined in Section 102(1) of the Act, as amended:</b> The term “act of terrorism” means any act that is certified by the Secretary of the Treasury, in concurrence with the Secretary of State, and the Attorney General of the United States-to be an act of terrorism; to be a violent act or an act that is dangerous to human life, property, or infrastructure; to have resulted in damage within the United States, or outside the United States in the case of an air carrier or vessel or the premises of a United States mission; and to have been committed by an individual or individuals, as part of an effort to coerce the civilian population of the United States or to influence the policy or affect the conduct of the United States Government by coercion. Any coverage you purchase for "acts of terrorism" shall expire at 12:00 midnight December 31, 2014, the date on which the TRIA Program is scheduled to terminate or the expiry date of the policy whichever occurs first, and shall not cover any losses or events which arise after the earlier of these dates.</p><p class="body-copy-grey">YOU SHOULD KNOW THAT COVERAGE PROVIDED BY THIS POLICY FOR LOSSES CAUSED BY CERTIFIED ACTS OF TERRORISM IS PARTIALLY REIMBURSED BY THE UNITED STATES UNDER A FORMULA ESTABLISHED BY FEDERAL LAW. HOWEVER, YOUR POLICY MAY CONTAIN OTHER EXCLUSIONS WHICH MIGHT AFFECT YOUR COVERAGE, SUCH AS AN EXCLUSION FOR NUCLEAR EVENTS. UNDER THIS FORMULA, THE UNITED STATES PAYS 85% OF COVERED TERRORISM LOSSES EXCEEDING THE STATUTORILY ESTABLISHED DEDUCTIBLE PAID BY THE INSURER(S) PROVIDING THE COVERAGE. YOU SHOULD ALSO KNOW THAT THE TERRORISM RISK INSURANCE ACT, AS AMENDED, CONTAINS A USD100 BILLION CAP THAT LIMITS U.S. GOVERNMENT REIMBURSEMENT AS WELL AS INSURERS' LIABILITY FOR LOSSES RESULTING FROM CERTIFIED ACTS OF TERRORISM WHEN THE AMOUNT OF SUCH LOSSES IN ANY ONE CALENDAR YEAR EXCEEDS USD100 BILLION. IF THE AGGREGATE INSURED LOSSES FOR ALL INSURERS EXCEED USD100 BILLION, YOUR COVERAGE MAY BE REDUCED.</p><p class="body-copy-grey">THE PREMIUM CHARGED FOR THIS COVERAGE IS PROVIDED BELOW AND DOES NOT INCLUDE ANY CHARGES FOR THE PORTION OF LOSS COVERED BY THE FEDERAL GOVERNMENT UNDER THE ACT.</p>    	
		</usd:checkOccup>
				<div class="horz-line2"></div>
				</div>
		<usd:verifyToInclude>
		<usd:checkFormTag formContent="start"></usd:checkFormTag>
		<usd:questionTag  />  
		<usd:actionTag  />
	<usd:trackingTag />
	<usd:checkFormTag formContent="end"></usd:checkFormTag>
	</usd:verifyToInclude>
	     <div class="btmbtn-cntnr"> 
						        		                									<div class="clear-both"></div>
		 </div> 
    <!--Screen builder section ends here-->
    </div>
    <div class="bottom clear-both" ><div class="right"> <div class="left"><div class="middle" ></div></div></div></div></div>
 </div>
<!-- Page container ends here-->
		  </div>
<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
</body>
</html>