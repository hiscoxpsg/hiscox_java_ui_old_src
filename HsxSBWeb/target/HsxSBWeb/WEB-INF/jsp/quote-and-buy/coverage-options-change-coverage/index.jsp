<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/tld/hsxSbWeb.tld" prefix="usd" %>
<%@page language="java" isErrorPage="false" errorPage="/WEB-INF/jsp/quote-and-buy/system-error/index.jsp" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Change coverage options | Hiscox USA</title>
<usd:getScreenBuilderContentTag questionContent="${screen.questionsContent}" actionContent="${screen.actionsContent}" trackingContent="${screen.trackingContent}"/>
		<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print"/>
<!--[if lte IE 8]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if lte IE 8]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
</head>
<body onload="rotatingtext()">
	<!--main container starts here-->
	<div class="main-container">
		<!-- Header starts here-->
		<div class="header">			
		<div class="logo-home">
	
	
    							
			                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		<div class="topmenu-container">
			<div class="utitlitylinks-hlder">
				<ul>
	
	
    							
							    				    				<li><p class="body-copy-grey">
						<a href="/about-hiscox-insurance/" title="" target="_blank" >
						About Hiscox</a></p>
					</li>
										
    				
							    				    				<li><p class="body-copy-grey">
						<a href="/contact-us/" title="" target="_blank" >
						Contact us</a></p>
					</li>
										
    	    		
		 		</ul>			</div>		
					
											<ul class="topmenu">					
																																																																																																									<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist firstitem">
					<div class="leftcurve"></div><div class="leftmiddle">
						<a href="/" target="_self" class="topmenu-links">Home page </a> 
					</div>
				</li>							 
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																													<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist-sel">
					<a href="/small-business-insurance/" target="_self" class="topmenu-links-twoline">Small business insurance direct</a>																																																																																												            <usd:checkProductEligibility productContent="RequestCallBack,AddEnO,AddGL">
						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																																																							<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/why-choose-hiscox-insurance/" target="_self" class='menulinks'>Why choose Hiscox Insurance?</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/quotes/" target="_self" class='menulinks'>Great value insurance quotes</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/professional-liability-insurance/" target="_self" class='menulinks'>Professional Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddEnO"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/errors-and-omissions-insurance/" target="_self" class='menulinks'>Errors and Omissions Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddGL"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/general-liability-insurance/" target="_self" class='menulinks'>General Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="AddGL,AddBOP"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/business-owner-insurance/" target="_self" class='menulinks'>Business Owner Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																	<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="RequestCallBack"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/workers-compensation-insurance/" target="_self" class='menulinks'>Workers' Compensation Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																																																																																																																																																																												</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
					</usd:checkProductEligibility>
									</li>	
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																								<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist">
					<a href="/broker/" target="_self" class="topmenu-links-twoline">Business insurance through brokers</a>																																            <usd:checkProductEligibility productContent="RequestCallBack,AddEnO,AddGL">
						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																												<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_professional_insurance.htm" target="_self" class='menulinks'>Professional insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_property_insurance.htm" target="_self" class='menulinks'>Property insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_specialty.htm" target="_self" class='menulinks'>Specialty insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																					<usd:checkOccup occupContent="none"> 
		<usd:checkProductEligibility productContent="none"> 
															<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_broker.htm" target="_self" class='menulinks'>Broker center</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																						</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
					</usd:checkProductEligibility>
									</li>	
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																																																								<usd:checkOccup occupContent="all"> 
		<usd:checkProductEligibility productContent="all"> 
															<li class="menulist lastitem">
					<div class="rightmiddle">
						<a href="/insurance-products/" target="_self" class="topmenu-links">Products</a>					</div>
					<div class="rightcurve"></div>
				</li>
					</usd:checkProductEligibility>
	</usd:checkOccup>		
																																														</ul>
								</div>
    		<div class="header-hp-right">
    			
	
	
    							
																<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="red-call-to-action-small"><a href="http://www.hiscox.com"><span class="icon-open-in-new-window"><b>Group site</b></span></a> </p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/en/careers.aspx">Careers</a></p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/investors.aspx">Investors</a></p></div>
										
    	    		
		 		    		</div>
		</div>
		<!-- Header ends here-->
		<!-- Page container starts here-->
		<div class="page-container">
		<!--Breadcrumb starts here-->
					<div class="breadcrumb">
				
	<div class="brdcrmb-sub">
			                    	                    					<div class="breadcrumb-link"><a href="/">Home page</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/">Small business insurance direct</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-current"><span>Change coverage options</span></div>
			</div>
									<usd:checkEligibility eligibleContent="stateVariant,primarybusiness">
					<div class="occupation-details">
						<span class = "float-right"><object>
						<div class="occupation-info"><span>State:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="stateVariant" /></a></div>
						<div class="occupation-info"><span>Type of business:</span> <a href="/small-business-insurance/quote-and-buy/change-your-industry-and-or-zipcode"><usd:include sessionValue="primarybusiness" /></a></div>
						</object></span>
					</div>
					</usd:checkEligibility>
							</div>
		<!--Breadcrumb ends here-->
		<!--Left nav starts here-->
		<div class="leftnav-gap clear-both">
						
							        	    		    									<div class="leftmenu-main">
																																																					<usd:checkOccup occupContent="all">		<usd:checkProductEligibility productContent="all">							<div class="leftmenu-mainitem">
					<div class="mainitem-topleft" ></div><div class="mainitem-topmiddle" ></div><div class="mainitem-topright"></div>
						<div class="mainitem-middle">
							<a href="/small-business-insurance/" target="_self" class="main-link">Small business insurance direct</a>
						</div>
					<div class="mainitem-btmleft"></div><div class="mainitem-btmmiddle" ></div><div class="mainitem-btmright"></div>
				</div>
					</usd:checkProductEligibility>	</usd:checkOccup>													<div class="leftmenu">
					<div class="leftmenu-top"><div class="leftmenu-topleft"></div><div class="leftmenu-topmiddle"></div><div class="leftmenu-topright"></div></div>
						<div class="leftmenu-middle">
							<ul>
																	        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																</ul>
						</div>
					<div class="leftmenu-bottom"><div class="leftmenu-btmleft"></div><div class="leftmenu-btmmiddle"></div><div class="leftmenu-btmright"></div></div>
				</div>
			</div>
			    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    	    							
																																									<!--Left nav starts here and the div starts in the global template-->		
							<usd:checkOccup occupContent="it-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    														<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Customized coverage for technology businesses</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">Because we understand the specific risks you face we customize our coverage for you. Our Professional Liability policy includes $200,000 of software copyright infringement coverage for most Technology businesses.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    														<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Customized coverage for small businesses</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">Because we understand the specific risks you face we customize your coverage to your needs. This means you get the right protection and only pay for what you really need.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    														<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Customized coverage for marketing consultants</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">Because we understand the specific risks you face we customize our coverage for you. Our Professional Liability policy includes $200,000 of copyright infringement coverage for marketing consultants.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    														<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Customized coverage for small businesses</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">Because we understand the specific risks you face we customize your coverage to your needs. This means you get the right protection and only pay for what you really need.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                         										             										    														<div class="section-container grey-toplft " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Customized coverage for small businesses</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">Because we understand the specific risks you face we customize your coverage to your needs. This means you get the right protection and only pay for what you really need.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                         										             										    										    										    										            										            										            										    										    										    										    																																        			</usd:checkOccup>
		</div>
		<!--Left nav ends here-->
		<!--Content section starts here-->
				<div class="cnt-sctn no-bg">
			<div class="cnt-subsctn">
				<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
				<div class="middle-content">
					<div class="page-heading2">
																																																																						<usd:checkOccup occupContent="it-consulting" >
    	
    	<h1 class="page-heading-h1">Change coverage options</h1>    	
		</usd:checkOccup>
																																																											<usd:checkOccup occupContent="consulting" >
    	
    	<h1 class="page-heading-h1">Change coverage options</h1>    	
		</usd:checkOccup>
																																																											<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<h1 class="page-heading-h1">Change coverage options</h1>    	
		</usd:checkOccup>
																																																											<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<h1 class="page-heading-h1">Change coverage options</h1>    	
		</usd:checkOccup>
																																																											<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<h1 class="page-heading-h1">Change coverage options</h1>    	
		</usd:checkOccup>
												</div>
					<div class="clear"></div>
											<div class="middlecnt-container">
																																																																						<usd:checkOccup occupContent="it-consulting" >
    	
    	<p class="body-copy-grey">Use the radio buttons below to add or remove coverages from your quote.</p>    	
		</usd:checkOccup>
																																																											<usd:checkOccup occupContent="consulting" >
    	
    	<p class="body-copy-grey">Use the radio buttons below to add or remove coverages from your quote.</p>    	
		</usd:checkOccup>
																																																											<usd:checkOccup occupContent="marketing-pr-consulting" >
    	
    	<p class="body-copy-grey">Use the radio buttons below to add or remove coverages from your quote.</p>    	
		</usd:checkOccup>
																																																											<usd:checkOccup occupContent="default-occupation-known" >
    	
    	<p class="body-copy-grey">Use the radio buttons below to add or remove coverages from your quote.</p>    	
		</usd:checkOccup>
																																																											<usd:checkOccup occupContent="default-occupation-not-known" >
    	
    	<p class="body-copy-grey">Use the radio buttons below to add or remove coverages from your quote.</p>    	
		</usd:checkOccup>
								<div class="horz-line2"></div>
											</div>
				</div>
			<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div> 
		</div>
			<!--second container starts here-->
			<usd:checkFormTag formContent="Start"></usd:checkFormTag>
			<usd:questionTag  />
			<!--secondary container starts here-->			
											<!--secondary container ends here-->
			<usd:actionTag />
			<usd:trackingTag />
			<usd:checkFormTag formContent="End"></usd:checkFormTag>			
			<!--second container ends here-->						
			<!--tertiary container starts here-->			
																																																																<usd:checkOccup occupContent="it-consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																																																									<usd:checkOccup occupContent="consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																																																									<usd:checkOccup occupContent="marketing-pr-consulting" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																																																									<usd:checkOccup occupContent="default-occupation-known" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
																																																									<usd:checkOccup occupContent="default-occupation-not-known" >
    	<div class='cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
    	<p class="body-copy-grey-small">This information is provided to assist you in understanding the different types of coverages and does not modify the terms of any insurance policy. Coverage is subject to underwriting and may not be available in all states.</p>    	</div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
		</usd:checkOccup>
								<!--tertiary container ends here-->			
			</div>				
		<!--Content section ends here-->
		<!-- Right nav starts here-->
		<div class="rightnav-gap showcntct-num">
			<!--Checklist section starts here -->			
			<usd:checkOccup occupContent="it-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            														<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                         										             										    										    										    										            										            														<div class="section-container topright-container-small print" >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
																																																<div class="contact-child-item print">
						<div>
							<script language="javascript" type="text/javascript">
function funChat()
{
var theURL="";
theURL="http://smartchat.suth.com/hiscox/hiscox.htm";
var features="";
if (navigator.userAgent.indexOf("MSIE")>0)
{
features="width=350,height=520,resizable=0";
}
else if (navigator.userAgent.indexOf("Firefox")>0)
{
features="width=348,height=520,resizable=0";
}
else
{
features="width=361,height=520,resizable=0";
}
window.open(theURL,"HISCOXChat",features);
}
</script>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p><span class="grey-call-to-action-large"><b>888-202-2973</b></span></p><p class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</p><img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="128" width="167" />						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<p class="contact-box-link"><a href="/small-business-insurance/quote-and-buy/let-us-call-you/" title=""><span class="icon-phone">Let us call you</span></a></p>						</div>
					</div>
																			<div class="contact-child-item">
						<div>
							<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="contact-box-link"><a href="javascript:funChat()"><span class="icon-mouse">Chat online now</span></a></p></div>						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p class="contact-box-link"><a href="mailto:contactus@hiscoxusa.com"><span class="icon-email">Email</span></a></p>						</div>
					</div>
															</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			            										    										    										    										    																																        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    																		<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Not sure what you need?</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">One of our licensed advisors can help. They understand the specific risks you face and can help customize a policy for your business.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    									    		    		    		        		        		        		        			        														<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    																		<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Not sure what you need?</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">One of our licensed advisors can help. They understand the specific risks you face and can help customize a policy for your business.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    									    		    		    		        		        		        		        			        														<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    																		<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Not sure what you need?</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">One of our licensed advisors can help. They understand the specific risks you face and can help customize a policy for your business.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    									    		    		    		        		        		        		        			        														<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    																		<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Not sure what you need?</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">One of our licensed advisors can help. They understand the specific risks you face and can help customize a policy for your business.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    									    		    		    		        		        		        		        			        														<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button grey-button rab-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/quote-and-buy/lifestyle-questions" title="Help me choose" target="_self">Help me choose<span class="button-icon"></span></a>
				</span>
			</div>
				</div><div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/coverage-options" title="Get a Quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
        			             							    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                         										             										    																		<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
				
			<h3 class="box-heading-h3"><b>Not sure what you need?</b></h3>		<div class="horz-line"></div> 
		<p class="body-copy-grey">One of our licensed advisors can help. They understand the specific risks you face and can help customize a policy for your business.</p>						</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			    																										<div class="section-container smallcontainer " >
		<div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
					<div class="middle-content vertical-container">
					<div class="inlineLeft">
		<h2 class="red-call-to-action-medium-h2"><b>Tailor your business insurance quote</b></h2>	</div>
	<div class="button-container-vertical">
				<div class='header-btns'>
													<div class="submit-button red-button raw-icon">
				<span class="submit-button-end">
					<a class="submit-input" href="/small-business-insurance/get-a-quote-for-your-small-business" title="Get a quote" target="_self">Get a quote<span class="button-icon"></span></a>
				</span>
			</div>
				</div>	</div>
					</div>
				<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
	</div>
			 			    										            										            										            										    										    										    										    																																        			</usd:checkOccup>			
			<usd:checkOccup occupContent="it-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="marketing-pr-consulting" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-known" > 
					                                         			             									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	            									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																	    									    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																											    		    		    		        		        		        		            		            		            		                		                		                																	        			</usd:checkOccup>
			<usd:checkOccup occupContent="default-occupation-not-known" > 
					                                         														    			</usd:checkOccup>			
			<div class="verLogo-holder"><img src="/resources/images/secured-icon.gif" alt=""  title="Verisign" /></div>
		</div>
		<!--Checklist section ends here -->
		<!-- Right nav ends here-->
		<!-- the closing div for left nav is present in the page templates-->
		<div class="boiler-plate">
    		
	
	
	
    			</div>
		<!--Footer starts here -->
		<div class="footer-home">
			<div class="copy-right-home-box">
				
	
	
    							
																<p class="body-copy-grey">© Hiscox Inc</p>
										
    	    		
		 					</div>			
							
	
	
    							
							<div class="footer-dropdown-box">
                <label for="country">Not in the US?</label>
                <select name="country" id="country" onchange="if(this.options[this.selectedIndex].value!='')openChosenURL(this);">
                <option value="">Select your country</option>
                                                	                		<option value="http://www.hiscox.be">Belgium</option>
                	                		<option value="http://www.hiscox.fr">France</option>
                	                		<option value="http://www.hiscox.de">Germany</option>
                	                		<option value="http://www.hiscox.ie">Ireland</option>
                	                		<option value="http://www.hiscox.nl">Netherlands</option>
                	                		<option value="http://www.hiscox.pt">Portugal</option>
                	                		<option value="http://www.hiscox.es">Spain</option>
                	                		<option value="http://www.hiscox.co.uk">United Kingdom</option>
                	                				</select></div>
						
    	    		
		 								<ul class='footer-links-home-box'>
	
	
	
    							
																  <li><p class="body-copy-grey">
	                    <a href="/legal-notices/" title="" target="_blank" >
							Legal notices</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/site-map.html" title="" target="_blank" >
							Site map</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/privacy-policy/" title="" target="_blank" >
							Privacy policy</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/accessibility/" title="" target="_blank" >
							Accessibility</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/terms-of-use/" title="" target="_blank" >
							Terms of use</a></p>
					  </li>
										
    	    		
		 		</ul>		</div>
		<!--Footer ends here -->
		</div>
		<!-- Page container ends here-->
	</div>	<!--main container ends here-->	
<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/landingpageprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/screenbuilderprototype.js"></script>
<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>
<usd:includeScript/>
</body>
</html>