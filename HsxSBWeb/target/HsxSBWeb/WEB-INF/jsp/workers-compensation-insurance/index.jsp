<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Small Business Insurance - Professional Liability Insurance for Small Business | Hiscox USA</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<meta name="description" content="Get the right small business insurance with Hiscox. Customized coverage from $22.50/month. Get a fast online quote now. " />
<meta name="keywords" content="small business insurance, small business liability insurance, insurance for small business, liability insurance for small business" />
		<link rel="canonical" href="http://www.hiscoxusa.com/small-business-insurance/"/>	
<link href="/resources/css/global-reset.css" rel="stylesheet" type="text/css" media="all" />
<link href="/resources/css/ephox-dynamic.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="/resources/css/screenbuilder.css" type="text/css" media="all" />
<link href="/resources/css/rhythmyx.css" rel="stylesheet" type="text/css" media="all" />

<link rel="stylesheet" href="/resources/css/print.css" type="text/css" media="print"/>
<!--[if lte IE 9]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
<!--[if IE]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->

<script language="javascript" type="text/javascript" src="/resources/javascript/usacom_javascript.js"></script>
<style>
@media screen and (-webkit-min-device-pixel-ratio:0) 
{
	.home .txt-hlp-icn>.icon.help{top:-3.8em;left: -3em;}
}
</style>
</head>
<body onload="rotatingtext()">
	<!--main container starts here-->
	<div class="main-container">
		<!-- Header starts here-->
		<div class="header">			
		<div class="logo-home">
	
    							
			                                                             	            	            	            		<a href="/">
            			            				<img id='hiscox_logo' src='/resources/images/hiscox_logo.jpg' alt='Hiscox USA  - offering small business insurance online and insurance through brokers.' title='Hiscox USA  - offering small business insurance online and insurance through brokers.' border='0'  height="76"  width="139" />
            			                	</a>
						
    	    		
		 		</div>
		<div class="topmenu-container">
			<div class="utitlitylinks-hlder">
				<ul>
	
    							
																  <li><p class="body-copy-grey">
	                    <a href="/about-hiscox-insurance/" title="" target="_self" >
							About Hiscox</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/contact-us/" title="" target="_self" >
							Contact us</a></p>
					  </li>
										
    	    		
		 		</ul>			</div>
				
	
    							
																
											<div id="cse-search-form" style=" width: 395px; float: right; position: relative;right: -71px;">Loading</div> <script src="http://www.google.com/jsapi" type="text/javascript"></script> <script type="text/javascript"> google.load('search', '1', {language : 'en'}); google.setOnLoadCallback(function() { var customSearchControl = new google.search.CustomSearchControl('004599377379842757095:ffvrv_nlocy'); customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET); var options = new google.search.DrawOptions(); options.enableSearchboxOnly("http://uat.hiscoxusa.com/search/index.html"); customSearchControl.draw('cse-search-form', options); }, true); </script> <link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" /> <style type="text/css"> input.gsc-input { border-color: #E8E8E8; width:311px; color:#616161; font-family:Arial, Helvetica, sans-serif; font-size:12px; background:none !important; height:19px; } input.gsc-search-button { background:transparent url(/resources/images/search.png) no-repeat; width:76px; height:24px; border:0; font-size: 0px; color:transparent; *padding-left:76px !important; line-height:0px; cursor:pointer; } .gsc-control-searchbox-only div.gsc-clear-button { background:none; } table.gsc-search-box td.gsc-input {  padding-right: 16px;}</style>
															
    	    		
		 							
											<ul class="topmenu">					
																																																																																																													
														<li class="menulist firstitem">
					<div class="leftcurve"></div><div class="leftmiddle">
						<a href="/" target="_self" class="topmenu-links">Home page </a> 
					</div>
				</li>							 
						
																																																												
														<li class="menulist-sel">
					<a href="/small-business-insurance/" target="_self" class="topmenu-links-twoline">For small business owners</a>																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																												            						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																																																							
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/why-choose-hiscox-insurance/" target="_self" class='menulinks'>Why choose Hiscox insurance?</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																	
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/quotes/" target="_self" class='menulinks'>Great value insurance quotes</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																	
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/professional-liability-insurance/" target="_self" class='menulinks'>Professional Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																		
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/errors-and-omissions-insurance/" target="_self" class='menulinks'>Errors and Omissions Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																		
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/general-liability-insurance/" target="_self" class='menulinks'>General Liability Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																		
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/business-owner-insurance/" target="_self" class='menulinks'>Business Owner Insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																		
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/workers-compensation-insurance/" target="_self" class='menulinks'>Workers' Compensation</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																													
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/blog/" target="_self" class='menulinks'>Small business blog</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																																																																																																																																																																																																																							
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/small-business-insurance/contract-insurance-requirements/" target="_self" class='menulinks'>Contract insurance requirements</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																															</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
														</li>	
						
																																																																									
														<li class="menulist">
					<a href="/broker/" target="_self" class="topmenu-links-twoline">For insurance brokers</a>																																            						<div class="submenu">		
							<div class="submenu-topmiddle"></div><div class="submenu-topright"></div>
								<div class="submenu-middle">
									<ul>	
																																																																							
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_professional_insurance.htm" target="_self" class='menulinks'>Professional insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																	
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_property_insurance.htm" target="_self" class='menulinks'>Property insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																	
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_specialty.htm" target="_self" class='menulinks'>Specialty insurance</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																																																																	
														<li>
					<div class="submenulist-topleft"></div><div class="submenulist-topmiddle"></div><div class="submenulist-topright"></div>
						<div class="submenulist-middle">
							<a href="/broker/usa_broker.htm" target="_self" class='menulinks'>Broker document center</a>						</div>
					<div class="submenulist-btmleft"></div><div class="submenulist-btmmiddle"></div><div class="submenulist-btmright"></div>	
				</li>
						
																							</ul>
								</div>
							<div class="submenu-btmleft"></div><div class="submenu-btmmiddle"></div><div class="submenu-btmright"></div>
						</div>
														</li>	
						
																																																																									
														<li class="menulist lastitem">
					<div class="rightmiddle">
						<a href="/insurance-products/" target="_self" class="topmenu-links">Products</a>					</div>
					<div class="rightcurve"></div>
				</li>
						
																																																		</ul>
								</div>
    		<div class="header-hp-right">
    			
	
    							
																<div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p class="red-call-to-action-small"><a href="http://www.hiscox.com"><span class="icon-open-in-new-window"><b>Group site</b></span></a>�</p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/en/careers.aspx">Careers</a></p><p class="body-copy-grey-small"><a href="http://www.hiscox.com/investors.aspx">Investors</a></p></div>
										
    	    		
		 		    		</div>
		</div>
		<!-- Header ends here-->
		<!-- Page container starts here-->
		<div class="page-container">
		<!--Breadcrumb starts here-->
				
							    					

				    		    						<!-- Page container starts here--
			<!--Breadcrumb starts here-->
						<div class="contact-number-home">
				<p class="grey-call-to-action-large"><b>866-283-7545</b></p>    				<form method="post"> 
    					
													<div class="header-btns">							
				<div class="submit-button grey-button rab-icon">
					<span class='submit-button-end'>
						<a class="submit-input" href="/small-business-insurance/quote-and-buy/retrieve-a-quote/" title="Retrieve a quote" target="_self">Retrieve a quote<span class="button-icon"></span></a>
					</span>
				</div>
			</div>
				    				</form>	
			</div>
				<!--Breadcrumb ends here--><!--Content section starts here-->
				<div class="cnt-sctn-home">
																				
				<!-- New tab text and content control -->
				
									
					<link href="/resources/css/3-tabs.css" type="text/css" rel="stylesheet" media="screen"/>

<script type="text/javascript">
  var panels = new Array('panel1','panel2','panel3');
  var selectedTab = null;
</script>

					<div class="banner-img-container" style="height:auto;">
					
    					<div id="tabs">
    						                                    							<table class="tab"  onmouseover="return event.returnValue = showPanel(this, 'panel1', panels);" id="tab1" ><tr><td valign="middle" ><div xmlns:o="urn:www.microsoft.com/office" xmlns:st1="urn:www.microsoft.com/smarttags" xmlns:st2="urn:www.microsoft.com/smarttags2" xmlns:w="urn:www.microsoft.com/word" xmlns:x="urn:www.microsoft.com/excel"><p>What fields do we cover?</p></div></td></tr></table>

    						                                    							<table class="tab"  onmouseover="return event.returnValue = showPanel(this, 'panel2', panels);" ><tr><td valign="middle" ><p>Why choose Hiscox?</p></td></tr></table>

    						                                    							<table class="tab"  onmouseover="return event.returnValue = showPanel(this, 'panel3', panels);" ><tr><td valign="middle" ><p>Hiscox<br />TV ad</p></td></tr></table>

    						    					</div>	

						    						<div class="panel" id="panel1" >
														
														<img src="/resources/images/content-bg-white.gif" alt="" />
    						<div class="content-overlay">
    							<table border="1" cellpadding="0" cellspacing="0" width="100%"><tr><td width="34%"><a href="http://www.hiscoxusa.com/small-business-insurance/it-insurance/" target="_self" shape="rect"><img id="Landing Page Icon - IT Consultant" src="/shared-images/Landing-Page-Icon---IT-100px-w-text-blk.png" alt="Hiscox offers tailored insurance for IT consultants." title="Hiscox offers tailored insurance for IT consultants." border="0" height="110" width="115" /></a></td><td width="33%"><a href="http://www.hiscoxusa.com/small-business-insurance/professional-business-insurance/web-design-insurance/" target="_self" shape="rect"><img id="Landing Page Icon - Designer" src="/shared-images/Landing-Page-Icon---Designer-100px-w-text-blk.png" alt="Hiscox offers tailored web and graphic designer insurance to fit your needs." title="Hiscox offers tailored web and graphic designer insurance to fit your needs." border="0" height="110" width="115" /></a></td><td width="33%"><a href="http://www.hiscoxusa.com/small-business-insurance/business-consultant-insurance/" target="_self" shape="rect"><img id="Landing Page Icon - Consultant" src="/shared-images/Landing-Page-Icon---Consultant-100px-w-text-blk.png" alt="Hiscox customizes insurance for business consultants." title="Hiscox customizes insurance for business consultants." border="0" height="110" width="115" /></a></td></tr><tr><td width="34%"><a href="http://www.hiscoxusa.com/small-business-insurance/real-estate-agent-insurance/" target="_self" shape="rect"><img id="Landing Page Icon - Real Estate Agent" src="/shared-images/Landing-Page-Icon---Realestate-100px-w-text-blk.png" alt="Hiscox tailors coverage for real estate agents and property managers." title="Hiscox tailors coverage for real estate agents and property managers." border="0" height="110" width="115" /></a></td><td width="33%"><a href="http://www.hiscoxusa.com/small-business-insurance/professional-business-insurance/photographers-insurance/" target="_self" shape="rect"><img id="Landing Page Icon - Photographer" src="/shared-images/Landing-Page-Icon---Photographer-100px-w-text-blk.png" alt="Get photographers insurance from Hiscox customized to the specific risks you face." title="Get photographers insurance from Hiscox customized to the specific risks you face." border="0" height="110" width="115" /></a></td><td width="33%"><a href="http://www.hiscoxusa.com/small-business-insurance/professional-business-insurance/" target="_self" shape="rect"><img id="Landing Page Icon - And More" src="/shared-images/Landing-Page-Icon---And-Many-More-100px-w-text-blk.png" alt="Hiscox tailored small business insurance for professional services businesses." title="Hiscox tailored small business insurance for professional services businesses." border="0" height="110" width="115" /></a></td></tr></table><br /><br /><a href="http://www.hiscoxusa.com/small-business-insurance/get-a-quote-for-your-small-business/" target="_self" shape="rect"><img id="Landing Page Button - Get the right insurance right now" src="/shared-images/get-a-quote-new-creative.png" alt="Get a small business insurance quote online." title="Get a small business insurance quote online." border="0" height="35" width="365" /></a>
    						</div>
							    						</div>
    					    						<div class="panel" id="panel2" style="display:none;overflow:hidden;">
														
															<div>
	            	    		<img id="why-choose-hiscox image" src="/shared-images/why-choose-hiscox-image.png" alt="We specialize in insurance for professional services businesses and tailor policies to your business risks." title="We specialize in insurance for professional services businesses and tailor policies to your business risks." height="324" width="444" />	
		<div class="banner-img-txtoverlay">
		<div style="margin-top:-37px;height:324px;padding-left:0px;width:420px;">
<h1 style="color:#616161; font:arial; font-size:16px; font-weight:700; padding-top:0;padding-right:0;padding-bottom:19px;padding-left:15px;">Small Business Insurance Specialists</h1>

<h2 class="red-call-to-action-medium-h2" style="padding-bottom:4px"><strong>Customized Coverage</strong></h2>
<p class="body-copy-grey" style="color:#616161"><strong>We specialize in insurance for professional services businesses and tailor policies to your business risks.
</strong></p>
<p>�</p>

<h2 class="red-call-to-action-medium-h2" style="padding-bottom:4px"><strong>Great Value</strong></h2>
<p class="body-copy-grey" style="color:#616161"><strong>Quote and buy direct from Hiscox online in real time or over the phone with one of our licensed advisors.
</strong></p>
<p>�</p>

<h2 class="red-call-to-action-medium-h2" style="padding-bottom:4px"><strong>Financial Strength</strong></h2>

<p class="body-copy-grey" style="color:#616161"><strong>Hiscox Insurance Company Inc. is rated 'A' (Excellent) by A.M. Best. The Hiscox Group is a global insurer with over 100 years experience insuring businesses.</strong></p>


<div style="padding-top:29px;text-align:right;width:420px;"><a href="/small-business-insurance/why-choose-hiscox-insurance/" shape="rect"><img width="105" height="24" src="/shared-images/learn_more_btnwht.png" border="0" title="" alt="Learn more" id="Learn more" /></a></div>
</div>	</div>
	</div>
							    						</div>
    					    						<div class="panel" id="panel3" style="display:none;overflow:hidden;">
														
														<img src="/resources/images/content-bg-white.gif" alt="" />
    						<div class="content-overlay">
    							<p>�</p><p>�</p><div style="width:100%;align:center">
			<object width="390" height="232"><param name="movie" value="http://www.youtube.com/v/QR2fsK0bau8?version=3&amp;hl=en_US&amp;rel=0" /><param name="allowFullScreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="wmode" value="transparent" /><embed src="http://www.youtube.com/v/QR2fsK0bau8?version=3&amp;hl=en_US&amp;rel=0" type="application/x-shockwave-flash" width="390" height="232" allowscriptaccess="always" allowfullscreen="true" wmode="transparent"></embed></object>		</div>
    						</div>
							    						</div>
    											
					</div>
								
				<script type="text/javascript">
				showPanel(document.getElementById('tab1'), 'panel1', panels);
				</script>
				<!-- New tab text and content control -->
					
					<!--Dynamic section starts here -->
						<div class="section-container dynamic-content-home">
							<div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>
							<div class="middle-content">								<h1 class="red-call-to-action-large-h1">GET A FAST, FREE QUOTE ONLINE</h1>	
								<h2 class="grey-heading-h2"><b>Customized coverage from $22.50/mo</b></h2>													
								<!--Question group starts here -->
								
								
								
								
								
								
								
								
																																								<!--Question group ends here -->
								
<!--Question group starts here -->
								
								<form name = "ScreenBuilderForm" onSubmit="return checkForm('Home_screen_id');" action="/small-business-insurance/quote-and-buy/brochureware"  id = "ScreenBuilderForm" method="post" commandName="screen">
								
																																																																
								<div class='screen-container home' id='Home_screen_id'><input
	type='submit' class='hide-question default-navigation-button'
	name='action_StartQuote_button' value='' />
<div class='group grey round-top round-bottom'>
<div class="group-top">
<div class="right-container">
<div class="left-container">
<div class="middle-container"></div>
</div>
</div>
</div>
<div class="group-middle">
<div class="right-container">
<div class="left-container">
<div class="middle-container">
<fieldset id='HM_1_question_id'
	class='group-container screen-reader-only'>
<ol>
	<li class='group transparent show-legend' id='hm_1.1_question_id'>
	<div class="group-top">
	<div class="right-container">
	<div class="left-container">
	<div class="middle-container"></div>
	</div>
	</div>
	</div>
	<div class="group-middle">
	<div class="right-container">
	<div class="left-container">
	<div class="middle-container">
	<fieldset id='HM_1.1_question_id'
		class='group-container screen-reader-only'><legend><span
		class='group-text'><object>
		<h2>Step 1 - basic details</h2>
	</object></span></legend>
	<ol>
		<li class='question-container drp ' id='state_question_id'>
		<div class='question-text'><label for='state_dropdown_id'
			id='statelabel_label_Id'>Main business location</label></div>
		<div class='control'><select class='select-type-dropdown'
			name='state' id='state_dropdown_id'>
			<option id='state1' value=""></option>
			<option id='state2' value="AL">Alabama</option>
			<option id='state3' value="AK">Alaska</option>
			<option id='state4' value="AZ">Arizona</option>
			<option id='state5' value="AR">Arkansas</option>
			<option id='state6' value="CA">California</option>
			<option id='state7' value="CO">Colorado</option>
			<option id='state8' value="CT">Connecticut</option>
			<option id='state9' value="DE">Delaware</option>
			<option id='state10' value="DC">District Of Columbia</option>
			<option id='state11' value="FL">Florida</option>
			<option id='state12' value="GA">Georgia</option>
			<option id='state13' value="HI">Hawaii</option>
			<option id='state14' value="ID">Idaho</option>
			<option id='state15' value="IL">Illinois</option>
			<option id='state16' value="IN">Indiana</option>
			<option id='state17' value="IA">Iowa</option>
			<option id='state18' value="KS">Kansas</option>
			<option id='state19' value="KY">Kentucky</option>
			<option id='state20' value="LA">Louisiana</option>
			<option id='state21' value="ME">Maine</option>
			<option id='state22' value="MD">Maryland</option>
			<option id='state23' value="MA">Massachusetts</option>
			<option id='state24' value="MI">Michigan</option>
			<option id='state25' value="MN">Minnesota</option>
			<option id='state26' value="MS">Mississippi</option>
			<option id='state27' value="MO">Missouri</option>
			<option id='state28' value="MT">Montana</option>
			<option id='state29' value="NE">Nebraska</option>
			<option id='state30' value="NV">Nevada</option>
			<option id='state31' value="NH">New Hampshire</option>
			<option id='state32' value="NJ">New Jersey</option>
			<option id='state33' value="NM">New Mexico</option>
			<option id='state34' value="NY">New York</option>
			<option id='state35' value="NC">North Carolina</option>
			<option id='state36' value="ND">North Dakota</option>
			<option id='state37' value="OH">Ohio</option>
			<option id='state38' value="OK">Oklahoma</option>
			<option id='state39' value="OR">Oregon</option>
			<option id='state40' value="PA">Pennsylvania</option>
			<option id='state41' value="RI">Rhode Island</option>
			<option id='state42' value="SC">South Carolina</option>
			<option id='state43' value="SD">South Dakota</option>
			<option id='state44' value="TN">Tennessee</option>
			<option id='state45' value="TX">Texas</option>
			<option id='state46' value="UT">Utah</option>
			<option id='state47' value="VT">Vermont</option>
			<option id='state48' value="VA">Virginia</option>
			<option id='state49' value="WA">Washington</option>
			<option id='state50' value="WV">West Virginia</option>
			<option id='state51' value="WI">Wisconsin</option>
			<option id='state52' value="WY">Wyoming</option>
		</select></div>
		<div id='state_additional1_id' class='additional-text'><span>
		<table width="135" border="0" cellpadding="2" cellspacing="0"
			title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications.">
			<tr>
				<td width="135" align="center" valign="top"><script
					type="text/javascript"
					src="https://seal.verisign.com/getseal?host_name=www.hiscoxusa.com&size=S&use_flash=YES&use_transparent=YES&lang=en"></script></td>
			</tr>
		</table>
		</span></div>
		</li>		<li class='question-container drp lbl-max txt-hlp-icn'
			id='primarybusiness_question_id'>
		<div class='question-text'><label
			for='primarybusiness_dropdown_id' id='primarybusinesslabel_label_Id'>Business description</label></div>
		<div class='control'><select onChange="showHideExplain(this);" class='select-type-dropdown'
			name='primarybusiness' id='primarybusiness_dropdown_id'>
			<option value="" id="primarybusiness1"></option><option value="Accounting" id="primarybusiness2">Accounting</option><option value="Actuarial services" id="primarybusiness3">Actuarial services</option><option value="Acupressure services" id="primarybusiness4">Acupressure services</option><option value="Acupuncture services" id="primarybusiness5">Acupuncture services</option><option value="Advertising" id="primarybusiness5">Advertising</option><option value="Answering/paging services" id="primarybusiness6">Answering/paging services</option><option value="Application development" id="primarybusiness7">Application development</option><option value="Application service provider" id="primarybusiness8">Application service provider</option><option value="Architecture" id="primarybusiness7">Architecture</option><option value="Art therapy" id="primarybusiness8">Art therapy</option><option value="Auctioneering" id="primarybusiness9">Auctioneering</option><option value="Audiology" id="primarybusiness10">Audiology</option><option value="Beautician/cosmetology services" id="primarybusiness11">Beautician/cosmetology services</option><option value="Bookkeeping" id="primarybusiness12">Bookkeeping</option><option value="Brand consultant" id="">Brand consultant</option><option value="Building inspection" id="primarybusiness13">Building inspection</option><option value="Business consulting" id="primarybusiness14">Business consulting</option><option value="Business manager services" id="primarybusiness15">Business manager services</option><option value="Civil engineering" id="primarybusiness16">Civil engineering</option><option value="Claims adjusting" id="primarybusiness17">Claims adjusting</option><option value="Computer consulting" id="primarybusiness20">Computer consulting</option><option value="Computer programming services" id="primarybusiness21">Computer programming services</option><option value="Computer system/network developer" id="primarybusiness22">Computer system/network developer</option><option value="Control systems integration/automation" id="primarybusiness18">Control systems integration/automation</option><option value="Court reporting" id="primarybusiness19">Court reporting</option><option value="Credit counseling" id="primarybusiness20">Credit counseling</option><option value="Dance therapy" id="primarybusiness21">Dance therapy</option><option value="Data processing" id="primarybusiness28">Data processing</option><option value="Database designer" id="primarybusiness29">Database designer</option><option value="Dietician/nutrition" id="primarybusiness22">Dietician/nutrition</option><option value="Digital marketing" id="">Digital marketing</option><option value="Direct marketing" id="primarybusiness23">Direct marketing</option><option value="Document preparation" id="primarybusiness24">Document preparation</option><option value="Draftsman (including CAD/CAM)" id="primarybusiness25">Draftsman (including CAD/CAM)</option><option value="Drama therapy" id="primarybusiness26">Drama therapy</option><option value="Education consulting" id="primarybusiness27">Education consulting</option><option value="Electrical engineering" id="primarybusiness28">Electrical engineering</option><option value="Engineering" id="primarybusiness29">Engineering</option><option value="Environmental engineering" id="primarybusiness30">Environmental engineering</option><option value="Esthetician services" id="primarybusiness31">Esthetician services</option><option value="Event planning/promotion" id="primarybusiness32">Event planning/promotion</option><option value="Executive placement" id="">Executive placement</option><option value="Expert witness services" id="primarybusiness33">Expert witness services</option><option value="Financial auditing or consulting" id="primarybusiness34">Financial auditing or consulting</option><option value="First aid and CPR training" id="primarybusiness35">First aid and CPR training</option><option value="Graphic design" id="primarybusiness36">Graphic design</option><option value="Barber/hair stylist services" id="primarybusiness37">Hair stylist/barber services</option><option value="Human Resources (HR) consulting" id="primarybusiness38">Human Resources (HR) consulting</option><option value="Hypnosis" id="primarybusiness39">Hypnosis</option><option value="Industrial engineering" id="primarybusiness40">Industrial engineering</option><option value="Interior design" id="primarybusiness41">Interior design</option><option value="Investment advice" id="primarybusiness42">Investment advice</option><option value="IT consulting" id="primarybusiness43">IT consulting</option><option value="IT project management" id="primarybusiness52">IT project management</option><option value="IT software/hardware training services" id="primarybusiness53">IT software/hardware training services</option><option value="Landscape architect" id="primarybusiness44">Landscape architect</option><option value="Legal services" id="primarybusiness45">Legal services</option><option value="Life/career/executive coaching" id="primarybusiness46">Life/career/executive coaching</option><option value="Management consulting" id="primarybusiness47">Management consulting</option><option value="Marketing/media consulting" id="primarybusiness48">Marketing/media consulting</option><option value="Market research" id="">Market research</option><option value="Marriage and family therapy" id="primarybusiness49">Marriage and family therapy</option><option value="Massage therapy" id="primarybusiness50">Massage therapy</option><option value="Medical billing" id="primarybusiness51">Medical billing</option><option value="Mental health counseling" id="primarybusiness52">Mental health counseling</option><option value="Mortgage brokering/banking" id="primarybusiness53">Mortgage brokering/banking</option><option value="Music therapy" id="primarybusiness54">Music therapy</option><option value="Nail technician services" id="primarybusiness55">Nail technician services</option><option value="Notary services" id="primarybusiness56">Notary services</option><option value="Occupational therapy" id="primarybusiness57">Occupational therapy</option><option value="Personal concierge/assistant" id="primarybusiness58">Personal concierge/assistant</option><option value="Personal training (health and fitness)" id="primarybusiness59">Personal training (health and fitness)</option><option value="Photography" id="primarybusiness60">Photography</option><option value="Process engineering" id="primarybusiness61">Process engineering</option><option value="Process server" id="primarybusiness62">Process server</option><option value="Property management" id="primarybusiness63">Property management</option><option value="Project management" id="">Project management</option><option value="Psychology" id="primarybusiness64">Psychology</option><option value="Public relations" id="primarybusiness65">Public relations</option><option value="Real estate agent/broker" id="primarybusiness66">Real estate agent/broker</option><option value="Recruiting (employment placements)" id="primarybusiness67">Recruiting (employment placements)</option><option value="Research consulting" id="primarybusiness68">Research consulting</option><option value="Resume consulting" id="primarybusiness69">Resume consulting</option><option value="Search engine services (SEO/SEM)" id="">Search engine services (SEO/SEM)</option><option value="Social media consultant" id="">Social media consultant</option><option value="Social work services" id="primarybusiness70">Social work services</option><option value="Software development" id="primarybusiness82">Software development</option><option value="Speech therapy" id="primarybusiness71">Speech therapy</option><option value="Stock brokering" id="primarybusiness72">Stock brokering</option><option value="Strategy consultant">Strategy consultant</option><option value="Substance abuse counseling" id="primarybusiness73">Substance abuse counseling</option><option value="Talent agency" id="primarybusiness74">Talent agency</option><option value="Tax preparation" id="primarybusiness75">Tax preparation</option><option value="Technology services" id="primarybusiness76">Technology services</option><option value="Training (business, vocational or life skills)" id="primarybusiness77">Training (business, vocational or life skills)</option><option value="Translating/interpreting" id="primarybusiness78">Translating/interpreting</option><option value="Transportation engineering" id="primarybusiness79">Transportation engineering</option><option value="Travel agency" id="primarybusiness80">Travel agency</option><option value="Trustee" id="primarybusiness81">Trustee</option><option value="Tutoring" id="primarybusiness82">Tutoring</option><option value="Value added reseller of computer hardware" id="primarybusiness95">Value added reseller of computer hardware</option><option value="Website design" id="primarybusiness96">Website design</option><option value="Yoga/pilates instruction" id="primarybusiness83">Yoga/pilates instruction</option><option value="Other architecture, engineering & design services" id="primarybusiness68">Other architecture, engineering & design services</option><option value="Other consulting services">Other consulting services</option><option value="Other creative services">Other creative services</option><option value="Other financial services" id="">Other financial services</option><option value="Other health, beauty & wellness services">Other health, beauty & wellness services</option><option value="Other legal services">Other legal services</option><option value="Other marketing/PR services">Other marketing/PR services</option><option value="Other professional services">Other professional services</option><option value="Other real estate services">Other real estate services</option><option value="Other technology services" id="primarybusiness68">Other technology services</option><option value="NONE OF THE ABOVE" id="primarybusiness84">NONE OF THE ABOVE</option>
		</select></div>		<div class="additional-text">
			<span>Pick the option that best describes your primary business activity.</span>
		</div>		<div id='primarybusiness_help_id' class='icon help' style="top:-3.6em;left: -3em;*top:0.3em;"><a
			target='_blank'
			href='quote-and-buy/help-text?pageName=Home&&questionCode=PrimaryBusiness'
			class='showTip L3 HelpHref' id='primarybusiness_anchor_id'><span>Not sure?</span></a><span
			id='primarybusiness_value_id' class='hide-question'>
		<p>The occupation categories listed are intended to be broad.
		Please select the category that best represents your business. In the
		quote process we will ask additional questions to ensure we understand
		your specific business and customize coverage to your needs.</p>
		<p>As we specialize in insuring professional services businesses
		with less than 10 employees many of our customers fall into the
		following groups.</p>
		<p><strong>Technology services or IT Consulting:</strong><br />
		This includes website design; web site hosting; software programming;
		systems development; application development; network design,
		implementation, and maintenance; IT project management; hardware
		support; technology training; and application service providers.</p>
		<p><strong>Business or management consulting:</strong><br />
		This includes advising on business operations, strategy,
		organizational structure, human resources, marketing and sales,
		professional trainers, systems or project management.</p>
		<p><strong>Marketing/media consulting:</strong><br />
		This includes developing, designing, researching and advising on
		marketing plans; developing marketing campaigns; advising on product
		development and testing; media planning; creative development; and
		project management.</p>
		<p>We also specialize in other similar professional service
		businesses like claims adjusting, photography, graphic designers,
		personal assistants and training. If you feel your profession does not
		fit in any category listed please select &#34;Other professional
		service&#34; and provide a description.</p>
		<p>Please note that we have no products available for businesses
		like tradesmen, restaurants, retail shops, building contractors and
		the like.</p>
		</span></div>
		</li>		<li class='question-container txt narr lbl-max ctl-max'
			id='primarybusinessexplain_question_id'>
		<noscript>
		<div class='non-js'>Please only answer this question if you have
		answered 'None of the above'</div>
		</noscript>
		<div class='question-text'><label
			for='primarybusinessexplain_text_id'
			id='primarybusinessexplainlabel_label_Id'>Please explain
		your primary type of business</label></div>
		<div class='control'><textarea name='primarybusinessexplain'
			maxlength='2000' id='primarybusinessexplain_text_id' rows='7'
			cols='1' class='input-type-textarea'></textarea></div>
		</li>
	</ol>
	</fieldset>
	</div>
	</div>
	</div>
	</div>
	<div class="group-bottom">
	<div class="right-container">
	<div class="left-container">
	<div class="middle-container"></div>
	</div>
	</div>
	</div>
	</li>
	<li class='group transparent show-legend' id='hm_1.2_question_id'>
	<div class="group-top">
	<div class="right-container">
	<div class="left-container">
	<div class="middle-container"></div>
	</div>
	</div>
	</div>
	<div class="group-middle">
	<div class="right-container">
	<div class="left-container">
	<div class="middle-container">
	<fieldset id='HM_1.2_question_id'
		class='group-container screen-reader-only'><legend><span
		class='group-text'><object>
		<h2>Step 2 - your choices</h2>
	</object></span></legend>
	<ol>
		<li class='question-container red-button raw-icon float-right mag'
			id='startquote_question_id'>
		<div class='submit-button'><span class='submit-button-end'><input
			type='submit' class='submit-input' id='startquote_button_id'
			name='action_StartQuote_button' value='Get a quote'
			title='Start Quote' /><span class='button-icon'></span></span></div>
		</li>
		<li class='question-container grey-button rab-icon float-right'
			id='helpmechoose_question_id'>
		<div class='submit-button'><span class='submit-button-end'><input
			type='submit' class='submit-input' id='helpmechoose_button_id'
			name='action_HelpMeChoose_button' value='Help me choose'
			title='Help Me Choose' /><span class='button-icon'></span></span></div>
		</li>
	</ol>
	</fieldset>
	</div>
	</div>
	</div>
	</div>
	<div class="group-bottom">
	<div class="right-container">
	<div class="left-container">
	<div class="middle-container"></div>
	</div>
	</div>
	</div>
	</li>
</ol>
</fieldset>
</div>
</div>
</div>
</div>
<div class="group-bottom">
<div class="right-container">
<div class="left-container">
<div class="middle-container"></div>
</div>
</div>
</div>
</div>
<input type='hidden' name='jsactionnameshiddenstringid_hidden'
	id='jsactionnameshiddenstringid' value='StartQuote~HelpMeChoose' /><input
	type='hidden' name='jsdependencyhiddenstringid_hidden'
	id='jsdependencyhiddenstringid'
	value='primarybusiness_dropdown_id~onchange~none of the above; please explain~primarybusinessexplain_question_id~blank' /><input
	type='hidden' name='jstooltiphiddenstringid_hidden'
	id='jstooltiphiddenstringid' value='primarybusiness' /><input
	type='hidden' name='contextpath_hidden' id='contextpath'
	value='/resources' /><input type='hidden'
	name='jscalendarhiddenstringid_hidden' id='jscalendarhiddenstringid'
	value='' /><input type='hidden' name='jsbuttonendsflagid_hidden'
	id='jsbuttonendsflagid' value='YES' /><input type='hidden'
	name='isJSEnabled' id='isJSEnabled' value='NO' /></div>										
																</form>
								<!--Question group ends here -->
								<div class='needHlp-home-holder'>		<div class='inl-fl-left'>
                										<img id='contact-woman-small' src='/shared-images/contact-woman-small-landing-page-2.PNG' alt='Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!' title='Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!' border='0'  height="36"  width="24" />
					</div>		<div class="needHlp-txt">		<h2 class="red-call-to-action-medium-h2"><b>Want advice from a licensed agent?</b></h2><p class="body-copy-grey">Our team can help you get the right coverage.<br />Call 866-283-7545 (Mon-Fri, 8am-10pm EST).</p>        </div>	</div>								<div id='secondarycontent'></div>						</div>
							<!--Dynamic section ends here -->
						<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
					</div>				
					<div class="clear"></div>
						<!--Product Boxes starts here -->
					<ul class="product-list">
												<li><div class='section-container product-box'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content vertical-container'>
		<h2 class="grey-heading-h2-with-line"><b>Insurance by profession</b></h2><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/professional-business-insurance/it-insurance/">IT/Technology</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/professional-business-insurance/marketing-insurance/">Marketing/PR/Design</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">Consulting/Training/Research</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/professional-business-insurance/property-management-insurance/">Property Management insurance</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/professional-business-insurance/real-estate-agent-insurance/">Real Estate Agent insurance</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/">Health, Beauty and Wellbeing</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/professional-business-insurance/" title="">Other professional services</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/professional-business-insurance/tradesman-insurance/">Tradesmen, Retail and more</a></p>	</div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div></li><li><div class='section-container product-box'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content vertical-container'>
		<h2 class="grey-heading-h2-with-line"><b>Small business insurance</b></h2><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/professional-liability-insurance/" title="">Professional Liability insurance</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/errors-and-omissions-insurance/" title="">Errors and Omissions insurance</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/general-liability-insurance/" title="">General Liability insurance</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/business-owner-insurance/" title="">Business Owner's Policy (BOP)</a>�</p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/workers-compensation-insurance/" title="">Workers' Compensation insurance</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/contract-insurance-requirements/">Need coverage for a client contract?</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/state/" title="">Small business insurance by state</a></p>	</div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div></li><li><div class='section-container product-box'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content vertical-container'>
		<h2 class="grey-heading-h2-with-line"><b>Useful information</b></h2><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/why-choose-hiscox-insurance/" title="">Why choose Hiscox insurance?</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/quotes/" title="">Great value insurance quotes</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/business-insurance-claims/" title="">Report a claim</a></p><p class="body-copy-grey-with-padding"><a href="http://leapyear.hiscoxusa.com" target="_blank">Watch Leap Year episodes</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/newsroom/" title="">Hiscox Newsroom</a></p><p class="body-copy-grey-with-padding"><a href="http://www.hiscoxusa.com/small-business-insurance/blog/">Small business blog</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/refer-a-friend/">Refer a friend to Hiscox</a></p><p class="body-copy-grey-with-padding"><a href="/small-business-insurance/hiscox-affiliate-program/">Become a Hiscox Affiliate Partner</a></p>	</div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div></li>						<li class='last'><div class='section-container rotating-textbox'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content vertical-container'>
																													<div id="rotatingTextContainer1" show-style">
					<div itemscope="" itemtype="http://schema.org/InsuranceAgency"><h2 class="grey-heading-h2-with-line"><b>Real <span itemprop="name">Hiscox</span> Reviews</b></h2><a href="http://www.hiscoxusa.com/small-business-insurance/business-insurance-reviews/" target="_self" shape="rect"><img id="96 percent snippet - landing page promo" src="/shared-images/96-percent-snippet-landing-page-2.jpg" alt="96% of those surveyed would recommend us" title="96% of those surveyed would recommend us" border="0" height="87" width="220" /></a><div itemprop="AggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating"><p class="body-copy-grey"><br /><b>Rated <span itemprop="ratingValue">4.9</span>/5 for overall service<br /></b></p><br /><p class="body-copy-grey"><a href="http://www.hiscoxusa.com/small-business-insurance/business-insurance-reviews/"><span itemprop="reviewCount" style=" text-decoration: underline; font-weight: bold;">6,671</span> <span style=" text-decoration: underline; font-weight: bold;">reviews</span></a><br /><br /></p></div></div>				</div>						
						<input type="hidden" id="maxRotatingTextItems" value="1"/>			
			</div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div></li>					</ul>
						<!--Product Boxes end here -->
				</div>     
				<!--Content section ends here-->
				<!--Page container ends here-->
		<!-- the closing div for left nav is present in the page templates-->
		<div class="boiler-plate">
			 <div class="top"><div class="right"><div class="left"><div class="middle">	</div></div></div></div>    
				<div class="middle-content"> 
					<div class="middlecnt-container">
							
	
	
    							
																<h2 class="red-call-to-action-medium-h2"><b>About Hiscox small business insurance</b></h2><ul class="list-bulleted-body-copy-grey"><li><b>Small business specialists</b> - focus on insuring professional businesses with less than 10 employees.</li><li><b>Customized coverage</b> - we help customize coverage to your specific needs. No more, no less.</li><li><b>Great value</b> - buy direct from Hiscox and enjoy excellent service and real value for money.</li><li><b>Money back guarantee</b> - be confident in your purchase with our 14 day guarantee.</li><li><b>Financial strength</b> - Hiscox Insurance Company Inc. is 'A' rated (Excellent) by A.M. Best.</li></ul>
										
    	    		
		 		</div>
				</div>
			<div class="bottom clear-both"><div class="right"><div class="left"><div class="middle"></div></div></div></div>
		</div>

	
			<!--Footer starts here -->
		<div class="footer-home">
			<div class="copy-right-home-box">
				
	
    							
																<p class="body-copy-grey-small">� 2013 Hiscox Inc. All rights reserved<br />Underwritten by Hiscox Insurance Company Inc.</p>
										
    	    		
		 					</div>			
							
	
    							
							<div class="footer-dropdown-box">
                <label for="country">Not in the US?</label>
                <select name="country" id="country" onchange="if(this.options[this.selectedIndex].value!='')openChosenURL(this);">
                <option value="">Select your country</option>
                                                	                		<option value="http://www.hiscox.be">Belgium</option>
                	                		<option value="http://www.hiscox.bm">Bermuda</option>
                	                		<option value="http://www.hiscox.fr">France</option>
                	                		<option value="http://www.hiscox.de">Germany</option>
                	                		<option value="http://www.hiscox.ie">Ireland</option>
                	                		<option value="http://www.hiscox.nl">Netherlands</option>
                	                		<option value="http://www.hiscox.pt">Portugal</option>
                	                		<option value="http://www.hiscox.es">Spain</option>
                	                		<option value="http://www.hiscox.co.uk">United Kingdom</option>
                	                				</select></div>
						
    	    		
		 								<ul class='footer-links-home-box'>
	
	
    							
																  <li><p class="body-copy-grey">
	                    <a href="/accessibility/" title="" target="_self" >
							Accessibility</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/site-map.html" title="" target="_self" >
							Site map</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/terms-of-use/" title="" target="_self" >
							Terms of use</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/privacy-policy/" title="" target="_self" >
							Privacy policy</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/legal-notices/" title="" target="_self" >
							Legal notices</a></p>
					  </li>
										
    	    		
		 		</ul>		</div>
		<!--Footer ends here -->
		</div>
		<!-- Page container ends here-->
	</div>	<!--main container ends here-->	
	<script type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
	<script type="text/javascript" src="/resources/javascript/prototype-1.6.0.3.js"></script>
	<script type="text/javascript" src="/resources/javascript/commonscriptprototype.js"></script>
	<script type="text/javascript" src="/resources/javascript/tooltipprototype.js"></script>


		<div style="width:100%;align:center">
			<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Business Insurance homepage
URL of the webpage where the tag is expected to be placed: http://www.hiscoxusa.com/small-business-insurance.htm
Creation Date: 05/06/2010
-->
<iframe src="https://fls.doubleclick.net/activityi;src=2723417;type=busin314;cat=busin029;qty=1;cost=[Revenue];u5=[Occupation];u2=[State];ord=[OrderID]?" width="1" height="1" frameborder="0"></iframe>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->		</div>
	</body>
</html>