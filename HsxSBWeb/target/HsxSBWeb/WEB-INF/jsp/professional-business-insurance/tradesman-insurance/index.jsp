<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Tradesman Insurance - Insurance for Tradesmen, Retail and more | Hiscox</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<meta name="description" content="Hiscox specializes in a wide range of professional service businesses. We offer customized liability insurance tailored to fit your needs." />
<meta name="keywords" content="decorators insurance, electricians insurance, insurance for cleaning and janitorial services, construction insurance, insurance for landscapers and gardeners, insurance for medical professionals, plumbers insurance, insurance for printing and copying, insurance for restaurant owners, retail business insurance" />

		
	<link href="/resources/css/global-reset-sbl.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/ephox-dynamic-sbl.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/screenbuilder.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/rhythmyx-sbl.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/print-sbl.css" rel="stylesheet" type="text/css" media="print" />

	<!--[if lte IE 9]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie8.css";</style><![endif]-->
	<!--[if lte IE 7]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie7.css";</style><![endif]-->
	<!--[if lte IE 6]><style type="text/css" media="all">@import "/resources/css/screenbuilder-ie6.css";</style><![endif]-->
	<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
	<!--[if IE]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
	<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->

	<script language="javascript" type="text/javascript" src="/resources/javascript/couk_javascript.js"></script>
	<script language="javascript" type="text/javascript" src="/resources/javascript/usacom_javascript.js"></script>
	
	<script src="/resources/javascript/jquery-1.7.1.min.js" type="text/javascript"></script>
	
	<script src="/resources/javascript/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
	
	<script src="/resources/javascript/jquery.ba-bbq.min.js" type="text/javascript"></script>
	
	<script src="/resources/javascript/jquery.selectbox-0.21.js" type="text/javascript"></script>
	<script src="/resources/javascript/jquery.bxSlider.min.js" type="text/javascript"></script>
	<script src="/resources/javascript/functions.js" type="text/javascript"></script>
	<script src="/resources/javascript/detectmobilebrowser.js" type="text/javascript"></script>
	
	<link rel="stylesheet" href="/resources/css/colorbox-sbl.css" />
	<script type="text/javascript" src="/resources/javascript/jquery.colorbox-min.js"></script>
	
		<script type="text/javascript">
		
		$(document).ready(function () {
		
		$('img').attr('draggable', false);
		
		$(".top-nav ul li").hover(  function () { 
            var li = $(this);
			var a = $(li).find("a");
			
			var left = $(a).position().left + ($(a).width() / 2);

            $(li).find('.panel-marker').css('margin-left', left);
			
          },function () {}
		);
		
		
		
      	$('#quote-and-buy-form-script').attr('style','display:inline;');
		
		$(".hl-col:first").addClass("first");
      	$(".hl-col:last").addClass("last");
		
			$('.sb-menu').hover(
				function(){
					$(this).toggleClass('sb-hover');
			});
			
			$('.sb-panel').mouseleave(function() { 
				var p = $(this).parent();
				if($(p).hasClass('sb-hover')) {
					$(this).attr('style','visibility:hidden;');
				}
			});
			
			$('.sb-menu').mouseenter(function() { 
				var p = $(this).find('.sb-panel');
				$(p).attr('style','visibility:visible;');
			});
			
			$('.search-text').focus(function() {           
			  if($(this).val() == 'Search') 
				$(this).val('');
			});
			$('.search-text').blur(function() {           
			  if($(this).val() == '') 
				$(this).val('Search');
			});
			
			$("a.youtube").colorbox({iframe:true, height:"300px", width:"400px", href: function(){
				return $(this).attr('href');
            }});

	
			$(function() {
				$( "ul.tooltips li a" ).tooltip({position: {my: "left-15 center", at: "right center", collision: "none"}});
			});
		});
      
		</script>
	
	
	</head>
	
	<body>
			
    		<div class="header">
    			<div class="header-inner">
    				<div class="logo-wrapper">
    					<div class="site-logo">
    						<a href="/" title="Hiscox USA"><img src="/resources/images/hiscox-logo-sbl.png" alt="Hiscox Logo" /></a>
    					</div>
    					<div class="sb-dropdown">
	<ul>
		<li class="sb-menu"><a href="/small-business-insurance/" title="Hiscox USA Small Business Insurance">Small Business</a>
			
	
	
    							
																<ul class="sb-panel"><li><a href="/" title="Hiscox">Homepage</a></li><li><a href="http://www.hiscoxbroker.com/" target="_blank" title="Broker Center">Broker Center</a></li><li><a href="http://www.hiscoxgroup.com/" target="_blank" title="Hiscox Corporate">Hiscox Corporate</a></li></ul>
										
    	    		
		 				</li>
	</ul>
</div>    				</div>
    				
    				<div class="header-info">
							
	
	
    		<div id='site-search'>					
																
											<label for="gsc-i-id1">Search</label><div id="site-search-google">Loading</div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script> 
<script type="text/javascript"> 
	google.load('search', '1', {language : 'en'}); 
  	google.setOnLoadCallback(function() { 
  	var customSearchControl = new google.search.CustomSearchControl('004599377379842757095:ffvrv_nlocy'); 
  	customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
  	customSearchControl.setUserDefinedLabel('Search');
  	var options = new google.search.DrawOptions(); 
  	options.enableSearchboxOnly("http://uat.hiscox.com/search/index.html"); 
  	customSearchControl.draw('site-search-google', options); }, true);
</script> 

<link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" /> 
<style type="text/css"> 
  input.gsc-input { border:none;width:150px;margin:-1px 0 0 10px;font-family:Arial, Helvetica, sans-serif; font-size:12px; background:none !important; height:19px; } 
  input.gsc-search-button { background:transparent url(/resources/images/icn-search-sbl.gif) no-repeat top right; margin-left: -225px; margin-top: -1px; width:15px; height:15px; border:0; font-size: 0px; color:transparent; *padding-left:76px !important; line-height:0px; cursor:pointer;margin-left:-241px\0;*position:relative; }
  :root input.gsc-search-button{margin-left:-225px \0/IE9} .gsc-control-searchbox-only div.gsc-clear-button { background:none; } 
</style>
															
    	    		</div>
		 							
						<div class='header-contact'>	
	
	
    							
																
											Call our licensed agents at <img src="/resources/images/icn-phone-sbl.gif" alt="" /> <span>1.866.283.7545</span>
															
    	    		
		 		</div>    				</div>
					
    				
									<div class="top-nav">
			<ul>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																															                                							    								<li>    									<a class="top-nav-link" href="/small-business-insurance/why-choose-hiscox-insurance/" title="Why Hiscox">Why Hiscox</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Coverage customized to the risks you face</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:210px;'>
					<h3 class="black-heading-h3">Watch our TV ad</h3><div>
<a href="http://www.youtube.com/embed/QR2fsK0bau8" class="youtube" shape="rect"><img id="sample-video-sbl" src="/small-business-insurance/shared-images/sample-video-sbl.jpg" alt="youtube" title="youtube" border="0" height="99" width="165" /></a></div>
				</div>
            
												
				<div class='panel-col ' style='width:230px;'>
					<div class="panel-section"><h3 class="black-heading-h3">Tailored coverage to your needs</h3><p>That's why our customers save on average <b>31%/yr</b>.</p><ul class="mega-menu-red-arrow"><li><a class="reg-link" href="/small-business-insurance/save-on-business-insurance-costs/"><strong>SAVE by Switching to Hiscox</strong></a></li></ul><div> </div><h3 class="black-heading-h3">Have confidence in your choice</h3><p>'A' (excellent) rating by A.M. Best. Over 100 years of experience.</p><ul class="mega-menu-red-arrow"><li><a class="reg-link" href="/small-business-insurance/why-choose-hiscox-insurance/"><strong>More Reasons to Choose Hiscox</strong></a></li></ul></div>
				</div>
            
												
				<div class='panel-col  last' style='width:230px;'>
					<div class="panel-section"><h3 class="black-heading-h3">Customer satisfaction<br /></h3><img id="SBL-5-small-gold-stars-no-padding-why-choose" src="/small-business-insurance/shared-images/SBL-5-small-gold-stars-no-padding-why-choose.gif" alt="" title="" border="0" height="17" width="102" /><div> </div><img id="SBL-96-percent-landing-why-choose-tab" src="/small-business-insurance/shared-images/SBL-96-percent-landing-why-choose-tab.gif" alt="" title="" border="0" height="47" width="200" /><p><br /></p><p>Rated <b>4.9/5</b> for service (<a href="/small-business-insurance/business-insurance-reviews/">10,129 reviews</a>)</p></div>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>

	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="/small-business-insurance/professional-business-insurance/" title="Who we insure">Who we insure</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Coverage customized to the risks in your field</h2>				<p class="body-copy-grey-right-align">Don't see your profession? <a href="/small-business-insurance/professional-business-insurance/">Click Here</a></p>			</div>
			
			
			
												
				<div class='panel-col ' style='width:170px;'>
					<h3>Technology</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/it-insurance/it-consultant-insurance/">IT Consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/it-insurance/">Software development</a></li><li><a href="/small-business-insurance/professional-business-insurance/it-insurance/">Systems install/support</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/it-insurance/">View all technology</a></li></ul><p><br /></p><h3>Creative</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/photographers-insurance/">Photography</a></li><li><a href="/small-business-insurance/professional-business-insurance/web-design-insurance/">Graphic/Web Design</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/">View all creative</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:170px;'>
					<h3>Consulting</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">Business Consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">Management Consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">Education Consulting</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">View all consulting</a></li></ul><p><br /></p><h3>Real Estate</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/real-estate-agent-insurance/">Real Estate agent/broker</a></li><li><a href="/small-business-insurance/professional-business-insurance/property-management-insurance/">Property Management</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/real-estate-agent-insurance/">View all real estate</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:170px;'>
					<h3>Marketing</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/marketing-insurance/marketing-consultant-insurance/">Marketing/Media consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/marketing-insurance/">Event planning/promotion</a></li><li><a href="/small-business-insurance/professional-business-insurance/marketing-insurance/">Research consulting</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/marketing-insurance/">View all marketing</a></li></ul><p><br /></p><h3>Architect &amp; Engineering</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/">Architects insurance</a></li><li><a href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/">Engineers insurance</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/">View all A&amp;E professions</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' style='width:170px;'>
					<h3>Health &amp; Beauty</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/personal-trainer-insurance/">Personal Training (fitness)</a></li><li><a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/cosmetology-insurance/">Beautician/cosmetologist</a></li><li><a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/cosmetology-insurance/">Barber services</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/">View all health and beauty</a></li></ul><p><br /></p><h3>Other Services</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/bookkeeper-insurance/">Tax prep/bookkeeping</a></li><li><a href="/small-business-insurance/professional-business-insurance/">Business Training</a></li><li><a a="" class="reg-link" href="/small-business-insurance/professional-business-insurance/">View all business services</a></li></ul>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>

	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="#" title="Tailored insurance">Tailored insurance</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Insurance as unique as your small business</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:210px;'>
					<h3>Small business insurance</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-liability-insurance/">Professional Liability Insurance</a></li><li><a href="/small-business-insurance/errors-and-omissions-insurance/">Errors and Omissions Insurance</a></li><li><a href="/small-business-insurance/general-liability-insurance/">General Liability Insurance</a></li><li><a href="/small-business-insurance/business-owner-insurance/">Business Owner's Policy (BOP)</a></li><li><a href="/small-business-insurance/workers-compensation-insurance/">Workers' Compensation Insurance</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:230px;'>
					<h3>Insurance by state</h3><ul class="panel-icons"><li><img id="icn-california-sbl" src="/small-business-insurance/shared-images/icn-california-sbl.gif" alt="California" title="California" border="0" height="25" width="25" /><a href="/small-business-insurance/california-business-insurance/">California</a></li><li><img id="icn-texas-sbl" src="/small-business-insurance/shared-images/icn-texas-sbl.gif" alt="Texas" title="Texas" border="0" height="25" width="25" /><a href="/small-business-insurance/texas-business-insurance/">Texas</a></li><li><img id="icn-florida-sbl" src="/small-business-insurance/shared-images/icn-florida-sbl.gif" alt="Florida" title="Florida" border="0" height="25" width="25" /><a href="/small-business-insurance/florida-business-insurance/">Florida</a></li><li><img id="icn-illinois-sbl" src="/small-business-insurance/shared-images/icn-illinois-sbl.gif" alt="Illinois" title="Illinois" border="0" height="25" width="25" /><a href="/small-business-insurance/illinois-business-insurance/">Illinois</a></li><li><img id="icn-newyork-sbl" src="/small-business-insurance/shared-images/icn-newyork-sbl.gif" alt="New York" title="New York" border="0" height="25" width="25" /><a href="/small-business-insurance/ny-business-insurance/">New York</a></li><li style=" list-style: none;"><a class="reg-link" href="/small-business-insurance/state/">View all states</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' >
					<h3>Business insurance in 90 seconds</h3><div>
<a href="http://www.youtube.com/embed/2-YCV7gSVoE" class="youtube" shape="rect"><img id="small-business-product-video-thumb-sbl" src="/small-business-insurance/shared-images/small-business-product-video-thumb-sbl.jpg" alt="" title="" border="0" height="99" width="165" /></a></div>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>

	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="/small-business-insurance/blog/" title="Blog">Blog</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>We help make your business a huge success - not just with great insurance</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:340px;'>
					<h3>Recent blog posts</h3><div class="mega-menu-red-arrow" style=" margin-left: 2em;"><script language="JavaScript" script="null" src="http://itde.vccs.edu/rss2js/feed2js.php?src=http%3A%2F%2Fwww.hiscox.com%2Fsmall-business-insurance%2Fblog%2Ffeed%2F&amp;chan=n&amp;num=5&amp;desc=0&amp;date=n&amp;targ=n" type="text/javascript">/**/

/**/</script><noscript><a href="http://itde.vccs.edu/rss2js/feed2js.php?src=http%3A%2F%2Fwww.hiscox.com%2Fsmall-business-insurance%2Fblog%2Ffeed%2F&amp;chan=n&amp;num=5&amp;desc=0&amp;date=n&amp;targ=n&amp;html=y">View RSS feed</a></noscript></div>
				</div>
            
												
				<div class='panel-col  last' style='width:340px;'>
					<p>Our blog is a "go to" resource for your small business. Find valuable information and advice on making your business a huge success.<br /><br /></p><h3>Join the conversation</h3><ul class="panel-icons-no-width"><li><a href="http://www.facebook.com/hiscoxsmallbiz" target="_blank" shape="rect"><img id="icn-facebook-sbl" src="/small-business-insurance/shared-images/icn-facebook-sbl.gif" alt="Hiscox Small Business Insurance facebook" title="Hiscox Small Business Insurance facebook" border="0" height="29" width="29" /></a></li><li><a href="http://www.twitter.com/hiscoxsmallbiz" target="_blank" shape="rect"><img id="icn-twitter-sbl" src="/small-business-insurance/shared-images/icn-twitter-sbl.gif" alt="Hiscox Small Business Insurance twitter" title="Hiscox Small Business Insurance twitter" border="0" height="29" width="29" /></a></li><li><a href="http://www.linkedin.com/company/hiscox-small-business-insurance/" target="_blank" shape="rect"><img id="icn-linkedin-sbl" src="/small-business-insurance/shared-images/icn-linkedin-sbl.gif" alt="Hiscox Small Business Insurance linkedin" title="Hiscox Small Business Insurance linkedin" border="0" height="29" width="29" /></a></li><li><a href="http://plus.google.com/117977778162613990129/" target="_blank" shape="rect"><img id="icn-google-plus-sbl" src="/small-business-insurance/shared-images/icn-google-plus-sbl.gif" alt="Follow us on Google Plus" title="Follow us on Google Plus" border="0" height="29" width="29" /></a></li><li><a href="http://www.youtube.com/hiscoxinsurance/" target="_blank" shape="rect"><img id="icn-youtube-sbl" src="/small-business-insurance/shared-images/icn-youtube-sbl.gif" alt="Hiscox Small Business Insurance youtube" title="Hiscox Small Business Insurance youtube" border="0" height="29" width="29" /></a></li><li><a href="/small-business-insurance/blog/feed/" target="_blank" shape="rect"><img id="icn-rss-sbl" src="/small-business-insurance/shared-images/icn-rss-sbl.gif" alt="Hiscox Small Business Insurance RSS" title="Hiscox Small Business Insurance RSS" border="0" height="29" width="29" /></a></li></ul>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>

	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="#" title="Customer support">Customer support</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Hiscox customer support - we're here to help</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:190px;'>
					<h3>Support services</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/business-insurance-claims/">Report a claim</a></li><li><a href="/small-business-insurance/contract-insurance-requirements/additional-terms/">Updating your policy</a></li><li><a href="/contact-us/">Contact us</a></li><li><a href="/small-business-insurance/business-insurance-reviews/">Customer reviews</a></li><li><a href="/small-business-insurance/blog/">Small business blog</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' >
					<div class="contact-details"><div>Call our licensed agents at</div><div class="tel-num">1.866.283.7545</div><div class="times">from 8am-10pm EST (Mon-Fri)</div></div>
				</div>
            		</div>
		
		<div class='panel-bottom'><div class='panel-bottom-tab'><div class='panel-bottom-left'><img src='/resources/images/topnav-bottom-left-sbl.png' alt='' /></div><div class='panel-bottom-middle'>
	
	<div class="quote-link">Need insurance? <a href="/small-business-insurance/get-a-quote-for-your-small-business/" title="GET A QUOTE">GET A QUOTE</a></div>
	
  </div></div></div>
	</div>

	
      								</li>    																																																																																																																																																																																									</ul>
		</div>
		
		
		
    
    			</div>
    		</div><!-- header -->
    		
			<div class="content-wrapper">
        		<div class="page-container">
        			
        				
	<div class="page-container">
		<!--Breadcrumb starts here-->
					<div class="breadcrumb">
				
	<div class="brdcrmb-sub">
			                    	                    	                    					<div class="breadcrumb-link"><a href="/">Home page</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/">For small business owners</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-link"><a href="/small-business-insurance/professional-business-insurance/">Insurance by profession</a></div>
					<div class="breadcrumb-current breadcrumb-separator"><span>&gt;</span></div>
							<div class="breadcrumb-current"><span>Tradesmen, retail & more</span></div>
			</div>
																						<div class="printbtnbrochureware-container">
    						<div class="submit-button grey-button print-icon">
    							<span class="submit-button-end">
    								<input type="button" value="Print" onClick="window.print()" class="submit-input">
        							<span class="button-icon"></span>
    							</span>
    						</div>
						</div>
													</div>
		<!--Breadcrumb ends here-->
		<!--Left nav starts here-->
		<div class="leftnav-gap clear-both">
						
							        	    		    									<div class="leftmenu-main">
																																																												<div class="leftmenu-mainitem">
					<div class="mainitem-topleft" ></div><div class="mainitem-topmiddle" ></div><div class="mainitem-topright"></div>
						<div class="mainitem-middle">
							<a href="/small-business-insurance/" target="_self" class="main-link">For small business owners</a>
						</div>
					<div class="mainitem-btmleft"></div><div class="mainitem-btmmiddle" ></div><div class="mainitem-btmright"></div>
				</div>
																	<div class="leftmenu">
					<div class="leftmenu-top"><div class="leftmenu-topleft"></div><div class="leftmenu-topmiddle"></div><div class="leftmenu-topright"></div></div>
						<div class="leftmenu-middle">
							<ul>
																	        																		        																		        																		        																		        																		        																		        																		        																		            	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    	    		    		    																																																											<li class="">
					<div class="leftmenu-listitem-sel"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/" target="_self" class="leftmenulinks-sel">Insurance by profession</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
																						  																                    																																															<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/" target="_self" class="leftmenulinks-nopad">Architects and Engineers</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																															<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/bookkeeper-insurance/" target="_self" class="leftmenulinks-nopad">Bookkeepers</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																															<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/" target="_self" class="leftmenulinks-nopad">Business consultants</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																															<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/" target="_self" class="leftmenulinks-nopad">Health, beauty & wellbeing</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																															<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/it-insurance/" target="_self" class="leftmenulinks-nopad">IT Insurance</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																															<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/marketing-insurance/" target="_self" class="leftmenulinks-nopad">Marketing & PR</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																															<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/photographers-insurance/" target="_self" class="leftmenulinks-nopad">Photographers</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																															<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/property-management-insurance/" target="_self" class="leftmenulinks-nopad">Property managers</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																                    																																															<li class="">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/real-estate-agent-insurance/" target="_self" class="leftmenulinks-nopad">Real estate agents</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    											  																																																																		<li class="last-item">
					<div class="leftmenu-listitem-sel2"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/tradesman-insurance/" target="_self" class="leftmenulinks-sel">Tradesmen, retail & more</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
																					    											  																                    																																															<li class="last-item">
					<div class="leftmenu-listitem"> 
						<div class="leftmenulist-topleft" ></div><div class="leftmenulist-topmiddle" ></div><div class="leftmenulist-topright"></div>
							<div class="leftmenulist-middle">
								<a href="/small-business-insurance/professional-business-insurance/web-design-insurance/" target="_self" class="leftmenulinks-nopad">Web/Graphic designers</a>
							</div>
						<div class="leftmenulist-btmleft"></div><div class="leftmenulist-btmmiddle" ></div><div class="leftmenulist-btmright"></div>
					</div>
				</li> 
				                                    	         	    				   																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																		        																</ul>
						</div>
					<div class="leftmenu-bottom"><div class="leftmenu-btmleft"></div><div class="leftmenu-btmmiddle"></div><div class="leftmenu-btmright"></div></div>
				</div>
			</div>
			    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    							    		    	    			
                                                                                                                                                        
                                                                                                
            
            
                                    
            										        										        										        										    
            										    
            										    
            										        										        										        										        										        										        										    
                                                                                                    
            
            
                                    
            										        										        										        										    
            										    
            										    
            										        										        										        										        										        										        										    
    </div>
<div class="cnt-sctn no-bg">
                                                                                        		<div class='cnt-subsctn'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='page-heading2'>
        <h1 class="page-heading-h1">Insurance for tradesmen, retail and more</h1>        </div><div class='clear'></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
        	
		
                                                                                        		<div class='cnt-subsctn white-corner-btm'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content'><div class='middlecnt-container'>
        <p class="body-copy-grey">At Hiscox we want to help every type of small business owner get the right coverage as quickly as possible. Click on the profession below that best describes your business and one of our partners can help get you the right insurance for your business.<br /><br /></p><table border="1" cellpadding="0" cellspacing="0" width="100%"><tr><td width="50%"><ul class="list-red-tick-body-copy-grey"><li><a href="http://beta.insurancebee.com/ecomms/step-one-your-business.aspx" rel="nofollow" target="_blank">Appliance stores</a></li><li><a href="https://hiscox.boltinsurance.com/insurance-quotes" rel="nofollow" target="_blank">Automobile services</a></li><li><a href="http://beta.insurancebee.com/ecomms/step-one-your-business.aspx" rel="nofollow" target="_blank">Cleaning services</a></li><li><a href="http://beta.insurancebee.com/ecomms/step-one-your-business.aspx" rel="nofollow" target="_blank">Clothing and apparel stores</a></li><li><a href="https://hiscox.boltinsurance.com/insurance-quotes" rel="nofollow" target="_blank">Construction</a></li><li><a href="https://hiscox.boltinsurance.com/insurance-quotes" rel="nofollow" target="_blank">Electricians</a></li><li><a href="http://beta.insurancebee.com/ecomms/step-one-your-business.aspx" rel="nofollow" target="_blank">Electronic stores</a></li><li><a href="http://beta.insurancebee.com/ecomms/step-one-your-business.aspx" rel="nofollow" target="_blank">Florists</a></li></ul></td><td width="50%"><ul class="list-red-tick-body-copy-grey"><li><a href="http://beta.insurancebee.com/ecomms/step-one-your-business.aspx" rel="nofollow" target="_blank">Home furniture stores</a></li><li><a href="http://beta.insurancebee.com/ecomms/step-one-your-business.aspx" rel="nofollow" target="_blank">Landscaping</a></li><li><a href="https://hiscox.boltinsurance.com/insurance-quotes" rel="nofollow" target="_blank">Medical professionals</a></li><li><a href="http://beta.insurancebee.com/ecomms/step-one-your-business.aspx" rel="nofollow" target="_blank">Other Stores (with food/drink)</a></li><li><a href="http://beta.insurancebee.com/ecomms/step-one-your-business.aspx" rel="nofollow" target="_blank">Other Stores (no food/drink)</a></li><li><a href="https://hiscox.boltinsurance.com/insurance-quotes" rel="nofollow" target="_blank">Plumbers</a></li><li><a href="https://hiscox.boltinsurance.com/insurance-quotes" rel="nofollow" target="_blank">Printing and copying</a></li><li><a href="https://hiscox.boltinsurance.com/insurance-quotes" rel="nofollow" target="_blank">Restaurant owners</a></li></ul></td></tr></table><p><br /></p><p class="body-copy-grey">If you don't see your profession listed above please call one of our licensed advisors at 866-283-7545 (Monday - Friday, 8 am - 10 pm EST). We're here to help you.<br /><br /></p>        </div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>
    		
		
    <!--Secondary content section starts here-->
            <!--Secondary content section ends here-->

    <!--Centre bottom section starts here-->
                                                                                                
            
            
                                    
            										        										        										        										    
            										    
            										    
            										        										        										        										        										        										        										    
        <!--Centre bottom section ends here-->
    <!--Tertiary content section starts here-->
	
		
            <!--Tertiary content section ends here-->
	
	</div>
<!--Content section ends here-->
<!-- Right nav starts here-->
<div class="rightnav-gap showcntct-num">
    <!--Checklist section starts here-->                                                                                                    
            
            
                                    
            										        										        										        										    
            										    
            																                        <div class="section-container topright-container-small print" >
        <div class="top"><div class="right" ><div class="left"><div class="middle">	</div></div></div></div>
                <div class="middle-content vertical-container">
                
																																				<div class="contact-child-item print">
						<div>
							<img id="contact-woman" src="/shared-images/contact-woman.png" alt="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" title="Contact Hiscox - the small business insurance specialists, and speak to one of our licensed advisors today!" border="0" height="140" width="167" />						</div>
					</div>
																			<div class="contact-child-item-last">
						<div>
							<p><span class="grey-call-to-action-large"><b>866-283-7545</b></span></p><p><span class="red-call-to-action-small">8am - 10pm (Mon - Fri) EST</span></p>						</div>
					</div>
										                </div>
                <div class="bottom clear-both"><div class="right"><div class="left"><div class="middle" ></div></div></div></div>
    </div>
    			 			    
            										        										        										        										        										        										        										    
                                                                                                                                                                                            
            
            
                                    
            										        										        										        										    
            										    
            										    
            										        										        										        										        										        										        										    
        <div class="verLogo-holder">
        <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications.">
            <tr>
                <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hiscox.com&size=L&use_flash=YES&use_transparent=YES&lang=en"></script><br />
                    <a href="http://www.verisign.com/ssl-certificate/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>
            </tr>
        </table>
    </div>
    <!--Checklist section ends here-->
</div>
<!-- Right nav ends here-->
</div>

        				
        				<div class="sb-footer">
        					<div class="sb-footer-inner">
        						


	
	
    						
			
									
											<div class='sb-footer-col'>
										
					<h3 class="box-heading-h3">Insurance by profession</h3><ul><li><a href="/small-business-insurance/professional-business-insurance/it-insurance/">IT / Technology</a></li><li><a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">Business/marketing consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/">Health, Beauty &amp; Wellbeing</a></li><li><a href="/small-business-insurance/professional-business-insurance/photographers-insurance/">Photographers</a></li><li><a href="/small-business-insurance/professional-business-insurance/real-estate-agent-insurance/">Real Estate Agents</a></li><li><a href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/">Architects &amp; engineers</a></li><li><a href="/small-business-insurance/professional-business-insurance/">View all professions</a></li></ul>
					</div>
							
    			
			
									
											<div class='sb-footer-col'>
										
					<h3>Small Business Insurance</h3><ul><li><a href="/small-business-insurance/liability-insurance/">Liability insurance</a></li><li><a href="/small-business-insurance/professional-liability-insurance/">Professional liability insurance</a></li><li><a href="/small-business-insurance/errors-and-omissions-insurance/">Errors and omissions insurance</a></li><li><a href="/small-business-insurance/general-liability-insurance/">General liability insurance</a></li><li><a href="/small-business-insurance/business-owner-insurance/">Business owner insurance</a></li><li><a href="/small-business-insurance/workers-compensation-insurance/">Workers' compensation insurance</a></li><li><a href="/small-business-insurance/contract-insurance-requirements/">Need coverage for a client contract?</a></li></ul>
					</div>
							
    			
			
									
											<div class='sb-footer-col'>
										
					<h3>Insurance by State</h3><ul><li><a href="/small-business-insurance/california-business-insurance/">California business insurance</a></li><li><a href="/small-business-insurance/florida-business-insurance/">Florida business insurance</a></li><li><a href="/small-business-insurance/ny-business-insurance/">New York business insurance</a></li><li><a href="/small-business-insurance/texas-business-insurance/">Texas business insurance</a></li><li><a href="/small-business-insurance/illinois-business-insurance/">Illinois business insurance</a></li><li><a href="/small-business-insurance/state/">View all states</a></li></ul>
					</div>
							
    			
			
									
											<div class='sb-footer-col' style='width:270px; margin-right:0;'>
										
					<h3>Helpful Links</h3><div class="help-left"><ul><li><a href="/small-business-insurance/quote-and-buy/retrieve-a-quote/">Retrieve a Quote</a></li><li><a href="/small-business-insurance/business-insurance-reviews/">Customer Reviews</a></li><li><a href="/small-business-insurance/blog/">Small Business Blog</a></li><li><a href="/small-business-insurance/refer-a-friend/">Refer a Friend</a></li><li><a href="/small-business-insurance/hiscox-affiliate-program/">Become an Affiliate</a></li><li><a href="/small-business-insurance/newsroom/">Hiscox Newsroom</a></li><li><a href="/small-business-insurance/business-insurance-claims/">Report a Claim</a></li></ul></div><div class="help-right"><ul><li><a href="http://www.hiscoxgroup.com/en/careers.aspx" target="_blank">Careers</a></li><li><a href="http://www.hiscoxgroup.com/investors.aspx" target="_blank">Investors</a></li><li><a href="http://www.hiscoxgroup.com/" target="_blank">Hiscox Corporate Site</a></li><li><a href="http://www.bbb.org/new-york-city/business-reviews/insurance-services/hiscox-inc-in-new-york-ny-123713/#bbbonlineclick" title="Hiscox Inc. BBB Business Review"><img alt="Hiscox Inc. BBB Business Review" src="http://seal-newyork.bbb.org/seals/blue-seal-96-50-hiscox-inc-123713.png" style=" border: 0;" /></a></li></ul></div>
					</div>
							
    	    	
	 		
<div class="footer-toolbar">
	
	
	
    		<div class='ft-partners'>					
																<div> </div>
										
    	    		</div>
		 			
	
	
    		<div class='ft-contact'>					
																<div itemscope="" itemtype="http://schema.org/Corporation"><div class="ft-contact" style=" text-align: center;">Need help? Contact a licensed agent at Hiscox <span itemprop="telephone">1.866.283.7545</span></div></div>
										
    	    		</div>
		 			
	
	
    		<div class='ft-social'>					
																<ul><li><a href="http://www.facebook.com/hiscoxsmallbiz" target="_blank" shape="rect"><img id="icn-social-facebook-sbl" src="/small-business-insurance/shared-images/icn-social-facebook-sbl.gif" alt="Hiscox Small Busines Insurance facebook" title="Hiscox Small Busines Insurance facebook" border="0" height="20" width="20" /></a></li><li><a href="http://www.twitter.com/hiscoxsmallbiz" target="_blank" shape="rect"><img id="icn-social-twitter-sbl" src="/small-business-insurance/shared-images/icn-social-twitter-sbl.gif" alt="Hiscox Small Business Insurance twitter" title="Hiscox Small Business Insurance twitter" border="0" height="20" width="20" /></a></li><li><a href="http://www.linkedin.com/company/hiscox-small-business-insurance/" target="_blank" shape="rect"><img id="icn-social-linkedin-sbl" src="/small-business-insurance/shared-images/icn-social-linkedin-sbl.gif" alt="Hiscox Small Business Insurance linkedin" title="Hiscox Small Business Insurance linkedin" border="0" height="20" width="20" /></a></li><li><a href="http://plus.google.com/117977778162613990129/" target="_blank" shape="rect"><img id="icn-social-google-plus-sbl" src="/small-business-insurance/shared-images/icn-social-google-plus-sbl.gif" alt="Follow us on Google Plus" title="Follow us on Google Plus" border="0" height="20" width="20" /></a></li><li><a href="http://www.youtube.com/hiscoxinsurance/" target="_blank" shape="rect"><img id="icn-social-youtube-sbl" src="/small-business-insurance/shared-images/icn-social-youtube-sbl.gif" alt="Hiscox Small Business Insurance youtube" title="Hiscox Small Business Insurance youtube" border="0" height="20" width="20" /></a></li><li><a href="/small-business-insurance/blog/feed/" target="_blank" shape="rect"><img id="icn-social-rss-sbl" src="/small-business-insurance/shared-images/icn-social-rss-sbl.gif" alt="Hiscox Small Business Insurance RSS" title="Hiscox Small Business Insurance RSS" border="0" height="20" width="20" /></a></li></ul>
										
    	    		</div>
		 		</div>
						
        					</div>
        				</div>
    					<div class="sb-footer-btm"></div>
        				<div class="sb-baseline">
        					<div class="sb-baseline-inner">
        						<div class="baseline-copyright">
        							
	
	
	
    							
																<p class="body-copy-grey-small">© 2013 Hiscox Inc. All rights reserved<br />Underwritten by Hiscox Insurance Company Inc.</p>
										
    	    		
		 		        						</div>
        						<div class="baseline-country">
        							
	
	
	
    							
							<div class="footer-dropdown-box">
                <label for="country">Not in the US?</label>
                <select name="country" id="country" onchange="if(this.options[this.selectedIndex].value!='')openChosenURL(this);">
                <option value="">Select your country</option>
                                                	                		<option value="http://www.hiscox.be">Belgium</option>
                	                		<option value="http://www.hiscox.bm">Bermuda</option>
                	                		<option value="http://www.hiscox.fr">France</option>
                	                		<option value="http://www.hiscox.de">Germany</option>
                	                		<option value="http://www.hiscox.ie">Ireland</option>
                	                		<option value="http://www.hiscox.nl">Netherlands</option>
                	                		<option value="http://www.hiscox.pt">Portugal</option>
                	                		<option value="http://www.hiscox.es">Spain</option>
                	                		<option value="http://www.hiscox.co.uk">United Kingdom</option>
                	                				</select></div>
						
    	    		
		 		        						</div>
        						<ul class='footer-links-home-box'>
	
	
	
	
    							
																  <li><p class="body-copy-grey">
	                    <a href="/accessibility/" title="" target="_self" >
							Accessibility</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/sitemap.html" title="" target="_self" >
							Sitemap HTML</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/terms-of-use/" title="" target="_self" >
							Terms of use</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/privacy-policy/" title="" target="_self" >
							Privacy policy</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/legal-notices/" title="" target="_self" >
							Legal notices</a></p>
					  </li>
										
    	    		
		 		</ul>        						
        					</div>
        				</div>
										</div>
			
			</div>
			
			


		<script type="text/javascript">
		
		$(document).ready(function () {

			$(".top-nav ul li .panel-bottom-middle").css("width", function () { 
    				var t = $(this).find(".quote-link").width();
					return t + 10;
    		});
			
			$(".top-nav ul li .panel").css("top", function () {
				var top = 19;
				
    			$(".top-nav ul li a.top-nav-link").each(function () { 
    				if($(this).height() > top) {
    					top = $(this).height();
    				}
        		});
				$(".top-nav ul li a.top-nav-link").css("height", top);
				
				return top + 6;
			});
			
			
			$(".top-nav ul li").hover(function () {
				var panelHeight = 0;
				$(this).find(".panel-col").each(function () {
					if($(this).height() > panelHeight) {
						panelHeight = $(this).height();
					}
				});
				
				$(this).find(".panel-col").css("min-height", panelHeight);
			});
			
			var tcnHeight = 0;
			
			$(".help-left").each(function () {
				if($(this).height() > tcnHeight) {
					tcnHeight = $(this).height();
				}
			});
			$(".help-right").each(function () {
				if($(this).height() > tcnHeight) {
					tcnHeight = $(this).height();
				}
			});
			
			$(".help-left").css("height", tcnHeight);
			
			$(".home-lower .col-more").each(function () {
				$(this).css('width',$(this).parent().width());
				$(this).addClass("hl-col-moved");
				$(this).addClass("col-more-moved");
				$(this).addClass($(this).parent().attr('class'));
				$(this).removeClass('hl-col');
				$(this).removeClass('col-more');
				$(this).appendTo($(this).parent().parent());
			});
			
			var hlHeight = 0;
			
			$(".hl-col").each(function () {
				if($(this).height() > hlHeight) {
					hlHeight = $(this).height();
				}
			});
			
			$(".hl-col").css("height", hlHeight-10);
			
		});
		
		</script>
		
	</body>
</html>