<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Small Business Insurance - Liability Insurance for Small Business| Hiscox</title>
			<meta name="description" content="Get the right small business insurance with Hiscox. Customized coverage from $22.50/month. Get a fast online quote now. " />
<meta name="keywords" content="small business insurance, small business liability insurance, insurance for small business, liability insurance for small business" />

				<link href="/resources/css/ephox-dynamic-sbl.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/hiscoxcom.css" rel="stylesheet" type="text/css" media="all" />
	<link href="/resources/css/print.css" rel="stylesheet" type="text/css" media="print" />
				<!--[if IE]><style type="text/css" media="all">@import "/resources/css/rhythmyx-ie.css";</style><![endif]-->
	<!--[if IE]><style type="text/css" media="print">@import "/resources/css/print-ie.css";</style><![endif]-->
	<!--[if IE 6]><style type="text/css"> body{ behavior: url('/resources/css/csshover3.htc');}</style><![endif]-->
			<script language="javascript" type="text/javascript" src="/resources/javascript/jquery-lib.js"></script>
		

			<script>rohit ji</script>
		</head>
	
	<body>
		
	
    		
    		<div class="header">
    			<div class="header-inner">
    				<div class="logo-wrapper">
						    					
							
<div class="site-logo">
	<a href="/" title="Hiscox USA"><img src="/resources/images/hiscox-logo-sbl.png" alt="Hiscox Logo" /></a>
</div>
<div class="sb-dropdown">
	<ul>
		<li class="sb-menu"><a href="/" title="Hiscox USA Small Business Insurance">Small Business</a>
			
    							
																<ul class="sb-panel"><li><a href="/" title="Hiscox">Homepage</a></li><li><a href="http://www.hiscoxbroker.com/" target="_blank" title="Broker Center">Hiscoxbroker.com</a></li><li><a href="http://www.hiscoxgroup.com/" target="_blank" title="Hiscox Corporate">Hiscox Corporate</a></li></ul>
										
    	    		
		 				</li>
	</ul>
</div>
																								    					    				</div>
    				
    				<div class="header-info">
							
    		<div id='site-search'>					
																
											<label for="gsc-i-id1">Search</label><div id="site-search-google">Loading</div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script> 
<script type="text/javascript"> 
	google.load('search', '1', {language : 'en'}); 
  	google.setOnLoadCallback(function() { 
  	var customSearchControl = new google.search.CustomSearchControl('014469312150351228698:ffvrv_nlocy'); 
  	customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
  	customSearchControl.setUserDefinedLabel('Search');
  	var options = new google.search.DrawOptions(); 
  	options.enableSearchboxOnly("http://www.hiscox.com/search/index.html"); 
  	customSearchControl.draw('site-search-google', options); }, true);
</script> 

<link rel="stylesheet" href="http://www.google.com/cse/style/look/default.css" type="text/css" /> 
<style type="text/css"> 
  input.gsc-input { border:none;width:150px;margin:-1px 0 0 10px;font-family:Arial, Helvetica, sans-serif; font-size:12px; background:none !important; height:19px; } 
  input.gsc-search-button { background:transparent url(/resources/images/icn-search-sbl.gif) no-repeat top right; margin-left: -225px; margin-top: -1px; width:15px; height:15px; border:0; font-size: 0px; color:transparent; *padding-left:76px !important; line-height:0px; cursor:pointer;margin-left:-241px\0;*position:relative; }
  :root input.gsc-search-button{margin-left:-225px \0/IE9} .gsc-control-searchbox-only div.gsc-clear-button { background:none; } 
</style>
															
    	    		</div>
		 							
						<div class='header-contact'>	
    							
																
											Call our licensed agents at <img src="/resources/images/icn-phone-sbl.gif" alt="" /> <span>1-866-283-7545</span>
															
    	    		
		 		</div>    				</div>
					
    				
			<div class="top-nav">
			<ul>
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						                                							    								<li>    									<a class="top-nav-link" href="#" title="Tailored insurance">Tailored insurance</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Insurance as unique as your small business</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:210px;'>
					<h3>Small business insurance</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/liability-insurance/">Liability Insurance Overview</a></li><li><a href="/small-business-insurance/general-liability-insurance/">General Liability Insurance</a></li><li><a href="/small-business-insurance/professional-liability-insurance/">Professional Liability Insurance</a></li><li><a href="/small-business-insurance/errors-and-omissions-insurance/">Errors and Omissions Insurance</a></li><li><a href="/small-business-insurance/business-owners-policy/">Business Owner's Policy (BOP)</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:230px;'>
					<h3>Insurance by state</h3><ul class="panel-icons"><li><img id="icn-california-sbl" src="/small-business-insurance/shared-images/icn-california-sbl.gif" alt="California" title="California" border="0" height="25" width="25" /><a href="/small-business-insurance/california-business-insurance/">California</a></li><li><img id="icn-florida-sbl" src="/small-business-insurance/shared-images/icn-florida-sbl.gif" alt="Florida" title="Florida" border="0" height="25" width="25" /><a href="/small-business-insurance/florida-business-insurance/">Florida</a></li><li><img id="icn-texas-sbl" src="/small-business-insurance/shared-images/icn-texas-sbl.gif" alt="Texas" title="Texas" border="0" height="25" width="25" /><a href="/small-business-insurance/texas-business-insurance/">Texas</a></li><li><img id="icn-illinois-sbl" src="/small-business-insurance/shared-images/icn-illinois-sbl.gif" alt="Illinois" title="Illinois" border="0" height="25" width="25" /><a href="/small-business-insurance/illinois-business-insurance/">Illinois</a></li><li><img id="icn-newyork-sbl" src="/small-business-insurance/shared-images/icn-newyork-sbl.gif" alt="New York" title="New York" border="0" height="25" width="25" /><a href="/small-business-insurance/ny-business-insurance/">New York</a></li><li style=" list-style: none;"><a class="reg-link" href="/small-business-insurance/state/">View all states</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' >
					<h3>Business insurance in 90 seconds</h3><div>
<a href="http://www.youtube.com/embed/2-YCV7gSVoE" class="youtube" shape="rect"><img id="small-business-product-video-thumb-sbl" src="/small-business-insurance/shared-images/small-business-product-video-thumb-sbl.jpg" alt="" title="" border="0" height="99" width="165" /></a></div>
				</div>
            		</div>
		
		
	</div>

	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="/small-business-insurance/professional-business-insurance/" title="Who we insure">Who we insure</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Coverage customized to the risks in your field</h2>				<p class="body-copy-grey-right-align">Don't see your profession? <a href="/small-business-insurance/professional-business-insurance/">Click Here</a></p>			</div>
			
			
			
												
				<div class='panel-col ' style='width:170px;'>
					<h3>Technology</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/it-insurance/it-consultant-insurance/">IT Consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/it-insurance/">Software development</a></li><li><a href="/small-business-insurance/professional-business-insurance/it-insurance/">Systems install/support</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/it-insurance/">View all technology</a></li></ul><p><br /></p><h3>Creative</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/photographers-insurance/">Photography</a></li><li><a href="/small-business-insurance/professional-business-insurance/web-design-insurance/">Graphic/Web Design</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/">View all creative</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:170px;'>
					<h3>Consulting</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">Business Consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">Management Consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">Education Consulting</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">View all consulting</a></li></ul><p><br /></p><h3>Real Estate</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/real-estate-agent-insurance/">Real Estate agent/broker</a></li><li><a href="/small-business-insurance/professional-business-insurance/property-management-insurance/">Property Management</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/real-estate-agent-insurance/">View all real estate</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:170px;'>
					<h3>Marketing</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/marketing-insurance/marketing-consultant-insurance/">Marketing/Media consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/marketing-insurance/">Event planning/promotion</a></li><li><a href="/small-business-insurance/professional-business-insurance/marketing-insurance/">Research consulting</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/marketing-insurance/">View all marketing</a></li></ul><p><br /></p><h3>Architect &amp; Engineering</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/">Architecture</a></li><li><a href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/">Engineering</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/">View all A&amp;E professions</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' style='width:170px;'>
					<h3>Health &amp; Beauty</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/fitness-insurance/personal-trainer-insurance/">Personal Training (fitness)</a></li><li><a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/beauty-insurance/cosmetology-insurance/">Beautician/cosmetologist</a></li><li><a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/beauty-insurance/barber-insurance/">Barber services</a></li><li><a class="reg-link" href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/">View all health and beauty</a></li></ul><p><br /></p><h3>More</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/professional-business-insurance/landscaper-insurance/">Landscaping</a></li><li><a href="/small-business-insurance/professional-business-insurance/janitor-insurance/">Janitorial Services</a></li><li><a href="/small-business-insurance/professional-business-insurance/retail-insurance/">Retail</a></li><li><a href="http://hiscox.insureon.com/?utm_source=hiscox&amp;utm_medium=referral&amp;esource=34605" target="_blank">Small Contractors</a></li><li><a a="" class="reg-link" href="/small-business-insurance/professional-business-insurance/">View All Services</a></li></ul>
				</div>
            		</div>
		
		
	</div>

	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="/small-business-insurance/why-choose-hiscox-insurance/" title="Why Hiscox">Why Hiscox</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>Coverage customized to the risks you face</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:225px;'>
					<div class="panel-section"><h3 class="black-heading-h3">Have confidence in your choice</h3><p>'A' (excellent) rating by A.M. Best. Over 100 years of experience.</p><ul class="mega-menu-red-arrow"><li><a class="reg-link" href="/small-business-insurance/why-choose-hiscox-insurance/"><strong>More Reasons to Choose Hiscox</strong></a></li></ul></div>
				</div>
            
												
				<div class='panel-col ' style='width:225px;'>
					<div class="panel-section"><h3 class="black-heading-h3">Tailored coverage to your needs</h3><p>That's why our customers save on average <b>31% a year</b>.</p><ul class="mega-menu-red-arrow"><li><a class="reg-link" href="/small-business-insurance/save-on-business-insurance-costs/"><strong>SAVE by Switching to Hiscox</strong></a></li></ul></div>
				</div>
            
												
				<div class='panel-col  last' style='width:225px;'>
					<div class="panel-section"><h3 class="black-heading-h3">Customer satisfaction<br /></h3><img id="SBL-5-small-gold-stars-no-padding-why-choose" src="/small-business-insurance/shared-images/SBL-5-small-gold-stars-no-padding-why-choose.gif" alt="" title="" border="0" height="17" width="102" /><div> </div><a href="http://ww2.feefo.com/en-us/reviews/hiscox" target="_blank" shape="rect"><img id="SBL-97-percent-landing-why-choose-tab" src="/small-business-insurance/shared-images/SBL-97-percent-landing-why-choose-tab.png" alt="" title="" border="0" height="47" width="200" /></a><p><br /></p><p>Rated <b>97%</b> for service ( <a href="http://ww2.feefo.com/en-us/reviews/hiscox?Language-tags=English&amp;Logon-tags=www.hiscoxusa.com&amp;Country-tags=United%20States&amp;Locale-tags=en_US&amp;Hostname-tags=www.hiscoxusa.com">4,033 reviews</a>)</p></div>
				</div>
            		</div>
		
		
	</div>

	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="/small-business-insurance/blog/" title="Blog">Blog</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>We help make your business a huge success - not just with great insurance</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:340px;'>
					<h3>RECENT BLOG POSTS</h3><div><div><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/blog/how-to-use-pr-to-manage-your-online-reputation/">How to use PR to Manage Your Online Reputation</a></li><li><a href="/small-business-insurance/blog/whos-stealing-from-small-businesses-in-the-us/">Who’s Stealing from Small Businesses In the US?</a></li><li><a href="/small-business-insurance/blog/how-small-business-owners-can-successfully-plan-for-retirement/">How Small Business Owners Can Successfully Plan...</a></li><li><a href="/small-business-insurance/blog/3-things-it-consultants-must-know-to-protect-their-businesses/">3 Things IT Consultants Must Know to Protect...</a></li><li><a href="/small-business-insurance/blog/mompreneur-spotlight-a-day-in-the-life-of-juniors-fresh-ceo-michelle-hoffmann/">MomPreneur Spotlight: A Day in the Life of Junior’s...</a></li><li><a class="reg-link" href="/small-business-insurance/blog/">More Posts</a></li></ul></div></div>
				</div>
            
												
				<div class='panel-col  last' style='width:340px;'>
					<p>Our blog is a "go to" resource for your small business. Find valuable information and advice on making your business a huge success.<br /><br /></p><h3>Join the conversation</h3><ul class="panel-icons-no-width"><li><a href="http://www.facebook.com/hiscoxsmallbiz" target="_blank" shape="rect"><img id="icn-facebook-sbl" src="/small-business-insurance/shared-images/icn-facebook-sbl.gif" alt="Hiscox Small Business Insurance facebook" title="Hiscox Small Business Insurance facebook" border="0" height="29" width="29" /></a></li><li><a href="http://www.twitter.com/hiscoxsmallbiz" target="_blank" shape="rect"><img id="icn-twitter-sbl" src="/small-business-insurance/shared-images/icn-twitter-sbl.gif" alt="Hiscox Small Business Insurance twitter" title="Hiscox Small Business Insurance twitter" border="0" height="29" width="29" /></a></li><li><a href="http://www.linkedin.com/company/hiscox-small-business-insurance/" target="_blank" shape="rect"><img id="icn-linkedin-sbl" src="/small-business-insurance/shared-images/icn-linkedin-sbl.gif" alt="Hiscox Small Business Insurance linkedin" title="Hiscox Small Business Insurance linkedin" border="0" height="29" width="29" /></a></li><li><a href="http://plus.google.com/117977778162613990129/" target="_blank" shape="rect"><img id="icn-google-plus-sbl" src="/small-business-insurance/shared-images/icn-google-plus-sbl.gif" alt="Follow us on Google Plus" title="Follow us on Google Plus" border="0" height="29" width="29" /></a></li><li><a href="http://www.youtube.com/hiscoxinsurance/" target="_blank" shape="rect"><img id="icn-youtube-sbl" src="/small-business-insurance/shared-images/icn-youtube-sbl.gif" alt="Hiscox Small Business Insurance youtube" title="Hiscox Small Business Insurance youtube" border="0" height="29" width="29" /></a></li><li><a href="/small-business-insurance/blog/feed/" target="_blank" shape="rect"><img id="icn-rss-sbl" src="/small-business-insurance/shared-images/icn-rss-sbl.gif" alt="Hiscox Small Business Insurance RSS" title="Hiscox Small Business Insurance RSS" border="0" height="29" width="29" /></a></li></ul>
				</div>
            		</div>
		
		
	</div>

	
      								</li>    																																		                                							    								<li>    									<a class="top-nav-link" href="/small-business-insurance/manage-your-policy/" title="Manage Your Policy">Manage Your Policy</a>    									
	
	<div class="panel">
		<div class="panel-top"><div class="panel-marker"></div></div>
		<div class="panel-body">
			<div class="panel-header">
				<h2>It's quick and easy to manage your policy online</h2>							</div>
			
			
			
												
				<div class='panel-col ' style='width:200px;'>
					<h3>Manage Your Policy</h3><ul class="mega-menu-red-arrow"><li><a href="https://www.hiscoxpolicymanagement.com/" onclick="_gaq.push(['_trackEvent', 'manage-your-policy', 'click', 'additional-insured', 3, false]);" rel="nofollow" target="_blank">Add an Additional Insured</a></li><li><a href="https://www.hiscoxpolicymanagement.com/" onclick="_gaq.push(['_trackEvent', 'manage-your-policy', 'click', 'certificate-of-insurance', 3, false]);" rel="nofollow" target="_blank">Get a Certificate of Insurance</a></li><li><a href="https://www.hiscoxpolicymanagement.com/" onclick="_gaq.push(['_trackEvent', 'manage-your-policy', 'click', 'acord-certificate', 3, false]);" rel="nofollow" target="_blank">Get an Acord Certificate</a></li></ul>
				</div>
            
												
				<div class='panel-col ' style='width:200px;'>
					<ul class="mega-menu-red-arrow"><li><a href="https://www.hiscoxpolicymanagement.com/" onclick="_gaq.push(['_trackEvent', 'manage-your-policy', 'click', 'change-address', 3, false]);" rel="nofollow" target="_blank">Change Your Address</a></li><li><a href="https://www.hiscoxpolicymanagement.com/" onclick="_gaq.push(['_trackEvent', 'manage-your-policy', 'click', 'change-business-name', 3, false]);" rel="nofollow" target="_blank">Change Your Business Name</a></li><li><a href="https://www.hiscoxpolicymanagement.com/" onclick="_gaq.push(['_trackEvent', 'manage-your-policy', 'click', 'add-authorized-person', 3, false]);" rel="nofollow" target="_blank">Add an Authorized Person</a></li><li><a href="https://www.hiscoxpolicymanagement.com/" onclick="_gaq.push(['_trackEvent', 'manage-your-policy', 'click', 'request-policy-documents', 3, false]);" rel="nofollow" target="_blank">Request Policy Documents</a></li></ul>
				</div>
            
												
				<div class='panel-col  last' style='width:200px;'>
					<h3>Claim Center</h3><ul class="mega-menu-red-arrow"><li><a href="/small-business-insurance/claims-center/">Overview</a></li><li><a href="/small-business-insurance/claims-center/process/">Claims process</a></li><li><a href="/small-business-insurance/claims-center/report-a-claim/">Report a claim</a></li><li><a href="/small-business-insurance/claims-center/claim-faqs/">Claims questions</a></li><li><a href="/small-business-insurance/claims-center/reviews/">Claims Reviews</a></li></ul>
				</div>
            		</div>
		
		
	</div>

	
      								</li>    																																																																																																																																																																																																																				</ul>
		</div>
		
    
    			</div>
    		</div><!-- header -->
    		
			<div class="content-wrapper">
        		<div class="landing-page-container">
        			
        				
		
		
	
	
	<script type="text/javascript">
		$(document).ready(function () {
		
			$('.slider').bxSlider({
				auto: false,
				pager: false,
				controls: false,
				autoDirection: 'next',
				touchEnabled: false,
				pause: 0
			});
			
		});
	</script>
	
	<div class='slider'>

	<div>
    	                            	            		<img id='Slider image - IT consultant new' src='/small-business-insurance/_rotator/business-consulting-insurance-slider.jpg' alt='Coverage tailored to the risks in your industries.' title='Coverage tailored to the risks in your industries.'   height="365"  width="960"  />	
    	    	<div class="slider-captions">
    		<h1 style="color:#333333">Voted as a Top 100 <br>
Small Business Influencer <br>
of 2014!</h1>
<p style="color:#333333">Tailored coverage as low as $22.50 per month.<br>
We specialize in providing insurance tailored to <br>
your specific business needs.</p>
<a class="slider-link"  href="/small-business-insurance/liability-insurance/" style="color:#333333">Find tailored insurance now</a>
    	</div>
	</div>
	
  </div>	
  		
		
	
	<script type="text/javascript">
	
		function historyCallback(e) { 
			if($(".btn-getquote a#inline_content_button").attr("href").length == 0) {
              $(".btn-getquote a#inline_content_button").attr("href", "#inline_content");
           };
	   
	   		busCatCallback();
			primServiceCallback();
		}
		
		function busCatCallback() {
			/*var buscat = $.bbq.getState("buscat");
			if (buscat != undefined && buscat != "Business Category" && buscat != "") {
				$('#businesscategory_dropdown_id').val(buscat);
				
								var buscatText = $("#businesscategory_dropdown_id option:selected").text();
    			if(buscatText.length >= 30) {
            		$('#businesscategory_dropdown_id-button .ui-selectmenu-status').text(buscatText.substring(0, buscatText.lastIndexOf(' ')) + ' ...');
    			}*/ 
    			
    			//loadPrimaryBusiness(document.getElementById('businesscategory_dropdown_id'), 'primarybusiness_dropdown_id'); 
				loadPrimaryBusiness('primarybusiness_dropdown_id'); 
			//}
		}
		
		function primServiceCallback() {
			var primService = $.bbq.getState("primService");
			if (primService != undefined && primService != "Primary Service" && primService != "") {
				$('#primarybusiness_dropdown_id').val(primService); 
				
				var primbusText = $("#primarybusiness_dropdown_id option:selected").text();
				if(primbusText.length >=28) {
					while (primbusText.length >=28) {
						primbusText = primbusText.substring(0, 28);
                        if(primbusText.indexOf(' ')>0)
                        {
                          primbusText = primbusText.substring(0, primbusText.lastIndexOf(' '));
                        }
                        else if(primbusText.indexOf('/'))
						{
                           primbusText = primbusText.substring(0, primbusText.lastIndexOf('/'));
						}

					}
                    $('#primarybusiness_dropdown_id-button .ui-selectmenu-status').text(primbusText + ' ...');
                } else {
                 	$('#primarybusiness_dropdown_id-button .ui-selectmenu-status').text(primbusText);
				} 
			}
		}
		
		$(document).ready(function () {
			$(window).bind("hashchange", historyCallback);
			
			
			
      		$('#quote-and-buy-form-script').attr('style','display:inline;')
			
			if(!jQuery.browser.mobile) {
        		$('select.selectbox').selectmenu({ style: 'dropdown', menuWidth: ($(this).find('select.selectbox').width() + 20), transferClasses: true});
				
			} else {
        		$('select.selectbox').attr("style","-webkit-appearance:menulist;");
			}
			
			$('#primarybusiness_dropdown_id-menu li a').click(function() {
				var textValue = $(this).text();
				var primValue = $("#primarybusiness_dropdown_id option").filter(function () { return $(this).text() == textValue; }).val();
				$("#primarybusiness_dropdown_id option:selected").val(primValue); 
			});
			
        	$('#primarybusiness_dropdown_id-button').click(function() {
        		if(jQuery.browser.version == "7.0") {
                  document.getElementById("primarybusiness_dropdown_id").value = "Primary Service";
                } else {
                  $("#primarybusiness_dropdown_id").val("Primary Service");
                }
			});
			
			/*$('#businesscategory_dropdown_id').change(function() {
				var buscatText = $("#businesscategory_dropdown_id option:selected").text();
				var buscatValue = $("#businesscategory_dropdown_id option:selected").val();
    			$.bbq.pushState({ buscat: buscatValue }, 2);
    		
    			if(buscatText.length >= 30) {
            		$('#businesscategory_dropdown_id-button .ui-selectmenu-status').text(buscatText.substring(0, buscatText.lastIndexOf(' ')) + ' ...');
    			}
			});*/
			
			$('#primarybusiness_dropdown_id').change(function() {
				var primbusText = $("#primarybusiness_dropdown_id option:selected").text();
				var primbusValue = $("#primarybusiness_dropdown_id option:selected").val();
    			$.bbq.pushState({ primService: primbusValue });
    			if(primbusText.length >=28) {
					while (primbusText.length >=28) {
						    primbusText = primbusText.substring(0, 28);
                            if(primbusText.indexOf(' ')>0)
                            {
                                   primbusText = primbusText.substring(0, primbusText.lastIndexOf(' '));
                            }
                            else
							{
                                   primbusText = primbusText.substring(0, primbusText.lastIndexOf('/'));
					        }

					}
                    $('#primarybusiness_dropdown_id-button .ui-selectmenu-status').text(primbusText + ' ...');
                } else {
                 	$('#primarybusiness_dropdown_id-button .ui-selectmenu-status').text(primbusText);
				}
				
				
			});
			
			historyCallback();
		});
		
		var hsx_primary_business = ["Accounting|Accounting", "Actuarial services|Actuarial services", "Acupressure services|Acupressure services", "Acupuncture services|Acupuncture services", "Advertising|Advertising", "Air conditioning systems installation/repair|Air conditioning systems installation/repair", "Answering/paging services|Answering/paging services", "Appliance/electronic stores|Appliance/electronic stores", "Application development|Application development", "Application service provider|Application service provider", "Appliance and accessories installation/repair|Appliance and accessories installation/repair", "Architecture|Architecture", "Art therapy|Art therapy", "Auctioneering|Auctioneering", "Audiology|Audiology", "Beautician/cosmetology services|Beautician/cosmetology services", "Bookkeeping|Bookkeeping", "Brand consultant|Brand consultant", "Building/construction inspection|Building inspection", "Business consulting|Business consulting", "Business manager services|Business manager services", "Carpentry (interior only)|Carpentry (interior only)", "Carpet/furniture/upholstery cleaning(offsite only)|Carpet/furniture/upholstery cleaning(offsite only)", "Civil engineering|Civil engineering", "Claims adjusting|Claims adjusting", "Clock making/repair|Clock making/repair", "Clothing/apparel stores|Clothing/apparel stores", "Computer consulting|Computer consulting", "Computer programming services|Computer programming services", "Computer system/network developer|Computer system/network developer", "Control systems integration/automation|Control systems integration/automation", "Court reporting|Court reporting", "Credit counseling|Credit counseling", "Dance therapy|Dance therapy", "Data processing|Data processing", "Database designer|Database designer", "Dietician/nutrition|Diet/nutrition services", "Digital marketing|Digital marketing", "Direct marketing|Direct marketing", "Document preparation|Document preparation", "Door or window installation/repair|Door or window installation/repair", "Draftsman (including CAD/CAM)|Draftsman (including CAD/CAM)", "Drama therapy|Drama therapy", "Driveway or sidewalk paving/repaving|Driveway or sidewalk paving/repaving", "Drywall or wallboard installation/repair|Drywall or wallboard installation/repair", "Education consulting|Education consulting", "Electrical engineering|Electrical engineering", "Electrical work (interior only)|Electrical work (interior only)", "Engineering|Engineering", "Environmental engineering|Environmental engineering", "Esthetician services|Esthetician services", "Event planning/promotion|Event planning/promotion", "Executive placement|Executive placement", "Expert witness services|Expert witness services", "Exterior cleaning services|Exterior cleaning services", "Fence installation/repair|Fence installation/repair", "Financial auditing or consulting|Financial auditing or consulting", "First aid and CPR training|First aid and CPR training", "Floor covering installation(no ceramic tile/stone)|Floor covering installation(no ceramic tile/stone)", "Florists|Florists", "Glass installation/repair (no auto work)|Glass installation/repair (no auto work)", "Graphic design|Graphic design", "Handyperson (no roof work)|Handyperson (no roof work)", "Barber/hair stylist services|Hair stylist/barber services", "Heating/air conditioning install/repair(no LPG)|Heating/air conditioning install/repair(no LPG)", "Home furnishing stores|Home furnishing stores", "Home healthcare aide|Home healthcare aide", "Human Resources (HR) consulting|Human Resources (HR) consulting", "Hypnosis|Hypnosis", "Industrial engineering|Industrial engineering", "Insurance agent|Insurance agent", "Insurance inspector|Insurance inspector", "Interior design|Interior Design", "Interior finishing work|Interior finishing work", "Investment advice|Investment advice", "IT consulting|IT consulting", "IT project management|IT project management", "IT software/hardware training services|IT software/hardware training services", "Janitorial/cleaning services|Janitorial/cleaning services", "Jewelry stores|Jewelry stores", "Landscape architect|Landscape architect", "Landscaping/gardening services|Landscaping/gardening services", "Land Surveyor|Land Surveyor", "Lawn care services|Lawn care services", "Legal services|Legal services", "Life/career/executive coaching|Life/career/executive coaching", "Locksmiths|Locksmiths", "Masonry work|Masonry work", "Management consulting|Management consulting", "Manufacturer sales representative|Manufacturer sales representative", "Market research|Market research", "Marketing/media consulting|Marketing/media consulting", "Marriage and family therapy|Marriage and family therapy", "Massage therapy|Massage therapy", "Medical billing|Medical billing", "Mental health counseling|Mental health counseling", "Mortgage brokering/banking|Mortgage brokering/banking", "Music therapy|Music therapy", "Nail technician services|Nail technician services", "Notary services|Notary services", "Occupational therapy|Occupational therapy", "Other stores (with food/drinks)|Other stores (with food/drinks)", "Other stores (without food/drinks)|Other stores (without food/drinks)", "Painting (interior only)|Painting (interior only)", "Personal care assistant|Personal care assistant", "Personal concierge/assistant|Personal concierge/assistant", "Personal training (health and fitness)|Personal training (health and fitness)", "Photography|Photography", "Plastering or stucco work|Plastering or stucco work", "Plumbing (commercial/industrial)|Plumbing (commercial/industrial)", "Plumbing (residential/domestic)|Plumbing (residential/domestic)", "Process engineering|Process engineering", "Process server|Process server", "Project management|Project management", "Construction/project management|Project manager (architecture or engineering)", "Property management|Property management", "Psychology|Psychology", "Public relations|Public relations", "Real estate agent/broker|Real estate agent/broker", "Recruiting (employment placements)|Recruiting (employment placements)", "Research consulting|Research consulting", "Resume consulting|Resume consulting", "Safety consultant|Safety consultant", "Search engine services (SEO/SEM)|Search engine services (SEO/SEM)", "Sign painting/lettering (interior only)|Sign painting/lettering (interior only)", "Sign painting/lettering (exterior only)|Sign painting/lettering (exterior only)", "Snow blowing and removal (no auto coverage)|Snow blowing and removal (no auto coverage)", "Social media consultant|Social media consultant", "Social work services|Social work services", "Software development|Software development", "Speech therapy|Speech therapy", "Stock brokering|Stock brokering", "Strategy consultant|Strategy consultant", "Substance abuse counseling|Substance abuse counseling", "Talent agency|Talent agency", "Tax preparation|Tax preparation", "Technology services|Technology services", "Tile/stone/marble/mosaic/terrazzo work(int. only)|Tile/stone/marble/mosaic/terrazzo work(int. only)", "Training (business, vocational or life skills)|Training (business, vocational or life skills)", "Translating/interpreting|Translating/interpreting", "Transportation engineering|Transportation engineering", "Travel agency|Travel agency", "Trustee|Trustee services", "Tutoring|Tutoring", "Upholstery work|Upholstery work", "Value added reseller of computer hardware|Value added reseller of computer hardware", "Website design|Website design", "Window cleaning (nothing above 15 feet)|Window cleaning (nothing above 15 feet)", "Yoga/pilates instruction|Yoga/pilates instruction", "Other consulting services|Other consulting services", "Other marketing/PR services|Other marketing/PR services", "Other technology services|Other technology services", "NONE OF THE ABOVE|NONE OF THE ABOVE"];
		/*var hsx_primary_business_aed = ["Architecture|Architecture", "Building/construction inspection|Building inspection", "Civil engineering|Civil engineering", "Control systems integration/automation|Control systems integration/automation", "Draftsman (including CAD/CAM)|Draftsman (including CAD/CAM)", "Electrical engineering|Electrical engineering", "Engineering|Engineering", "Environmental engineering|Environmental engineering", "Industrial engineering|Industrial engineering", "Interior design|Interior Design", "Landscape architect|Landscape architect", "Process engineering|Process engineering", "Construction/project management|Project manager (architecture or engineering)", "Transportation engineering|Transportation engineering"];
		var hsx_primary_business_consulting = ["Business consulting|Business consulting", "Education consulting|Education consulting", "Financial auditing or consulting|Financial auditing or consulting", "Human Resources (HR) consulting|Human Resources (HR) consulting", "IT consulting|IT consulting", "IT project management|IT project management", "IT software/hardware training services|IT software/hardware training services", "Management consulting|Management consulting", "Marketing/media consulting|Marketing/media consulting", "Project management|Project management", "Research consulting|Research consulting", "Resume consulting|Resume consulting", "Strategy consultant|Strategy consultant", "Training (business, vocational or life skills)|Training (business, vocational or life skills)", "Other consulting services|Other consulting services"];
		var hsx_primary_business_creative = ["Advertising|Advertising", "Application development|Application development", "Brand consultant|Brand consultant", "Event planning/promotion|Event planning/promotion", "Graphic design|Graphic design", "Interior design|Interior Design", "Photography|Photography", "Search engine services (SEO/SEM)|Search engine services (SEO/SEM)", "Social media consultant|Social media consultant", "Website design|Website design"];
		var hsx_primary_business_financial_services = ["Accounting|Accounting", "Actuarial services|Actuarial services", "Auctioneering|Auctioneering", "Bookkeeping|Bookkeeping", "Claims adjusting|Claims adjusting", "Credit counseling|Credit counseling", "Financial auditing or consulting|Financial auditing or consulting", "Investment advice|Investment advice", "Medical billing|Medical billing", "Mortgage brokering/banking|Mortgage brokering/banking", "Notary services|Notary services", "Stock brokering|Stock brokering", "Tax preparation|Tax preparation", "Trustee|Trustee services"];
		var hsx_primary_business_hbw = ["Acupressure services|Acupressure services", "Acupuncture services|Acupuncture services", "Art therapy|Art therapy", "Audiology|Audiology", "Beautician/cosmetology services|Beautician/cosmetology services", "Dance therapy|Dance therapy", "Dietician/nutrition|Diet/nutrition services", "Drama therapy|Drama therapy", "Esthetician services|Esthetician services", "First aid and CPR training|First aid and CPR training", "Barber/hair stylist services|Hair stylist/barber services", "Hypnosis|Hypnosis", "Marriage and family therapy|Marriage and family therapy", "Massage therapy|Massage therapy", "Medical billing|Medical billing", "Mental health counseling|Mental health counseling", "Music therapy|Music therapy", "Nail technician services|Nail technician services", "Occupational therapy|Occupational therapy", "Personal training (health and fitness)|Personal training (health and fitness)", "Psychology|Psychology", "Social work services|Social work services", "Speech therapy|Speech therapy", "Substance abuse counseling|Substance abuse counseling", "Yoga/pilates instruction|Yoga/pilates instruction"];
		var hsx_primary_business_legal_services = ["Claims adjusting|Claims adjusting", "Court reporting|Court reporting", "Document preparation|Document preparation", "Expert witness services|Expert witness services", "Legal services|Legal services", "Notary services|Notary services", "Process server|Process server", "Tax preparation|Tax preparation", "Trustee|Trustee services"];
		var hsx_primary_business_marketing_pr = ["Advertising|Advertising", "Answering/paging services|Answering/paging services", "Application development|Application development", "Brand consultant|Brand consultant", "Digital marketing|Digital marketing", "Direct marketing|Direct marketing", "Event planning/promotion|Event planning/promotion", "Graphic design|Graphic design", "Market research|Market research", "Marketing/media consulting|Marketing/media consulting", "Public relations|Public relations", "Search engine services (SEO/SEM)|Search engine services (SEO/SEM)", "Social media consultant|Social media consultant", "Website design|Website design", "Other marketing/PR services|Other marketing/PR services"];
		var hsx_primary_business_other = ["Answering/paging services|Answering/paging services", "Auctioneering|Auctioneering", "Business manager services|Business manager services", "Claims adjusting|Claims adjusting", "Court reporting|Court reporting", "Credit counseling|Credit counseling", "Document preparation|Document preparation", "Event planning/promotion|Event planning/promotion", "Executive placement|Executive placement", "Expert witness services|Expert witness services", "Financial auditing or consulting|Financial auditing or consulting", "Life/career/executive coaching|Life/career/executive coaching", "Medical billing|Medical billing", "Notary services|Notary services", "Personal concierge/assistant|Personal concierge/assistant", "Photography|Photography", "Project management|Project management", "Recruiting (employment placements)|Recruiting (employment placements)", "Resume consulting|Resume consulting", "Talent agency|Talent agency", "Training (business, vocational or life skills)|Training (business, vocational or life skills)", "Translating/interpreting|Translating/interpreting", "Travel agency|Travel agency", "Trustee|Trustee services", "Tutoring|Tutoring"];
		var hsx_primary_business_real_estate = ["Property management|Property management", "Real estate agent/broker|Real estate agent/broker"];
		var hsx_primary_business_technology = ["Application development|Application development", "Application service provider|Application service provider", "Computer consulting|Computer consulting", "Computer programming services|Computer programming services", "Computer system/network developer|Computer system/network developer", "Data processing|Data processing", "Database designer|Database designer", "IT consulting|IT consulting", "IT project management|IT project management", "IT software/hardware training services|IT software/hardware training services", "Software development|Software development", "Technology services|Technology services", "Value added reseller of computer hardware|Value added reseller of computer hardware", "Website design|Website design", "Other technology services|Other technology services"];
		
		function loadPrimaryBusiness(businesscategory, primarybusiness_id) {*/
		function loadPrimaryBusiness(primarybusiness_id) {
			var prim = document.getElementById(primarybusiness_id);	
			var buscat;

			/*switch(businesscategory.value)
			{
				case "Architecture, Engineering & Design":
					buscat = hsx_primary_business_aed;
					break;
				case "Consulting":
					buscat = hsx_primary_business_consulting;
					break;
				case "Creative":
					buscat = hsx_primary_business_creative;
					break;
				case "Financial Services":
					buscat = hsx_primary_business_financial_services;
					break;
				case "Health, Beauty & Wellness":
					buscat = hsx_primary_business_hbw;
					break;
				case "Legal services":
					buscat = hsx_primary_business_legal_services;
					break;
				case "Marketing/PR":
					buscat = hsx_primary_business_marketing_pr;
					break;
				case "Other Professional Services":
					buscat = hsx_primary_business_other;
					break;
				case "Real Estate":
					buscat = hsx_primary_business_real_estate;
					break;
				case "Technology":
					buscat = hsx_primary_business_technology;
					break;
				default:*/
               		buscat = hsx_primary_business;
             //}
				
			if(!jQuery.browser.mobile) {
    			
    			var prim_pres = document.getElementById(primarybusiness_id + "-menu");
    			
    			$(prim_pres).find("li:eq(0)").addClass('ui-selectmenu-item-selected');
    			$(prim_pres).find("li:eq(0) a").attr('aria-selected', 'true');
    			
    			$(prim_pres).find("li:gt(0)").removeClass('ui-selectmenu-item-selected').attr('style', 'display:none');
    			$(prim_pres).find("li:gt(0) a").attr('aria-selected', 'false').removeAttr('id');

    			for(var i = 0;i<buscat.length;i++) {
    				var dd_value = buscat[i].substring(buscat[i].indexOf('|') + 1);
					
					var presentation = $("#primarybusiness_dropdown_id-menu li a:contains('" + dd_value + "')");
					
					presentation.parent().attr('style', 'display:block');
    			}
    			
    			$("a#primarybusiness_dropdown_id-button span.ui-selectmenu-status").text("PRIMARY SERVICE");
    			$("#primarybusiness_dropdown_id").val("Primary service");
    			
    			$(prim).selectmenu("option", "menuWidth", $("#primarybusiness_dropdown_id").width() + 20);
    			
    			/*if(businesscategory.value == "NONE OF THE ABOVE") {
    				$("a#primarybusiness_dropdown_id-button").attr("style", "background:url('/resources/images/quote-select-disabled-landing-bg-sbl.png') top left no-repeat;");
    				$("a#primarybusiness_dropdown_id-button span.ui-selectmenu-icon").attr("style", "background: url('/resources/images/quote-select-disabled-arrow-down-sbl.png') no-repeat;");
    				$(prim).selectmenu("disable"); 
    			} else {
    				$("a#primarybusiness_dropdown_id-button").removeAttr("style");
    				$("a#primarybusiness_dropdown_id-button span.ui-selectmenu-icon").removeAttr("style");
    				$(prim).selectmenu("enable"); 
    			}*/
			} else {
    			
				$(prim).find("option:eq(0)").attr('selected','selected');
    			$(prim).find("option:gt(0)").remove();
				
    			for(var i = 0;i<buscat.length;i++) {
    				var dd_value = buscat[i].substring(0, buscat[i].indexOf('|'));
					var dd_text = buscat[i].substring(buscat[i].indexOf('|') + 1, buscat[i].length);
    				$(prim).append('<option value="' + dd_value + '">' + dd_text + '</option>');
    			}
    			/*if(businesscategory.value == "NONE OF THE ABOVE") {
    				$(prim).attr('disabled','disabled');
					if($(prim).find("option:eq(0)").text().indexOf("*") != -1) {
						$(prim).find("option:eq(0)").text($(prim).find("option:eq(0)").text().substring(2));
					}
    			} else {
    				$(prim).removeAttr('disabled'); 
    			}*/
			
			}
		
		}
		
		function checkValues(submitbuttonid) {
            var ysValue = $("#state_dropdown_id").val(); 
			/*var bcValue = $("#businesscategory_dropdown_id").val();*/
            var pbValue = $("#primarybusiness_dropdown_id").val();
            
            /*if(!ysValue || (pbValue == "Primary Service" && bcValue != "NONE OF THE ABOVE")) {*/
			if(!ysValue || (pbValue == "Primary Service")) {
			
              if(!jQuery.browser.mobile) {
              
                var ysText = $("#state_dropdown_id-button .ui-selectmenu-status").text();
                /*var bcText = $("#businesscategory_dropdown_id-button .ui-selectmenu-status").text();*/
                var pbText = $("#primarybusiness_dropdown_id-button .ui-selectmenu-status").text();
    
                if(!ysValue && ysText.indexOf("*") == -1) {
                  $("#state_dropdown_id-button .ui-selectmenu-status").prepend("<span class='validation'>*</span> ");
                }
                
                /*if(pbValue == "Primary Service" && bcValue != "NONE OF THE ABOVE" && pbText.indexOf("*") == -1) {*/
				if(pbValue == "Primary Service" && pbText.indexOf("*") == -1) {
                  $("#primarybusiness_dropdown_id-button .ui-selectmenu-status").prepend("<span class='validation'>*</span> ");
                }
                
                $(".btn-getquote a#inline_content_button").colorbox({speed:150, inline:true, title:true, href:"#inline_content", innerWidth:420, opacity:0.5});
				$("#cboxTitle").removeAttr("style");
				
              } else {
              
                var ysText = $("#state_dropdown_id option").eq(0).text();
                /*var bcText = $("#businesscategory_dropdown_id option").eq(0).text();*/
    			var pbText = $("#primarybusiness_dropdown_id option").eq(0).text();
                
				if(!ysValue && ysText.indexOf("*") == -1) {
                  $("#state_dropdown_id option").eq(0).text("* " + ysText);
                }
                /*if(pbValue == "Primary Service" && bcValue != "NONE OF THE ABOVE" && pbText.indexOf("*") == -1) {*/
                if(pbValue == "Primary Service" && pbText.indexOf("*") == -1) {
                  $("#primarybusiness_dropdown_id option").eq(0).text("* " + pbText);
                }
                
                alert("Please fill out all the required fields");
              
              }
              
            } else {
              $(".btn-getquote a#inline_content_button").colorbox.remove();
              $(".btn-getquote a#inline_content_button").removeAttr("href");
			  document.getElementById(submitbuttonid).click();
            }
          }
		
		</script>
	
	<div>
		<form action="/small-business-insurance/quote-and-buy/brochureware/" id="quote-and-buy-form-script" style="display:none;" method="post">
				
				<div class="quote-box-wrapper">
					<div class="quote-box-inner">
						<div class="quote-box">
						
							<div class="quote-col-one">
								<div class="quote-hr"></div>
								<div class="quote-caption">
									<h2>Get a fast, free quote now</h2>								</div>
								<div class="quote-hr"></div>
							</div>
							<div class="quote-col-two">
								<select class="selectbox" name="state" id="state_dropdown_id">
									<option value="" selected="selected">YOUR STATE</option>
																			<option value="AL">Alabama</option>
																			<option value="AK">Alaska</option>
																			<option value="AZ">Arizona</option>
																			<option value="AR">Arkansas</option>
																			<option value="CA">California</option>
																			<option value="CO">Colorado</option>
																			<option value="CT">Connecticut</option>
																			<option value="DE">Delaware</option>
																			<option value="DC">District Of Columbia</option>
																			<option value="FL">Florida</option>
																			<option value="GA">Georgia</option>
																			<option value="HI">Hawaii</option>
																			<option value="ID">Idaho</option>
																			<option value="IL">Illinois</option>
																			<option value="IN">Indiana</option>
																			<option value="IA">Iowa</option>
																			<option value="KS">Kansas</option>
																			<option value="KY">Kentucky</option>
																			<option value="LA">Louisiana</option>
																			<option value="ME">Maine</option>
																			<option value="MD">Maryland</option>
																			<option value="MA">Massachusetts</option>
																			<option value="MI">Michigan</option>
																			<option value="MN">Minnesota</option>
																			<option value="MS">Mississippi</option>
																			<option value="MO">Missouri</option>
																			<option value="MT">Montana</option>
																			<option value="NE">Nebraska</option>
																			<option value="NV">Nevada</option>
																			<option value="NH">New Hampshire</option>
																			<option value="NJ">New Jersey</option>
																			<option value="NM">New Mexico</option>
																			<option value="NY">New York</option>
																			<option value="NC">North Carolina</option>
																			<option value="ND">North Dakota</option>
																			<option value="OH">Ohio</option>
																			<option value="OK">Oklahoma</option>
																			<option value="OR">Oregon</option>
																			<option value="PA">Pennsylvania</option>
																			<option value="RI">Rhode Island</option>
																			<option value="SC">South Carolina</option>
																			<option value="SD">South Dakota</option>
																			<option value="TN">Tennessee</option>
																			<option value="TX">Texas</option>
																			<option value="UT">Utah</option>
																			<option value="VT">Vermont</option>
																			<option value="VA">Virginia</option>
																			<option value="WA">Washington</option>
																			<option value="WV">West Virginia</option>
																			<option value="WI">Wisconsin</option>
																			<option value="WY">Wyoming</option>
																	</select>
								<!--<select class="selectbox" name="businesscategory" id="businesscategory_dropdown_id" onChange="javascript:loadPrimaryBusiness(this, 'primarybusiness_dropdown_id')">
									<option value="Business Category" selected="selected">BUSINESS CATEGORY</option>
																			<option value="Architecture, Engineering & Design">Architecture, Engineering & Design</option>
																			<option value="Consulting">Consulting</option>
																			<option value="Creative">Creative Services</option>
																			<option value="Financial Services">Financial Services</option>
																			<option value="Health, Beauty & Wellness">Health, Beauty & Wellness</option>
																			<option value="Legal services">Legal services</option>
																			<option value="Marketing/PR">Marketing/PR</option>
																			<option value="Other Professional Services">Other Professional Services</option>
																			<option value="Real Estate">Real Estate</option>
																			<option value="Technology">Technology Services</option>
																			<option value="NONE OF THE ABOVE">NONE OF THE ABOVE</option>
																	</select>-->
							
								<select class="selectbox" name="primarybusiness" id="primarybusiness_dropdown_id">
									<option value="Primary Service" selected="selected">PRIMARY SERVICE</option>
																			<option value="Accounting">Accounting</option>
																			<option value="Actuarial services">Actuarial services</option>
																			<option value="Acupressure services">Acupressure services</option>
																			<option value="Acupuncture services">Acupuncture services</option>
																			<option value="Advertising">Advertising</option>
																			<option value="Air conditioning systems installation/repair">Air conditioning systems installation/repair</option>
																			<option value="Answering/paging services">Answering/paging services</option>
																			<option value="Appliance/electronic stores">Appliance/electronic stores</option>
																			<option value="Application development">Application development</option>
																			<option value="Application service provider">Application service provider</option>
																			<option value="Appliance and accessories installation/repair">Appliance and accessories installation/repair</option>
																			<option value="Architecture">Architecture</option>
																			<option value="Art therapy">Art therapy</option>
																			<option value="Auctioneering">Auctioneering</option>
																			<option value="Audiology">Audiology</option>
																			<option value="Beautician/cosmetology services">Beautician/cosmetology services</option>
																			<option value="Bookkeeping">Bookkeeping</option>
																			<option value="Brand consultant">Brand consultant</option>
																			<option value="Building/construction inspection">Building inspection</option>
																			<option value="Business consulting">Business consulting</option>
																			<option value="Business manager services">Business manager services</option>
																			<option value="Carpentry (interior only)">Carpentry (interior only)</option>
																			<option value="Carpet/furniture/upholstery cleaning(offsite only)">Carpet/furniture/upholstery cleaning(offsite only)</option>
																			<option value="Civil engineering">Civil engineering</option>
																			<option value="Claims adjusting">Claims adjusting</option>
																			<option value="Clock making/repair">Clock making/repair</option>
																			<option value="Clothing/apparel stores">Clothing/apparel stores</option>
																			<option value="Computer consulting">Computer consulting</option>
																			<option value="Computer programming services">Computer programming services</option>
																			<option value="Computer system/network developer">Computer system/network developer</option>
																			<option value="Control systems integration/automation">Control systems integration/automation</option>
																			<option value="Court reporting">Court reporting</option>
																			<option value="Credit counseling">Credit counseling</option>
																			<option value="Dance therapy">Dance therapy</option>
																			<option value="Data processing">Data processing</option>
																			<option value="Database designer">Database designer</option>
																			<option value="Dietician/nutrition">Diet/nutrition services</option>
																			<option value="Digital marketing">Digital marketing</option>
																			<option value="Direct marketing">Direct marketing</option>
																			<option value="Document preparation">Document preparation</option>
																			<option value="Door or window installation/repair">Door or window installation/repair</option>
																			<option value="Draftsman (including CAD/CAM)">Draftsman (including CAD/CAM)</option>
																			<option value="Drama therapy">Drama therapy</option>
																			<option value="Driveway or sidewalk paving/repaving">Driveway or sidewalk paving/repaving</option>
																			<option value="Drywall or wallboard installation/repair">Drywall or wallboard installation/repair</option>
																			<option value="Education consulting">Education consulting</option>
																			<option value="Electrical engineering">Electrical engineering</option>
																			<option value="Electrical work (interior only)">Electrical work (interior only)</option>
																			<option value="Engineering">Engineering</option>
																			<option value="Environmental engineering">Environmental engineering</option>
																			<option value="Esthetician services">Esthetician services</option>
																			<option value="Event planning/promotion">Event planning/promotion</option>
																			<option value="Executive placement">Executive placement</option>
																			<option value="Expert witness services">Expert witness services</option>
																			<option value="Exterior cleaning services">Exterior cleaning services</option>
																			<option value="Fence installation/repair">Fence installation/repair</option>
																			<option value="Financial auditing or consulting">Financial auditing or consulting</option>
																			<option value="First aid and CPR training">First aid and CPR training</option>
																			<option value="Floor covering installation(no ceramic tile/stone)">Floor covering installation(no ceramic tile/stone)</option>
																			<option value="Florists">Florists</option>
																			<option value="Glass installation/repair (no auto work)">Glass installation/repair (no auto work)</option>
																			<option value="Graphic design">Graphic design</option>
																			<option value="Handyperson (no roof work)">Handyperson (no roof work)</option>
																			<option value="Barber/hair stylist services">Hair stylist/barber services</option>
																			<option value="Heating/air conditioning install/repair(no LPG)">Heating/air conditioning install/repair(no LPG)</option>
																			<option value="Home furnishing stores">Home furnishing stores</option>
																			<option value="Home healthcare aide">Home healthcare aide</option>
																			<option value="Human Resources (HR) consulting">Human Resources (HR) consulting</option>
																			<option value="Hypnosis">Hypnosis</option>
																			<option value="Industrial engineering">Industrial engineering</option>
																			<option value="Insurance agent">Insurance agent</option>
																			<option value="Insurance inspector">Insurance inspector</option>
																			<option value="Interior design">Interior Design</option>
																			<option value="Interior finishing work">Interior finishing work</option>
																			<option value="Investment advice">Investment advice</option>
																			<option value="IT consulting">IT consulting</option>
																			<option value="IT project management">IT project management</option>
																			<option value="IT software/hardware training services">IT software/hardware training services</option>
																			<option value="Janitorial/cleaning services">Janitorial/cleaning services</option>
																			<option value="Jewelry stores">Jewelry stores</option>
																			<option value="Landscape architect">Landscape architect</option>
																			<option value="Landscaping/gardening services">Landscaping/gardening services</option>
																			<option value="Land Surveyor">Land Surveyor</option>
																			<option value="Lawn care services">Lawn care services</option>
																			<option value="Legal services">Legal services</option>
																			<option value="Life/career/executive coaching">Life/career/executive coaching</option>
																			<option value="Locksmiths">Locksmiths</option>
																			<option value="Masonry work">Masonry work</option>
																			<option value="Management consulting">Management consulting</option>
																			<option value="Manufacturer sales representative">Manufacturer sales representative</option>
																			<option value="Market research">Market research</option>
																			<option value="Marketing/media consulting">Marketing/media consulting</option>
																			<option value="Marriage and family therapy">Marriage and family therapy</option>
																			<option value="Massage therapy">Massage therapy</option>
																			<option value="Medical billing">Medical billing</option>
																			<option value="Mental health counseling">Mental health counseling</option>
																			<option value="Mortgage brokering/banking">Mortgage brokering/banking</option>
																			<option value="Music therapy">Music therapy</option>
																			<option value="Nail technician services">Nail technician services</option>
																			<option value="Notary services">Notary services</option>
																			<option value="Occupational therapy">Occupational therapy</option>
																			<option value="Other stores (with food/drinks)">Other stores (with food/drinks)</option>
																			<option value="Other stores (without food/drinks)">Other stores (without food/drinks)</option>
																			<option value="Painting (interior only)">Painting (interior only)</option>
																			<option value="Personal care assistant">Personal care assistant</option>
																			<option value="Personal concierge/assistant">Personal concierge/assistant</option>
																			<option value="Personal training (health and fitness)">Personal training (health and fitness)</option>
																			<option value="Photography">Photography</option>
																			<option value="Plastering or stucco work">Plastering or stucco work</option>
																			<option value="Plumbing (commercial/industrial)">Plumbing (commercial/industrial)</option>
																			<option value="Plumbing (residential/domestic)">Plumbing (residential/domestic)</option>
																			<option value="Process engineering">Process engineering</option>
																			<option value="Process server">Process server</option>
																			<option value="Project management">Project management</option>
																			<option value="Construction/project management">Project manager (architecture or engineering)</option>
																			<option value="Property management">Property management</option>
																			<option value="Psychology">Psychology</option>
																			<option value="Public relations">Public relations</option>
																			<option value="Real estate agent/broker">Real estate agent/broker</option>
																			<option value="Recruiting (employment placements)">Recruiting (employment placements)</option>
																			<option value="Research consulting">Research consulting</option>
																			<option value="Resume consulting">Resume consulting</option>
																			<option value="Safety consultant">Safety consultant</option>
																			<option value="Search engine services (SEO/SEM)">Search engine services (SEO/SEM)</option>
																			<option value="Sign painting/lettering (interior only)">Sign painting/lettering (interior only)</option>
																			<option value="Sign painting/lettering (exterior only)">Sign painting/lettering (exterior only)</option>
																			<option value="Snow blowing and removal (no auto coverage)">Snow blowing and removal (no auto coverage)</option>
																			<option value="Social media consultant">Social media consultant</option>
																			<option value="Social work services">Social work services</option>
																			<option value="Software development">Software development</option>
																			<option value="Speech therapy">Speech therapy</option>
																			<option value="Stock brokering">Stock brokering</option>
																			<option value="Strategy consultant">Strategy consultant</option>
																			<option value="Substance abuse counseling">Substance abuse counseling</option>
																			<option value="Talent agency">Talent agency</option>
																			<option value="Tax preparation">Tax preparation</option>
																			<option value="Technology services">Technology services</option>
																			<option value="Tile/stone/marble/mosaic/terrazzo work(int. only)">Tile/stone/marble/mosaic/terrazzo work(int. only)</option>
																			<option value="Training (business, vocational or life skills)">Training (business, vocational or life skills)</option>
																			<option value="Translating/interpreting">Translating/interpreting</option>
																			<option value="Transportation engineering">Transportation engineering</option>
																			<option value="Travel agency">Travel agency</option>
																			<option value="Trustee">Trustee services</option>
																			<option value="Tutoring">Tutoring</option>
																			<option value="Upholstery work">Upholstery work</option>
																			<option value="Value added reseller of computer hardware">Value added reseller of computer hardware</option>
																			<option value="Website design">Website design</option>
																			<option value="Window cleaning (nothing above 15 feet)">Window cleaning (nothing above 15 feet)</option>
																			<option value="Yoga/pilates instruction">Yoga/pilates instruction</option>
																			<option value="Other consulting services">Other consulting services</option>
																			<option value="Other marketing/PR services">Other marketing/PR services</option>
																			<option value="Other technology services">Other technology services</option>
																			<option value="NONE OF THE ABOVE">NONE OF THE ABOVE</option>
																	</select>
							</div>
							
							<div class="quote-col-three">
							  <div class="slide-button btn-getquote"><a id="inline_content_button" href="#inline_content" onclick="javascript:checkValues('startquote_button_id');" title="GET A QUOTE"><span>GET A QUOTE</span></a></div>
							  <div class="slide-button btn-retquote"><a href="/small-business-insurance/quote-and-buy/retrieve-a-quote/" title="RETRIEVE YOUR SAVED QUOTE"><span>RETRIEVE YOUR SAVED QUOTE</span></a></div>
							</div>
							
							
						</div>
					</div>
				</div>
				<div>
					<input type="submit" title="Start Quote" value="Get a quote" style="left:-9999px;display:none;" name="action_StartQuote_button" id="startquote_button_id" class="submit-input" />

					<input type="hidden" name="partner" id="partner" value="US Direct"/>
					<input type="hidden" name="partnerAgent" id="partnerAgent" value="88"/>
										<input type="hidden" name="agentName" value=""/>
					<input type="hidden" name="agentPhoneNumber" value=""/>
					<input type="hidden" name="agentEmail" value=""/>
					<input type="hidden" name="agencyName" value=""/>
					<input type="hidden" name="partnerRepresentative" value=""/>
					<input type="hidden" name="quoteOrigin" id="quoteOrigin" value="Online"/>
										
					
				</div>
				</form>
				
				<noscript>
				
				<form action="/small-business-insurance/quote-and-buy/brochureware/" id="quote-and-buy-form-noscript" method="post">
				
				<div class="quote-box-wrapper">
					<div class="quote-box-inner">
						<div class="quote-box">
						
							
							
							<div class="quote-col-one">
								<div class="quote-hr"></div>
								<div class="quote-caption">
									<h2>Get a fast, free quote now</h2>								</div>
								<div class="quote-hr"></div>
							</div>
							<div class="quote-col-two">
								<select class="selectbox" name="state" id="state_dropdown_id">
									<option value="" selected="selected">YOUR STATE</option>
																			<option value="AL">Alabama</option>
																			<option value="AK">Alaska</option>
																			<option value="AZ">Arizona</option>
																			<option value="AR">Arkansas</option>
																			<option value="CA">California</option>
																			<option value="CO">Colorado</option>
																			<option value="CT">Connecticut</option>
																			<option value="DE">Delaware</option>
																			<option value="DC">District Of Columbia</option>
																			<option value="FL">Florida</option>
																			<option value="GA">Georgia</option>
																			<option value="HI">Hawaii</option>
																			<option value="ID">Idaho</option>
																			<option value="IL">Illinois</option>
																			<option value="IN">Indiana</option>
																			<option value="IA">Iowa</option>
																			<option value="KS">Kansas</option>
																			<option value="KY">Kentucky</option>
																			<option value="LA">Louisiana</option>
																			<option value="ME">Maine</option>
																			<option value="MD">Maryland</option>
																			<option value="MA">Massachusetts</option>
																			<option value="MI">Michigan</option>
																			<option value="MN">Minnesota</option>
																			<option value="MS">Mississippi</option>
																			<option value="MO">Missouri</option>
																			<option value="MT">Montana</option>
																			<option value="NE">Nebraska</option>
																			<option value="NV">Nevada</option>
																			<option value="NH">New Hampshire</option>
																			<option value="NJ">New Jersey</option>
																			<option value="NM">New Mexico</option>
																			<option value="NY">New York</option>
																			<option value="NC">North Carolina</option>
																			<option value="ND">North Dakota</option>
																			<option value="OH">Ohio</option>
																			<option value="OK">Oklahoma</option>
																			<option value="OR">Oregon</option>
																			<option value="PA">Pennsylvania</option>
																			<option value="RI">Rhode Island</option>
																			<option value="SC">South Carolina</option>
																			<option value="SD">South Dakota</option>
																			<option value="TN">Tennessee</option>
																			<option value="TX">Texas</option>
																			<option value="UT">Utah</option>
																			<option value="VT">Vermont</option>
																			<option value="VA">Virginia</option>
																			<option value="WA">Washington</option>
																			<option value="WV">West Virginia</option>
																			<option value="WI">Wisconsin</option>
																			<option value="WY">Wyoming</option>
																	</select>
								<!--<select class="selectbox" name="businesscategory" id="businesscategory_dropdown_id">
									<option value="Business Category" selected="selected">BUSINESS CATEGORY</option>
																			<option value="Architecture, Engineering & Design">Architecture, Engineering & Design</option>
																			<option value="Consulting">Consulting</option>
																			<option value="Creative">Creative Services</option>
																			<option value="Financial Services">Financial Services</option>
																			<option value="Health, Beauty & Wellness">Health, Beauty & Wellness</option>
																			<option value="Legal services">Legal services</option>
																			<option value="Marketing/PR">Marketing/PR</option>
																			<option value="Other Professional Services">Other Professional Services</option>
																			<option value="Real Estate">Real Estate</option>
																			<option value="Technology">Technology Services</option>
																			<option value="NONE OF THE ABOVE">NONE OF THE ABOVE</option>
																	</select>-->
								
								<select class="selectbox" name="primarybusiness" id="primarybusiness_dropdown_id">
									<option value="Primary Service" selected="selected">PRIMARY SERVICE</option>
																			<option value="Accounting">Accounting</option>
																			<option value="Actuarial services">Actuarial services</option>
																			<option value="Acupressure services">Acupressure services</option>
																			<option value="Acupuncture services">Acupuncture services</option>
																			<option value="Advertising">Advertising</option>
																			<option value="Air conditioning systems installation/repair">Air conditioning systems installation/repair</option>
																			<option value="Answering/paging services">Answering/paging services</option>
																			<option value="Appliance/electronic stores">Appliance/electronic stores</option>
																			<option value="Application development">Application development</option>
																			<option value="Application service provider">Application service provider</option>
																			<option value="Appliance and accessories installation/repair">Appliance and accessories installation/repair</option>
																			<option value="Architecture">Architecture</option>
																			<option value="Art therapy">Art therapy</option>
																			<option value="Auctioneering">Auctioneering</option>
																			<option value="Audiology">Audiology</option>
																			<option value="Beautician/cosmetology services">Beautician/cosmetology services</option>
																			<option value="Bookkeeping">Bookkeeping</option>
																			<option value="Brand consultant">Brand consultant</option>
																			<option value="Building/construction inspection">Building inspection</option>
																			<option value="Business consulting">Business consulting</option>
																			<option value="Business manager services">Business manager services</option>
																			<option value="Carpentry (interior only)">Carpentry (interior only)</option>
																			<option value="Carpet/furniture/upholstery cleaning(offsite only)">Carpet/furniture/upholstery cleaning(offsite only)</option>
																			<option value="Civil engineering">Civil engineering</option>
																			<option value="Claims adjusting">Claims adjusting</option>
																			<option value="Clock making/repair">Clock making/repair</option>
																			<option value="Clothing/apparel stores">Clothing/apparel stores</option>
																			<option value="Computer consulting">Computer consulting</option>
																			<option value="Computer programming services">Computer programming services</option>
																			<option value="Computer system/network developer">Computer system/network developer</option>
																			<option value="Control systems integration/automation">Control systems integration/automation</option>
																			<option value="Court reporting">Court reporting</option>
																			<option value="Credit counseling">Credit counseling</option>
																			<option value="Dance therapy">Dance therapy</option>
																			<option value="Data processing">Data processing</option>
																			<option value="Database designer">Database designer</option>
																			<option value="Dietician/nutrition">Diet/nutrition services</option>
																			<option value="Digital marketing">Digital marketing</option>
																			<option value="Direct marketing">Direct marketing</option>
																			<option value="Document preparation">Document preparation</option>
																			<option value="Door or window installation/repair">Door or window installation/repair</option>
																			<option value="Draftsman (including CAD/CAM)">Draftsman (including CAD/CAM)</option>
																			<option value="Drama therapy">Drama therapy</option>
																			<option value="Driveway or sidewalk paving/repaving">Driveway or sidewalk paving/repaving</option>
																			<option value="Drywall or wallboard installation/repair">Drywall or wallboard installation/repair</option>
																			<option value="Education consulting">Education consulting</option>
																			<option value="Electrical engineering">Electrical engineering</option>
																			<option value="Electrical work (interior only)">Electrical work (interior only)</option>
																			<option value="Engineering">Engineering</option>
																			<option value="Environmental engineering">Environmental engineering</option>
																			<option value="Esthetician services">Esthetician services</option>
																			<option value="Event planning/promotion">Event planning/promotion</option>
																			<option value="Executive placement">Executive placement</option>
																			<option value="Expert witness services">Expert witness services</option>
																			<option value="Exterior cleaning services">Exterior cleaning services</option>
																			<option value="Fence installation/repair">Fence installation/repair</option>
																			<option value="Financial auditing or consulting">Financial auditing or consulting</option>
																			<option value="First aid and CPR training">First aid and CPR training</option>
																			<option value="Floor covering installation(no ceramic tile/stone)">Floor covering installation(no ceramic tile/stone)</option>
																			<option value="Florists">Florists</option>
																			<option value="Glass installation/repair (no auto work)">Glass installation/repair (no auto work)</option>
																			<option value="Graphic design">Graphic design</option>
																			<option value="Handyperson (no roof work)">Handyperson (no roof work)</option>
																			<option value="Barber/hair stylist services">Hair stylist/barber services</option>
																			<option value="Heating/air conditioning install/repair(no LPG)">Heating/air conditioning install/repair(no LPG)</option>
																			<option value="Home furnishing stores">Home furnishing stores</option>
																			<option value="Home healthcare aide">Home healthcare aide</option>
																			<option value="Human Resources (HR) consulting">Human Resources (HR) consulting</option>
																			<option value="Hypnosis">Hypnosis</option>
																			<option value="Industrial engineering">Industrial engineering</option>
																			<option value="Insurance agent">Insurance agent</option>
																			<option value="Insurance inspector">Insurance inspector</option>
																			<option value="Interior design">Interior Design</option>
																			<option value="Interior finishing work">Interior finishing work</option>
																			<option value="Investment advice">Investment advice</option>
																			<option value="IT consulting">IT consulting</option>
																			<option value="IT project management">IT project management</option>
																			<option value="IT software/hardware training services">IT software/hardware training services</option>
																			<option value="Janitorial/cleaning services">Janitorial/cleaning services</option>
																			<option value="Jewelry stores">Jewelry stores</option>
																			<option value="Landscape architect">Landscape architect</option>
																			<option value="Landscaping/gardening services">Landscaping/gardening services</option>
																			<option value="Land Surveyor">Land Surveyor</option>
																			<option value="Lawn care services">Lawn care services</option>
																			<option value="Legal services">Legal services</option>
																			<option value="Life/career/executive coaching">Life/career/executive coaching</option>
																			<option value="Locksmiths">Locksmiths</option>
																			<option value="Masonry work">Masonry work</option>
																			<option value="Management consulting">Management consulting</option>
																			<option value="Manufacturer sales representative">Manufacturer sales representative</option>
																			<option value="Market research">Market research</option>
																			<option value="Marketing/media consulting">Marketing/media consulting</option>
																			<option value="Marriage and family therapy">Marriage and family therapy</option>
																			<option value="Massage therapy">Massage therapy</option>
																			<option value="Medical billing">Medical billing</option>
																			<option value="Mental health counseling">Mental health counseling</option>
																			<option value="Mortgage brokering/banking">Mortgage brokering/banking</option>
																			<option value="Music therapy">Music therapy</option>
																			<option value="Nail technician services">Nail technician services</option>
																			<option value="Notary services">Notary services</option>
																			<option value="Occupational therapy">Occupational therapy</option>
																			<option value="Other stores (with food/drinks)">Other stores (with food/drinks)</option>
																			<option value="Other stores (without food/drinks)">Other stores (without food/drinks)</option>
																			<option value="Painting (interior only)">Painting (interior only)</option>
																			<option value="Personal care assistant">Personal care assistant</option>
																			<option value="Personal concierge/assistant">Personal concierge/assistant</option>
																			<option value="Personal training (health and fitness)">Personal training (health and fitness)</option>
																			<option value="Photography">Photography</option>
																			<option value="Plastering or stucco work">Plastering or stucco work</option>
																			<option value="Plumbing (commercial/industrial)">Plumbing (commercial/industrial)</option>
																			<option value="Plumbing (residential/domestic)">Plumbing (residential/domestic)</option>
																			<option value="Process engineering">Process engineering</option>
																			<option value="Process server">Process server</option>
																			<option value="Project management">Project management</option>
																			<option value="Construction/project management">Project manager (architecture or engineering)</option>
																			<option value="Property management">Property management</option>
																			<option value="Psychology">Psychology</option>
																			<option value="Public relations">Public relations</option>
																			<option value="Real estate agent/broker">Real estate agent/broker</option>
																			<option value="Recruiting (employment placements)">Recruiting (employment placements)</option>
																			<option value="Research consulting">Research consulting</option>
																			<option value="Resume consulting">Resume consulting</option>
																			<option value="Safety consultant">Safety consultant</option>
																			<option value="Search engine services (SEO/SEM)">Search engine services (SEO/SEM)</option>
																			<option value="Sign painting/lettering (interior only)">Sign painting/lettering (interior only)</option>
																			<option value="Sign painting/lettering (exterior only)">Sign painting/lettering (exterior only)</option>
																			<option value="Snow blowing and removal (no auto coverage)">Snow blowing and removal (no auto coverage)</option>
																			<option value="Social media consultant">Social media consultant</option>
																			<option value="Social work services">Social work services</option>
																			<option value="Software development">Software development</option>
																			<option value="Speech therapy">Speech therapy</option>
																			<option value="Stock brokering">Stock brokering</option>
																			<option value="Strategy consultant">Strategy consultant</option>
																			<option value="Substance abuse counseling">Substance abuse counseling</option>
																			<option value="Talent agency">Talent agency</option>
																			<option value="Tax preparation">Tax preparation</option>
																			<option value="Technology services">Technology services</option>
																			<option value="Tile/stone/marble/mosaic/terrazzo work(int. only)">Tile/stone/marble/mosaic/terrazzo work(int. only)</option>
																			<option value="Training (business, vocational or life skills)">Training (business, vocational or life skills)</option>
																			<option value="Translating/interpreting">Translating/interpreting</option>
																			<option value="Transportation engineering">Transportation engineering</option>
																			<option value="Travel agency">Travel agency</option>
																			<option value="Trustee">Trustee services</option>
																			<option value="Tutoring">Tutoring</option>
																			<option value="Upholstery work">Upholstery work</option>
																			<option value="Value added reseller of computer hardware">Value added reseller of computer hardware</option>
																			<option value="Website design">Website design</option>
																			<option value="Window cleaning (nothing above 15 feet)">Window cleaning (nothing above 15 feet)</option>
																			<option value="Yoga/pilates instruction">Yoga/pilates instruction</option>
																			<option value="Other consulting services">Other consulting services</option>
																			<option value="Other marketing/PR services">Other marketing/PR services</option>
																			<option value="Other technology services">Other technology services</option>
																			<option value="NONE OF THE ABOVE">NONE OF THE ABOVE</option>
																	</select>
							</div>
							<div class="quote-col-three">
								<div class='slide-button'>

	
	<input type="submit" class="noScriptBtn" name="action_StartQuote_button" title="Get a Quote NOSCRIPT button" value=""/>

  </div>								<div class="slide-button btn-retquote"><a href="/small-business-insurance/quote-and-buy/retrieve-a-quote/" title="RETRIEVE YOUR SAVED QUOTE"><span>RETRIEVE YOUR SAVED QUOTE</span></a></div>
							</div>
							
							<div class="quote-hr"></div>
							
						</div>
					</div>
				</div>
				<div>
					<input type="hidden" name="partner" id="partner" value="US Direct"/>
					<input type="hidden" name="partnerAgent" id="partnerAgent" value="88"/>
										<input type="hidden" name="agentName" value=""/>
					<input type="hidden" name="agentPhoneNumber" value=""/>
					<input type="hidden" name="agentEmail" value=""/>
					<input type="hidden" name="agencyName" value=""/>
					<input type="hidden" name="partnerRepresentative" value=""/>
					<input type="hidden" name="quoteOrigin" id="quoteOrigin" value="Online"/>
										
					   
					
					
				</div>
				</form>
				
				</noscript>

				<!-- This contains the hidden content for inline calls -->
				<div style='display:none'><div id='inline_content' style='padding:10px; background:#fff; font-size:2em;'><p><span style=" font-size: 30px; color: red;"><strong>*</strong></span> <strong>Please fill out all the required fields</strong></p></div></div>
			</div>
				
			<script type="text/javascript">	
				
				$(document).ready(function () {
            		$(".quote-caption h2").css("padding-top", function () { 
            				var t = $(this).height();
            				var p = $(".quote-caption").height();
        					return ((p - t) / 2);
            		});
				});
				
				
			</script>
			
  		
		<div class='home-lower'><div class='home-lower-inner'>
		<div class="hl-col multi-col"><h3>Insurance by Industry</h3><ul class="industry-icons"><li><div>
<a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/" shape="rect"><img id="icn-health-sbl" src="/small-business-insurance/shared-images/icn-health-sbl.gif" alt="Health &amp; Beauty" title="Health &amp; Beauty" border="0" height="48" width="54" /><br clear="none" />Health &amp; Beauty</a></div></li><li><div>
<a href="/small-business-insurance/professional-business-insurance/it-insurance/" shape="rect"><img id="icn-it-consulting-sbl" src="/small-business-insurance/shared-images/icn-it-consulting-sbl.gif" alt="IT/Technology" title="IT/Technology" border="0" height="48" width="54" /><br clear="none" />IT/Technology</a></div></li><li><div>
<a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/" shape="rect"><img id="icn-consulting-sbl" src="/small-business-insurance/shared-images/icn-consulting-sbl.gif" alt="Consulting" title="Consulting" border="0" height="48" width="54" /><br clear="none" />Consulting</a></div></li><li><div>
<a href="/small-business-insurance/professional-business-insurance/photographers-insurance/" shape="rect"><img id="icn-photography-sbl" src="/small-business-insurance/shared-images/icn-photography-sbl.gif" alt="Photography" title="Photography" border="0" height="48" width="54" /><br clear="none" />Photography</a></div></li><li><div>
<a href="/small-business-insurance/professional-business-insurance/real-estate-agent-insurance/" shape="rect"><img id="icn-real-estate-sbl" src="/small-business-insurance/shared-images/icn-real-estate-sbl.gif" alt="Real Estate" title="Real Estate" border="0" height="48" width="54" /><br clear="none" />Real Estate</a></div></li><li><div>
<a href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/" shape="rect"><img id="icn-architect-sbl" src="/small-business-insurance/shared-images/icn-architect-sbl.gif" alt="Architects" title="Architects" border="0" height="48" width="54" /><br clear="none" />Architects</a></div></li></ul><div class="col-more">See other <a href="/small-business-insurance/professional-business-insurance/">professions we insure</a></div></div>	
		<div class="hl-col tooltips-col"><h3>Tailored insurance policies</h3><ul class="tooltips"><li><a href="/small-business-insurance/liability-insurance/" title="Liability insurance protects your small business. Find the right insurance for you.">Liability Insurance Overview</a></li><li><a href="/small-business-insurance/general-liability-insurance/" title="Protect your business from third-party claims for bodily injury, related medical costs, and damage to someone else’s property.">General Liability Insurance</a></li><li><a href="/small-business-insurance/professional-liability-insurance/" title="Protect your business in case you’re sued for negligence, even if you didn’t make a mistake.">Professional Liability Insurance</a></li><li><a href="/small-business-insurance/errors-and-omissions-insurance/" title="Errors and omissions insurance protects your business if you’re sued for negligence or failure to perform professional services.">Errors and Omissions Insurance</a></li><li><a href="/small-business-insurance/business-owners-policy/">Business Owner’s Policy (BOP)</a></li><li><a href="/small-business-insurance/home-based-business-insurance/">Home Based Insurance Policies</a></li><li><a href="/small-business-insurance/contract-insurance-requirements/" title="Our liability coverages satisfy most standard contract insurance requirements. Policy documents emailed immediately!">Need Coverage for a Client Contract?</a></li><li><a href="/small-business-insurance/contract-insurance-requirements/additional-terms/" title="Making updates to your policy like adding an Additional Insured is quick and easy. Visit this page to learn more.">Need to Update Your Policy?</a></li></ul><p> </p><div class="col-more"> </div></div>	
	
	<script type="text/javascript">
		$(document).ready(function () {
		
			$('.customers-slider').bxSlider({
				auto: false,
				pager: false,
				controls: false,
				autoDirection: 'next',
				touchEnabled: false,
				pause: 5000
			});
			
		});
	</script>
	
	<div class="hl-col reviews-col">
		<h3><span itemprop="itemreviewed">Real customers, real reviews</span></h3>		<div class='customers-slider' >
		<a href="http://www.hiscox.com/small-business-insurance/business-insurance-reviews/" target="_blank" shape="rect"><img id="feefo3" src="/small-business-insurance/shared-images/US-Feefo-ISR-logo1.png" alt="Hiscox Business Insurance Reviews" title="Hiscox Business Insurance Reviews" border="0" height="115" width="240" /></a>	</div>		<div class="col-more"><div itemscope="" itemtype="http://data-vocabulary.org/Review-aggregate"><b>Rated <span itemprop="rating" itemscope="" itemtype="http://data-vocabulary.org/Rating"><span itemprop="average">97%</span></span> for service (<a href="/small-business-insurance/business-insurance-reviews/"><span itemprop="votes">4,033 reviews</span></a>)</b></div></div>	</div>
	
  </div></div>		
		
	        				
        				<div class="sb-footer">
        					<div class="sb-footer-inner">
        						


    						
			
									
											<div class='sb-footer-col'>
										
					<h3 class="box-heading-h3">Insurance by profession</h3><ul><li><a href="/small-business-insurance/professional-business-insurance/health-and-wellbeing/">Health, Beauty &amp; Wellbeing</a></li><li><a href="/small-business-insurance/professional-business-insurance/business-consultant-insurance/">Business/Marketing Consulting</a></li><li><a href="/small-business-insurance/professional-business-insurance/it-insurance/">IT / Technology</a></li><li><a href="/small-business-insurance/professional-business-insurance/photographers-insurance/">Photographers</a></li><li><a href="/small-business-insurance/professional-business-insurance/real-estate-agent-insurance/">Real Estate Agents</a></li><li><a href="/small-business-insurance/professional-business-insurance/architects-and-engineers-insurance/">Architects &amp; Engineers</a></li><li><a href="/small-business-insurance/professional-business-insurance/tradesman-insurance/">Tradesmen, Retail and more</a></li><li><a href="/small-business-insurance/professional-business-insurance/">View all professions</a></li></ul>
					</div>
							
    			
			
									
											<div class='sb-footer-col'>
										
					<h3>Small Business Insurance</h3><ul><li><a href="/small-business-insurance/general-liability-insurance/">General liability insurance</a></li><li><a href="/small-business-insurance/professional-liability-insurance/">Professional liability insurance</a></li><li><a href="/small-business-insurance/errors-and-omissions-insurance/">Errors and omissions insurance</a></li><li><a href="/small-business-insurance/business-owner-insurance/">Business owner insurance</a></li><li><a href="/small-business-insurance/liability-insurance/">Liability insurance</a></li><li><a href="/small-business-insurance/contract-insurance-requirements/">Need coverage for a client contract?</a></li></ul>
					</div>
							
    			
			
									
											<div class='sb-footer-col'>
										
					<h3>Insurance by State</h3><ul><li><a href="/small-business-insurance/california-business-insurance/">California business insurance</a></li><li><a href="/small-business-insurance/florida-business-insurance/">Florida business insurance</a></li><li><a href="/small-business-insurance/ny-business-insurance/">New York business insurance</a></li><li><a href="/small-business-insurance/texas-business-insurance/">Texas business insurance</a></li><li><a href="/small-business-insurance/illinois-business-insurance/">Illinois business insurance</a></li><li><a href="/shared-documents/illinois-notice-of-policy-moratorium-11-10-2013.pdf">Important notice on IL policies</a></li><li><a href="/small-business-insurance/state/">View all states</a></li></ul>
					</div>
							
    			
			
									
											<div class='sb-footer-col' style='width:270px; margin-right:0;'>
										
					<h3>Helpful Links</h3><div class="help-left"><ul><li><a href="/small-business-insurance/quote-and-buy/retrieve-a-quote/">Retrieve a Quote</a></li><li><a href="/small-business-insurance/business-insurance-reviews/">Customer Reviews</a></li><li><a href="/small-business-insurance/claims-center/report-a-claim/">Report a Claim online</a></li><li><a href="/small-business-insurance/blog/">Small Business Blog</a></li><li><a href="/small-business-insurance/hiscox-affiliate-program/">Become an Affiliate</a></li><li><a href="/small-business-insurance/refer-a-friend/">Refer a Friend</a></li><li><a href="/small-business-insurance/newsroom/">Hiscox Newsroom</a></li></ul></div><div class="help-right"><ul><li><a href="http://www.hiscoxgroup.com/en/careers.aspx" target="_blank">Careers</a></li><li><a href="http://www.hiscoxgroup.com/investors.aspx" target="_blank">Investors</a></li><li><a href="http://www.hiscoxgroup.com/" target="_blank">Hiscox Corporate Site</a></li><li><a href="http://www.bbb.org/new-york-city/business-reviews/insurance-services/hiscox-inc-in-new-york-ny-123713/#bbbonlineclick" target="_blank" title="Hiscox Inc. BBB Business Review"><img alt="Hiscox Inc. BBB Business Review" src="http://seal-newyork.bbb.org/seals/blue-seal-96-50-hiscox-inc-123713.png" style=" border: 0;" /></a></li></ul></div>
					</div>
							
    	    	
	 		
<div class="footer-toolbar">
	
    		<div class='ft-partners'>					
																<div> </div>
										
    	    		</div>
		 			
    		<div class='ft-contact'>					
																<div itemscope="" itemtype="http://schema.org/Corporation"><div class="ft-contact" style=" text-align: center;">Need help? Contact a licensed agent at Hiscox <span itemprop="telephone">1.866.283.7545</span></div></div>
										
    	    		</div>
		 			
    		<div class='ft-social'>					
																<ul><li><a href="http://www.facebook.com/hiscoxsmallbiz" target="_blank" shape="rect"><img id="icn-social-facebook-sbl" src="/small-business-insurance/shared-images/icn-social-facebook-sbl.gif" alt="Hiscox Small Busines Insurance facebook" title="Hiscox Small Busines Insurance facebook" border="0" height="20" width="20" /></a></li><li><a href="http://www.twitter.com/hiscoxsmallbiz" target="_blank" shape="rect"><img id="icn-social-twitter-sbl" src="/small-business-insurance/shared-images/icn-social-twitter-sbl.gif" alt="Hiscox Small Business Insurance twitter" title="Hiscox Small Business Insurance twitter" border="0" height="20" width="20" /></a></li><li><a href="http://www.linkedin.com/company/hiscox-small-business-insurance/" target="_blank" shape="rect"><img id="icn-social-linkedin-sbl" src="/small-business-insurance/shared-images/icn-social-linkedin-sbl.gif" alt="Hiscox Small Business Insurance linkedin" title="Hiscox Small Business Insurance linkedin" border="0" height="20" width="20" /></a></li><li><a href="http://plus.google.com/117977778162613990129/" target="_blank" shape="rect"><img id="icn-social-google-plus-sbl" src="/small-business-insurance/shared-images/icn-social-google-plus-sbl.gif" alt="Follow us on Google Plus" title="Follow us on Google Plus" border="0" height="20" width="20" /></a></li><li><a href="http://www.youtube.com/hiscoxinsurance/" target="_blank" shape="rect"><img id="icn-social-youtube-sbl" src="/small-business-insurance/shared-images/icn-social-youtube-sbl.gif" alt="Hiscox Small Business Insurance youtube" title="Hiscox Small Business Insurance youtube" border="0" height="20" width="20" /></a></li><li><a href="/small-business-insurance/blog/feed/" target="_blank" shape="rect"><img id="icn-social-rss-sbl" src="/small-business-insurance/shared-images/icn-social-rss-sbl.gif" alt="Hiscox Small Business Insurance RSS" title="Hiscox Small Business Insurance RSS" border="0" height="20" width="20" /></a></li></ul>
										
    	    		</div>
		 		</div>
						
        					</div>
        				</div>
    					<div class="sb-footer-btm"></div>
        				<div class="sb-baseline">
        					<div class="sb-baseline-inner">
        						<div class="baseline-copyright">
        							
	
    							
																<p class="body-copy-grey-small">© 2015 Hiscox Inc. All rights reserved.<br />Underwritten by Hiscox Insurance Company Inc.</p>
										
    	    		
		 		        						</div>
        						<div class="baseline-country">
        							
	
    							
							<div class="footer-dropdown-box">
                <label for="country">Not in the US?</label>
                <select name="country" id="country" onchange="if(this.options[this.selectedIndex].value!='')openChosenURL(this);">
                <option value="">Select your country</option>
                                                	                		<option value="http://www.hiscox.be">Belgium</option>
                	                		<option value="http://www.hiscox.bm">Bermuda</option>
                	                		<option value="http://www.hiscox.fr">France</option>
                	                		<option value="http://www.hiscox.de">Germany</option>
                	                		<option value="http://www.hiscox.ie">Ireland</option>
                	                		<option value="http://www.hiscox.nl">Netherlands</option>
                	                		<option value="http://www.hiscox.pt">Portugal</option>
                	                		<option value="http://www.hiscox.es">Spain</option>
                	                		<option value="http://www.hiscox.co.uk">United Kingdom</option>
                	                				</select></div>
						
    	    		
		 		        						</div>
        						<ul class='footer-links-home-box'>
	
	
    							
																  <li><p class="body-copy-grey">
	                    <a href="/accessibility/" title="" target="_self" >
							Accessibility</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/site-map.html" title="" target="_self" >
							Site map</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/terms-of-use/" title="" target="_self" >
							Terms of use</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/privacy-policy/" title="" target="_self" >
							Privacy policy</a></p>
					  </li>
										
    				
																  <li><p class="body-copy-grey">
	                    <a href="/legal-notices/" title="" target="_self" >
							Legal notices</a></p>
					  </li>
										
    	    		
		 		</ul>        						
        					</div>
        				</div>
																					<div class='sb-footer-text cnt-subsctn white-all-corner'><div class='top'><div class='right'><div class='left'><div class='middle'></div></div></div></div><div class='middle-content sb-footer-inner-text'><div class='middlecnt-container sb-middlecnt-container'><p>Underwritten by Hiscox Insurance Company Inc. (HICI), a Chicago-based insurance company. Based on a December 2012 analysis of the average premiums charged by HICI and three competing insurance companies in 38 states and DC for professional liability insurance ($1,000,000 policy limit) for: (a) An IT consultant ($200,000 annual revenue); and (b) A business consultant ($160,000 annual revenue).</p></div></div><div class='bottom clear-both'><div class='right'><div class='left'><div class='middle'></div></div></div></div></div>																	</div>
			
			</div>
			
			

		
		<script language="javascript" type="text/javascript" src="/resources/javascript/hiscoxcom.js"></script>
	<script language="javascript" type="text/javascript" src="/resources/javascript/jquery-merged.js"></script>
		
	 </body> 
</html>