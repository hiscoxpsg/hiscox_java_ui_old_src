package com.hiscox.sbweb.taglib;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;

import com.hiscox.sbweb.bean.HsxSBWebBean;
import com.hiscox.sbweb.constants.IHsxSBWebConstants;
import com.hiscox.sbweb.logger.HsxWebLogger;
import com.hiscox.sbweb.util.HsxSBWebPageBeanUtil;
import com.hiscox.sbweb.util.HsxSBWebResourceManager;
import com.hiscox.sbweb.util.HsxSBWebSessionManager;

/**
 * Custom tag lib class which initiates the XHTML rendering process.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBWebCheckOccupationTag implements Tag, IHsxSBWebConstants {

    /**
	 *
	 */
    private PageContext pc = null;
    private Tag parent = null;
    private String occupContent = STR_EMPTY;

    public static final HsxWebLogger logger = HsxWebLogger.getLogger();

    public void setPageContext(PageContext p) {
	pc = p;
    }

    public PageContext getContext() {
	return this.pc;
    }

    public void setParent(Tag t) {
	parent = t;
    }

    public Tag getParent() {
	return parent;
    }

    public String getOccupContent() {
	return occupContent;
    }

    public void setOccupContent(String occupContent) {
	this.occupContent = occupContent;
    }

    /**
     *
     * @param formContent
     * @return skip body constant
     */
    public int doStartTag() throws JspException {

	HsxSBWebBean webBean = (HsxSBWebBean) HsxSBWebResourceManager
		.getResource(WEB_BEAN);

	Map<String, String> businessMap = null;

	businessMap = HsxSBWebSessionManager.occupationMap;
	if (businessMap == null) {
	    businessMap = new HashMap<String, String>();
	}
	if (null != businessMap && businessMap.isEmpty()) {
	    businessMap.put(STR_DEFAULT, STR_RX_DEFAULT_OCCUPATION);
	}

	Map<String, String> sessionMap = webBean.getSessionMap();
	if (HsxSBWebPageBeanUtil.isMapEmpty(sessionMap)) {
	    sessionMap = new HashMap<String, String>();
	    sessionMap.put(PRIMARY_BUSINESS_CODE, STR_DEFAULT);
	    webBean.setSessionMap(sessionMap);
	} else {
	    if (!sessionMap.containsKey(PRIMARY_BUSINESS_CODE)) {
		sessionMap.put(PRIMARY_BUSINESS_CODE, STR_DEFAULT);
		webBean.setSessionMap(sessionMap);
	    }
	}

	final String primBusiness = sessionMap.get(PRIMARY_BUSINESS_CODE);

	final String occContentValue = businessMap.get(primBusiness);
	StringBuilder renderContent = new StringBuilder();
	

	if (StringUtils.isNotEmpty(occContentValue)) {
	    if (occContentValue.equalsIgnoreCase(occupContent)) {
		return EVAL_BODY_INCLUDE;
	    }
	} else {
	    if (STR_RX_DEFAULT_OCCUPATION.equalsIgnoreCase(occupContent)) {
		return EVAL_BODY_INCLUDE;
	    }
	}
	/*Start: To show error message in Top centre in Saved Quotes page when no quotes are available*/
	if(STR_RX_PAGE_ERROR_HEADING.equalsIgnoreCase(occupContent)){
		if(STR_RETRIEVE_A_QUOTE_BUTTON_NAME.equalsIgnoreCase(webBean.getRequestedAction())&& SAVED_QUOTE_EXCEPTION.equalsIgnoreCase(webBean.getNextScreenId())){
		renderContent.append("<h1 class=\"page-heading-h1\">Call us to access your quote or policy details</h1>");
		}else{
			return EVAL_BODY_INCLUDE;
		}
    }
	if(STR_RX_PAGE_ERROR_TOP_CENTER.equalsIgnoreCase(occupContent)){
		if(STR_RETRIEVE_A_QUOTE_BUTTON_NAME.equalsIgnoreCase(webBean.getRequestedAction())&& SAVED_QUOTE_EXCEPTION.equalsIgnoreCase(webBean.getNextScreenId())){
		renderContent.append("<p class=\"body-copy-grey\">Your quote or policy details are still available, but can no longer be accessed through our online system.</p><p class=\"body-copy-grey\">Please call our licensed advisors at 888-202-2973 (Mon-Fri, 8am-10pm EST). They can access your details and pick up where you left off,  helping you ensure that you've got the right coverage for your business.</p>");
		}else{
			return EVAL_BODY_INCLUDE;
		}
    }
	/*End: To show error message in Top center in Saved Quotes page when no quotes are available*/
	try {
		if (renderContent != null) {
		    pc.getOut().write(renderContent.toString());
		}
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		logger.error("IOException", e);
	    }
	if (STR_ALL.equalsIgnoreCase(occupContent)) {
	    return EVAL_BODY_INCLUDE;
	} else {
	    return SKIP_BODY;
	}

    }

    public int doEndTag() throws JspException {
	return EVAL_PAGE;
    }

    public void release() {
	pc = null;
	parent = null;
	occupContent = null;
    }

}

