package com.hiscox.sb.framework.validator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.simple.HsxSBDateWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBDateWidgetValidatorTest {
	
	private HsxSBDateWidgetValidator hsxSBDateWidgetValidator;
	private HsxSBDateWidget dateWidget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBDateWidgetValidator = new HsxSBDateWidgetValidator();
		dateWidget = new HsxSBDateWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setSavedValue("01/01/2015");
		hsxSBWidgetInfo.setAllowAnyDate("yes");
		dateWidget.setWidgetInfo(hsxSBWidgetInfo);
		
	}

	@After
	public void tearDown() throws Exception {
		dateWidget = null;
		hsxSBDateWidgetValidator = null;
		hsxSBWidgetInfo = null;
	}

	@Test
	public void test() {
		Boolean result = hsxSBDateWidgetValidator.validateWidget(dateWidget);
		Assert.assertTrue(result);
	}

}
