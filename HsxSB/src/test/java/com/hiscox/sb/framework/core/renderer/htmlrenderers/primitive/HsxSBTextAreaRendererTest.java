package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBTextAreaWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBTextAreaRendererTest {

    private HsxSBTextAreaRenderer hsxSBTextAreaRenderer;
    private HsxSBTextAreaWidget widget;
    private HsxSBWidgetInfo hsxSBWidgetInfo;
    private Map<String,String> additionalInfoMap;
    private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;

	@Before
	public void setUp() throws Exception {
		hsxSBTextAreaRenderer = new HsxSBTextAreaRenderer();
		widget = new HsxSBTextAreaWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setMaxLength("5");
		hsxSBWidgetInfo.setRows("3");
		hsxSBWidgetInfo.setColumns("3");
		hsxSBWidgetInfo.setSavedValue("test");
		hsxSBWidgetInfo.setDefaultValue("test");
		hsxSBWidgetInfo.setDisplayOnly("yes");
		hsxSBWidgetInfo.setStyle("test");
		hsxSBWidgetInfo.setErrorIndicator(true);
		additionalInfoMap = new HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		hsxSBTextAreaRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
	}

	@Test
	public void test() {
		String result = hsxSBTextAreaRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='control'><textarea name='test' maxlength='5' id='test_text_id' rows='3' cols='3' " +
							"disabled='disabled' " +"class='test'>test</textarea></div>", result);
	}

}
