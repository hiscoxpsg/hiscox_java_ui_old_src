package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.primitive.HsxSBPopUpButtonWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBPopUpButtonRendererTest {


	private HsxSBPopUpButtonRenderer hsxSBPopUpButtonRenderer ;
	private HsxSBPopUpButtonWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBPopUpButtonRenderer = new HsxSBPopUpButtonRenderer();
		widget = new HsxSBPopUpButtonWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setAdditionalText4("test");
		hsxSBWidgetInfo.setLabel("test");
		hsxSBWidgetInfo.setCode("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBPopUpButtonRenderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBPopUpButtonRenderer.renderContent(widget);
		Assert.assertNotNull(result);
	}

}
