package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBStaticWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBStaticRendererTest {

	private HsxSBStaticRenderer hsxSBStaticRenderer;
	private HsxSBStaticWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
	private Map<String,String> additionalInfoMap;

	@Before
	public void setUp() throws Exception {
		hsxSBStaticRenderer = new HsxSBStaticRenderer();
		widget= new HsxSBStaticWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setSavedValue("yes");
		hsxSBWidgetInfo.setSubType("TwoColumn");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setLabel("test");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		additionalInfoMap = new  HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}
	

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		hsxSBStaticRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
	}

	@Test
	public void test() {
		String result = hsxSBStaticRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='question-text'><label id='test'>yes</label></div>", result);
	}

}
