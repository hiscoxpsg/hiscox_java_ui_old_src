package com.hiscox.sb.framework.binding;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBUIRequestBinderTest {
	
	private HsxSBUIRequestBinder hsxSBUIRequestBinder;
	private Map<String, HsxSBWidget> widgetMap;
	private Map<String, String> requestValueMap;
	private HsxSBQuestionWidget hsxSBWidget;
	private HsxSBWidgetInfo widgetInfo;
	private HsxSBScreenWidget screenQuestionWidget;

	@Before
	public void setUp() throws Exception {
		hsxSBUIRequestBinder = new HsxSBUIRequestBinder();
		widgetMap = new HashMap<String, HsxSBWidget>();
		requestValueMap = new HashMap<String, String>();
		requestValueMap.put("test", "test");
		hsxSBWidget = new HsxSBQuestionWidget();
		screenQuestionWidget = new HsxSBScreenWidget();
		hsxSBWidget.setName("test");
		screenQuestionWidget.setName("test");
		widgetInfo = new HsxSBWidgetInfo();
		widgetInfo.setType("date");
		widgetInfo.setSubType("test");
		widgetInfo.setSavedValue("test");
		screenQuestionWidget.setWidgetInfo(widgetInfo);
		hsxSBWidget.setWidgetInfo(widgetInfo);
		widgetMap.put("test", hsxSBWidget);
		widgetMap.put("jsbuttonendsflagid", hsxSBWidget);
		widgetMap.put("test_question_id", screenQuestionWidget);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBUIRequestBinder = null;
		hsxSBWidget = null;
		requestValueMap = null;
		screenQuestionWidget = null;
		widgetInfo = null;
		widgetMap = null;
	}

	@Test
	public void test() {
		hsxSBUIRequestBinder.bindToWidgets(widgetMap, requestValueMap);
		Assert.assertTrue(widgetMap.containsKey("test_question_id"));
	}

}
