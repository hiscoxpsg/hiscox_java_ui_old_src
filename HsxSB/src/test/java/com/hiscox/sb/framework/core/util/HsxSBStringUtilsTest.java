package com.hiscox.sb.framework.core.util;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HsxSBStringUtilsTest {
	
	private HsxSBStringUtils hsxSBStringUtils;

	@Before
	public void setUp() throws Exception {
		hsxSBStringUtils = new  HsxSBStringUtils();
	}

	@After
	public void tearDown() throws Exception {
		hsxSBStringUtils = new HsxSBStringUtils();
	}

	@Test
	public void testRemoveBlanks() {
		String target = "test util";
		String result = hsxSBStringUtils.removeBlanks(target);
		Assert.assertEquals("testutil", result);
	}

	@Test
	public void testRemoveLeadingBlanks() {
		String target = "   testutil";
		String result = hsxSBStringUtils.removeLeadingBlanks(target);
		Assert.assertEquals("testutil", result);
	}

	@Test
	public void testLineWrapping() {
		String theString = "testutil";
		int lineLength = 10;
		String result = hsxSBStringUtils.lineWrapping(theString, lineLength);
		Assert.assertEquals("testutil", result);
	}
	
	@Test
	public void testToTitleCase() {
		String input = "test";
		String result = hsxSBStringUtils.toTitleCase(input);
		Assert.assertEquals("Test", result);
	}
	
	@Test
	public void testMatchesTemplate() {
		Boolean result = hsxSBStringUtils.matchesTemplate("test", "test", true);
		Assert.assertFalse(result);
	}
	
	@Test
	public void testRemoveCharacter() {
		String  result = hsxSBStringUtils.removeCharacter("test", 't');
		Assert.assertEquals("es", result);
	}
	
	@Test
	public void testDeleteRange() {
		String  result = hsxSBStringUtils.deleteRange("testutil", 2, 7);
		Assert.assertEquals("tel", result);
	}
	
	@Test
	public void testQuoted() {
		String  result = hsxSBStringUtils.quoted("test");
		Assert.assertEquals("'test'", result);
	}
	
	@Test
	public void testTokenize() {
		Object  result = hsxSBStringUtils.tokenize("test", "t");
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testReplaceSubstring() {
		String  result = hsxSBStringUtils.replaceSubstring("test", "t", "a");
		Assert.assertEquals("aesa", result);
	}
	
	@Test
	public void testFindInteger() {
		Object  result = hsxSBStringUtils.findInteger("test1");
		Assert.assertEquals(1, result);
	}
	
	@Test
	public void testIsNumeric() {
		Boolean  result = hsxSBStringUtils.isNumeric("123");
		Assert.assertTrue(result);
	}
	
	@Test
	public void testIsDigit() {
		Boolean  result = hsxSBStringUtils.isDigit('1');
		Assert.assertTrue(result);
	}
	
	@Test
	public void testLeftPad() {
		String  result = hsxSBStringUtils.leftPad("test", 2);
		Assert.assertEquals("test", result);
	}
	
	@Test
	public void testTwoDecimalFormatter() {
		String  result = hsxSBStringUtils.twoDecimalFormatter("55");
		Assert.assertEquals("55.00", result);
	}
}
