package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBCheckBoxWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBCheckBoxRendererTest {

	 private HsxSBCheckBoxRenderer hsxSBCheckBoxRenderer;
	 private HsxSBCheckBoxWidget widget;
	 private HsxSBWidgetInfo hsxSBWidgetInfo;
	 private Map<String,String> additionalInfoMap;
	 private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;

		@Before
		public void setUp() throws Exception {
			hsxSBCheckBoxRenderer = new HsxSBCheckBoxRenderer();
			widget = new HsxSBCheckBoxWidget();
			hsxSBWidgetInfo = new HsxSBWidgetInfo();
			hsxSBWidgetInfo.setSavedValue("Yes");
			hsxSBWidgetInfo.setStyle("test");
			hsxSBWidgetInfo.setId("test");
			hsxSBWidgetInfo.setSubType("reverse");
			hsxSBWidgetInfo.setName("test");
			hsxSBWidgetInfo.setErrorIndicator(true);
			additionalInfoMap = new HashMap<String, String>();
			additionalInfoMap.put("AdditionalText1", "Yes");
			additionalInfoMap.put("AdditionalText2", "Yes");
			additionalInfoMap.put("AdditionalText4", "Yes");
			hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
			hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
			hsxSBWidgetInfo.setAdditionalText1("test");
			widget.setWidgetInfo(hsxSBWidgetInfo);
		}

		@After
		public void tearDown() throws Exception {
			additionalInfoMap = null;
			hsxSBCheckBoxRenderer = null;
			hsxSBUIBuilderControllerImpl = null;
			hsxSBWidgetInfo = null;
			widget = null;
		}

		@Test
		public void test() {
			String result = hsxSBCheckBoxRenderer.renderContent(widget);
			Assert.assertNotNull(result);
			Assert.assertEquals("<div class='control'><input type='checkbox' class='test' id='test_checkbox_id' name='test' value='Yes' checked ='checked'/>" +
								"</div><div id='test_additional1_id'class='additional-text'><span>test</span></div>", result);
		}

	}
