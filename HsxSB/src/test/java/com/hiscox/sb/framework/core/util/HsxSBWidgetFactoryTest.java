package com.hiscox.sb.framework.core.util;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

public class HsxSBWidgetFactoryTest implements HsxSBUIBuilderConstants  {
	
	private HsxSBWidgetFactory hsxSBWidgetFactory;
	private HsxSBWidget widget;
	private String widgetType;
	private String id;
	private String  name;

	@Before
	public void setUp() throws Exception {
		hsxSBWidgetFactory = new HsxSBWidgetFactory();
		id = "test";
		name = "test";
	}

	@After
	public void tearDown() throws Exception {
		hsxSBWidgetFactory = null;
		widget = null;
		widgetType = null;
		id = null;
		name = null;
	}

	@Test
	public void testTextWidget() {
		widgetType = TYPE_TEXT;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testFormWidget() {
		widgetType = TYPE_FORM;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testDivWidget() {
		widgetType = TYPE_DIV;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testScreenWidget() {
		widgetType = TYPE_SCREEN;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testQuestionGroupWidget() {
		widgetType = TYPE_QUESTION_GROUP;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testQuestionWidget() {
		widgetType = TYPE_QUESTION;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testLabelWidget() {
		widgetType = TYPE_LABEL;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testAnchorWidget() {
		widgetType = TYPE_ANCHOR;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testOptionWidget() {
		widgetType = TYPE_OPTION;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testRadioWidget() {
		widgetType = TYPE_RADIO;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testDropDownWidget() {
		widgetType = TYPE_DROPDOWN;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testButtonWidget() {
		widgetType = TYPE_ACTION;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testCheckBoxWidget() {
		widgetType = TYPE_CHECKBOX;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testSpanWidget() {
		widgetType = TYPE_SPAN;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testTableSpanWidget() {
		widgetType = TYPE_TABLE_SPAN;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testHiddenWidget() {
		widgetType = TYPE_HIDDEN;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testErrorWidget() {
		widgetType = TYPE_ERROR;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testActionListWidget() {
		widgetType = TYPE_ACTIONGROUP;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testTextAreaWidget() {
		widgetType = TYPE_TEXTAREA;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testImgWidget() {
		widgetType = TYPE_IMAGE;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testPasswordWidget() {
		widgetType = TYPE_PASSWORD;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testDateWidget() {
		widgetType = TYPE_DATE;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testTelephoneWidget() {
		widgetType = TYPE_TELEPHONE;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testAdditionalTextWidget() {
		widgetType = TYPE_ADDITIONALTEXT;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testHelpWidget() {
		widgetType = TYPE_HELP;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testEditWidget() {
		widgetType = TYPE_EDIT;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testTableWidget() {
		widgetType = TYPE_TABLE;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testTRWidget() {
		widgetType = TYPE_TABLEROW;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testTHWidget() {
		widgetType = TYPE_TABLEHEADER;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testTDWidget() {
		widgetType = TYPE_TABLECOLLABELENTRY;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testStaticWidget() {
		widgetType = TYPE_STATIC;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testLimitSummaryWidget() {
		widgetType = TYPE_LIMITSUMMARY;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testSurchargeSummaryWidget() {
		widgetType = TYPE_SURCHARGESUMMARY;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testAddressSummaryWidget() {
		widgetType = TYPE_ADDRESSSUMMARY;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testEndorsementWidget() {
		widgetType = TYPE_FILELINK;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testPopUpButtonWidget() {
		widgetType = TYPE_ACTION_HREF;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}
	@Test
	public void testIframeWidget() {
		widgetType = TYPE_IFRAME;
		widget = hsxSBWidgetFactory.getWidgetInstance(widgetType, id, name);
		Assert.assertNotNull(widget);
	}

}
