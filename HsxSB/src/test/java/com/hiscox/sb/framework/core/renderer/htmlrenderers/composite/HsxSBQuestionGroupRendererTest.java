package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionGroupWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBQuestionGroupRendererTest {


	private HsxSBQuestionGroupRenderer hsxSBQuestionGroupRenderer;
	private HsxSBQuestionGroupWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBWidget hsxSBWidget; 
	private List<HsxSBWidget> widgetsList;
	private Map<String, String> additionalInfoMap;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;

	@Before
	public void setUp() throws Exception {
		hsxSBQuestionGroupRenderer = new HsxSBQuestionGroupRenderer();
		widget = new HsxSBQuestionGroupWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		widgetsList = new ArrayList<HsxSBWidget>();
		widgetsList.add(hsxSBWidget);
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setStyleHint("test");
		hsxSBWidgetInfo.setAdditionalText3("test");
		hsxSBWidgetInfo.setLabel("test");
		additionalInfoMap = new HashMap<String, String>();
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		hsxSBWidgetInfo.setErrorIndicator(true);
		hsxSBWidgetInfo.setErrorText("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBQuestionGroupRenderer = null;
		hsxSBWidget = null;
		hsxSBWidgetInfo = null;
		widget = null;
		widgetsList = null;
		additionalInfoMap = null;
		hsxSBUIBuilderControllerImpl = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBQuestionGroupRenderer.renderContent(widget);
		Assert.assertNotNull(result);
	}

}
