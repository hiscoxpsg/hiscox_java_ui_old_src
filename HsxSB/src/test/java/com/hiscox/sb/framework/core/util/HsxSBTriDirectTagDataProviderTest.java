package com.hiscox.sb.framework.core.util;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HsxSBTriDirectTagDataProviderTest {
	
	private HsxSBTriDirectTagDataProvider hsxSBTriDirectTagDataProvider;
	private Map<String, String> customVariablesMap;

	@Before
	public void setUp() throws Exception {
		hsxSBTriDirectTagDataProvider = new HsxSBTriDirectTagDataProvider();
		customVariablesMap = new HashMap<String, String>();
	}

	@After
	public void tearDown() throws Exception {
		customVariablesMap = null;
		hsxSBTriDirectTagDataProvider = null;
	}

	@Test
	public void testTellUsAboutYourBusinessPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("LetUsCallYou", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testHelpYouChooseBusinessOptionPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("ProductOptions", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testAboutYourRegistrationFirstPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("AboutYou", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testYourBusinessRegistrationSecondPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("YourBusiness", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testYourBusinessRegistrationThirdPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("YourBusiness2", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testApplicationDeclinedRefferdPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("Declined", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testYourQuoteRegistrationFourthPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("YourQuote", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testImportantStatementPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("ImportantInformation", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testApplicationSummaryPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("ApplicationSummary", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testPaymentDetailsPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("PaymentDetails", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testConfirmationPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("Confirmation", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testPayByPhonePage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("PayByPhone", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testSaveQuotePage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("SaveQuote", customVariablesMap);
		Assert.assertNotNull(result);
	}
	
	@Test
	public void testCallBackRequestPage() {
		String result = hsxSBTriDirectTagDataProvider.provideHTMLContent("WCRequestCallback", customVariablesMap);
		Assert.assertNotNull(result);
	}
}
