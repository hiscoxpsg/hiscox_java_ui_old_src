package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBLabelWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBLabelSpanRendererTest {

    private HsxSBLabelSpanRenderer hsxSBLabelSpanRenderer;
    private HsxSBLabelWidget widget;
    private HsxSBWidgetInfo hsxSBWidgetInfo;
    private Map<String,String> additionalInfoMap;
    private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;

	@Before
	public void setUp() throws Exception {
		hsxSBLabelSpanRenderer = new HsxSBLabelSpanRenderer();
		widget = new HsxSBLabelWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setLabelForId("test");
		hsxSBWidgetInfo.setSavedValue("test");
		additionalInfoMap = new HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		hsxSBWidgetInfo.setAdditionalText1("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		hsxSBLabelSpanRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
	}

	@Test
	public void test() {
		String result = hsxSBLabelSpanRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='question-text'><label for='test' id='test_label_Id' >test</label></div><div class='control'><span id='test_span_id'>test</span>" +
							"</div><div id='test_additional1_id'class='additional-text'><span>test</span></div>", result);
	}

}
