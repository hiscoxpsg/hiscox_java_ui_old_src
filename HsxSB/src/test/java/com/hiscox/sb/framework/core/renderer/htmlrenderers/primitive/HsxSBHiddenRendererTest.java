package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.primitive.HsxSBHiddenWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBHiddenRendererTest {

    private HsxSBHiddenRenderer hsxSBHiddenRenderer;
    private HsxSBHiddenWidget widget;
    private HsxSBWidgetInfo hsxSBWidgetInfo;
    
	@Before
	public void setUp() throws Exception {
		hsxSBHiddenRenderer = new HsxSBHiddenRenderer();
		widget = new HsxSBHiddenWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setName("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBHiddenRenderer = null;
		hsxSBWidgetInfo = null;
		widget = null;
	}

	@Test
	public void test() {
		String result = hsxSBHiddenRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<input type='hidden' name='test' id='test' value='' />", result);
	}

}
