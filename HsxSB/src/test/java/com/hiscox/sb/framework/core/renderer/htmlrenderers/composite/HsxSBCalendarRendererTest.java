package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import com.hiscox.sb.framework.core.widgets.composite.HsxSBCalendarWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBCalendarRendererTest {

	private HsxSBCalendarRenderer hsxSBCalendarRenderer;
	private HsxSBCalendarWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	

	@Before
	public void setUp() throws Exception {
		hsxSBCalendarRenderer = new HsxSBCalendarRenderer();
		widget = new HsxSBCalendarWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setSavedValue("test");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setStyle("test");
		hsxSBWidgetInfo.setTarget("test");
		hsxSBWidgetInfo.setTabIndex("1");
		hsxSBWidgetInfo.setHelpText("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBCalendarRenderer = null;
		hsxSBWidgetInfo = null;
		widget = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBCalendarRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='icon calendar'><a href='test' class='test' id='test_help_id' target='test' tabindex='1'></a><a href=#><input type=hidden value='test' id='test'/></a></div>", result);
	}

}