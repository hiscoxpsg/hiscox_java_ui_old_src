package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBButtonWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBButtonRendererTest {
	
    private HsxSBButtonRenderer hsxSBButtonRenderer;
    private HsxSBButtonWidget widget;
    private HsxSBWidgetInfo hsxSBWidgetInfo;
    private Map<String,String> additionalInfoMap;
    private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;

	@Before
	public void setUp() throws Exception {
		hsxSBButtonRenderer = new HsxSBButtonRenderer();
		widget = new HsxSBButtonWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setAction("submit");
		hsxSBWidgetInfo.setStyleHint("test");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setDisplayText("test");
		hsxSBWidgetInfo.setCode("test");
		additionalInfoMap = new HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		hsxSBWidgetInfo.setAdditionalText1("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		hsxSBButtonRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
	}

	@Test
	public void test() {
		String result = hsxSBButtonRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<li class='question-container test' id='test_question_id'><div class='submit-button'>" +
							"<span class='submit-button-end'>" +"<input type='submit' class='submit-input' id='test_button_id' " +
							"name='action_test_button' value='test' title='test' /><" +
							"span class='button-icon'></span></span></div></li>", result);
	}

}
