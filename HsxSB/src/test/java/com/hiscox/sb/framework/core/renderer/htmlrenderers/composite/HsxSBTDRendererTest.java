package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTDWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBTDRendererTest {

	private HsxSBTDRenderer hsxSBTDRenderer;
	private HsxSBTDWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBWidget hsxSBWidget; 
	private List<HsxSBWidget> widgetsList;

	@Before
	public void setUp() throws Exception {
		hsxSBTDRenderer = new HsxSBTDRenderer();
		widget = new HsxSBTDWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		widgetsList = new ArrayList<HsxSBWidget>();
		widgetsList.add(hsxSBWidget);
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBTDRenderer = null;
		hsxSBWidget = null;
		hsxSBWidgetInfo = null;
		widget = null;
		widgetsList = null;
	}

	@Test
	public void test() {
		String result = hsxSBTDRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<td></td>", result);
	}

}
