package com.hiscox.sb.framework.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.util.bean.HsxSBJsHiddenVariablesBean;
import com.hiscox.sb.framework.core.widgets.HsxSBCompositeWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBHelpWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBWidgetUtilTest {
	
	private HsxSBWidgetUtil hsxSBWidgetUtil;
	private HsxSBScreenWidget screenRowWidget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private Map<String, String> uiWidgetDepencymap;
	private Map<String, HsxSBWidget> widgetMap ;
	private HsxSBWidget hsxSBWidget;
	private List<HsxSBWidget> getWidgetsList;
	private HsxSBHelpWidget  controlWidget;
	private HsxSBCompositeWidget formWidget;
	private HsxSBJsHiddenVariablesBean jsHiddenVariablesBean;

	@Before
	public void setUp() throws Exception {
		hsxSBWidgetUtil = new HsxSBWidgetUtil();
		screenRowWidget = new HsxSBScreenWidget();
		hsxSBWidgetInfo= new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setHelpText("test");
		hsxSBWidgetInfo.setHelpTitle("test");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setCode("test");
		hsxSBWidgetInfo.setOrder(1);
		hsxSBWidgetInfo.setAdditionalText6("text6");
		hsxSBWidgetInfo.setAdditionalText1("text1");
		hsxSBWidgetInfo.setAdditionalText2("text2");
		hsxSBWidgetInfo.setAdditionalText4("text4");
		hsxSBWidgetInfo.setAdditionalText5("text5");
		hsxSBWidgetInfo.setEditTitle("test");
		hsxSBWidgetInfo.setType("test");
		hsxSBWidgetInfo.setErrorText("test");
		hsxSBWidgetInfo.setLabel("test");
		hsxSBWidgetInfo.setLabelForId("test");
		hsxSBWidgetInfo.setMandatory("yes");
		hsxSBWidgetInfo.setOptionalText("optionalText");
		hsxSBWidgetInfo.setShowOptionalText("yes");
		controlWidget = new HsxSBHelpWidget();
		jsHiddenVariablesBean = new HsxSBJsHiddenVariablesBean();
		controlWidget.setWidgetInfo(hsxSBWidgetInfo);
		uiWidgetDepencymap = new HashMap<String, String>();
		widgetMap = new HashMap<String, HsxSBWidget>();
		getWidgetsList = new ArrayList<HsxSBWidget>();
		getWidgetsList.add(hsxSBWidget);
		screenRowWidget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		controlWidget = null;
		formWidget = null;
		getWidgetsList = null;
		hsxSBWidget = null;
		hsxSBWidgetInfo = null;
		hsxSBWidgetUtil = null;
		jsHiddenVariablesBean = null;
		screenRowWidget = null;
		uiWidgetDepencymap = null;
		widgetMap = null;
	}

	@Test
	public void testUnHideQuestionWidget() {
		hsxSBWidgetUtil.unHideQuestionWidget(screenRowWidget, "test");
		Assert.assertNotNull(hsxSBWidgetInfo.getStyleHint());
	}

	@Test
	public void testHideQuestionWidget() {
		hsxSBWidgetUtil.hideQuestionWidget(screenRowWidget, "test");
		Assert.assertNotNull(hsxSBWidgetInfo.getStyleHint());
	}
	
	@Test
	 public void testCreateUIWidgetHideUnHideMap(){
		Map result = hsxSBWidgetUtil.createUIWidgetHideUnHideMap(screenRowWidget, uiWidgetDepencymap, widgetMap);
		Assert.assertNotNull(result);
	}
	@Test
	 public void testAddHelpRenderer(){
		String result =  hsxSBWidgetUtil.addHelpRenderer(controlWidget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div id='test_help_id' class='icon help'><a target='_blank'  href='help-text?pageName=&&questionCode=test' " +
							"class='showTip L3 HelpHref' id='test_anchor_id' ><span>text6</span></a><span id='test_value_id' " +
							"class='hide-question' >test</span></div>", result);
	 }
	  
	 @Test
	  public void testUpdateTooltipHiddenString(){
		 hsxSBWidgetUtil.updateTooltipHiddenString("testId", "testHelp");
		 Assert.assertNotNull(hsxSBWidgetUtil);
	 }
	 
	 @Test
	  public void testAddAdditionalTextOneRenderer(){
		 String result = hsxSBWidgetUtil.addAdditionalTextOneRenderer(controlWidget);
		 Assert.assertNotNull(result);
		 Assert.assertEquals("<div id='test_additional1_id'class='additional-text'><span>text1</span></div>", result);
	 }
	 
	 @Test
	  public void testAddAdditionalTextTwoRenderer(){
		 String result = hsxSBWidgetUtil.addAdditionalTextTwoRenderer(controlWidget);
		 Assert.assertNotNull(result);
		 Assert.assertEquals("<div id='test_additional2_id'class='additional-text-2'><span>text2</span></div>", result);
	 }
	 
	 @Test
	  public void testAddAdditionalText4Renderer(){
		 String result = hsxSBWidgetUtil.addAdditionalText4Renderer(controlWidget);
		 Assert.assertNotNull(result);
		 Assert.assertEquals("<div class='icon edit'><a title='Edit' href=text4>text5<input type='hidden' " +
		 					 "value='text4' id='test_edit_id'/></a></div>", result);
	 }
	 
	 @Test
	  public void testAddErrorRenderer(){
		 String result = hsxSBWidgetUtil.addErrorRenderer(controlWidget);
		 Assert.assertNotNull(result);
		 Assert.assertEquals("<div class='error-text'><span>test</span></div>", result);
	 }
	 
	 @Test
	  public void testAddLabelRenderer(){
		 String result = hsxSBWidgetUtil.addLabelRenderer(controlWidget);
		 Assert.assertNotNull(result);
		 Assert.assertEquals("<div class='question-text'><label for='test' id='labeltest_label_Id' >test</label></div>", result);
	 }
	 
	 @Test
	 public void testCreateJsHiddenVariables(){
		 formWidget = new HsxSBCompositeWidget() {
				
				@Override
				public void createWidget(HsxSBWidgetInfo widgetInfo) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void removeWidget(HsxSBWidget widget) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void createWidget(Element element) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void addWidget(HsxSBWidget widget) {
					// TODO Auto-generated method stub
					
				}
			};
		 StringBuilder st = new StringBuilder("testutil");
		 jsHiddenVariablesBean.setJsActionNames(st);
		 jsHiddenVariablesBean.setTextPercentString(st);
		 jsHiddenVariablesBean.setHideUnHideJsString(st);
		 jsHiddenVariablesBean.setJsAutoTabLogicString(st);
		 jsHiddenVariablesBean.setJsFilterByGroupingString(st);
		 jsHiddenVariablesBean.setTextBoxHideUnHideJsString(st);
		 jsHiddenVariablesBean.setJsButtonEnableDisableString(st);
		 jsHiddenVariablesBean.setJsQuestionEnableDisableString(st);
		 jsHiddenVariablesBean.setJsIsOneOfAndLogicString(st);
		 jsHiddenVariablesBean.setJsDropdownAndLogicString(st);
		 jsHiddenVariablesBean.setJsIsOneOfAndValidationString(st);
		 jsHiddenVariablesBean.setJsAndCaseLogicString(st);
		 jsHiddenVariablesBean.setDependencyValidationString(st);
		 jsHiddenVariablesBean.setJsPageSubmitLogicString(st);
		 hsxSBWidgetUtil.createJsHiddenVariables(formWidget, widgetMap, jsHiddenVariablesBean);
		 Assert.assertNotNull(formWidget);
	 }
	 
	 
}
