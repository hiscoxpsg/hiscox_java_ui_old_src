package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.primitive.HsxSBAnchorWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBAnchorRendererTest {

	private HsxSBAnchorRenderer hsxSBAnchorRenderer ;
	private HsxSBAnchorWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBAnchorRenderer = new HsxSBAnchorRenderer();
		widget = new HsxSBAnchorWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setSavedValue("test");
		hsxSBWidgetInfo.setLabel("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBAnchorRenderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBAnchorRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='icon help'><a href='test' title='help'><span>test</span></a></div>", result);
	}

}
