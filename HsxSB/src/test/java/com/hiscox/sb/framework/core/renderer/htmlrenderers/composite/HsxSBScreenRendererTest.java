package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBScreenRendererTest {

	private HsxSBScreenRenderer hsxSBScreenRenderer;
	private HsxSBScreenWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBWidget hsxSBWidget; 
	private List<HsxSBWidget> widgetsList;

	@Before
	public void setUp() throws Exception {
		hsxSBScreenRenderer = new HsxSBScreenRenderer();
		widget = new HsxSBScreenWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		widgetsList = new ArrayList<HsxSBWidget>();
		widgetsList.add(hsxSBWidget);
		hsxSBWidgetInfo.setStyleHint("test");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setErrorIndicator(true);
		hsxSBWidgetInfo.setErrorStyle("test");
		hsxSBWidgetInfo.setPageErrorText("test");
		hsxSBWidgetInfo.setPageIntroText("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBScreenRenderer = null;
		hsxSBWidget = null;
		hsxSBWidgetInfo = null;
		widget = null;
		widgetsList = null;
	}

	@Test
	public void test() {
		String result = hsxSBScreenRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='screen-container test' id='test_screen_id'><div class='test' id='test_screen_error_id'>" +
				"<div class='error-text'><span>test</span></div></div><div class='opening-message'><span>test</span></div></div>", result);
	}

}
