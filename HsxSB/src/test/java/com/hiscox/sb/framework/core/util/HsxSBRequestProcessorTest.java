package com.hiscox.sb.framework.core.util;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBTextWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBRequestProcessorTest {
	
	private HsxSBRequestProcessor hsxSBRequestProcessor;
	private HsxSBWidgetInfo screenWidgetInfo;
	private Map<String, HsxSBWidget> widgetMap;
	private HsxSBWidget widget;
	private Document document;
	private Map<String, HsxSBTextManagerVO> textMap;
	

	@Before
	public void setUp() throws Exception {
		hsxSBRequestProcessor = new HsxSBRequestProcessor();
		screenWidgetInfo = new HsxSBWidgetInfo();
		widgetMap = new HashMap<String, HsxSBWidget>();
		textMap = new HashMap<String, HsxSBTextManagerVO>();
		screenWidgetInfo.setType("text");
		
	}

	@After
	public void tearDown() throws Exception {
		document = null;
		hsxSBRequestProcessor = null;
		screenWidgetInfo = null;
		textMap = null;
		widget = null;
		widgetMap = null;
	}

	@Test
	public void testBuildHTMLContent() {
		HsxSBTextWidget hsxSBTextWidget = new HsxSBTextWidget();
		hsxSBTextWidget.setWidgetInfo(screenWidgetInfo);
		hsxSBRequestProcessor.buildHTMLContent(hsxSBTextWidget, 1);
		Assert.assertNotNull(hsxSBRequestProcessor);
	}
	
	@Test
	public void testProcessScreenQuestionXMLWithWidgetMap(){
		String result = hsxSBRequestProcessor.processScreenQuestionXML(widgetMap, widget, document);
		Assert.assertEquals("", result);
		
	}
	
	@Test
	public void testProcessScreenQuestionXMLWithTextMap(){
		String result = hsxSBRequestProcessor.processScreenQuestionXML(textMap, widgetMap, widget, document);
		Assert.assertEquals(null, result);
		
	}

}
