package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBSpanWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBSpanRendererTest {
	
	private HsxSBSpanRenderer hsxSBSpanRenderer;
	private HsxSBSpanWidget spanWidget;
	private HsxSBWidgetInfo widgetInfo;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
	private Map<String,String> additionalInfoMap;

	@Before
	public void setUp() throws Exception {
		hsxSBSpanRenderer = new HsxSBSpanRenderer();
		spanWidget = new HsxSBSpanWidget();
		widgetInfo = new HsxSBWidgetInfo();
		widgetInfo.setId("test");
		widgetInfo.setSavedValue("01/02/2015");
		widgetInfo.setSubType("currency");
		widgetInfo.setCurrency("currency");
		widgetInfo.setTMLookup("Yes");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		additionalInfoMap = new  HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		spanWidget.setWidgetInfo(widgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		spanWidget = null;
		hsxSBSpanRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		widgetInfo = null;
	}

	@Test
	public void test() {
		String result = hsxSBSpanRenderer.renderContent(spanWidget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='control'><span id='test_span_id' >01/02/2015</span></div>", result);
	}

}
