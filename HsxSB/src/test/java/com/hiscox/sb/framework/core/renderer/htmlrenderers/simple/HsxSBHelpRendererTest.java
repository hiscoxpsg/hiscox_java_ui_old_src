package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.composite.HsxSBHelpWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBHelpRendererTest {

	private HsxSBHelpRenderer hsxSBHelpRenderer;
	private HsxSBHelpWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBHelpRenderer = new HsxSBHelpRenderer();
		widget = new HsxSBHelpWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setCode("test");
		hsxSBWidgetInfo.setAdditionalText6("test");
		hsxSBWidgetInfo.setHelpText("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBHelpRenderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBHelpRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div id='test_help_id' class='icon help'><a target='_blank'  href='help-text?pageName=&&questionCode=test' class='showTip L3 HelpHref' id='test_anchor_id' ><span>test</span></a><span id='test_value_id' class='hide-question' >test</span></div>", result);
	}

}
