package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBDivWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBDivRendererTest {

	private HsxSBDivRenderer hsxSBDivRenderer;
	private HsxSBDivWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBWidget hsxSBWidget; 
	private List<HsxSBWidget> widgetsList;

	@Before
	public void setUp() throws Exception {
		hsxSBDivRenderer = new HsxSBDivRenderer();
		widget = new HsxSBDivWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		widgetsList = new ArrayList<HsxSBWidget>();
		widgetsList.add(hsxSBWidget);
		hsxSBWidgetInfo.setStyle("test");
		hsxSBWidgetInfo.setId("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBDivRenderer = null;
		hsxSBWidget = null;
		hsxSBWidgetInfo = null;
		widget = null;
		widgetsList = null;
	}

	@Test
	public void test() {
		String result = hsxSBDivRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='test' id='test_question_id'></div>", result);
	}

}