package com.hiscox.sb.framework.core.util;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBRenderer;

public class HsxSBRendererFactoryTest {
	
	private HsxSBRendererFactory hsxSBRendererFactory;
	private String rendererType;
	private int uiContentType;

	@Before
	public void setUp() throws Exception {
		hsxSBRendererFactory = new HsxSBRendererFactory() {
			
			@Override
			public HsxSBRenderer getRendererInstance(String rendererType) {
				return null;
			}
		};
		
	}

	@After
	public void tearDown() throws Exception {
		hsxSBRendererFactory = null;
		rendererType = null;
		uiContentType = 0;
	}

	@Test
	public void testHtmlContentType() {
		rendererType="text";
		uiContentType=1;
		HsxSBRenderer renderer = hsxSBRendererFactory.getRendererInstance(rendererType, uiContentType);
		Assert.assertNotNull(renderer);
	}
	
	@Test
	public void testJsfContentType() {
		rendererType="dropdown";
		uiContentType=2;
		HsxSBRenderer renderer = hsxSBRendererFactory.getRendererInstance(rendererType, uiContentType);
		Assert.assertNotNull(renderer);
	}

}
