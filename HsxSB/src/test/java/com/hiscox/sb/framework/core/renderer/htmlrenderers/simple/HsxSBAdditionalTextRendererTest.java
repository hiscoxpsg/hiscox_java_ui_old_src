package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import junit.framework.Assert;

import org.jboss.system.server.profileservice.hotdeploy.HDScanner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.simple.HsxSBAdditionalTextWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBAdditionalTextRendererTest {

	private HsxSBAdditionalTextRenderer hsxSBAdditionalTextRenderer;
	private HsxSBAdditionalTextWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBAdditionalTextRenderer = new HsxSBAdditionalTextRenderer();
		widget = new HsxSBAdditionalTextWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setStyle("test");
		hsxSBWidgetInfo.setAdditionalText1("test");
		hsxSBWidgetInfo.setAdditionalText2("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBAdditionalTextRenderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBAdditionalTextRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div id='test'class='test'><span>testtest</span></div>", result);
	}

}
