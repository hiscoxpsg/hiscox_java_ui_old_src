package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBTextWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBTextBoxRendererTest {
	
	private HsxSBTextBoxRenderer hsxSBTextBoxRenderer;
	private HsxSBTextWidget textWidget;
	private HsxSBWidgetInfo widgetInfo;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
	private Map<String,String> additionalInfoMap;

	@Before
	public void setUp() throws Exception {
		hsxSBTextBoxRenderer = new HsxSBTextBoxRenderer();
		textWidget = new HsxSBTextWidget();
		widgetInfo = new HsxSBWidgetInfo();
		widgetInfo.setId("test");
		widgetInfo.setName("test");
		widgetInfo.setSubType("currency");
		widgetInfo.setCurrency("currency");
		widgetInfo.setSavedValue("123");
		widgetInfo.setMaxLength("2");
		widgetInfo.setStyle("test");
		widgetInfo.setDecimalPlaces("55.2");
		widgetInfo.setDisplayOnly("Yes");
		widgetInfo.setErrorIndicator(true);
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		additionalInfoMap = new  HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		textWidget.setWidgetInfo(widgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		textWidget = null;
		hsxSBTextBoxRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		widgetInfo = null;
	}

	@Test
	public void test() {
		String result = hsxSBTextBoxRenderer.renderContent(textWidget);
		Assert.assertNotNull(result);
	}

}
