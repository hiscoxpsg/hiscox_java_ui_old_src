package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBLimitSummaryWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBLimitSummaryRendererTest {

	private HsxSBLimitSummaryRenderer hsxSBLimitSummaryRenderer;
	private HsxSBLimitSummaryWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
	private Map<String,String> additionalInfoMap;

	@Before
	public void setUp() throws Exception {
		hsxSBLimitSummaryRenderer = new HsxSBLimitSummaryRenderer();
		widget= new HsxSBLimitSummaryWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setDisplayOnly("yes");
		hsxSBWidgetInfo.setLabel("test");
		hsxSBWidgetInfo.setSavedValue("test");
		hsxSBWidgetInfo.setId("test");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		additionalInfoMap = new  HashMap<String, String>();
		additionalInfoMap.put("AdditionalText4", "Yes");
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}
	

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		hsxSBLimitSummaryRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
	}

	@Test
	public void test() {
		String result = hsxSBLimitSummaryRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div disabled='disabled' class='question-text'><label>" +
							"<span class='optional'>test</span></label></div><div class='control'><span>test</span></div>", result);
	}

}
