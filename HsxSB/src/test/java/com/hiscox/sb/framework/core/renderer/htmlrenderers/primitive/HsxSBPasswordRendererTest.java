package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBPasswordWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBPasswordRendererTest {

    private HsxSBPasswordRenderer hsxSBPasswordRenderer;
    private HsxSBPasswordWidget widget;
    private HsxSBWidgetInfo hsxSBWidgetInfo;
    private Map<String,String> additionalInfoMap;
    private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;

	@Before
	public void setUp() throws Exception {
		hsxSBPasswordRenderer = new HsxSBPasswordRenderer();
		widget = new HsxSBPasswordWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setName("test");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setSavedValue("test");
		hsxSBWidgetInfo.setDefaultValue("test");
		hsxSBWidgetInfo.setDisplayOnly("yes");
		hsxSBWidgetInfo.setStyle("test");
		hsxSBWidgetInfo.setErrorIndicator(true);
		additionalInfoMap = new HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		hsxSBWidgetInfo.setAdditionalText1("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		hsxSBPasswordRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
	}

	@Test
	public void test() {
		String result = hsxSBPasswordRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='control'><input type='password' name='test' id='test' value='test' disabled='disabled' class='test'/>" +
				     		"</div><div id='test_additional1_id'class='additional-text'><span>test</span></div>", result);
	}

}
