package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBTelephoneWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBTelephoneRendererTest {

	private HsxSBTelephoneRenderer hsxSBTelephoneRenderer;
	private HsxSBTelephoneWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
	private Map<String,String> additionalInfoMap;

	@Before
	public void setUp() throws Exception {
		hsxSBTelephoneRenderer = new HsxSBTelephoneRenderer();
		widget= new HsxSBTelephoneWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setSavedValue("1234567890");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setDisplayOnly("yes");
		hsxSBWidgetInfo.setStyle("test");
		hsxSBWidgetInfo.setErrorIndicator(true);
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		additionalInfoMap = new  HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}
	

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		hsxSBTelephoneRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
	}

	@Test
	public void test() {
		String result = hsxSBTelephoneRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='control'><input type='text' name='test1' id='test1_text_id' value='123' maxlength='3' disabled='disabled' class='test'/><span>-</span>" +
				"<input type='text' name='test2' id='test2_text_id' value='456' maxlength='3' disabled='disabled' class='test'/>" +
				"<span>-</span><input type='text' name='test3' id='test3_text_id' value='7890' maxlength='4' disabled='disabled' class='test'/>" +
				"<input type='hidden' name='test' id='test_hidden' value='1234567890' /></div>", result);
	}

}