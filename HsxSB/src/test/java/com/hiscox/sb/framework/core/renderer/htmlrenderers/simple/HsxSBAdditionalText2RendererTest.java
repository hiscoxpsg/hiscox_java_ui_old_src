package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.simple.HsxSBAdditionalText2Widget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBAdditionalText2RendererTest {
	
	private HsxSBAdditionalText2Renderer hsxSBAdditionalText2Renderer;
	private HsxSBAdditionalText2Widget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBAdditionalText2Renderer = new HsxSBAdditionalText2Renderer();
		widget = new HsxSBAdditionalText2Widget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setStyle("test");
		hsxSBWidgetInfo.setAdditionalText2("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBAdditionalText2Renderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBAdditionalText2Renderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='test'><span>test</span></div>", result);
	}

}
