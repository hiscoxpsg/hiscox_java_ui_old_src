package com.hiscox.sb.framework.validator;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HsxSBTextWidgetValidatorTest {
	
	private HsxSBTextWidgetValidator hsxSBTextWidgetValidator;

	@Before
	public void setUp() throws Exception {
		hsxSBTextWidgetValidator= new HsxSBTextWidgetValidator();
	}

	@After
	public void tearDown() throws Exception {
		hsxSBTextWidgetValidator = null;
	}

	@Test
	public void test() {
		Boolean result = hsxSBTextWidgetValidator.validateWidget(null, null);
		Assert.assertFalse(result);
		
	}

}
