package com.hiscox.sb.framework.core.util;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

public class HsxSBHTMLRendererFactoryTest implements HsxSBUIBuilderConstants {
	
	private HsxSBHTMLRendererFactory hsxSBHTMLRendererFactory;
	private String rendererType ;
	private HsxSBRenderer renderer;

	@Before
	public void setUp() throws Exception {
		hsxSBHTMLRendererFactory = new HsxSBHTMLRendererFactory();
	}

	@After
	public void tearDown() throws Exception {
		hsxSBHTMLRendererFactory = null;
		rendererType = null;
		renderer = null;
	}

	@Test
	  public void testTextBoxRenderer() {
				rendererType = TYPE_TEXT;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
			}
	@Test
		public void testDropDownRenderer() {
					rendererType = TYPE_DROPDOWN;
					renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
					Assert.assertNotNull(renderer);
			}
	@Test
	 public void testAnchorRenderer() {
					rendererType = TYPE_ANCHOR;
					renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
					Assert.assertNotNull(renderer);
			}
	@Test
	  public void testCheckBoxRenderer() {
					rendererType = TYPE_CHECKBOX;
					renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
					Assert.assertNotNull(renderer);
			}	
	@Test
		public void testHiddenRenderer() {
					rendererType = TYPE_HIDDEN;
					renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
					Assert.assertNotNull(renderer);
			}	
	@Test
		public void testLabelRenderer() {
					rendererType = TYPE_LABEL;
					renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
					Assert.assertNotNull(renderer);
			}
	@Test
	public void testOptionRenderer() {
				rendererType = TYPE_OPTION;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testRadioRenderer() {
				rendererType = TYPE_RADIO;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testButtonRenderer() {
				rendererType = TYPE_ACTION;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testSpanRenderer() {
				rendererType = TYPE_SPAN;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testLabelSpanRenderer() {
				rendererType = TYPE_LABEL_SPAN;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testTableSpanRenderer() {
				rendererType = TYPE_TABLE_SPAN;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	
	@Test
	public void testFormRenderer() {
				rendererType = TYPE_FORM;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testDivRenderer() {
				rendererType = TYPE_DIV;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testQuestionRenderer() {
				rendererType = TYPE_QUESTION;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testScreenRenderer() {
				rendererType = TYPE_SCREEN;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testQuestionGroupRenderer() {
				rendererType = TYPE_QUESTION_GROUP;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testTextAreaRenderer() {
				rendererType = TYPE_TEXTAREA;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testActionListRenderer() {
				rendererType = TYPE_ACTIONLIST;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testTelephoneRenderer() {
				rendererType = TYPE_TELEPHONE;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testHelpRenderer() {
				rendererType = TYPE_HELP;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testStaticRenderer() {
				rendererType = TYPE_STATIC;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testErrorRenderer() {
				rendererType = TYPE_ERROR;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testTableRenderer() {
				rendererType = TYPE_TABLE;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testTRRenderer() {
				rendererType = TYPE_TABLEROW;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testTHRenderer() {
				rendererType = TYPE_TABLEHEADER;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testTDRenderer() {
				rendererType = TYPE_TABLECOLLABELENTRY;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	
	@Test
	public void testDateRenderer() {
				rendererType = TYPE_DATE;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	
	@Test
	public void testPasswordRenderer() {
				rendererType = TYPE_PASSWORD;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testAdditionalTextRenderer() {
				rendererType = TYPE_ADDITIONALTEXT;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testEndorsementRenderer() {
				rendererType = TYPE_FILELINK;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testLimitSummaryRenderer() {
				rendererType = TYPE_LIMITSUMMARY;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testSurchargeSummaryRenderer() {
				rendererType = TYPE_SURCHARGESUMMARY;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testAddressSummaryRenderer() {
				rendererType = TYPE_ADDRESSSUMMARY;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testPopUpButtonRenderer() {
				rendererType = TYPE_ACTION_HREF;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testDefaultNavigationButtonRenderer() {
				rendererType = TYPE_DEFAULT_NAVIGATOR;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	@Test
	public void testIframeRenderer() {
				rendererType = TYPE_IFRAME;
				renderer = hsxSBHTMLRendererFactory.getRendererInstance(rendererType);
				Assert.assertNotNull(renderer);
		}
	
	
}
