package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.simple.HsxSBAdditionalText4Widget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBAdditionalText4RendererTest {

	private HsxSBAdditionalText4Renderer hsxSBAdditionalText4Renderer;
	private HsxSBAdditionalText4Widget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBAdditionalText4Renderer = new HsxSBAdditionalText4Renderer();
		widget = new HsxSBAdditionalText4Widget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setStyle("test");
		hsxSBWidgetInfo.setSavedValue("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBAdditionalText4Renderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBAdditionalText4Renderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='control'><span id='test_span_id' class='test'>test</span></div>", result);
	}

}