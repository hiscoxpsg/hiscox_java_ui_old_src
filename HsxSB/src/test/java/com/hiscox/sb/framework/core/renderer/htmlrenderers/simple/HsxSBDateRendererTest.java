package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBDateWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBDateRendererTest {
	
	private HsxSBDateRenderer hsxSBDateRenderer;
	private HsxSBDateWidget dateWidget;
	private HsxSBWidgetInfo widgetInfo;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
	private Map<String,String> additionalInfoMap;
	

	@Before
	public void setUp() throws Exception {
		hsxSBDateRenderer = new HsxSBDateRenderer();
		dateWidget = new HsxSBDateWidget();
		widgetInfo = new HsxSBWidgetInfo();
		widgetInfo.setId("test");
		widgetInfo.setDateFormat("01/01/2015");
		widgetInfo.setSubType("MonthYear");
		widgetInfo.setYearRangeStart("20");
		widgetInfo.setYearRangeEnd("50");
		widgetInfo.setAllowPastDate("Yes");
		widgetInfo.setAllowFutureDate("Yes");
		widgetInfo.setSavedValue("01/01/2015day");
		widgetInfo.setDisplayOnly("Yes");
		widgetInfo.setOrder(1);
		widgetInfo.setErrorIndicator(true);
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		additionalInfoMap = new  HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		dateWidget.setWidgetInfo(widgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		dateWidget = null;
		hsxSBDateRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		widgetInfo = null;
	}

	@Test
	public void testRenderContent() {
		String result = hsxSBDateRenderer.renderContent(dateWidget);
		Assert.assertNotNull(result);
	}

}
