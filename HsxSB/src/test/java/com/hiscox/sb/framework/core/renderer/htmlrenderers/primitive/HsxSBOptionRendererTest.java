package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.primitive.HsxSBOptionWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBOptionRendererTest {

	private HsxSBOptionRenderer hsxSBOptionRenderer;
	private HsxSBOptionWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBOptionRenderer = new HsxSBOptionRenderer();
		widget = new HsxSBOptionWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setSavedValue("test");
		hsxSBWidgetInfo.setDefaultValue("test");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setDisplayText("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBOptionRenderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBOptionRenderer.renderContent(widget);
		Assert.assertNotNull(result);
	}

}
