package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBDropDownWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBPermitedValues;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBDropDownRendererTest {
	
	private HsxSBDropDownRenderer hsxSBDropDownRenderer;
	private HsxSBDropDownWidget dropDownWidget;
	private HsxSBPermitedValues hsxSBPermitedValues;
	private List<HsxSBPermitedValues> permitedValues;
	private HsxSBWidgetInfo widgetInfo;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
	private Map<String,String> additionalInfoMap;

	@Before
	public void setUp() throws Exception {
		hsxSBDropDownRenderer = new HsxSBDropDownRenderer();
		dropDownWidget = new HsxSBDropDownWidget();
		hsxSBPermitedValues = new HsxSBPermitedValues();
		hsxSBPermitedValues.setOrder(1);
		hsxSBPermitedValues.setValue("test");
		hsxSBPermitedValues.setDisplayText("test");
		permitedValues = new ArrayList<HsxSBPermitedValues>();
		permitedValues.add(hsxSBPermitedValues);
		widgetInfo = new HsxSBWidgetInfo();
		widgetInfo.setCurrency("currency");
		widgetInfo.setSubType("currency");
		widgetInfo.setStyle("test");
		widgetInfo.setName("test");
		widgetInfo.setId("test");
		widgetInfo.setDisplayOnly("Yes");
		widgetInfo.setDefaultValue("Blank");
		widgetInfo.setSavedValue("test");
		widgetInfo.setPermitedVlaues(permitedValues);
		widgetInfo.setErrorIndicator(true);
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		additionalInfoMap = new  HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		dropDownWidget.setWidgetInfo(widgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		dropDownWidget = null;
		hsxSBDropDownRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		widgetInfo = null;
		hsxSBPermitedValues = null;
		permitedValues = null;
	}

	@Test
	public void test() {
		String result = hsxSBDropDownRenderer.renderContent(dropDownWidget);
		Assert.assertNotNull(result);
	}

}
