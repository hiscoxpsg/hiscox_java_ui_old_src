package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTHWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBTHRendererTest {

	private HsxSBTHRenderer hsxSBTHRenderer;
	private HsxSBTHWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBWidget hsxSBWidget; 
	private List<HsxSBWidget> widgetsList;

	@Before
	public void setUp() throws Exception {
		hsxSBTHRenderer = new HsxSBTHRenderer();
		widget = new HsxSBTHWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		widgetsList = new ArrayList<HsxSBWidget>();
		widgetsList.add(hsxSBWidget);
		hsxSBWidgetInfo.setStyleHint("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBTHRenderer = null;
		hsxSBWidget = null;
		hsxSBWidgetInfo = null;
		widget = null;
		widgetsList = null;
	}

	@Test
	public void test() {
		String result = hsxSBTHRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<th class='test'></th>", result);
	}

}
