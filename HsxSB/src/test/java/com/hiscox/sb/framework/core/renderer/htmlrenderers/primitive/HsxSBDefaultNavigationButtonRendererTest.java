package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import com.hiscox.sb.framework.core.widgets.primitive.HsxSBButtonWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBDefaultNavigationButtonRendererTest {

	 private HsxSBDefaultNavigationButtonRenderer hsxSBDefaultNavigationButtonRenderer;
	 private HsxSBButtonWidget widget;
	 private HsxSBWidgetInfo hsxSBWidgetInfo;
	

		@Before
		public void setUp() throws Exception {
			hsxSBDefaultNavigationButtonRenderer = new HsxSBDefaultNavigationButtonRenderer();
			widget = new HsxSBButtonWidget();
			hsxSBWidgetInfo = new HsxSBWidgetInfo();
			hsxSBWidgetInfo.setCode("test");
			widget.setWidgetInfo(hsxSBWidgetInfo);
		}

		@After
		public void tearDown() throws Exception {
			hsxSBDefaultNavigationButtonRenderer = null;
			hsxSBWidgetInfo = null;
			widget = null;
		}

		@Test
		public void test() {
			String result = hsxSBDefaultNavigationButtonRenderer.renderContent(widget);
			Assert.assertNotNull(result);
			Assert.assertEquals("<input type='submit' class='hide-question default-navigation-button' name='action_test_button' value='' />", result);
		}

	}
