package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBEndorsementWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBEndorsementRendererTest {

	private HsxSBEndorsementRenderer hsxSBEndorsementRenderer;
	private HsxSBEndorsementWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
	private Map<String,String> additionalInfoMap;
	private HashMap<String, String> endorsementMap;

	@Before
	public void setUp() throws Exception {
		hsxSBEndorsementRenderer = new HsxSBEndorsementRenderer();
		widget= new HsxSBEndorsementWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setDisplayOnly("yes");
		hsxSBWidgetInfo.setAdditionalText4("test");
		hsxSBWidgetInfo.setFileType("text");
		hsxSBWidgetInfo.setUseFilename("test");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		additionalInfoMap = new  HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		endorsementMap = new HashMap<String, String>();
		endorsementMap.put("test", "test");
		hsxSBWidgetInfo.setEndorsementMap(endorsementMap);
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}
	

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		hsxSBEndorsementRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
		endorsementMap = null;
	}

	@Test
	public void test() {
		String result = hsxSBEndorsementRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div disabled='disabled' class='question-text'><a href='test' target=_blank class='text' title=test><" +
							"span>test</span></a><span><ol><li>test</li></ol></span></div>", result);
	}

}
