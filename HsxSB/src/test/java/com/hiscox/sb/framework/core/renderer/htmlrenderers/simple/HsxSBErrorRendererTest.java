package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.simple.HsxSBErrorWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBErrorRendererTest {
	
	private HsxSBErrorRenderer hsxSBErrorRenderer;
	private HsxSBErrorWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBErrorRenderer = new HsxSBErrorRenderer();
		widget = new HsxSBErrorWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setErrorText("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBErrorRenderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBErrorRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='error-text'><span>test</span></div>", result);
	}

}
