package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBQuestionRendererTest {


	private HsxSBQuestionRenderer hsxSBQuestionRenderer;
	private HsxSBQuestionWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private HsxSBWidget hsxSBWidget; 
	private List<HsxSBWidget> widgetsList;

	@Before
	public void setUp() throws Exception {
		hsxSBQuestionRenderer = new HsxSBQuestionRenderer();
		widget = new HsxSBQuestionWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		widgetsList = new ArrayList<HsxSBWidget>();
		widgetsList.add(hsxSBWidget);
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setStyleHint("test");
		hsxSBWidgetInfo.setAdditionalText3("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBQuestionRenderer = null;
		hsxSBWidget = null;
		hsxSBWidgetInfo = null;
		widget = null;
		widgetsList = null;
	}

	@Test
	public void test() {
		String result = hsxSBQuestionRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<li class='test' id='test'><noscript><div class='non-js'>test</div></noscript></li>", result);
	}

}
