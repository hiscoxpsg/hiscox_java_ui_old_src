package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBAddressSummaryWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBAddressSummaryRendererTest {
	
	private HsxSBAddressSummaryRenderer hsxSBAddressSummaryRenderer;
	private HsxSBAddressSummaryWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;
	private List<String> addressLine;
	private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
	private Map<String,String> additionalInfoMap;

	@Before
	public void setUp() throws Exception {
		hsxSBAddressSummaryRenderer = new HsxSBAddressSummaryRenderer();
		widget= new HsxSBAddressSummaryWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setDisplayOnly("yes");
		hsxSBWidgetInfo.setAssemblyMethod("line-break");
		addressLine = new ArrayList<String>();
		addressLine.add("Test");
		hsxSBWidgetInfo.setAddressLine(addressLine);
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		additionalInfoMap = new  HashMap<String, String>();
		additionalInfoMap.put("AdditionalText4","Yes");
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}
	

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		addressLine = null;
		hsxSBAddressSummaryRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
	}

	@Test
	public void test() {
		String result = hsxSBAddressSummaryRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div disabled='disabled' class='control'><span>Test</span></div>", result);
	}

}
