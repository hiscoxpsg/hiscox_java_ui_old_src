package com.hiscox.sb.framework.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;

public class HsxSBUIValidatorTest {
	
	private HsxSBUIValidator hsxSBUIValidator;
	private HsxSBScreenWidget pageScreenWidget;
	private HashMap<String, HsxSBWidget> dynamicQuestionMap;
	private Map<String, String> requestValueMap;
	private Properties validationHintProperties;
	private Map<String, String> sessionMap;
	private String requestedAction;
	private String groupValidationFlag;
	private HsxSBWidgetInfo widgetInfo;
	private List<HsxSBWidget> widgetsList;
	
	@Before
	public void setUp() throws Exception {
		hsxSBUIValidator = new HsxSBUIValidator();
		pageScreenWidget = new HsxSBScreenWidget();
		dynamicQuestionMap = new HashMap<String, HsxSBWidget>();
		requestValueMap= new HashMap<String, String>();
		validationHintProperties = new Properties();
		sessionMap = new HashMap<String, String>();
		requestedAction = "test";
		groupValidationFlag = "Yes";
		pageScreenWidget.setName("test");
		widgetInfo = new HsxSBWidgetInfo();
		widgetInfo.setType("question_group");
		widgetInfo.setGroupLimit("22");
		widgetInfo.setCompareQuestionCode("2");
		widgetInfo.setCompareOperator("test");
		widgetInfo.setGroupValidation("Yes");
		widgetInfo.setSavedValue("test");
		widgetInfo.setSubType("checkbox");
		widgetInfo.setIsMandatory("Yes");
		widgetInfo.setId("test");
		pageScreenWidget.setWidgetInfo(widgetInfo);
		widgetsList = new ArrayList<HsxSBWidget>();
		widgetsList.add(pageScreenWidget);
		widgetsList.add(pageScreenWidget);
		pageScreenWidget.setWidgetInfo(widgetInfo);
		pageScreenWidget.setWidgetsList(widgetsList);
		dynamicQuestionMap.put("question_group", pageScreenWidget);
		dynamicQuestionMap.put("test", pageScreenWidget);
	}

	@After
	public void tearDown() throws Exception {
		dynamicQuestionMap = null;
		groupValidationFlag  = null;
		hsxSBUIValidator = null; 
		pageScreenWidget = null;
		requestedAction = null;
		requestValueMap = null; 
		sessionMap = null;
		validationHintProperties = null; 
		widgetInfo = null;
		widgetsList = null;
	}

	@Test
	public void testValidateScreen() throws HsxSBUIBuilderRuntimeException, Exception {
		Boolean result = hsxSBUIValidator.validateScreen(pageScreenWidget, dynamicQuestionMap, requestValueMap, 
				validationHintProperties, sessionMap, requestedAction, groupValidationFlag);
		Assert.assertFalse(result);
	}
	
	@Test
	public void testPasswordContainsSubStringOfFirstName(){
		Boolean result = hsxSBUIValidator.passwordContainsSubStringOfUserName("testPassword", "test", "none");
		Assert.assertTrue(result);
	}

	
	@Test
	public void testPasswordContainsSubStringOfLastName(){
		Boolean result = hsxSBUIValidator.passwordContainsSubStringOfUserName("testPassword", "none", "Password");
		Assert.assertTrue(result);
	}
}
