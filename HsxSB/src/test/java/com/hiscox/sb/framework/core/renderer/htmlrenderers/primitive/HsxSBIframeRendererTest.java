package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.primitive.HsxSBIframeWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBIframeRendererTest {
	private HsxSBIframeRenderer hsxSBIframeRenderer;
	private HsxSBIframeWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBIframeRenderer = new HsxSBIframeRenderer();
		widget = new HsxSBIframeWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setSessionId("JER34I");
		hsxSBWidgetInfo.setHostUrl("test");
		hsxSBWidgetInfo.setIsJSEnabled("No");
		hsxSBWidgetInfo.setHeight("820");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBIframeRenderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBIframeRenderer.renderContent(widget);
		Assert.assertNotNull(result);
	}

}
