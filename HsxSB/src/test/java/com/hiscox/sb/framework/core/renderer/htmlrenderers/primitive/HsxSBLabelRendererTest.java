package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.primitive.HsxSBLabelWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBLabelRendererTest {

	private HsxSBLabelRenderer hsxSBLabelRenderer;
	private HsxSBLabelWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBLabelRenderer = new HsxSBLabelRenderer();
		widget = new HsxSBLabelWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setLabelForId("testLabel");
		hsxSBWidgetInfo.setLabel("test");
		hsxSBWidgetInfo.setIsMandatory("No");
		hsxSBWidgetInfo.setDisplayOnly("No");
		hsxSBWidgetInfo.setOptionalText("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBLabelRenderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBLabelRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='question-text'><label for='testLabel' id='test_label_Id' >test<span class='optional'> test</span></label></div>", result);
	}

}
