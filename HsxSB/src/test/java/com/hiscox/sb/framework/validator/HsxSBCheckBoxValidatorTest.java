package com.hiscox.sb.framework.validator;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HsxSBCheckBoxValidatorTest {

	private HsxSBCheckBoxValidator hsxSBCheckBoxValidator;

	@Before
	public void setUp() throws Exception {
		hsxSBCheckBoxValidator= new HsxSBCheckBoxValidator();
	}

	@After
	public void tearDown() throws Exception {
		hsxSBCheckBoxValidator = null;
	}

	@Test
	public void test() {
		Boolean result = hsxSBCheckBoxValidator.validateWidget(null, null);
		Assert.assertFalse(result);
		
	}

}