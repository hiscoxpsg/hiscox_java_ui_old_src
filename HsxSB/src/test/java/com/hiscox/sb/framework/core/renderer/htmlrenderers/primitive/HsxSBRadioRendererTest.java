package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBRadioWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBPermitedValues;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBRadioRendererTest {


    private HsxSBRadioRenderer hsxSBRadioRenderer;
    private HsxSBRadioWidget widget;
    private HsxSBWidgetInfo hsxSBWidgetInfo;
    private Map<String,String> additionalInfoMap;
    private HsxSBUIBuilderControllerImpl hsxSBUIBuilderControllerImpl;
    private HsxSBPermitedValues hsxSBPermitedValues;
    private List<HsxSBPermitedValues> permitedValuesList;

	@Before
	public void setUp() throws Exception {
		hsxSBRadioRenderer = new HsxSBRadioRenderer();
		widget = new HsxSBRadioWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setLabel("test");
		hsxSBWidgetInfo.setIsMandatory("No");
		hsxSBWidgetInfo.setOptionalText("test");
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setSubType("reverse");
		hsxSBWidgetInfo.setStyle("test");
		hsxSBWidgetInfo.setName("test");
		hsxSBWidgetInfo.setSavedValue("test");
		hsxSBWidgetInfo.setErrorIndicator(true);
		hsxSBPermitedValues = new HsxSBPermitedValues();
		hsxSBPermitedValues.setDisplayText("test");
		hsxSBPermitedValues.setValue("test");
		permitedValuesList = new ArrayList<HsxSBPermitedValues>();
		permitedValuesList.add(hsxSBPermitedValues);
		hsxSBWidgetInfo.setPermitedVlaues(permitedValuesList);
		additionalInfoMap = new HashMap<String, String>();
		additionalInfoMap.put("AdditionalText1", "Yes");
		additionalInfoMap.put("AdditionalText2", "Yes");
		additionalInfoMap.put("AdditionalText4", "Yes");
		hsxSBUIBuilderControllerImpl = new HsxSBUIBuilderControllerImpl();
		hsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
		hsxSBWidgetInfo.setAdditionalText1("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		additionalInfoMap = null;
		hsxSBRadioRenderer = null;
		hsxSBUIBuilderControllerImpl = null;
		hsxSBWidgetInfo = null;
		widget = null;
		hsxSBPermitedValues = null;
		permitedValuesList = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBRadioRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<fieldset class='fs-type-radio screen-reader-only'><legend><span class='question-text'><ins>test</ins></span></legend>" +
				"<div class='question-text'><label>test<span class='optional'> test</span></label></div>" +
				"<div class='control'><label for='test_radio_test_id'><input type='radio' class='test' id='test_radio_test_id' name='test' value='test' checked ='checked'/>" +
				"<span>test</span></label></div><div id='test_additional1_id'class='additional-text'><span>test</span></div></fieldset>", result);
	}

}
