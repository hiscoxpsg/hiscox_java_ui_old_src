package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.hiscox.sb.framework.core.widgets.simple.HsxSBEditWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

public class HsxSBEditRendererTest {

	private HsxSBEditRenderer hsxSBEditRenderer;
	private HsxSBEditWidget widget;
	private HsxSBWidgetInfo hsxSBWidgetInfo;

	@Before
	public void setUp() throws Exception {
		hsxSBEditRenderer = new HsxSBEditRenderer();
		widget = new HsxSBEditWidget();
		hsxSBWidgetInfo = new HsxSBWidgetInfo();
		hsxSBWidgetInfo.setId("test");
		hsxSBWidgetInfo.setAdditionalText4("test");
		hsxSBWidgetInfo.setAdditionalText5("test");
		widget.setWidgetInfo(hsxSBWidgetInfo);
	}

	@After
	public void tearDown() throws Exception {
		hsxSBEditRenderer = null;
		widget = null;
		hsxSBWidgetInfo = null;
		
	}

	@Test
	public void test() {
		String result = hsxSBEditRenderer.renderContent(widget);
		Assert.assertNotNull(result);
		Assert.assertEquals("<div class='icon edit'><a title='Edit' href=test>test<input type='hidden' value='test' id='test'/></a></div>", result);
	}

}
