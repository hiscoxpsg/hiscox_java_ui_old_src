package com.hiscox.sb.framework.validator;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;

/**
 * This class validates check box widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:08 AM
 */
public class HsxSBCheckBoxValidator {

	public HsxSBBaseWidget hsxSBBaseWidget;

	public HsxSBCheckBoxValidator() {

	}

	/**
	 * This method is to validate the widget.
	 * @param regExp is a input String
	 * @param inputValue is a input string
	 * @return false
	 */
	public boolean validateWidget(String regExp, String inputValue) {
		return false;
	}

}
