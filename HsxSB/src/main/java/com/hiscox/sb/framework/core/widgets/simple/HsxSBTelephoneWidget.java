package com.hiscox.sb.framework.core.widgets.simple;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.HsxSBCompositeWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class holds the characteristics of Telephone number entry fields. and
 * implements the basic functionality defined by the widget interface to create
 * a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:14 AM
 */
public class HsxSBTelephoneWidget extends HsxSBCompositeWidget {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<HsxSBWidget> widgetsList = new ArrayList<HsxSBWidget>();

    public HsxSBTelephoneWidget() {

	super();
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     * @param widgetInfo contains info of widget
     */
    public HsxSBTelephoneWidget(String id, String name,
	    HsxSBWidgetInfo widgetInfo) {

	super(id, name, widgetInfo);
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     */
    public HsxSBTelephoneWidget(String id, String name) {

	super(id, name);
    }

    /**
     * Method to create widget by setting widget info, Style hint etc.
     *
     * @param widgetInfo contains info of widget
     */
    @Override
    public void createWidget(HsxSBWidgetInfo widgetInfo) {
	if (HsxSBUIBuilderConstants.HTML_TELEPHONE_SUBTYPE_STYLE
		.equalsIgnoreCase(widgetInfo.getSubType())) {
	    widgetInfo.setStyle(HsxSBUIBuilderConstants.HTML_TELEPHONE_STYLE);
	}
	this.setWidgetInfo(widgetInfo);

    }

    /**
     * This method adds the widget to the list.
     *
     * @param widget contains widget data
     */
    @Override
    public void addWidget(HsxSBWidget widget) {

	widgetsList.add(widget);
    }

    /**
     * This method retrieves the widgetlist.
     *
     * @return widget list
     */
    public List<HsxSBWidget> getWidgetsList() {
	return widgetsList;
    }

    @Override
    public void createWidget(Element element) {
	// TODO Auto-generated method stub

    }

    /**
     * This method removes the widget form the list.
     *
     * @param widget contains widget data
     */
    @Override
    public void removeWidget(HsxSBWidget widget) {

	widgetsList.remove(widget);
    }

}
