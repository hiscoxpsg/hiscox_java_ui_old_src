package com.hiscox.sb.framework.exception;

/**
 * This class handles the Runtime exception of entire dynamic framework.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:44 AM
 */
public class HsxSBUIBuilderRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private String errorMessage;

	public HsxSBUIBuilderRuntimeException() {

	}

	public HsxSBUIBuilderRuntimeException(String message) {
		this.errorMessage = message;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
