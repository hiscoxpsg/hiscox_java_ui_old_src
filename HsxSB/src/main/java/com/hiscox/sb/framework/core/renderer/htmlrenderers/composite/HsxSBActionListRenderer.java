package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBActionListWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the HTML tags to display questions.
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:29 AM
 */
public class HsxSBActionListRenderer extends HsxSBHTMLCompositeRenderer
	implements HsxSBUIBuilderConstants {

    /**
     * This method returns the String for rendering the HTML tag to display the
     * Action Lists.
     * @param widget contains widget data
     * @return contentBuilder in the form of string
     * @throws HsxSBUIBuilderRuntimeException which returns run time exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();
	HsxSBActionListWidget actionGroupWidget = (HsxSBActionListWidget) widget;
	if (actionGroupWidget.getWidgetInfo().getStyleHint() == null) {
	    actionGroupWidget.getWidgetInfo().setStyleHint(STR_EMPTY);
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SCREEN_STYLE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(actionGroupWidget.getWidgetInfo().getStyleHint());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);
	if (null != actionGroupWidget.getWidgetsList()) {
	    Iterator<HsxSBWidget> widgetsListIterator = actionGroupWidget
		    .getWidgetsList().iterator();
	    while (widgetsListIterator.hasNext()) {
		HsxSBWidget innerWidget = widgetsListIterator.next();
		HsxSBRenderer renderer = HsxSBHTMLRendererFactory
			.getRendererInstance(innerWidget.getWidgetInfo()
				.getType());
		if (renderer != null) {
		    contentBuilder.append(renderer.renderContent(innerWidget));
		}

	    }
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);
	return contentBuilder.toString();
    }

}
