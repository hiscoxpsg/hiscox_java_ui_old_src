package com.hiscox.sb.framework.core.widgets;

import java.io.Serializable;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class defines the basic functionalities to manipulate a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:05 AM
 */
public abstract class HsxSBBaseWidget implements HsxSBWidget, HsxSBUIBuilderConstants, Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -335852454538968402L;
	private String widgetId;
	private String name;
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	private HsxSBWidgetInfo widgetInfo;
	public HsxSBWidgetInfo hsxSBWidgetInfo;
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public HsxSBBaseWidget() {

		super();
	}

	/**
	 * This method sets the Widget id and name of the base widget.
	 * @param widgetId contains id
	 * @param name contains name
	 */
	public HsxSBBaseWidget(String widgetId, String name) {

		super();
		this.widgetId = widgetId;
		this.name = name;
	}

	/**
	 * This method sets the Widget id, widget info and name of the base widget.
	 * @param widgetId contains Id
	 * @param name contains name
	 * @param widgetInfo contains info of widget
	 */
	public HsxSBBaseWidget(String widgetId, String name,
			HsxSBWidgetInfo widgetInfo) {

		super();
		this.widgetId = widgetId;
		this.name = name;
		this.widgetInfo = widgetInfo;
	}

	public String getId() {
		return widgetId;
	}

	public String getName() {
		return name;
	}

	public HsxSBWidgetInfo getWidgetInfo() {
		return widgetInfo;
	}

	/**
	 *
	 * @param widgetId contains id of widget
	 */
	public void setId(String widgetId) {

		this.widgetId = widgetId;
	}

	/**
	 *
	 * @param name contains name of widget
	 */
	public void setName(String name) {

		this.name = name;
	}

	/**
	 *
	 * @param widgetInfo contains info of widget
	 */
	public void setWidgetInfo(HsxSBWidgetInfo widgetInfo) {

		this.widgetInfo = widgetInfo;
	}

	/**
	 * Method to create widget by setting widget info, Style hint etc.
	 *
	 * @param widgetInfo contains info of widget
	 */
	public abstract void createWidget(HsxSBWidgetInfo widgetInfo);
}
