package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBTelephoneWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML Telephone tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:13 AM
 */
public class HsxSBTelephoneRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Telephone tag.
     * The Telephone renderer is combination of three text box renderer's
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBTelephoneWidget textWidget = (HsxSBTelephoneWidget) widget;
	String value = null;
	String name = STR_EMPTY;
	String tel1 = STR_EMPTY;
	String tel2 = STR_EMPTY;
	String tel3 = STR_EMPTY;
	int maxLength = 10;
	if (HsxSBUtil.isNotBlank(textWidget.getWidgetInfo().getMaxLength())) {
	    maxLength = Integer.valueOf(textWidget.getWidgetInfo()
		    .getMaxLength());
	}

	if (null != textWidget.getWidgetInfo().getSavedValue()
		&& textWidget.getWidgetInfo().getSavedValue().trim().length() > 0) {
	    value = textWidget.getWidgetInfo().getSavedValue();
	} else {
	    value = textWidget.getWidgetInfo().getDefaultValue();
	}

	if (null == textWidget.getWidgetInfo().getName()) {
	    name = textWidget.getWidgetInfo().getId();
	} else {
	    name = textWidget.getWidgetInfo().getName();
	}
	String telephoneString = value;

	if (telephoneString != null) {
	    int telephoneLength = telephoneString.length();
	    if (telephoneLength >= maxLength) {
		tel1 = telephoneString.substring(0, 3);
		tel2 = telephoneString.substring(3, 6);
		tel3 = telephoneString.substring(6, maxLength);

	    } else if ((telephoneLength > 0) && (telephoneLength <= 3)) {
		tel1 = telephoneString;
		tel2 = STR_EMPTY;
		tel3 = STR_EMPTY;
	    } else if ((telephoneLength > 3) && (telephoneLength <= 6)) {
		tel1 = telephoneString.substring(0, 3);
		tel2 = telephoneString.substring(3, telephoneLength);
		tel3 = STR_EMPTY;
	    } else if ((telephoneLength > 6) && (telephoneLength < maxLength)) {
		tel1 = telephoneString.substring(0, 3);
		tel2 = telephoneString.substring(3, 6);
		tel3 = telephoneString.substring(6, telephoneLength);
	    } else {
		tel1 = STR_EMPTY;
		tel2 = STR_EMPTY;
		tel3 = STR_EMPTY;

	    }

	} else {
	    tel1 = STR_EMPTY;
	    tel2 = STR_EMPTY;
	    tel3 = STR_EMPTY;

	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CONTROL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_TEXT);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	/*contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TABINDEX);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getOrder());
	contentBuilder.append(HTML_SINGLE_QUOTE);*/
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(name);
	contentBuilder.append(STR_ONE);
	contentBuilder.append(HTML_SINGLE_QUOTE);

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getId());
	contentBuilder.append(STR_ONE);
	contentBuilder.append(TEXTBOX_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(tel1);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_MAXLENGTH);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(STR_THREE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	if (HTML_YES.equalsIgnoreCase(textWidget.getWidgetInfo()
		.getDisplayOnly())) {
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE + HTML_DISABLED
		    + HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	}
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getStyle());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CLOSE_END_TAG);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(HTML_HYPHEN);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_TEXT);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	/*contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TABINDEX);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getOrder());
	contentBuilder.append(HTML_SINGLE_QUOTE);*/
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(name);
	contentBuilder.append(STR_TWO);
	contentBuilder.append(HTML_SINGLE_QUOTE);

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getId());
	contentBuilder.append(STR_TWO);
	contentBuilder.append(TEXTBOX_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(tel2);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_MAXLENGTH);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(STR_THREE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	if (HTML_YES.equalsIgnoreCase(textWidget.getWidgetInfo()
		.getDisplayOnly())) {
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	}
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getStyle());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CLOSE_END_TAG);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(HTML_HYPHEN);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_TEXT);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	/*contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TABINDEX);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getOrder());
	contentBuilder.append(HTML_SINGLE_QUOTE);*/
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(name);
	contentBuilder.append(STR_THREE);
	contentBuilder.append(HTML_SINGLE_QUOTE);

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getId());
	contentBuilder.append(STR_THREE);
	contentBuilder.append(TEXTBOX_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(tel3);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_MAXLENGTH);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(maxLength - 6);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	if (HTML_YES.equalsIgnoreCase(textWidget.getWidgetInfo()
		.getDisplayOnly())) {
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	}
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getStyle());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CLOSE_END_TAG);

	/*************************** Hidden input to store the telephone number ******/

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_HIDDEN);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(name);
	contentBuilder.append(HTML_SINGLE_QUOTE);

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getId());
	contentBuilder.append(HIDDEN_PREFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textWidget.getWidgetInfo().getSavedValue());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLOSE_END_TAG);

	/*****************************/

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	String includeHelp = HsxSBWidgetUtil.addHelpRenderer(textWidget);
	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
		String includeAdditionalText4 = HsxSBWidgetUtil
			.addAdditionalText4Renderer(textWidget);
		contentBuilder.append(includeAdditionalText4);
	    }

	contentBuilder.append(includeHelp);

	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
		String includeAdditionalText1 = HsxSBWidgetUtil
			.addAdditionalTextOneRenderer(textWidget);
		contentBuilder.append(includeAdditionalText1);
	    }
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
		String includeAdditionalText2 = HsxSBWidgetUtil
			.addAdditionalTextTwoRenderer(textWidget);
		contentBuilder.append(includeAdditionalText2);
	    }
	}

	if (textWidget.getWidgetInfo().isErrorIndicator()) {
	    String includeError = HsxSBWidgetUtil.addErrorRenderer(textWidget);
	    contentBuilder.append(includeError);
	}
	return contentBuilder.toString();
    }
}
