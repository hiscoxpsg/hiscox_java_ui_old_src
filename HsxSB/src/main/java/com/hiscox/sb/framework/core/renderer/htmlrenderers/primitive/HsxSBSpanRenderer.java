package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBSpanWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML Span tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:38 AM
 */
public class HsxSBSpanRenderer extends HsxSBHTMLBaseRenderer implements
		HsxSBUIBuilderConstants {

	/**
	 * This method will return the String for rendering the HTML Span.
	 * @param widget contains widget data
     * @return content
	 */
	@Override
	public String renderContent(final HsxSBWidget widget) {
		StringBuilder contentBuilder = new StringBuilder();

		HsxSBSpanWidget spanWidget = (HsxSBSpanWidget) widget;
		String displayValue = STR_EMPTY;

		StringBuffer decPlaces = new StringBuffer();
		DecimalFormatSymbols seperator = new DecimalFormatSymbols();
		DecimalFormat decFormat;

		if (HsxSBUtil.isNotBlank(spanWidget.getWidgetInfo().getSavedValue())) {
			displayValue = spanWidget.getWidgetInfo().getSavedValue();
		}

		// to display the date values in the us lolace specific format
		final String subType = spanWidget.getWidgetInfo().getSubType();
		if (HsxSBUtil.isDate(displayValue)) {
			try {
				DateFormat formatter;
				Date date;
				formatter = new SimpleDateFormat(HTML_DATE_FORMAT);
				date = (Date) formatter.parse(displayValue);

				if (HTML_DATE_MONTH_YEAR.equalsIgnoreCase(subType)) {
					formatter = new SimpleDateFormat(HTML_MONTH_YEAR_FORMAT);
					displayValue = formatter.format(date);

				} else {
					// point to US locale
					Locale locale = Locale.US;
					displayValue = DateFormat.getDateInstance(DateFormat.LONG,
							locale).format(date);
				}

			} catch (ParseException e) {
				throw new HsxSBUIBuilderRuntimeException(e.getMessage());
			}

		}

		// to display the the Numeric / currency values with decimal format and
		// thousand separator
		if ((HTML_CURRENCY.equalsIgnoreCase(subType) || HTML_NUMERIC
				.equalsIgnoreCase(subType))
				&& HsxSBUtil.isNotBlank(displayValue)
				&& HsxSBUtil.isNumeric(displayValue)) {

			displayValue = displayValue.replaceAll(REG_ONLY_NUMERIC, STR_EMPTY);

			String pattern = STR_INTEGER_PATTERN;
			if (HsxSBUtil.isNotBlank(spanWidget.getWidgetInfo()
					.getDecimalPlaces())
					&& displayValue.contains(STR_DOT)) {
				for (int i = 0; i < Integer.valueOf(spanWidget.getWidgetInfo()
						.getDecimalPlaces()); i++) {
					decPlaces.append(STR_ZERO_PATTERN);
				}
				pattern = STR_DECIMAL_PATTERN;
			} else if (displayValue.contains(STR_DOT)) {
				decPlaces.append(STR_ZERO_PATTERN);
				decPlaces.append(STR_ZERO_PATTERN);
				pattern = STR_DECIMAL_PATTERN;
			}

			if (!HsxSBUtil.isNotBlank(spanWidget.getWidgetInfo()
					.getThousandSeperator())) {
				seperator.setGroupingSeparator(CHAR_COMMA);
				pattern = pattern.replaceAll(STR_COMMA, STR_EMPTY);
			} else {
				seperator.setGroupingSeparator(spanWidget.getWidgetInfo()
						.getThousandSeperator().toCharArray()[0]);
			}

			decFormat = new DecimalFormat(decPlaces.insert(0, pattern)
					.toString(), seperator);

			if (displayValue.contains(STR_DOT)) {
				displayValue = decFormat.format(Double.valueOf(displayValue));
			} else {
				displayValue = decFormat.format(Long.valueOf(displayValue));
			}
			if(displayValue.startsWith(STR_DOT)){
				displayValue = displayValue.replace(STR_DOT, STR_ZERO_DOT);
			}
		}

		if (HsxSBUtil.isNotBlank(displayValue)) {
			contentBuilder.append(HTML_START_TAG);
			contentBuilder.append(HTML_DIV);
			contentBuilder.append(HTML_SINGLESPACE);
			contentBuilder.append(HTML_CLASS);
			contentBuilder.append(HTML_EQUALS);
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(HTML_CONTROL_DIV_STYLE);
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(HTML_END_TAG);

			contentBuilder.append(HTML_START_TAG);
			contentBuilder.append(HTML_SPAN);
			contentBuilder.append(HTML_SINGLESPACE);
			contentBuilder.append(HTML_ID);
			contentBuilder.append(HTML_EQUALS);
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(spanWidget.getWidgetInfo().getId());
			contentBuilder.append(SPAN_ID_SUFFIX);
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(HTML_SINGLESPACE);
			/*
			 * contentBuilder.append(HTML_CLASS);
			 * contentBuilder.append(HTML_EQUALS);
			 * contentBuilder.append(HTML_SINGLE_QUOTE);
			 * contentBuilder.append(spanWidget.getWidgetInfo().getStyle());
			 * contentBuilder.append(HTML_SINGLE_QUOTE);
			 */
			contentBuilder.append(HTML_END_TAG);
			Map<String, HsxSBTextManagerVO> textMap = HsxSBUIBuilderControllerImpl.textMap;
			if (HTML_CURRENCY.equalsIgnoreCase(subType)
					&& HsxSBUtil.isNotBlank(spanWidget.getWidgetInfo()
							.getCurrency())) {

				StringBuilder currTextCodeBldr = new StringBuilder(STR_EMPTY);
				currTextCodeBldr.append(spanWidget.getWidgetInfo()
						.getCurrency());
				currTextCodeBldr.append(STR_CURRENCY_KEY);
				final String currTextCode = currTextCodeBldr.toString();
				if (currTextCodeBldr != null && textMap != null
						&& textMap.containsKey(currTextCode)) {
					final String currLabelText = textMap.get(currTextCode)
							.getLabelText();
					if (HsxSBUtil.isNotBlank(currLabelText)) {
						contentBuilder.append(currLabelText);
					}
				}

			}
			// some of the permitted values has to be taken from the TxtManger
			// for display only cases
			if (STR_YES.equalsIgnoreCase(spanWidget.getWidgetInfo()
					.getTMLookup())) {
				displayValue = spanWidget.getWidgetInfo().getSavedValue();
				if (textMap != null && textMap.containsKey(displayValue)) {
					displayValue = HsxSBUIBuilderControllerImpl.textMap.get(
							displayValue).getLabelText();
				}

			}
			if (SUBTYPE_PERCENTAGE.equalsIgnoreCase(subType)
					&& HsxSBUtil.isNotBlank(displayValue)) {
				displayValue = displayValue.replaceAll(
						REG_ONLY_NUMERIC_WITHOUT_DECIMALS, STR_EMPTY);
				contentBuilder.append(displayValue);
				contentBuilder.append(HTML_PERCENTAGE_SYMBOL);
			}else if((spanWidget.getWidgetInfo().getId()).equalsIgnoreCase(STR_SCSECONDARY_COB)){
				 	displayValue = displayValue.replaceAll(STR_COMMA, HTML_BREAK);
			    	contentBuilder.append(displayValue);
			 	}else {
					contentBuilder.append(displayValue);
				}
			contentBuilder.append(HTML_START_TAG);
			contentBuilder.append(HTML_FRONT_SLASH);
			contentBuilder.append(HTML_SPAN);
			contentBuilder.append(HTML_END_TAG);

			contentBuilder.append(HTML_START_TAG);
			contentBuilder.append(HTML_FRONT_SLASH);
			contentBuilder.append(HTML_DIV);
			contentBuilder.append(HTML_END_TAG);

		}
		String includeHelp = HsxSBWidgetUtil.addHelpRenderer(spanWidget);
		if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
				.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
				String includeAdditionalText4 = HsxSBWidgetUtil
						.addAdditionalText4Renderer(spanWidget);
				contentBuilder.append(includeAdditionalText4);
		}

		contentBuilder.append(includeHelp);

		if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
			if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
					.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
				String includeAdditionalText1 = HsxSBWidgetUtil
						.addAdditionalTextOneRenderer(spanWidget);
				contentBuilder.append(includeAdditionalText1);
			}
			if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
					.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
				String includeAdditionalText2 = HsxSBWidgetUtil
						.addAdditionalTextTwoRenderer(spanWidget);
				contentBuilder.append(includeAdditionalText2);
			}
		}

		return contentBuilder.toString();
	}

}
