package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBStaticWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML static type display of the text.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:26 AM
 */
public class HsxSBStaticRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Label.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	HsxSBStaticWidget staticWidget = (HsxSBStaticWidget) widget;
	StringBuilder contentBuilder = new StringBuilder();

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_LABEL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_LABEL);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(staticWidget.getWidgetInfo().getId());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	if (HsxSBUtil.isNotBlank(staticWidget.getWidgetInfo().getSavedValue())) {
	    contentBuilder.append(staticWidget.getWidgetInfo().getSavedValue());
	} else if (HsxSBUtil.isNotBlank(staticWidget.getWidgetInfo().getLabel())) {
	    contentBuilder.append(staticWidget.getWidgetInfo().getLabel());

	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_LABEL);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	String includeHelp = HsxSBWidgetUtil.addHelpRenderer(staticWidget);
	if (SUBTYPE_TWOCOLUMN.equalsIgnoreCase(staticWidget.getWidgetInfo()
		.getSubType())) {
	    if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
		if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
			.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
		    String includeAdditionalText1 = HsxSBWidgetUtil
			    .addAdditionalTextOneRenderer(staticWidget);
		    contentBuilder.append(includeAdditionalText1);
		}
		if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
			.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
		    String includeAdditionalText4 = HsxSBWidgetUtil
			    .addAdditionalText4Renderer(staticWidget);
		    contentBuilder.append(includeAdditionalText4);
		}

	    }

	    contentBuilder.append(includeHelp);

	    if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {

		if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
			.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
		    String includeAdditionalText2 = HsxSBWidgetUtil
			    .addAdditionalTextTwoRenderer(staticWidget);
		    contentBuilder.append(includeAdditionalText2);
		}
	    }
	} else {
	    if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
				.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
		    String includeAdditionalText4 = HsxSBWidgetUtil
			    .addAdditionalText4Renderer(staticWidget);
		    contentBuilder.append(includeAdditionalText4);
		}

	  

	    contentBuilder.append(includeHelp);

	    if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
		if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
			.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
		    String includeAdditionalText1 = HsxSBWidgetUtil
			    .addAdditionalTextOneRenderer(staticWidget);
		    contentBuilder.append(includeAdditionalText1);
		}
		if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
			.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
		    String includeAdditionalText2 = HsxSBWidgetUtil
			    .addAdditionalTextTwoRenderer(staticWidget);
		    contentBuilder.append(includeAdditionalText2);
		}
	    }
	}

	return contentBuilder.toString();
    }

}
