package com.hiscox.sb.framework.core.util;

import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class processes the widgets and generates Screen XML.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:36 AM
 */
public class HsxSBResponseProcessor implements HsxSBUIBuilderConstants {

	/**
	 * Widget map and xml document is passed as an input paramater and using
	 * which SCREEn XML is constructed.
	 *
	 * @param xmlDocument contains XML document
	 * @param widgetMap contains widget map
	 * @param requestedAction contains requestedAction
	 * @throws HsxSBUIBuilderRuntimeException throws run time exception
	 * @return ScreenQuestionXML
	 * @throws HsxSBUIBuilderException which throws exception
	 */
	public static String generateScreenQuestionXML(Document xmlDocument,
			Map<String, HsxSBWidget> widgetMap, String requestedAction)
			throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {

		if (xmlDocument != null) {
			xmlDocument = removeUnWantedConfigElements(xmlDocument);
			xmlDocument = removeblankElements(xmlDocument);
			Element requestedActionElement = (Element) xmlDocument
					.getElementsByTagNameNS(STR_ASTERISK, XML_REQUESTED_ACTION)
					.item(0);
			if (requestedActionElement != null) {
				requestedActionElement.setTextContent(requestedAction);
			}

			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = null;
			try {
				expr = xpath
						.compile(XPATH_SCREEN_QUESTION_CONFIG_NAME_SAVED_VALUE);
				Object transaction = expr.evaluate(xmlDocument,
						XPathConstants.NODESET);
				NodeList savedValueConfigs = (NodeList) transaction;
				final int configListLen = savedValueConfigs.getLength();
				for (int configIndex = 0; configIndex < configListLen; configIndex++) {
					Element savedValueConfig = (Element) savedValueConfigs
							.item(configIndex);
					if (savedValueConfig != null) {
						Element questionNode = (Element) savedValueConfig
								.getParentNode();
						if (questionNode != null) {
							String screenQuestionCode = questionNode
									.getAttribute(XML_CODE);
							if (HsxSBUtil.isNotBlank(screenQuestionCode)) {
								screenQuestionCode = screenQuestionCode.trim();
								String answerdValue = STR_EMPTY;
								answerdValue = getWidgetValue(widgetMap,
										screenQuestionCode);
								savedValueConfig.setAttribute(STR_VALUE,
										answerdValue);
							}
						}
					}
				}
			} catch (XPathExpressionException e) {
				e.printStackTrace();
				throw new HsxSBUIBuilderRuntimeException(e.getMessage());
			}

			// NodeList screenQuestionList = xmlDocument.getElementsByTagNameNS(
			// STR_ASTERISK, XML_SCREEN_QUESTION);
			// final int noOfScreenQues = screenQuestionList.getLength();
			// for (int questionIndex = 0; questionIndex < noOfScreenQues;
			// questionIndex++) {
			// Element screenQuestion = (Element) screenQuestionList
			// .item(questionIndex);
			// NodeList childElementsList = screenQuestion.getChildNodes();
			// final int noOfChildElements = childElementsList.getLength();
			// for (int configIndex = 0; configIndex < noOfChildElements;
			// configIndex++) {
			// if (childElementsList.item(configIndex) != null) {
			// String nodeName = childElementsList.item(configIndex)
			// .getNodeName();
			// if (childElementsList.item(configIndex).getNodeType() ==
			// Node.ELEMENT_NODE
			// && nodeName != null
			// && nodeName.contains(XML_CONFIG)) {
			// Element childElement = (Element) childElementsList
			// .item(configIndex);
			// if (CONFIG_SAVED_VALUE
			// .equalsIgnoreCase((childElement
			// .getAttribute(STR_NAME)).toString())) {
			// String answerdValue = STR_EMPTY;
			// String screenQuestionCode = screenQuestion
			// .getAttribute(XML_CODE).toString();
			// answerdValue = getWidgetValue(widgetMap,
			// screenQuestionCode);
			// childElement.setAttribute(STR_VALUE,
			// answerdValue);
			// }
			// }
			// }
			// }
			// }
			xmlDocument.normalize();
			String responseXml = HsxSBUtil.getStringFromDocument(xmlDocument);
			return responseXml;
		} else {
			return STR_EMPTY;
		}

	}

	/**
	 * @param widgetMap contains widget map
	 * @param xmlQuestionCode contains xmlQuestionCode
	 * @return answerValue
	 */
	public static String getWidgetValue(Map<String, HsxSBWidget> widgetMap,
			String xmlQuestionCode) {
		String answerValue = null;
		if (xmlQuestionCode != null) {
			xmlQuestionCode = xmlQuestionCode.trim().toLowerCase();
			if (widgetMap != null && widgetMap.containsKey(xmlQuestionCode)
					&& widgetMap.get(xmlQuestionCode) != null
					&& widgetMap.get(xmlQuestionCode).getWidgetInfo() != null) {
				answerValue = widgetMap.get(xmlQuestionCode).getWidgetInfo()
						.getSavedValue();

				String subType = widgetMap.get(xmlQuestionCode).getWidgetInfo()
						.getSubType();
				if (!QUESTION_CODE_SECURITYCODE.equalsIgnoreCase(widgetMap.get(
						xmlQuestionCode).getWidgetInfo().getId())
						&& HTML_TEXT.equalsIgnoreCase(widgetMap.get(
								xmlQuestionCode).getWidgetInfo().getType())
						&& (HTML_NUMERIC.equalsIgnoreCase(subType)
								|| SUBTYPE_CURRENCY.equalsIgnoreCase(subType) || SUBTYPE_PERCENTAGE
								.equalsIgnoreCase(subType))) {
					answerValue = formatAnswerValue(answerValue);
				}
			}

		}
		return answerValue;
	}

	/**
	 * @param answerValue contains unformatted answer value
	 * @return answerValue
	 */
	private static String formatAnswerValue(String answerValue) {
		if (HsxSBUtil.isNotBlank(answerValue)) {
			answerValue = answerValue.replaceAll(REG_ONLY_NUMERIC, STR_EMPTY);

			if (HsxSBUtil.isNotBlank(answerValue)) {
				Double d = Double.valueOf(answerValue);
				if (d != null && (d - (d.intValue())) == 0) {
					answerValue = Integer.toString(d.intValue());
				}
			}
		}
		return answerValue;
	}

	/**
	 * @param questionId contains question Id
	 * @param savedValue contains saved value
	 */
	public void updateResponseXML(String questionId, String savedValue) {

	}

	/**
	 * @param screenQuestion contains screenQuestion
	 * @param savedValue contains saved values
	 */
	public void updateDocumentValue(Element screenQuestion, String savedValue) {

	}

	/**
	 * This method will remove unwanted config elements, ipeQuestionReferences
	 * and Permitted Values from the given xml document.
	 * @param xmlDocument contains xmlDocument with unwanted config elements
	 * @return xmlDocument
	 */
	public static Document removeUnWantedConfigElements(Document xmlDocument) {
		if (xmlDocument != null) {
			XPath xpath = XPathFactory.newInstance().newXPath();
			XPathExpression expr = null;
			try {
				StringBuilder xpathQuery = new StringBuilder(STR_EMPTY);
				xpathQuery.append(XPATH_APPLICATION_AREA_STATUS);
				xpathQuery.append(XPATH_OR);
				xpathQuery.append(XPATH_APPLICATION_AREA_CORRELATION_ID);
				xpathQuery.append(XPATH_OR);
				xpathQuery.append(XPATH_APPLICATION_AREA_ERRORS);
				xpathQuery.append(XPATH_OR);
				xpathQuery.append(XPATH_DATA_AREA_CONFIG_NAME_UPDATE_QUOTE_ID);
				xpathQuery.append(XPATH_OR);
				xpathQuery.append(XPATH_FORM_CONFIG_FORM_ACTION_LIST);
				xpathQuery.append(XPATH_OR);
				xpathQuery.append(XPATH_QUESTION_GROUP_IPEQUESTION_REFERENCE);
				xpathQuery.append(XPATH_OR);
				xpathQuery.append(XPATH_QUESTION_GROUP_CONFIG);
				xpathQuery.append(XPATH_OR);
				xpathQuery.append(XPATH_SCREEN_QUESTION_IPEQUESTION_REFERENCE);
				xpathQuery.append(XPATH_OR);
				xpathQuery.append(XPATH_SCREEN_QUESTION_PERMITTED_VALUE);
				xpathQuery.append(XPATH_OR);
				xpathQuery
						.append(XPATH_SCREEN_QUESTION_CONFIG_NAME_NOT_SAVED_VALUE);
				xpathQuery.append(XPATH_OR);
				xpathQuery.append(XPATH_TAG_LIST_CONFIG);

				expr = xpath.compile(xpathQuery.toString());

				removeChildElements(xmlDocument, expr);
			} catch (XPathExpressionException e) {
				e.printStackTrace();
				throw new HsxSBUIBuilderRuntimeException(e.getMessage());
			}

			// Element appAreaElement = (Element) xmlDocument
			// .getElementsByTagNameNS(STR_ASTERISK, XML_APPLICATION_AREA)
			// .item(0);
			// if (appAreaElement != null) {
			// removeInvalidElement(appAreaElement, XML_STATUS);
			// removeInvalidElement(appAreaElement, XML_CORRELATION_ID);
			// removeErrorElement(appAreaElement);
			// }

			// Element dataAreaElement = (Element) xmlDocument
			// .getElementsByTagNameNS(STR_ASTERISK, XML_DATA_AREA)
			// .item(0);
			//
			// NodeList dataAreaChildNodesList =
			// dataAreaElement.getChildNodes();
			// final int dataAreaChldNodesLstLength = dataAreaChildNodesList
			// .getLength();
			// for (int dataAreaChildNodesIndex = 0; dataAreaChildNodesIndex <
			// dataAreaChldNodesLstLength; dataAreaChildNodesIndex++) {
			// Node daNode = dataAreaChildNodesList
			// .item(dataAreaChildNodesIndex);
			// if (daNode != null && daNode.getNodeType() == Node.ELEMENT_NODE)
			// {
			// String nodeName = daNode.getNodeName();
			// if (nodeName != null) {
			// if (nodeName.contains(XML_CONFIG)) {
			// Element config = (Element) dataAreaChildNodesList
			// .item(dataAreaChildNodesIndex);
			// if (!CONFIG_UPDATE_QUOTEID.equalsIgnoreCase(config
			// .getAttribute(STR_NAME))) {
			// dataAreaElement.removeChild(daNode);
			// }
			// } else if (nodeName.contains(XML_FORM)) {
			// NodeList formChildList = daNode.getChildNodes();
			// final int formChildLen = formChildList.getLength();
			// for (int fcIndex = 0; fcIndex < formChildLen; fcIndex++) {
			// Node fcNode = formChildList.item(fcIndex);
			// if (fcNode != null
			// && fcNode.getNodeType() == Node.ELEMENT_NODE) {
			// final String formChildName = fcNode
			// .getNodeName();
			// if (formChildName != null
			// && (formChildName
			// .contains(XML_CONFIG) || (formChildName
			// .contains(XML_ACTION)))) {
			// daNode.removeChild(fcNode);
			// }
			// }
			// }
			// }
			// }
			// }
			// }
			// replication of Question Group child removal functionality
			// using XPath

			// try {
			// expr = xpath
			// .compile("//QuestionGroup/IPEQuestionReference|//QuestionGroup/Config");
			// //Remove QuestionGroup unwanted child elements
			// removeChildElements(xmlDocument, expr);
			//
			// } catch (XPathExpressionException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// throw new HsxSBUIBuilderRuntimeException(e.getMessage());
			// }

			// NodeList questionGroupList = xmlDocument.getElementsByTagNameNS(
			// STR_ASTERISK, XML_QUESTION_GROUP);
			// final int noOfQuesGroups = questionGroupList.getLength();
			// for (int questionGroupListIndex = 0; questionGroupListIndex <
			// noOfQuesGroups; questionGroupListIndex++) {
			// Element questionGroupElement = (Element) questionGroupList
			// .item(questionGroupListIndex);
			// if (questionGroupElement != null) {
			// NodeList questionGroupChildElementsList = questionGroupElement
			// .getChildNodes();
			// final int noOfChildQuesGroupElements =
			// questionGroupChildElementsList
			// .getLength();
			// for (int qGChildElementsListIndex = 0; qGChildElementsListIndex <
			// noOfChildQuesGroupElements; qGChildElementsListIndex++) {
			// Node qGNode = questionGroupChildElementsList
			// .item(qGChildElementsListIndex);
			// if (qGNode != null
			// && qGNode.getNodeType() == Node.ELEMENT_NODE) {
			// String nodeName = qGNode.getNodeName();
			// if (nodeName != null
			// && (nodeName.contains(XML_CONFIG) || nodeName
			// .contains(XML_IPE_QUESTION_REFERENCE))) {
			// questionGroupElement.removeChild(qGNode);
			// }
			// }
			// }
			// }
			// }

			// NodeList screenQuestionList = xmlDocument.getElementsByTagNameNS(
			// STR_ASTERISK, XML_SCREEN_QUESTION);
			// final int noOfScreenQuestions = screenQuestionList.getLength();
			// for (int questionIndex = 0; questionIndex < noOfScreenQuestions;
			// questionIndex++) {
			// Element screenQuestion = (Element) screenQuestionList
			// .item(questionIndex);
			// if (screenQuestion != null) {
			// NodeList childElementsList = screenQuestion.getChildNodes();
			// final int noOfChildElements = childElementsList.getLength();
			// for (int configIndex = 0; configIndex < noOfChildElements;
			// configIndex++) {
			// if (childElementsList.item(configIndex) != null
			// && childElementsList.item(configIndex)
			// .getNodeType() == Node.ELEMENT_NODE) {
			// String nodeName = childElementsList.item(
			// configIndex).getNodeName();
			// if (nodeName != null) {
			// if (nodeName.contains(XML_CONFIG)) {
			// Element config = (Element) childElementsList
			// .item(configIndex);
			// if (!(CONFIG_SAVED_VALUE
			// .equalsIgnoreCase(config
			// .getAttribute(STR_NAME)
			// .toString()))) {
			// screenQuestion
			// .removeChild(childElementsList
			// .item(configIndex));
			// }
			//
			// } else if ((nodeName
			// .contains(XML_PERMITTED_VALUE))
			// || (nodeName
			// .contains(XML_IPE_QUESTION_REFERENCE))) {
			// Node node = childElementsList
			// .item(configIndex);
			// removeAll(node, Node.ELEMENT_NODE, nodeName);
			// }
			// }
			// }
			// }
			// }
			// }

			xmlDocument.normalize();
		}
		return xmlDocument;
	}
/**
 * @param xmlDocument contains xmlDocument
 * @param expr contains expression
 * @throws XPathExpressionException which throws exception
 */
	private static void removeChildElements(Document xmlDocument,
			XPathExpression expr) throws XPathExpressionException {
		Object transaction = expr.evaluate(xmlDocument, XPathConstants.NODESET);
		if (transaction != null) {
			NodeList childElements = (NodeList) transaction;
			final int childLen = childElements.getLength();
			for (int childIndex = 0; childIndex < childLen; childIndex++) {
				Node childNode = childElements.item(childIndex);
				if (childNode != null) {
					Node parentNode = childNode.getParentNode();
					if (parentNode != null) {
						parentNode.removeChild(childNode);
					}
				}
			}
		}
	}

	// /**
	// * @param appAreaElement
	// */
	// private static void removeInvalidElement(Element appAreaElement,
	// String elementName) {
	// if (appAreaElement.getElementsByTagNameNS(STR_ASTERISK, elementName) !=
	// null) {
	// Node invalidNode = appAreaElement.getElementsByTagNameNS(
	// STR_ASTERISK, elementName).item(0);
	// if (invalidNode != null) {
	// appAreaElement.removeChild(invalidNode);
	// }
	// }
	// }
	//
	// /**
	// * @param appAreaElement
	// */
	// private static void removeErrorElement(Element appAreaElement) {
	// Node errorsNode = appAreaElement.getElementsByTagNameNS(STR_ASTERISK,
	// ERRORS).item(0);
	// if (errorsNode != null) {
	// appAreaElement.removeChild(errorsNode);
	// }
	// }

	// public static void removeAll(Node node, int nodeType, String name) {
	// if (node.getNodeType() == nodeType
	// && (name == null || node.getNodeName().equals(name))) {
	// node.getParentNode().removeChild(node);
	// } else {
	// // Visit the children
	// NodeList list = node.getChildNodes();
	// for (int i = 0; i < list.getLength(); i++) {
	// removeAll(list.item(i), nodeType, name);
	// }
	// }
	// }
	/**
	 * @param element contains element
	 */

	public static void formatElement(Element element) {
		if (element != null) {
			Node child;
			Node next = (Node) element.getFirstChild();
			while ((child = next) != null) {
				next = child.getNextSibling();
				if (child.getNodeType() == Node.TEXT_NODE) {
					String trimmed = child.getNodeValue().trim();
					if (trimmed.length() == 0) {
						element.removeChild(child);
					}
				} else if (child.getNodeType() == Node.ELEMENT_NODE) {
					formatElement((Element) child);
				}
			}
		}
	}
/**
 * @param xmlDocument contains xml document with blank elements
 * @return xmlDocument
 */
	public static Document removeblankElements(Document xmlDocument) {
		Element appAreaElement = (Element) xmlDocument.getElementsByTagNameNS(
				STR_ASTERISK, XML_APPLICATION_AREA).item(0);
		formatElement(appAreaElement);
		Element dataAreaElement = (Element) xmlDocument.getElementsByTagNameNS(
				STR_ASTERISK, XML_DATA_AREA).item(0);
		formatElement(dataAreaElement);
		Element formElement = (Element) xmlDocument.getElementsByTagNameNS(
				STR_ASTERISK, XML_FORM).item(0);
		formatElement(formElement);
		NodeList questionGroupList = xmlDocument.getElementsByTagNameNS(
				STR_ASTERISK, XML_QUESTION_GROUP);
		final int noOfQuestionGroups = questionGroupList.getLength();
		for (int questionGroupListIndex = 0; questionGroupListIndex < noOfQuestionGroups; questionGroupListIndex++) {
			Element questionGroupElement = (Element) questionGroupList
					.item(questionGroupListIndex);
			formatElement(questionGroupElement);
		}
		NodeList screenQuestionList = xmlDocument.getElementsByTagNameNS(
				STR_ASTERISK, XML_SCREEN_QUESTION);
		final int noOfScreenQues = screenQuestionList.getLength();
		for (int questionIndex = 0; questionIndex < noOfScreenQues; questionIndex++) {
			Element screenQuestion = (Element) screenQuestionList
					.item(questionIndex);
			formatElement(screenQuestion);

		}

		return xmlDocument;
	}

}
