package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBAdditionalText2Widget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the HTML AdditionalText2 tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:13 AM
 */
public class HsxSBAdditionalText2Renderer extends HsxSBHTMLBaseRenderer
	implements HsxSBUIBuilderConstants {
	/**
	 * Default constructor.
	 */
    public HsxSBAdditionalText2Renderer() {
	// TODO Auto-generated constructor stub
    }

    /**
     * This method will return the String for rendering the HTML AdditionalText2
     * tag.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBAdditionalText2Widget additionaltext2Widget = (HsxSBAdditionalText2Widget) widget;
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(additionaltext2Widget.getWidgetInfo().getStyle());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);
	if (null != additionaltext2Widget.getWidgetInfo().getAdditionalText2()) {
	    contentBuilder.append(additionaltext2Widget.getWidgetInfo()
		    .getAdditionalText2());
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
