package com.hiscox.sb.framework.core.widgets.primitive;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class holds the characteristics of Iframe tag. and implements the basic
 * functionality defined by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 21-Sep-2012 6:27:04 AM
 */
public class HsxSBIframeWidget extends HsxSBBaseWidget {

	/**
	 *
	 */
	private static final long serialVersionUID = -4077646267796925065L;

	public HsxSBIframeWidget() {

		super();
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 * @param widgetInfo contains info of the widget
	 */
	public HsxSBIframeWidget(String id, String name, HsxSBWidgetInfo widgetInfo) {

		super(id, name, widgetInfo);
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 */
	public HsxSBIframeWidget(String id, String name) {

		super(id, name);
	}

	/**
	 * Method to create widget by setting widget info, Style hint etc.
	 *
	 * @param widgetInfo contains info of the widget
	 */
	@Override
	public void createWidget(HsxSBWidgetInfo widgetInfo) {

		this.setWidgetInfo(widgetInfo);
		this.getWidgetInfo().setStyle("");
	}

}
