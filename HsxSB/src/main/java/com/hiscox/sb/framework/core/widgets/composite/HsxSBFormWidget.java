package com.hiscox.sb.framework.core.widgets.composite;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.HsxSBCompositeWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class carries the characteristics to construct a FORM tag, and also
 * holds the list of primitive widgets which would come under the FORM tag.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:18 AM
 */
public class HsxSBFormWidget extends HsxSBCompositeWidget {

	/**
	 *
	 */
	private static final long serialVersionUID = -2153258810318858408L;
	private List<HsxSBWidget> widgetsList = new ArrayList<HsxSBWidget>();

	public HsxSBFormWidget() {
		super();
	}

	public List<HsxSBWidget> getWidgetsList() {
		return widgetsList;
	}

	public void setWidgetsList(List<HsxSBWidget> widgetsList) {
		this.widgetsList = widgetsList;
	}

	public HsxSBFormWidget(String id, String name) {
		super(id, name);
	}

	public HsxSBFormWidget(String id, String name, HsxSBWidgetInfo widgetInfo) {
		super(id, name, widgetInfo);
	}

	/**
	 * This method adds the widget to the list.
	 *
	 * @param widget contains widget data
	 */
	@Override
	public void addWidget(HsxSBWidget widget) {

		widgetsList.add(widget);
	}

	/**
	 *
	 * @param screenElement contains screen elements
	 */
	@Override
	public void createWidget(Element screenElement) {

	}

	/**
	 * Method to create widget by setting widget info, name etc.
	 *
	 * @param widgetInfo contains info of widgets
	 */

	@Override
	public void createWidget(HsxSBWidgetInfo widgetInfo) {
		this.setWidgetInfo(widgetInfo);
		this.setId(widgetInfo.getId());
		this.setName(widgetInfo.getName());

	}

	/**
	 * This method removes the widget form the list.
	 *
	 * @param widget contains widget data
	 */
	@Override
	public void removeWidget(HsxSBWidget widget) {

		widgetsList.remove(widget);
	}

}
