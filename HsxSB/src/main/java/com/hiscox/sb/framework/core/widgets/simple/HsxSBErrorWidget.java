package com.hiscox.sb.framework.core.widgets.simple;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class holds the characteristics of Error tag. and implements the basic
 * functionality defined by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:14 AM
 */
public class HsxSBErrorWidget extends HsxSBBaseWidget {

	/**
	 *
	 */
	private static final long serialVersionUID = 4224126889416885097L;

	public HsxSBErrorWidget() {

		super();
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 * @param widgetInfo contains info of widget
	 */
	public HsxSBErrorWidget(String id, String name, HsxSBWidgetInfo widgetInfo) {

		super(id, name, widgetInfo);
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 */
	public HsxSBErrorWidget(String id, String name) {

		super(id, name);
	}

	/**
	 * Method to create widget by setting widget info.
	 *
	 * @param widgetInfo contains info of widget
	 */
	@Override
	public void createWidget(HsxSBWidgetInfo widgetInfo) {
		this.setWidgetInfo(widgetInfo);

	}

}
