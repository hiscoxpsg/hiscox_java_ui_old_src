package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBButtonWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the HTML button tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:06 AM
 */
public class HsxSBDefaultNavigationButtonRenderer extends HsxSBHTMLBaseRenderer
	implements
	    HsxSBUIBuilderConstants {
    /**
     * This method will return the String for rendering the HTML Button.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBButtonWidget buttonWidget = (HsxSBButtonWidget) widget;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SUBMIT);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);

	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(CSS_HIDE_CLASS);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(CSS_DEFAULT_NAVIGATION_BUTTON);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(STR_ACTION);
	contentBuilder.append(HTML_UNDERSCORE);
	contentBuilder.append(buttonWidget.getWidgetInfo().getCode());
	contentBuilder.append(HTML_UNDERSCORE);
	contentBuilder.append(STR_BUTTON);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(STR_EMPTY);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLOSE_END_TAG);

	return contentBuilder.toString();
    }

}
