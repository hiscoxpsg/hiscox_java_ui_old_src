package com.hiscox.sb.framework.binding;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class binds the user entered values(request params) to widgets under
 * saved value.
 *
 * @author Cognizant
 * @version 1.0
 */
public class HsxSBUIRequestBinder implements HsxSBUIBuilderConstants {
	/**
	 * This method binds the user entered values(retrieved from request params)
	 * to widgets under saved value.
	 * @param widgetMap Is a Map with Widgets
	 * @param requestValueMap contains requested values
	 */
	public static void bindToWidgets(final Map<String, HsxSBWidget> widgetMap,
			final Map<String, String> requestValueMap) {
		Set<Entry<String, String>> entries = requestValueMap.entrySet();
		Iterator<Map.Entry<String, String>> questionIterator = entries
				.iterator();

		while (questionIterator.hasNext()) {
			Map.Entry<String, String> entry = questionIterator.next();
			String questionName = entry.getKey().toLowerCase();
			String answerValue = entry.getValue();

			// retrieve the question widget
			HsxSBWidget hsxSBWidget = widgetMap.get(questionName);
			String screenQuestionId = (questionName + QUESTION_ID_SUFFIX)
					.toLowerCase();

			if (hsxSBWidget != null && hsxSBWidget.getWidgetInfo() != null) {
				String type = hsxSBWidget.getWidgetInfo().getType();
				String subType = hsxSBWidget.getWidgetInfo().getSubType();
				if (TYPE_DATE.equalsIgnoreCase(type)) {
					String day = null;
					if (!SUBTYPE_MONTHYEAR.equalsIgnoreCase(subType)) {
						day = requestValueMap
								.get(questionName + STR_DAY_SUFFIX);
						// Appending 0 to all the days between 1 to 9
						if (HsxSBUtil.isNotBlank(day)
								&& day.trim().length() == 1) {
							day = STR_ZERO + day;
						}
					} else {
						day = STR_ZERO_ONE;
					}

					String month = requestValueMap.get(questionName
							+ STR_MONTH_SUFFIX);
					String year = requestValueMap.get(questionName
							+ STR_YEAR_SUFFIX);
					answerValue = day + STR_FORWARD_SLASH + month
							+ STR_FORWARD_SLASH + year;
				} else if (TYPE_TABLERADIOENTRY.equalsIgnoreCase(type)) {
					HsxSBWidget refNoWidget = widgetMap
							.get(STR_REFERENCE_NUMBER);
					refNoWidget.getWidgetInfo().setSavedValue(answerValue);
				} else if (SUBTYPE_TELEPHONE.equalsIgnoreCase(hsxSBWidget
						.getWidgetInfo().getSubType())) {

					String telPart1 = requestValueMap.get(questionName
							+ STR_ONE);
					String telPart2 = requestValueMap.get(questionName
							+ STR_TWO);
					String telPart3 = requestValueMap.get(questionName
							+ STR_THREE);
					if (telPart1 != null && telPart2 != null
							&& telPart3 != null) {
						answerValue = telPart1 + telPart2 + telPart3;
					} else {
						answerValue = STR_EMPTY;
					}

				}

			}

			// retrieve the question row widget
			HsxSBWidget screenQuestionWidget = widgetMap.get(screenQuestionId);

			// update the question widget's saved value with the user entered
			// value
			if (hsxSBWidget != null) {
				hsxSBWidget.getWidgetInfo().setSavedValue(answerValue);
				widgetMap.put(questionName, hsxSBWidget);

			}

			// update the question row widget's saved value with the user
			// entered value
			if (screenQuestionWidget != null) {
				screenQuestionWidget.getWidgetInfo().setSavedValue(answerValue);
				widgetMap.put(screenQuestionId, screenQuestionWidget);
			}

		}
		if (widgetMap != null) {
			Set<Entry<String, HsxSBWidget>> widgetSet = widgetMap.entrySet();
			for (Entry<String, HsxSBWidget> widgetEntry : widgetSet) {
				String questionId = widgetEntry.getKey();

				String widgetType = widgetEntry.getValue().getWidgetInfo()
						.getType();
				boolean existingQID = requestValueMap.keySet().contains(
						questionId.toLowerCase());
				if (TYPE_CHECKBOX.equalsIgnoreCase(widgetType) && !existingQID) {
					(widgetMap.get(questionId)).getWidgetInfo().setSavedValue(
							STR_NO);
				}
			}

			if (widgetMap.containsKey(STR_JS_BUTTON_EN_DS_FLAG_ID)) {
				HsxSBWidget hidWidget = widgetMap
						.get(STR_JS_BUTTON_EN_DS_FLAG_ID);
				if (hidWidget != null) {
					hidWidget.getWidgetInfo().setSavedValue(STR_NO);
				}
			}
		}

	}

}
