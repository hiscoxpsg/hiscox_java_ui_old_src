package com.hiscox.sb.framework.core.util.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

public class HsxSBJsHiddenVariablesBean implements HsxSBUIBuilderConstants,
		Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4570078756812202146L;
	private StringBuilder hideUnHideJsString = null;
	private StringBuilder textBoxHideUnHideJsString = null;
	private Map<String, StringBuilder> jsHideUnHideOrConditionMap = null;
	private Map<String, StringBuilder> jsHideUnHideAndOrConditionMap = null;
	private StringBuilder toolTipHiddenJsString = null;
	private StringBuilder calendarHiddenJsString = null;
	private StringBuilder calendarJsObserver = null;
	private StringBuilder jsButtonEnableDisableString = null;
	private StringBuilder jsQuestionEnableDisableString = null;
	private StringBuilder jsActionNames = null;
	private StringBuilder jsIsOneOfAndLogicString = null;
	private StringBuilder jsIsOneOfAndValidationString = null;
	private StringBuilder dependencyValidationString = null;
	private StringBuilder jsAndCaseLogicString = null;
	private StringBuilder textPercentString = null;
	private StringBuilder jsDropdownAndLogicString = null;
	private StringBuilder jsAutoTabLogicString = null;
	private StringBuilder jsPageSubmitLogicString = null;
	private StringBuilder jsFilterByGroupingString = null;
	public StringBuilder getJsFilterByGroupingString() {
		return jsFilterByGroupingString;
	}
/**
 * @param jsFilterByGroupingString for jsFiltering By Grouping.
 */
	public void setJsFilterByGroupingString(StringBuilder jsFilterByGroupingString) {
		this.jsFilterByGroupingString = jsFilterByGroupingString;
	}
/**
 * Getter method.
 * @return jsPageSubmitLogicString
 */
	public StringBuilder getJsPageSubmitLogicString() {
		return jsPageSubmitLogicString;
	}
/**
 * Setter method.
 * @param jsPageSubmitLogicString for js Page Submit Logic String
 */
	public void setJsPageSubmitLogicString(StringBuilder jsPageSubmitLogicString) {
		this.jsPageSubmitLogicString = jsPageSubmitLogicString;
	}
/**
 * Getter method.
 * @return jsAutoTabLogicString
 */
	public StringBuilder getJsAutoTabLogicString() {
		return jsAutoTabLogicString;
	}
/**
 * Setter method.
 * @param jsAutoTabLogicString for js Auto Tab Logic String
 */
	public void setJsAutoTabLogicString(StringBuilder jsAutoTabLogicString) {
		this.jsAutoTabLogicString = jsAutoTabLogicString;
	}
/**
 * Getter method.
 * @return jsDropdownAndLogicString
 */
	public StringBuilder getJsDropdownAndLogicString() {
		return jsDropdownAndLogicString;
	}
/**
 * Setter method.
 * @param jsDropdownAndLogicString for js Drop down And Logic String
 */
	public void setJsDropdownAndLogicString(StringBuilder jsDropdownAndLogicString) {
		this.jsDropdownAndLogicString = jsDropdownAndLogicString;
	}
/**
 * Getter method.
 * @return textPercentString
 */
	public StringBuilder getTextPercentString() {
		return textPercentString;
	}
/**
 * Setter method.
 * @param textPercentString for text Percent String
 */
	public void setTextPercentString(StringBuilder textPercentString) {
		this.textPercentString = textPercentString;
	}
/**
 * Getter method.
 * @return jsAndCaseLogicString
 */
	public StringBuilder getJsAndCaseLogicString() {
		return jsAndCaseLogicString;
	}
/**
 * Setter method.
 * @param jsAndCaseLogicString for js And Case Logic String
 */
	public void setJsAndCaseLogicString(StringBuilder jsAndCaseLogicString) {
		this.jsAndCaseLogicString = jsAndCaseLogicString;
	}
/**
 * Getter method.
 * @return dependencyValidationString
 */
	public StringBuilder getDependencyValidationString() {
		return dependencyValidationString;
	}
/**
 * Setter method.
 * @param dependencyValidationString for dependency Validation String
 */
	public void setDependencyValidationString(
			StringBuilder dependencyValidationString) {
		this.dependencyValidationString = dependencyValidationString;
	}
/**
 * Getter method.
 * @return jsIsOneOfAndValidationString
 */
	public StringBuilder getJsIsOneOfAndValidationString() {
		return jsIsOneOfAndValidationString;
	}
/**
 * Setter method.
 * @param jsIsOneOfAndValidationString for js Is One Of And Validation String
 */
	public void setJsIsOneOfAndValidationString(
			StringBuilder jsIsOneOfAndValidationString) {
		this.jsIsOneOfAndValidationString = jsIsOneOfAndValidationString;
	}
/**
 * Getter method.
 * @return jsIsOneOfAndLogicString
 */
	public StringBuilder getJsIsOneOfAndLogicString() {
		return jsIsOneOfAndLogicString;
	}
/**
 * Setter method.
 * @param jsIsOneOfAndLogicString for js Is One Of And Logic String
 */
	public void setJsIsOneOfAndLogicString(StringBuilder jsIsOneOfAndLogicString) {
		this.jsIsOneOfAndLogicString = jsIsOneOfAndLogicString;
	}
/**
 * Getter method.
 * @return jsActionNames
 */
	public StringBuilder getJsActionNames() {
		return jsActionNames;
	}
/**
 * Setter method.
 * @param jsActionNames for js Action Names
 */
	public void setJsActionNames(StringBuilder jsActionNames) {
		this.jsActionNames = jsActionNames;
	}
/**
 * Getter method.
 * @return hideUnHideJsString
 */
	public StringBuilder getHideUnHideJsString() {
		return hideUnHideJsString;
	}
/**
 * Setter method.
 * @param hideUnHideJsString for hide Un Hide Js String
 */
	public void setHideUnHideJsString(StringBuilder hideUnHideJsString) {
		this.hideUnHideJsString = hideUnHideJsString;
	}
/**
 * Getter method.
 * @return textBoxHideUnHideJsString
 */
	public StringBuilder getTextBoxHideUnHideJsString() {
		return textBoxHideUnHideJsString;
	}
/**
 * Setter method.
 * @param textBoxHideUnHideJsString for text Box Hide Un Hide Js String
 */
	public void setTextBoxHideUnHideJsString(
			StringBuilder textBoxHideUnHideJsString) {
		this.textBoxHideUnHideJsString = textBoxHideUnHideJsString;
	}
/**
 * Getter method.
 * @return jsHideUnHideOrConditionMap
 */
	public Map<String, StringBuilder> getJsHideUnHideOrConditionMap() {
		if (jsHideUnHideOrConditionMap == null) {
			jsHideUnHideOrConditionMap = new HashMap<String, StringBuilder>();
		}

		return jsHideUnHideOrConditionMap;
	}
/**
 * Setter method.
 * @param jsHideUnHideOrConditionMap for jsHideUnHideOrConditionMap
 */
	public void setJsHideUnHideOrConditionMap(
			Map<String, StringBuilder> jsHideUnHideOrConditionMap) {
		this.jsHideUnHideOrConditionMap = jsHideUnHideOrConditionMap;
	}
/**
 * Getter method.
 * @return jsHideUnHideAndOrConditionMap
 */
	public Map<String, StringBuilder> getJsHideUnHideAndOrConditionMap() {
		if (jsHideUnHideAndOrConditionMap == null) {
			jsHideUnHideAndOrConditionMap = new HashMap<String, StringBuilder>();
		}

		return jsHideUnHideAndOrConditionMap;
	}
/**
 * Setter method.
 * @param jsHideUnHideAndOrConditionMap for jsHideUnHideAndOrConditionMap
 */
	public void setJsHideUnHideAndOrConditionMap(
			Map<String, StringBuilder> jsHideUnHideAndOrConditionMap) {
		this.jsHideUnHideAndOrConditionMap = jsHideUnHideAndOrConditionMap;
	}
/**
 * Getter method.
 * @return toolTipHiddenJsString
 */
	public StringBuilder getToolTipHiddenJsString() {
		return toolTipHiddenJsString;
	}
/**
 * Setter method.
 * @param toolTipHiddenJsString for toolTipHiddenJsString
 */
	public void setToolTipHiddenJsString(StringBuilder toolTipHiddenJsString) {
		this.toolTipHiddenJsString = toolTipHiddenJsString;
	}
/**
 * Getter method.
 * @return calendarHiddenJsString
 */
	public StringBuilder getCalendarHiddenJsString() {
		return calendarHiddenJsString;
	}
/**
 * Setter method.
 * @param calendarHiddenJsString for calendarHiddenJsString
 */
	public void setCalendarHiddenJsString(StringBuilder calendarHiddenJsString) {
		this.calendarHiddenJsString = calendarHiddenJsString;
	}
/**
 * Getter method.
 * @return calendarJsObserver
 */
	public StringBuilder getCalendarJsObserver() {
		return calendarJsObserver;
	}
/**
 * Setter method.
 * @param calendarJsObserver for calendarJsObserver
 */
	public void setCalendarJsObserver(StringBuilder calendarJsObserver) {
		this.calendarJsObserver = calendarJsObserver;
	}
/**
 * Getter method.
 * @return jsButtonEnableDisableString
 */
	public StringBuilder getJsButtonEnableDisableString() {
		return jsButtonEnableDisableString;
	}
/**
 * Setter method.
 * @param jsButtonEnableDisableString for jsButtonEnableDisableString
 */
	public void setJsButtonEnableDisableString(
			StringBuilder jsButtonEnableDisableString) {
		this.jsButtonEnableDisableString = jsButtonEnableDisableString;
	}
/**
 * Getter method.
 * @return jsQuestionEnableDisableString
 */
	public StringBuilder getJsQuestionEnableDisableString() {
		return jsQuestionEnableDisableString;
	}
/**
 * Setter method.
 * @param jsQuestionEnableDisableString for jsQuestionEnableDisableString
 */
	public void setJsQuestionEnableDisableString(
			StringBuilder jsQuestionEnableDisableString) {
		this.jsQuestionEnableDisableString = jsQuestionEnableDisableString;
	}

}
