package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBHiddenWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class renders the HTML hidden tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:21 AM
 */
public class HsxSBHiddenRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Hidden.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBHiddenWidget hiddenWidget = (HsxSBHiddenWidget) widget;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_HIDDEN);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	if (null != hiddenWidget.getWidgetInfo().getName()) {
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(hiddenWidget.getWidgetInfo().getName());
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	} else {
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(hiddenWidget.getWidgetInfo().getId());
	    contentBuilder.append(HIDDEN_PREFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	}
	String savedValue = hiddenWidget.getWidgetInfo().getSavedValue();
	if (HsxSBUtil.isBlank(savedValue)) {
		savedValue = STR_EMPTY;
	}
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(hiddenWidget.getWidgetInfo().getId());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(savedValue);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLOSE_END_TAG);

	return contentBuilder.toString();
    }

}
