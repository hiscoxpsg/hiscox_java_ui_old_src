package com.hiscox.sb.framework.core.widgets.simple;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class holds the implementation of the static text functionality defined
 * by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:26 AM
 */
public class HsxSBStaticWidget extends HsxSBBaseWidget {

	/**
	 *
	 */
	private static final long serialVersionUID = -7212026130244873126L;

	public HsxSBStaticWidget() {
		super();
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 * @param widgetInfo contains info of widget
	 */
	public HsxSBStaticWidget(String id, String name, HsxSBWidgetInfo widgetInfo) {
		super(id, name, widgetInfo);
		this.getWidgetInfo().setStyle("");
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 */
	public HsxSBStaticWidget(String id, String name) {
		super(id, name);
	}

	/**
	 * Method to create widget by setting widget info, Style hint etc.
	 *
	 * @param widgetInfo contains info of widget
	 */
	@Override
	public void createWidget(HsxSBWidgetInfo widgetInfo) {
		this.setWidgetInfo(widgetInfo);
		this.getWidgetInfo().setStyle("");
	}

}
