package com.hiscox.sb.framework.core.widgets.vo;

import java.io.Serializable;

/**
 * This class is value holder for the option of of certain widgets like select,
 * radio etc.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:28 AM
 */
public class HsxSBIPEQuestionReference implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -188836871572898007L;
	private String comparison;
	private String questionId;
	private String value;
	private String displayHint;
	public String getDisplayHint() {
	    return displayHint;
	}

	public void setDisplayHint(String displayHint) {
	    this.displayHint = displayHint;
	}

	public String getComparison() {
		return comparison;
	}

	public void setComparison(String comparison) {
		this.comparison = comparison;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public HsxSBIPEQuestionReference() {

	}

}
