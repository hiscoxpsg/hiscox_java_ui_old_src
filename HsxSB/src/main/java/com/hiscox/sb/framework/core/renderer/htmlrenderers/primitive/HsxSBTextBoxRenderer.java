package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Map;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBTextWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML Textbox tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:40 AM
 */
public class HsxSBTextBoxRenderer extends HsxSBHTMLBaseRenderer implements
		HsxSBUIBuilderConstants {
	/**
	 * This method will return the String for rendering the HTML Text box.
	 * @param widget contains widget data
     * @return content
	 */
	@Override
	public String renderContent(final HsxSBWidget widget) {
		StringBuilder contentBuilder = new StringBuilder();

		StringBuffer decPlaces = new StringBuffer();
		DecimalFormatSymbols seperator = new DecimalFormatSymbols();
		DecimalFormat decFormat;

		HsxSBTextWidget textWidget = (HsxSBTextWidget) widget;
		String value = STR_EMPTY;
		String name = STR_EMPTY;

		final String id = textWidget.getWidgetInfo().getId();
		if (null != textWidget.getWidgetInfo().getName()) {
			name = textWidget.getWidgetInfo().getName();
		} else {
			name = id;
		}
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_CONTROL_DIV_STYLE);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_END_TAG);
		Map<String, HsxSBTextManagerVO> textMap = HsxSBUIBuilderControllerImpl.textMap;
		final String subType = textWidget.getWidgetInfo().getSubType();
		if (HTML_CURRENCY.equalsIgnoreCase(subType)
				&& HsxSBUtil.isNotBlank(textWidget.getWidgetInfo()
						.getCurrency())) {
			StringBuilder currTextCodeBldr = new StringBuilder(STR_EMPTY);
			currTextCodeBldr.append(textWidget.getWidgetInfo().getCurrency());
			currTextCodeBldr.append(STR_CURRENCY_KEY);

			final String currTextCode = currTextCodeBldr.toString();

			if (currTextCodeBldr != null && textMap != null
					&& textMap.containsKey(currTextCode)) {

				final String currLabelText = textMap.get(currTextCode)
						.getLabelText();

				if (HsxSBUtil.isNotBlank(currLabelText)) {
					contentBuilder.append(HTML_START_TAG);
					contentBuilder.append(HTML_SPAN);
					contentBuilder.append(HTML_END_TAG);
					contentBuilder.append(currLabelText);
					contentBuilder.append(HTML_START_TAG);
					contentBuilder.append(HTML_FRONT_SLASH);
					contentBuilder.append(HTML_SPAN);
					contentBuilder.append(HTML_END_TAG);
				}
			}
		}
		// else if (HTML_PERCENTAGE.equalsIgnoreCase(textWidget.getWidgetInfo()
		// .getSubType())) {
		// contentBuilder.append(HTML_START_TAG);
		// contentBuilder.append(HTML_SPAN);
		// contentBuilder.append(HTML_END_TAG);
		// contentBuilder.append(HTML_PERCENTAGE_SYMBOL);
		// contentBuilder.append(HTML_START_TAG);
		// contentBuilder.append(HTML_FRONT_SLASH);
		// contentBuilder.append(HTML_SPAN);
		// contentBuilder.append(HTML_END_TAG);
		// }
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_INPUT);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_TYPE);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_TEXT);
		contentBuilder.append(HTML_SINGLE_QUOTE);

		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(textWidget.getWidgetInfo().getStyle());
		contentBuilder.append(HTML_SINGLE_QUOTE);

		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_NAME);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(name);
		contentBuilder.append(HTML_SINGLE_QUOTE);

		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_ID);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(id);
		contentBuilder.append(TEXTBOX_ID_SUFFIX);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_VALUE);
		contentBuilder.append(HTML_EQUALS);
		final String savedValue = textWidget.getWidgetInfo().getSavedValue();
		if (null != savedValue && savedValue.trim().length() > 0) {
			value = savedValue;
		} else if (!textWidget.getWidgetInfo().isErrorIndicator()
				&& null != textWidget.getWidgetInfo().getDefaultValue()) {
			value = textWidget.getWidgetInfo().getDefaultValue();
		}
		contentBuilder.append(HTML_DOUBLE_QUOTE);
		if (!QUESTION_CODE_SECURITYCODE.equalsIgnoreCase(id)
				&& (HTML_CURRENCY.equalsIgnoreCase(subType) || HTML_NUMERIC
						.equalsIgnoreCase(subType))
				&& HsxSBUtil.isNotBlank(value) && HsxSBUtil.isNumeric(value)) {
			value = value.replaceAll(REG_ONLY_NUMERIC, STR_EMPTY);

			String pattern = STR_INTEGER_PATTERN;
			String decimalPlaces = textWidget.getWidgetInfo().getDecimalPlaces();

			if(HsxSBUtil.isNotBlank(decimalPlaces)){
			if (HsxSBUtil.isNumeric(decimalPlaces)
					&& value.contains(STR_DOT)) {
				for (int i = 0; i < Integer.valueOf(decimalPlaces); i++) {
					decPlaces.append(STR_ZERO_PATTERN);
				}
				pattern = STR_DECIMAL_PATTERN;
			} else if (value.contains(STR_DOT)) {
				decPlaces.append(STR_ZERO_PATTERN);
				decPlaces.append(STR_ZERO_PATTERN);
				pattern = STR_DECIMAL_PATTERN;
			}}

			if (!HsxSBUtil.isNotBlank(textWidget.getWidgetInfo()
					.getThousandSeperator())) {
				seperator.setGroupingSeparator(CHAR_COMMA);
				pattern = pattern.replaceAll(STR_COMMA, STR_EMPTY);
				//
			} else {
				seperator.setGroupingSeparator(textWidget.getWidgetInfo()
						.getThousandSeperator().toCharArray()[0]);
			}

			decFormat = new DecimalFormat(decPlaces.insert(0, pattern)
					.toString(), seperator);

			if (value.contains(STR_DOT)) {
				value = decFormat.format(Double.valueOf(value));
			} else {
				value = decFormat.format(Long.valueOf(value));
			}
			if(value.startsWith(STR_DOT)){
				value = value.replace(STR_DOT, STR_ZERO_DOT);
			}

		}
		if (HsxSBUtil.isNotBlank(value) && value.contains(HTML_DOUBLE_QUOTE)) {
			value = value.replaceAll(HTML_DOUBLE_QUOTE, HTML_SINGLE_QUOTE);
		}
		if (SUBTYPE_PERCENTAGE.equalsIgnoreCase(subType)
				&& HsxSBUtil.isNotBlank(value)) {
			value = value.replaceAll(REG_ONLY_NUMERIC_WITHOUT_DECIMALS, STR_EMPTY);
			contentBuilder.append(value);
			contentBuilder.append(HTML_PERCENTAGE_SYMBOL);
		} else {
			contentBuilder.append(value);
		}

		contentBuilder.append(HTML_DOUBLE_QUOTE);
		if (HsxSBUtil.isNotBlank(textWidget.getWidgetInfo().getMaxLength())) {
			contentBuilder.append(HTML_SINGLESPACE);
			contentBuilder.append(HTML_MAXLENGTH);
			contentBuilder.append(HTML_EQUALS);
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(HsxSBUtil.getMaxLength(textWidget
					.getWidgetInfo().getMaxLength()));
			contentBuilder.append(HTML_SINGLE_QUOTE);
		}
		contentBuilder.append(HTML_SINGLESPACE);

		if (HTML_YES.equalsIgnoreCase(textWidget.getWidgetInfo()
				.getDisplayOnly())) {
			contentBuilder.append(HTML_DISABLED);
			contentBuilder.append(HTML_EQUALS);
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(HTML_DISABLED);
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(HTML_SINGLESPACE);
		}
		contentBuilder.append(HTML_CLOSE_END_TAG);

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_END_TAG);

		String includeHelp = HsxSBWidgetUtil.addHelpRenderer(textWidget);

		if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
				.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
				String includeAdditionalText4 = HsxSBWidgetUtil
						.addAdditionalText4Renderer(textWidget);
				contentBuilder.append(includeAdditionalText4);
			}
		

		contentBuilder.append(includeHelp);

		if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
			if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
					.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
				String includeAdditionalText1 = HsxSBWidgetUtil
						.addAdditionalTextOneRenderer(textWidget);
				contentBuilder.append(includeAdditionalText1);
			}
			if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
					.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
				String includeAdditionalText2 = HsxSBWidgetUtil
						.addAdditionalTextTwoRenderer(textWidget);
				contentBuilder.append(includeAdditionalText2);
			}
		}

		if (textWidget.getWidgetInfo().isErrorIndicator()) {
			String includeError = HsxSBWidgetUtil.addErrorRenderer(textWidget);
			contentBuilder.append(includeError);
		}
		return contentBuilder.toString();
	}
}
