package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBEndorsementWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML tags for endorsement.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:03 AM
 */
public class HsxSBEndorsementRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the endorsement tags.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();
	String fileName = STR_EMPTY;
	HsxSBEndorsementWidget endorseWidget = (HsxSBEndorsementWidget) widget;
	HsxSBWidgetInfo widgetInfo = endorseWidget.getWidgetInfo();

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	if (HTML_YES.equalsIgnoreCase(endorseWidget.getWidgetInfo()
		.getDisplayOnly())) {
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	}
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_LABEL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	// If widgetInfo.getShowSize() is present then the link to the PDF
	// should have the file size appended to it.

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_HREF);
	contentBuilder.append(HTML_EQUALS);

	// If widgetInfo.getUseFilename() is present then the descriptive
	// link to the PDF should be the filename, not a CTM lookup value.
	// This will have the effect of overwriting with {questionText} the
	// filename section of {FilePath}.

	contentBuilder.append(HTML_SINGLE_QUOTE);
	if (endorseWidget.getWidgetInfo().getAdditionalText4() != null) {
	    contentBuilder.append(endorseWidget.getWidgetInfo()
		    .getAdditionalText4());
	}

	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);

	contentBuilder.append(HTML_TARGET);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(STR_BLANK);
	contentBuilder.append(HTML_SINGLESPACE);

	//if pop-up is required then unlock this comments
	/*contentBuilder.append(STR_ONCLICK);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(POPUP_CALL);
	contentBuilder.append(HTML_SINGLESPACE);*/

	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(endorseWidget.getWidgetInfo().getFileType());
	contentBuilder.append(HTML_SINGLE_QUOTE);

	contentBuilder.append(HTML_SINGLESPACE);

	contentBuilder.append(HTML_TITLE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(endorseWidget.getWidgetInfo().getUseFilename());
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	if (null != widgetInfo.getUseFilename()
		&& HsxSBUtil.isBlank(widgetInfo.getAdditionalText4()) == false) {
	    String[] label = null;

	    if (widgetInfo.getAdditionalText4().contains(STR_FORWARD_SLASH)) {
		label = widgetInfo.getAdditionalText4()
			.split(STR_FORWARD_SLASH);
		if (label.length > 1) {
		    fileName = label[label.length - 1];
		}
	    } else {
		fileName = widgetInfo.getAdditionalText4();
	    }
	    if (fileName.contains(HTML_DOT)) {
		label = fileName.split("\\.");
		if (label.length > 1) {
		    fileName = label[label.length - 1];
		}
	    }
	    contentBuilder.append(fileName);
	} else if (null != widgetInfo.getLabel()) {
	    contentBuilder.append(endorseWidget.getWidgetInfo().getLabel());
	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_END_TAG);

	if (null != widgetInfo.endorsementMap) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_OL);
	    contentBuilder.append(HTML_END_TAG);

	    Collection<String> keys;
	    keys = widgetInfo.endorsementMap.keySet();
	    List<String> list = new ArrayList<String>();
	    list.addAll(keys);
	    //Iterator<String> iterate = list.iterator();
	    for (String key : list) {
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_LI);
		contentBuilder.append(HTML_END_TAG);

		contentBuilder.append(widgetInfo.endorsementMap.get(key));

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_LI);
		contentBuilder.append(HTML_END_TAG);
	    }

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_OL);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);
	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	String includeHelp = HsxSBWidgetUtil.addHelpRenderer(endorseWidget);

	contentBuilder.append(includeHelp);

	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
		String includeAdditionalText1 = HsxSBWidgetUtil
			.addAdditionalTextOneRenderer(endorseWidget);
		contentBuilder.append(includeAdditionalText1);
	    }
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
		String includeAdditionalText2 = HsxSBWidgetUtil
			.addAdditionalTextTwoRenderer(endorseWidget);
		contentBuilder.append(includeAdditionalText2);
	    }
	}

	return contentBuilder.toString();
    }
}
