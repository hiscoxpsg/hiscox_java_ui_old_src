package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBTextAreaWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML TextArea tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:38 AM
 */
public class HsxSBTextAreaRenderer extends HsxSBHTMLCompositeRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Text area.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBTextAreaWidget textAreaWidget = (HsxSBTextAreaWidget) widget;
	String value = STR_EMPTY;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CONTROL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_TEXTAREA);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);

	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textAreaWidget.getWidgetInfo().getId());
	contentBuilder.append(HTML_SINGLE_QUOTE);

	if (HsxSBUtil.isNotBlank(textAreaWidget.getWidgetInfo()
			.getMaxLength())) {
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_MAXLENGTH);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HsxSBUtil.getMaxLength(textAreaWidget.getWidgetInfo()
		.getMaxLength()));
	contentBuilder.append(HTML_SINGLE_QUOTE);
	}

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textAreaWidget.getWidgetInfo().getId());
	contentBuilder.append(TEXTBOX_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ROWS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(Integer.valueOf(textAreaWidget.getWidgetInfo()
		.getRows()));
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_COLS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(Integer.valueOf(textAreaWidget.getWidgetInfo()
		.getColumns()));
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);

	if (null != textAreaWidget.getWidgetInfo().getSavedValue()
		&& textAreaWidget.getWidgetInfo().getSavedValue().trim()
			.length() > 0) {
	    value = textAreaWidget.getWidgetInfo().getSavedValue();
	} else if (null != textAreaWidget.getWidgetInfo().getDefaultValue()
		&& textAreaWidget.getWidgetInfo().getDefaultValue().trim()
			.length() > 0) {
	    value = textAreaWidget.getWidgetInfo().getDefaultValue();
	}

	if (HTML_YES.equalsIgnoreCase(textAreaWidget.getWidgetInfo()
		.getDisplayOnly())) {
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	}
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(textAreaWidget.getWidgetInfo().getStyle());
	contentBuilder.append(HTML_SINGLE_QUOTE);

	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(value);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_TEXTAREA);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	String includeHelp = HsxSBWidgetUtil.addHelpRenderer(textAreaWidget);
	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
		String includeAdditionalText4 = HsxSBWidgetUtil
			.addAdditionalText4Renderer(textAreaWidget);
		contentBuilder.append(includeAdditionalText4);
	   
	}

	contentBuilder.append(includeHelp);

	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
		String includeAdditionalText1 = HsxSBWidgetUtil
			.addAdditionalTextOneRenderer(textAreaWidget);
		contentBuilder.append(includeAdditionalText1);
	    }
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
		String includeAdditionalText2 = HsxSBWidgetUtil
			.addAdditionalTextTwoRenderer(textAreaWidget);
		contentBuilder.append(includeAdditionalText2);
	    }
	}

	if (textAreaWidget.getWidgetInfo().isErrorIndicator()) {
	    String includeError = HsxSBWidgetUtil.addErrorRenderer(textAreaWidget);
	    contentBuilder.append(includeError);
	}
	return contentBuilder.toString();
    }

}
