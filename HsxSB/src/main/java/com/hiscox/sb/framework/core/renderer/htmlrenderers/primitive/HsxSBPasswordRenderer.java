package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBPasswordWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML textbox tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:40 AM
 */
public class HsxSBPasswordRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Password tag.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBPasswordWidget passwordWidget = (HsxSBPasswordWidget) widget;
	String value = STR_EMPTY;
	String name = STR_EMPTY;
	passwordWidget.getWidgetInfo().setSavedValue(value);
	if (null != passwordWidget.getWidgetInfo().getName()) {
	    name = passwordWidget.getWidgetInfo().getName();
	} else {
	    name = passwordWidget.getWidgetInfo().getId();
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CONTROL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_PASSWORD);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(name);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(passwordWidget.getWidgetInfo().getId());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	if (null != passwordWidget.getWidgetInfo().getSavedValue()
		&& passwordWidget.getWidgetInfo().getSavedValue().trim()
			.length() > 0) {
	    value = passwordWidget.getWidgetInfo().getSavedValue();
	} else if (null != passwordWidget.getWidgetInfo().getDefaultValue()) {
	    value = passwordWidget.getWidgetInfo().getDefaultValue();
	}
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(value);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	if (HTML_YES.equalsIgnoreCase(passwordWidget.getWidgetInfo()
		.getDisplayOnly())) {
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	}
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(passwordWidget.getWidgetInfo().getStyle());
	contentBuilder.append(HTML_SINGLE_QUOTE);

	contentBuilder.append(HTML_CLOSE_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	String includeHelp = HsxSBWidgetUtil.addHelpRenderer(passwordWidget);
	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
		String includeAdditionalText4 = HsxSBWidgetUtil
			.addAdditionalText4Renderer(passwordWidget);
		contentBuilder.append(includeAdditionalText4);
	    }
	

	contentBuilder.append(includeHelp);

	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
		String includeAdditionalText1 = HsxSBWidgetUtil
			.addAdditionalTextOneRenderer(passwordWidget);
		contentBuilder.append(includeAdditionalText1);
	    }
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
		String includeAdditionalText2 = HsxSBWidgetUtil
			.addAdditionalTextTwoRenderer(passwordWidget);
		contentBuilder.append(includeAdditionalText2);
	    }
	}

	if (passwordWidget.getWidgetInfo().isErrorIndicator()) {
	    String includeError = HsxSBWidgetUtil.addErrorRenderer(passwordWidget);
	    contentBuilder.append(includeError);
	}
	return contentBuilder.toString();
    }

}
