package com.hiscox.sb.framework.core.widgets.simple;


 import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
 /**
  * This class holds the characteristics of Label tag. and implements the basic
  * functionality defined by the widget interface to create a widget.
  *
  * @author Cognizant
  * @version 1.0
  * created 25-Mar-2010 6:27:26 AM
  */
 public class HsxSBAdditionalText4Widget extends HsxSBBaseWidget {

 	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public HsxSBAdditionalText4Widget() {
 		super();
 	}

 	/**
 	 *
 	 * @param id contains id
 	 * @param name contains name
 	 * @param widgetInfo contains info of widget
 	 */
 	public HsxSBAdditionalText4Widget(String id, String name, HsxSBWidgetInfo widgetInfo) {
 		super(id, name, widgetInfo);
 		this.getWidgetInfo().setStyle("");
 	}

 	/**
 	 *
 	 * @param id contains id
 	 * @param name contains name
 	 */
 	public HsxSBAdditionalText4Widget(String id, String name) {
 		super(id, name);
 	}

 	/**
 	 * Method to create widget by setting widget info, Style hint etc.
 	 *
 	 * @param widgetInfo contains info of widget
 	 */
 	@Override
 	public void createWidget(HsxSBWidgetInfo widgetInfo) {
 		this.setWidgetInfo(widgetInfo);
 		this.getWidgetInfo().setStyle(HsxSBUIBuilderConstants.HTML_ADDITIONAL_TEXT_DIV_STYLE);
 	}
 }
