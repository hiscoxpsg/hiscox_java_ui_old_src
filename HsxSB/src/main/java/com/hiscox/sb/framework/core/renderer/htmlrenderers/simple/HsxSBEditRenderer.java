package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBEditWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the Edit tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:13 AM
 */
public class HsxSBEditRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /** Default constructor. */
    public HsxSBEditRenderer() {
	super();
    }

    /**
     * This method will return the String for rendering the HTML Help tag.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBEditWidget editWidget = (HsxSBEditWidget) widget;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);

	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_EDITICON_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);

	contentBuilder.append(HTML_END_TAG);
	if (editWidget.getWidgetInfo().getAdditionalText4() != null) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_ANCHOR);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_TITLE);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_ANCHOREDIT);
	    contentBuilder.append(HTML_SINGLE_QUOTE);

	   /* contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_TABINDEX);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(editWidget.getWidgetInfo().getOrder());
		contentBuilder.append(HTML_SINGLE_QUOTE);*/
		contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_HREF);
	    contentBuilder.append(HTML_EQUALS);

	    contentBuilder.append(editWidget.getWidgetInfo()
		    .getAdditionalText4());

	    contentBuilder.append(HTML_END_TAG);

	    if (editWidget.getWidgetInfo().getAdditionalText5() != null) {

		contentBuilder.append(editWidget.getWidgetInfo()
			.getAdditionalText5());

	    }
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_INPUT);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_TYPE);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_HIDDEN);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_VALUE);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(editWidget.getWidgetInfo()
		    .getAdditionalText4());
	    contentBuilder.append(HTML_SINGLE_QUOTE);

	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_ID);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(editWidget.getWidgetInfo().getId());
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_CLOSE_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_ANCHOR);
	    contentBuilder.append(HTML_END_TAG);
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
