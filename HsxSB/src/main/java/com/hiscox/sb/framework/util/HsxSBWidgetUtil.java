package com.hiscox.sb.framework.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.util.HsxSBStringUtils;
import com.hiscox.sb.framework.core.util.HsxSBWidgetFactory;
import com.hiscox.sb.framework.core.util.bean.HsxSBJsHiddenVariablesBean;
import com.hiscox.sb.framework.core.widgets.HsxSBCompositeWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBFormWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionGroupWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBHiddenWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBIPEQuestionReference;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class is used to accomodate all the utility methods requires for the
 * screen builder.
 *
 * @author Cognizant
 *
 */
public class HsxSBWidgetUtil implements HsxSBUIBuilderConstants {
	/**
	 * changes the style to unhide the question.
	 *
	 * @param screenRowWidget
	 * @param style
	 */
	public static void unHideQuestionWidget(HsxSBWidget screenRowWidget,
			String style) {
		if (style != null) {
			style = removehideUnHideStyles(style);
			style = style.trim() + STR_SINGLE_SPACE + CSS_SHOW_CLASS;
			screenRowWidget.getWidgetInfo().setStyleHint(style);
		}
	}

	/**
	 * changes the style to hide the question.
	 *
	 * @param screenRowWidget
	 * @param style
	 */
	public static void hideQuestionWidget(HsxSBWidget screenRowWidget,
			String style) {
		if (style != null) {
			style = removehideUnHideStyles(style);
			style = style.trim() + STR_SINGLE_SPACE + CSS_HIDE_CLASS;
			screenRowWidget.getWidgetInfo().setStyleHint(style);
		}
	}

	/**
	 * removes the hide / unhides styles.
	 *
	 * @param style
	 * @return style
	 */
	public static String removehideUnHideStyles(String style) {
		if (HsxSBUtil.isNotBlank(style)) {
			style = style.replaceAll(CSS_SHOW_CLASS, STR_EMPTY);
			style = style.replaceAll(CSS_HIDE_CLASS, STR_EMPTY);
		}
		return style;
	}

	/**
	 * Processes the question group widget.
	 *
	 * @param formlistWidget
	 * @param uiSavedValueMap
	 * @param uiWidgetDepencymap
	 * @param widgetMap
	 */
	public static void processQuestionGroupWidget(HsxSBWidget formlistWidget,
			Map<String, String> uiWidgetDepencymap,
			Map<String, HsxSBWidget> widgetMap) {
		HsxSBQuestionGroupWidget questionGroupWidget = (HsxSBQuestionGroupWidget) formlistWidget;
		// printWidgetinfo(questionGroupWidget.getWidgetInfo());
		processIpeQuestionReferences(uiWidgetDepencymap, widgetMap,
				questionGroupWidget);
		ArrayList<HsxSBWidget> questionGroupList = (ArrayList<HsxSBWidget>) questionGroupWidget
				.getWidgetsList();
		for (Iterator<HsxSBWidget> questionGroupListIterator = questionGroupList
				.iterator(); questionGroupListIterator.hasNext();) {
			HsxSBWidget questionGroupListWidget = (HsxSBWidget) questionGroupListIterator
					.next();
			if (TYPE_QUESTION.equalsIgnoreCase(questionGroupListWidget
					.getWidgetInfo().getType())) {
				HsxSBQuestionWidget questionWidget = (HsxSBQuestionWidget) questionGroupListWidget;
				ArrayList<HsxSBWidget> questionList = (ArrayList<HsxSBWidget>) questionWidget
						.getWidgetsList();
				for (Iterator<HsxSBWidget> questionListIterator = questionList
						.iterator(); questionListIterator.hasNext();) {
					HsxSBWidget questionListWidget = (HsxSBWidget) questionListIterator
							.next();
					if (TYPE_QUESTION_GROUP.equalsIgnoreCase(questionListWidget
							.getWidgetInfo().getType())) {
						processQuestionGroupWidget(questionListWidget,
								uiWidgetDepencymap, widgetMap);
					} else {
						processIpeQuestionReferences(uiWidgetDepencymap,
								widgetMap, questionListWidget);

					}
				}
			}

		}
	}

	private static void processIpeQuestionReferences(
			Map<String, String> uiWidgetDepencymap,
			Map<String, HsxSBWidget> widgetMap, HsxSBWidget questionListWidget) {
		if (questionListWidget != null
				&& questionListWidget.getWidgetInfo() != null
				&& widgetMap != null) {
			String questionId = questionListWidget.getWidgetInfo().getId();
			HsxSBWidget widget = null;
			if (widgetMap != null && widgetMap.containsKey(questionId)) {
				widget = widgetMap.get(questionId);
				String widgetSavedValue = widget.getWidgetInfo()
						.getSavedValue();
				questionListWidget.getWidgetInfo().setSavedValue(
						widgetSavedValue);
			}
			int ipeQuestionReferenceListsize = 0;
			if (questionListWidget.getWidgetInfo()
					.getIpeQuestionReferenceList() != null) {
				ipeQuestionReferenceListsize = questionListWidget
						.getWidgetInfo().getIpeQuestionReferenceList().size();
			}
			if (ipeQuestionReferenceListsize > 0) {
				List<HsxSBIPEQuestionReference> ipeQuestionReferenceList = questionListWidget
						.getWidgetInfo().getIpeQuestionReferenceList();
				for (Iterator<HsxSBIPEQuestionReference> iterator = ipeQuestionReferenceList
						.iterator(); iterator.hasNext();) {
					HsxSBIPEQuestionReference ipeQuestionReference = (HsxSBIPEQuestionReference) iterator
							.next();
					String parentQuestionId = ipeQuestionReference
							.getQuestionId().toLowerCase();
					String triggerValue = ipeQuestionReference.getValue();
					String comparission = ipeQuestionReference.getComparison();
					String childQuestionId = questionListWidget.getWidgetInfo()
							.getId();
					String displayHint = ipeQuestionReference.getDisplayHint();
					if (HsxSBUtil.isBlank(displayHint)) {
						displayHint = STR_BLANK_VALUE.toLowerCase();
					} else {
						displayHint = displayHint.toLowerCase();
					}
					if (STR_EQUALS.equalsIgnoreCase(comparission) && widgetMap != null
							&& widgetMap.containsKey(parentQuestionId)) {
						// System.out.println("Parent Question Id :"
						// + parentQuestionId);
						// System.out
						// .println("widgetMap.containsKey(parentQuestionId) :"
						// + widgetMap
						// .containsKey(parentQuestionId));
							if (childQuestionId != null
									&& childQuestionId.trim().length() > 0) {
								childQuestionId = childQuestionId.toLowerCase();
								String parenetQuestionAnswerValue = widgetMap
										.get(parentQuestionId).getWidgetInfo()
										.getSavedValue();
								if (STR_BLANK_VALUE
										.equalsIgnoreCase(displayHint)) {
									// System.out
									// .println("parenetQuestionAnswerValue :"
									// + parenetQuestionAnswerValue);
									// System.out.println("triggerValue :"
									// + triggerValue);
									if (triggerValue != null
											&& triggerValue
													.equalsIgnoreCase(parenetQuestionAnswerValue)) {

										if (!uiWidgetDepencymap
												.containsKey(childQuestionId)) {
											uiWidgetDepencymap
													.put(
															childQuestionId
																	+ QUESTION_ID_SUFFIX,
															STR_SHOW);
										}

									} else {
										uiWidgetDepencymap.put(childQuestionId
												+ QUESTION_ID_SUFFIX, STR_HIDE);

									}
								} else {
									if (triggerValue != null
											&& triggerValue
													.equalsIgnoreCase(parenetQuestionAnswerValue)) {
										childQuestionId = childQuestionId
												.toLowerCase();
										boolean childQuestionHiddenFlag = false;
										if (uiWidgetDepencymap
												.containsKey(childQuestionId)) {
											String dispHint = uiWidgetDepencymap
													.get(childQuestionId);

											if (STR_HIDE
													.equalsIgnoreCase(dispHint)) {
												childQuestionHiddenFlag = true;
											}
										}
										if (!childQuestionHiddenFlag) {
											uiWidgetDepencymap.put(
													childQuestionId,
													displayHint.toLowerCase());
										}
									}
								}
							}
					}
				}

			}
		}
	}

	/**
	 * This method creates a map which will be used to hide/unhide the questions.
	 *
	 * @param widget
	 * @param uiWidgetDepencymap
	 * @param widgetMap
	 * @return uiWidgetDepencymap
	 */
	public static Map<String, String> createUIWidgetHideUnHideMap(
			HsxSBWidget widget, Map<String, String> uiWidgetDepencymap,
			Map<String, HsxSBWidget> widgetMap) {
		HsxSBScreenWidget screenWidget = (HsxSBScreenWidget) widget;
		if (screenWidget != null) {
			ArrayList<HsxSBWidget> screenWidgetList = (ArrayList<HsxSBWidget>) screenWidget
					.getWidgetsList();
			for (Iterator<HsxSBWidget> screenWidgetListIterator = screenWidgetList
					.iterator(); screenWidgetListIterator.hasNext();) {
				HsxSBWidget hsxSBWidget = screenWidgetListIterator.next();
				HsxSBFormWidget formWidget = (HsxSBFormWidget) hsxSBWidget;
				ArrayList<HsxSBWidget> wigetList = (ArrayList<HsxSBWidget>) formWidget
						.getWidgetsList();
				for (Iterator<HsxSBWidget> iterator = wigetList.iterator(); iterator
						.hasNext();) {
					HsxSBWidget formlistWidget = (HsxSBWidget) iterator.next();
					if (TYPE_QUESTION_GROUP.equalsIgnoreCase(formlistWidget
							.getWidgetInfo().getType())) {
						processQuestionGroupWidget(formlistWidget,
								uiWidgetDepencymap, widgetMap);
					}

				}

			}
		}
		// Collection c = uiSavedValueMap.keySet();
		// for (Iterator<String> iterator = c.iterator(); iterator.hasNext();)
		// {
		// String questionCode = iterator.next();
		// String answeredValue = "";
		// if(uiSavedValueMap.containsKey(questionCode)){
		// answeredValue = uiSavedValueMap
		// .get(questionCode);
		// }
		//
		// }
		return uiWidgetDepencymap;
	}

	/**
	 * This method add the help widget to questionRowWidget.
	 *
	 * @param controlWidget
	 * @return renderered
	 */
	public static String addHelpRenderer(HsxSBWidget controlWidget) {
		String helpText = controlWidget.getWidgetInfo().getHelpText();
		String helpTitle = controlWidget.getWidgetInfo().getHelpTitle();
		if (HsxSBUtil.isNotBlank(helpText)) {
			HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();
			String helpContentText = helpText;

			helpContentText = replaceContent(controlWidget, helpContentText);
			widgetInfo.setSavedValue(helpContentText);
			String childWidgetType = TYPE_HELP;
			String id = controlWidget.getWidgetInfo().getId();
			String name = controlWidget.getWidgetInfo().getId();
			widgetInfo.setHelpText(helpContentText);
			widgetInfo.setHelpTitle(helpTitle);
			widgetInfo.setCode(controlWidget.getWidgetInfo().getCode());
			widgetInfo.setOrder(controlWidget.getWidgetInfo().getOrder());
			// To display help text link instead of help icon
			if (HsxSBUtil.isNotBlank(controlWidget.getWidgetInfo()
					.getAdditionalText6())) {
				widgetInfo.setAdditionalText6(controlWidget.getWidgetInfo()
						.getAdditionalText6());
			}
			// updateTooltipHiddenString(id,helpText);
			id = id + STR_HELP_ID;
			name = name + STR_HELP_NAME;

			HsxSBWidget helpWidget = processChildQuestion(widgetInfo,
					childWidgetType, id, name);

			HsxSBRenderer renderer = HsxSBHTMLRendererFactory
					.getRendererInstance(helpWidget.getWidgetInfo().getType());
			String renderered = null;
			if (renderer != null) {
				renderered = renderer.renderContent(helpWidget);
			}
			return renderered;
		} else {
			return STR_EMPTY;
		}
	}

	/**
	 * @param id
	 */
	public static void updateTooltipHiddenString(String id, String helpText) {
		if (HsxSBUtil.isNotBlank(helpText)) {
			HsxSBUIBuilderControllerImpl.toolTipHiddenJsString.append(id);
			HsxSBUIBuilderControllerImpl.toolTipHiddenJsString
					.append(STR_TILDE);
		}
	}

	/**
	 * This method add the addtionaltext1 widget to questionRowWidget.
	 *
	 * @param questionRowWidgetInfo
	 * @param questionRowDivWidget
	 * @param additionalText1
	 * @return renderered
	 */
	public static String addAdditionalTextOneRenderer(HsxSBWidget controlWidget) {
		String additionalText1 = controlWidget.getWidgetInfo()
				.getAdditionalText1();
		if (HsxSBUtil.isNotBlank(additionalText1)) {
			HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();
			widgetInfo.setSavedValue(additionalText1);
			String childWidgetType = TYPE_ADDITIONALTEXT;

			String addtionalText = additionalText1;

			addtionalText = replaceContent(controlWidget, addtionalText);

			widgetInfo.setSavedValue(addtionalText);
			widgetInfo.setAdditionalText1(addtionalText);
			String id = controlWidget.getWidgetInfo().getId().concat(
					ADDITIONAL1_ID_SUFFIX);
			String name = controlWidget.getWidgetInfo().getId().concat(
					ADDITIONAL1_NAME_SUFFIX);
			widgetInfo.setId(id);
			widgetInfo.setName(name);

			widgetInfo.setStyle(HTML_ADDITIONAL_TEXT_DIV_STYLE);
			if (SUBTYPE_TWOCOLUMN.equalsIgnoreCase(controlWidget
					.getWidgetInfo().getSubType())) {
				widgetInfo.setStyle(HTML_CONTROL_DIV_STYLE);
			}

			HsxSBWidget additionalOneWidget = processChildQuestion(widgetInfo,
					childWidgetType, id, name);

			HsxSBRenderer renderer = HsxSBHTMLRendererFactory
					.getRendererInstance(additionalOneWidget.getWidgetInfo()
							.getType());
			String renderered = STR_EMPTY;
			if (renderer != null) {
				renderered = renderer.renderContent(additionalOneWidget);
			}
			return renderered;
		} else {
			return STR_EMPTY;
		}
	}

	/**
	 * This method add the addtionaltext2 widget to questionRowWidget.
	 *
	 * @param controlWidget
	 * @return renderered
	 */
	public static String addAdditionalTextTwoRenderer(HsxSBWidget controlWidget) {
		String additionalText2 = controlWidget.getWidgetInfo()
				.getAdditionalText2();
		if (HsxSBUtil.isNotBlank(additionalText2)) {
			HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();
			widgetInfo.setSavedValue(additionalText2);
			String childWidgetType = TYPE_ADDITIONALTEXT;
			String id = controlWidget.getWidgetInfo().getId().concat(
					ADDITIONAL2_ID_SUFFIX);
			String name = controlWidget.getWidgetInfo().getId().concat(
					ADDITIONAL2_NAME_SUFFIX);
			widgetInfo.setStyle(HTML_ADDITIONAL_TEXT_TWO_DIV_STYLE);
			widgetInfo.setAdditionalText2(additionalText2);
			widgetInfo.setId(id);
			widgetInfo.setName(name);

			HsxSBWidget additionalTwoWidget = processChildQuestion(widgetInfo,
					childWidgetType, id, name);

			HsxSBRenderer renderer = HsxSBHTMLRendererFactory
					.getRendererInstance(additionalTwoWidget.getWidgetInfo()
							.getType());
			String renderered = STR_EMPTY;
			if (renderer != null) {
				renderered = renderer.renderContent(additionalTwoWidget);
			}
			return renderered;
		} else {
			return STR_EMPTY;
		}

	}

	/**
	 * This method add the edit widget to questionRowWidget.
	 *
	 * @param controlWidget
	 * @return renderered
	 */
	public static String addAdditionalText4Renderer(HsxSBWidget controlWidget) {
		String additionalText4 = controlWidget.getWidgetInfo()
				.getAdditionalText4();
		String additionalText5 = controlWidget.getWidgetInfo()
				.getAdditionalText5();
		String editTitle = controlWidget.getWidgetInfo().getEditTitle();

		if (HsxSBUtil.isNotBlank(additionalText4)) {
			HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();
			widgetInfo.setSavedValue(additionalText4);
			String childWidgetType = TYPE_EDIT;
			String id = controlWidget.getWidgetInfo().getId().concat(
					EDIT_ID_SUFFIX);
			String name = controlWidget.getWidgetInfo().getId().concat(
					EDIT_NAME_SUFFIX);
			widgetInfo.setAdditionalText4(additionalText4);
			widgetInfo.setAdditionalText5(additionalText5);
			widgetInfo.setEditTitle(editTitle);
			widgetInfo.setOrder(controlWidget.getWidgetInfo().getOrder());

			HsxSBWidget additionalFourWidget = processChildQuestion(widgetInfo,
					childWidgetType, id, name);

			HsxSBRenderer renderer = HsxSBHTMLRendererFactory
					.getRendererInstance(additionalFourWidget.getWidgetInfo()
							.getType());
			String renderered = STR_EMPTY;
			if (renderer != null) {
				renderered = renderer.renderContent(additionalFourWidget);
			}
			return renderered;
		} else {
			return STR_EMPTY;
		}
	}
/**
 * This method is to add the error.
 * @param controlWidget
 * @return renderered
 */
	public static String addErrorRenderer(HsxSBWidget controlWidget) {
		String errorText = controlWidget.getWidgetInfo().getErrorText();

		if (HsxSBUtil.isNotBlank(errorText)) {
			HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();
			widgetInfo.setSavedValue(errorText);
			String childWidgetType = TYPE_ERROR;
			String id = TYPE_ERROR
					.concat(controlWidget.getWidgetInfo().getId());
			String name = controlWidget.getWidgetInfo().getId().concat(
					TYPE_ERROR);
			widgetInfo.setErrorText(errorText);

			HsxSBWidget errorWidget = processChildQuestion(widgetInfo,
					childWidgetType, id, name);

			HsxSBRenderer renderer = HsxSBHTMLRendererFactory
					.getRendererInstance(errorWidget.getWidgetInfo().getType());
			String renderered = STR_EMPTY;
			if (renderer != null) {
				renderered = renderer.renderContent(errorWidget);
			}
			return renderered;
		} else {
			return STR_EMPTY;
		}

	}

	public static String addLabelRenderer(HsxSBWidget controlWidget) {
		String labelText = controlWidget.getWidgetInfo().getLabel();

		if (HsxSBUtil.isNotBlank(labelText)) {
			HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();
			widgetInfo.setSavedValue(labelText);
			widgetInfo.setLabelForId(controlWidget.getWidgetInfo()
					.getLabelForId());
			widgetInfo
					.setMandatory(controlWidget.getWidgetInfo().isMandatory());
			widgetInfo.setOptionalText(controlWidget.getWidgetInfo()
					.getOptionalText());
			if (STR_NO.equalsIgnoreCase(controlWidget.getWidgetInfo()
					.getShowOptionalText())) {
				widgetInfo.setOptionalText(STR_EMPTY);
			}
			String childWidgetType = TYPE_LABEL;
			String id = TYPE_LABEL
					.concat(controlWidget.getWidgetInfo().getId());
			String name = controlWidget.getWidgetInfo().getId().concat(
					TYPE_LABEL);
			widgetInfo.setLabel(labelText);

			HsxSBWidget labelWidget = processChildQuestion(widgetInfo,
					childWidgetType, id, name);

			HsxSBRenderer renderer = HsxSBHTMLRendererFactory
					.getRendererInstance(labelWidget.getWidgetInfo().getType());
			String renderered = STR_EMPTY;
			if (renderer != null) {
				renderered = renderer.renderContent(labelWidget);
			}
			return renderered;
		} else {
			return STR_EMPTY;
		}

	}

	/**
	 * this method replaces the dynamic content in the text manager content
	 * taking either from the JRules config or the session.
	 *
	 * @param questionRowDivWidget
	 * @param addtionalText
	 * @return addtionalText
	 */
	public static String replaceContent(HsxSBWidget controlWidget,
			String addtionalText) {
		String subString = null;
		Map<String, String> sessionMap = new HashMap<String, String>();
		do {
			subString = HsxSBUtil.getTextBetweenStrings(addtionalText,
					SYMBOL_DUOBLE_HASH, SYMBOL_DUOBLE_TILDE);

			if (subString != null) {
				String replaceString = SYMBOL_DUOBLE_HASH + subString
						+ SYMBOL_DUOBLE_TILDE;
				String numericSubString = subString.replaceAll(
						REG_ONLY_NUMERIC, STR_EMPTY);
				String nonNumericSubString = subString.replaceAll(
						REG_ONLY_ALPHA, STR_EMPTY);
				String addtionalValue = STR_EMPTY;
				if (STR_REPLACE_ME.equalsIgnoreCase(nonNumericSubString)) {
					String additionalValueKey = CONFIG_ADDITIONAL_VALUE
							+ numericSubString;
					addtionalValue = controlWidget.getWidgetInfo()
							.getAdditionalValueMap().get(additionalValueKey);
					if (addtionalValue != null) {
						if (Pattern.matches(REG_ONLY_NUMERIC_FORMATTED,
								addtionalValue)) {
							addtionalValue = HsxSBStringUtils
									.twoDecimalFormatter(addtionalValue);
						}

						addtionalText = addtionalText.replaceFirst(
								replaceString, addtionalValue);
					} else {
						addtionalText = addtionalText.replaceFirst(
								replaceString, STR_EMPTY);
					}
				} else {
					subString = subString.trim();
					sessionMap = controlWidget.getWidgetInfo().getSessionMap();
					if (null != sessionMap) {
						if (sessionMap.containsKey(subString)) {
							addtionalValue = sessionMap.get(subString);
							if (addtionalValue != null) {
								addtionalText = addtionalText.replaceFirst(
										replaceString, addtionalValue);
							} else {
								addtionalText = addtionalText.replaceFirst(
										replaceString, STR_EMPTY);
							}
						} else {
							addtionalText = addtionalText.replaceFirst(
									replaceString, STR_EMPTY);
						}
					}
				}

			}
		} while (subString != null);
		return addtionalText;
	}

	/**
	 * This method add the child widget of a question row to questionRowWidget.
	 * @param widgetInfo
	 * @param childWidgetType
	 * @param id
	 * @param name
	 * @return widget
	 */
	private static HsxSBWidget processChildQuestion(HsxSBWidgetInfo widgetInfo,
			String childWidgetType, String id, String name) {
		widgetInfo.setId(id);
		widgetInfo.setName(name);
		widgetInfo.setType(childWidgetType);
		HsxSBWidget widget = HsxSBWidgetFactory.getWidgetInstance(
				childWidgetType, id, name);
		widget.createWidget(widgetInfo);
		return widget;

	}

	/**
	 * To add the hidden field in the page that are required for the JS. Eg. For
	 * tooltips,...
	 *
	 * @param formWidget
	 */
	public static void createJsHiddenVariables(HsxSBCompositeWidget formWidget,
			Map<String, HsxSBWidget> widgetMap,
			HsxSBJsHiddenVariablesBean jsHiddenVariablesBean) {

		// ActionNames String
		StringBuilder jsActionNames = jsHiddenVariablesBean.getJsActionNames();
		if (jsActionNames != null
				&& HsxSBUtil.isNotBlank(jsActionNames.toString())) {
			HsxSBWidgetInfo jsActionNamesWidgetInfo = new HsxSBWidgetInfo();
			jsActionNamesWidgetInfo.setId(STR_JS_HIDDEN_ACTION_NAMES_ID);
			jsActionNamesWidgetInfo.setType(TYPE_HIDDEN);
			if (jsActionNames != null && jsActionNames.length() > 0) {
				int jsActionNamesLength = jsActionNames.length();
				jsActionNames = jsActionNames
						.deleteCharAt(jsActionNamesLength - 1);
				jsActionNamesWidgetInfo.setSavedValue(jsActionNames.toString());
				// System.out.println("ActionNames :" +
				// jsActionNames.toString());
			}

			HsxSBHiddenWidget jsActionNamesWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jsActionNamesWidget.createWidget(jsActionNamesWidgetInfo);
			formWidget.addWidget(jsActionNamesWidget);

		}
		// Text Percent String
		StringBuilder jsTextPercentNames = jsHiddenVariablesBean.getTextPercentString();
		if (jsTextPercentNames != null && HsxSBUtil.isNotBlank(jsTextPercentNames.toString())) {
			HsxSBWidgetInfo jsTextPercentWidgetInfo = new HsxSBWidgetInfo();
			jsTextPercentWidgetInfo.setId(STR_JS_TEXTBOX_PERCENT_STRING_ID);
			jsTextPercentWidgetInfo.setType(TYPE_HIDDEN);
			if (jsTextPercentNames != null && jsTextPercentNames.length() > 0) {
				jsTextPercentWidgetInfo.setSavedValue(jsTextPercentNames.toString().toLowerCase());
			}
			HsxSBHiddenWidget jsTextPercentWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory.getWidgetInstance(HTML_HIDDEN, null, null);
			jsTextPercentWidget.createWidget(jsTextPercentWidgetInfo);
			formWidget.addWidget(jsTextPercentWidget);
		}

		// Js Dependency String Hidden String created here
		StringBuilder hideUnHideJsString = jsHiddenVariablesBean
				.getHideUnHideJsString();
		if (hideUnHideJsString != null
				&& HsxSBUtil.isNotBlank(hideUnHideJsString.toString())) {
			HsxSBWidgetInfo jsDepHiddenWidgetInfo = new HsxSBWidgetInfo();
			jsDepHiddenWidgetInfo.setId(STR_JS_HIDDEN_STRING_ID);
			jsDepHiddenWidgetInfo.setType(TYPE_HIDDEN);
			// System.out.println("Js Hidden String :"
			// + jsHiddenVariablesBean.getHideUnHideJsString());
			jsDepHiddenWidgetInfo.setSavedValue(jsHiddenVariablesBean
					.getHideUnHideJsString().toString());
			HsxSBHiddenWidget jsDepHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jsDepHiddenWidget.createWidget(jsDepHiddenWidgetInfo);
			formWidget.addWidget(jsDepHiddenWidget);
		}

		//JS AutoTab hidden String for telephone numbers generated here
		StringBuilder jsAutoTabLogicString = jsHiddenVariablesBean.getJsAutoTabLogicString();
		if (jsAutoTabLogicString != null && HsxSBUtil.isNotBlank(jsAutoTabLogicString.toString())) {
			HsxSBWidgetInfo jsAutoTabWidgetInfo = new HsxSBWidgetInfo();
			jsAutoTabWidgetInfo.setId(STR_JS_TELEPHONE_AUTOTAB_HIDDEN_STRING_ID);
			jsAutoTabWidgetInfo.setType(TYPE_HIDDEN);
			//System.out.println("Auto Tab Hidden String :"+jsAutoTabLogicString.toString());
			jsAutoTabWidgetInfo.setSavedValue(jsAutoTabLogicString.toString().toLowerCase());
			HsxSBHiddenWidget jsAutoTabWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
			.getWidgetInstance(HTML_HIDDEN, null, null);
			jsAutoTabWidget.createWidget(jsAutoTabWidgetInfo);
			formWidget.addWidget(jsAutoTabWidget);
		}

		//Js Filter By Grouping on Category List in drop down are generated here
		StringBuilder jsFilterByGroupingString = jsHiddenVariablesBean
				.getJsFilterByGroupingString();
		if (jsFilterByGroupingString != null
				&& HsxSBUtil.isNotBlank(jsFilterByGroupingString.toString())) {
			HsxSBWidgetInfo jsFilterByGroupWidgetInfo = new HsxSBWidgetInfo();
			jsFilterByGroupWidgetInfo.setId(STR_JS_FILTER_BY_GROUPING_HIDDEN_STRING_ID);
			jsFilterByGroupWidgetInfo.setType(TYPE_HIDDEN);
			jsFilterByGroupWidgetInfo.setSavedValue(jsFilterByGroupingString.toString().toLowerCase());
			HsxSBHiddenWidget jsFilterGroupWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jsFilterGroupWidget.createWidget(jsFilterByGroupWidgetInfo);
			formWidget.addWidget(jsFilterGroupWidget);
		}


		// Js Hidden String For TextBox created here
		StringBuilder txBoxJsStrBuilder = jsHiddenVariablesBean
				.getTextBoxHideUnHideJsString();
		if (txBoxJsStrBuilder != null
				&& HsxSBUtil.isNotBlank(txBoxJsStrBuilder.toString())) {
			HsxSBWidgetInfo jsTxtBoxHiddenWidgetInfo = new HsxSBWidgetInfo();
			jsTxtBoxHiddenWidgetInfo.setId(STR_JS_TEXTBOX_HIDDEN_STRING_ID);
			jsTxtBoxHiddenWidgetInfo.setType(TYPE_HIDDEN);
			if (txBoxJsStrBuilder != null && txBoxJsStrBuilder.length() > 0) {
				int txBoxJsStrLength = txBoxJsStrBuilder.length();
				txBoxJsStrBuilder = txBoxJsStrBuilder
						.deleteCharAt(txBoxJsStrLength - 1);
				jsTxtBoxHiddenWidgetInfo.setSavedValue(txBoxJsStrBuilder
						.toString().toLowerCase());
			}
			HsxSBHiddenWidget jstxtBoxHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jstxtBoxHiddenWidget.createWidget(jsTxtBoxHiddenWidgetInfo);
			formWidget.addWidget(jstxtBoxHiddenWidget);
			if (txBoxJsStrBuilder != null
					&& HsxSBUtil.isNotBlank(txBoxJsStrBuilder.toString())) {
				widgetMap.put(STR_JS_TEXTBOX_HIDDEN_STRING_ID,
						jstxtBoxHiddenWidget);
			}
		}
		// Js Button Enable Disable Hidden String created here
		StringBuilder buttonJsStrBuilder = jsHiddenVariablesBean
				.getJsButtonEnableDisableString();
		if (buttonJsStrBuilder != null
				&& HsxSBUtil.isNotBlank(buttonJsStrBuilder.toString())) {
			HsxSBWidgetInfo jsButtEnbleDsbleWidgetInfo = new HsxSBWidgetInfo();
			jsButtEnbleDsbleWidgetInfo
					.setId(STR_JS_BUTTON_ENABLE_DISABLE_STRING_ID);
			jsButtEnbleDsbleWidgetInfo.setType(TYPE_HIDDEN);

			if (buttonJsStrBuilder != null && buttonJsStrBuilder.length() > 0) {
				int buttonStringLength = buttonJsStrBuilder.length();
				buttonJsStrBuilder = buttonJsStrBuilder
						.deleteCharAt(buttonStringLength - 1);
				jsButtEnbleDsbleWidgetInfo.setSavedValue(buttonJsStrBuilder
						.toString().toLowerCase());
			}
			HsxSBHiddenWidget jsButtEnbleDsbleHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jsButtEnbleDsbleHiddenWidget
					.createWidget(jsButtEnbleDsbleWidgetInfo);
			formWidget.addWidget(jsButtEnbleDsbleHiddenWidget);
		}
		// Js Question Enable Disable Hidden String created here
		StringBuilder questionJsStrBuilder = jsHiddenVariablesBean
				.getJsQuestionEnableDisableString();
		if (questionJsStrBuilder != null
				&& HsxSBUtil.isNotBlank(questionJsStrBuilder.toString())) {
			HsxSBWidgetInfo jsQuestEnbleDsbleWidgetInfo = new HsxSBWidgetInfo();
			jsQuestEnbleDsbleWidgetInfo
					.setId(STR_JS_QUESTION_ENABLE_DISABLE_STRING_ID);
			jsQuestEnbleDsbleWidgetInfo.setType(TYPE_HIDDEN);

			if (questionJsStrBuilder != null
					&& questionJsStrBuilder.length() > 0) {
				int questionStringLength = questionJsStrBuilder.length();
				questionJsStrBuilder = questionJsStrBuilder
						.deleteCharAt(questionStringLength - 1);
				jsQuestEnbleDsbleWidgetInfo.setSavedValue(questionJsStrBuilder
						.toString());
			}

			HsxSBHiddenWidget jsQuestEnbleDsbleHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jsQuestEnbleDsbleHiddenWidget
					.createWidget(jsQuestEnbleDsbleWidgetInfo);
			formWidget.addWidget(jsQuestEnbleDsbleHiddenWidget);
		}

		// Tooltip Hidden String Created here

		StringBuilder toolTipJsStrBuilder = HsxSBUIBuilderControllerImpl.toolTipHiddenJsString;
		// if (toolTipJsStrBuilder != null
		// && HsxSBUtil.isNotBlank(toolTipJsStrBuilder.toString()))
		// {
		HsxSBWidgetInfo jsToolTipHiddenWidgetInfo = new HsxSBWidgetInfo();
		HsxSBHiddenWidget jsToolTipHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
				.getWidgetInstance(HTML_HIDDEN, null, null);
		jsToolTipHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
				.getWidgetInstance(HTML_HIDDEN, null, null);

		if (toolTipJsStrBuilder != null && toolTipJsStrBuilder.length() > 0) {
			int toolTipStringLength = toolTipJsStrBuilder.length();
			toolTipJsStrBuilder = toolTipJsStrBuilder
					.deleteCharAt(toolTipStringLength - 1);
			jsToolTipHiddenWidgetInfo.setSavedValue(toolTipJsStrBuilder
					.toString());
		}

		jsToolTipHiddenWidgetInfo.setId(STR_JS_TOOLTIP_HIDDEN_STRING_ID);
		jsToolTipHiddenWidgetInfo.setType(TYPE_HIDDEN);
		jsToolTipHiddenWidget.createWidget(jsToolTipHiddenWidgetInfo);
		formWidget.addWidget(jsToolTipHiddenWidget);
		HsxSBUIBuilderControllerImpl.toolTipHiddenJsString = new StringBuilder(
				STR_EMPTY);
		// }

		// Resources Project ContextPath Hidden String Created here
		HsxSBWidgetInfo contextPathHiddenWidgetInfo = new HsxSBWidgetInfo();
		contextPathHiddenWidgetInfo.setId(STR_CONTEXT_PATH);
		contextPathHiddenWidgetInfo.setType(TYPE_HIDDEN);
		contextPathHiddenWidgetInfo.setSavedValue(STR_FORWARD_SLASH
				+ STR_RESOURCE_CONTEXT_PATH_VALUE);

		HsxSBHiddenWidget contextPathHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
				.getWidgetInstance(HTML_HIDDEN, null, null);
		contextPathHiddenWidget.createWidget(contextPathHiddenWidgetInfo);
		formWidget.addWidget(contextPathHiddenWidget);

		// Calendar Hiiden String Created here

		StringBuilder calJsStrBuilder = HsxSBUIBuilderControllerImpl.calendarHiddenJsString;
		// if (calJsStrBuilder != null
		// && HsxSBUtil.isNotBlank(calJsStrBuilder.toString()))
		// {
		HsxSBWidgetInfo calHiddenWidgetInfo = new HsxSBWidgetInfo();
		calHiddenWidgetInfo.setId(STR_CALENDAR_JS_HIDDEN_STRING_ID);
		calHiddenWidgetInfo.setType(TYPE_HIDDEN);

		if (calJsStrBuilder != null && calJsStrBuilder.length() > 0) {
			int calendarStringLength = calJsStrBuilder.length();
			calJsStrBuilder = calJsStrBuilder
					.deleteCharAt(calendarStringLength - 1);
			calHiddenWidgetInfo.setSavedValue(calJsStrBuilder.toString());
		}

		HsxSBHiddenWidget calHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
				.getWidgetInstance(HTML_HIDDEN, null, null);
		calHiddenWidget.createWidget(calHiddenWidgetInfo);
		formWidget.addWidget(calHiddenWidget);
		HsxSBUIBuilderControllerImpl.calendarHiddenJsString = new StringBuilder(
				STR_EMPTY);
		// }

		// Button Enable Disable Flag variable is created
		HsxSBWidgetInfo jsButtonEnDsFlagWidgetInfo = new HsxSBWidgetInfo();
		jsButtonEnDsFlagWidgetInfo.setId(STR_JS_BUTTON_EN_DS_FLAG_ID);
		jsButtonEnDsFlagWidgetInfo.setType(TYPE_HIDDEN);
		jsButtonEnDsFlagWidgetInfo.setSavedValue(STR_YES.toUpperCase());

		HsxSBHiddenWidget jsButtonEnDsFlagWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
				.getWidgetInstance(HTML_HIDDEN, null, null);
		jsButtonEnDsFlagWidget.createWidget(jsButtonEnDsFlagWidgetInfo);
		formWidget.addWidget(jsButtonEnDsFlagWidget);
		widgetMap.put(STR_JS_BUTTON_EN_DS_FLAG_ID, jsButtonEnDsFlagWidget);

		// is js enabled variable is created
		HsxSBWidgetInfo jsEnHiddenWidgetInfo = new HsxSBWidgetInfo();
		jsEnHiddenWidgetInfo.setId(STR_IS_JS_ENABLED);
		jsEnHiddenWidgetInfo.setName(STR_IS_JS_ENABLED);
		jsEnHiddenWidgetInfo.setType(TYPE_HIDDEN);
		jsEnHiddenWidgetInfo.setSavedValue(STR_NO.toUpperCase());

		HsxSBHiddenWidget jsEnHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
				.getWidgetInstance(HTML_HIDDEN, null, null);
		jsEnHiddenWidget.createWidget(jsEnHiddenWidgetInfo);
		formWidget.addWidget(jsEnHiddenWidget);

		// Js Hide UnHide Or Condition Hidden Variable

		if (jsHiddenVariablesBean.getJsHideUnHideOrConditionMap() != null
				&& !jsHiddenVariablesBean.getJsHideUnHideOrConditionMap()
						.isEmpty()) {
			StringBuilder jsHideUnHideOrCondString = new StringBuilder(
					STR_EMPTY);
			Map<String, StringBuilder> jsHideUnHideOrConditionMap = jsHiddenVariablesBean
					.getJsHideUnHideOrConditionMap();
			// Collection<String> c = jsHideUnHideOrConditionMap.keySet();
			Set<Entry<String, StringBuilder>> conditionSet = jsHideUnHideOrConditionMap
					.entrySet();

			for (Entry<String, StringBuilder> entry : conditionSet) {
				String key = entry.getKey();
				StringBuilder value = entry.getValue();
				if (value != null && value.length() > 1) {
					int valueLength = value.length();
					value = value.delete(valueLength - 2, valueLength);
				}
				jsHideUnHideOrCondString.append(key);
				jsHideUnHideOrCondString.append(QUESTION_ID_SUFFIX);
				jsHideUnHideOrCondString.append(STR_HYPHEN);
				jsHideUnHideOrCondString.append(GREATER_THAN);
				jsHideUnHideOrCondString.append(value);
			}
			HsxSBWidgetInfo jsHideUnHideOrCondWidgetInfo = new HsxSBWidgetInfo();
			jsHideUnHideOrCondWidgetInfo
					.setId(STR_JS_HIDE_UNHIDE_OR_CONDITION_STRING_ID);
			jsHideUnHideOrCondWidgetInfo.setType(TYPE_HIDDEN);
			jsHideUnHideOrCondWidgetInfo.setSavedValue(jsHideUnHideOrCondString
					.toString().toLowerCase());

			HsxSBHiddenWidget jsHideUnHideOrCondWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jsHideUnHideOrCondWidget.createWidget(jsHideUnHideOrCondWidgetInfo);
			formWidget.addWidget(jsHideUnHideOrCondWidget);
		}

		// Js Hide UnHide And Or Condition Hidden Variable

		if (jsHiddenVariablesBean.getJsHideUnHideAndOrConditionMap() != null
				&& !jsHiddenVariablesBean.getJsHideUnHideAndOrConditionMap()
						.isEmpty()) {
			StringBuilder jsHideUnHideAndOrCondString = new StringBuilder(
					STR_EMPTY);
			Map<String, StringBuilder> jsHideUnHideAndOrConditionMap = jsHiddenVariablesBean
					.getJsHideUnHideAndOrConditionMap();
			// Collection<String> c = jsHideUnHideAndOrConditionMap.keySet();

			Set<Entry<String, StringBuilder>> conditionSet = jsHideUnHideAndOrConditionMap
					.entrySet();

			for (Entry<String, StringBuilder> entry : conditionSet) {
				String key = entry.getKey();
				StringBuilder value = entry.getValue();
				jsHideUnHideAndOrCondString.append(key);
				jsHideUnHideAndOrCondString.append(QUESTION_ID_SUFFIX);
				jsHideUnHideAndOrCondString.append(STR_HYPHEN);
				jsHideUnHideAndOrCondString.append(GREATER_THAN);
				jsHideUnHideAndOrCondString.append(value);
			}
			HsxSBWidgetInfo jsHideUnHideAndOrCondWidgetInfo = new HsxSBWidgetInfo();
			jsHideUnHideAndOrCondWidgetInfo
					.setId(STR_JS_HIDE_UNHIDE_AND_OR_CONDITION_STRING_ID);
			jsHideUnHideAndOrCondWidgetInfo.setType(TYPE_HIDDEN);
			jsHideUnHideAndOrCondWidgetInfo
					.setSavedValue(jsHideUnHideAndOrCondString.toString()
							.toLowerCase());

			HsxSBHiddenWidget jsHideUnHideAndOrCondWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jsHideUnHideAndOrCondWidget
					.createWidget(jsHideUnHideAndOrCondWidgetInfo);
			formWidget.addWidget(jsHideUnHideAndOrCondWidget);
		}

		// Is One Of and logic

		StringBuilder jsIsOneOfAndLogicString = jsHiddenVariablesBean
				.getJsIsOneOfAndLogicString();
		if (jsIsOneOfAndLogicString != null
				&& HsxSBUtil.isNotBlank(jsIsOneOfAndLogicString.toString())) {
			HsxSBWidgetInfo jsIsOneOfAndLogicWidgetInfo = new HsxSBWidgetInfo();
			jsIsOneOfAndLogicWidgetInfo
					.setId(STR_JS_ISONEOF_AND_LOGIC_STRING_ID);
			jsIsOneOfAndLogicWidgetInfo.setType(TYPE_HIDDEN);
			if (jsIsOneOfAndLogicString.length() > 0) {

				jsIsOneOfAndLogicWidgetInfo
						.setSavedValue(jsIsOneOfAndLogicString.toString());
				// System.out.println("ActionNames :" +
				// jsActionNames.toString());
			}

			HsxSBHiddenWidget jsIsOneOfAndLogicWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jsIsOneOfAndLogicWidget.createWidget(jsIsOneOfAndLogicWidgetInfo);
			formWidget.addWidget(jsIsOneOfAndLogicWidget);
		}


		//Drop down And logic string

		StringBuilder jsDropdownAndLogicString = jsHiddenVariablesBean
				.getJsDropdownAndLogicString();

		if (jsDropdownAndLogicString != null
				&& HsxSBUtil.isNotBlank(jsDropdownAndLogicString.toString())) {
			HsxSBWidgetInfo jsDropdownAndLogicWidgetInfo = new HsxSBWidgetInfo();
			jsDropdownAndLogicWidgetInfo
					.setId(STR_JS_DROPDOWN_AND_LOGIC_STRING_ID);
			jsDropdownAndLogicWidgetInfo.setType(TYPE_HIDDEN);

			if (jsDropdownAndLogicString.length() > 0) {
				jsDropdownAndLogicWidgetInfo
						.setSavedValue(jsDropdownAndLogicString.toString());
			}

			HsxSBHiddenWidget jsDropdownAndLogicWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			jsDropdownAndLogicWidget.createWidget(jsDropdownAndLogicWidgetInfo);
			formWidget.addWidget(jsDropdownAndLogicWidget);
		}


		// Is One Of And Logic Validation String

		StringBuilder isOneOfAndValidationString = jsHiddenVariablesBean
				.getJsIsOneOfAndValidationString();
		//System.out.println("isOneOfAndValidationString :"+isOneOfAndValidationString);
		if (isOneOfAndValidationString != null
				&& HsxSBUtil.isNotBlank(isOneOfAndValidationString.toString())) {
			HsxSBWidgetInfo isOneOfAndValidationWidgetInfo = new HsxSBWidgetInfo();
			isOneOfAndValidationWidgetInfo
					.setId(STR_JS_ISONEOF_AND_VALIDATION_STRING_ID);
			isOneOfAndValidationWidgetInfo.setType(TYPE_HIDDEN);
			isOneOfAndValidationWidgetInfo
					.setSavedValue(isOneOfAndValidationString.toString()
							.toLowerCase());
			HsxSBHiddenWidget isOneOfAndValidationHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			isOneOfAndValidationHiddenWidget
					.createWidget(isOneOfAndValidationWidgetInfo);
			if (isOneOfAndValidationString != null
					&& HsxSBUtil.isNotBlank(isOneOfAndValidationString
							.toString())) {
				// System.out.println("ISONEOFAND VALIDATION WIDGET CREATED");
				widgetMap.put(STR_JS_ISONEOF_AND_VALIDATION_STRING_ID,
						isOneOfAndValidationHiddenWidget);
			}
		}
		isOneOfAndValidationString = null;


		// And Logic case Validation String

		StringBuilder jsAndCaseLogicString = jsHiddenVariablesBean.getJsAndCaseLogicString();
		if (jsAndCaseLogicString != null && HsxSBUtil.isNotBlank(jsAndCaseLogicString.toString())) {
			HsxSBWidgetInfo jsAndCaseWidgetInfo = new HsxSBWidgetInfo();
			jsAndCaseWidgetInfo.setId(STR_JS_AND_LOGIC_STRING_ID);
			jsAndCaseWidgetInfo.setType(TYPE_HIDDEN);
			jsAndCaseWidgetInfo.setSavedValue(jsAndCaseLogicString.toString().toLowerCase());
			HsxSBHiddenWidget jsAndCaseHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
			.getWidgetInstance(HTML_HIDDEN, null, null);
			jsAndCaseHiddenWidget.createWidget(jsAndCaseWidgetInfo);
			formWidget.addWidget(jsAndCaseHiddenWidget);

		}


		// Dependency Validation String
//		System.out.println("Check this out :----->  "
//				+ jsHiddenVariablesBean.getDependencyValidationString());
		StringBuilder dependencyValidationString = jsHiddenVariablesBean.getDependencyValidationString();
		if (dependencyValidationString != null
				&& HsxSBUtil.isNotBlank(dependencyValidationString.toString())) {

			HsxSBWidgetInfo dependencyValidationWidgetInfo = new HsxSBWidgetInfo();
			dependencyValidationWidgetInfo
					.setId(STR_JS_DEPENDENCY_VALIDATION_STRING_ID);
			dependencyValidationWidgetInfo.setType(TYPE_HIDDEN);
			dependencyValidationWidgetInfo
					.setSavedValue(dependencyValidationString.toString());
			HsxSBHiddenWidget dependencyValidationHiddenWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
					.getWidgetInstance(HTML_HIDDEN, null, null);
			dependencyValidationHiddenWidget
					.createWidget(dependencyValidationWidgetInfo);
			widgetMap.put(STR_JS_DEPENDENCY_VALIDATION_STRING_ID,
					dependencyValidationHiddenWidget);

		}

		//JS PageSubmit hidden String for change option generated here
		StringBuilder jsPageRefreshLogicString = jsHiddenVariablesBean.getJsPageSubmitLogicString();
		if (jsPageRefreshLogicString != null && HsxSBUtil.isNotBlank(jsPageRefreshLogicString.toString())) {
			HsxSBWidgetInfo jsPageRefreshWidgetInfo = new HsxSBWidgetInfo();
			jsPageRefreshWidgetInfo.setId(STR_JS_CHANGE_PAGESUBMIT_HIDDEN_STRING_ID);
			jsPageRefreshWidgetInfo.setType(TYPE_HIDDEN);
			jsPageRefreshWidgetInfo.setSavedValue(jsPageRefreshLogicString.toString().toLowerCase());
			HsxSBHiddenWidget jsPageRefreshWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
			.getWidgetInstance(HTML_HIDDEN, null, null);
			jsPageRefreshWidget.createWidget(jsPageRefreshWidgetInfo);
			formWidget.addWidget(jsPageRefreshWidget);

			HsxSBWidgetInfo jsPSQuestionValueInfo = new HsxSBWidgetInfo();
			jsPSQuestionValueInfo.setId(STR_JS_PS_QUESTION_VALUE_STRING_ID);
			jsPSQuestionValueInfo.setType(TYPE_HIDDEN);
			jsPSQuestionValueInfo.setSavedValue(STR_EMPTY);
			HsxSBHiddenWidget jsPSQuestionValueWidget = (HsxSBHiddenWidget) HsxSBWidgetFactory
			.getWidgetInstance(HTML_HIDDEN, null, null);
			jsPSQuestionValueWidget.createWidget(jsPSQuestionValueInfo);
			formWidget.addWidget(jsPSQuestionValueWidget);
		}

	}
}
