package com.hiscox.sb.framework.core.widgets.composite;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.HsxSBCompositeWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class carries the characteristics to construct a generic question row,
 * and also holds the list of primitive widgets which would come under in the
 * corresponding question row.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:30 AM
 */
public class HsxSBQuestionWidget extends HsxSBCompositeWidget {

	/**
	 *
	 */
	private static final long serialVersionUID = -2009595867579382428L;
	private List<HsxSBWidget> widgetsList = new ArrayList<HsxSBWidget>();

	public HsxSBQuestionWidget() {

		super();
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 * @param widgetInfo contains info of widget
	 */
	public HsxSBQuestionWidget(String id, String name,
			HsxSBWidgetInfo widgetInfo) {

		super(id, name, widgetInfo);
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 */
	public HsxSBQuestionWidget(String id, String name) {

		super(id, name);
	}

	/**
	 * This method adds the widget to the list.
	 *
	 * @param widget contains widget data
	 */
	@Override
	public void addWidget(HsxSBWidget widget) {

		widgetsList.add(widget);
	}

	/**
	 *
	 * @param element contains element
	 */
	@Override
	public void createWidget(Element element) {

	}

	/**
	 * Method to create widget by setting widget info, name etc.
	 *
	 * @param widgetInfo contains widget info
	 */
	@Override
	public void createWidget(HsxSBWidgetInfo widgetInfo) {
		this.setWidgetInfo(widgetInfo);
		this.setId(widgetInfo.getId());
		this.setName(widgetInfo.getName());
	}

	public List<HsxSBWidget> getWidgetsList() {
		return widgetsList;
	}

	/**
	 * This method removes the widget form the list.
	 *
	 * @param widget contains widget data
	 */
	@Override
	public void removeWidget(HsxSBWidget widget) {

		widgetsList.remove(widget);
	}

	/**
	 *
	 * @param widgetsList contains list of widgets
	 */
	public void setWidgetsList(List<HsxSBWidget> widgetsList) {

		this.widgetsList = widgetsList;
	}

}
