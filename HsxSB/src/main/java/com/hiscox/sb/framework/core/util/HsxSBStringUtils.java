package com.hiscox.sb.framework.core.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
/**
 * This class a  String utils class.
 * @author Cognizant
 *
 */
public class HsxSBStringUtils {

    /**
     * Remove all spaces from a string.
     *
     * @return returns the input string target, but remove all space characters
     * @param target String used for input
     **/
    public static String removeBlanks(String target) {
	char a;
	StringBuffer source = new StringBuffer(target);
	StringBuffer dest = new StringBuffer();
	int length = source.length();
	for (int i = 0; i < length; i++) {
	    a = source.charAt(i);
	    if (a != ' ') {
		dest.append(a);
	    }
	}
	return dest.toString();
    }

    /**
     * Remove leading from a string.
     *
     * @return returns the input string target, but without the leading space
     *         characters
     * @param target  String used for input
     **/
    public static String removeLeadingBlanks(String target) {
	char a;
	String dest = "";
	int length = target.length();
	for (int i = 0; i < length; i++) {
	    a = target.charAt(i);
	    if (a != ' ') {
		dest = target.substring(i, length);
		break;
	    }
	}
	return dest;
    }

    /**
     * Split the input string on several lines with the required lineLength.
     * Line breaks are inserted in the String whereever it contains a line
     * greater than the line length. The line break is not inserted in the
     * middle of a word.
     *
     * @param theString
     *            String used for input
     * @param lineLength
     *            used for input the length of the line
     * @return the formatted String.
     **/
    public static String lineWrapping(String theString, int lineLength) {
	int currentLineLength = 0;
	StringTokenizer st = new StringTokenizer(theString, " ", true);
	StringBuffer sb = new StringBuffer();

	while (st.hasMoreTokens()) {
	    String currentString = st.nextToken();
	    if (currentString.indexOf("\n") != -1) {
		currentLineLength = 0;
	    } else {
		currentLineLength += currentString.length();
	    }
	    if (currentLineLength < lineLength) {
		sb.append(currentString);
	    } else {
		sb.append("\n");
		sb.append(currentString);
		currentLineLength = currentString.length();
	    }
	}
	return sb.toString();
    }

    /**
     * Convert the first character of each word to upper case, and all
     * characters of any words which contain numbers to upper case.
     * @param input which is a input string
     * @return string
     */
    public static String toTitleCase(String input) {
	if (input == null || input.trim().equals("")) {
	    return input;
	}
	// This method takes a " "-separated string and converts it to Title
	// Case
	StringTokenizer st = new StringTokenizer(input, " ");
	StringBuffer sb = new StringBuffer();
	char zero = "0".charAt(0);
	char nine = "9".charAt(0);
	while (st.hasMoreTokens()) {
	    String token = st.nextToken();
	    // If the token contains numbers it should probably be translated to
	    // upper case
	    // This is typically useful for a postcode contained within an
	    // address, or a
	    // reference number containing letters and numbers.
	    int l = token.length();
	    boolean containsNumbers = false;
	    for (int i = 0; i < l; i++) {
		if (token.charAt(i) >= zero && token.charAt(i) <= nine) {
		    containsNumbers = true;
		    break;
		}
	    }
	    if (containsNumbers) {
		sb.append(token.toUpperCase());
	    } else {
		sb.append(token.substring(0, 1).toUpperCase());
		sb.append(token.substring(1).toLowerCase());
	    }
	    sb.append(" ");
	}
	return sb.toString().substring(0, sb.toString().length() - 1);
    }

    /**
     * This method validates a given string against a given template. The
     * template must be a string containing precise character matches or, more
     * usefully special characters prefixed by a % sign.
     *
     * Currently available special characters are: %n - Any numeric character %c
     * - Any non-numeric character %* - Any character %% - % sign
     *
     * If ignoreSpaces == true, all spaces are stripped from the template and
     * data strings before a comparison is made.
     *
     * e.g. a template of "%n%n-%n%n-%n%n" might be used to validate a UK Bank
     * Sort Code
     * @param data contains data
     * @param template contains templates
     * @param ignoreSpaces input
     * @return true
     */
    public static boolean matchesTemplate(String data, String template,
	    boolean ignoreSpaces) {

	if (data == null || template == null) {
	    return false; // drop out if either string is null.
	}
	String dta, tmp;
	if (ignoreSpaces) {
	    dta = removeBlanks(data);
	    tmp = removeBlanks(template).toLowerCase();
	} else {
	    dta = data;
	    tmp = template.toLowerCase();
	}

	char zero = "0".charAt(0);
	char nine = "9".charAt(0);
	// char space = (new String(" ")).charAt(0);
	char percent = "%".charAt(0);

	int j = 0; // Used as the index of the data string
	int l = tmp.length();
	int m = dta.length();
	for (int i = 0; i < l; i++, j++) {
	    if (j >= m) {
		return false; // in this case the data string is not as long as
	    }
	    // the template
	    // so they cannot possibly match
	    String tmpChar = tmp.substring(i, i + 1);
	    char dtaChar = dta.charAt(j);
	    if (tmpChar.equals("%")) {
		// Next character controls how to validate string
		if (++i >= l) { // (NOTE THAT THIS INCREMENTS THE LOOP COUNTER)

		    return false; // cannot complete if there's no character
		    // after % sign
		}
		tmpChar = tmp.substring(i, i + 1); // get next character after %
		if (tmpChar.equals("n")) { // looking for a numeric character

		    if (dtaChar < zero || dtaChar > nine) {
			return false; // found a non-number
		    }
		} else {
		    if (tmpChar.equals("c")) { // looking for a non-numeric
		    // character

			if (dtaChar >= zero && dtaChar <= nine) {
			    return false; // found a number
			}
		    } else {
			if (tmpChar.equals("%") && dtaChar != percent) {
			    return false; // Expected a % sign - found something
			    // else
			} else {

			    return false; // found a control character we don't
			    // recognize.

			}
		    }
		}
	    } else {
		// Not a special character so just check for an exact match
		if (tmpChar.charAt(0) == dtaChar) {
		    return false; // found non-matching character
		}
	    }
	}
	if (m > j) {
	    return false; // the data string still has unvalidated characters
	}
	// left at the
	// end of it. i.e. it was longer than the template in which case
	// they don't match.
	return true;
    }

    /**
     * Remove all occurences of a character from a string.
     * @param target contains target
     * @param character character input to this method
     * @return dest
     */
    public static String removeCharacter(String target, char character) {
	// A more general version of removeBlanks()
	char a;
	StringBuffer source = new StringBuffer(target);
	StringBuffer dest = new StringBuffer();
	int length = source.length();
	for (int i = 0; i < length; i++) {
	    a = source.charAt(i);
	    if (a != character) {
		dest.append(a);
	    }
	}
	return dest.toString();
    }

    /**
     * Deletes a range of characters from specified start to end positions.
     * @param str input to this method
     * @param start input to this method
     * @param end input to this method
     * @return newStringBuffer
     */
    public static String deleteRange(String str, int start, int end) {

	String __str1, __str2;

	__str1 = str.substring(0, start);
	__str2 = str.substring(end, str.length());

	return new StringBuffer(__str1).append(__str2).toString();
    }

    /**
     * Doubles single quotes within a string and adds surounding single quotes.
     * @return empty
     * @param s input string
     */
    public static String quoted(String s) {
	if (s == null || s.equals("")) {
	    return "''";
	}
	// ...................................................
	String quotedStr = ""; // Return string.
	String cQuote = "'"; // Search pattern.
	int iCharAt; // Position in string.
	String sPrefix, sPostfix; // Portions of string.
	quotedStr = s;
	iCharAt = quotedStr.indexOf(cQuote);
	while (iCharAt >= 0) {
	    sPrefix = quotedStr.substring(0, iCharAt);
	    sPostfix = quotedStr.substring(iCharAt + 1);
	    // Double the quote and look for next one.
	    quotedStr = sPrefix + "''" + sPostfix;
	    iCharAt = quotedStr.indexOf(cQuote, iCharAt + 2);
	}
	// Add surrounding quotes.
	quotedStr = "'" + quotedStr + "'";
	return quotedStr;
    }

    /**
     * Tokenize a String. This splits a String into an array of Strings. The
     * split is made arpound each occurence of a token String.
     * @param input string as input
     * @param token string as input for tokenizing
     * @return tokens
     */
    public static String[] tokenize(String input, String token) {
	StringTokenizer st = new StringTokenizer(input, token);
	int c = st.countTokens();
	String[] tokens = new String[c];
	int i = 0;
	while (st.hasMoreTokens()) {
	    tokens[i++] = st.nextToken();
	}
	return tokens;
    }

    /**
     * Method replaces all occurences of a substring in the input string with a
     * new substring. Note: This method is not case sensitive
     *
     * @param inputString
     *            original string for manipulation
     * @param oldSubstring
     *            substring to be replaced with newSubstring
     * @param newSubstring
     *            new substring to replace oldSubstring
     *
     * @return The newly manipulated string after all replacements
     */
    // -----------------------------------------------------------
    public static String replaceSubstring(String inputString,
	    String oldSubstring, String newSubstring) {
	return replaceSubstring(inputString, oldSubstring, newSubstring, true);
    }

    /**
     * Method replaces all occurences of a substring in the input string with a
     * new substring.
     * @param p_OriginalString original string for manipulation
     * @param p_OldString substring to be replaced with newSubstring
     * @param p_NewString new substring to replace oldSubstring
     * @param p_IgnoreCase true if case should be ignored (this should only be used if
     *            its really required as it slows things down)
     * @return The newly manipulated string after all replacements
     */
    public static String replaceSubstring(String p_OriginalString,
	    String p_OldString, String p_NewString, boolean p_IgnoreCase) {

	// Store the lengths for speed
	int oldStringLength = p_OldString.length();
	int newStringLength = p_NewString.length();

	if (oldStringLength == 0) {
	    // Nowt to replace
	    return p_OriginalString;
	}

	// Find out the indexes of the substrings to replace, this could be
	// factored
	// to use a helper method, but for speed it is left as one

	int oldStringCount = 0;
	int[] oldStringIndexes = null;

	if (p_IgnoreCase) {

	    String lowerCaseString = p_OriginalString.toLowerCase();
	    String lowerCaseOldString = p_OldString.toLowerCase();

	    int oldStringIndex = lowerCaseString.indexOf(lowerCaseOldString);

	    if (oldStringIndex != -1) {
		oldStringIndexes = new int[p_OriginalString.length()];
	    }

	    while (oldStringIndex != -1) {
		oldStringIndexes[oldStringCount] = oldStringIndex;
		oldStringCount++;
		oldStringIndex = lowerCaseString.indexOf(lowerCaseOldString,
			oldStringIndex + oldStringLength);
	    }

	} else {

	    int oldStringIndex = p_OriginalString.indexOf(p_OldString);

	    if (oldStringIndex != -1) {
		oldStringIndexes = new int[p_OriginalString.length()];
	    }

	    while (oldStringIndex != -1) {
		oldStringIndexes[oldStringCount] = oldStringIndex;
		oldStringCount++;
		oldStringIndex = p_OriginalString.indexOf(p_OldString,
			oldStringIndex + oldStringLength);
	    }

	}

	if (oldStringCount == 0) {
	    // Nothing to replace so just return
	    return p_OriginalString;
	}

	// Store the following for speed
	int originalStringLength = p_OriginalString.length();
	int lengthDiff = newStringLength - oldStringLength;

	// Make a new char array for the string we are to return
	char[] returnStringChars = new char[p_OriginalString.length()
		+ lengthDiff * oldStringCount];

	// Copy the original parts we want to retain to their new positions

	char[] originalStringChars = p_OriginalString.toCharArray();
	int srcPosn = 0;

	for (int i = 0; i < oldStringCount; i++) {

	    int dstPosn = srcPosn + (i * lengthDiff);
	    int length = oldStringIndexes[i] - srcPosn;

	    System.arraycopy(originalStringChars, srcPosn, returnStringChars,
		    dstPosn, length);

	    srcPosn = oldStringIndexes[i] + oldStringLength;

	}

	int dstPosn = srcPosn + (oldStringCount * lengthDiff);
	int length = originalStringLength - srcPosn;

	System.arraycopy(originalStringChars, srcPosn, returnStringChars,
		dstPosn, length);

	// Copy in the new strings that replace the old

	char[] newStringChars = p_NewString.toCharArray();

	for (int i = 0; i < oldStringCount; i++) {
	    dstPosn = oldStringIndexes[i] + (i * lengthDiff);
	    System.arraycopy(newStringChars, 0, returnStringChars, dstPosn,
		    newStringLength);
	}

	// Return our spangly new string
	return new String(returnStringChars);

    }

    /**
     * Mimics the way arguments are parsed and passed into a main() method. It
     * takes in a string containing zero or more string arguments delimited by
     * spaces and returns them in an array.
     *
     * Arguments containing spaces can be delimited with double quotes. These
     * quotes are stripped from the returned value.
     *
     * <em>NOTE:</em> Arguments cannot contain double quotes
     *
     * @param p_ArgumentsString
     *            a string containing the arguments to be parsed, e.g. 1 "2 3" 4
     *            5 "6 7"
     * @return an array containing the arguments grouped by double quotes e.g.
     *         "1", "2 3", "4", "5", "6 7"
     */
    static public String[] getArgumentArray(String p_ArgumentsString) {

	char[] argumentChars = p_ArgumentsString.toCharArray();
	boolean inQuotes = false;

	int argCharLen = argumentChars.length;
	for (int i = 0; i < argCharLen; i++) {

	    // Keep track of whether we're in quotes or not and replace quotes
	    // with
	    // character zero to stop them appearing in the output
	    if (argumentChars[i] == '"') {
		inQuotes = !inQuotes;
		argumentChars[i] = '\u0000';
	    }

	    // Replace spaces with character zero if outside quotes
	    if ((argumentChars[i] == ' ') && !inQuotes) {
		argumentChars[i] = '\u0000';
	    }

	}

	// Build up our array of arguments
	StringTokenizer tokenizer = new StringTokenizer(new String(
		argumentChars), "\u0000");
	String[] argumentArray = new String[tokenizer.countTokens()];
	int argArrLen = argumentArray.length;
	for (int i = 0; i < argArrLen; i++) {
	    argumentArray[i] = tokenizer.nextToken();
	}

	return argumentArray;

    }

    /**
     * Method replaces all keys in the input string with the corresponding value
     * in a HashMap.
     *
     * @param inputString original string for manipulation
     * @param tag token to be searched for in order to find the key, format
     *            TAGkeyTAG
     * @param parameters HashMap containing real values against specified keys
     * @return - The newly manipulated string after all replacements of
     *          TAGkeyTAG
     */

    public static String replaceTokens(String inputString, String tag,
	    HashMap<String, String> parameters) {
	StringBuffer message = new StringBuffer(inputString);
	int startReplaceIndex = 0;
	int endReplaceIndex = 0;
	int startKeyIndex = 0;
	int endKeyIndex = 0;
	boolean firstLoop = true;

	int length = inputString.length();
	for (int i = 0; i < length;) {
	    startReplaceIndex = inputString.indexOf(tag, i); // start index of
	    // start tag, i=0
	    // unless key in
	    // previous loop
	    // was invalid
	    if (startReplaceIndex == -1 && firstLoop == true ) { // no start tag
		    break;
	    }
	    startKeyIndex = startReplaceIndex + tag.length(); // start index of
	    // key/end index
	    // of start tag
	    endKeyIndex = inputString.indexOf(tag, startKeyIndex); // end index
	    // of
	    // key/start
	    // index of
	    // end tag
	    if (endKeyIndex == -1) { // no end tag - key cannot be found
		break;
	    }
	    endReplaceIndex = endKeyIndex + tag.length(); // end index of end
	    // tag
	    String key = "";
	    try {
		key = inputString.substring(startKeyIndex, endKeyIndex);
		String replaceWith = null;
		replaceWith = (String) parameters.get(key);
		if (replaceWith != null) {
		    try {
			message.replace(startReplaceIndex, endReplaceIndex,
				replaceWith);
			inputString = message.toString(); // litText changed
		    } catch (StringIndexOutOfBoundsException e) {
			break;
		    }
		} else { // key not found in HashMap
		    i = endReplaceIndex;
		}
	    } catch (StringIndexOutOfBoundsException e) {
		break;
	    }
	    firstLoop = false;
	}

	return message.toString();
    }

    /**
     * Locates, and returns, the first instance of an integer embedded in a
     * string containing other, non-numeric, characters. e.g. "XYZ123A" returns
     * Integer 123. "W8TR$56" returns Integer 8.
     *
     * @param p_Source The string to be searched for an embedded integer.
     * @return The first embedded Integer located. null if the string contains
     *         no integers.
     */
    public static Integer findInteger(String p_Source) {
	StringBuffer result = new StringBuffer();
	char[] chars = p_Source.toCharArray();
	int length = chars.length;

	boolean foundNumberStart = false;
	for (int i = 0; i < length; i++) {
	    if (Character.isDigit(chars[i])) {
		result.append(chars[i]);
		foundNumberStart = true;
	    } else if (!Character.isDigit(chars[i]) && foundNumberStart) {
		break;
	    }
	}

	Integer resultInteger = null;
	if (foundNumberStart) {
	    resultInteger = Integer.valueOf(result.toString());
	}
	return resultInteger;
    }

    /**
     * Method replaces all keys in the input string with the corresponding value
     * in a HashMap.
     * @param p_Query String input
     * @return - The newly manipulated string after all replacements of
     *          TAGkeyTAG
     */
    public static List<String> parseQueryString(String p_Query) {
	List<String> vals = new ArrayList<String>();
	String s = p_Query;
	// extract all the quoted phrases
	while (s.indexOf("\"") != -1) {
	    int i = s.indexOf("\"");
	    int j = s.indexOf("\"", i + 1);
	    if (j != -1) {
		String token = s.substring(i + 1, j);
		token = replaceSubstring(token, "'", "''");
		vals.add(token);
		s = s.substring(0, i) + s.substring(j + 1, s.length());
	    } else {
		s = s.substring(0, i) + s.substring(i + 1, s.length());
	    }
	}
	// now get the other words
	if (s.length() != 0) {
	    StringTokenizer st = new StringTokenizer(s);
	    while (st.hasMoreTokens()) {
		String token = st.nextToken();
		token = replaceSubstring(token, "'", "''");
		vals.add(token);
	    }
	}
	return vals;
    }
/**
 * @param number a number
 * @return a boolean value
 */
    public static boolean isNumeric(String number) {

	if (isEmpty(number)) {
	    return false;
	}

	char[] c = number.toCharArray();

	int points = 0;
	for (int i = 0; i < c.length; i++) {
	    if (!isDigit(c[i]) && c[i] != '.') {
		return false;
	    }
	    if (c[i] == '.') {
		points++;
	    }
	}

	if (points > 1) {
	    return false;
	}

	return true;
    }
/**
 * @param c a character
 * @return boolean value
 */
    public static boolean isDigit(char c) {
	int x = c;

	if ((x >= 48) && (x <= 57)) {
	    return true;
	}

	return false;
    }
/**
 * @param s a string
 * @return a boolean value
 */
    public static boolean isDigit(String s) {
	if (isEmpty(s)) {
	    return false;
	}

	char[] c = s.toCharArray();

	for (int i = 0; i < c.length; i++) {
	    if (!isDigit(c[i])) {
		return false;
	    }
	}

	return true;
    }
/**
 * @param s a string
 * @return a boolean value
 */
    public static boolean isEmpty(String s)  {
	if (s == null) {
	    return true;
	}

	s = s.trim();

	if ((s == null) || (s.equals(""))) {
	    return true;
	}

	return false;
    }
/**
 * @param s a string
 * @return a boolean value
 */
    public static boolean isNotEmpty(String s)  {
	return !isEmpty(s);
    }

    /**
     * Left pads a string with blanks.
     *
     * @param p_String the string
     * @param p_Length the length to pad it to
     * @return - the left padded string
     */
    public static String leftPad(String p_String, int p_Length) {
	if (p_String == null) {
	    return null;
	}

	StringBuffer sb = new StringBuffer(p_Length);

	while ((sb.length() + p_String.length()) < p_Length) {
	    sb.append(" ");
	}

	sb.append(p_String);
	return sb.toString();
    }

    /**
     * Right pads a string with blanks.
     * @param p_String the string
     * @param p_Length the length to pad it to
     * @return - the left padded string
     */
    public static String rightPad(String p_String, int p_Length) {
	StringBuffer s = new StringBuffer(p_String.trim());
	while (s.length() < p_Length) {
	    s.append(" ");
	}
	return s.toString();
    }

    /**
     * formats a number as text.
     * @param p_Number the number
     * @param p_DecimalPlaces the decimal places
     * @return - the text string
     */
    public static String getNumberAsText(double p_Number, int p_DecimalPlaces) {
	// Cant do new BigDecimal(double) as rounding problems occur
	// for some values ie 8.2875 (3dp)
	return getNumberAsText(new BigDecimal("" + p_Number), p_DecimalPlaces);
    }

    /**
     * formats a number as text.
     * @param p_Number the number
     * @param p_DecimalPlaces the decimal places
     * @return - the text string
     */
    public static String getNumberAsText(BigDecimal p_Number,
	    int p_DecimalPlaces) {
	BigDecimal bd = p_Number.setScale(p_DecimalPlaces,
		BigDecimal.ROUND_HALF_UP);
	String n = bd.toString();

	return n;
    }

    /**
     * Creates a string of a certain number of spaces.
     *
     * @param p_NumberOfSpaces the number of spaces
     * @return the string
     */
    public static String spaces(int p_NumberOfSpaces) {
	StringBuffer spaces = new StringBuffer(p_NumberOfSpaces);
	for (int i = 0; i < p_NumberOfSpaces; i++) {
	    spaces.append(" ");
	}
	return spaces.toString();
    }

    /**
     * Creates a string of a certain number of dashes.
     * @param p_Number the number
     * @return the string
     */
    public static String dashes(int p_Number) {
	StringBuffer dashes = new StringBuffer(p_Number);
	for (int i = 0; i < p_Number; i++) {
	    dashes.append("-");
	}
	return dashes.toString();
    }

    /**
     * HTML unescape the supplied string. Currently unescapes <, > and &.
     * @param p_String The string to unescape
     * @return The unescaped string.
     */
//    public static String unEscape(String p_String)
//    {
//	String ret = p_String.replaceAll("&amp;", "&"); // do this first
//	ret = ret.replaceAll("&lt;", "<");
//	ret = ret.replaceAll("&gt;", ">");
//	return ret;
//    }

    /**
     * Safely parse any integer - return default value if invalid.
     * @param p_String input string
     * @param p_Default default input string
     * @return default value
     */
    public static int parseInt(String p_String, int p_Default) {

	if (p_String == null) {
	    return p_Default;
	}

	try {
	    return Integer.parseInt(p_String);
	} catch (NumberFormatException e) {
	    return p_Default;
	}
    } // parseInt

    /**
     * Read Text from file.
     * @param p_Filename input string
     * @return string buffer
     */
    public static String readStringFromFile(String p_Filename) {

	StringBuffer sb = new StringBuffer(10000);

	BufferedReader input = null;
	try {
	    // use buffering, reading one line at a time
	    FileReader fr = new FileReader(p_Filename);
	    input = new BufferedReader(fr);

	    char[] buf = new char[10000];
	    int numRead = 1;

	    while (numRead > 0) {
		numRead = input.read(buf);
		if (numRead > 0) {
		    sb.append(new String(buf, 0, numRead));
		}
	    } // while
	} catch (FileNotFoundException ex) {
		ex.printStackTrace();
	} catch (Exception ex) {
	} finally {
	    try {
		if (input != null) {
		    // flush and close both "input" and its underlying
		    // FileReader
		    input.close();
		}
	    } catch (IOException ex) {
		ex.printStackTrace();
	    }
	}

	return sb.toString();
    } // readStringFromFile

    /**
     * Write String to file.
     * @param p_Str input string
     * @param p_Filename input file name
     */
    public static void writeStringToFile(String p_Str, String p_Filename) {
	BufferedWriter out = null;

	try {
	    // Create file
	    out = new BufferedWriter(new FileWriter(p_Filename));
	    out.write(p_Str);
	    // Close the output stream
	    out.close();
	} catch (Exception e) {
		// Catch exception if any
	} finally {
	    try {
		if (out != null) {
		    // flush and close both "input" and its underlying
		    // FileReader
		    out.close();
		}
	    } catch (IOException ex) {
	    }
	}

    }

    /**
     * This method formats to a decimal value.
     *
     * @param decimalValue input string as a decimal value
     * @return decimalValue
     */

    public static String twoDecimalFormatter(String decimalValue) {
	float floatValue = Float.parseFloat(decimalValue);
	DecimalFormat decFormat = new DecimalFormat("#,###.00");
	decimalValue = decFormat.format(floatValue).toString();
	return decimalValue;
    }
}
