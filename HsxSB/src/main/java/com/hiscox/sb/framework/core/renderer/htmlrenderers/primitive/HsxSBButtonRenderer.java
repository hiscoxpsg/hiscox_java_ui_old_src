package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBButtonWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class renders the HTML button tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:06 AM
 */
public class HsxSBButtonRenderer extends HsxSBHTMLBaseRenderer
	implements
	    HsxSBUIBuilderConstants {
    /**
     * This method will return the String for rendering the HTML Button.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBButtonWidget buttonWidget = (HsxSBButtonWidget) widget;

	if (!XML_SCREEN_QUESTION.equalsIgnoreCase(buttonWidget.getWidgetInfo()
		.getAction())) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_LI);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_BUTTON_LI_STYLE);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(buttonWidget.getWidgetInfo().getStyleHint());
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_ID);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(buttonWidget.getWidgetInfo().getId());
	    contentBuilder.append(QUESTION_ID_SUFFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);
	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_BUTTON_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_BUTTON_SPAN_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	if (null != buttonWidget.getWidgetInfo().getDisplayText()
		&& buttonWidget.getWidgetInfo().getDisplayText()
			.equalsIgnoreCase(STR_PRINT)) {
	    contentBuilder.append(HTML_BUTTON);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(JS_PRINT);
	    contentBuilder.append(HTML_SINGLESPACE);
	} else {
	    contentBuilder.append(HTML_SUBMIT);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	}
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_BUTTON_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(buttonWidget.getWidgetInfo().getId());
	contentBuilder.append(BUTTON_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(STR_ACTION);
	contentBuilder.append(HTML_UNDERSCORE);
	contentBuilder.append(buttonWidget.getWidgetInfo().getCode());
	contentBuilder.append(HTML_UNDERSCORE);
	contentBuilder.append(STR_BUTTON);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);

	if (null != buttonWidget.getWidgetInfo().getDisplayText()) {
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder
		    .append(buttonWidget.getWidgetInfo().getDisplayText());
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	} else {
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(STR_EMPTY);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	}

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TITLE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1)) && HsxSBUtil.isNotBlank(buttonWidget.getWidgetInfo()
					.getAdditionalText1())) {
		    contentBuilder.append(buttonWidget.getWidgetInfo()
			    .getAdditionalText1());
		}
	    
	} else if (HsxSBUtil.isNotBlank(buttonWidget.getWidgetInfo()
		.getDisplayText())) {
	    contentBuilder
		    .append(buttonWidget.getWidgetInfo().getDisplayText());
	} else {
	    contentBuilder.append(buttonWidget.getWidgetInfo().getCode());
	}

	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLOSE_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_BUTTONICON_SPAN_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	if (!XML_SCREEN_QUESTION.equalsIgnoreCase(buttonWidget.getWidgetInfo()
		.getAction())) {

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_LI);
	    contentBuilder.append(HTML_END_TAG);

	}

	return contentBuilder.toString();
    }

}
