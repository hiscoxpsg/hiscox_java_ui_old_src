package com.hiscox.sb.framework.core.widgets.primitive;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class holds the characteristics of Hidden tag. and implements the basic
 * functionality defined by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:21 AM
 */
public class HsxSBHiddenWidget extends HsxSBBaseWidget {

	/**
	 *
	 */
	private static final long serialVersionUID = 7910968215165429396L;

	public HsxSBHiddenWidget() {
		super();
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 * @param widgetInfo contains info of the widget
	 */
	public HsxSBHiddenWidget(String id, String name, HsxSBWidgetInfo widgetInfo) {
		super(id, name, widgetInfo);
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 */
	public HsxSBHiddenWidget(String id, String name) {
		super(id, name);
	}

	/**
	 *
	 * @param id contains id
	 * @param value contains value
	 */
	public void createWidget(String id, String value) {

	}

	/**
	 * Method to create widget by setting widget info, Style hint etc.
	 *
	 * @param widgetInfo contains info of the widget
	 */
	@Override
	public void createWidget(HsxSBWidgetInfo widgetInfo) {
		this.setWidgetInfo(widgetInfo);
		this.getWidgetInfo().setStyle("");
	}

}
