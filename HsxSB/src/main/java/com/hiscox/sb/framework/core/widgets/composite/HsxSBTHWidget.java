package com.hiscox.sb.framework.core.widgets.composite;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.HsxSBCompositeWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class holds the characteristics of Table Header tag. and implements the basic
 * functionality defined by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:04 AM
 */
public class HsxSBTHWidget extends HsxSBCompositeWidget {

    /**
	 *
	 */
	private static final long serialVersionUID = -5495873873905593390L;

	public HsxSBTHWidget() {
	super();
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     * @param widgetInfo contains info of widget
     */
    public HsxSBTHWidget(String id, String name, HsxSBWidgetInfo widgetInfo) {
	super(id, name, widgetInfo);
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     */
    public HsxSBTHWidget(String id, String name) {
	super(id, name);
    }

    private List<HsxSBWidget> widgetsList = new ArrayList<HsxSBWidget>();

    public List<HsxSBWidget> getWidgetsList() {
	return widgetsList;
    }

    public void setWidgetsList(List<HsxSBWidget> widgetsList) {
	this.widgetsList = widgetsList;
    }

    /**
     * Method to create widget by setting widget info, name etc.
     *
     * @param widgetInfo contains info of widget
     */
    @Override
    public void createWidget(HsxSBWidgetInfo widgetInfo) {

	this.setWidgetInfo(widgetInfo);
	this.getWidgetInfo().setStyle("");
    }

    /**
     * This method removes the widget form the list.
     *
     * @param widget contains widget data
     */
    @Override
    public void removeWidget(HsxSBWidget widget) {

	widgetsList.remove(widget);
    }

    /**
     * This method adds the widget to the list.
     *
     * @param widget contains widget
     */
    @Override
    public void addWidget(HsxSBWidget widget) {

	widgetsList.add(widget);
    }

    /**
     *
     * @param element contains element
     */
    @Override
    public void createWidget(Element element) {

    }
}
