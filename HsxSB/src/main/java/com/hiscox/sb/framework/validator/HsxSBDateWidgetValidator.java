package com.hiscox.sb.framework.validator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class validates the date functionality validation.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:11 AM
 */
public class HsxSBDateWidgetValidator implements HsxSBUIBuilderConstants {

	public HsxSBDateWidgetValidator() {

	}

	/**
	 * this method validated the date widget.
	 *
	 * @param dateWidget is a date widget
	 * @return isValidDate
	 */
	public static boolean validateWidget(HsxSBWidget dateWidget) {

		String strInputDate = dateWidget.getWidgetInfo().getSavedValue();

		boolean isValidDate = true;
		if (HsxSBUtil.isNotBlank(strInputDate)
				&& strInputDate.contains(STR_FORWARD_SLASH)) {

			String[] dateTokens = strInputDate.split(STR_FORWARD_SLASH);
			if (dateTokens.length > 2
					&& HsxSBUtil.isNotBlank(dateTokens[0].trim())
					&& HsxSBUtil.isNotBlank(dateTokens[1].trim())
					&& HsxSBUtil.isNotBlank(dateTokens[2].trim())) {
				String strDay = dateTokens[0].trim();
				String strMonth = dateTokens[1].trim();
				String strYear = dateTokens[2].trim();

				Integer day = Integer.valueOf(strDay);
				Integer month = Integer.valueOf(strMonth);
				Integer year = Integer.valueOf(strYear);

				Integer currYear = Integer.valueOf(Calendar.getInstance()
						.get(Calendar.YEAR));
				Integer currDay = Integer.valueOf(Calendar.getInstance()
						.get(Calendar.DAY_OF_MONTH));
				if (HsxSBUtil.isNotBlank(dateWidget.getWidgetInfo().getSubType()) && dateWidget.getWidgetInfo().getSubType().equalsIgnoreCase(SUBTYPE_MONTHYEAR)){
					currDay = Integer.valueOf(STR_ZERO_ONE);
				}
				Integer currMonth = Integer.valueOf(((Calendar.getInstance())
						.get(Calendar.MONTH)) + 1);

				isValidDate = checkDate(day, month, year);

				if (!isValidDate) {
					return isValidDate;
				} else if (HsxSBUtil.isNotBlank(dateWidget.getWidgetInfo()
						.getAllowAnyDate())
						&& STR_YES.equalsIgnoreCase(dateWidget.getWidgetInfo()
								.getAllowAnyDate())) {
					boolean isCorrectFutureDate = true;
					boolean isCorrectPastDate = true;

					isCorrectFutureDate = validateAllowedFutureDays(dateWidget);
					isCorrectPastDate = validateAllowedPastDays(dateWidget);

					if ((!isCorrectFutureDate) || (!isCorrectPastDate)) {
						isValidDate = false;
					}

				} else if (HsxSBUtil.isNotBlank(dateWidget.getWidgetInfo()
						.getAllowFutureDate())
						&& STR_YES.equalsIgnoreCase(dateWidget.getWidgetInfo()
								.getAllowFutureDate())) {
					boolean isCorrectFutureDate = true;
					boolean isCorrectAllowed = true;

					isCorrectFutureDate = validatePastDate(day, month, year,
							currYear, currDay, currMonth);
					isCorrectAllowed = validateAllowedFutureDays(dateWidget);


					if ((!isCorrectFutureDate) || (!isCorrectAllowed)) {
						//System.out.println("setting date validation false "+isCorrectFutureDate+isCorrectFutureDate);
						isValidDate = false;
					}
				} else if (HsxSBUtil.isNotBlank(dateWidget.getWidgetInfo()
						.getAllowPastDate())
						&& STR_YES.equalsIgnoreCase(dateWidget.getWidgetInfo()
								.getAllowPastDate())) {
					boolean isCorrectFutureDate = true;
					boolean isCorrectAllowed = true;

					isCorrectFutureDate = validateFutureDate(day, month, year,
							currYear, currDay, currMonth);
					isCorrectAllowed = validateAllowedPastDays(dateWidget);

					if ((!isCorrectFutureDate) || (!isCorrectAllowed)) {
						isValidDate = false;
					}

				}

			}
		}

		return isValidDate;
	}

	/**
	 * This method checks whether the given input date exists or not.
	 *
	 * @param day
	 * @param month
	 * @param year
	 * @return isDateExits
	 */
	private static boolean checkDate(Integer day, Integer month, Integer year) {
		boolean isDateExits = true;

		// check if the month is in the limit of 1 to 12
		if (month < 1 || month > 12) {
			isDateExits = false;
		}
		// check if the day is in the limit of 1 to 31
		if (day < 1 || day > 31) {
			isDateExits = false;
		}
		// check if the month consists for 31 days or not
		if ((month == 4 || month == 6 || month == 9 || month == 11)
				&& day == 31) {
			isDateExits = false;
		}
		// validate the leap year
		if (month == 2) {
			boolean isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
			if (day > 29 || (day == 29 && !isleap)) {
				isDateExits = false;
			}
		}
		return isDateExits;
	}

	/**
	 * this method allows the specified days in the future. If it exceeds the
	 * allowed days validation will fail.
	 *
	 * @param dateWidget
	 * @return isValidDate
	 */
	private static boolean validateAllowedFutureDays(HsxSBWidget dateWidget) {
		boolean isValidDate = true;
		if (HsxSBUtil.isNotBlank(dateWidget.getWidgetInfo()
				.getDaysAllowedForward())
				&& HsxSBUtil.isNumeric(dateWidget.getWidgetInfo()
						.getDaysAllowedForward())) {
			Integer daysAllowedFwd = Integer.valueOf(dateWidget.getWidgetInfo()
					.getDaysAllowedForward());

			DateFormat dateFormat = new SimpleDateFormat(HTML_DATE_FORMAT);
			Date inputDate;
			try {
				inputDate = dateFormat.parse(dateWidget.getWidgetInfo()
						.getSavedValue());
				Calendar allowedFutureDate = Calendar.getInstance();
				allowedFutureDate.add(Calendar.DATE, daysAllowedFwd);

				if (inputDate.compareTo(allowedFutureDate.getTime()) > 0) {
					isValidDate = false;
				}
			} catch (ParseException e) {
				e.printStackTrace();
				throw new HsxSBUIBuilderRuntimeException(e.getMessage());
			}

		}
		return isValidDate;
	}

	/**
	 * this method allows the specified days in the past. If it preceeds the
	 * allowed days validation will fail.
	 *
	 * @param dateWidget
	 * @return isValidDate
	 */
	private static boolean validateAllowedPastDays(HsxSBWidget dateWidget) {
		boolean isValidDate = true;
		if (HsxSBUtil.isNotBlank(dateWidget.getWidgetInfo()
				.getDaysAllowedBackward())
				&& HsxSBUtil.isNumeric(dateWidget.getWidgetInfo()
						.getDaysAllowedBackward())) {
			Integer daysAllowedBwd = Integer.valueOf(dateWidget.getWidgetInfo()
					.getDaysAllowedBackward());

			DateFormat dateFormat = new SimpleDateFormat(HTML_DATE_FORMAT);
			Date inputDate;
			try {
				inputDate = dateFormat.parse(dateWidget.getWidgetInfo()
						.getSavedValue());
				Calendar allowedPastDate = Calendar.getInstance();
				allowedPastDate.add(Calendar.DATE, (-daysAllowedBwd));

				if ((allowedPastDate.getTime()).compareTo(inputDate) > 0) {
					isValidDate = false;
				}
			} catch (ParseException e) {
				e.printStackTrace();
				throw new HsxSBUIBuilderRuntimeException(e.getMessage());
			}

		}
		return isValidDate;
	}

	/**
	 * validation will fail if past date is selected.
	 *
	 * @param day
	 * @param month
	 * @param year
	 * @param currYear
	 * @param currDay
	 * @param currMonth
	 * @return isCorrectDate
	 */
	private static boolean validatePastDate(Integer day, Integer month,
			Integer year, Integer currYear, Integer currDay, Integer currMonth) {
		boolean isCorrectDate = true;

		if (year.intValue() < currYear.intValue()) {
			isCorrectDate = false;
		} else if ((currYear.intValue() == year.intValue())
				&& (month.intValue() < currMonth.intValue())) {
			isCorrectDate = false;
		} else if ((year.intValue() == currYear.intValue())
				&& (month.intValue() == currMonth.intValue())
				&& (day.intValue() < currDay.intValue())) {
			isCorrectDate = false;

		}
		return isCorrectDate;
	}

	/**
	 * validation will fail if future date is selected.
	 *
	 * @param day
	 * @param month
	 * @param year
	 * @param currYear
	 * @param currDay
	 * @param currMonth
	 * @return isCorrectDate
	 */
	private static boolean validateFutureDate(Integer day, Integer month,
			Integer year, Integer currYear, Integer currDay, Integer currMonth) {
		boolean isCorrectDate = true;

		if (year.intValue() > currYear.intValue()) {
			isCorrectDate = false;
		} else if ((currYear.intValue() == year.intValue())
				&& (month.intValue() > currMonth.intValue())) {
			isCorrectDate = false;
		} else if ((year.intValue() == currYear.intValue())
				&& (month.intValue() == currMonth.intValue())
				&& (day.intValue() > currDay.intValue())) {
			isCorrectDate = false;

		}
		return isCorrectDate;
	}
}
