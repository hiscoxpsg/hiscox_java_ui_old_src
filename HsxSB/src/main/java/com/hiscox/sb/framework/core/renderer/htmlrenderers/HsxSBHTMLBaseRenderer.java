package com.hiscox.sb.framework.core.renderer.htmlrenderers;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.HsxSBBaseRenderer;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;

/**
 * This class defines basic functionalities to render the primitive HTML tag.
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:22 AM
 */
public abstract class HsxSBHTMLBaseRenderer extends HsxSBBaseRenderer {

    /**
     * Method to render the tags content. For e,g HTML content, JSF content etc.
     * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws a runtime exception
     */
    @Override
    public abstract String renderContent(HsxSBWidget widget) throws HsxSBUIBuilderRuntimeException;

}
