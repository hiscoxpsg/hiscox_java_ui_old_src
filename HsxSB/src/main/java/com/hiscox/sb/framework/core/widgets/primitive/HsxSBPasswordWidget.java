package com.hiscox.sb.framework.core.widgets.primitive;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.HsxSBCompositeWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class holds the characteristics of Password tag. and implements the
 * basic functionality defined by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:41 AM
 */
public class HsxSBPasswordWidget extends HsxSBCompositeWidget {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private List<HsxSBWidget> widgetsList = new ArrayList<HsxSBWidget>();

    public HsxSBPasswordWidget() {
	super();
	this.setType(HsxSBUIBuilderConstants.PASSWORD);
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     * @param widgetInfo contains info of widget
     */
    public HsxSBPasswordWidget(String id, String name,
	    HsxSBWidgetInfo widgetInfo) {
	super(id, name, widgetInfo);
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     */
    public HsxSBPasswordWidget(String id, String name) {
	super(id, name);
    }

    /**
     * Method to create widget by setting widget info, Style hint etc.
     *
     * @param widgetInfo contains info of the widget
     */
    @Override
    public void createWidget(HsxSBWidgetInfo widgetInfo) {

	this.setWidgetInfo(widgetInfo);
	this.getWidgetInfo().setStyle(
		HsxSBUIBuilderConstants.HTML_PASSWORD_STYLE);
    }

    /**
     * This method adds the widget to the list.
     *
     * @param widget contains widget data
     */
    @Override
    public void addWidget(HsxSBWidget widget) {

	widgetsList.add(widget);
    }

    /**
     * This method retrieves the widgetlist.
     *
     * @return widget list
     */
    public List<HsxSBWidget> getWidgetsList() {
	return widgetsList;
    }

    @Override
    public void createWidget(Element element) {
	// TODO Auto-generated method stub

    }

    /**
     * This method removes the widget form the list.
     *
     * @param widget contains widget data
     */
    @Override
    public void removeWidget(HsxSBWidget widget) {

	widgetsList.remove(widget);
    }
}
