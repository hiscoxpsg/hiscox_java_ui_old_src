package com.hiscox.sb.framework.core.renderer.htmlrenderers;

/**
 * This class defines basic functionalities to render the composite HTML design.
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:23 AM
 */
public abstract class HsxSBHTMLCompositeRenderer extends HsxSBHTMLBaseRenderer {
/**
 * Default constructor.
 */
public HsxSBHTMLCompositeRenderer() {

}

}
