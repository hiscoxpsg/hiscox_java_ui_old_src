package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBAdditionalText4Widget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class renders the HTML Span tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:38 AM
 */
public class HsxSBAdditionalText4Renderer extends HsxSBHTMLBaseRenderer
	implements HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Span.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBAdditionalText4Widget additionalWidget = (HsxSBAdditionalText4Widget) widget;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CONTROL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(additionalWidget.getWidgetInfo().getId());
	contentBuilder.append(SPAN_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(additionalWidget.getWidgetInfo().getStyle());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);
	if (HsxSBUtil.isNotBlank(additionalWidget.getWidgetInfo().getSavedValue())) {
	    contentBuilder.append(additionalWidget.getWidgetInfo()
		    .getSavedValue());
	} else {
	    contentBuilder.append(additionalWidget.getWidgetInfo().getLabel());
	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
