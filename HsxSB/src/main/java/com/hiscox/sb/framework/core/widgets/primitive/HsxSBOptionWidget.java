package com.hiscox.sb.framework.core.widgets.primitive;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class holds the characteristics of Option tag. and implements the basic
 * functionality defined by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:28 AM
 */
public class HsxSBOptionWidget extends HsxSBBaseWidget {

	/**
	 *
	 */
	private static final long serialVersionUID = -2091378550033322547L;
	private boolean selected;

	public HsxSBOptionWidget() {
		super();
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 * @param widgetInfo contains info of widget
	 */
	public HsxSBOptionWidget(String id, String name, HsxSBWidgetInfo widgetInfo) {
		super(id, name, widgetInfo);
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 */
	public HsxSBOptionWidget(String id, String name) {
		super(id, name);
	}

	/**
	 * Method to create widget by setting widget info, Style hint etc.
	 *
	 * @param widgetInfo contains info of widgets
	 */
	@Override
	public void createWidget(HsxSBWidgetInfo widgetInfo) {
		this.setWidgetInfo(widgetInfo);
		this.getWidgetInfo().setStyle("");
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
