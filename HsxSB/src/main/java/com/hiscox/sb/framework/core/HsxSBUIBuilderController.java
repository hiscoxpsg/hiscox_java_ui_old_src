package com.hiscox.sb.framework.core;

import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Document;

import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;

/**
 * This interface defines basic functionalities to build the widgets, update the
 * widgets, build IPEXML.
 *
 * @author Cognizant
 * @version 1.0
 */
public interface HsxSBUIBuilderController {

/**
 * This method builds the widget and renders the HTML content, takes screen
 * XML in String format.
 * @param renderXML contains data in XML format
 * @param textMap having map variables
 * @param isJSEnabled denotes true if JS is enabled.
 * @param uiContentType contains Content type
 * @param widgetTypeProperties contains Type of widgets.
 * @param additionalMap contains Additional Map values
 * @param sessionMap contains session values
 * @param previousScreen contains name of previous screen
 * @return HtmlContent
 * @throws HsxSBUIBuilderRuntimeException
 * @throws HsxSBUIBuilderException
 */
	String generateHTML(String renderXML,
			Map<String, HsxSBTextManagerVO> textMap, String isJSEnabled,
			int uiContentType, Properties widgetTypeProperties,
			Map<String, String> additionalMap, Map<String, String> sessionMap, String previousScreen)
			throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException;

	/**
	 * This method builds the widget and renders the HTML content, takes
	 * screen XML in Document format.
	 * @param renderDocument COntains data in Doc format
	 * @param textMap having map variables
	 * @param isJSEnabled denotes true if JS is enabled.
	 * @param uiContentType contains Content type
	 * @param widgetTypeProperties contains Type of widgets.
	 * @param additionalMap contains Additional Map values
	 * @param sessionMap contains session values
	 * @param previousScreen contains name of previous screen
	 * @return HtmlContent
	 * @throws HsxSBUIBuilderRuntimeException
	 * @throws HsxSBUIBuilderException
	 */
	String generateHTML(Document renderDocument,
			Map<String, HsxSBTextManagerVO> textMap, String isJSEnabled,
			int uiContentType, Properties widgetTypeProperties,
			Map<String, String> additionalMap, Map<String, String> sessionMap, String previousScreen)
			throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException;

	/**
	 * Return the widget map created during building the widgets.
	 * @return WidgetMap
	 */
	Map<String, HsxSBWidget> getWidgetMap();

}
