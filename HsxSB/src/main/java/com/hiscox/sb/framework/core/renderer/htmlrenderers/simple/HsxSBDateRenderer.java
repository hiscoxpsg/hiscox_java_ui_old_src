package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.util.HsxSBWidgetFactory;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBOptionWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBDateWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBPermitedValues;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML Date tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:13 AM
 */
public class HsxSBDateRenderer extends HsxSBHTMLBaseRenderer
	implements
	    HsxSBUIBuilderConstants {

    private StringTokenizer dateToken;
    private String renderFormat = DATE_DEF_FORMAT;
    private String startYearRange = Integer.toString(Calendar.getInstance()
	    .get(Calendar.YEAR) - 30);
    private String endYearRange = Integer.toString(Calendar.getInstance().get(
	    Calendar.YEAR) + 30);
    private String savedDay;
    private String savedMonth;
    private String savedYear;
    private List<HsxSBPermitedValues> months = new ArrayList<HsxSBPermitedValues>();
    private List<HsxSBWidget> optionsList = new ArrayList<HsxSBWidget>();

    /**
     * This method will return the String for rendering the HTML Date. The Date
     * renderer will be combination of drop down renderer
     * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws runtime exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBDateWidget dateWidget = (HsxSBDateWidget) widget;
	String questionId = dateWidget.getWidgetInfo().getId();

	/* Get the Months from permitted Values */
	if (dateWidget.getWidgetInfo().getPermitedVlaues().isEmpty()) {
	    for (int i = 0; i < 12; i++) {
		HsxSBPermitedValues pv = new HsxSBPermitedValues();
		pv.setValue(Integer.toString(i + 1));
		try {
		    pv.setDisplayText(new SimpleDateFormat(MONTH_FORMAT_1)
			    .format(new SimpleDateFormat(MONTH_FORMAT_2)
				    .parse(Integer.toString(i + 1))));
		} catch (ParseException e) {
		    throw new HsxSBUIBuilderRuntimeException();
		}
		months.add(pv);
	    }
	} else {
	    months = widget.getWidgetInfo().getPermitedVlaues();
	}

	int[] loopCounts = 	{0, 0, 0};
	String[] dateNames = new String[3];

	if (null != widget.getWidgetInfo().getDateFormat()) {
	    renderFormat = widget.getWidgetInfo().getDateFormat();
	}

	char[] a = renderFormat.toCharArray();

	if (HsxSBUtil.isNotBlank(widget.getWidgetInfo().getSubType())
		&& HTML_DATE_MONTH_YEAR.equalsIgnoreCase(dateWidget
			.getWidgetInfo().getSubType())) {
	    a = HsxSBUtil.dateFormatAdjuster(a);
	}

	if (HsxSBUtil.isNotBlank(widget.getWidgetInfo().getYearRangeStart()) == true) {
	    startYearRange = widget.getWidgetInfo().getYearRangeStart();
	}

	if (HsxSBUtil.isNotBlank(widget.getWidgetInfo().getYearRangeEnd()) == true) {
	    endYearRange = widget.getWidgetInfo().getYearRangeEnd();
	}

	if (startYearRange.length() < 4) {

	    startYearRange = Integer.toString(Integer.parseInt(startYearRange)
		    + Calendar.getInstance().get(Calendar.YEAR));

	}
	if (endYearRange.length() < 4) {
	    endYearRange = Integer.toString(Integer.parseInt(endYearRange)
		    + Calendar.getInstance().get(Calendar.YEAR));
	}

	if (HsxSBUtil.isNotBlank(dateWidget.getWidgetInfo().getAllowPastDate())
		&& dateWidget.getWidgetInfo().getAllowPastDate()
			.equalsIgnoreCase(STR_YES)) {
	    endYearRange = Integer.toString(Calendar.getInstance().get(
		    Calendar.YEAR));
	}

	if (HsxSBUtil.isNotBlank(dateWidget.getWidgetInfo()
		.getAllowFutureDate())
		&& dateWidget.getWidgetInfo().getAllowFutureDate()
			.equalsIgnoreCase(STR_YES)) {
	    startYearRange = Integer.toString(Calendar.getInstance().get(
		    Calendar.YEAR));
	}

	final int length = a.length;
	for (int i = 0; i < length; i++) {
	    char key = a[i];
	    switch (key) {
		case DAY_CHAR :
		    loopCounts[i] = 31;
		    dateNames[i] = STR_DAY;
		    break;
		case MONTH_CHAR :
		    loopCounts[i] = 12;
		    dateNames[i] = STR_MONTH;
		    break;
		case YEAR_CHAR :
		    loopCounts[i] = Integer.parseInt(endYearRange)
			    - Integer.parseInt(startYearRange) + 1;
		    dateNames[i] = STR_YEAR;
		    break;
		default :
		    break;
	    }

	}

	if (HsxSBUtil.isNotBlank(widget.getWidgetInfo().getSavedValue())
		&& HsxSBUtil.isDate(widget.getWidgetInfo().getSavedValue())) {
	    dateToken = new StringTokenizer(widget.getWidgetInfo()
		    .getSavedValue(), DATE_TOKENS);
	    try {
		savedDay = Integer.toString(Integer.valueOf(dateToken
			.nextToken()));
	    } catch (NumberFormatException numExptn) {
		throw new HsxSBUIBuilderRuntimeException();
	    }
	    savedMonth = dateToken.nextToken().toString();
	    savedYear = dateToken.nextToken();
	    try {
		savedMonth = months.get(Integer.valueOf(savedMonth) - 1)
			.getValue().toString();
		if (savedMonth != null && savedMonth.trim().length() == 1) {
		    savedMonth = STR_ZERO + savedMonth;
		}
	    } catch (NumberFormatException numberFormExcep) {
		HsxSBPermitedValues temp = null;
		Iterator<HsxSBPermitedValues> it = months.iterator();
		while (it.hasNext()) {
		    temp = it.next();
		    if (savedMonth.regionMatches(true, 0,
			    temp.getDisplayText(), 0, 3)) {
			savedMonth = temp.getValue();
			break;
		    }
		}
	    } catch (IndexOutOfBoundsException outOfBoundsEx) {
		//System.out.println("ERROR: Saved value "+widget.getWidgetInfo().getSavedValue()+" has format other than DDMMYYYY, hence rendering today's date.");
		this.setDefaultDate();
	    }
	} else {
	   this.setDefaultDate();
	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CONTROL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	for (int i = 0; i < 3; i++) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_SELECT);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_DROPDOWN_STYLE);

	    if (HTML_DATE_MONTH_YEAR.equalsIgnoreCase(dateWidget
		    .getWidgetInfo().getSubType())
		    && a[i] == DAY_CHAR) {
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(CSS_HIDE_CLASS);
	    }
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    /*
	     * contentBuilder.append(HTML_SINGLESPACE);
	     * contentBuilder.append(HTML_TABINDEX);
	     * contentBuilder.append(HTML_EQUALS);
	     * contentBuilder.append(HTML_SINGLE_QUOTE);
	     * contentBuilder.append(dateWidget.getWidgetInfo().getOrder());
	     * contentBuilder.append(HTML_SINGLE_QUOTE);
	     */
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_NAME);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(questionId);
	    contentBuilder.append(STR_DROPDOWN_SUFFIX);

	    contentBuilder.append(dateNames[i]);
	    contentBuilder.append(STR_NAME_SUFFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_ID);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(questionId);
	    contentBuilder.append(STR_DROPDOWN_SUFFIX);
	    contentBuilder.append(dateNames[i]);
	    contentBuilder.append(XML_QUESTION_ID);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);

	    if (HTML_YES.equalsIgnoreCase(dateWidget.getWidgetInfo()
		    .getDisplayOnly())) {
		contentBuilder.append(HTML_DISABLED);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE + HTML_DISABLED
			+ HTML_SINGLE_QUOTE);
	    }
	    contentBuilder.append(HTML_END_TAG);

	    for (int j = 1; j <= loopCounts[i]; j++) {
		HsxSBWidget optionWidget = HsxSBWidgetFactory
			.getWidgetInstance(HTML_OPTION, STR_EMPTY, STR_EMPTY);
		HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();

		if (a[i] == MONTH_CHAR) {
		    String monthValue = months.get(j - 1).getValue().toString();
		    if (monthValue != null && monthValue.trim().length() == 1) {
			monthValue = STR_ZERO + monthValue;
		    }
		    widgetInfo.setDefaultValue(monthValue);
		    widgetInfo.setDisplayText(months.get(j - 1)
			    .getDisplayText());
		    widgetInfo.setId(STR_MONTH + HTML_HYPHEN
			    + Integer.toString(j));
		    widgetInfo.setSavedValue(savedMonth);
		} else if (a[i] == YEAR_CHAR) {
		    widgetInfo.setDefaultValue(Integer.toString(Integer
			    .parseInt(startYearRange)
			    + j - 1));
		    widgetInfo.setDisplayText(Integer.toString(Integer
			    .parseInt(startYearRange)
			    + j - 1));
		    widgetInfo.setId(STR_YEAR
			    + HTML_HYPHEN
			    + (Integer.toString(Integer
				    .parseInt(startYearRange)
				    + j - 1)));
		    widgetInfo.setSavedValue(savedYear);

		} else {
		    widgetInfo.setDefaultValue(Integer.toString(j));
		    widgetInfo.setDisplayText(Integer.toString(j));
		    widgetInfo.setId(STR_DAY + HTML_HYPHEN
			    + Integer.toString(j));
		    widgetInfo.setSavedValue(savedDay);

		}
		optionWidget.createWidget(widgetInfo);
		optionsList.add(optionWidget);
	    }

	    Iterator<HsxSBWidget> optionsListIterator = optionsList.iterator();
	    while (optionsListIterator.hasNext()) {
		HsxSBWidget optWidget = (HsxSBOptionWidget) optionsListIterator
			.next();
		optWidget.getWidgetInfo().setType(HTML_OPTION);
		HsxSBRenderer renderer = HsxSBHTMLRendererFactory
			.getRendererInstance(optWidget.getWidgetInfo()
				.getType());

		String rendererd = renderer.renderContent(optWidget);
		contentBuilder.append(rendererd);
	    }

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SELECT);
	    contentBuilder.append(HTML_END_TAG);
	    optionsList.clear();

	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	// if java script is disabled then the calendar icon should be hidden
	String calendarIconCss = CSS_HIDE_CLASS;
	String isJsEnabled = dateWidget.getWidgetInfo().getIsJSEnabled();
	//System.out.println("isJsEnabled :" + isJsEnabled);
	if (!STR_NO.equalsIgnoreCase(isJsEnabled)) {
	    calendarIconCss = HTML_CALENDARICON_DIV_STYLE;
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(calendarIconCss);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TABINDEX);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(dateWidget.getWidgetInfo().getOrder());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_HREF);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_DATE_HREF_VALUE); /*To avoid jerk on click */
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	/*contentBuilder.append(HTML_TABINDEX);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTES);
	contentBuilder.append(HTML_SINGLESPACE);*/
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(questionId);
	contentBuilder.append(STR_IMAGE_BUTTON_ID);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_HIDDEN);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(questionId);
	contentBuilder.append(STR_INPUT_HIDDEN_NAME);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(questionId);
	contentBuilder.append(STR_INPUT_HIDDEN_ID);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_READONLY);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_READONLY_ESCAPED);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_HIDDEN);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(questionId);
	contentBuilder.append(STR_YEAR_RANGE_START_ID);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(startYearRange);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_READONLY);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_READONLY_ESCAPED);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_HIDDEN);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(questionId);
	contentBuilder.append(STR_YEAR_RANGE_END_ID);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(endYearRange);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_READONLY);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_READONLY_ESCAPED);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_HIDDEN);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(questionId);
	contentBuilder.append(ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_DOUBLE_QUOTE);

	if (dateWidget.getWidgetInfo().getSavedValue() != null) {
	    if (dateWidget.getWidgetInfo().getSavedValue().contains(STR_DAY)) {
		String day = STR_EMPTY + Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		// Appending 0 to all the days between 1 to 9
		if (HsxSBUtil.isNotBlank(day)
			&& day.trim().length() == 1) {
		    day = STR_ZERO + day;
		}
		String month = STR_EMPTY + Calendar.getInstance().get(Calendar.MONTH) + 1;
		String year = STR_EMPTY + Calendar.getInstance().get(Calendar.YEAR);
		 String  answerValue = day + STR_FORWARD_SLASH + month
		    + STR_FORWARD_SLASH + year;
		 dateWidget.getWidgetInfo().setSavedValue(answerValue);
	    }
	    contentBuilder.append(dateWidget.getWidgetInfo().getSavedValue());
	}

	contentBuilder.append(HTML_DOUBLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(questionId);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_READONLY);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_READONLY_ESCAPED);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	String includeHelp = HsxSBWidgetUtil.addHelpRenderer(dateWidget);

	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
		String includeAdditionalText4 = HsxSBWidgetUtil
			.addAdditionalText4Renderer(dateWidget);
		contentBuilder.append(includeAdditionalText4);
	    
	}

	contentBuilder.append(includeHelp);

	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
		String includeAdditionalText1 = HsxSBWidgetUtil
			.addAdditionalTextOneRenderer(dateWidget);
		contentBuilder.append(includeAdditionalText1);
	    }
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
		String includeAdditionalText2 = HsxSBWidgetUtil
			.addAdditionalTextTwoRenderer(dateWidget);
		contentBuilder.append(includeAdditionalText2);
	    }
	}

	if (dateWidget.getWidgetInfo().isErrorIndicator()) {
	    String includeError = HsxSBWidgetUtil.addErrorRenderer(dateWidget);
	    contentBuilder.append(includeError);
	}

	return contentBuilder.toString();
    }
/**
 * Default date setter.
 */
    public void setDefaultDate() {
	    savedDay = Integer.toString(Calendar.getInstance().get(
		    Calendar.DAY_OF_MONTH));
	    savedMonth = months.get(Calendar.getInstance().get(Calendar.MONTH))
		    .getValue();
	    if (savedMonth != null && savedMonth.trim().length() == 1) {
		savedMonth = STR_ZERO + savedMonth;
	    }
	    savedYear = Integer.toString(Calendar.getInstance().get(
		    Calendar.YEAR));
    }
   /* public static void main(String[] a){
	System.out.println("day :" +Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
	System.out.println("month :" +Calendar.getInstance().get(Calendar.MONTH)+1);
	System.out.println("year :" +Calendar.getInstance().get(Calendar.YEAR));
	String day = ""+Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	String month = ""+Calendar.getInstance().get(Calendar.MONTH)+1;
	String year = ""+Calendar.getInstance().get(Calendar.YEAR);
	 String  answerValue = day + STR_FORWARD_SLASH + month
	    + STR_FORWARD_SLASH + year;
	 System.out.println("answerValue :: "+answerValue);
    }*/
}
