package com.hiscox.sb.framework.core.util;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBActionListRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBDivRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBFormRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBQuestionGroupRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBQuestionRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBScreenRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBTDRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBTHRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBTRRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.composite.HsxSBTableRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBAnchorRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBButtonRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBCheckBoxRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBDefaultNavigationButtonRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBDropDownRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBHiddenRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBIframeRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBLabelRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBLabelSpanRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBOptionRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBPasswordRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBPopUpButtonRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBRadioRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBSpanRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBTableSpanRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBTextAreaRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive.HsxSBTextBoxRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBAdditionalTextRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBAddressSummaryRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBDateRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBEditRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBEndorsementRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBErrorRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBHelpRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBLimitSummaryRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBStaticRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBSurchargeSummaryRenderer;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.simple.HsxSBTelephoneRenderer;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:24 AM
 */
public class HsxSBHTMLRendererFactory implements HsxSBUIBuilderConstants {
/**
 * Default constructor.
 */
    public HsxSBHTMLRendererFactory() {

    }

    /**
     * Return the right Widget Type based on the String parameter.
     * @return renderer
     * @param rendererType contains type of renderer
     */
    public static HsxSBRenderer getRendererInstance(final String rendererType) {
	HsxSBRenderer renderer = null;
	if (TYPE_TEXT.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBTextBoxRenderer();
	} else if (TYPE_DROPDOWN.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBDropDownRenderer();
	} else if (TYPE_ANCHOR.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBAnchorRenderer();
	} else if (TYPE_CHECKBOX.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBCheckBoxRenderer();
	} else if (TYPE_HIDDEN.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBHiddenRenderer();
	} else if (TYPE_LABEL.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBLabelRenderer();
	} else if (TYPE_OPTION.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBOptionRenderer();
	} else if (TYPE_RADIO.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBRadioRenderer();
	} else if (TYPE_ACTION.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBButtonRenderer();
	} else if (TYPE_SPAN.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBSpanRenderer();
	} else if (TYPE_LABEL_SPAN.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBLabelSpanRenderer();
	} else if (TYPE_TABLE_SPAN.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBTableSpanRenderer();
	} else if (TYPE_FORM.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBFormRenderer();
	} else if (TYPE_DIV.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBDivRenderer();
	} else if (TYPE_QUESTION.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBQuestionRenderer();
	} else if (TYPE_SCREEN.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBScreenRenderer();
	} else if (TYPE_QUESTION_GROUP.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBQuestionGroupRenderer();
	} else if (TYPE_TEXTAREA.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBTextAreaRenderer();
	} else if (TYPE_ACTIONLIST.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBActionListRenderer();
	} else if (TYPE_TELEPHONE.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBTelephoneRenderer();
	} else if (TYPE_HELP.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBHelpRenderer();
	} else if (TYPE_STATIC.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBStaticRenderer();
	} else if (TYPE_ERROR.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBErrorRenderer();
	} else if (TYPE_TABLE.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBTableRenderer();
	} else if (TYPE_TABLEROW.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBTRRenderer();
	} else if (TYPE_TABLEHEADER.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBTHRenderer();
	} else if (TYPE_TABLECOLLABELENTRY.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBTDRenderer();
	} else if (TYPE_TABLERADIOENTRY.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBTDRenderer();
	} else if (TYPE_DATE.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBDateRenderer();
	} else if (TYPE_PASSWORD.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBPasswordRenderer();
	} else if (TYPE_ACTIONGROUP.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBActionListRenderer();
	} else if (TYPE_EDIT.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBEditRenderer();
	} else if (TYPE_ADDITIONALTEXT.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBAdditionalTextRenderer();
	} else if (TYPE_FILELINK.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBEndorsementRenderer();
	} else if (TYPE_LIMITSUMMARY.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBLimitSummaryRenderer();
	} else if (TYPE_SURCHARGESUMMARY.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBSurchargeSummaryRenderer();
	} else if (TYPE_ADDRESSSUMMARY.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBAddressSummaryRenderer();
	} else if (TYPE_ACTION_HREF.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBPopUpButtonRenderer();
	} else if (TYPE_DEFAULT_NAVIGATOR.equalsIgnoreCase(rendererType)) {
	    renderer = new HsxSBDefaultNavigationButtonRenderer();
	} else if (TYPE_IFRAME.equalsIgnoreCase(rendererType)) {
		renderer = new HsxSBIframeRenderer();
	}
	return renderer;
    }

}
