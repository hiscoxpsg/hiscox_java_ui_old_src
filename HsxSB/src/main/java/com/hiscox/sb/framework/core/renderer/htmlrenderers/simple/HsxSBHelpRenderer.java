package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBHelpWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the HTML Help tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:13 AM
 */
public class HsxSBHelpRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /** Default constructor. */
    public HsxSBHelpRenderer() {
	super();
    }

    /**
     * This method will return the String for rendering the HTML Help tag.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();
	HsxSBHelpWidget helpWidget = (HsxSBHelpWidget) widget;
	String helpId = helpWidget.getWidgetInfo().getId();
	helpId = helpId.replace(STR_HELP_ID, STR_EMPTY);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(helpId);
	contentBuilder.append(HELP_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_HELPICON_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TARGET);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(STR_BLANK);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_HREF);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(STR_HELP_TEXT);
	contentBuilder.append(STR_QUESTION_MARK_SYMBOL);
	contentBuilder.append(STR_PAGE_NAME);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HsxSBUIBuilderControllerImpl.screenName);
	contentBuilder.append(AMPERSAND);
	contentBuilder.append(AMPERSAND);
	contentBuilder.append(STR_QUESTION_CODE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(helpWidget.getWidgetInfo().getCode());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(CSS_TOOL_TIP_ACHOR_TAG_CLASS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(helpId);
	contentBuilder.append(ANCHOR_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);

	/*
	 * contentBuilder.append(HTML_TARGET);
	 * contentBuilder.append(HTML_EQUALS);
	 * contentBuilder.append(HTML_SINGLE_QUOTE);
	 * contentBuilder.append(STR_BLANK);
	 * contentBuilder.append(HTML_SINGLE_QUOTE);
	 * contentBuilder.append(HTML_SINGLESPACE);
	 */
	contentBuilder.append(HTML_END_TAG);
	// To display help text link instead of help icon
	if (helpWidget.getWidgetInfo().getAdditionalText6() != null) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);
	    contentBuilder.append(helpWidget.getWidgetInfo()
		    .getAdditionalText6());
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);
	}
	//ends here
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_END_TAG);

	// contentBuilder.append(HTML_START_TAG);
	// contentBuilder.append(HTML_ANCHOR);
	// contentBuilder.append(HTML_SINGLESPACE);
	// contentBuilder.append(HTML_HREF);
	// contentBuilder.append(HTML_EQUALS);
	// contentBuilder.append(HTML_HASH);
	// contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(helpId);
	contentBuilder.append(VALUE_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(CSS_HIDE_CLASS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(helpWidget.getWidgetInfo().getHelpText());
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	// contentBuilder.append(HTML_START_TAG);
	// contentBuilder.append(HTML_FRONT_SLASH);
	// contentBuilder.append(HTML_ANCHOR);
	// contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
