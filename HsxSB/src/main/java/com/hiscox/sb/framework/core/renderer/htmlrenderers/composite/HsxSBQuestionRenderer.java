package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class renders the HTML tags to display questions .
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:29 AM
 */
public class HsxSBQuestionRenderer extends HsxSBHTMLCompositeRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method returns the String for rendering the HTML tags to display the
     * screen questions. The method in turn calls the renderer's such as Label
     * renderer, text box renderer as per the screen question.
     * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws runtime exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBQuestionWidget questionWidget = (HsxSBQuestionWidget) widget;
	String questionId = questionWidget.getWidgetInfo().getId();

	if (!questionId.contains(STR_DUMMY)) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_LI);

	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(questionWidget.getWidgetInfo().getStyleHint());
	    contentBuilder.append(HTML_SINGLE_QUOTE);

	    if (null != questionWidget.getWidgetInfo().getId()
		    && questionWidget.getWidgetInfo().getId().trim().length() > 0) {
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_ID);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(questionWidget.getWidgetInfo().getId());
		contentBuilder.append(HTML_SINGLE_QUOTE);
	    }
	    contentBuilder.append(HTML_END_TAG);
	}

	// non java script text
	if (HsxSBUtil.isNotBlank(questionWidget.getWidgetInfo().getAdditionalText3())){
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_NOSCRIPT);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(CSS_NON_JS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(questionWidget.getWidgetInfo()
		    .getAdditionalText3());

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_NOSCRIPT);
	    contentBuilder.append(HTML_END_TAG);

	}

	Iterator<HsxSBWidget> widgetsListIterator = questionWidget
		.getWidgetsList().iterator();
	while (widgetsListIterator.hasNext()) {
	    HsxSBWidget sbwidget = widgetsListIterator.next();
	    HsxSBRenderer renderer = HsxSBHTMLRendererFactory
		    .getRendererInstance(sbwidget.getWidgetInfo().getType());
	    String rendererd = null;
	    if (renderer != null) {
		rendererd = renderer.renderContent(sbwidget);
	    }
	    contentBuilder.append(rendererd);
	}

	if (!questionId.contains(STR_DUMMY)) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_LI);
	    contentBuilder.append(HTML_END_TAG);
	}

	return contentBuilder.toString();
    }

}
