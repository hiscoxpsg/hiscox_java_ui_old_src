package com.hiscox.sb.framework.core.widgets.vo;

import java.io.Serializable;

/**
 * This class is value holder for the option value of of certain widgets like
 * select, radio, check box etc.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:28 AM
 */
public class HsxSBPermitedValues implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5881093620832359642L;
	private String displayText;
	private int order;
	private String value;

	public HsxSBPermitedValues() {

	}

	public String getDisplayText() {
		return displayText;
	}

	public int getOrder() {
		return order;
	}

	public String getValue() {
		return value;
	}

	/**
	 *
	 * @param displayText contains input string
	 */
	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}

	/**
	 *
	 * @param order contains order
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	/**
	 *
	 * @param value contains value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
