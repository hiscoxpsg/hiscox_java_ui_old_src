package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBLabelWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class renders the HTML label tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:26 AM
 */
public class HsxSBLabelRenderer extends HsxSBHTMLBaseRenderer
	implements
	    HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Label.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBLabelWidget labelWidget = (HsxSBLabelWidget) widget;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_LABEL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_LABEL);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_FOR);
	contentBuilder.append(HTML_EQUALS);
	final String labelId = labelWidget.getWidgetInfo().getId();
	String forId = labelId.replaceAll(STR_LABEL.toLowerCase(), STR_EMPTY);

	contentBuilder.append(HTML_SINGLE_QUOTE);

	if (HsxSBUtil.isNotBlank(labelWidget.getWidgetInfo().getLabelForId())) {
	    forId = labelWidget.getWidgetInfo().getLabelForId();
	}

	contentBuilder.append(forId);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(labelId);
	contentBuilder.append(LABEL_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_END_TAG);
	// contentBuilder.append(labelWidget.getWidgetInfo().getSavedValue());
	// Need to be remove once Text manager is ready - Start
	if (null == labelWidget.getWidgetInfo().getLabel()) {
	    contentBuilder.append(labelId);
	} else {
	    contentBuilder.append(labelWidget.getWidgetInfo().getLabel());
	}
	if ((HsxSBUtil.isNotBlank(labelWidget.getWidgetInfo().isMandatory()) && !STR_YES
		.equalsIgnoreCase(labelWidget.getWidgetInfo().isMandatory()))
		&& HsxSBUtil.isNotBlank(labelWidget.getWidgetInfo()
			.getOptionalText())
		&& !STR_YES.equalsIgnoreCase(labelWidget.getWidgetInfo()
			.getDisplayOnly())) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(CSS_OPTIONAL);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);
	    contentBuilder.append(STR_SINGLE_SPACE);
	    contentBuilder
		    .append(labelWidget.getWidgetInfo().getOptionalText());

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_LABEL);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
