package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTDWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the HTML TD tag.
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:06 AM
 */
public class HsxSBTDRenderer extends HsxSBHTMLCompositeRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method returns the String for rendering the HTML TD tag. This method
     * in turn calls the Span renderer.
     * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws runtime exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBTDWidget tdWidget = (HsxSBTDWidget) widget;
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_TD);
	contentBuilder.append(HTML_END_TAG);

	Iterator<HsxSBWidget> widgetListIterator = tdWidget.getWidgetsList()
		.iterator();
	while (widgetListIterator.hasNext()) {
	    HsxSBWidget sbWidget = widgetListIterator.next();
	    HsxSBRenderer tdRenderer = HsxSBHTMLRendererFactory
		    .getRendererInstance(sbWidget.getWidgetInfo().getType());
	    String rendererd = tdRenderer.renderContent(sbWidget);
	    contentBuilder.append(rendererd);
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_TD);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }
}
