package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBIframeWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class renders the HTML Iframe tag.
 * @author Cognizant
 * @version 1.0
 * created 21-Sep-2012 6:27:03 AM
 */
public class HsxSBIframeRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Iframe.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBIframeWidget iframeWidget = (HsxSBIframeWidget) widget;

	String sessionId = null;
	String hostUrl = null;
	if (null != iframeWidget.getWidgetInfo().getSessionId()
		&& iframeWidget.getWidgetInfo().getSessionId().trim().length() > 0) {
		sessionId = iframeWidget.getWidgetInfo().getSessionId();
	}

	if (null != iframeWidget.getWidgetInfo().getHostUrl()
			&& iframeWidget.getWidgetInfo().getHostUrl().trim().length() > 0) {
		hostUrl = iframeWidget.getWidgetInfo().getHostUrl();
	}

	contentBuilder.append(HTML_IFRAME_START);
	contentBuilder.append(hostUrl);
	contentBuilder.append(HTML_QUESTION_MARK_SYMBOL);
	contentBuilder.append(HTML_HPS_SESSIONID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(sessionId);
	contentBuilder.append(HTML_DOUBLE_QUOTE);

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_HEIGHT);
	contentBuilder.append(HTML_EQUALS);
	if (STR_NO.equalsIgnoreCase(iframeWidget.getWidgetInfo().getIsJSEnabled())) {
		if (HsxSBUtil.isNotBlank(iframeWidget.getWidgetInfo().getHeight())) {
	contentBuilder.append(iframeWidget.getWidgetInfo().getHeight());
		} else {
			contentBuilder.append("820");
		}
	} else {
	contentBuilder.append("435");
	}

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_WIDTH);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append("575");

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_IFRAMECSS);

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ALLOWTRANSPARENCY);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_TRUE);

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_FRAMEBRODER);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(STR_ZERO);

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_IFRAME_END_2);

	return contentBuilder.toString();
    }

}
