package com.hiscox.sb.framework.core.renderer.htmlrenderers.simple;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBAddressSummaryWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the Address tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:13 AM
 */
public class HsxSBAddressSummaryRenderer extends HsxSBHTMLBaseRenderer implements
	    HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Address tag.
     * The Address renderer is combination of Spans
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBAddressSummaryWidget addressWidget = (HsxSBAddressSummaryWidget) widget;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	if (HTML_YES.equalsIgnoreCase(addressWidget.getWidgetInfo()
		.getDisplayOnly())) {
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_DISABLED);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	}
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CONTROL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	if (null != addressWidget.getWidgetInfo().getAssemblyMethod()
		&& CONFIG_LINEBREAK.equalsIgnoreCase(addressWidget
			.getWidgetInfo().getAssemblyMethod())) {
	    Iterator<String> iterator = widget.getWidgetInfo().getAddressLine()
		    .iterator();
	    contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_SPAN);
		contentBuilder.append(HTML_END_TAG);
	    if (iterator.hasNext()) {
		contentBuilder.append(iterator.next());

		while (iterator.hasNext()) {
		    contentBuilder.append(STR_COMMA);
		    contentBuilder.append(HTML_BREAK);
		    contentBuilder.append(iterator.next());
		}

	    }
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);

	} else {
	    Iterator<String> iterator = widget.getWidgetInfo().getAddressLine()
		    .iterator();
	    if (iterator.hasNext()) {
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_SPAN);
		contentBuilder.append(HTML_END_TAG);
		contentBuilder.append(iterator.next());

		while (iterator.hasNext()) {
		    contentBuilder.append(STR_COMMA);
		    contentBuilder.append(HTML_SINGLESPACE);
		    contentBuilder.append(iterator.next());
		}
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_SPAN);
		contentBuilder.append(HTML_END_TAG);
	    }

	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	String includeHelp = HsxSBWidgetUtil.addHelpRenderer(addressWidget);
	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
		String includeAdditionalText4 = HsxSBWidgetUtil
			.addAdditionalText4Renderer(addressWidget);
		contentBuilder.append(includeAdditionalText4);
	}

	contentBuilder.append(includeHelp);

	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
		String includeAdditionalText1 = HsxSBWidgetUtil
			.addAdditionalTextOneRenderer(addressWidget);
		contentBuilder.append(includeAdditionalText1);
	    }
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
		String includeAdditionalText2 = HsxSBWidgetUtil
			.addAdditionalTextTwoRenderer(addressWidget);
		contentBuilder.append(includeAdditionalText2);
	    }
	}
	return contentBuilder.toString();
    }

}
