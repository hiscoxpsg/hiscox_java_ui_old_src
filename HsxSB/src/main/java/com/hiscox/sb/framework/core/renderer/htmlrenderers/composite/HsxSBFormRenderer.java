package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBFormWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBButtonWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class renders the HTML form tag .
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:17 AM
 */
public class HsxSBFormRenderer extends HsxSBHTMLCompositeRenderer
	implements
	    HsxSBUIBuilderConstants {

    /**
     * This method returns the String for rendering the HTML Form tag. This
     * method in turn calls the Question group renderer, which calls the
     * question renderer, which returns the primitive type renderer's such as
     * Label or Text box renderer's etc.
     * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws runtime exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBFormWidget formWidget = (HsxSBFormWidget) widget;

	if (formWidget.getWidgetInfo().getFormName() != null) {

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FORM);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_NAME);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(formWidget.getWidgetInfo().getFormName());
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_ID);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(formWidget.getWidgetInfo().getFormName());
	    contentBuilder.append(FORM_ID_SUFFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);

	    if (null != formWidget.getWidgetInfo().getFormAction()
		    && HTML_RETRIEVEQUOTE_ACTION.equalsIgnoreCase(formWidget
			    .getWidgetInfo().getFormAction())) {
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_ACTION);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(formWidget.getWidgetInfo()
			.getFormAction());
		contentBuilder.append(HTML_SINGLE_QUOTE);
	    } else if (null != formWidget.getWidgetInfo().getFormAction()
		    && HTML_SAVEQUOTE_ACTION.equalsIgnoreCase(formWidget
			    .getWidgetInfo().getFormAction())) {
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_ACTION);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(formWidget.getWidgetInfo()
			.getFormAction());
		contentBuilder.append(HTML_SINGLE_QUOTE);
	    }

	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_METHOD);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_POST);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_COMMAND_NAME);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SCREEN);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);

	}

	if ((null != formWidget.getWidgetInfo().getFormAction())
		&& (formWidget.getWidgetInfo().isErrorIndicator() == true)
		&& ((HTML_SAVEQUOTE_ACTION.equalsIgnoreCase(formWidget
			.getWidgetInfo().getFormAction())) || (HTML_RETRIEVEQUOTE_ACTION
			.equalsIgnoreCase(formWidget.getWidgetInfo()
				.getFormAction())))) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(formWidget.getWidgetInfo().getErrorStyle());
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_ID);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(formWidget.getWidgetInfo().getId());
	    contentBuilder.append(SCREEN_ERROR_ID_SUFFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_ERROR_STYLE);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);
	    // Error text from the Text manager
	    if (null != formWidget.getWidgetInfo().getPageErrorText()) {
		contentBuilder.append(formWidget.getWidgetInfo()
			.getPageErrorText());
	    }

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_END_TAG);

	}
//	System.out.println("In the Form renderer :: "
//		+ formWidget.getWidgetInfo().getName());
//	System.out.println("is defaultNavigatorButtonWidget :: "
//		+ formWidget.getWidgetInfo().getDefaultNavigator());
	if (HsxSBUtil.isNotBlank(formWidget.getWidgetInfo()
		.getDefaultNavigator())) {
	    String defaultNavigatinButton = STR_EMPTY;
	    HsxSBButtonWidget defaultNavigatorButtonWidget = new HsxSBButtonWidget();

	    HsxSBWidgetInfo defaultNavigatorWidInfo = new HsxSBWidgetInfo();
	    defaultNavigatorWidInfo.setCode(formWidget.getWidgetInfo()
		    .getDefaultNavigator());
	    defaultNavigatorWidInfo.setType(TYPE_DEFAULT_NAVIGATOR);
	    defaultNavigatorButtonWidget.setWidgetInfo(defaultNavigatorWidInfo);
	    HsxSBRenderer renderer = HsxSBHTMLRendererFactory
		    .getRendererInstance(TYPE_DEFAULT_NAVIGATOR);
	    //System.out.println("renderer :: " + renderer);
	    if (renderer != null) {
		defaultNavigatinButton = renderer
			.renderContent(defaultNavigatorButtonWidget);
	    }
	    contentBuilder.append(defaultNavigatinButton);
//	    System.out
//		    .println("In the button renderer for defaultNavigatorButtonWidget:: "
//			    + defaultNavigatinButton);
	}

	Iterator<HsxSBWidget> quesionGroupsListIterator = formWidget
		.getWidgetsList().iterator();

	while (quesionGroupsListIterator.hasNext()) {
	    HsxSBWidget sbWidget = quesionGroupsListIterator.next();
	    if (null != sbWidget.getWidgetInfo().getIsRenderingRequired() && STR_NO.equalsIgnoreCase(sbWidget.getWidgetInfo().getIsRenderingRequired())) {
			//No action required.
	    } else {
	    HsxSBRenderer renderer = HsxSBHTMLRendererFactory
		    .getRendererInstance(sbWidget.getWidgetInfo().getType());
	    String rendererd = renderer.renderContent(sbWidget);
	    contentBuilder.append(rendererd);
	    }
	}

	if (formWidget.getWidgetInfo().getFormName() != null) {

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_FORM);
	    contentBuilder.append(HTML_END_TAG);
	}
	return contentBuilder.toString();

    }

}
