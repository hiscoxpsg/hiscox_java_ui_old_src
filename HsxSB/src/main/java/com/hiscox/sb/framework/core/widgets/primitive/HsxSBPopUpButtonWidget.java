package com.hiscox.sb.framework.core.widgets.primitive;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
/**
 * This class holds the characteristics of pop up buttons of Option tag. and implements the basic
 * functionality defined by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:28 AM
 */
public class HsxSBPopUpButtonWidget extends HsxSBBaseWidget {
    /**
	 *
	 */
	private static final long serialVersionUID = -3660644707455846455L;

	public HsxSBPopUpButtonWidget() {
	super();
    }
    public HsxSBPopUpButtonWidget(String id, String name) {

	super(id, name);
    }

    public HsxSBPopUpButtonWidget(String id, String name,
	    HsxSBWidgetInfo widgetInfo) {

	super(id, name, widgetInfo);
    }
/**
 * This method is used to create the widget.
 * @param widgetInfo contains widget Info
 */

    @Override
    public void createWidget(HsxSBWidgetInfo widgetInfo) {
	this.setWidgetInfo(widgetInfo);
	this.getWidgetInfo().setStyle("");

    }

}
