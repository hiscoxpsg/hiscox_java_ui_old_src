package com.hiscox.sb.framework.core.widgets.primitive;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class holds the characteristics of Anchor tag. and implements the basic
 * functionality defined by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:04 AM
 */
public class HsxSBAnchorWidget extends HsxSBBaseWidget {

	/**
	 *
	 */
	private static final long serialVersionUID = -4077646267796925065L;

	public HsxSBAnchorWidget() {

		super();
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 * @param widgetInfo contains info of widget
	 */
	public HsxSBAnchorWidget(String id, String name, HsxSBWidgetInfo widgetInfo) {

		super(id, name, widgetInfo);
	}

	/**
	 *
	 * @param id contains id
	 * @param name contains name
	 */
	public HsxSBAnchorWidget(String id, String name) {

		super(id, name);
	}

	/**
	 * Method to create widget by setting widget info, Style hint etc.
	 *
	 * @param widgetInfo contains info of widget
	 */
	@Override
	public void createWidget(HsxSBWidgetInfo widgetInfo) {

		this.setWidgetInfo(widgetInfo);
		this.getWidgetInfo().setStyle("");
	}

}
