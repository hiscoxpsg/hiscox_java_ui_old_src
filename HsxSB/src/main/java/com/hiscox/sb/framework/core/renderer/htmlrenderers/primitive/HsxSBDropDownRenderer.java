package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.util.HsxSBWidgetFactory;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBDropDownWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBOptionWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBPermitedValues;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML drop down tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:13 AM
 */
public class HsxSBDropDownRenderer extends HsxSBHTMLBaseRenderer implements
		HsxSBUIBuilderConstants {
	/**
	 * Initialization of OptionList.
	 */
	List<HsxSBWidget> optionsList = new ArrayList<HsxSBWidget>();
	/**
	 * Initialization of permitted values.
	 */
	List<HsxSBPermitedValues> permitedValues;

	/**
	 * This method will return the String for rendering the HTML Drop down.
	 * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws runtime exception
	 */
	@Override
	public String renderContent(final HsxSBWidget widget)
			throws HsxSBUIBuilderRuntimeException {
		StringBuilder contentBuilder = new StringBuilder();

		HsxSBDropDownWidget dropDownWidget = (HsxSBDropDownWidget) widget;

		String savedValue = null;

		this.permitedValues = dropDownWidget.getWidgetInfo()
				.getPermitedVlaues();

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_CONTROL_DIV_STYLE);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_END_TAG);
		Map<String, HsxSBTextManagerVO> textMap = HsxSBUIBuilderControllerImpl.textMap;
		if (HTML_CURRENCY.equalsIgnoreCase(dropDownWidget.getWidgetInfo()
				.getSubType())
				&& HsxSBUtil.isNotBlank(dropDownWidget.getWidgetInfo()
						.getCurrency())) {

			StringBuilder currTextCodeBldr = new StringBuilder(STR_EMPTY);
			currTextCodeBldr.append(dropDownWidget.getWidgetInfo()
					.getCurrency());
			currTextCodeBldr.append(STR_CURRENCY_KEY);
			final String currTextCode = currTextCodeBldr.toString();
			if (currTextCodeBldr != null && textMap != null
					&& textMap.containsKey(currTextCode)) {

				final String currLabelText = textMap.get(currTextCode)
						.getLabelText();
				if (HsxSBUtil.isNotBlank(currLabelText)) {
					contentBuilder.append(HTML_START_TAG);
					contentBuilder.append(HTML_SPAN);
					contentBuilder.append(HTML_END_TAG);
					contentBuilder.append(currLabelText);
					contentBuilder.append(HTML_START_TAG);
					contentBuilder.append(HTML_FRONT_SLASH);
					contentBuilder.append(HTML_SPAN);
					contentBuilder.append(HTML_END_TAG);
				}
			}

		}
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_SELECT);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(dropDownWidget.getWidgetInfo().getStyle());
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_NAME);
		contentBuilder.append(HTML_EQUALS);
		if (null != dropDownWidget.getWidgetInfo().getName()) {
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(dropDownWidget.getWidgetInfo().getName());
			contentBuilder.append(HTML_SINGLE_QUOTE);
		} else {
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(dropDownWidget.getWidgetInfo().getId());
			contentBuilder.append(HTML_SINGLE_QUOTE);
		}
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_ID);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(dropDownWidget.getWidgetInfo().getId());
		contentBuilder.append(DROPDOWN_ID_SUFFIX);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);

		if (HTML_YES.equalsIgnoreCase(dropDownWidget.getWidgetInfo()
				.getDisplayOnly())) {
			contentBuilder.append(HTML_DISABLED);
			contentBuilder.append(HTML_EQUALS);
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(HTML_DISABLED);
			contentBuilder.append(HTML_SINGLE_QUOTE);
		}
		contentBuilder.append(HTML_END_TAG);
		String ddWidgetDefValue = dropDownWidget.getWidgetInfo()
				.getDefaultValue();
		// Add a empty Option if screenQuestion has Blank as its default Value

		if (HsxSBUtil
				.isNotBlank(dropDownWidget.getWidgetInfo().getSavedValue())) {
			savedValue = dropDownWidget.getWidgetInfo().getSavedValue();
		} else if (!dropDownWidget.getWidgetInfo().isErrorIndicator()
				&& HsxSBUtil.isNotBlank(ddWidgetDefValue) && "Blank".equalsIgnoreCase(ddWidgetDefValue)) {
				savedValue = STR_EMPTY;
			}
		
		if (STR_BLANK_VALUE.equalsIgnoreCase(ddWidgetDefValue)
				|| HsxSBUtil.isBlank(ddWidgetDefValue)) {
			HsxSBWidget blnkOptionWidget = HsxSBWidgetFactory
					.getWidgetInstance(HTML_OPTION, STR_EMPTY, STR_EMPTY);
			HsxSBWidgetInfo blnkWidgetInfo = new HsxSBWidgetInfo();
			blnkWidgetInfo.setDefaultValue(STR_EMPTY);
			blnkWidgetInfo.setSavedValue(savedValue);
			blnkWidgetInfo.setDisplayText(STR_EMPTY);
			blnkOptionWidget.createWidget(blnkWidgetInfo);
			optionsList.add(blnkOptionWidget);
		}

		for (HsxSBPermitedValues permittedValue : this.permitedValues) {
			HsxSBWidget optionWidget = HsxSBWidgetFactory.getWidgetInstance(
					HTML_OPTION, STR_EMPTY, STR_EMPTY);
			HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();
			widgetInfo.setDefaultValue(permittedValue.getValue());
			widgetInfo.setSavedValue(dropDownWidget.getWidgetInfo()
					.getSavedValue());
			String displayValue = permittedValue.getDisplayText();
			if (displayValue != null
					&& textMap.containsKey(displayValue.trim())) {
				displayValue = textMap.get(displayValue.trim()).getLabelText();
			}
			widgetInfo.setDisplayText(displayValue);
			optionWidget.createWidget(widgetInfo);
			optionsList.add(optionWidget);
		}

		Iterator<HsxSBWidget> optionsListIterator = optionsList.iterator();
		int optionIncrementor = 0;
		while (optionsListIterator.hasNext()) {
			HsxSBWidget optWidget = optionsListIterator.next();
			if (null != savedValue) {
				if (savedValue.equalsIgnoreCase(((HsxSBOptionWidget) optWidget)
						.getWidgetInfo().getSavedValue())) {
					((HsxSBOptionWidget) optWidget).setSelected(true);
				} else {
					((HsxSBOptionWidget) optWidget).setSelected(false);
				}
			}
			optWidget.getWidgetInfo().setType(HTML_OPTION);
			optionIncrementor++;
			String optionId = dropDownWidget.getWidgetInfo().getId()
					+ optionIncrementor;
			optWidget.getWidgetInfo().setId(optionId);
			HsxSBRenderer renderer = HsxSBHTMLRendererFactory
					.getRendererInstance(optWidget.getWidgetInfo().getType());
			String rendererd = renderer.renderContent(optWidget);
			contentBuilder.append(rendererd);
		}

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_SELECT);
		contentBuilder.append(HTML_END_TAG);
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_END_TAG);

		String includeHelp = HsxSBWidgetUtil.addHelpRenderer(dropDownWidget);
		if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
				.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
			
				String includeAdditionalText4 = HsxSBWidgetUtil
						.addAdditionalText4Renderer(dropDownWidget);
				contentBuilder.append(includeAdditionalText4);
			}
	

		contentBuilder.append(includeHelp);

		if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
			if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
					.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
				String includeAdditionalText1 = HsxSBWidgetUtil
						.addAdditionalTextOneRenderer(dropDownWidget);
				contentBuilder.append(includeAdditionalText1);
			}
			if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
					.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
				String includeAdditionalText2 = HsxSBWidgetUtil
						.addAdditionalTextTwoRenderer(dropDownWidget);
				contentBuilder.append(includeAdditionalText2);
			}
		}
		if (dropDownWidget.getWidgetInfo().isErrorIndicator()) {
			String includeError = HsxSBWidgetUtil
					.addErrorRenderer(dropDownWidget);
			contentBuilder.append(includeError);
		}
		return contentBuilder.toString();
	}

}
