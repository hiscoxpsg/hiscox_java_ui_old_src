package com.hiscox.sb.framework.core;

import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;


/**
 * This interface defines basic functionalities to render the tag.
 *
 * @author Cognizant
 * @version 1.0
 */
public interface HsxSBRenderer {

    /**
     * Method to render the tags content. For e,g HTML content, JSF content etc.
     * @param widget contains widget
     * @return Content
     * @throws HsxSBUIBuilderRuntimeException
     */
    String renderContent(HsxSBWidget widget) throws HsxSBUIBuilderRuntimeException;

}
