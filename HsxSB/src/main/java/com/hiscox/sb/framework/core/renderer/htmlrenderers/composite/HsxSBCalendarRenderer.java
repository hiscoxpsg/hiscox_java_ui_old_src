package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBCalendarWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
/**
 * This class renders the html tags to display calendar.
 * @author Cognizant
 * @version 1.0
 */
public class HsxSBCalendarRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * * This method will return the String for rendering the HTML Help tag.
     * @param widget contains widget data
     * @throws HsxSBUIBuilderRuntimeException throws run time exception
     * @return Content in the form of string
     */

    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();
	HsxSBCalendarWidget calendarWidget = (HsxSBCalendarWidget) widget;
	String value = null;
	if (null != calendarWidget.getWidgetInfo().getSavedValue()
		&& calendarWidget.getWidgetInfo().getSavedValue().trim()
			.length() > 0) {
	    value = calendarWidget.getWidgetInfo().getSavedValue();
	} else {
	    value = calendarWidget.getWidgetInfo().getDefaultValue();
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CALENDARICON_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_HREF);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(value);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(calendarWidget.getWidgetInfo().getStyle());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(calendarWidget.getWidgetInfo().getId());
	contentBuilder.append(HELP_ID_SUFFIX);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TARGET);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(calendarWidget.getWidgetInfo().getTarget());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TABINDEX);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(calendarWidget.getWidgetInfo().getTabIndex());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_HREF);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_HASH);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INPUT);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TYPE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_HIDDEN);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(calendarWidget.getWidgetInfo().getHelpText());
	contentBuilder.append(HTML_SINGLE_QUOTE);

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(calendarWidget.getWidgetInfo().getId());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CLOSE_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
