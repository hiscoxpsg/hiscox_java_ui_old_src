package com.hiscox.sb.framework.core.util.bean;

import java.io.Serializable;

/**
 * HsxSBJSDependencyBean This bean is used to format the JavaScript Hidden
 * String.
 * @author Cognizant
 */
public class HsxSBJSDependencyBean implements Serializable {
/**
 */
	private static final long serialVersionUID = -6023274052020883721L;
	/**
	 * variable initialisation.
	 */
	private String parentQuestionElementId = null;
	/**
	 * variable initialisation.
	 */
	private String eventDescription = null;
	/**
	 * variable initialisation.
	 */
	private String elementValue = null;
	/**
	 * variable initialisation.
	 */
	private String childQuestionDivId = null;
	/**
	 * variable initialisation.
	 */
	private String displayHint = null;
/**
 * Getter for parentQuestionElementId.
 * @return parentQuestionElementId
 */
	public String getParentQuestionElementId() {
		return parentQuestionElementId;
	}
/**
 * setter for parentQuestionElementId.
 * @param parentQuestionElementId input parameter
 */
	public void setParentQuestionElementId(String parentQuestionElementId) {
		this.parentQuestionElementId = parentQuestionElementId;
	}
/**
 * getter for childQuestionDivId.
 * @return childQuestionDivId
 */
	public String getChildQuestionDivId() {
		return childQuestionDivId;
	}
/**
 * setter for childQuestionDivId.
 * @param childQuestionDivId input parameter
 */
	public void setChildQuestionDivId(String childQuestionDivId) {
		this.childQuestionDivId = childQuestionDivId;
	}
/**
 * getter for eventDescription.
 * @return eventDescription
 */
	public String getEventDescription() {
		return eventDescription;
	}
/**
 * setter for eventDescription.
 * @param eventDescription input parameter
 */
	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}
/**
 * getter for elementValue.
 * @return elementValue
 */
	public String getElementValue() {
		return elementValue;
	}
/**
 * setter for elementValue.
 * @param elementValue input parameter
 */
	public void setElementValue(String elementValue) {
		this.elementValue = elementValue;
	}
/**
 * getter for displayHint.
 * @return displayHint
 */
	public String getDisplayHint() {
		return displayHint;
	}
/**
 * setter for displayHint.
 * @param displayHint input parameter
 */
	public void setDisplayHint(String displayHint) {
		this.displayHint = displayHint;
	}

}
