package com.hiscox.sb.framework.core;

import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This interface defines basic functionalities of widget.
 * @author Cognizant
 * @version 1.0
 */
public interface HsxSBWidget {

	/**
	 * Method for creating widget, like setting widget information, style etc.
	 * @param widgetInfo for creating the info of widgets
	 */
	void createWidget(HsxSBWidgetInfo widgetInfo);

	/**
	 * method for setting the HTML properties.
	 * @param id for setting Ids
	 */
	void setId(String id);
	/**
	 * method for setting the HTML properties.
	 * @param name for setting names
	 */
	void setName(String name);

	/**
	 * This method set the widget information.
	 * @param widgetInfo to set all the info of widgets
	 */
	void setWidgetInfo(HsxSBWidgetInfo widgetInfo);

	/**
	 * this method returns the widget info.
	 * @return WidgetInfo
	 */
	HsxSBWidgetInfo getWidgetInfo();

	/**
	 * this returns type.
	 * @return type
	 */
	String getType();

	/**
	 * This returns Id.
	 * @return Id
	 */
	String getId();
}
