package com.hiscox.sb.framework.core.util;

import java.util.Map;

import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
/**
 * This class is a tri direct data provider in the form of
 * html content.
 * @author Cognizant
 *
 */
public class HsxSBTriDirectTagDataProvider implements HsxSBUIBuilderConstants {
/**
 * @param screenName contains screen name
 * @param customVariablesMap contains custom variables map
 * @return a html string
 */
    public static String provideHTMLContent(String screenName,
	    Map<String, String> customVariablesMap) {
	String htmlString = null;
	switch (getScreenCode(screenName)) {
	    case TELL_US_ABOUT_YOUR_BUSINESS_PAGE:
		// Tell Us About Your Business Help Me Choose - Lifestyle
		// Questions
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "helpy184" + HTML_FRAME_SRC_URL_CATEGORY + "tellu072"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "helpy184"
			+ HTML_FRAME_SRC_URL_CATEGORY + "tellu072"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;
	    case HELP_YOU_CHOOSE_PRODUCT_OPTIONS_PAGE:
		// Help You Choose Product Options Help Me choose - Product
		// Options
		htmlString = HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "helpm234"
			+ HTML_FRAME_SRC_URL_CATEGORY
			+ "helpy988;qty=1;cost=[Revenue];";
		htmlString = checkValueAndAppend(customVariablesMap,
			htmlString, XML_U4)
			+ "ord=[OrderID]?\""
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER
			+ HTML_IFRAME_END_2 + "";
		break;
	    case ABOUT_YOU_REGISTRATION_1_PAGE:
		// About You - Registration #1 Quote
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "regis002" + HTML_FRAME_SRC_URL_CATEGORY + "about381"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "regis002"
			+ HTML_FRAME_SRC_URL_CATEGORY + "about381"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;
	    case YOUR_BUSINESS_REGISTRATION_2_PAGE:
		// Your Business - Registration #2 Quote
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "regis002" + HTML_FRAME_SRC_URL_CATEGORY
			+ "yourb168;";
		htmlString = checkValueAndAppend(customVariablesMap,
			htmlString, XML_U3);

		htmlString = htmlString + HTML_FRAME_SRC_URL_ORD_NUM1
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "regis002"
			+ HTML_FRAME_SRC_URL_CATEGORY + "yourb168;";
		htmlString = checkValueAndAppend(customVariablesMap,
			htmlString, XML_U3);

		htmlString = htmlString + HTML_FRAME_SRC_URL_ORD_NUM_3
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;

	    case YOUR_BUSINESS_REGISTRATION_3_PAGE:
		// Your Business - Registration #3 Quote (BOP only)
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL_SECURE + HTML_FRAME_SRC_URL_TYPE
			+ "regis002" + HTML_FRAME_SRC_URL_CATEGORY + "yourq419"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL_SECURE
			+ HTML_FRAME_SRC_URL_TYPE + "regis002"
			+ HTML_FRAME_SRC_URL_CATEGORY + "yourq419"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;

	    case APPLICATION_DECLINED_REFFERED_PAGE:
		// Application Declined / Referred Quote
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "regis002" + HTML_FRAME_SRC_URL_CATEGORY + "appli035"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "regis002"
			+ HTML_FRAME_SRC_URL_CATEGORY + "appli035"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;

	    case YOUR_QUOTE_REGISTRATION_4_PAGE:
		// Your quote - Registration #4 Quote
		htmlString = HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "regis458"
			+ HTML_FRAME_SRC_URL_CATEGORY
			+ "yourq936;qty=1;cost=[Revenue];";
		htmlString = checkValueAndAppend(customVariablesMap,
			htmlString, XML_U1);

		htmlString = htmlString + "ord=[OrderID]?\""
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ "";
		break;

	    case IMPORTANT_STATEMENTS_PAGE:
		// Important Statements Post-quote
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "postq575" + HTML_FRAME_SRC_URL_CATEGORY + "impor318"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "postq575"
			+ HTML_FRAME_SRC_URL_CATEGORY + "impor318"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;

	    case APPLICATION_SUMMARY_PAGE:
		// Application Summary Post-quote
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "postq575" + HTML_FRAME_SRC_URL_CATEGORY + "appli777"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "postq575"
			+ HTML_FRAME_SRC_URL_CATEGORY + "appli777"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;

	    case PAYMENT_DETAILS_PAGE:
		// Payment Details Post-quote
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "postq575" + HTML_FRAME_SRC_URL_CATEGORY + "payme337"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "postq575"
			+ HTML_FRAME_SRC_URL_CATEGORY + "payme337"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;
	    case CONFIRMATION_PAGE:

		// Confirmation Page Buy
		htmlString = HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "buy-s466"
			+ HTML_FRAME_SRC_URL_CATEGORY + "confi102"
			+ ";qty=1;cost=[Revenue];";
		htmlString = checkValueAndAppend(customVariablesMap,
			htmlString, XML_U5);
		htmlString = checkValueAndAppend(customVariablesMap,
			htmlString, XML_U4);
		htmlString = checkValueAndAppend(customVariablesMap,
			htmlString, XML_U3);
		htmlString = checkValueAndAppend(customVariablesMap,
			htmlString, XML_U2);
		htmlString = checkValueAndAppend(customVariablesMap,
			htmlString, XML_U1);
		htmlString = htmlString + "ord=[OrderID]?\""
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2;
		break;
	    case PAY_BY_PHONE_PAGE:
		// Pay by Phone Buy
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "buyiu478" + HTML_FRAME_SRC_URL_CATEGORY + "payby136"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "buyiu478"
			+ HTML_FRAME_SRC_URL_CATEGORY + "payby136"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;
	    case SAVE_QUOTE_PAGE:
		// Save Quote Save Quote
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "saveq972" + HTML_FRAME_SRC_URL_CATEGORY + "saveq218"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "saveq972"
			+ HTML_FRAME_SRC_URL_CATEGORY + "saveq218"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;
	    case CALL_BACK_REQUEST_PAGE:
		// Call back request Call back request
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "callb331" + HTML_FRAME_SRC_URL_CATEGORY + "callb151"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "callb331"
			+ HTML_FRAME_SRC_URL_CATEGORY + "callb151"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;
	    case PRODUCT_SELECTION_HELP_ME_CHOOSE:
		// Life style questions
		htmlString = HTML_JAVASCRIPT_START + HTML_IFRAME_START
			+ HTML_FRAME_SRC_URL + HTML_FRAME_SRC_URL_TYPE
			+ "helpy184" + HTML_FRAME_SRC_URL_CATEGORY + "tellu072"
			+ HTML_FRAME_SRC_URL_ORD_NUM
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END
			+ HTML_JAVASCRIPT_END + HTML_JAVA_NOSCRIPT_START
			+ HTML_IFRAME_START + HTML_FRAME_SRC_URL
			+ HTML_FRAME_SRC_URL_TYPE + "helpy184"
			+ HTML_FRAME_SRC_URL_CATEGORY + "tellu072"
			+ HTML_FRAME_SRC_URL_ORD_NUM_2
			+ HTML_WIDTH_HEIGHT_FRAMEBORDER + HTML_IFRAME_END_2
			+ HTML_JAVA_NOSCRIPT_END;
		break;
	}
	return htmlString;
    }
/**
 * @param customVariablesMap contains a custom variable map
 * @param htmlString contains a html string
 * @param key contains a key
 * @return htmlString
 */
    private static String checkValueAndAppend(
	    Map<String, String> customVariablesMap, String htmlString,
	    String key) {
	if ((customVariablesMap != null && !customVariablesMap.isEmpty())
		&& customVariablesMap.containsKey(key)) {
	    htmlString = htmlString + key + "=" + customVariablesMap.get(key)
		    + ";";
	}
	return htmlString;
    }
/**
 * @param screenName contains name of the screen
 * @return screenCode
 */
    private static int getScreenCode(String screenName) {
	int screenCode = 0;

	if ("LifestyleQuestions".equalsIgnoreCase(screenName)) {
	    screenCode = PRODUCT_SELECTION_HELP_ME_CHOOSE;
	} else if ("LetUsCallYou".equalsIgnoreCase(screenName)) {
	    screenCode = TELL_US_ABOUT_YOUR_BUSINESS_PAGE;
	} else if ("ProductOptions".equalsIgnoreCase(screenName)) {
	    screenCode = HELP_YOU_CHOOSE_PRODUCT_OPTIONS_PAGE;
	} else if ("AboutYou".equalsIgnoreCase(screenName)) {
	    screenCode = ABOUT_YOU_REGISTRATION_1_PAGE;
	} else if ("YourBusiness".equalsIgnoreCase(screenName)) {
	    screenCode = YOUR_BUSINESS_REGISTRATION_2_PAGE;
	} else if ("YourBusiness2".equalsIgnoreCase(screenName)) {
	    screenCode = YOUR_BUSINESS_REGISTRATION_3_PAGE;
	} else if ("Declined".equalsIgnoreCase(screenName) || "Referred".equalsIgnoreCase(screenName)) {
	    screenCode = APPLICATION_DECLINED_REFFERED_PAGE;
	} else if ("YourQuote".equalsIgnoreCase(screenName)) {
	    screenCode = YOUR_QUOTE_REGISTRATION_4_PAGE;
	} else if ("ImportantInformation".equalsIgnoreCase(screenName)) {
	    screenCode = IMPORTANT_STATEMENTS_PAGE;
	} else if ("ApplicationSummary".equalsIgnoreCase(screenName)) {
	    screenCode = APPLICATION_SUMMARY_PAGE;
	} else if ("PaymentDetails".equalsIgnoreCase(screenName)) {
	    screenCode = PAYMENT_DETAILS_PAGE;
	} else if ("Confirmation".equalsIgnoreCase(screenName)) {
	    screenCode = CONFIRMATION_PAGE;
	} else if ("PayByPhone".equalsIgnoreCase(screenName)) {
	    screenCode = PAY_BY_PHONE_PAGE;
	} else if ("SaveQuote".equalsIgnoreCase(screenName)) {
	    screenCode = SAVE_QUOTE_PAGE;
	} else if ("WCRequestCallback".equalsIgnoreCase(screenName)) {
	    screenCode = CALL_BACK_REQUEST_PAGE;
	}

	return screenCode;
    }
}
