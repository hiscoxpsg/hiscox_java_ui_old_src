package com.hiscox.sb.framework.core.util;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This factory class gives back appropriate render depending upon the Widget
 * type.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:33 AM
 */
public abstract class HsxSBRendererFactory implements HsxSBUIBuilderConstants {
/**
 * Default constructor.
 */
    public HsxSBRendererFactory() {

    }

    /**
     * Return the HTML renderer based on the renderer/widget type.
     * @return rendererInstance
     * @param rendererType contains renderer type
     */
    public abstract HsxSBRenderer getRendererInstance(String rendererType);

    /**
     * Return the HTML renderer based on the renderer/widget type.
     * @return renderer
     * @param rendererType contains renderer type
     * @param uiContentType contains UI content type
     */
    public static HsxSBRenderer getRendererInstance(String rendererType,
	    int uiContentType) {
	HsxSBRenderer renderer = null;

	switch (uiContentType) {
	    case HTML_CONTENT_TYPE:
		renderer = HsxSBHTMLRendererFactory
			.getRendererInstance(rendererType);
		break;
	    case JSF_CONTENT_TYPE:
		renderer = HsxSBHTMLRendererFactory
			.getRendererInstance(rendererType);
		break;
	    default:
		break;
	}
	return renderer;
    }

}
