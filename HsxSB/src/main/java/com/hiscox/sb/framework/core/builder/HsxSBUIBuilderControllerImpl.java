package com.hiscox.sb.framework.core.builder;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.hiscox.sb.framework.binding.HsxSBUIRequestBinder;
import com.hiscox.sb.framework.core.HsxSBUIBuilderController;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.util.HsxSBRequestProcessor;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBFormWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionGroupWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderException;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;
import com.hiscox.sb.framework.validator.HsxSBUIValidator;

/**
 * This class is the Entry point to build widgets, render HTML content and to
 * construct the response XML(back from widget to XML). This class uses
 * RequestProcessor and ResponseProcessor to achieve the same.
 * @author Cognizant
 */
public class HsxSBUIBuilderControllerImpl
	implements
	    HsxSBUIBuilderController,
	    Serializable,
	    HsxSBUIBuilderConstants {

    private static final long serialVersionUID = -1212332496927559592L;
    private HsxSBWidget widget = null;
    private HsxSBWidget actionWidget = null;
    private Map<String, HsxSBWidget> widgetMap = new HashMap<String, HsxSBWidget>();
    public static Map<String, HsxSBTextManagerVO> textMap = new HashMap<String, HsxSBTextManagerVO>();
    private Map<String, String> uiWidgetDependencyMap = null;
    private static Map<String, String> requestValueMap = new HashMap<String, String>();
    public static StringBuilder toolTipHiddenJsString = new StringBuilder();
    public static StringBuilder calendarHiddenJsString = new StringBuilder();
    public static String resourcesContextPath = STR_RESOURCE_CONTEXT_PATH_VALUE;
    public static Properties widgetTypesProperties = new Properties();
    public static Properties validationRegExpProperties = new Properties();
    public Map<String, String> sessionMap = new HashMap<String, String>();
    public static Map<String, String> additionalInfoMap = new HashMap<String, String>();
    public static String screenName = STR_EMPTY;
    public static List<Integer> questionOrder = new ArrayList<Integer>();
    public static String previousScreen = null;
    public static HashMap<String, String> widgetHintMap = new HashMap<String, String>();

    public static String getPreviousScreen() {
	return previousScreen;
    }

    public static void setPreviousScreen(String previousScreen) {
	HsxSBUIBuilderControllerImpl.previousScreen = previousScreen;
    }

    public static Map<String, String> getAdditionalInfoMap() {
	return additionalInfoMap;
    }

    public static void setAdditionalInfoMap(
	    Map<String, String> additionalInfoMap) {
	HsxSBUIBuilderControllerImpl.additionalInfoMap = additionalInfoMap;
    }

    public Map<String, String> getSessionMap() {
	if (sessionMap == null) {
	    this.sessionMap = new HashMap<String, String>();
	}
	return this.sessionMap;
    }

    public void setSessionMap(Map<String, String> sessionMap) {
	this.sessionMap = sessionMap;
    }

    public HsxSBWidget getActionWidget() {
	return actionWidget;
    }

    public Map<String, HsxSBTextManagerVO> getTextMap() {
	if (textMap == null) {
	    textMap = new HashMap<String, HsxSBTextManagerVO>();
	}
	return textMap;
    }

    public static void setTextMap(Map<String, HsxSBTextManagerVO> textMap) {
	HsxSBUIBuilderControllerImpl.textMap = textMap;
    }

    public static Map<String, String> getRequestValueMap() {
	return requestValueMap;
    }

    public static void setRequestValueMap(Map<String, String> requestValueMap) {
	HsxSBUIBuilderControllerImpl.requestValueMap = requestValueMap;
    }

    public void setActionWidget(HsxSBWidget actionWidget) {
	this.actionWidget = actionWidget;
    }

    /**
     * Default constructor.
     */
    public HsxSBUIBuilderControllerImpl() {
	super();
    }

    /**
     * This method uses the HsxSBRequestProcessor to tri direct tag, takes
     * String XML as an input for tag list.
     * @param renderDocument
     * @param textMap
     * @return htmlString
     * @throws HsxSBUIBuilderRuntimeException
     */
    public String generateFloodLightsHTML(Document renderDocument)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {

	String htmlString = STR_EMPTY;
	try {

	    htmlString = HsxSBRequestProcessor
		    .buildTriDirectHTML(renderDocument);
	} catch (RuntimeException e) {
	    e.printStackTrace();
	    throw new HsxSBUIBuilderRuntimeException(e.getMessage());
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new HsxSBUIBuilderException(e.getMessage());
	}
	return htmlString;
    }

    /**
     * This method uses the HsxSBRequestProcessor to build widget and render the
     * HTML string, takes String XML as an input for the action list in the
     * screen.
     * @param renderDocument
     * @param textMap
     * @return htmlString
     * @throws HsxSBUIBuilderRuntimeException
     */
    public String generateActionListHTML(Document renderDocument,
	    Map<String, HsxSBTextManagerVO> textMap, int uiContentType,
	    Properties widgetTypeProperties, Map<String, String> sessionMap,
	    String isJSEnabled) throws HsxSBUIBuilderRuntimeException,
	    HsxSBUIBuilderException {

	String htmlString = STR_EMPTY;
	if (renderDocument != null) {
	    try {

		// getting text manager data

		actionWidget = HsxSBRequestProcessor.buildActionWidget(textMap,
			widgetMap, widget, renderDocument,
			widgetTypeProperties, sessionMap, isJSEnabled);

		if (actionWidget != null) {
		    htmlString = getHtmlContent(actionWidget, uiContentType);
		    HsxSBUIBuilderControllerImpl.questionOrder.clear();
		}
	    } catch (RuntimeException e) {
		e.printStackTrace();
		throw new HsxSBUIBuilderRuntimeException(e.getMessage());
	    } catch (Exception e) {
		e.printStackTrace();
		throw new HsxSBUIBuilderException(e.getMessage());
	    }
	}
	return htmlString;
    }

    /**
     * This method uses the HsxSBRequestProcessor to build widget and render the
     * HTML string, takes String XML as an input.
     * @param renderXML
     * @throws HsxSBUIBuilderRuntimeException
     * @return Html content
     */
    public String generateHTML(String xMLString,
	    Map<String, HsxSBTextManagerVO> textMap, String isJSEnabled,
	    int uiContentType, Properties widgetTypeProperties,
	    Map<String, String> additionalInfoMap,
	    Map<String, String> sessionMap, String previousScreen)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	factory.setNamespaceAware(true);
	Document xmlDocument = null;
	try {
	    // getting text manager data
	    setTextMap(textMap);
	    // getting session manager data
	    setSessionMap(sessionMap);

	    DocumentBuilder builder = factory.newDocumentBuilder();
	    xmlDocument = builder.parse(new InputSource(new StringReader(
		    xMLString)));

	} catch (Exception e) {
	    throw new HsxSBUIBuilderException(e.getMessage());
	}
	return generateHTML(xmlDocument, textMap, isJSEnabled, uiContentType,
		widgetTypeProperties, additionalInfoMap, sessionMap,
		previousScreen);
    }

    /**
     * This method uses the HsxSBRequestProcessor to build widget and render the
     * HTML string, takes Document as an input.
     * @param renderDocument
     * @return htmlString
     * @throws HsxSBUIBuilderRuntimeException
     */
    public String generateHTML(Document renderDocument,
	    Map<String, HsxSBTextManagerVO> textMap, String isJSEnabled,
	    int uiContentType, Properties widgetTypeProperties,
	    Map<String, String> additionalInfoMap,
	    Map<String, String> sessionMap, String previousScreen)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {
	String htmlString = STR_EMPTY;
	if (renderDocument != null) {
	    try {
		// getting text manager data
		setTextMap(textMap);
		// getting session manager data
		setSessionMap(sessionMap);

		setPreviousScreen(previousScreen);

		widget = HsxSBRequestProcessor.buildWidget(textMap, widgetMap,
			widget, renderDocument, isJSEnabled,
			widgetTypeProperties, sessionMap);
		if (STR_YES.equalsIgnoreCase(isJSEnabled)) {
		    uiWidgetDependencyMap = new HashMap<String, String>();
		    uiWidgetDependencyMap = HsxSBWidgetUtil
			    .createUIWidgetHideUnHideMap(widget,
				    uiWidgetDependencyMap, widgetMap);

		}
		HsxSBUIBuilderControllerImpl
			.setAdditionalInfoMap(additionalInfoMap);
		// if (uiWidgetDependencyMap != null)
		// {
		// Collection<String> c = uiWidgetDependencyMap.keySet();
		// for (Iterator<String> iterator = c.iterator(); iterator
		// .hasNext();)
		// {
		// String key = iterator.next();
		// System.out.println("Key in builder :" + key
		// + "  Value :" + uiWidgetDependencyMap.get(key));
		//
		// }
		// }
		updateQuestions(widget, widgetMap, uiWidgetDependencyMap,
			isJSEnabled);

		// HashMap<String,String> depValidationMap = (HashMap<String,
		// String>) dependencyValidationMap;
		// Collection<String> coll = depValidationMap.keySet();
		// System.out.println("****Validation Testing****");
		// for (Iterator<String> iterator = coll.iterator();
		// iterator.hasNext();)
		// {
		// String key = iterator.next();
		// System.out.println("Key :"+key
		// +"value :"+depValidationMap.get(key));
		//
		// }

		htmlString = getHtmlContent(widget, uiContentType);
	    } catch (RuntimeException e) {
		e.printStackTrace();
		throw new HsxSBUIBuilderRuntimeException(e.getMessage());
	    } catch (Exception e) {
		throw new HsxSBUIBuilderException(e.getMessage());
	    }
	}
	return htmlString;
    }

    public String getHtmlContent(HsxSBWidget sbWidget, int uiContentType)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {
	return HsxSBRequestProcessor.buildHTMLContent(sbWidget, uiContentType);
    }

    /**
     * This method uses the HsxSBRequestProcessor to render the HTML string,
     * takes built Widget as an input.
     * @param renderDocument
     * @return htmlString
     * @throws HsxSBUIBuilderRuntimeException
     */
    public String generateHTML(HsxSBWidget widget, int uiContentType)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {
	String htmlString = STR_EMPTY;
	try {
	    // getting text manager data
	    setTextMap(textMap);
	    // getting session manager data
	    setSessionMap(sessionMap);
	    htmlString = HsxSBRequestProcessor.buildHTMLContent(widget,
		    uiContentType);
	} catch (HsxSBUIBuilderRuntimeException e) {
	    throw e;
	} catch (RuntimeException e) {
	    throw new HsxSBUIBuilderRuntimeException(e.getMessage());
	} catch (Exception e) {
	    throw new HsxSBUIBuilderException(e.getMessage());
	}
	return htmlString;
    }

    public HsxSBWidget getWidget() {
	return this.widget;
    }

    /**
     * This method hide/show the screen level error widget based on screen level
     * error indicator.
     * @param formDivWidget
     * @throws HsxSBUIBuilderRuntimeException
     */
    private void updateQuestions(HsxSBWidget widget,
	    Map<String, HsxSBWidget> widgetMap,
	    Map<String, String> uiRenderDependencyMap, String isJSEnabled)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {
	try {
	    HsxSBScreenWidget screenWidget = (HsxSBScreenWidget) widget;
	    if (screenWidget != null && screenWidget.getWidgetInfo() != null) {
		String errorStyle = screenWidget.getWidgetInfo()
			.getErrorStyle();
		if (screenWidget.getWidgetInfo().isErrorIndicator()) {
		    // show the common error message
		    errorStyle = errorStyle.replaceAll(CSS_HIDE_CLASS,
			    STR_EMPTY);
		    screenWidget.getWidgetInfo().setErrorStyle(errorStyle);
		} else {
		    // hide the common error message
		    errorStyle = errorStyle.replaceAll(CSS_HIDE_CLASS,
			    STR_EMPTY);
		    errorStyle = errorStyle.concat(STR_SINGLE_SPACE
			    + CSS_HIDE_CLASS);
		    screenWidget.getWidgetInfo().setErrorStyle(errorStyle);
		}
		ArrayList<HsxSBWidget> formWidgets = (ArrayList<HsxSBWidget>) screenWidget
			.getWidgetsList();

		for (Iterator<HsxSBWidget> formIterator = formWidgets
			.iterator(); formIterator.hasNext();) {
		    HsxSBFormWidget formWidget = (HsxSBFormWidget) formIterator
			    .next();
		    if (formWidget != null) {
			ArrayList<HsxSBWidget> groupWidgets = (ArrayList<HsxSBWidget>) formWidget
				.getWidgetsList();

			for (Iterator<HsxSBWidget> groupIterator = groupWidgets
				.iterator(); groupIterator.hasNext();) {
			    HsxSBWidget subFormWidget = groupIterator.next();
			    if (subFormWidget != null
				    && subFormWidget.getWidgetInfo() != null
				    && !TYPE_ACTIONLIST
					    .equalsIgnoreCase(subFormWidget
						    .getWidgetInfo().getType())
				    && (!TYPE_HIDDEN
					    .equalsIgnoreCase(subFormWidget
						    .getWidgetInfo().getType()))) {

				HsxSBQuestionGroupWidget groupWidget = (HsxSBQuestionGroupWidget) subFormWidget;
				updateQuestionGroupWidget(groupWidget,
					widgetMap, uiRenderDependencyMap,
					isJSEnabled);
				// hideUnHideWidgetsOnLoad(uiRenderDependencyMap,
				// groupWidget);
			    }

			}
		    }

		}
	    }
	} catch (RuntimeException e) {
	    e.printStackTrace();
	    throw new HsxSBUIBuilderRuntimeException(e.getMessage());
	} catch (Exception e) {
	    throw new HsxSBUIBuilderException(e.getMessage());
	}

    }

    /**
     * This method adds an error widget on top the question if the corresponding
     * question has error indicator set after validation.
     * @param widget
     * @param widgetMap
     * @param uiRenderDependencyMap
     */
    private void updateQuestionGroupWidget(HsxSBWidget widget,
	    Map<String, HsxSBWidget> widgetMap,
	    Map<String, String> uiRenderDependencyMap, String isJSEnabled) {
	HsxSBQuestionGroupWidget groupWidget = (HsxSBQuestionGroupWidget) widget;
	ArrayList<HsxSBWidget> screenWidgets = (ArrayList<HsxSBWidget>) groupWidget
		.getWidgetsList();
	hideUnHideWidgetsOnLoad(uiRenderDependencyMap, groupWidget, isJSEnabled);
	if (screenWidgets != null
		&& screenWidgets.size() > 0
		&& screenWidgets.get(0).getWidgetInfo() != null
		&& (!TYPE_TABLE.equalsIgnoreCase(screenWidgets.get(0)
			.getWidgetInfo().getType()))) {
	    for (Iterator<HsxSBWidget> questionIterator = screenWidgets
		    .iterator(); questionIterator.hasNext();) {

		HsxSBQuestionWidget screenRowWidget = (HsxSBQuestionWidget) questionIterator
			.next();
		String styleHint = screenRowWidget.getWidgetInfo()
			.getStyleHint();

		if (screenRowWidget.getWidgetInfo().isErrorIndicator()
			&& styleHint != null) {
		    styleHint = styleHint
			    .replaceAll(CSS_ERROR_CLASS, STR_EMPTY);
		    styleHint = styleHint.concat(STR_SINGLE_SPACE).concat(
			    CSS_ERROR_CLASS);
		} else if (styleHint != null) {
		    styleHint = styleHint
			    .replaceAll(CSS_ERROR_CLASS, STR_EMPTY);

		}

		screenRowWidget.getWidgetInfo().setStyleHint(styleHint);
		updateScreenQuestions(screenRowWidget, widgetMap,
			uiRenderDependencyMap, isJSEnabled);
		hideUnHideWidgetsOnLoad(uiRenderDependencyMap, screenRowWidget,
			isJSEnabled);
	    }
	}
    }

    /**
     * This Method will hide/unHide the Widgets based on the
     * uiRenderDependencyMap which has displayHint for all the
     * DependencyQuestions.
     * @param uiRenderDependencyMap
     * @param screenRowWidget
     * @param screenRowWidgetId
     */
    private void hideUnHideWidgetsOnLoad(
	    Map<String, String> uiRenderDependencyMap,
	    HsxSBWidget screenRowWidget, String isJSEnabled) {
	if (uiRenderDependencyMap != null
		&& STR_YES.equalsIgnoreCase(isJSEnabled)) {
	    String screenRowWidgetId = screenRowWidget.getWidgetInfo().getId();
	    Collection<String> collection = uiRenderDependencyMap.keySet();
	    String style = null;

	    for (Iterator<String> iterator = collection.iterator(); iterator
		    .hasNext();) {
		String questionId = iterator.next();

		if (screenRowWidgetId != null
			&& (screenRowWidgetId).equalsIgnoreCase(questionId)) {
		    String displayHint = uiRenderDependencyMap.get(questionId);
		    style = screenRowWidget.getWidgetInfo().getStyleHint();
		    if (STR_HIDE.equalsIgnoreCase(displayHint)) {
			HsxSBWidgetUtil.hideQuestionWidget(screenRowWidget,
				style);
		    } else if (STR_SHOW.equalsIgnoreCase(displayHint)) {
			HsxSBWidgetUtil.unHideQuestionWidget(screenRowWidget,
				style);
		    }

		}
	    }

	}
    }

    /**
     * This method removes the added error widget if any.
     * @param screenRowWidget contains screen widget
     */
    @SuppressWarnings("unused")
	private void removeErrorWidget(HsxSBQuestionWidget screenRowWidget) {
	ArrayList<HsxSBWidget> inputWidgets = (ArrayList<HsxSBWidget>) screenRowWidget
		.getWidgetsList();

	for (Iterator<HsxSBWidget> inputIterator = inputWidgets.iterator(); inputIterator
		.hasNext();) {
		HsxSBWidget inputWidget = inputIterator.next();

	    if (inputWidget != null
		    && TYPE_ERROR.equalsIgnoreCase(inputWidget.getWidgetInfo()
			    .getType())) {
		inputIterator.remove();
		}
	}
	screenRowWidget.setWidgetsList(inputWidgets);
    }

    /**
     * This method updates the question group inside the Screen question.
     * @param screenRowWidget contains screen row widget
     * @param widgetMap is a map with widgets
     * @param uiRenderDependencyMap is a dependeny map
     */
    private void updateScreenQuestions(HsxSBQuestionWidget screenRowWidget,
	    Map<String, HsxSBWidget> widgetMap,
	    Map<String, String> uiRenderDependencyMap, String isJSEnabled) {
	ArrayList<HsxSBWidget> inputWidgets = (ArrayList<HsxSBWidget>) screenRowWidget
		.getWidgetsList();

	for (Iterator<HsxSBWidget> inputIterator = inputWidgets.iterator(); inputIterator
		.hasNext();) {
	    HsxSBWidget inputWidget = inputIterator.next();

	    if (inputWidget != null
		    && inputWidget.getWidgetInfo() != null
		    && TYPE_QUESTION_GROUP.equalsIgnoreCase(inputWidget
			    .getWidgetInfo().getType())) {
		updateQuestionGroupWidget(inputWidget, widgetMap,
			uiRenderDependencyMap, isJSEnabled);
		// hideUnHideWidgetsOnLoad(uiRenderDependencyMap,
		// screenRowWidget);
	    } else if (inputWidget != null && inputWidget.getWidgetInfo() != null) {
		String questionId = inputWidget.getWidgetInfo().getId();
		HsxSBWidget savedWidget = widgetMap.get(questionId);
		if (savedWidget != null && savedWidget.getWidgetInfo() != null) {
			String savedValue = savedWidget.getWidgetInfo()
			    .getSavedValue();
		    inputWidget.getWidgetInfo().setSavedValue(savedValue);
		}
	    }

	}

    }

    public Map<String, HsxSBWidget> getWidgetMap() {
	return this.widgetMap; 
	}

    public void setWidgetMap(Map<String, HsxSBWidget> widgetMap) {
	this.widgetMap = widgetMap;
	}

    public void setWidget(HsxSBWidget widget) {
	this.widget = widget; 
	}

    /**
     * This mthod binds the user entered values(retrieved from request params)
     * to widgets under saved value.
     * @param widgetMap contains widget map
     * @param requestValueMap contains requested values
     * @throws HsxSBUIBuilderRuntimeException
     */
    public void bindValueToWidget(final Map<String, HsxSBWidget> widgetMap,
	    final HashMap<String, String> requestValueMap)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {
	try {
		HsxSBUIRequestBinder.bindToWidgets(widgetMap, requestValueMap);
	    setRequestValueMap(requestValueMap);

	} catch (RuntimeException e) {
	    throw new HsxSBUIBuilderRuntimeException(e.getMessage());
	} catch (Exception e) {
	    throw new HsxSBUIBuilderException(e.getMessage());
	    }
    }

    /**
     * This method validates the screen questions and sets the error indicator
     * for the widgets based on the validation results.
     * @param widget cotains wiget
     * @param dynamicQuestionMap contains dynamic questions
     * @param isJSEnabled returns true if JS is enabled
     * @param validationHintProperties contains the validation hints of variables
     * @param requestedAction contains action that is requested
     * @param groupValidationFlag Contains validation flag
     * @return hasErrors
     * @throws HsxSBUIBuilderRuntimeException
     * @throws HsxSBUIBuilderException
     */
    public  Boolean validateScreenBuilder(final HsxSBWidget widget,
	    final HashMap<String, HsxSBWidget> dynamicQuestionMap,
	    final String isJSEnabled, final Properties validationHintProperties,
	    final String requestedAction, final String groupValidationFlag)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {

	Boolean hasErrors = false;
	HsxSBUIValidator bldUIValidator = new HsxSBUIValidator();
	try {
	    hasErrors = bldUIValidator.validateScreen(widget,
		    dynamicQuestionMap, getRequestValueMap(),
		    validationHintProperties, getSessionMap(), requestedAction,
		    groupValidationFlag);
	    // call updateQuestions method to add the error widget to the
	    // corresponding widget which will fail the validation.
	    HashMap<String, String> uiWidgetDepencymap = null;
	    if (STR_YES.equalsIgnoreCase(isJSEnabled)) {
		uiWidgetDepencymap = new HashMap<String, String>();
		uiWidgetDependencyMap = HsxSBWidgetUtil
			.createUIWidgetHideUnHideMap(widget,
				uiWidgetDepencymap, dynamicQuestionMap);
	    }
	    updateQuestions(widget, dynamicQuestionMap, uiWidgetDepencymap,
		    isJSEnabled);
	} catch (RuntimeException e) {
	    e.printStackTrace();
	    throw new HsxSBUIBuilderRuntimeException(e.getMessage());
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new HsxSBUIBuilderException(e.getMessage());
	}

	return hasErrors;

    }

    /**
     * This method shows and hide the common error message for the acegy based
     * on the Error indicator.
     * @param scWidget contains screen widget data
     * @param widget contains widget data
     * @param errorMessage carries error message
     */
    public void updateFormsForAcegy(final HsxSBScreenWidget scWidget,
	    final HsxSBWidget widget, final String errorMessage) {

	HsxSBFormWidget formWidget = (HsxSBFormWidget) widget;
	if (errorMessage != null) {
	    formWidget.getWidgetInfo().setErrorIndicator(true);
	    formWidget.getWidgetInfo().setPageErrorText(errorMessage);
	}
	String errorStyle = scWidget.getWidgetInfo().getErrorStyle();
	if (formWidget != null) {
	    if ((null != formWidget.getWidgetInfo().getFormAction())
		    && ((HTML_SAVEQUOTE_ACTION.equalsIgnoreCase(formWidget
			    .getWidgetInfo().getFormAction())) || (HTML_RETRIEVEQUOTE_ACTION
			    .equalsIgnoreCase(formWidget.getWidgetInfo()
				    .getFormAction())))) {
		if (formWidget.getWidgetInfo().isErrorIndicator()) {
		    // show the common error message
		    errorStyle = errorStyle.replaceAll(CSS_HIDE_CLASS,
			    STR_EMPTY);
		    formWidget.getWidgetInfo().setErrorStyle(errorStyle);

		} else {
		    // hide the common error message
		    errorStyle = errorStyle.replaceAll(CSS_HIDE_CLASS,
			    STR_EMPTY);
		    errorStyle = errorStyle.concat(STR_SINGLE_SPACE
			    + CSS_HIDE_CLASS);
		    formWidget.getWidgetInfo().setErrorStyle(errorStyle);
		    }
		} else {
		// hide the common error message
		formWidget.getWidgetInfo().setErrorIndicator(true);
		errorStyle = errorStyle.replaceAll(CSS_HIDE_CLASS, STR_EMPTY);
		errorStyle = errorStyle.concat(STR_SINGLE_SPACE
			+ CSS_HIDE_CLASS);
		formWidget.getWidgetInfo().setErrorStyle(errorStyle);
	    }
	}

    }

   /**
    * This method sets the error style.
    * @param widget contains widget data
    * @param errorMessage contains error message
    * @throws HsxSBUIBuilderRuntimeException
    * @throws HsxSBUIBuilderException
    */
    public void updateQuestionsForAcegy(final HsxSBWidget widget, final String errorMessage)
	    throws HsxSBUIBuilderRuntimeException, HsxSBUIBuilderException {
	try {
	    HsxSBScreenWidget screenWidget = (HsxSBScreenWidget) widget;
	    Iterator<HsxSBWidget> iterator = screenWidget.getWidgetsList()
		    .iterator();

	    while (iterator.hasNext()) {
		HsxSBWidget sbWidget = iterator.next();
		if (HTML_FORM.equalsIgnoreCase(sbWidget.getWidgetInfo()
			.getType())) {
		    updateFormsForAcegy(screenWidget, sbWidget, errorMessage);
		}
	    }
	} catch (RuntimeException e) {
	    throw new HsxSBUIBuilderRuntimeException(e.getMessage());
	} catch (Exception e) {
	    throw new HsxSBUIBuilderException(e.getMessage());
	}

    }

}
