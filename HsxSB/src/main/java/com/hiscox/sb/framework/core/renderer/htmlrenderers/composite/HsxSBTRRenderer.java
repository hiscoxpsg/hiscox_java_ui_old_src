package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTRWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the HTML TR tag.
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:06 AM
 */
public class HsxSBTRRenderer extends HsxSBHTMLCompositeRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method returns the String for rendering the HTML TR tag. This method
     * in turn calls the TD renderer, which calls the span renderer.
     * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws runtime exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBTRWidget trWidget = (HsxSBTRWidget) widget;
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_TR);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(trWidget.getWidgetInfo().getStyle());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	Iterator<HsxSBWidget> widgetListIterator = trWidget.getWidgetsList()
		.iterator();
	while (widgetListIterator.hasNext()) {
	    HsxSBWidget sbWidget = widgetListIterator.next();
	    HsxSBRenderer trRenderer = HsxSBHTMLRendererFactory
		    .getRendererInstance(sbWidget.getWidgetInfo().getType());
	    String rendererd = trRenderer.renderContent(sbWidget);
	    contentBuilder.append(rendererd);
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_TR);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }
}
