package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionGroupWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML tags to display questions .
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:29 AM
 */
public class HsxSBQuestionGroupRenderer extends HsxSBHTMLCompositeRenderer
	implements
	    HsxSBUIBuilderConstants {

    /**
     * This method returns the String for rendering the HTML tags to display the
     * screen questions. This method in turn calls the Question renderer which
     * calls different renderer's such as Label or Text Box renderer.
     * @param widget contais widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws runtime exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBQuestionGroupWidget questionGroupWidget = (HsxSBQuestionGroupWidget) widget;
	String groupCode = questionGroupWidget.getWidgetInfo().getId();
	//System.out.println("Group Code Testing :" + groupCode);
	if (HsxSBUtil.isNotBlank(groupCode)
		&& groupCode.toLowerCase().startsWith(STR_DUMMY.toLowerCase())) {
	    return STR_EMPTY;
	}
	if (null != questionGroupWidget.getWidgetInfo() && null != groupCode
		&& groupCode.trim().length() > 0) {

	    if (groupCode.contains(HTML_DOT)) {
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_LI);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_GROUP);
		contentBuilder.append(questionGroupWidget.getWidgetInfo()
			.getStyleHint());
		contentBuilder.append(HTML_SINGLE_QUOTE);

		if (null != questionGroupWidget.getWidgetInfo().getId()
			&& questionGroupWidget.getWidgetInfo().getId().trim()
				.length() > 0) {
		    contentBuilder.append(HTML_SINGLESPACE);
		    contentBuilder.append(HTML_ID);
		    contentBuilder.append(HTML_EQUALS);
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		    contentBuilder.append(questionGroupWidget.getWidgetInfo()
			    .getId().toLowerCase());
		    // Changed from subGroupId to question_id
		    contentBuilder.append(QUESTION_ID_SUFFIX);
		    contentBuilder.append(HTML_SINGLE_QUOTE);

		}
		contentBuilder.append(HTML_END_TAG);

	    } else {
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_GROUP);
		contentBuilder.append(questionGroupWidget.getWidgetInfo()
			.getStyleHint());
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_END_TAG);

	    }

	    contentBuilder.append(GROUPSTART_TAGS);

	    // non java script text
	    if (HsxSBUtil.isNotBlank(questionGroupWidget.getWidgetInfo()
		    .getAdditionalText3())) {
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_NOSCRIPT);
		contentBuilder.append(HTML_END_TAG);

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(CSS_NON_JS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_END_TAG);

		contentBuilder.append(questionGroupWidget.getWidgetInfo()
			.getAdditionalText3());

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_END_TAG);

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_NOSCRIPT);
		contentBuilder.append(HTML_END_TAG);

	    }

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FIELD_SET);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_ID);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(questionGroupWidget.getWidgetInfo().getId());
	    contentBuilder.append(QUESTION_ID_SUFFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_GROUP_STYLE);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);
	}

	if (questionGroupWidget.getWidgetInfo() != null
		&& (HsxSBUtil.isNotBlank(questionGroupWidget.getWidgetInfo()
			.getLabel()) || HsxSBUtil
			.isNotBlank(questionGroupWidget.getWidgetInfo()
				.getAdditionalText1()))) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_LEGEND);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(CSS_GROUP_TEXT);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_INS);
	    contentBuilder.append(HTML_END_TAG);
		String includeHelp = HsxSBWidgetUtil.addHelpRenderer(questionGroupWidget);
	    // added the text that come form the text manager here.
	    if (HsxSBUtil.isNotBlank(questionGroupWidget.getWidgetInfo()
		    .getLabel())) {
		contentBuilder.append(questionGroupWidget.getWidgetInfo()
			.getLabel());
	    } else {
		contentBuilder.append(questionGroupWidget.getWidgetInfo()
			.getAdditionalText1());
	    }
	    if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
				.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
		    String includeAdditionalText4 = HsxSBWidgetUtil
			    .addAdditionalText4Renderer(questionGroupWidget);
		    contentBuilder.append(includeAdditionalText4);
		}

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_INS);
	    contentBuilder.append(HTML_END_TAG);
		contentBuilder.append(includeHelp);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_LEGEND);
	    contentBuilder.append(HTML_END_TAG);

	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_OL);
	contentBuilder.append(HTML_END_TAG);

	//System.out.println("griup Error indicator :::::::::::"+questionGroupWidget.getWidgetInfo().isErrorIndicator());
	if (questionGroupWidget.getWidgetInfo().isErrorIndicator()) {
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_LI);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(CSS_ERROR_CLASS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_END_TAG);
    	questionGroupWidget.getWidgetInfo().getErrorText();
    	questionGroupWidget.getWidgetInfo().setErrorStyle(CSS_ERR);
	    String includeError = HsxSBWidgetUtil.addErrorRenderer(questionGroupWidget);
	    contentBuilder.append(includeError);
	    contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_LI);
		contentBuilder.append(HTML_END_TAG);
	}
	if (null != questionGroupWidget.getWidgetsList()) {
	    Iterator<HsxSBWidget> widgetsListIterator = questionGroupWidget
		    .getWidgetsList().iterator();
	    while (widgetsListIterator.hasNext()) {
		HsxSBWidget innerWidget = widgetsListIterator.next();
		if (null != innerWidget.getWidgetInfo().getIsRenderingRequired() && STR_NO.equalsIgnoreCase(innerWidget.getWidgetInfo().getIsRenderingRequired())) {
	    	//No Action required
	    } else {
		HsxSBRenderer renderer = HsxSBHTMLRendererFactory
			.getRendererInstance(innerWidget.getWidgetInfo()
				.getType());
		if (renderer != null) {
		    contentBuilder.append(renderer.renderContent(innerWidget));
		}
	    }
	    }
	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_OL);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_FIELD_SET);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(GROUPEND_TAGS);

	if (null != questionGroupWidget.getWidgetInfo() && null != groupCode
		&& groupCode.trim().length() > 0) {
	    if (groupCode.contains(HTML_DOT)) {

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_LI);
		contentBuilder.append(HTML_END_TAG);
	    } else {
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_END_TAG);

	    }
	}

	return contentBuilder.toString();
    }

}
