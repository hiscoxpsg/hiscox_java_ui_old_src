package com.hiscox.sb.framework.util;

import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;

/**
 * This class is used to accomodate all the utility methods requires for the
 * screen builder.
 *
 * @author Cognizant
 *
 */
public class HsxSBUtil implements HsxSBUIBuilderConstants {
	/**
	 *
	 * @param doc is an input parameter
	 * @return null
	 */
	public static String getStringFromDocument(Document doc) {
		try {
			doc.normalizeDocument();
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		} catch (TransformerException ex) {
			ex.printStackTrace();
			return null;
		}

	}

	/**
	 * this method check for null as well as empty.
	 *
	 * @param input is a String
	 * @return result
	 */
	public static boolean isNotBlank(String input) {

		boolean result = false;

		if (input != null && !STR_EMPTY.equalsIgnoreCase(input.trim())) {
			result = true;
		}

		return result;
	}

	/**
	 * this method check whether the give input is numeric or not.
	 *
	 * @param input is a string
	 * @return result
	 */
	public static boolean isNumeric(String input) {
		boolean result = true;

		input = input.replaceAll(REG_ONLY_NUMERIC, STR_EMPTY);
		try {
			Double.valueOf(input);

		} catch (NumberFormatException e) {
			result = false;
		} catch (Exception e) {
			result = false;
		}
		return result;
	}

	public static boolean isInteger(String string) {
	    try {
	        Integer.valueOf(string);
	        return true;
	    } catch (NumberFormatException e) {
	        return false;
	    }
	}


	/**
	 * this method check whether the give input is Date object or not.
	 *
	 * @param input is a string
	 * @return result
	 */
	public static boolean isDate(String input) {
		boolean result = true;

		try {
			DateFormat formatter;
			formatter = new SimpleDateFormat(HTML_DATE_FORMAT);
			formatter.parse(input);

		} catch (ParseException e) {
			result = false;
		} catch (Exception e) {
			result = false;
		}
		return result;
	}
	/**
	 * this method check whether the give input is blank or not.
	 *
	 * @param input is a string
	 * @return result
	 */
	public static boolean isBlank(String input) {
		boolean result = false;
		if (input == null || STR_EMPTY.equalsIgnoreCase(input.trim())) {
			result = true;
		}
		return result;
	}
	/**
	 * this method check whether the give input is currency code or not.
	 *
	 * @param input is a string
	 * @return result
	 */
	public static boolean isCurrencyCode(String input) {
		boolean result = true;
		try {
			Currency.getInstance(input).getSymbol();
		} catch (IllegalArgumentException illegalcode) {
			result = false;
		}

		return result;
	}
	/**
	 * this method is to get the symbol of the currency code.
	 *
	 * @param currencyCode is a string
	 * @return symbol
	 */
	public static String getSymbol(String currencyCode) {
		String symbol = STR_EMPTY;
		Locale[] locales = Locale.getAvailableLocales();

		for (Locale locale : locales) {

			if (locale.toString().contains(STR_UNDERSCORE)
					&& currencyCode.equalsIgnoreCase(Currency.getInstance(
							locale).getCurrencyCode())) {

				symbol = Currency.getInstance(currencyCode).getSymbol(locale);

			}
		}
		return symbol;
	}
	/**
	 * this method is to get the text between two strings.
	 * @param input is a string
	 * @param startString is a start string
	 * @param endString is a end string
	 * @return result
	 */

	public static String getTextBetweenStrings(String input,
			String startString, String endString) {
		String result = null;
		if (input != null && startString != null && endString != null) {
			int startIndex = input.indexOf(startString);
			int lastIndex = input.indexOf(endString);
			int startStringLength = startString.length();

			if (startIndex >= 0 && lastIndex > 1) {
				startIndex = startIndex + startStringLength;
				result = input.substring(startIndex, lastIndex);
			}
		}

		return result;
	}
/**
 * this method is to remove symbols and numbers.
 * @param value is a string
 * @return null if input is null
 */
	public static String removeSymbolsAndNumbers(String value) {
		if (value != null) {
			return value.replaceAll(REG_ONLY_ALPHA_NUMERIC, STR_EMPTY)
					.toLowerCase();
		} else {
			return null;
		}

	}
/**
 * this method is to adjust date formatter.
 * @param dForm is a character input
 * @return dForm
 */
	public static char[] dateFormatAdjuster(char[] dForm) {
		char temp;
		if (dForm[0] == 'D') {
			temp = dForm[0];
			dForm[0] = dForm[1];
			dForm[1] = dForm[2];
			dForm[2] = temp;
		} else if (dForm[1] == 'D') {
			temp = dForm[1];
			dForm[1] = dForm[2];
			dForm[2] = temp;
		}
		return dForm;
	}
/**
 * This method is to convert the date format.
 * @param input is a string
 * @param sourceFormat is a format currently exists
 * @param destnationFormat is a format finally to be converted
 * @return strDate
 */
	public static String convertDateFormat(String input, String sourceFormat,
			String destnationFormat) {
		String strDate = input;

		try {
			if (HsxSBUtil.isDate(strDate)) {
				// create SimpleDateFormat object with source string
				// date format
				SimpleDateFormat sourceDateFormat = new SimpleDateFormat(
						sourceFormat);

				// parse the string into Date object
				Date date = sourceDateFormat.parse(strDate);

				// create SimpleDateFormat object with desired date
				// format
				SimpleDateFormat destDateFormat = new SimpleDateFormat(
						destnationFormat);
				// System.out.println("Destination date format :: "
				// + destnationFormat);
				// parse the date into another format
				strDate = destDateFormat.format(date);
			}

		} catch (ParseException pe) {
			throw new HsxSBUIBuilderRuntimeException();
		}
		return strDate;

	}
/**
 * This method is to get the maximum length of input string.
 * @param maxLength is a input String
 * @return maxLength
 */
	public static String getMaxLength(String maxLength) {
		if (HsxSBUtil.isNotBlank(maxLength) && maxLength.contains(HTML_DOT)) {

			StringTokenizer tokens = new StringTokenizer(maxLength, HTML_DOT);

			if (tokens.countTokens() > 1) {
				String integerPartLength = tokens.nextToken();
				String decimalPartLength = tokens.nextToken();
				if (HsxSBUtil.isNumeric(integerPartLength)
						&& HsxSBUtil.isNumeric(decimalPartLength)) {
					Integer length = Integer.valueOf(integerPartLength)
							+ Integer.valueOf(decimalPartLength) + 1;
					maxLength = length.toString();
				}
			}

		}
		return maxLength;
	}

	/**
	 * This method creates HashMap for the given two dimentional array and
	 * returns the HashMap object.
	 *
	 * @param twoDimArray is an two dimensional array string
	 * @return generatedHashMap
	 */
	public static HashMap<String, String> createHashMap(String[][] twoDimArray) {

		HashMap<String, String> generatedHashMap = new HashMap<String, String>();
		int twoDimArrayLength = twoDimArray.length;

		for (int i = 0; i < twoDimArrayLength; i++) {
			generatedHashMap.put(twoDimArray[i][0], twoDimArray[i][1]);
		}

		return generatedHashMap;
	}
/**
 * This method is used to delete the white spaces in a string.
 * @param str is an input string
 * @return string with out white spaces
 */
	public static String deleteWhitespace(String str) {
		if (isEmpty(str)) {
			return str;
		}
		int sz = str.length();
		char[] chs = new char[sz];
		int count = 0;
		for (int i = 0; i < sz; i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				chs[count++] = str.charAt(i);
			}
		}
		if (count == sz) {
			return str;
		}
		return new String(chs, 0, count);
	}
/**
 * This method is used to check if white spaces are present in a string.
 * @param input is an input string
 * @return true if white spaces are present
 */
	public static boolean containsWhiteSpaces(String input) {
		if (isBlank(input)) {
			return false;
		}
		int sz = input.length();
		for (int i = 0; i < sz; i++) {
			if (Character.isWhitespace(input.charAt(i))) {
				return true;
			}
		}
		return false;
	}
	/**
	 * This method is used to check if commas are present in a string.
	 * @param input is an input string
	 * @return true if commas are present
	 */
	public static boolean containsCommas(String input) {
		if (isBlank(input)) {
			return false;
		}
		return (!Pattern.matches(REG_NON_COMMA_VALUE, input));
	}

	// public static void main(String[] args) {
	// String ab = "rkhdfshs@hjis.com";
	// System.out.println(replaceChars(ab, "\\.", ""));
	// //System.out.println(containsCommas("rk@g&^$&,$&$*%mail.com"));
	// }

	public static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static boolean isInRange(int value, int min, int max) {
		return (value >= min && value <= max);
	}

}
