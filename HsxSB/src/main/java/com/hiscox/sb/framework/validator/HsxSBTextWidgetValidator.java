package com.hiscox.sb.framework.validator;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;

/**
 * This class validates the text widget with a given validation hint or subtype
 * provided in the input xml Dom object.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:41 AM
 */
public class HsxSBTextWidgetValidator {

	public HsxSBBaseWidget hsxSBBaseWidget;

	public HsxSBTextWidgetValidator() {

	}

	/**
	 * This method takes the widget value and corresponding RegExp from the
	 * property file and validates against it and returns true if there are any
	 * validation errors otherwise false.
	 *
	 * @param regexp is a input string
	 * @param inputValue is a input string to validate widget
	 * @return false
	 */
	public boolean validateWidget(String regexp, String inputValue) {
		return false;
	}

}
