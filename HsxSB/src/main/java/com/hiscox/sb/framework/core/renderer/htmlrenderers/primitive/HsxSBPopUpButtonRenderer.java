package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBPopUpButtonWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * This class renders the HTML button tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:06 AM
 */
public class HsxSBPopUpButtonRenderer extends HsxSBHTMLBaseRenderer implements
	    HsxSBUIBuilderConstants {
    /**
     * This method will return the String for rendering the HTML Button.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBPopUpButtonWidget popUpWidget = (HsxSBPopUpButtonWidget) widget;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_BUTTON_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_BUTTON_SPAN_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_BUTTON_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_HREF);
	contentBuilder.append(HTML_EQUALS);

	contentBuilder.append(HTML_SINGLE_QUOTE);
	if (null != popUpWidget.getWidgetInfo().getDisplayText()
		&& popUpWidget.getWidgetInfo().getDisplayText()
			.equalsIgnoreCase("Back")) {
	    String previousScreen = HsxSBUIBuilderControllerImpl
		    .getPreviousScreen();
	   // System.out.println(" previuosScreen set :" + previousScreen);
	   // System.out.println(" URL generated :: /small-business-insurance/"+previousScreen);

	    contentBuilder.append("/small-business-insurance/");
	    if (HsxSBUtil.isNotBlank(previousScreen) && !previousScreen.equalsIgnoreCase("small-business-insurance")) {
	    	contentBuilder.append(previousScreen);
	    	contentBuilder.append(STR_FORWARD_SLASH);
	    }
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	} else {
		if (HsxSBUtil.isNotBlank(popUpWidget.getWidgetInfo().getAdditionalText4())) {
	    contentBuilder.append(popUpWidget.getWidgetInfo().getAdditionalText4());
		}
	    // popUpWidget.getWidgetInfo().get
	    contentBuilder.append(HTML_SINGLE_QUOTE);

	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_TARGET);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(STR_BLANK);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);

	    contentBuilder.append(STR_ONCLICK);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_DOUBLE_QUOTE);
	    contentBuilder.append(POPUP_CALL);
	    contentBuilder.append(HTML_DOUBLE_QUOTE);
	}

	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(popUpWidget.getWidgetInfo().getCode());
	contentBuilder.append(HTML_SINGLESPACE);

	contentBuilder.append(HTML_TITLE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	if (null != popUpWidget.getWidgetInfo().getLabel()) {
	    contentBuilder.append(popUpWidget.getWidgetInfo().getLabel());

	}
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_END_TAG);

	if (null != popUpWidget.getWidgetInfo().getLabel()) {
	    contentBuilder.append(popUpWidget.getWidgetInfo().getLabel());

	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_BUTTONICON_SPAN_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	/*
	 * if (null != popUpWidget.getWidgetInfo().getDisplayText()) {
	 * contentBuilder.append(popUpWidget.getWidgetInfo().getDisplayText());
	 * }
	 */
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
