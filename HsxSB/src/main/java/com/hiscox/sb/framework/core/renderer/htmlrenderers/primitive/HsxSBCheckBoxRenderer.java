package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBCheckBoxWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML check box tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:08 AM
 */
public class HsxSBCheckBoxRenderer extends HsxSBHTMLBaseRenderer implements
		HsxSBUIBuilderConstants {

	/**
	 * This method will return the String for rendering the HTML Check Box.
	 * @param widget contains widget data
     * @return content
	 */

	public String renderContent(final HsxSBWidget widget) {

		StringBuilder contentBuilder = new StringBuilder();
		HsxSBCheckBoxWidget checkboxWidget = (HsxSBCheckBoxWidget) widget;
		String checkBoxId = checkboxWidget.getWidgetInfo().getId()
				+ CHECKBOX_ID_SUFFIX;
		checkboxWidget.getWidgetInfo().setLabelForId(checkBoxId);

		if (SUBTYPE_REVERSE.equalsIgnoreCase(checkboxWidget.getWidgetInfo()
				.getSubType())) {
			String includeLabel = HsxSBWidgetUtil
					.addLabelRenderer(checkboxWidget);
			contentBuilder.append(includeLabel);
		}
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_CONTROL_DIV_STYLE);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_END_TAG);

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_INPUT);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_TYPE);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_CHECKBOX);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(checkboxWidget.getWidgetInfo().getStyle());
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_ID);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(checkBoxId);
		contentBuilder.append(HTML_SINGLE_QUOTE);

		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_NAME);
		contentBuilder.append(HTML_EQUALS);

		if (null != checkboxWidget.getWidgetInfo().getName()) {
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(checkboxWidget.getWidgetInfo().getName());
			contentBuilder.append(HTML_SINGLE_QUOTE);
		} else {
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(checkboxWidget.getWidgetInfo().getId());
			contentBuilder.append(HTML_SINGLE_QUOTE);
		}

		String savedValue = checkboxWidget.getWidgetInfo().getSavedValue();
		if (HsxSBUtil.isNotBlank(savedValue)
				&& (STR_YES.equalsIgnoreCase(savedValue) || (STR_PRE_TICK
						.equalsIgnoreCase(savedValue)))) {
			checkboxWidget.setChecked(true);
		} else {
			checkboxWidget.setChecked(false);
		}
		String defaultValue = checkboxWidget.getWidgetInfo().getDefaultValue();
		if (HsxSBUtil.isNotBlank(defaultValue)
				&& (STR_YES.equalsIgnoreCase(defaultValue))) {
			checkboxWidget.setChecked(true);
		} else if(STR_NO.equalsIgnoreCase(defaultValue)){
			checkboxWidget.setChecked(false);
		}
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_VALUE);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(STR_YES);
		contentBuilder.append(HTML_SINGLE_QUOTE);

		if (checkboxWidget.isChecked()) {
			contentBuilder.append(HTML_SINGLESPACE);
			contentBuilder.append(HTML_CHECKED);
			contentBuilder.append(HTML_SINGLESPACE);
			contentBuilder.append(HTML_EQUALS);
			contentBuilder.append(HTML_SINGLE_QUOTE);
			contentBuilder.append(HTML_CHECKED);
			contentBuilder.append(HTML_SINGLE_QUOTE);
		}
		contentBuilder.append(HTML_CLOSE_END_TAG);

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_DIV);
		contentBuilder.append(HTML_END_TAG);

		if (!SUBTYPE_REVERSE.equalsIgnoreCase(checkboxWidget.getWidgetInfo()
				.getSubType())) {
			String includeLabel = HsxSBWidgetUtil
					.addLabelRenderer(checkboxWidget);
			contentBuilder.append(includeLabel);
		}

		String includeHelp = HsxSBWidgetUtil.addHelpRenderer(checkboxWidget);

		if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
				.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
				String includeAdditionalText4 = HsxSBWidgetUtil
						.addAdditionalText4Renderer(checkboxWidget);
				contentBuilder.append(includeAdditionalText4);
			}

		contentBuilder.append(includeHelp);

		if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
			if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
					.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
				String includeAdditionalText1 = HsxSBWidgetUtil
						.addAdditionalTextOneRenderer(checkboxWidget);
				contentBuilder.append(includeAdditionalText1);
			}
			if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
					.getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
				String includeAdditionalText2 = HsxSBWidgetUtil
						.addAdditionalTextTwoRenderer(checkboxWidget);
				contentBuilder.append(includeAdditionalText2);
			}
		}
		if (checkboxWidget.getWidgetInfo().isErrorIndicator()) {
			String includeError = HsxSBWidgetUtil
					.addErrorRenderer(checkboxWidget);
			contentBuilder.append(includeError);
		}
		return contentBuilder.toString();
	}

}
