package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTableWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the HTML TABLE tag .
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:06 AM
 */
public class HsxSBTableRenderer extends HsxSBHTMLCompositeRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method returns the String for rendering the HTML Table tag. This
     * method calls the TR renderer which calls the TD renderer further.
     * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws runtime exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBTableWidget tableWidget = (HsxSBTableWidget) widget;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_LI);
	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_TABLE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_SUMMARY);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	if (null != tableWidget.getWidgetInfo().getAdditionalText2()
		&& tableWidget.getWidgetInfo().getAdditionalText2().trim().length() > 0) {
	    contentBuilder.append(tableWidget.getWidgetInfo().getAdditionalText2());
	}
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CELLSPACING);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(STR_ZERO);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CELLPADDING);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(STR_ZERO);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(tableWidget.getWidgetInfo().getStyleHint());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_CAPTION);
	contentBuilder.append(HTML_END_TAG);
	if (null != tableWidget.getWidgetInfo().getAdditionalText1()
		&& tableWidget.getWidgetInfo().getAdditionalText1().trim()
			.length() > 0) {
	    contentBuilder.append(tableWidget.getWidgetInfo()
		    .getAdditionalText1());
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_CAPTION);
	contentBuilder.append(HTML_END_TAG);

	Iterator<HsxSBWidget> widgetListIterator = tableWidget.getWidgetsList()
		.iterator();
	while (widgetListIterator.hasNext()) {
	    HsxSBWidget sbWidget = widgetListIterator.next();
	    HsxSBRenderer tableRenderer = HsxSBHTMLRendererFactory
		    .getRendererInstance(sbWidget.getWidgetInfo().getType());
	    String rendererd = tableRenderer.renderContent(sbWidget);
	    contentBuilder.append(rendererd);
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_TABLE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_LI);
	contentBuilder.append(HTML_END_TAG);
	return contentBuilder.toString();
    }

}
