package com.hiscox.sb.framework.core.widgets.vo;

import java.io.Serializable;

/**
 * This class (HsxSBTextItem bean) is used accommodate the Text Manager data.
 * Each TextItem row in the text manager is represented with this instance.
 *
 * @author Cognizant
 *
 */
public class HsxSBTextManagerVO implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = -4448385443420560668L;
	private String textCode = null;
    private String labelText = null;
    private String helpText = null;
    private String errorText = null;
    //private String purpose = null;
    private String nonJavascriptText = null;
    private String helpTitle = null;
    private String defaultValue = null;
    private String editTitle = null;
    private String editText = null;
    private String style = null;

    // Additional text about the question would be stored here
    private String additionalText1 = null;

    // A second set of additional text about the question would be stored here
    private String additionalText2 = null;

    // Non Javascript introductory text could be stored here
    private String additionalText3 = null;

    // edit href link could be stored here
    private String additionalText4 = null;

    // edit link text could be stored here
    private String additionalText5 = null;

    // for future use
    private String additionalText6 = null;

    //Not In Use
//    private String additionalText7 = null;
//    private String additionalText8 = null;
//    private String additionalText9 = null;


    //private String onScreenHeader = null;

    //private String screenReaderHeader = null;

    //private String pageIntroText = null;
    //private String pageErrorText = null;
  //Not In Use

    public String getStyle() {
	return style;
    }

    public void setStyle(String style) {
	this.style = style;
    }

    public String getTextCode() {
	return textCode;
    }

//    public String getPurpose()
//    {
//	return purpose;
//    }
//
//    public void setPurpose(String purpose)
//    {
//	this.purpose = purpose;
//    }

    public String getAdditionalText6() {
	return additionalText6;
    }

    public void setAdditionalText6(String additionalText6) {
	this.additionalText6 = additionalText6;
    }

//    public String getAdditionalText7()
//    {
//	return additionalText7;
//    }
//
//    public void setAdditionalText7(String additionalText7)
//    {
//	this.additionalText7 = additionalText7;
//    }
//
//    public String getAdditionalText8()
//    {
//	return additionalText8;
//    }
//
//    public void setAdditionalText8(String additionalText8)
//    {
//	this.additionalText8 = additionalText8;
//    }
//
//    public String getAdditionalText9()
//    {
//	return additionalText9;
//    }
//
//    public void setAdditionalText9(String additionalText9)
//    {
//	this.additionalText9 = additionalText9;
//    }

    public void setTextCode(String textCode) {
	this.textCode = textCode;
    }

    public String getLabelText() {
	return labelText;
    }

    public void setLabelText(String labelText) {
	this.labelText = labelText;
    }

    public String getHelpText() {
	return helpText;
    }

    public void setHelpText(String helpText) {
	this.helpText = helpText;
    }

    public String getErrorText() {
	return errorText;
    }

    public void setErrorText(String errorText) {
	this.errorText = errorText;
    }

    public String getNonJavascriptText() {
	return nonJavascriptText;
    }

    public void setNonJavascriptText(String nonJavascriptText) {
	this.nonJavascriptText = nonJavascriptText;
    }

    public String getHelpTitle() {
	return helpTitle;
    }

    public void setHelpTitle(String helpTitle) {
	this.helpTitle = helpTitle;
    }

    public String getDefaultValue() {
	return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
	this.defaultValue = defaultValue;
    }

    public String getEditTitle() {
	return editTitle;
    }

    public void setEditTitle(String editTitle) {
	this.editTitle = editTitle;
    }

    public String getEditText() {
	return editText;
    }

    public void setEditText(String editText) {
	this.editText = editText;
    }

    public String getAdditionalText1() {
	return additionalText1;
    }

    public void setAdditionalText1(String additionalText1) {
	this.additionalText1 = additionalText1;
    }

    public String getAdditionalText2() {
	return additionalText2;
    }

    public void setAdditionalText2(String additionalText2) {
	this.additionalText2 = additionalText2;
    }

    public String getAdditionalText3() {
	return additionalText3;
    }

    public void setAdditionalText3(String additionalText3) {
	this.additionalText3 = additionalText3;
    }

    public String getAdditionalText4() {
	return additionalText4;
    }

    public void setAdditionalText4(String additionalText4) {
	this.additionalText4 = additionalText4;
    }

    public String getAdditionalText5() {
	return additionalText5;
    }

    public void setAdditionalText5(String additionalText5) {
	this.additionalText5 = additionalText5;
    }

//    public String getOnScreenHeader()
//    {
//	return onScreenHeader;
//    }
//
//    public void setOnScreenHeader(String onScreenHeader)
//    {
//	this.onScreenHeader = onScreenHeader;
//    }

//    public String getScreenReaderHeader()
//    {
//	return screenReaderHeader;
//    }
//
//    public void setScreenReaderHeader(String screenReaderHeader)
//    {
//	this.screenReaderHeader = screenReaderHeader;
//    }
//
//    public String getPageIntroText()
//    {
//	return pageIntroText;
//    }
//
//    public void setPageIntroText(String pageIntroText)
//    {
//	this.pageIntroText = pageIntroText;
//    }
//
//    public String getPageErrorText()
//    {
//	return pageErrorText;
//    }
//
//    public void setPageErrorText(String pageErrorText)
//    {
//	this.pageErrorText = pageErrorText;
//    }

}
