package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBOptionWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the HTML option tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:27 AM
 */
public class HsxSBOptionRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Option.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBOptionWidget optionWidget = (HsxSBOptionWidget) widget;
	String value = null;
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_OPTION);
	contentBuilder.append(HTML_SINGLESPACE);

	contentBuilder.append(HTML_ID);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(optionWidget.getWidgetInfo().getId());
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_SINGLESPACE);

	contentBuilder.append(HTML_VALUE);
	contentBuilder.append(HTML_EQUALS);

	value = optionWidget.getWidgetInfo().getDefaultValue();
	if (null != optionWidget.getWidgetInfo().getSavedValue()
		&& value.equalsIgnoreCase(optionWidget.getWidgetInfo()
			.getSavedValue())) {
	    optionWidget.setSelected(true);
	} else {
	    optionWidget.setSelected(false);
	}
	contentBuilder.append(HTML_DOUBLE_QUOTE);
	contentBuilder.append(optionWidget.getWidgetInfo().getDefaultValue());
	contentBuilder.append(HTML_DOUBLE_QUOTE);

	if (optionWidget.isSelected()) {
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_SELECTED);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_DOUBLE_QUOTE);
	    contentBuilder.append(HTML_SELECTED);
	    contentBuilder.append(HTML_DOUBLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);

	}

	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(optionWidget.getWidgetInfo().getDisplayText());
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_OPTION);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
