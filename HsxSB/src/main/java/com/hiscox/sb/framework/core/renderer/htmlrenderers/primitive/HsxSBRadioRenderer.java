package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import java.util.Iterator;
import java.util.Map;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBRadioWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBPermitedValues;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML radio tag.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:31 AM
 */
public class HsxSBRadioRenderer extends HsxSBHTMLBaseRenderer implements
	    HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Radio Button.
     * @param widget contains widget data
     * @return content
     */

    @Override
    public String renderContent(final HsxSBWidget widget) {

	StringBuilder contentBuilder = new StringBuilder();

	HsxSBRadioWidget radioWidget = (HsxSBRadioWidget) widget;

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FIELD_SET);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_FIELDSET_RADIO_STYLE);
	contentBuilder.append(STR_SINGLE_SPACE);
	contentBuilder.append(HTML_SCREEN_READER_CSS_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_LEGEND);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_LABEL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_INS);
	contentBuilder.append(HTML_END_TAG);

	if (null != radioWidget.getWidgetInfo().getLabel()) {
	    contentBuilder.append(radioWidget.getWidgetInfo().getLabel());
	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_INS);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_LEGEND);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_LABEL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_LABEL);
	contentBuilder.append(HTML_END_TAG);

	if (null != radioWidget.getWidgetInfo().getLabel()) {
	    contentBuilder.append(radioWidget.getWidgetInfo().getLabel());
	}
	if ((null == radioWidget.getWidgetInfo().isMandatory() || !STR_YES
		.equalsIgnoreCase(radioWidget.getWidgetInfo().isMandatory()))
		&& HsxSBUtil.isNotBlank(radioWidget.getWidgetInfo()
			.getOptionalText())) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(CSS_OPTIONAL);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);
	    contentBuilder.append(STR_SINGLE_SPACE);
	    contentBuilder
		    .append(radioWidget.getWidgetInfo().getOptionalText());

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);

	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_LABEL);

	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_CONTROL_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);
	Map<String, HsxSBTextManagerVO> textMap = HsxSBUIBuilderControllerImpl.textMap;
	Iterator<HsxSBPermitedValues> valuesIterator = radioWidget
		.getWidgetInfo().getPermitedVlaues().iterator();
	while (valuesIterator.hasNext()) {
	    HsxSBPermitedValues value = valuesIterator.next();
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_LABEL);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_FOR);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(radioWidget.getWidgetInfo().getId());
	    contentBuilder.append(RADIO_BUTTON_SUFFIX);
	    contentBuilder.append(HsxSBUtil.removeSymbolsAndNumbers(value
		    .getValue()));
	    contentBuilder.append(ID_SUFFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);
	    String displayValue = value.getDisplayText();
	    if (SUBTYPE_REVERSE.equalsIgnoreCase(radioWidget.getWidgetInfo()
		    .getSubType())) {

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_INPUT);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_TYPE);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_RADIO);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(radioWidget.getWidgetInfo().getStyle());
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_ID);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(radioWidget.getWidgetInfo().getId());
		contentBuilder.append(RADIO_BUTTON_SUFFIX);
		contentBuilder.append(HsxSBUtil.removeSymbolsAndNumbers(value
			.getValue()));
		contentBuilder.append(ID_SUFFIX);
		contentBuilder.append(HTML_SINGLE_QUOTE);

		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_NAME);
		contentBuilder.append(HTML_EQUALS);
		if (null != radioWidget.getWidgetInfo().getName()) {
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		    contentBuilder
			    .append(radioWidget.getWidgetInfo().getName());
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		} else {
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		    contentBuilder.append(radioWidget.getWidgetInfo().getId());
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		}

		if (null != radioWidget.getWidgetInfo().getSavedValue()
			&& value.getValue().equalsIgnoreCase(
				radioWidget.getWidgetInfo().getSavedValue())) {
		    radioWidget.setChecked(true);
		} else {
		    radioWidget.setChecked(false);
		}
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_VALUE);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(value.getValue());
		contentBuilder.append(HTML_SINGLE_QUOTE);

		if (radioWidget.isChecked()) {
		    contentBuilder.append(HTML_SINGLESPACE);
		    contentBuilder.append(HTML_CHECKED);
		    contentBuilder.append(HTML_SINGLESPACE);
		    contentBuilder.append(HTML_EQUALS);
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		    contentBuilder.append(HTML_CHECKED);
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		}
		contentBuilder.append(HTML_CLOSE_END_TAG);

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_SPAN);
		contentBuilder.append(HTML_END_TAG);

		if (displayValue != null
			&& textMap.containsKey(displayValue.trim())) {
		    displayValue = textMap.get(displayValue.trim())
			    .getLabelText();
		    displayValue = HsxSBWidgetUtil.replaceContent(radioWidget,
			    displayValue);
		}
		contentBuilder.append(displayValue);
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_SPAN);
		contentBuilder.append(HTML_END_TAG);

	    } else {
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_SPAN);
		contentBuilder.append(HTML_END_TAG);
		if (displayValue != null
			&& textMap.containsKey(displayValue.trim())) {
		    displayValue = textMap.get(displayValue.trim())
			    .getLabelText();
		    displayValue = HsxSBWidgetUtil.replaceContent(radioWidget,
			    displayValue);
		}
		contentBuilder.append(displayValue);
		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_FRONT_SLASH);
		contentBuilder.append(HTML_SPAN);
		contentBuilder.append(HTML_END_TAG);

		contentBuilder.append(HTML_START_TAG);
		contentBuilder.append(HTML_INPUT);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_TYPE);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_RADIO);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_CLASS);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(radioWidget.getWidgetInfo().getStyle());
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_ID);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(radioWidget.getWidgetInfo().getId());
		contentBuilder.append(RADIO_BUTTON_SUFFIX);
		contentBuilder.append(HsxSBUtil.removeSymbolsAndNumbers(value
			.getValue()));
		contentBuilder.append(ID_SUFFIX);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_NAME);
		contentBuilder.append(HTML_EQUALS);
		if (null != radioWidget.getWidgetInfo().getName()) {
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		    contentBuilder
			    .append(radioWidget.getWidgetInfo().getName());
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		} else {
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		    contentBuilder.append(radioWidget.getWidgetInfo().getId());
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		}

		if (null != radioWidget.getWidgetInfo().getSavedValue()
			&& value.getValue().equalsIgnoreCase(
				radioWidget.getWidgetInfo().getSavedValue())) {
		    radioWidget.setChecked(true);
		} else {
		    radioWidget.setChecked(false);
		}
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(HTML_VALUE);
		contentBuilder.append(HTML_EQUALS);
		contentBuilder.append(HTML_SINGLE_QUOTE);
		contentBuilder.append(value.getValue());
		contentBuilder.append(HTML_SINGLE_QUOTE);

		if (radioWidget.isChecked()) {
		    contentBuilder.append(HTML_SINGLESPACE);
		    contentBuilder.append(HTML_CHECKED);
		    contentBuilder.append(HTML_SINGLESPACE);
		    contentBuilder.append(HTML_EQUALS);
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		    contentBuilder.append(HTML_CHECKED);
		    contentBuilder.append(HTML_SINGLE_QUOTE);
		}
		contentBuilder.append(HTML_CLOSE_END_TAG);
	    }
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_LABEL);
	    contentBuilder.append(HTML_END_TAG);

	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	String includeHelp = HsxSBWidgetUtil.addHelpRenderer(radioWidget);

	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap() && STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT4))) {
		String includeAdditionalText4 = HsxSBWidgetUtil
			.addAdditionalText4Renderer(radioWidget);
		contentBuilder.append(includeAdditionalText4);
	    }

	contentBuilder.append(includeHelp);

	if (null != HsxSBUIBuilderControllerImpl.getAdditionalInfoMap()) {
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT1))) {
		String includeAdditionalText1 = HsxSBWidgetUtil
			.addAdditionalTextOneRenderer(radioWidget);
		contentBuilder.append(includeAdditionalText1);
	    }
	    if (STR_YES.equalsIgnoreCase(HsxSBUIBuilderControllerImpl
		    .getAdditionalInfoMap().get(TEXT_ITEM_ADD_TEXT2))) {
		String includeAdditionalText2 = HsxSBWidgetUtil
			.addAdditionalTextTwoRenderer(radioWidget);
		contentBuilder.append(includeAdditionalText2);
	    }
	}

	if (radioWidget.getWidgetInfo().isErrorIndicator()) {
	    String includeError = HsxSBWidgetUtil.addErrorRenderer(radioWidget);
	    contentBuilder.append(includeError);
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_FIELD_SET);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
