package com.hiscox.sb.framework.core.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.builder.HsxSBUIBuilderControllerImpl;
import com.hiscox.sb.framework.core.util.bean.HsxSBJsHiddenVariablesBean;
import com.hiscox.sb.framework.core.widgets.HsxSBCompositeWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBActionListWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBFormWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionGroupWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTDWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTHWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTRWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTableWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBButtonWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBIPEQuestionReference;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBPermitedValues;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBTextManagerVO;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class is responsible for processing the Screen XML, creating the
 * widgets, rendering the HTML.
 * @author Cognizant
 * @version 1.0
 */
public class HsxSBRequestProcessor implements HsxSBUIBuilderConstants {
	/**
	 * This method is used to get CTM Screen Ref Map Values.
	 */
    private static HashMap<String, String> ctmScreenRefMap = HsxSBUtil.createHashMap(CTM_SCREEN_NAME_REF);
    /**
     * This is to get the ctmScreenMirrorRefMap values.
     */
    private static HashMap<String, String> ctmScreenMirrorRefMap = HsxSBUtil
    .createHashMap(CTM_SCREEN_MIRROR_NAME_REF);

    /**
     * This method renders the HTML content using corresponding renderer.
     * @param widget
     * @param uiContentType
     * @return renderer
     * @throws HsxSBUIBuilderRuntimeException
     */
    public static String buildHTMLContent(final HsxSBWidget widget, final int uiContentType)
    throws HsxSBUIBuilderRuntimeException {
	// System.out.println("widget.getWidgetInfo().getType() :"
	// + widget.getWidgetInfo().getType());
	HsxSBRenderer renderer = HsxSBRendererFactory.getRendererInstance(
		widget.getWidgetInfo().getType(), uiContentType);
	// System.out.println("renderer :" + renderer);
	return renderer.renderContent(widget);
    }

    /**
     * @param widgetMap
     * @param widget
     * @param document
     */
    public static void buildWidget(final Map<String, HsxSBWidget> widgetMap,
	    final HsxSBWidget widget, final Document document) {

    }

    /**
     * This method starts the process of building action widget list.
     * @param textMap
     * @param widgetMap
     * @param actionWidget
     * @param oDEdocument
     * @param widgetTypeProperties
     * @param sessionMap
     * @param isJSEnabled
     * @return actionWidget
     * @throws HsxSBUIBuilderRuntimeException
     */
    public static HsxSBWidget buildActionWidget(
	    final Map<String, HsxSBTextManagerVO> textMap,
	    final Map<String, HsxSBWidget> widgetMap, HsxSBWidget actionWidget,
	    final Document oDEdocument, final Properties widgetTypeProperties,
	    final Map<String, String> sessionMap, final String isJSEnabled)
	    throws HsxSBUIBuilderRuntimeException {

	Element actionElement = (Element) oDEdocument.getElementsByTagNameNS(
		STR_ASTERISK, XML_ACTION_LIST).item(0);
	if (actionElement != null) {
	    actionWidget = processActionList(oDEdocument, widgetMap, textMap,
		    widgetTypeProperties, sessionMap, isJSEnabled);
	} else {
	    actionWidget = null;
	}

	return actionWidget;
    }

    /**
     * This method starts the process of building widget list.
     *
     * @param textMap
     * @param widgetMap
     * @param widget
     * @param document
     * @return widget
     * @throws HsxSBUIBuilderRuntimeException
     */
    public static HsxSBWidget buildWidget(
	    final Map<String, HsxSBTextManagerVO> textMap,
	    final Map<String, HsxSBWidget> widgetMap, HsxSBWidget widget,
	    final Document oDEdocument, final String isJSEnabled,
	    final Properties widgetTypeProperties, Map<String, String> sessionMap)
	    throws HsxSBUIBuilderRuntimeException {
	Element screenElement = (Element) oDEdocument.getElementsByTagNameNS(
		STR_ASTERISK, XML_DATA_AREA).item(0);
	widget = processScreen(screenElement, widgetMap, textMap, isJSEnabled,
		widgetTypeProperties, sessionMap);
	return widget;
    }

    /**
     * This method builds the widgets for DataArea tag and initiates the process
     * to build widgets for FORMS.
     *
     * @param screenElement
     * @param widgetMap
     * @param textMap
     * @param widgetTypeProperties
     * @param sessionMap
     * @return screenWidget
     * @throws HsxSBUIBuilderRuntimeException
     */
    private static HsxSBWidget processScreen(Element screenElement,
	    Map<String, HsxSBWidget> widgetMap,
	    Map<String, HsxSBTextManagerVO> textMap, String isJSEnabled,
	    Properties widgetTypeProperties, Map<String, String> sessionMap) {

	HsxSBWidgetInfo screenWidgetInfo = createWidgetInfo(screenElement);

	HsxSBCompositeWidget screenWidget = (HsxSBScreenWidget) HsxSBWidgetFactory
		.getWidgetInstance(HTML_SCREEN, null, null);
	String screenCode = null;
	if (null != screenElement) {

	    // getting an element based on the name of the tag and
	    // ignore the
	    // the name space by using wild character '*'
	    // also make sure that you set the nameSpaceAware to
	    // true as
	    // documentBuilderFactory.setNamespaceAware(true);
	    // while your are creating the document

	    NodeList screenCodeElement = screenElement.getElementsByTagNameNS(
		    STR_ASTERISK, XML_CODE);
	    screenCode = screenCodeElement.item(0).getTextContent();
	    String tempScreenCode = screenCode;
	    if (ctmScreenRefMap.containsKey(screenCode)) {
		tempScreenCode = ctmScreenRefMap.get(screenCode);
	    }
	    HsxSBUIBuilderControllerImpl.screenName = screenCode;
	    screenWidgetInfo.setId(screenCode);
	    screenWidgetInfo.setErrorStyle(PAGE_ERROR_STYLE_CLASS);
	    HsxSBTextManagerVO textItem = textMap.get(tempScreenCode);
	    addtoWidgetMap(screenCode, widgetMap, screenWidget);
	    if (textItem != null) {
		String pageIntroText = textItem.getAdditionalText1();
		String genericErrorText = textItem.getAdditionalText2();
		String customErrorText = textItem.getAdditionalText3();
		String pageErrorText = textItem.getErrorText();
		screenWidgetInfo.setPageIntroText(pageIntroText);
		screenWidgetInfo.setPageErrorText(pageErrorText);
		// Read Screen Level Style Hint From CTM
		screenWidgetInfo.setStyleHint(textItem.getStyle());
		screenWidgetInfo.setAdditionalText2(genericErrorText);
		screenWidgetInfo.setAdditionalText3(customErrorText);
	    }

	    screenWidget.createWidget(screenWidgetInfo);

	    NodeList screenForms = screenElement.getChildNodes();
	    String defaultNavigator = screenWidgetInfo.getDefaultNavigator();
	    final int noOfForms = screenForms.getLength();
		for (int i = 0; i < noOfForms; i++) {
		String nodeName = screenForms.item(i).getNodeName();
		if (screenForms.item(i).getNodeType() == Node.ELEMENT_NODE
			&& (nodeName != null) && nodeName.contains(XML_FORM)) {
		    Element formElement = (Element) screenForms.item(i);
		    screenWidget
			    .addWidget(processForms(formElement, widgetMap,
				    textMap, screenCode, isJSEnabled,
				    widgetTypeProperties, sessionMap,
				    defaultNavigator));
		}

	    }
	}
	return screenWidget;
    }

    /**
     * This method builds the widgets for Form tag and initiates the process to
     * build widgets for QUESTION_GROUP.
     *
     * @param formElement
     * @param widgetMap
     * @param textMap
     * @return formWidget
     * @throws HsxSBUIBuilderRuntimeException
     */
    private static HsxSBWidget processForms(Element formElement,
	    Map<String, HsxSBWidget> widgetMap,
	    Map<String, HsxSBTextManagerVO> textMap, String screenCode,
	    String isJSEnabled, Properties widgetTypeProperties,
	    Map<String, String> sessionMap, String defaultNavigator) {
	HsxSBWidgetInfo formWidgetInfo = createWidgetInfo(formElement);
	formWidgetInfo.setDefaultNavigator(defaultNavigator);
	HsxSBCompositeWidget formWidget = (HsxSBFormWidget) HsxSBWidgetFactory
		.getWidgetInstance(HTML_FORM, null, null);
	formWidget.createWidget(formWidgetInfo);

	NodeList questionGroups = formElement.getChildNodes();
	final int noOfQuesGroups = questionGroups.getLength();
	for (int i = 0; i < noOfQuesGroups; i++) {
	    String nodeName = questionGroups.item(i).getNodeName();
	    if (questionGroups.item(i).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null
		    && nodeName.contains(XML_QUESTION_GROUP)) {
		Element questionGroupElement = (Element) questionGroups.item(i);

		formWidget.addWidget(processQuestionGroup(questionGroupElement,
			widgetMap, textMap, screenCode, widgetTypeProperties,
			sessionMap, isJSEnabled, defaultNavigator));

	    }

	}
	if (formWidgetInfo.getId() != null) {
	    addtoWidgetMap(formWidgetInfo.getId(), widgetMap, formWidget);
	}
	HsxSBJsHiddenVariablesBean jsHiddenVariablesBean = new HsxSBJsHiddenVariablesBean();
	jsHiddenVariablesBean = HsxSBJavascriptHelper.generateJsHidenVariables(
		formElement,
		widgetMap, jsHiddenVariablesBean);
	HsxSBWidgetUtil.createJsHiddenVariables(formWidget, widgetMap,
		jsHiddenVariablesBean);
	jsHiddenVariablesBean = null;
	// if ("RetrieveAQuote".equalsIgnoreCase(screenCode))
	// {
	// NodeList actionList = (NodeList) formElement.getChildNodes();
	//
	// for (int i = 0; i < actionList.getLength(); i++)
	// {
	// String nodeName = actionList.item(i).getNodeName();
	// if (actionList.item(i).getNodeType() == Node.ELEMENT_NODE
	// && nodeName != null
	// && nodeName.contains(XML_ACTION_LIST))
	// {
	// Element actionElement = (Element) actionList.item(i);
	// formWidget.addWidget(processActionList(actionElement,
	// widgetMap, textMap, screenCode,
	// widgetTypeProperties, sessionMap,isJSEnabled));
	// }
	// }
	// }

	return formWidget;
    }

    /**
     * This method builds the widgets for ActionList tag and initiates the
     * process to build widgets for QUESTION_GROUP
     *
     * @param actionElement
     * @param widgetMap
     * @param textMap
     * @return
     * @throws HsxSBUIBuilderRuntimeException
     */
    /**
     * This method builds the widgets for ActionList tag and initiates the
     * process to build widgets for QUESTION_GROUP.
     *
     * @param actionElement
     * @param widgetMap
     * @param textMap
     * @return actionWidget
     * @throws HsxSBUIBuilderRuntimeException
     */
    private static HsxSBWidget processActionList(Document xmlDocument,
	    Map<String, HsxSBWidget> widgetMap,
	    Map<String, HsxSBTextManagerVO> textMap,
	    Properties widgetTypeProperties, Map<String, String> sessionMap,
	    String isJSEnabled) throws HsxSBUIBuilderRuntimeException {
	// retrieve DefaultNavigator From Screen Level Configs
	String defaultNavigator = STR_EMPTY;
	XPath xpath = XPathFactory.newInstance().newXPath();
	XPathExpression expr = null;
		try {
			expr = xpath.compile(XPATH_DATA_AREA_CONFIG_NAME_DEFAULT_NAVIGATOR);
			Object transaction = expr.evaluate(xmlDocument,
					XPathConstants.NODESET);
			if (transaction != null) {
				NodeList configList = (NodeList) transaction;
				if (configList.item(0) != null) {
					Element defaultNavElement = (Element) configList.item(0);
					defaultNavigator = defaultNavElement
							.getAttribute(STR_VALUE);
				}
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new HsxSBUIBuilderRuntimeException(e.getMessage());
		}
//	if (screenElement != null)
//	{
//	    NodeList dataAreaChildNodesList = screenElement.getChildNodes();
//	    int dataAreaChldNodesLstLength = dataAreaChildNodesList.getLength();
//
//	    for (int dataAreaChildNodesIndex = 0; dataAreaChildNodesIndex < dataAreaChldNodesLstLength; dataAreaChildNodesIndex++)
//	    {
//		Node daNode = dataAreaChildNodesList
//			.item(dataAreaChildNodesIndex);
//		if (daNode != null && daNode.getNodeType() == Node.ELEMENT_NODE)
//		{
//		    String nodeName = daNode.getNodeName();
//		    if (nodeName != null && nodeName.contains(XML_CONFIG))
//		    {
//			Element configElement = (Element) daNode;
//			if (CONFIG_DEFAULT_NAVIGATOR
//				.equalsIgnoreCase(configElement
//					.getAttribute(STR_NAME)))
//			{
//			    defaultNavigator = configElement
//				    .getAttribute(STR_VALUE);
//			    break;
//			}
//		    }
//		}
//	    }
//	}
		Element screenElement = (Element) xmlDocument.getElementsByTagNameNS(
				STR_ASTERISK, XML_DATA_AREA).item(0);

		String screenCode = null;

		if (null != screenElement) {
			NodeList screenCodeElement = screenElement.getElementsByTagNameNS(
					STR_ASTERISK, XML_CODE);
			screenCode = screenCodeElement.item(0).getTextContent();

		}
		Element actionElement = (Element) xmlDocument.getElementsByTagNameNS(
				STR_ASTERISK, XML_ACTION_LIST).item(0);

		HsxSBWidgetInfo actionWidgetInfo = createWidgetInfo(actionElement);
		HsxSBCompositeWidget actionWidget = (HsxSBActionListWidget) HsxSBWidgetFactory
				.getWidgetInstance(HTML_ACTION_GROUP, null, null);
		actionWidget.createWidget(actionWidgetInfo);
		try {
			expr = xpath.compile(XPATH_ACTION_LIST_QUESTION_GROUP);
			Object transaction = expr.evaluate(xmlDocument,
					XPathConstants.NODESET);
			if (transaction != null) {
				NodeList questionGroups = (NodeList) transaction;
				final int qgLen = questionGroups.getLength();
				for (int i = 0; i < qgLen; i++) {
					Element questionGroupElement = (Element) questionGroups
							.item(i);
					actionWidget.addWidget(processQuestionGroup(
							questionGroupElement, widgetMap, textMap,
							screenCode, widgetTypeProperties, sessionMap,
							isJSEnabled, defaultNavigator));
				}
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new HsxSBUIBuilderRuntimeException(e.getMessage());
		}

		return actionWidget;
	}
    /**
     * This method processes and builds the widgets for QUESTION_GROUP tag and
     * initiated the process to build widgets for either SCREEN_QUESTION or
     * ACTION.
     * @param questionGroupElement
     * @param widgetMap
     * @param textMap
     * @return groupWidget
     * @throws HsxSBUIBuilderRuntimeException
     */
    private static HsxSBWidget processQuestionGroup(
	    Element questionGroupElement, Map<String, HsxSBWidget> widgetMap,
	    Map<String, HsxSBTextManagerVO> textMap, String screenCode,
	    Properties widgetTypeProperties, Map<String, String> sessionMap,
	    String isJSEnabled, String defaultNavigator) {
	HsxSBWidgetInfo groupWidgetInfo = createWidgetInfo(questionGroupElement);
	groupWidgetInfo = processIPEQuestionReferenceValues(
		questionGroupElement, groupWidgetInfo);
	HsxSBCompositeWidget groupWidget = (HsxSBQuestionGroupWidget) HsxSBWidgetFactory
		.getWidgetInstance(HTML_QUESTION_GROUP, null, null);
	String tempScreenCode = screenCode;
	if (ctmScreenRefMap.containsKey(screenCode)) {
	    tempScreenCode = ctmScreenRefMap.get(screenCode);
	}
	StringBuilder textItemCode = new StringBuilder();
	String groupId = groupWidgetInfo.getId();
	addtoWidgetMap(groupId, widgetMap, groupWidget);
	// System.out.println("tempScreenCode before Modification :"+tempId);
	// if (tempId.contains("Action"))
	// {
	// tempId = "Action";
	// }
	// System.out.println("tempScreenCode After Modification :"+tempId);

	textItemCode.append(tempScreenCode).append(STR_UNDERSCORE).append(
		groupId);
	HsxSBTextManagerVO textItem = textMap.get(textItemCode.toString());
	if (textItem == null) {
	    textItemCode = new StringBuilder();
	    if (ctmScreenMirrorRefMap.containsKey(screenCode)) {
		tempScreenCode = ctmScreenMirrorRefMap.get(screenCode);
	    }
	    textItemCode.append(tempScreenCode).append(STR_UNDERSCORE).append(
			groupId);
	    textItem = textMap.get(textItemCode.toString());
	}
	if (textItem != null) {
	    String groupHeader = textItem.getLabelText();
	    String nonJsHeader = textItem.getAdditionalText3();
	    String screenReader = textItem.getAdditionalText1();
	    String editLink = textItem.getAdditionalText4();
	    String editNameLink = textItem.getAdditionalText5();
	    String errorText = textItem.getErrorText();
	    String helpText = textItem.getHelpText();
	    groupWidgetInfo.setLabel(groupHeader);
	    groupWidgetInfo.setAdditionalText3(nonJsHeader);
	    groupWidgetInfo.setAdditionalText1(screenReader);
	    groupWidgetInfo.setAdditionalText4(editLink);
	    groupWidgetInfo.setAdditionalText5(editNameLink);
	    groupWidgetInfo.setHelpText(helpText);
	    // Read QuestionGroup Level Style Hint From CTM
	    groupWidgetInfo.setStyleHint(textItem.getStyle());
	    
	    HsxSBWidgetUtil.updateTooltipHiddenString(groupId,
	    helpText);
	    // Read QuestionGroup error Text
	    groupWidgetInfo.setErrorText(errorText);
	}

	 
	groupWidget.createWidget(groupWidgetInfo);

	NodeList screenQuestions = questionGroupElement.getChildNodes();

	// get the last order from questions and pass it to action for tabindex
	int arraySize = HsxSBUIBuilderControllerImpl.questionOrder.size();
	int actionOrder = 0;
	if (arraySize > 0  && null != HsxSBUIBuilderControllerImpl.questionOrder
		    .get(arraySize - 1)) {
		actionOrder = HsxSBUIBuilderControllerImpl.questionOrder
			.get(arraySize - 1);
	}

	final int noOfScreenQues = screenQuestions.getLength();
	for (int i = 0; i < noOfScreenQues; i++) {
	    String nodeName = screenQuestions.item(i).getNodeName();
	    if (screenQuestions.item(i).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null
		    && nodeName.contains(XML_SCREEN_QUESTION)) {
		Element screenQuestionElement = (Element) screenQuestions
			.item(i);
		groupWidget.addWidget(processScreenQuestion(
			screenQuestionElement, widgetMap, textMap, screenCode,
			widgetTypeProperties, sessionMap, isJSEnabled,
			defaultNavigator));
	    } else if (screenQuestions.item(i).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null && nodeName.contains(XML_ACTION)) {
		String actionName = null;
		Element actionElement = (Element) screenQuestions.item(i);
		if (actionElement != null) {
		    actionName = ((Element) screenQuestions.item(i))
			    .getAttribute(STR_NAME);
		    if (STR_PRINT.equalsIgnoreCase(actionName)
			    && STR_NO.equalsIgnoreCase(isJSEnabled)) {
			continue;
		    } else {
			groupWidget.addWidget(processActions(actionElement,
				 textMap, screenCode, actionOrder++,
				defaultNavigator));
		    }
		}
	    }
	}

	return groupWidget;
    }

    /**
     * This method will processes all the Actions and add those to the question
     * group.
     * @param actionElement
     * @param widgetMap
     * @param textMap
     * @return actionWidget
     */
    private static HsxSBWidget processActions(Element actionElement,
	    Map<String, HsxSBTextManagerVO> textMap, String screenCode,
	    int order, String defaultNavigator) {
	HsxSBWidgetInfo actionWidgetInfo = createWidgetInfo(actionElement);
	actionWidgetInfo.setOrder(order);
	actionWidgetInfo.setType(XML_ACTION);
	HsxSBButtonWidget actionWidget = (HsxSBButtonWidget) HsxSBWidgetFactory
		.getWidgetInstance(HTML_ACTION, null, null);

	// Styles Read From CTM
	StringBuilder textItemCode = new StringBuilder();
	String tempScreenCode = screenCode;
	if (ctmScreenRefMap.containsKey(screenCode)) {
	    tempScreenCode = ctmScreenRefMap.get(screenCode);
	}
	textItemCode.append(tempScreenCode).append(STR_UNDERSCORE).append(
		actionWidgetInfo.getId());
	HsxSBTextManagerVO hsxSBTextItem = textMap.get(textItemCode.toString());
	// retrieve the values from the text manager
	if (hsxSBTextItem == null) {
	    hsxSBTextItem = new HsxSBTextManagerVO();
	}
	String labelText = hsxSBTextItem.getLabelText();
	String styleHint = hsxSBTextItem.getStyle();
	String titleText = hsxSBTextItem.getAdditionalText1();

	// updated the widgetInfo with the the text manager data
	actionWidgetInfo.setDisplayText(labelText);
	if (null != defaultNavigator
		&& !defaultNavigator.equalsIgnoreCase(actionWidgetInfo.getId())) {
	    styleHint = CSS_IE_BUTTON_STYLE + HTML_SINGLESPACE + styleHint;
	}
	actionWidgetInfo.setStyleHint(styleHint);
	actionWidgetInfo.setAdditionalText1(titleText);

	actionWidgetInfo.setId(actionWidgetInfo.getId().toLowerCase());
	actionWidget.createWidget(actionWidgetInfo);

	return actionWidget;
    }

    /**
     * This method process all the screen questions and returns that widget to
     * be added to the question group.
     * @param screenQuestionElement
     * @param widgetMap
     * @param textMap
     * @return screenQuestionWidget
     * @throws HsxSBUIBuilderRuntimeException
     */
    private static HsxSBWidget processScreenQuestion(
	    Element screenQuestionElement, Map<String, HsxSBWidget> widgetMap,
	    Map<String, HsxSBTextManagerVO> textMap, String screenCode,
	    Properties widgetTypeProperties, Map<String, String> sessionMap,
	    String isJSEnabled, String defaultNavigator) {

	HsxSBWidgetInfo questionWidgetInfo = createWidgetInfo(screenQuestionElement);
	HsxSBUIBuilderControllerImpl.questionOrder.add(questionWidgetInfo
		.getOrder());
	questionWidgetInfo = processIPEQuestionReferenceValues(
		screenQuestionElement, questionWidgetInfo);
	questionWidgetInfo.setSessionMap(sessionMap);
	// if Question type is TABLE then call
	// processTableScreenQuestion() else
	// continue as it is
	if (null != questionWidgetInfo.getType()
		&& HTML_TABLE.equalsIgnoreCase(questionWidgetInfo.getType())) {
	    return processTableWidget(screenQuestionElement, widgetMap,
		    textMap, questionWidgetInfo, widgetTypeProperties,
		    screenCode, defaultNavigator);

	} else {

	    HsxSBCompositeWidget screenQuestionWidget = (HsxSBQuestionWidget) HsxSBWidgetFactory
		    .getWidgetInstance(HTML_QUESTION, null, null);
	    // To make the type to 'Question' of the QuestionRow
	    // level widget
	    HsxSBWidgetInfo screenQuestionInfo = createWidgetInfo(screenQuestionElement);

	    // Adding Screen Question widgets to map, which has some
	    // SavedValue
	    // but an undefined type
	    // System.out.println("Premium Questions :"+screenQuestionInfo.getId());
	    // System.out.println("saved Value :"+screenQuestionInfo.getSavedValue());
	    if (screenQuestionInfo != null
		    && HsxSBUtil.isBlank(screenQuestionInfo.getType())
		    && HsxSBUtil.isNotBlank(screenQuestionInfo.getSavedValue())) {
		// System.out.println("Special Questions Added :"+screenQuestionInfo.getId());
		addtoWidgetMap(screenQuestionInfo.getId(), widgetMap,
			screenQuestionWidget);
	    }
	    screenQuestionInfo.setSessionMap(sessionMap);
	    screenQuestionInfo.setType(HTML_QUESTION);
	    String questionCode = screenQuestionInfo.getId().toLowerCase();
	    String screenQuestionId = screenQuestionInfo.getId().concat(
		    QUESTION_ID_SUFFIX).toLowerCase();
	    screenQuestionInfo.setId(screenQuestionId);
	    screenQuestionWidget.createWidget(screenQuestionInfo);
	    addtoWidgetMap(screenQuestionId, widgetMap, screenQuestionWidget);
	    // if there is any question group under screen question
	    // we process
	    // it
	    NodeList questionGroups = screenQuestionElement.getChildNodes();

	    final int noOfQuestionGroups = questionGroups.getLength();
		for (int i = 0; i < noOfQuestionGroups; i++) {
		String nodeName = questionGroups.item(i).getNodeName();
		if (questionGroups.item(i).getNodeType() == Node.ELEMENT_NODE
			&& nodeName != null
			&& nodeName.contains(XML_QUESTION_GROUP)) {
		    Element questionGroupElement = (Element) questionGroups
			    .item(i);
		    screenQuestionWidget.addWidget(processQuestionGroup(
			    questionGroupElement, widgetMap, textMap,
			    screenCode, widgetTypeProperties, sessionMap,
			    isJSEnabled, defaultNavigator));
		}

	    }

	    // here reading from the property file will go and the
	    // child widgets
	    // added to this questionRowDivWidget
	    String groupCode = null;
	    String stateCode = null;
	    String enoProduct = null;/**Added for US25094-Add Accounting PL to DPD**/
	    if (sessionMap != null) {
		if (sessionMap.containsKey(OCCUPATION_VARIANT)) {
		    groupCode = sessionMap.get(OCCUPATION_VARIANT);
		}
		if (sessionMap.containsKey(STATE_VARIANT)) {
		    stateCode = sessionMap.get(STATE_VARIANT);
		}
		/**Added for US25094-Add Accounting PL to DPD - start**/		
		if (sessionMap.containsKey(ENO_PRODUCT)) {
			enoProduct = sessionMap.get(ENO_PRODUCT);	
		}/**Added for US25094-Add Accounting PL to DPD - end **/
		}
	
	    HsxSBTextManagerVO hsxSBTextItem = null;
	    String tempScreenCode = screenCode;
	    if (ctmScreenRefMap.containsKey(screenCode)) {
	 	tempScreenCode = ctmScreenRefMap.get(screenCode);
	    }
	    hsxSBTextItem = getVariableQuestionTextCode(textMap,
				questionWidgetInfo, groupCode, stateCode, hsxSBTextItem,
				tempScreenCode,enoProduct);
	    // retrieve the values from the text manager when Text item is not found in current screen
	    if (hsxSBTextItem == null) {
	    	if (ctmScreenMirrorRefMap.containsKey(screenCode)) {
			tempScreenCode = ctmScreenMirrorRefMap.get(screenCode);
		    }
	    	hsxSBTextItem = getVariableQuestionTextCode(textMap,
					questionWidgetInfo, groupCode, stateCode, hsxSBTextItem,
					tempScreenCode,enoProduct);
	    	if (hsxSBTextItem == null) {
			    hsxSBTextItem = new HsxSBTextManagerVO();
			}
	    }
	    // updated the widgetInfo with the the text manager data
	    updateWidgetInfo(screenQuestionInfo, hsxSBTextItem);
	    HsxSBWidgetUtil.updateTooltipHiddenString(questionCode,
		    hsxSBTextItem.getHelpText());

	    // set the hard coded styles are applied
	    setQuestionRowStyles(questionWidgetInfo.getType(),
		    screenQuestionInfo);

	    /*
	     * ResourceBundle labels = ResourceBundle
	     * .getBundle(WIDGET_TYPE_PROPERTIES);
	     */

	    String widgetsHint = STR_EMPTY;

	    // get the appropriate widget hint from the property
	    // file based on
	    // the type,subtype and displayOnly
	    widgetsHint = getWidgetsNameFromProperties(questionWidgetInfo,
		    widgetTypeProperties, widgetsHint);

	    String[] widgetTypeArr = HsxSBStringUtils.tokenize(widgetsHint, STR_COMMA);
		if (widgetTypeArr != null) {
		for (String widgetType : widgetTypeArr) {
		    String childWidgetType = null;
		    HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();
		    widgetInfo.setSessionMap(sessionMap);
		    if (null != widgetType
			    || !STR_EMPTY.equalsIgnoreCase(widgetType)) {
			childWidgetType = widgetType;
			if (HsxSBUtil.isNotBlank(childWidgetType)) {
			childWidgetType = childWidgetType.trim();

			// STR_LABEL will be used to
			// display the labels for the
			// input widgets
			// STR_LABEL_SPAN will be used
			// to display and additional
			// text if there is no input
			// widget
			if (STR_LABEL.equalsIgnoreCase(childWidgetType)) {
			    String labelText = HsxSBWidgetUtil.replaceContent(
				    screenQuestionWidget, screenQuestionInfo
					    .getLabel());

			    widgetInfo.setLabel(labelText);
			    widgetInfo.setMandatory(questionWidgetInfo
				    .isMandatory());

			    widgetInfo.setOptionalText(screenQuestionInfo
				    .getOptionalText());
			    widgetInfo.setDisplayOnly(questionWidgetInfo
				    .getDisplayOnly());
			    widgetInfo.setId(questionWidgetInfo.getId().concat(
				    childWidgetType));
			    widgetInfo.setType(childWidgetType);
			    widgetInfo.setSubType(questionWidgetInfo
				    .getSubType());
			    String id = widgetInfo.getId().toLowerCase();
			    widgetInfo.setId(id);
			    String forID = questionWidgetInfo.getId()
				    + STR_UNDERSCORE
				    + questionWidgetInfo.getType()
				    + STR_UNDERSCORE + HTML_ID;
			    widgetInfo.setLabelForId(forID.toLowerCase());
			    String name = questionWidgetInfo.getId();
			    widgetInfo.setName(name);
			    widgetInfo.setType(childWidgetType);
			    HsxSBWidget childWidget = HsxSBWidgetFactory
				    .getWidgetInstance(childWidgetType, id,
					    name);
			    childWidget.createWidget(widgetInfo);
			    screenQuestionWidget.addWidget(childWidget);
			} else {
			    widgetInfo = questionWidgetInfo;
			    updateWidgetInfo(widgetInfo, hsxSBTextItem);
			    widgetInfo.setSessionMap(sessionMap);
			    String labelText = HsxSBWidgetUtil
				    .replaceContent(screenQuestionWidget,
					    widgetInfo.getLabel());
			    String defaultValue = widgetInfo.getDefaultValue();
			    widgetInfo.setLabel(labelText);
			    widgetInfo.setType(childWidgetType);
			    widgetInfo.setDefaultValue(defaultValue);
			    widgetInfo.setIsJSEnabled(isJSEnabled);

			    // updated the
			    // widgetInfo with the
			    // the text manager
			    // data

			    // widgetInfo.setSavedValue(screenQuestionInfo.getSavedValue());
			    String id = widgetInfo.getId().toLowerCase();
			    widgetInfo.setId(id);
			    String name = questionWidgetInfo.getId();
			    widgetInfo.setName(name);
			    if (TYPE_ACTION.equalsIgnoreCase(widgetInfo
				    .getType())) {
				// widgetInfo.setStyleHint(styleHint);
				widgetInfo.setDisplayText(labelText);
				// To check
				// whether this
				// Action Button
				// is of
				// type
				// ScreenQuestion
				// or Action
				widgetInfo.setAction(XML_SCREEN_QUESTION);

			    }
			    // Check for the Saved
			    // Value if it is empty
			    // then
			    // inject it with
			    // default
			    // value(provided
			    // default value is also
			    // not BLANK or
			    // empty)
			    if (HsxSBUtil.isBlank(widgetInfo.getSavedValue()) && HsxSBUtil.isNotBlank(defaultValue)
					&& !STR_BLANK_VALUE
						.equalsIgnoreCase(defaultValue)) {
				    widgetInfo.setSavedValue(defaultValue);
				}
			    HsxSBWidget childWidget = HsxSBWidgetFactory
				    .getWidgetInstance(childWidgetType, id,
					    name);
			    if (childWidget != null) {
				childWidget.createWidget(widgetInfo);
				addtoWidgetMap(widgetInfo.getId(), widgetMap,
					childWidget);
				screenQuestionWidget.addWidget(childWidget);
			    }

			}
		    }
		}

		}

	    }
	    return screenQuestionWidget;
	}
    }
    /**
     * This method is to get relative question text/variable text from CTM text map.
     * @param textMap
     * @param questionWidgetInfo
     * @param groupCode
     * @param stateCode
     * @param hsxSBTextItem
     * @param tempScreenCode
     * @return hsxSBTextItem
     */
	private static HsxSBTextManagerVO getVariableQuestionTextCode(
			Map<String, HsxSBTextManagerVO> textMap, HsxSBWidgetInfo questionWidgetInfo,
			String groupCode, String stateCode, HsxSBTextManagerVO hsxSBTextItem,
			String tempScreenCode, String enoProduct) {

		StringBuilder textItemCode = new StringBuilder();
		if (HsxSBUtil.isNotBlank(groupCode) || HsxSBUtil.isNotBlank(stateCode)) {

			textItemCode.append(tempScreenCode).append(STR_UNDERSCORE).append(
					questionWidgetInfo.getId()).append(STR_UNDERSCORE).append(
					groupCode);
			/**Added for US25094-Add Accounting PL to DPD - start**/
			if (HsxSBUtil.isNotBlank(groupCode) && STR_GROUP_18.equals(groupCode) && HsxSBUtil.isNotBlank(enoProduct)  ) {				
			
				textItemCode.append(
								STR_UNDERSCORE).append(enoProduct);
			}/**Added for US25094-Add Accounting PL to DPD - end**/
			if (textMap.containsKey(textItemCode.toString())) {
				hsxSBTextItem = textMap.get(textItemCode.toString());
			} else {
				if (HsxSBUtil.isNotBlank(stateCode)) {
					stateCode = stateCode.replaceAll(STR_SINGLE_SPACE,
							STR_UNDERSCORE);
					textItemCode = new StringBuilder();
					textItemCode.append(tempScreenCode).append(STR_UNDERSCORE)
							.append(questionWidgetInfo.getId()).append(
									STR_UNDERSCORE).append(stateCode);
				}
				if (textItemCode != null
						&& HsxSBUtil.isNotBlank(textItemCode.toString())
						&& textMap.containsKey(textItemCode.toString())) {
					hsxSBTextItem = textMap.get(textItemCode.toString());
				} else {
					// If variable text is found then it is set to default entry
					textItemCode = new StringBuilder();
					textItemCode.append(tempScreenCode).append(STR_UNDERSCORE).append(
							questionWidgetInfo.getId());
					hsxSBTextItem = textMap.get(textItemCode.toString());
				}
			}
		} else {
			// To populate text details on small-business-error page
			textItemCode = new StringBuilder();
			textItemCode.append(tempScreenCode).append(STR_UNDERSCORE).append(
					questionWidgetInfo.getId());
			hsxSBTextItem = textMap.get(textItemCode.toString());
		}
		return hsxSBTextItem;
	}

    /**
     * This method process the table widget.
     * @param screenQuestionElement
     * @param widgetMap
     * @param textMap
     * @param questionRowWidgetInfo
     * @return tableQuestionWidget
     * @throws HsxSBUIBuilderRuntimeException
     */
    private static HsxSBWidget processTableWidget(
	    Element screenQuestionElement, Map<String, HsxSBWidget> widgetMap,
	    Map<String, HsxSBTextManagerVO> textMap,
	    HsxSBWidgetInfo questionRowWidgetInfo,
	    Properties widgetTypeProperties, String screenCode,
	    String defaultNavigator) {
	HsxSBCompositeWidget tableQuestionWidget = (HsxSBTableWidget) HsxSBWidgetFactory
		.getWidgetInstance(HTML_TABLE, null, null);

	processTableScreenQuestion(screenQuestionElement, widgetMap, textMap,
		questionRowWidgetInfo, tableQuestionWidget,
		widgetTypeProperties, screenCode, defaultNavigator);
	return tableQuestionWidget;
    }

    /**
     * this method updates the widgetInfo with the the text manager data.
     * @param questionDivInfo
     * @param helpText
     * @param errorText
     * @param nonJavascriptText
     * @param helpTitle
     * @param additionalText1
     * @param additionalText2
     * @param editTitle
     * @param editText
     */
    private static void updateWidgetInfo(HsxSBWidgetInfo screenQuestionInfo,
	    HsxSBTextManagerVO hsxSBTextItem) {

	String labelText = hsxSBTextItem.getLabelText();
	String defaultValue = hsxSBTextItem.getDefaultValue();
	String helpText = hsxSBTextItem.getHelpText();
	String errorText = hsxSBTextItem.getErrorText();
	String nonJavascriptText = hsxSBTextItem.getNonJavascriptText();
	String helpTitle = hsxSBTextItem.getHelpTitle();
	String additionalText1 = hsxSBTextItem.getAdditionalText1();
	String additionalText2 = hsxSBTextItem.getAdditionalText2();
	String editText = hsxSBTextItem.getEditText();
	String additionalText3 = hsxSBTextItem.getAdditionalText3();
	String additionalText4 = hsxSBTextItem.getAdditionalText4();
	String additionalText5 = hsxSBTextItem.getAdditionalText5();
	String additionalText6 = hsxSBTextItem.getAdditionalText6();
	//Not In Use
//	String additionalText7 = hsxSBTextItem.getAdditionalText7();
//	String additionalText8 = hsxSBTextItem.getAdditionalText8();
//	String additionalText9 = hsxSBTextItem.getAdditionalText9();
	//Not In Use
	String styleHint = hsxSBTextItem.getStyle();

	// Optional text for the questions will be added
	String optionalText = STR_EMPTY;

	if (HsxSBUIBuilderControllerImpl.textMap.get(STR_OPTIONAL) != null
		&& HsxSBUIBuilderControllerImpl.textMap.get(STR_OPTIONAL)
			.getLabelText() != null) {
	    optionalText = HsxSBUIBuilderControllerImpl.textMap.get(
		    STR_OPTIONAL).getLabelText();
	}

	screenQuestionInfo.setLabel(labelText);
	screenQuestionInfo.setErrorText(errorText);
	screenQuestionInfo.setHelpText(helpText);
	screenQuestionInfo.setNonJavascriptText(nonJavascriptText);
	screenQuestionInfo.setHelpTitle(helpTitle);
	screenQuestionInfo.setAdditionalText1(additionalText1);
	screenQuestionInfo.setAdditionalText2(additionalText2);
	screenQuestionInfo.setAdditionalText3(additionalText3);
	screenQuestionInfo.setAdditionalText4(additionalText4);
	screenQuestionInfo.setAdditionalText5(additionalText5);
	screenQuestionInfo.setAdditionalText6(additionalText6);
	//Not in Use
//	screenQuestionInfo.setAdditionalText7(additionalText7);
//	screenQuestionInfo.setAdditionalText8(additionalText8);
//	screenQuestionInfo.setAdditionalText9(additionalText9);
	//Not in Use

	screenQuestionInfo.setEditTitle(additionalText6);
	screenQuestionInfo.setEditText(editText);
	screenQuestionInfo.setOptionalText(optionalText);

	// Read Question Level Style Hints from CTM
	if (styleHint != null) {
	    screenQuestionInfo.setStyleHint(styleHint);
	} else {
	    screenQuestionInfo.setStyleHint(STR_EMPTY);
	}

	screenQuestionInfo.setDefaultValue(defaultValue);

    }

    /**
     * This method takes the type,subtype and displayOnly and concatenates them
     * and retrieves the widget hint.
     * @param questionRowWidgetInfo
     * @param labels
     * @param widgetsHint
     * @return widgetsHint
     * @throws HsxSBUIBuilderRuntimeException
     */
    private static String getWidgetsNameFromProperties(
	    HsxSBWidgetInfo questionRowWidgetInfo,
	    Properties widgetTypeProperties, String widgetsHint) {
	if (questionRowWidgetInfo.getType() != null) {
	    final String type = questionRowWidgetInfo.getType().toUpperCase();
	    StringBuilder subType = new StringBuilder(STR_EMPTY);
	    StringBuilder displayOnly = new StringBuilder(STR_EMPTY);
	    final String subTypeStr = questionRowWidgetInfo.getSubType();
		if (HsxSBUtil.isNotBlank(subTypeStr)) {
	    subType.append(STR_DOT);
	    subType.append(subTypeStr.toUpperCase());
	    }
	    final String displayOnlyStr = questionRowWidgetInfo.getDisplayOnly();
		if (HsxSBUtil.isNotBlank(displayOnlyStr)) {
		displayOnly.append(STR_DOT);
		displayOnly.append(displayOnlyStr.toUpperCase());
	    }

	    StringBuilder propertyKey = new StringBuilder();
	    propertyKey.append(type).append(subType).append(displayOnly);
	    // handling the property not found exception
	    Enumeration<Object> propetyKeys = widgetTypeProperties.keys();
	    HashMap<String, String> widgetHintMap = HsxSBUIBuilderControllerImpl.widgetHintMap;
	    if (widgetHintMap == null || widgetHintMap.isEmpty()) {
	    widgetHintMap = populateWidgetHintMap(widgetTypeProperties,
				propetyKeys, widgetHintMap);
	    }
	    if (!widgetHintMap.keySet().contains(
		    propertyKey.toString().toUpperCase())) {
		if (widgetHintMap.keySet().contains(type.toUpperCase())) {
		    propertyKey = new StringBuilder(type);
		} else {
		    propertyKey = new StringBuilder(STR_DEFAULT);
		}

	    }

	    widgetsHint = widgetTypeProperties.getProperty(propertyKey
		    .toString().toUpperCase());
	}
	return widgetsHint;
    }
    /**
     * @param widgetTypeProperties contains widgetTypeProperties
     * @param propetyKeys contains propetyKeys
     * @param widgetHintMap contains widgetHintMap
     * @return widgetHintMap
     */
	private static synchronized HashMap<String, String> populateWidgetHintMap(
			Properties widgetTypeProperties, Enumeration<Object> propetyKeys,
			HashMap<String, String> widgetHintMap) {
	    widgetHintMap = new HashMap<String, String>();
	    while (propetyKeys.hasMoreElements()) {
	    String typeKey = propetyKeys.nextElement().toString();
		String answerValue = widgetTypeProperties.getProperty(typeKey);
		widgetHintMap.put(typeKey, answerValue);
	    }
	    HsxSBUIBuilderControllerImpl.widgetHintMap = widgetHintMap;
	   return widgetHintMap;
	}

    /**
     * This method will set the styles at Li level based on some different
     * combinations of type, subtype and display only.
     * @param type
     * @param questionDivInfo
     */
    private static void setQuestionRowStyles(String type,
	    HsxSBWidgetInfo questionDivInfo) {
	StringBuilder styleHint = new StringBuilder(STR_EMPTY);

	if (TYPE_TEXT.equalsIgnoreCase(type)
		&& SUBTYPE_NUMERIC.equalsIgnoreCase(questionDivInfo
			.getSubType())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_NUMERIC);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)
		&& SUBTYPE_ZIPCODE5.equalsIgnoreCase(questionDivInfo
			.getSubType())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_ZIPCODE5);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)
		&& SUBTYPE_CREDIT_CARD.equalsIgnoreCase(questionDivInfo
			.getSubType())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_CREDITCARD);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)
		&& SUBTYPE_TELEPHONE.equalsIgnoreCase(questionDivInfo
			.getSubType())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_TELEPHONE_US);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)
		&& SUBTYPE_EMAIL.equalsIgnoreCase(questionDivInfo.getSubType())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_EMAIL);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)
		&& SUBTYPE_NARRATIVE.equalsIgnoreCase(questionDivInfo
			.getSubType())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_NARRATIVE);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)
		&& SUBTYPE_PASSWORD.equalsIgnoreCase(questionDivInfo
			.getSubType())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_PASSWORD);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)
		&& STR_YES.equalsIgnoreCase(questionDivInfo.getDisplayOnly())
		&& (SUBTYPE_CURRENCY.equalsIgnoreCase(questionDivInfo
			.getSubType()))) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_DEFAULT);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)
		&& (SUBTYPE_CURRENCY.equalsIgnoreCase(questionDivInfo
			.getSubType()))) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_CURRENCY);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)
		&& STR_YES.equalsIgnoreCase(questionDivInfo.getDisplayOnly())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_STATIC);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_STATIC_TWO_COLUMN);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_DISPLAY_ONLY);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_TEXT.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_TEXT_DEFAULT);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_DROPDOWN.equalsIgnoreCase(type)
		&& SUBTYPE_CURRENCY.equalsIgnoreCase(questionDivInfo
			.getSubType())
		&& STR_YES.equalsIgnoreCase(questionDivInfo.getDisplayOnly())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_DISPLAY_ONLY);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_DROPDOWN.equalsIgnoreCase(type)
		&& SUBTYPE_CURRENCY.equalsIgnoreCase(questionDivInfo
			.getSubType())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_DROPDOWN_CURRENCY);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_DROPDOWN.equalsIgnoreCase(type)
		&& STR_YES.equalsIgnoreCase(questionDivInfo.getDisplayOnly())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_DISPLAY_ONLY);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_DROPDOWN.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_DROPDOWN_DEFAULT);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_STATIC.equalsIgnoreCase(type)
		&& (SUBTYPE_TWOCOLUMN.equalsIgnoreCase(questionDivInfo
			.getSubType()))) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_STATIC);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_STATIC_TWO_COLUMN);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_STATIC.equalsIgnoreCase(type)
		&& STR_YES.equalsIgnoreCase(questionDivInfo.getDisplayOnly())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_STATIC);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_STATIC.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_STATIC);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_CHECKBOX.equalsIgnoreCase(type)
		&& STR_YES.equalsIgnoreCase(questionDivInfo.getDisplayOnly())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_DISPLAY_ONLY);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_CHECKBOX.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_CHECKBOX);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_RADIO.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_RADIO);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_DATE.equalsIgnoreCase(type)
		&& SUBTYPE_MONTHYEAR.equalsIgnoreCase(questionDivInfo
			.getSubType())) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_DATE_MONTHYEAR);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_DATE.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_DATE_DEFAULT);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_ADDRESSSUMMARY.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_ADDRESS_SUMM_DEFAULT);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_FILELINK.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_FLINK_DEFAULT);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_SURCHARGESUMMARY.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_SURCHARGE_SUMM_DEFAULT);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else if (TYPE_LIMITSUMMARY.equalsIgnoreCase(type)) {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_STATIC_TWO_COLUMN);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_DISPLAY_ONLY);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(CSS_LIMIT_SUMM_DEFAULT);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	} else {
	    styleHint.append(CSS_QUESTION_CONTAINER);
	    styleHint.append(STR_SINGLE_SPACE);
	    styleHint.append(questionDivInfo.getStyleHint());
	    questionDivInfo.setStyleHint(styleHint.toString());
	}

    }

    /**
     * This method will process all the config values add them to the widgetInfo
     * object.
     */
    private static void processConfigElementsAndPermittedValues(
	    Element element, HsxSBWidgetInfo widgetInfo) {

	if (element != null) {

	    NodeList childNodes = element.getChildNodes();

	    final int childNodeLen = childNodes.getLength();
		for (int i = 0; i < childNodeLen; i++) {
		final Node childNode = childNodes.item(i);
		String nodeName = childNode.getNodeName();
		if (childNode.getNodeType() == Node.ELEMENT_NODE && HsxSBUtil.isNotBlank(nodeName)) {
		if (nodeName.contains(XML_CONFIG)) {
		    Element config = (Element) childNode;
		    if (XML_QUESTION_TYPE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setType(config.getAttribute(STR_VALUE));

		    } else if (CONFIG_SUBTYPE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setSubType(config.getAttribute(STR_VALUE));

		    } else if (CONFIG_ERROR_INDICATOR.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setErrorIndicator(Boolean
				.parseBoolean(config.getAttribute(STR_VALUE)));

		    } else if (CONFIG_DISPLAY_ONLY.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setDisplayOnly(config
				.getAttribute(STR_VALUE));

		    } else if (CONFIG_IS_MANDATORY.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setMandatory(config.getAttribute(STR_VALUE));

		    } else if (CONFIG_SAVED_VALUE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {

			widgetInfo
				.setSavedValue(config.getAttribute(STR_VALUE));
			if (null == widgetInfo.getDisplayText()) {
			    widgetInfo.setDisplayText(config
				    .getAttribute(STR_VALUE));
			}
		    } else if (CONFIG_MAXIMUM.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAllowMaxValue(config
				.getAttribute(STR_VALUE));

		    } else if (CONFIG_MINIMUM.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAllowMinValue(config
				.getAttribute(STR_VALUE));

		    } else if (CONFIG_STYLE_HINT.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setStyleHint(config.getAttribute(STR_VALUE));

		    } else if (CONFIG_DISPLAY_TEXT.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setDisplayText(config
				.getAttribute(STR_VALUE));

		    } else if (CONFIG_STYLE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setStyle(config.getAttribute(STR_VALUE));

		    } else if (XML_CODE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setId(config.getAttribute(STR_VALUE));

		    } else if (CONFIG_FORM_ACTION.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo
				.setFormAction(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_FORM_NAME.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setFormName(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_YEAR_RANGE_START.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setYearRangeStart(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_YEAR_RANGE_END.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setYearRangeEnd(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_DATE_FORMAT.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo
				.setDateFormat(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_ROWS.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setRows(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_COLUMNS.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setColumns(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_ASSEMBLY_METHOD.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAssemblyMethod(config
				.getAttribute(STR_VALUE));
		    } else if (config.getAttribute(STR_NAME).toLowerCase() != null
			    && config
				    .getAttribute(STR_NAME)
				    .toLowerCase()
				    .contains(CONFIG_ADDRESS_LINE.toLowerCase())) {
			widgetInfo.getAddressLine().add(
				config.getAttribute(STR_VALUE));

		    } else if (CONFIG_ADDRESS_LINE_1.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAddressLine1(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_ADDRESS_LINE_2.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAddressLine2(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_ADDRESS_LINE_3.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAddressLine3(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_ADDRESS_LINE_4.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAddressLine4(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_ADDRESS_LINE_5.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAddressLine5(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_LIMIT.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setLimit(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_EDIT_LINK.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setEditLink(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_SHOW_BLANK.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setShowBlank(config.getAttribute(STR_VALUE));
		    }  else if (CONFIG_VALID_BLANK.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo
				.setValidBlank(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_CURRENCY.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setCurrency(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_SHOW_SIZE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setShowSize(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_USE_FILE_NAME.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setUseFilename(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_TMLOOKUP.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setTMLookup(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_FILE_TYPE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setFileType(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_DECIMAL_PLACES.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setDecimalPlaces(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_THOUSAND_SEPERATOR.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setThousandSeperator(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_MAX_LENGTH.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setMaxLength(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_ACTION.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAction(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_IS_VALIDATION_REQUIRED
			    .equalsIgnoreCase(config.getAttribute(STR_NAME))) {
			widgetInfo.setIsValidationRequired(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_SHOW_OPTIONALTEXT.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setShowOptionalText(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_ISCHECK_REQUIRED.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setIsCheckRequired(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_IS_PROGRESS_BAR_REQUIRED
			    .equalsIgnoreCase(config.getAttribute(STR_NAME))) {
			widgetInfo.setIsProgrssbarRequired(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_ALLOW_PAST_DATE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAllowPastDate(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_ALLOW_FUTURE_DATE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAllowFutureDate(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_ALLOW_ANY_DATE.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setAllowAnyDate(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_DAYS_ALLOWED_FWD.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setDaysAllowedForward(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_DAYS_ALLOWED_BWD.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setDaysAllowedBackward(config
				.getAttribute(STR_VALUE));
		    } else if ((config.getAttribute(STR_NAME).toLowerCase())
			    .contains(CONFIG_ADDITIONAL_VALUE.toLowerCase())) {
			widgetInfo.getAdditionalValueMap().put(
				config.getAttribute(STR_NAME),
				config.getAttribute(STR_VALUE));
		    } else if ((config.getAttribute(STR_NAME).toLowerCase())
			    .contains(CONFIG_ENDORSEMENT.toLowerCase())) {
			widgetInfo.getEndorsementMap().put(
				config.getAttribute(STR_NAME),
				config.getAttribute(STR_VALUE));
		    } else if (CONFIG_DEFAULT_NAVIGATOR.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setDefaultNavigator(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_GROUP_ACTION.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setGroupValidation(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_DATEDEPEND.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setDateDependency(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_COMPARE_VALIDATION_REQUIRED
			    .equalsIgnoreCase(config.getAttribute(STR_NAME))) {
			widgetInfo.setCompareValidationRequired(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_COMPARE_QUESTION_CODE
			    .equalsIgnoreCase(config.getAttribute(STR_NAME))) {
			widgetInfo.setCompareQuestionCode(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_COMPARE_OPERATOR.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo.setCompareOperator(config
				.getAttribute(STR_VALUE));
		    } else if (CONFIG_GROUP_LIMIT.equalsIgnoreCase(config
			    .getAttribute(STR_NAME))) {
			widgetInfo
				.setGroupLimit(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_SESSION_ID.equalsIgnoreCase(config.getAttribute(STR_NAME))) {
		    	widgetInfo.setSessionId(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_HOST_URL.equalsIgnoreCase(config.getAttribute(STR_NAME))) {
		    	widgetInfo.setHostUrl(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_REFERENCE.equalsIgnoreCase(config.getAttribute(STR_NAME))) {
		    	widgetInfo.setReference(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_RETURN_URL.equalsIgnoreCase(config.getAttribute(STR_NAME))) {
		    	widgetInfo.setReturnUrl(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_EXPIRY_URL.equalsIgnoreCase(config.getAttribute(STR_NAME))) {
		    	widgetInfo.setExpiryUrl(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_PAGE_SETID.equalsIgnoreCase(config.getAttribute(STR_NAME))) {
		    	widgetInfo.setPageSetId(config.getAttribute(STR_VALUE));
		    } else if (CONFIG_IS_RENDERING_REQUIRED.equalsIgnoreCase(config.getAttribute(STR_NAME))) {
		    	widgetInfo.setIsRenderingRequired(
		    			config.getAttribute(STR_VALUE));
		    } else if (CONFIG_HEIGHT.equalsIgnoreCase(config.getAttribute(STR_NAME))) {
		    	widgetInfo.setHeight(config.getAttribute(STR_VALUE));
		    }
		 // if was made as else if <-->
		}else if (nodeName.contains(XML_PERMITTED_VALUE)) {
		    Element permittedValue = (Element) childNode;
		    processPermittedValues(permittedValue, widgetInfo);
		}
		}
	    }

	}
    }

    /**
     * This method will process all the permitted values for a particular screen
     * question and sets it to widgetInfo.
     * @param permittedValue contains permitted values
     * @param widgetInfo contains widget info
     */
    private static void processPermittedValues(Element permittedValue,
	    HsxSBWidgetInfo widgetInfo) {
	HsxSBPermitedValues permValue = new HsxSBPermitedValues();
	if (permittedValue.getAttribute(STR_VALUE) != null) {
	    permValue.setValue(permittedValue.getAttribute(STR_VALUE).trim());
	}
	if (HsxSBUtil.isNotBlank(permittedValue.getAttribute(STR_ORDER))) {
	    permValue.setOrder(Integer.parseInt(permittedValue
		    .getAttribute(STR_ORDER)));
	permValue.setDisplayText(permittedValue.getTextContent());
	}
	if (null == widgetInfo.getPermitedVlaues()) {
	    widgetInfo.setPermitedVlaues(new ArrayList<HsxSBPermitedValues>());
	}
	widgetInfo.getPermitedVlaues().add(permValue);
    }

    /**
     * This mehod will process all the IPEQuestionReferences for a particular
     * screen question and sets it to widgetInfo.
     * @param widgetInfo contains widget Info
     */
    private static void processIPEQuestionReferences(
	    Element ipeQuestionReferenceElement, HsxSBWidgetInfo widgetInfo)  {
	HsxSBIPEQuestionReference ipeQuestionReference = new HsxSBIPEQuestionReference();

	ipeQuestionReference.setComparison(ipeQuestionReferenceElement
		.getAttribute(STR_COMPARISON).trim());
	ipeQuestionReference.setQuestionId(ipeQuestionReferenceElement
		.getAttribute(STR_QUESTION_ID).trim());
	ipeQuestionReference.setValue(ipeQuestionReferenceElement.getAttribute(
		STR_VALUE).trim());
	ipeQuestionReference.setDisplayHint(ipeQuestionReferenceElement
		.getAttribute(STR_DISPLAY_HINT).trim());
	if (null == widgetInfo.getIpeQuestionReferenceList()) {
	    widgetInfo
		    .setIpeQuestionReferenceList(new ArrayList<HsxSBIPEQuestionReference>());
	}
	widgetInfo.getIpeQuestionReferenceList().add(ipeQuestionReference);
    }

    /**
     * This method copies the value from XML config elements to widget info.
     * Which is an input for widgets
     * @param element contains elements
     * @return widgetInfo
     */
    private static HsxSBWidgetInfo createWidgetInfo(Element element) {

	HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();

	// id and the name of the widgets are created through
	// contructors
	if (element != null) {
	    widgetInfo.setId(element.getAttribute(XML_CODE));
	    widgetInfo.setCode(element.getAttribute(XML_CODE));
	    widgetInfo.setName(element.getAttribute(STR_NAME));
	    String order = element.getAttribute(XML_ORDER);
	    if (HsxSBUtil.isNotBlank(order)) {
		widgetInfo.setOrder(Integer.parseInt(order));
	    }

	    // widgetInfo.setName(element.getAttribute("name"));
	    String nodeName = element.getNodeName();
	    if (nodeName != null) {
		nodeName = nodeName.toLowerCase();
		if (nodeName.contains(XML_DATA_AREA.toLowerCase())) {
		    widgetInfo.setType(TYPE_SCREEN);
		} else if (nodeName.contains(TYPE_FORM.toLowerCase())) {
		    widgetInfo.setType(TYPE_FORM);
		} else if (nodeName.contains(XML_QUESTION_GROUP.toLowerCase())) {
		    widgetInfo.setType(TYPE_QUESTION_GROUP);
		} else if (nodeName.contains(TYPE_ACTIONLIST.toLowerCase())) {
		    widgetInfo.setType(TYPE_ACTIONLIST);
		} else if (nodeName.contains(XML_ACTION.toLowerCase())) {
		    widgetInfo.setDisplayText(element.getAttribute(STR_NAME));
		    widgetInfo.setId(element.getAttribute(STR_NAME));
		    widgetInfo.setCode(element.getAttribute(STR_NAME));
		}
	    }
	}

	processConfigElementsAndPermittedValues(element, widgetInfo);

	return widgetInfo;

    }

    /**
     * @param textMap contains textmap
     * @param widgetMap contains widgetMap
     * @param widget contains widget
     * @param document contains document details
     * @return null
     */
    public final String processScreenQuestionXML(
	    Map<String, HsxSBTextManagerVO> textMap,
	    Map<String, HsxSBWidget> widgetMap, HsxSBWidget widget,
	    Document document) {
	return null;
    }

    /**
     * @param widgetMap contains widgetMap
     * @param document contains document details
     * @return emptyString
     */
    public final String processScreenQuestionXML(Map<String, HsxSBWidget> widgetMap,
	    HsxSBWidget widget, Document document) {
	return STR_EMPTY;
    }

    /**
     * @param widgetMap contains widget map
     */
    public void updateScreenQuestionsError(Map<String, HsxSBWidget> widgetMap) {
    }

    /**
     * @param error contains errors
     * @param widgetMap contains widget map
     */
    public void updateScreenError(String error,
	    Map<String, HsxSBWidget> widgetMap) {
    }

    /**
     * This method process all table screen questions and returns that widget to
     * be added to the question group.
     * @param screenQuestionElement contains screen Question Element
     * @param widgetMap contains widgetMap
     * @param textMap contains textMap
     */

    private static void processTableScreenQuestion(
	    Element screenQuestionElement, Map<String, HsxSBWidget> widgetMap,
	    Map<String, HsxSBTextManagerVO> textMap,
	    HsxSBWidgetInfo tableWidgetInfo,
	    HsxSBCompositeWidget tableQuestionWidget,
	    Properties widgetTypeProperties, String screenCode,
	    String defaultNavigator) {
	tableWidgetInfo.setType(HTML_TABLE);
	tableQuestionWidget.createWidget(tableWidgetInfo);
	NodeList questionGroups = screenQuestionElement.getChildNodes();
	Integer counter = 0;
	final int noOfQuestionGroups = questionGroups.getLength();
	for (int i = 0; i < noOfQuestionGroups; i++) {
	    String nodeName = questionGroups.item(i).getNodeName();
	    if (questionGroups.item(i).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null
		    && nodeName.contains(XML_QUESTION_GROUP)) {
		Element questionGroupElement = (Element) questionGroups.item(i);
		counter++;
		tableQuestionWidget.addWidget(processTableRow(
			questionGroupElement, widgetMap, textMap, counter,
			widgetTypeProperties, screenCode));
	    }
	}
    }

    /**
     * This method process the QuestionGroup Tag inside the screen question of
     * type TABLE and creates the widget for the same.
     * @param questionGroupElement contains questionGroupElement
     * @param widgetMap contains widgetMap
     * @param textMap contains textMap
     * @return groupWidget contains groupWidget
     */
    private static HsxSBWidget processTableRow(Element questionGroupElement,
	    Map<String, HsxSBWidget> widgetMap,
	    Map<String, HsxSBTextManagerVO> textMap, Integer count,
	    Properties widgetTypeProperties, String screenCode)  {
	HsxSBWidgetInfo groupWidgetInfo = createWidgetInfo(questionGroupElement);

	HsxSBCompositeWidget groupWidget = (HsxSBTRWidget) HsxSBWidgetFactory
		.getWidgetInstance(HTML_TABLEROW, null, null);
	groupWidgetInfo.setType(HTML_TABLEROW);
	groupWidget.createWidget(groupWidgetInfo);

	NodeList screenQuestions = questionGroupElement.getChildNodes();

	if (count != 0) {
	    if (count % 2 == 0) {
		groupWidgetInfo.setStyle(HTML_TR_GREY_STYLE);
	    } else {
		groupWidgetInfo.setStyle(HTML_TR_WHITE_STYLE);
	    }
	}

	int arraySize = HsxSBUIBuilderControllerImpl.questionOrder.size();
	int actionOrder = 1;
	if (arraySize > 0) {
	    actionOrder = HsxSBUIBuilderControllerImpl.questionOrder
		    .get(arraySize - 1) + 1;
	}
	final int noOfScreenQuestions = screenQuestions.getLength();
	for (int i = 0; i < noOfScreenQuestions; i++) {
	    String nodeName = screenQuestions.item(i).getNodeName();
	    if (screenQuestions.item(i).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null
		    && nodeName.contains(XML_SCREEN_QUESTION)) {
		Element screenQuestionElement = (Element) screenQuestions
			.item(i);
		groupWidget.addWidget(processTableData(screenQuestionElement,
			widgetMap, widgetTypeProperties));
	    } else if (screenQuestions.item(i).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null && nodeName.contains(XML_ACTION)) {
		Element actionElement = (Element) screenQuestions.item(i);
		groupWidget.addWidget(processActions(actionElement,
			textMap, screenCode, actionOrder++, null));
	    }

	}

	return groupWidget;
    }

    /**
     * This method process the ScreenQuestion and creates the widget(TD and
     * other) for the same.
     * @param screenQuestionElement contains screenQuestionElement
     * @param widgetMap contains widget map
     * @param textMap contains text map
     * @return tableWidget
     */
    private static HsxSBWidget processTableData(Element screenQuestionElement,
	    Map<String, HsxSBWidget> widgetMap,
	    Properties widgetTypeProperties) {

	HsxSBCompositeWidget tableWidget = null;
	HsxSBWidgetInfo tableWidgetInfo = createWidgetInfo(screenQuestionElement);

	HsxSBWidgetInfo tableRowWidgetInfo = createWidgetInfo(screenQuestionElement);

	if (tableWidgetInfo.getType().equalsIgnoreCase(HTML_TABLEHEADER)) {
	    tableWidget = (HsxSBTHWidget) HsxSBWidgetFactory.getWidgetInstance(
		    HTML_TABLEHEADER, null, null);
	    tableWidgetInfo.setType(HTML_TABLEHEADER);
	    tableWidget.createWidget(tableWidgetInfo);

	} else {
	    tableWidget = (HsxSBTDWidget) HsxSBWidgetFactory.getWidgetInstance(
		    HTML_TABLECOL, null, null);
	    tableWidget.setType(HTML_TABLECOL);
	    tableWidget.createWidget(tableWidgetInfo);

	    if (null != tableRowWidgetInfo
		    && HTML_TABLERADIOENTRY.equalsIgnoreCase(tableRowWidgetInfo
			    .getType())) {
		HsxSBWidgetInfo tableRadioWidgetInfo = createWidgetInfo(screenQuestionElement);
		HsxSBCompositeWidget tableRadioWidget = (HsxSBTDWidget) HsxSBWidgetFactory
			.getWidgetInstance(HTML_TABLERADIOENTRY, null, null);
		tableRadioWidget.setType(HTML_TABLERADIOENTRY);
		tableRadioWidget.createWidget(tableRadioWidgetInfo);

	    }
	}
	/*
	 * ResourceBundle labels = ResourceBundle
	 * .getBundle(WIDGET_TYPE_PROPERTIES);
	 */

	String widgetsHint = STR_EMPTY;
	widgetsHint = getWidgetsNameFromProperties(tableRowWidgetInfo,
		widgetTypeProperties, widgetsHint);

	String[] widgetTypeArr = HsxSBStringUtils.tokenize(widgetsHint, STR_COMMA);
	if (widgetTypeArr != null) {
	    for (String widgetType : widgetTypeArr) {
		String childWidgetType = null;
		HsxSBWidgetInfo widgetInfo = new HsxSBWidgetInfo();
		if (null != widgetType
			|| !STR_EMPTY.equalsIgnoreCase(widgetType)) {
		    childWidgetType = widgetType;
		    widgetInfo = tableRowWidgetInfo;
		    addtoWidgetMap(widgetInfo.getId(), widgetMap, tableWidget);

		}
		String id = widgetInfo.getId();
		String name = tableRowWidgetInfo.getId();
		widgetInfo.setName(name);
		widgetInfo.setType(childWidgetType);
		if (childWidgetType != null) {
		HsxSBWidget childWidget = HsxSBWidgetFactory.getWidgetInstance(
			childWidgetType, id, name);
		childWidget.createWidget(widgetInfo);
		tableWidget.addWidget(childWidget);
		}
	    }

	}

	return tableWidget;

    }

    /**
     * This method adds the widget to widget Hash Map.
     * @param id
     * @param widgetMap
     * @param tableDataWidget
     */
    private static void addtoWidgetMap(String id,
	    Map<String, HsxSBWidget> widgetMap, HsxSBWidget mapWidget) {
	widgetMap.put(id.toLowerCase(), mapWidget);
    }

    /**
     * This method will process all the config values add them to the widgetInfo
     * object.
     * @return widgetInfo
     */
    private static HsxSBWidgetInfo processIPEQuestionReferenceValues(
	    Element element, HsxSBWidgetInfo widgetInfo) {

	if (element != null) {

	    NodeList childNodes = element.getChildNodes();

	    final int childNodeLen = childNodes.getLength();
		for (int i = 0; i < childNodeLen; i++) {
		String nodeName = childNodes.item(i).getNodeName();
		if (childNodes.item(i).getNodeType() == Node.ELEMENT_NODE
			&& nodeName != null
			&& nodeName.contains(XML_IPE_QUESTION_REFERENCE)) {
		    Element ipeQuestionReferenceElement = (Element) childNodes
			    .item(i);
		    processIPEQuestionReferences(ipeQuestionReferenceElement,
			    widgetInfo);

		}
	    }

	}
	return widgetInfo;
    }

    /**
     * This method is used for the unit testing purpose and needs tobe removed.
     * while deploying
     * @param args
     * @return triDirectionCount as String
     */
    // Only for testing
//    public static void main(String args[])
//    {
//
//	Map<String, String> additionalInfoMap = new HashMap<String, String>();
//	additionalInfoMap.put("AdditionalText1".toLowerCase(), "yes");
//
//	HsxSBUIBuilderControllerImpl.setAdditionalInfoMap(additionalInfoMap);
//
//	String inputXML = "";
//	String textInputXML = ""; // replace the below code
//	// to retrieve IPEXML from rule engine
//	try
//	{
//	    ResourceBundle widgetBundle = ResourceBundle
//		    .getBundle(WIDGET_TYPE_PROPERTIES);
//
//	    Properties widgetTypeProperties = new Properties();
//
//	    Enumeration<String> keys = widgetBundle.getKeys();
//	    while (keys.hasMoreElements())
//	    {
//		String key = keys.nextElement();
//		widgetTypeProperties.put(key, widgetBundle.getString(key));
//	    }
//
//	    File f = new File("C:\\TestData\\Dev-CTM-Testing\\about-you.xml");
//	    RandomAccessFile rnd;
//	    rnd = new RandomAccessFile(f, "rw");
//	    String inter = rnd.readLine();
//	    while (inter != null)
//	    {
//		inputXML = inputXML + inter;
//		inter = rnd.readLine();
//	    }
//	    rnd.close();
//
//	    File f2 = new File("C:\\TestData\\TestData.xml");
//	    RandomAccessFile rnd2;
//	    rnd2 = new RandomAccessFile(f2, "rw");
//	    String inter2 = rnd2.readLine();
//	    while (inter2 != null)
//	    {
//		textInputXML = textInputXML + inter2;
//		inter2 = rnd2.readLine();
//	    }
//	    rnd2.close();
//
//	    DocumentBuilderFactory factory = DocumentBuilderFactory
//		    .newInstance();
//	    factory.setNamespaceAware(true);
//	    DocumentBuilder builder = factory.newDocumentBuilder();
//	    HashMap<String, HsxSBTextManagerVO> textItemsMap = new HashMap<String, HsxSBTextManagerVO>();
//	    String txtResponseXml = textInputXML;
//
//	    DocumentBuilderFactory factory1 = DocumentBuilderFactory
//		    .newInstance();
//	    Document xmlDocument = null;
//
//	    try
//	    {
//		DocumentBuilder txtBuilder = factory1.newDocumentBuilder();
//		xmlDocument = txtBuilder.parse(new InputSource(
//			new StringReader(txtResponseXml)));
//	    }
//	    catch (Exception e)
//	    {
//		// TODOAuto-generated catch block
//	    }
//
//	    Element retrieveElement = xmlDocument.getDocumentElement();
//
//	    Element outputElement = (Element) retrieveElement
//		    .getElementsByTagName(STR_OUTPUT).item(0);
//
//	    // NodeList textItemList =(NodeList) outputElement.getChildNodes();
//	    NodeList textItemList = retrieveElement
//		    .getElementsByTagName(TEXT_ITEM);
//	    for (int i = 0; i < textItemList.getLength(); i++)
//	    {
//		Element textItemElement = (Element) textItemList.item(i);
//		NodeList textItemChildList = textItemElement.getChildNodes();
//		HsxSBTextManagerVO textItem = new HsxSBTextManagerVO();
//		String textItemCode = null;
//		for (int j = 0; j < textItemChildList.getLength(); j++)
//		{
//
//		    Node childItem = textItemChildList.item(j);
//		    String nodeName = childItem.getNodeName();
//
//		    if (null != nodeName)
//		    {
//			if (nodeName.contains(TEXT_ITEM_HELP_TEXT))
//			{
//			    textItem.setHelpText(childItem.getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_TEXT_CODE))
//			{
//			    textItem.setTextCode(childItem.getTextContent());
//			    textItemCode = childItem.getTextContent();
//			}
//			else if (nodeName.contains(TEXT_ITEM_LABEL))
//			{
//			    textItem.setLabelText(childItem.getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ERROR_TEXT))
//			{
//			    textItem.setErrorText(childItem.getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_STYLE))
//			{
//			    textItem.setStyle(childItem.getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_PURPOSE))
//			{
//			    textItem.setPurpose(childItem.getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_HELP_TITLE))
//			{
//			    textItem.setHelpTitle(childItem.getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_DEFAULT_VALUE))
//			{
//			    textItem
//				    .setDefaultValue(childItem.getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ADD_TEXT1))
//			{
//			    textItem.setAdditionalText1(childItem
//				    .getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ADD_TEXT2))
//			{
//			    textItem.setAdditionalText2(childItem
//				    .getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ADD_TEXT3))
//			{
//			    textItem.setAdditionalText3(childItem
//				    .getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ADD_TEXT4))
//			{
//			    textItem.setAdditionalText4(childItem
//				    .getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ADD_TEXT5))
//			{
//			    textItem.setAdditionalText5(childItem
//				    .getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ADD_TEXT6))
//			{
//			    textItem.setAdditionalText6(childItem
//				    .getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ADD_TEXT7))
//			{
//			    textItem.setAdditionalText7(childItem
//				    .getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ADD_TEXT8))
//			{
//			    textItem.setAdditionalText8(childItem
//				    .getTextContent());
//			}
//			else if (nodeName.contains(TEXT_ITEM_ADD_TEXT9))
//			{
//			    textItem.setAdditionalText9(childItem
//				    .getTextContent());
//			}
//
//		    }
//		}
//		if (null != textItemCode)
//		{
//		    textItemsMap.put(textItemCode, textItem);
//		}
//
//	    }
//
//	    Document document = builder.parse(new InputSource(new StringReader(
//		    inputXML)));
//	    HsxSBUIBuilderControllerImpl com = new HsxSBUIBuilderControllerImpl();
//	    String outPutHtml = com.generateHTML(document, textItemsMap,
//		    STR_YES, 1, widgetTypeProperties, null, null, null);
//	    String outPutActionHtml = com.generateActionListHTML(document,
//		    textItemsMap, 1, widgetTypeProperties, null, STR_YES);
//	    String outPutTridirectHtml = com.generateFloodLightsHTML(document);
//	    //System.out.println(outPutHtml);
//
//	}
//	catch (Exception e)
//	{
//	    e.printStackTrace();
//	}
//    }
/**
 * @param oDEdocument contains ODE document
 * @return triDirectcontent
 */
    public static String buildTriDirectHTML(Document oDEdocument) {
	StringBuilder triDirectcontent = new StringBuilder();
	if (oDEdocument != null
		&& oDEdocument.getElementsByTagNameNS(STR_ASTERISK,
			XML_DATA_AREA) != null) {
	    Element screenElement = (Element) oDEdocument
		    .getElementsByTagNameNS(STR_ASTERISK, XML_DATA_AREA)
		    .item(0);

	    String screenName = null;

	    Map<String, String> customVariablesMap = null;

	    if (null != screenElement) {
		NodeList screenCodeElement = screenElement
			.getElementsByTagNameNS(STR_ASTERISK, XML_CODE);
		screenName = screenCodeElement.item(0).getTextContent();
	    }
	    Element tagListElement = (Element) oDEdocument
		    .getElementsByTagNameNS(STR_ASTERISK, XML_TAG_LIST).item(0);
	    if (tagListElement != null) {
		customVariablesMap = processTagList(tagListElement);
	    }
			/*String doubleClickTag = HsxSBTriDirectTagDataProvider
					.provideHTMLContent(screenName, customVariablesMap);
			if (doubleClickTag != null) {
				triDirectcontent = triDirectcontent.append(doubleClickTag);
			}*/
			// Hidden Variables accessible by JS for F5 tags
			StringBuilder hiddenJSTagVariables = new StringBuilder();
			if(null!=customVariablesMap){
			for (Entry<String, String> entry : customVariablesMap.entrySet()) {
				String tagKey = entry.getKey();
				if (!((Character.isDigit(tagKey.charAt(tagKey.length() - 1))) || tagKey
						.startsWith(STR_LETTER_U))) {
					String hiddenJSTag = STR_EMPTY;
					hiddenJSTag = LESS_THAN + HTML_INPUT + STR_SINGLE_SPACE
							+ HTML_TYPE + HTML_EQUALS + HTML_SINGLE_QUOTE
							+ HTML_HIDDEN + HTML_SINGLE_QUOTE
							+ STR_SINGLE_SPACE + HTML_NAME + HTML_EQUALS
							+ HTML_SINGLE_QUOTE + entry.getKey()
							+ HTML_SINGLE_QUOTE + HTML_SINGLESPACE + HTML_ID
							+ HTML_EQUALS + HTML_SINGLE_QUOTE + entry.getKey()
							+ STR_ID + HTML_SINGLE_QUOTE + HTML_SINGLESPACE
							+ HTML_VALUE + HTML_EQUALS + HTML_SINGLE_QUOTE
							+ entry.getValue() + HTML_SINGLE_QUOTE
							+ STR_FORWARD_SLASH + GREATER_THAN;
					hiddenJSTagVariables = hiddenJSTagVariables
							.append(hiddenJSTag);
				}
			}
			}
			if (hiddenJSTagVariables != null) {
				triDirectcontent.append(hiddenJSTagVariables);
			}
	}
	return triDirectcontent.toString();
    }
    /**
     * @param tagListElement contains tag List Elements
     * @return customVariablesMap
     */
    private static Map<String, String> processTagList(Element tagListElement) {
	Map<String, String> customVariablesMap = new HashMap<String, String>();

	NodeList childNodes = tagListElement.getChildNodes();

	final int childNodeLen = childNodes.getLength();
	for (int i = 0; i < childNodeLen; i++) {
	    String nodeName = childNodes.item(i).getNodeName();
	    if (childNodes.item(i).getNodeType() == Node.ELEMENT_NODE
		    && nodeName != null && nodeName.contains(XML_CONFIG)) {
		Element config = (Element) childNodes.item(i);
		customVariablesMap.put(config.getAttribute(STR_NAME), config.getAttribute(STR_VALUE));
	    }
	}
	return customVariablesMap;
    }
}
