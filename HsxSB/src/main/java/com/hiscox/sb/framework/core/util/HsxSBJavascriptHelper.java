package com.hiscox.sb.framework.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.util.bean.HsxSBJSDependencyBean;
import com.hiscox.sb.framework.core.util.bean.HsxSBJsHiddenVariablesBean;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBPermitedValues;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;

/**
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:21 AM
 */
public class HsxSBJavascriptHelper implements HsxSBUIBuilderConstants {

	/**
	 * This method creates the Java script hidden string based on the IPE
	 * references of the screen question.The string generated will represents
	 * which question is dependent on what question and for what value.Eg.
	 * ClaimsAgainstYou_Radio_No_Id-onClick-No-ClaimDetails_question_id-Hide
	 * Above String will represent that when onClick event happens on
	 * ClaimsAgainstYou_Radio_No_Id(radio button Id)
	 * ClaimDetails_question_id(Child Question Div Id) has to be Hidden.
	 * @param formElement contain form elements
	 * @param widgetMap contains widget Map values
	 * @param jsHiddenVariablesBean carries JS Hidden Variables Bean
	 * @return jsHiddenVariablesBean
	 */
	public static HsxSBJsHiddenVariablesBean generateJsHidenVariables(
			Element formElement, Map<String, HsxSBWidget> widgetMap,
			HsxSBJsHiddenVariablesBean jsHiddenVariablesBean) {

		// Form a hidden variable with all the question codes with
		// Percentage Sub type
		XPath xpath = XPathFactory.newInstance().newXPath();
		XPathExpression expr = null;

		populatePercentageHiddenString(formElement, jsHiddenVariablesBean,
				xpath, expr);

		jsHiddenVariablesBean.setJsActionNames(retrieveJsActionNames(
				formElement, xpath, expr));

		NodeList iPEQuestionReferenceList = formElement.getElementsByTagNameNS(
				STR_ASTERISK, XML_IPE_QUESTION_REFERENCE);
		Map<String, String> dependencyValidationMap = new HashMap<String, String>();
		Map<Integer, HsxSBJSDependencyBean> dependencyMap = new HashMap<Integer, HsxSBJSDependencyBean>();
		final int ipeQuesRefLen = iPEQuestionReferenceList.getLength();
		int mapIndex = 1;
		for (int ipeIndex = 0; ipeIndex < ipeQuesRefLen; ipeIndex++) {

			Element iPEQuestionReferenceElement = (Element) iPEQuestionReferenceList
					.item(ipeIndex);
			String superQuestionCode = iPEQuestionReferenceElement
					.getAttribute(CONFIG_NAME_QID);
			Node parentNode = iPEQuestionReferenceElement.getParentNode();

			String parentNodeName = parentNode.getNodeName();
			String subQuestionCode = null;
			if (XML_ACTION.equalsIgnoreCase(parentNodeName)) {
				subQuestionCode = parentNode.getAttributes().getNamedItem(
						STR_NAME).getNodeValue();
			} else {
				subQuestionCode = parentNode.getAttributes().getNamedItem(
						XML_CODE).getNodeValue();
			}
			String triggerValue = iPEQuestionReferenceElement
					.getAttribute(XML_QUESTION_VALUE);
			String comparission = iPEQuestionReferenceElement
					.getAttribute(STR_COMPARISON);
			String displayHint = iPEQuestionReferenceElement
					.getAttribute(STR_DISPLAY_HINT);
			if (HsxSBUtil.isBlank(displayHint)) {
				displayHint = STR_BLANK_VALUE;
			}

			if (STR_NOT_EQUALS.equalsIgnoreCase(comparission) || STR_CHANGE_OPTION.equalsIgnoreCase(comparission)) {
				superQuestionCode = superQuestionCode.toLowerCase();
				String type = STR_BLANK_VALUE;
				String buttonTriggerValue = STR_BLANK_VALUE;
				StringBuilder permittedValueString = new StringBuilder(
						STR_EMPTY);
				if (widgetMap != null
						&& widgetMap.containsKey(superQuestionCode)) {
					HsxSBWidget superQuesWidget = widgetMap
							.get(superQuestionCode);
					// System.out.println("Super Question Type :"+superQuesWidget.getWidgetInfo().getType());
					HsxSBWidgetInfo supQuesWidhetInfo = superQuesWidget
							.getWidgetInfo();
					type = supQuesWidhetInfo.getType();
					buttonTriggerValue = supQuesWidhetInfo.getSavedValue();

					if (STR_RADIO.equalsIgnoreCase(type)) {
						buttonTriggerValue = HsxSBUtil
								.removeSymbolsAndNumbers(buttonTriggerValue);
						if (supQuesWidhetInfo.getPermitedVlaues() != null) {
							ArrayList<HsxSBPermitedValues> permitedValuesList = (ArrayList<HsxSBPermitedValues>) supQuesWidhetInfo
									.getPermitedVlaues();

							for (HsxSBPermitedValues permitedValues : permitedValuesList) {
								permittedValueString.append(HsxSBUtil
										.removeSymbolsAndNumbers(permitedValues
												.getValue()));
								permittedValueString.append(SYMBOL_CAPS);
							}
							if (permittedValueString != null
									&& permittedValueString.length() > 0) {
								int permValLength = permittedValueString
										.length();
								permittedValueString = permittedValueString
										.deleteCharAt(permValLength - 1);
							}
						}
					}

				}
				if (STR_CHANGE_OPTION.equalsIgnoreCase(comparission)) {
					StringBuilder jsPageSubmitLogicString = jsHiddenVariablesBean.getJsPageSubmitLogicString();
					if (jsPageSubmitLogicString == null || jsPageSubmitLogicString.toString().trim().length() == 0) {
						jsPageSubmitLogicString = new StringBuilder(STR_EMPTY);
					} else {
						jsPageSubmitLogicString.append(STR_LESSTHAN_GREATERTHAN_DELIMETER);
					}
					jsPageSubmitLogicString.append(subQuestionCode.toLowerCase());
					jsPageSubmitLogicString.append(STR_TILDE);
					jsPageSubmitLogicString.append(type.toLowerCase());
					jsPageSubmitLogicString.append(STR_TILDE);
					jsPageSubmitLogicString.append(displayHint);
					if (STR_RADIO.equalsIgnoreCase(type)) {
						jsPageSubmitLogicString.append(SYMBOL_ARROW);
						jsPageSubmitLogicString.append(permittedValueString);
					}
					jsPageSubmitLogicString.append(SYMBOL_HASH);
					if (triggerValue != null) {
						triggerValue = triggerValue.replaceAll(ID_SUFFIX, STR_EMPTY);
					}
					jsPageSubmitLogicString.append(triggerValue);
					jsHiddenVariablesBean.setJsPageSubmitLogicString(jsPageSubmitLogicString);

				} else if (STR_NOT_EQUALS.equalsIgnoreCase(comparission)) {
				StringBuilder jsButtonEnableDisableString = jsHiddenVariablesBean
						.getJsButtonEnableDisableString();
				if (jsButtonEnableDisableString == null) {
					jsButtonEnableDisableString = new StringBuilder(STR_EMPTY);
				}
				jsButtonEnableDisableString.append(superQuestionCode);
				jsButtonEnableDisableString.append(STR_TILDE);
				jsButtonEnableDisableString.append(type);
				jsButtonEnableDisableString.append(STR_TILDE);
				jsButtonEnableDisableString.append(subQuestionCode);
				jsButtonEnableDisableString.append(STR_TILDE);
				jsButtonEnableDisableString.append(buttonTriggerValue);
				jsButtonEnableDisableString.append(STR_TILDE);
				jsButtonEnableDisableString.append(displayHint);
				if (STR_RADIO.equalsIgnoreCase(type)) {
					jsButtonEnableDisableString.append(SYMBOL_ARROW);
					jsButtonEnableDisableString.append(permittedValueString);
				}
				jsButtonEnableDisableString.append(HTML_PIPE_SYMBOL);
				jsHiddenVariablesBean
						.setJsButtonEnableDisableString(jsButtonEnableDisableString);
				}

			} else if (STR_GREATER_THAN.equalsIgnoreCase(comparission)) {
				superQuestionCode = superQuestionCode.toLowerCase();
				if (widgetMap != null
						&& widgetMap.containsKey(superQuestionCode)) {
					HsxSBWidget superQuesWidget = widgetMap
							.get(superQuestionCode);
					HsxSBWidgetInfo supQuesWidhetInfo = superQuesWidget
							.getWidgetInfo();
					String parentQuestionType = supQuesWidhetInfo.getType();
					if (TYPE_TEXT.equalsIgnoreCase(parentQuestionType)) {
						StringBuilder textBoxHideUnHideString = new StringBuilder();
						textBoxHideUnHideString.append(superQuestionCode);
						textBoxHideUnHideString.append(TEXTBOX_ID_SUFFIX);
						textBoxHideUnHideString.append(STR_TILDE);
						textBoxHideUnHideString.append(comparission);
						textBoxHideUnHideString.append(STR_TILDE);
						textBoxHideUnHideString.append(triggerValue);
						textBoxHideUnHideString.append(STR_TILDE);
						textBoxHideUnHideString.append(subQuestionCode);
						textBoxHideUnHideString.append(QUESTION_ID_SUFFIX);
						textBoxHideUnHideString.append(HTML_PIPE_SYMBOL);
						StringBuilder textBoxHideUnHideJsString = jsHiddenVariablesBean
								.getTextBoxHideUnHideJsString();
						if (textBoxHideUnHideJsString == null) {
							textBoxHideUnHideJsString = new StringBuilder(
									STR_EMPTY);
						}
						textBoxHideUnHideJsString
								.append(textBoxHideUnHideString);
						jsHiddenVariablesBean
								.setTextBoxHideUnHideJsString(textBoxHideUnHideJsString);
					}

				}
			} else if (STR_EQUALS.equalsIgnoreCase(comparission)) {
				String parentString = (superQuestionCode + QUESTION_ID_SUFFIX)
						.toLowerCase()
						+ STR_TILDE + triggerValue;
				updateValidationMap(dependencyValidationMap, subQuestionCode,
						parentString, widgetMap);
				superQuestionCode = superQuestionCode.toLowerCase();
				if (widgetMap != null
						&& widgetMap.containsKey(superQuestionCode)) {
					HsxSBWidget superQuesWidget = widgetMap
							.get(superQuestionCode);
					HsxSBWidgetInfo supQuesWidhetInfo = superQuesWidget
							.getWidgetInfo();
					String parentQuestionType = supQuesWidhetInfo.getType();
					List<HsxSBPermitedValues> parQuesPermValuesList = supQuesWidhetInfo
							.getPermitedVlaues();
					if (STR_RADIO.equalsIgnoreCase(parentQuestionType)) {
						mapIndex = createDependencyMapForRadioButton(
								parQuesPermValuesList, subQuestionCode,
								superQuestionCode, triggerValue, dependencyMap,
								mapIndex);
					} else if (STR_DROPDOWN
							.equalsIgnoreCase(parentQuestionType)) {
						HsxSBJSDependencyBean jsDependencyBean = new HsxSBJSDependencyBean();
						jsDependencyBean
								.setParentQuestionElementId(superQuestionCode
										+ DROPDOWN_ID_SUFFIX);
						jsDependencyBean.setEventDescription(STR_ONCHANGE);
						jsDependencyBean.setElementValue(triggerValue);
						jsDependencyBean.setChildQuestionDivId(subQuestionCode
								+ QUESTION_ID_SUFFIX);
						jsDependencyBean.setDisplayHint(displayHint);
						dependencyMap.put(mapIndex, jsDependencyBean);
						mapIndex++;
					} else if (STR_CHECKBOX
							.equalsIgnoreCase(parentQuestionType)) {
						if (STR_ENABLE.equalsIgnoreCase(displayHint)) {

							subQuestionCode = subQuestionCode.toLowerCase();
							if (widgetMap != null
									&& widgetMap.containsKey(subQuestionCode)) {
								HsxSBWidget childQuesWidget = widgetMap
										.get(subQuestionCode);
								HsxSBWidgetInfo childQuesWidhetInfo = childQuesWidget
										.getWidgetInfo();
								String childQuesType = childQuesWidhetInfo
										.getType();
								String childQuesSubType = childQuesWidhetInfo
										.getSubType();
								if (HsxSBUtil.isNotBlank(childQuesType)) {
									childQuesType = childQuesType.trim();
								}
								if (HsxSBUtil.isNotBlank(childQuesSubType)) {
									childQuesSubType = childQuesSubType.trim();
								} else {
									childQuesSubType = STR_BLANK_VALUE;
								}
								StringBuilder jsQuestionEnableDisableString = jsHiddenVariablesBean
										.getJsQuestionEnableDisableString();
								if (jsQuestionEnableDisableString == null) {
									jsQuestionEnableDisableString = new StringBuilder(
											STR_EMPTY);
								}
								if (TYPE_TELEPHONE
										.equalsIgnoreCase(childQuesType)) {
									childQuesType = HTML_TEXT;
								}
								jsQuestionEnableDisableString
										.append(superQuestionCode);
								jsQuestionEnableDisableString
										.append(CHECKBOX_ID_SUFFIX);
								jsQuestionEnableDisableString.append(STR_TILDE);
								jsQuestionEnableDisableString
										.append(subQuestionCode);
								jsQuestionEnableDisableString.append(STR_TILDE);
								jsQuestionEnableDisableString
										.append(childQuesType);
								jsQuestionEnableDisableString.append(STR_TILDE);
								jsQuestionEnableDisableString
										.append(childQuesSubType);
								jsQuestionEnableDisableString.append(STR_TILDE);
								jsQuestionEnableDisableString
										.append(displayHint);
								jsQuestionEnableDisableString
										.append(HTML_PIPE_SYMBOL);
								jsHiddenVariablesBean
										.setJsQuestionEnableDisableString(jsQuestionEnableDisableString);
							}
						} else if (STR_BLANK_VALUE
								.equalsIgnoreCase(displayHint)) {
							HsxSBJSDependencyBean jsDependencyBean = new HsxSBJSDependencyBean();
							jsDependencyBean
									.setParentQuestionElementId(superQuestionCode
											+ CHECKBOX_ID_SUFFIX);
							jsDependencyBean
									.setEventDescription(STR_CHECKBOX_ONCLICK);
							jsDependencyBean.setElementValue(triggerValue);
							jsDependencyBean
									.setChildQuestionDivId(subQuestionCode
											+ QUESTION_ID_SUFFIX);
							jsDependencyBean.setDisplayHint(displayHint);
							dependencyMap.put(mapIndex, jsDependencyBean);
							mapIndex++;
						}
					}

				}

			} else if (STR_IS_ONE_OF.equalsIgnoreCase(comparission)) {
				String parentString = (superQuestionCode + QUESTION_ID_SUFFIX)
						.toLowerCase()
						+ STR_TILDE + triggerValue;
				updateValidationMap(dependencyValidationMap, subQuestionCode,
						parentString, widgetMap);
			} else if (STR_SHOW_ON_LOGIC_STRING.equalsIgnoreCase(comparission)) {
				// System.out.println("SubQuestionCode :"+subQuestionCode);
				StringBuilder jsIsOneOfAndLogicString = jsHiddenVariablesBean
						.getJsIsOneOfAndLogicString();
				if (jsIsOneOfAndLogicString == null) {
					jsIsOneOfAndLogicString = new StringBuilder(STR_EMPTY);
				} else {
					jsIsOneOfAndLogicString
							.append(STR_LESSTHAN_GREATERTHAN_DELIMETER);
				}
				jsIsOneOfAndLogicString.append(subQuestionCode);
				jsIsOneOfAndLogicString.append(QUESTION_ID_SUFFIX);

				jsIsOneOfAndLogicString.append(STR_DOUBLE_CAP_DELIMETER);
				String supQ1 = STR_EMPTY;
				String supQ2 = STR_EMPTY;
				if (superQuestionCode.contains(SYMBOL_DUOBLE_TILDE)) {
					supQ1 = superQuestionCode.split(SYMBOL_DUOBLE_TILDE)[0]
							.toLowerCase();
					supQ2 = superQuestionCode.split(SYMBOL_DUOBLE_TILDE)[1]
							.toLowerCase();
				}
				String trigValue1 = STR_EMPTY;
				String trigValue2 = STR_EMPTY;
				if (triggerValue.contains(SYMBOL_DUOBLE_TILDE)) {
					trigValue1 = triggerValue.split(SYMBOL_DUOBLE_TILDE)[0];
					trigValue2 = triggerValue.split(SYMBOL_DUOBLE_TILDE)[1];
				}

				generateDropdownDependencyString(widgetMap,
						jsIsOneOfAndLogicString, supQ1, trigValue1);
				jsIsOneOfAndLogicString.append(STR_DOUBLE_CAP_DELIMETER);
				generateRadioDependencyString(widgetMap,
						jsIsOneOfAndLogicString, supQ2, trigValue2);
				jsHiddenVariablesBean
						.setJsIsOneOfAndLogicString(jsIsOneOfAndLogicString);

			} else if (STR_VALIDATE_ON_LOGIC_STRING
					.equalsIgnoreCase(comparission)) {
				StringBuilder jsIsOneOfAndValidationString = jsHiddenVariablesBean
						.getJsIsOneOfAndValidationString();
				if (jsIsOneOfAndValidationString == null) {
					jsIsOneOfAndValidationString = new StringBuilder(STR_EMPTY);
				} else {
					jsIsOneOfAndValidationString
							.append(STR_LESSTHAN_GREATERTHAN_DELIMETER);
				}
				jsIsOneOfAndValidationString.append(subQuestionCode);
				jsIsOneOfAndValidationString
						.append(STR_DOUBLE_AMPERSAND_SYMBOL);
				String supQ1 = STR_EMPTY;
				String supQ2 = STR_EMPTY;
				if (superQuestionCode.contains(SYMBOL_DUOBLE_TILDE)) {
					supQ1 = superQuestionCode.split(SYMBOL_DUOBLE_TILDE)[0];
					supQ2 = superQuestionCode.split(SYMBOL_DUOBLE_TILDE)[1];
				}
				String trigValue1 = STR_EMPTY;
				String trigValue2 = STR_EMPTY;
				if (triggerValue.contains(SYMBOL_DUOBLE_TILDE)) {
					trigValue1 = triggerValue.split(SYMBOL_DUOBLE_TILDE)[0];
					trigValue2 = triggerValue.split(SYMBOL_DUOBLE_TILDE)[1];
				}
				jsIsOneOfAndValidationString.append(supQ1);
				jsIsOneOfAndValidationString.append(SYMBOL_DUOBLE_LESSTHAN_END);
				jsIsOneOfAndValidationString.append(trigValue1);
				jsIsOneOfAndValidationString
						.append(STR_DOUBLE_AMPERSAND_SYMBOL);

				jsIsOneOfAndValidationString.append(supQ2);
				jsIsOneOfAndValidationString.append(SYMBOL_DUOBLE_LESSTHAN_END);
				jsIsOneOfAndValidationString.append(trigValue2);
				jsHiddenVariablesBean
						.setJsIsOneOfAndValidationString(jsIsOneOfAndValidationString);
			} else if (STR_SHOW_ON_AND_CASE.equalsIgnoreCase(comparission)) {
				StringBuilder jsAndCaseLogicString = jsHiddenVariablesBean
						.getJsAndCaseLogicString();

				if (jsAndCaseLogicString == null) {
					jsAndCaseLogicString = new StringBuilder(STR_EMPTY);
				} else {
					jsAndCaseLogicString
							.append(STR_LESSTHAN_GREATERTHAN_DELIMETER);
				}
				jsAndCaseLogicString.append(subQuestionCode
						+ QUESTION_ID_SUFFIX);
				jsAndCaseLogicString.append(STR_DOUBLE_CAP_DELIMETER);
				String supQ1 = STR_EMPTY;
				String supQ2 = STR_EMPTY;

				if (superQuestionCode.contains(SYMBOL_DUOBLE_TILDE)) {
					supQ1 = superQuestionCode.split(SYMBOL_DUOBLE_TILDE)[0]
							.toLowerCase();
					supQ2 = superQuestionCode.split(SYMBOL_DUOBLE_TILDE)[1]
							.toLowerCase();
				}
				String trigValue1 = STR_EMPTY;
				String trigValue2 = STR_EMPTY;
				if (triggerValue.contains(SYMBOL_DUOBLE_TILDE)) {
					trigValue1 = triggerValue.split(SYMBOL_DUOBLE_TILDE)[0];
					trigValue2 = triggerValue.split(SYMBOL_DUOBLE_TILDE)[1];
				}
				generateRadioDependencyString(widgetMap, jsAndCaseLogicString,
						supQ1, trigValue1);
				jsAndCaseLogicString.append(STR_DOUBLE_AMPERSAND_SYMBOL);
				generateRadioDependencyString(widgetMap, jsAndCaseLogicString,
						supQ2, trigValue2);
				jsHiddenVariablesBean
						.setJsAndCaseLogicString(jsAndCaseLogicString);
			} else if (STR_SHOW_ON_DD_AND_CASE.equalsIgnoreCase(comparission)) {
				// System.out.println("SubQuestionCode :"+subQuestionCode);
				StringBuilder jsDropdownAndLogicString = jsHiddenVariablesBean
						.getJsDropdownAndLogicString();
				if (jsDropdownAndLogicString == null) {
					jsDropdownAndLogicString = new StringBuilder(STR_EMPTY);
				} else {
					jsDropdownAndLogicString
							.append(STR_LESSTHAN_GREATERTHAN_DELIMETER);
				}
				jsDropdownAndLogicString.append(subQuestionCode);
				jsDropdownAndLogicString.append(QUESTION_ID_SUFFIX);

				jsDropdownAndLogicString.append(STR_DOUBLE_CAP_DELIMETER);

				String supQ1 = STR_EMPTY;
				String supQ2 = STR_EMPTY;
				if (superQuestionCode.contains(SYMBOL_DUOBLE_TILDE)) {
					supQ1 = superQuestionCode.split(SYMBOL_DUOBLE_TILDE)[0]
							.toLowerCase();
					supQ2 = superQuestionCode.split(SYMBOL_DUOBLE_TILDE)[1]
							.toLowerCase();
				}
				String trigValue1 = STR_EMPTY;
				String trigValue2 = STR_EMPTY;
				if (triggerValue.contains(SYMBOL_DUOBLE_TILDE)) {
					trigValue1 = triggerValue.split(SYMBOL_DUOBLE_TILDE)[0];
					trigValue2 = triggerValue.split(SYMBOL_DUOBLE_TILDE)[1];
				}

				generateDropdownDependencyString(widgetMap,
						jsDropdownAndLogicString, supQ1, trigValue1);
				jsDropdownAndLogicString.append(STR_DOUBLE_CAP_DELIMETER);
				generateDropdownDependencyString(widgetMap,
						jsDropdownAndLogicString, supQ2, trigValue2);

				jsHiddenVariablesBean.setJsDropdownAndLogicString(jsDropdownAndLogicString);

			} else if (STR_EQUALLS_MAXLENGTH.equalsIgnoreCase(comparission)) {
				StringBuilder jsAutoTabLogicString = jsHiddenVariablesBean.getJsAutoTabLogicString();
				if (jsAutoTabLogicString == null || jsAutoTabLogicString.toString().trim().length() == 0) {
					jsAutoTabLogicString = new StringBuilder(STR_EMPTY);
				} else {
					jsAutoTabLogicString.append(STR_LESSTHAN_GREATERTHAN_DELIMETER);
				}

				jsAutoTabLogicString.append(subQuestionCode);
				jsAutoTabLogicString.append(STR_TILDE);
				jsAutoTabLogicString.append(comparission);
				jsAutoTabLogicString.append(STR_TILDE);
				jsAutoTabLogicString.append(triggerValue);
				jsAutoTabLogicString.append(STR_TILDE);
				jsAutoTabLogicString.append(displayHint);
				jsHiddenVariablesBean.setJsAutoTabLogicString(jsAutoTabLogicString);

			} else if (STR_GROUPING.equalsIgnoreCase(comparission)) {
				StringBuilder jsFilterByGroupingString = jsHiddenVariablesBean.getJsFilterByGroupingString();
				if (jsFilterByGroupingString == null || jsFilterByGroupingString.toString().trim().length() == 0) {
					jsFilterByGroupingString = new StringBuilder(STR_EMPTY);
				} else {
					jsFilterByGroupingString.append(STR_LESSTHAN_GREATERTHAN_DELIMETER);
				}
				jsFilterByGroupingString.append(superQuestionCode);
				jsFilterByGroupingString.append(STR_TILDE);
				jsFilterByGroupingString.append(comparission);
				jsFilterByGroupingString.append(STR_TILDE);
				jsFilterByGroupingString.append(subQuestionCode);
				jsFilterByGroupingString.append(STR_TILDE);
				jsFilterByGroupingString.append(displayHint);
				jsHiddenVariablesBean.setJsFilterByGroupingString(jsFilterByGroupingString);
			}
		}
		String hiddenString = createHiddenStringFromDependencymap(dependencyMap);
		if (hiddenString == null) {
			hiddenString = STR_EMPTY;
		}
		StringBuilder hideUnHideJsString = new StringBuilder(hiddenString);
		jsHiddenVariablesBean.setHideUnHideJsString(hideUnHideJsString);

		populatedependencyValidationString(jsHiddenVariablesBean,
				dependencyValidationMap);

		return jsHiddenVariablesBean;

	}

	private static void generateDropdownDependencyString(
			Map<String, HsxSBWidget> widgetMap,
			StringBuilder jsIsOneOfAndLogicString, String supQ1,
			String trigValue1) {
		if (widgetMap.containsKey(supQ1)) {
			HsxSBWidget supQ1Widget = widgetMap.get(supQ1);
			HsxSBWidgetInfo supQ1WidgetInfo = supQ1Widget
					.getWidgetInfo();

			String supQ1Type = STR_EMPTY;
			if (HsxSBStringUtils.isNotEmpty(supQ1WidgetInfo.getType())) {
				supQ1Type = supQ1WidgetInfo.getType();
				supQ1Type = supQ1Type.toLowerCase();
			}

			jsIsOneOfAndLogicString.append(supQ1.toLowerCase());
			jsIsOneOfAndLogicString.append(STR_UNDERSCORE);
			jsIsOneOfAndLogicString.append(supQ1Type);
			jsIsOneOfAndLogicString.append(ID_SUFFIX);
			jsIsOneOfAndLogicString.append(SYMBOL_DUOBLE_LESSTHAN_END);
			jsIsOneOfAndLogicString.append(STR_ONCHANGE);
			jsIsOneOfAndLogicString.append(SYMBOL_DUOBLE_LESSTHAN_END);
			jsIsOneOfAndLogicString.append(trigValue1);

		}
	}

	private static void populatePercentageHiddenString(Element formElement,
			HsxSBJsHiddenVariablesBean jsHiddenVariablesBean, XPath xpath,
			XPathExpression expr) {
		try {
			expr = xpath
					.compile(XPATH_SCREEN_QUESTIONS_WITH_PERCENTAGE_SUBTYPE);
			if (formElement != null) {
				generatePercentageHiddenString(formElement,
						jsHiddenVariablesBean, expr);
			}

		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new HsxSBUIBuilderRuntimeException(e.getMessage());
		}
	}

	private static void generatePercentageHiddenString(Element formElement,
			HsxSBJsHiddenVariablesBean jsHiddenVariablesBean,
			XPathExpression expr) throws XPathExpressionException {
		Object transaction = expr.evaluate(formElement, XPathConstants.NODESET);
		if (transaction != null) {
			StringBuilder textPercentString = new StringBuilder(STR_EMPTY);
			NodeList nodeList = (NodeList) transaction;
			final int nodeLen = nodeList.getLength();
			for (int nodeIndex = 0; nodeIndex < nodeLen; nodeIndex++) {
				Element node = (Element) nodeList.item(nodeIndex);
				final Element parentNode = (Element) node.getParentNode();
				if (parentNode != null) {
					String questionCode = parentNode.getAttribute(XML_CODE);
					if (questionCode != null) {
						textPercentString.append(questionCode);
						if (nodeIndex < nodeLen - 1) {
							textPercentString.append(JSP_PERCENT);
						}
					}
				}
			}
			jsHiddenVariablesBean.setTextPercentString(textPercentString);
		}
	}

	private static void generateRadioDependencyString(
			Map<String, HsxSBWidget> widgetMap,
			StringBuilder jsAndCaseLogicString, String supQ2, String trigValue2) {
		if (widgetMap.containsKey(supQ2)) {
			HsxSBWidget supQ2Widget = widgetMap.get(supQ2);
			HsxSBWidgetInfo supQ2WidgetInfo = supQ2Widget.getWidgetInfo();
			String supQ2Type = STR_EMPTY;
			if (HsxSBStringUtils.isNotEmpty(supQ2WidgetInfo.getType())) {
				supQ2Type = supQ2WidgetInfo.getType();
				supQ2Type = supQ2Type.toLowerCase();
			}
			List<HsxSBPermitedValues> permittedValuesList = supQ2WidgetInfo
					.getPermitedVlaues();
			int ampCounter = 0;
			for (HsxSBPermitedValues permitedValue : permittedValuesList) {
				String optionValue = permitedValue.getValue();
				if (HsxSBStringUtils.isNotEmpty(optionValue)) {
					optionValue = optionValue.toLowerCase();
				} else {
					optionValue = STR_EMPTY;
				}
				jsAndCaseLogicString.append(supQ2);
				jsAndCaseLogicString.append(HTML_UNDERSCORE);
				jsAndCaseLogicString.append(supQ2Type);
				jsAndCaseLogicString.append(HTML_UNDERSCORE);
				jsAndCaseLogicString.append(optionValue);
				jsAndCaseLogicString.append(ID_SUFFIX);
				jsAndCaseLogicString.append(SYMBOL_DUOBLE_LESSTHAN_END);
				if (trigValue2.equalsIgnoreCase(optionValue)) {
					jsAndCaseLogicString.append(STR_DISPLAY_HINT_SHOW);
				} else {
					jsAndCaseLogicString.append(STR_DISPLAY_HINT_HIDE);
				}
				ampCounter++;
				if (ampCounter < permittedValuesList.size()) {
					jsAndCaseLogicString.append(STR_DOUBLE_AMPERSAND_SYMBOL);
				}
			}
		}
	}

	private static void populatedependencyValidationString(
			HsxSBJsHiddenVariablesBean jsHiddenVariablesBean,
			Map<String, String> dependencyValidationMap) {
		if (dependencyValidationMap != null
				&& !dependencyValidationMap.isEmpty()) {
			StringBuilder dependencyValidationString = jsHiddenVariablesBean
					.getDependencyValidationString();
			if (dependencyValidationString == null) {
				dependencyValidationString = new StringBuilder(STR_EMPTY);
			}
			Collection<String> depValCollection = dependencyValidationMap
					.keySet();
			for (Iterator<String> keyIetrator = depValCollection.iterator(); keyIetrator
					.hasNext();) {
				String key = keyIetrator.next();
				String value = dependencyValidationMap.get(key);
				if (dependencyValidationString != null
						&& HsxSBUtil.isNotBlank(dependencyValidationString
								.toString())) {
					dependencyValidationString
							.append(SYMBOL_DUOBLE_LESSTHAN_END);
				}
				dependencyValidationString.append(key);
				dependencyValidationString.append(SYMBOL_ARROW);
				dependencyValidationString.append(value);
			}

			jsHiddenVariablesBean
					.setDependencyValidationString(dependencyValidationString);
		}
	}

	/**
	 * @param formElement contain form elements
	 * @param xpath contains xpath
	 * @param expr contains expression
	 * @return jsActionNames
	 */

	private static StringBuilder retrieveJsActionNames(Element formElement,
			XPath xpath, XPathExpression expr) {
		StringBuilder jsActionNames = new StringBuilder(STR_EMPTY);
		try {
			expr = xpath
					.compile(XPATH_SCREEN_QUESTION_CONFIG_NAME_TYPE_VALUE_ACTION);
			Object transaction = expr.evaluate(formElement,
					XPathConstants.NODESET);
			if (transaction != null) {
				NodeList quesActionList = (NodeList) transaction;
				if (quesActionList != null) {
					final int actionLength = quesActionList.getLength();
					for (int actionIndex = 0; actionIndex < actionLength; actionIndex++) {
						Node configAction = quesActionList.item(actionIndex);
						Element questionElement = (Element) configAction
								.getParentNode();
						if (questionElement.hasAttribute(XML_CODE)) {
							jsActionNames.append(questionElement
									.getAttribute(XML_CODE));
							jsActionNames.append(STR_TILDE);

						}

					}
				}
			}

			expr = xpath.compile(XPATH_ACTION);
			transaction = expr.evaluate(formElement, XPathConstants.NODESET);
			if (transaction != null) {
				NodeList buttonActionList = (NodeList) transaction;
				if (buttonActionList != null) {
					final int buttActLen = buttonActionList.getLength();
					for (int actionIndex = 0; actionIndex < buttActLen; actionIndex++) {
						Element actionElement = (Element) buttonActionList
								.item(actionIndex);
						if (actionElement != null
								&& actionElement.hasAttribute(STR_NAME)) {
							jsActionNames.append(actionElement
									.getAttribute(STR_NAME));
							jsActionNames.append(STR_TILDE);

						}
					}
				}
			}

		} catch (XPathExpressionException e) {
			e.printStackTrace();
			throw new HsxSBUIBuilderRuntimeException(e.getMessage());
		}

		return jsActionNames;
		// Element screenQuestionElement = (Element) screenQuestionList
		// .item(screenIndex);
		// if (screenQuestionElement != null) {
		// NodeList configList = screenQuestionElement.getChildNodes();
		// if (configList != null) {
		// final int configLength = configList.getLength();
		// for (int configIndex = 0; configIndex < configLength; configIndex++)
		// {
		// Node screenChildNode = configList.item(configIndex);
		// if (screenChildNode != null && screenChildNode.getNodeType() ==
		// Node.ELEMENT_NODE) {
		// String configNodeName = screenChildNode.getNodeName();
		// if (configNodeName != null
		// && configNodeName.contains(XML_CONFIG)) {
		// Element configElement = (Element) screenChildNode;
		// if (configElement.hasAttribute(STR_NAME)
		// && configElement.hasAttribute(STR_VALUE)) {
		// if (XML_QUESTION_TYPE
		// .equalsIgnoreCase(configElement
		// .getAttribute(STR_NAME))
		// && XML_ACTION
		// .equalsIgnoreCase(configElement
		// .getAttribute(STR_VALUE))) {
		// if (screenQuestionElement
		// .hasAttribute(XML_CODE)) {
		// jsActionNames
		// .append(screenQuestionElement
		// .getAttribute(XML_CODE));
		// jsActionNames.append(STR_TILDE);
		// break;
		// }
		//
		// }
		//
		// }
		//
		// }
		// }
		// }
		// }
		// }
	}

	// private static StringBuilder updateJsActionNames(StringBuilder
	// jsActionNames,
	// NodeList screenQuestionList, int screenIndex) {
	// Element screenQuestionElement = (Element) screenQuestionList
	// .item(screenIndex);
	// if (screenQuestionElement != null
	// && screenQuestionElement.hasAttribute(STR_NAME)) {
	// jsActionNames.append(screenQuestionElement.getAttribute(STR_NAME));
	// jsActionNames.append(STR_TILDE);
	//
	// }
	// return jsActionNames;
	// }

	/**
	 * @param dependencyValidationMap contains dependency Validation Map
	 * @param subQuestionCode contains sub Question Code
	 * @param parentString contains parent String
	 * @param widgetMap contains widget Map
	 */
	private static void updateValidationMap(
			Map<String, String> dependencyValidationMap,
			String subQuestionCode, String parentString,
			Map<String, HsxSBWidget> widgetMap) {
		if (widgetMap.containsKey(subQuestionCode.toLowerCase())) {
			HsxSBWidget childQuestionWidget = widgetMap.get(subQuestionCode
					.toLowerCase());
			if (childQuestionWidget != null
					&& childQuestionWidget.getWidgetInfo() != null) {
				String childType = childQuestionWidget.getWidgetInfo()
						.getType();
				if (!HTML_QUESTION_GROUP.equalsIgnoreCase(childType)) {
					String subQuesCode = (subQuestionCode + QUESTION_ID_SUFFIX)
							.toLowerCase();
					if (!dependencyValidationMap.containsKey(subQuesCode)) {
						dependencyValidationMap.put(subQuesCode, parentString);
					} else {
						String trigValue = dependencyValidationMap
								.get(subQuesCode);
						trigValue = trigValue + STR_DOUBLE_AMPERSAND_SYMBOL
								+ parentString;
						dependencyValidationMap.put(subQuesCode, trigValue);
					}
				}
			}
		}
	}

	/**
	 * @param parQuesPermValuesList contains Question Permitted Values List
	 * @param subQuestionCode carries sub Question Code
	 * @param superQuestionCode carries super Question Code
	 * @param triggerValue carries trigger Value
	 * @param dependencyMap carries dependency map
	 * @param mapIndex carries map Index
	 * @return returnMapIndex
	 */
	private static int createDependencyMapForRadioButton(
			List<HsxSBPermitedValues> parQuesPermValuesList,
			String subQuestionCode, String superQuestionCode,
			String triggerValue,
			Map<Integer, HsxSBJSDependencyBean> dependencyMap, int mapIndex) {
		int returnMapIndex = 0;
		for (HsxSBPermitedValues permittedValues : parQuesPermValuesList) {
			String permittedValue = permittedValues.getValue();
			permittedValue = HsxSBUtil.removeSymbolsAndNumbers(permittedValue);
			HsxSBJSDependencyBean jsDependencyBean = new HsxSBJSDependencyBean();
			jsDependencyBean.setParentQuestionElementId(superQuestionCode
					+ RADIO_BUTTON_SUFFIX + permittedValue + ID_SUFFIX);
			jsDependencyBean.setEventDescription(STR_ONCLICK);
			jsDependencyBean.setElementValue(permittedValue);
			jsDependencyBean.setChildQuestionDivId(subQuestionCode
					+ QUESTION_ID_SUFFIX);
			triggerValue = HsxSBUtil.removeSymbolsAndNumbers(triggerValue);
			if (triggerValue != null
					&& triggerValue.equalsIgnoreCase(permittedValue)) {
				jsDependencyBean.setDisplayHint(STR_SHOW);
			} else {
				jsDependencyBean.setDisplayHint(STR_HIDE);
			}
			dependencyMap.put(mapIndex, jsDependencyBean);
			mapIndex++;
			returnMapIndex = mapIndex;
		}
		return returnMapIndex;

	}

	/**
	 * @param dependencyMap carries dependency map
	 * @return hiddenString
	 */
	private static String createHiddenStringFromDependencymap(
			Map<Integer, HsxSBJSDependencyBean> dependencyMap) {
		Collection<Integer> c = dependencyMap.keySet();
		String hiddenString = STR_EMPTY;
		StringBuilder tempBuffer = new StringBuilder();
		for (Iterator<Integer> iterator = c.iterator(); iterator.hasNext();) {
			int mapIndex = iterator.next();
			if (dependencyMap.containsKey(mapIndex)) {
				HsxSBJSDependencyBean jsDependencyBean = dependencyMap
						.get(mapIndex);
				tempBuffer = tempBuffer.append(
						jsDependencyBean.getParentQuestionElementId()).append(
						STR_TILDE + jsDependencyBean.getEventDescription())
						.append(STR_TILDE + jsDependencyBean.getElementValue())
						.append(
								STR_TILDE
										+ jsDependencyBean
												.getChildQuestionDivId())
						.append(STR_TILDE + jsDependencyBean.getDisplayHint())
						.append(HTML_PIPE_SYMBOL);
			}

		}
		hiddenString = tempBuffer.toString();

		int lastIndex = 0;
		if (hiddenString != null) {
			lastIndex = hiddenString.lastIndexOf(HTML_PIPE_SYMBOL);
			if (lastIndex > 0) {
				StringBuilder sb = new StringBuilder(hiddenString);
				sb.replace(lastIndex, lastIndex + 1, STR_EMPTY);
				hiddenString = sb.toString().toLowerCase();
			}
		}
		return hiddenString;
	}

}
