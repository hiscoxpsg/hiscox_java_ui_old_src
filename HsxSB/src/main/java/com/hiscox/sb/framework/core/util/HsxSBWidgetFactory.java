package com.hiscox.sb.framework.core.util;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBActionListWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBDivWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBFormWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBHelpWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionGroupWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBQuestionWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTDWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTHWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTRWidget;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBTableWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBAnchorWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBButtonWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBCheckBoxWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBDropDownWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBHiddenWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBIframeWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBImgWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBLabelWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBOptionWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBPasswordWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBPopUpButtonWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBRadioWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBSpanWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBTableSpanWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBTextAreaWidget;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBTextWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBAdditionalTextWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBAddressSummaryWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBDateWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBEditWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBEndorsementWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBErrorWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBLimitSummaryWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBStaticWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBSurchargeSummaryWidget;
import com.hiscox.sb.framework.core.widgets.simple.HsxSBTelephoneWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This is the factory class responsible for creation of individual widgets
 * based on the type classes.
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:47 AM
 */
public class HsxSBWidgetFactory implements HsxSBUIBuilderConstants {
/**
 * hsxSBBaseWidget initialisation.
 */
    public HsxSBBaseWidget hsxSBBaseWidget;
/**
 * Default Constructor.
 */
    public HsxSBWidgetFactory() {

    }

    /**
     * Returns the appropriate Widget based on the String parameter.
     * @return widget
     *
     * @param widgetType contains widget type
     * @param id contains id
     * @param name contains name
     */
    public static HsxSBWidget getWidgetInstance(String widgetType, String id,
	    String name) {

	HsxSBWidget widget = null;
	widgetType = widgetType.trim();
	if (TYPE_TEXT.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBTextWidget(id, name);
	} else if (TYPE_FORM.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBFormWidget(id, name);
	} else if (TYPE_DIV.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBDivWidget(id, name);
	} else if (TYPE_SCREEN.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBScreenWidget(id, name);
	} else if (TYPE_QUESTION_GROUP.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBQuestionGroupWidget(id, name);
	} else if (TYPE_QUESTION.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBQuestionWidget(id, name);
	} else if (TYPE_LABEL.equalsIgnoreCase(widgetType)
		|| TYPE_LABEL_SPAN.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBLabelWidget(id, name);
	} else if (TYPE_ANCHOR.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBAnchorWidget(id, name);
	} else if (TYPE_OPTION.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBOptionWidget(id, name);
	} else if (TYPE_RADIO.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBRadioWidget(id, name);
	} else if (TYPE_DROPDOWN.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBDropDownWidget(id, name);
	} else if (TYPE_ACTION.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBButtonWidget(id, name);
	} else if (TYPE_SUBMIT.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBButtonWidget(id, name);
	} else if (TYPE_CHECKBOX.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBCheckBoxWidget(id, name);
	} else if (TYPE_SPAN.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBSpanWidget(id, name);
	} else if (TYPE_TABLE_SPAN.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBTableSpanWidget(id, name);
	} else if (TYPE_HIDDEN.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBHiddenWidget(id, name);
	} else if (TYPE_ERROR.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBErrorWidget(id, name);
	} else if (TYPE_ACTIONGROUP.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBActionListWidget(id, name);
	} else if (TYPE_TEXTAREA.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBTextAreaWidget(id, name);
	} else if (TYPE_IMAGE.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBImgWidget(id, name);
	} else if (TYPE_PASSWORD.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBPasswordWidget(id, name);
	} else if (TYPE_DATE.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBDateWidget(id, name);
	} else if (TYPE_TELEPHONE.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBTelephoneWidget(id, name);
	} else if (TYPE_ADDITIONALTEXT.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBAdditionalTextWidget(id, name);
	} else if (TYPE_HELP.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBHelpWidget(id, name);
	} else if (TYPE_EDIT.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBEditWidget(id, name);
	} else if (TYPE_TABLE.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBTableWidget(id, name);
	} else if (TYPE_TABLEROW.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBTRWidget(id, name);
	} else if (TYPE_TABLEHEADER.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBTHWidget(id, name);
	} else if (TYPE_TABLECOLLABELENTRY.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBTDWidget(id, name);
	} else if (TYPE_TABLERADIOENTRY.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBTDWidget(id, name);
	} else if (TYPE_STATIC.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBStaticWidget(id, name);
	} else if (TYPE_LIMITSUMMARY.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBLimitSummaryWidget(id, name);
	} else if (TYPE_SURCHARGESUMMARY.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBSurchargeSummaryWidget(id, name);
	} else if (TYPE_ADDRESSSUMMARY.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBAddressSummaryWidget(id, name);
	} else if (TYPE_FILELINK.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBEndorsementWidget(id, name);
	} else if (TYPE_ACTION_HREF.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBPopUpButtonWidget(id, name);
	} else if (TYPE_IFRAME.equalsIgnoreCase(widgetType)) {
	    widget = new HsxSBIframeWidget(id, name);
	}

	/*
	 * Not there in the EA anymore else if
	 * (PAGEINCLUDE.equalsIgnoreCase(widgetType)) { widget = new
	 * HsxSBJSPPageIncludeWidget(id,name); }
	 */

	return widget;
    }

}
