package com.hiscox.sb.framework.core.widgets;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.w3c.dom.Element;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class comprises of one are more primitive widgets, and defines the
 * functionality to add and remove the widget from the list.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:10 AM
 */
public abstract class HsxSBCompositeWidget extends HsxSBBaseWidget {

    /**
	 *
	 */
	private static final long serialVersionUID = 6731754420787991991L;

	public HsxSBCompositeWidget() {

	super();
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     * @param widgetInfo contains widget info
     */
    public HsxSBCompositeWidget(String id, String name,
	    HsxSBWidgetInfo widgetInfo) {

	super(id, name, widgetInfo);
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     */
    public HsxSBCompositeWidget(String id, String name) {

	super(id, name);
    }
/**
 * Method for sorting widgets.
 * @param widgetsList contains list of widgets
 */
    public void sortWidgets(List<HsxSBWidget> widgetsList) {
	// System.out.println("Sorting all Screen Questions :");
	Collections.sort(widgetsList, new Comparator<Object>(){
	    public int compare(Object a, Object b) {
		return ((((HsxSBBaseWidget) a).getWidgetInfo().getOrder()) - (((HsxSBBaseWidget) b)
			.getWidgetInfo().getOrder()));
	    }
	});

    }

    /**
     * @param widget - widget
     */
    public abstract void addWidget(HsxSBWidget widget);

    /**
     *
     * @param element - element
     */
    public abstract void createWidget(Element element);

    /**
     *
     * @param widget - widget
     */
    public abstract void removeWidget(HsxSBWidget widget);

}
