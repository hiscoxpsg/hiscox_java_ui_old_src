package com.hiscox.sb.framework.core.renderer.htmlrenderers.primitive;

import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLBaseRenderer;
import com.hiscox.sb.framework.core.widgets.primitive.HsxSBAnchorWidget;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the HTML Anchor tag.
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:03 AM
 */
public class HsxSBAnchorRenderer extends HsxSBHTMLBaseRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Anchor.
     * @param widget contains widget data
     * @return content
     */
    @Override
    public String renderContent(final HsxSBWidget widget) {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBAnchorWidget anchorWidget = (HsxSBAnchorWidget) widget;

	String value = null;
	if (null != anchorWidget.getWidgetInfo().getSavedValue()
		&& anchorWidget.getWidgetInfo().getSavedValue().trim().length() > 0) {
	    value = anchorWidget.getWidgetInfo().getSavedValue();
	} else {
	    value = anchorWidget.getWidgetInfo().getDefaultValue();
	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_CLASS);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_HELPICON_DIV_STYLE);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_HREF);
	contentBuilder.append(HTML_EQUALS);
	if (null != value) {
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(value);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	}
	contentBuilder.append(HTML_SINGLESPACE);
	contentBuilder.append(HTML_TITLE);
	contentBuilder.append(HTML_EQUALS);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_ANCHORHELP);
	contentBuilder.append(HTML_SINGLE_QUOTE);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	if (null != anchorWidget.getWidgetInfo().getLabel()) {
	    contentBuilder.append(anchorWidget.getWidgetInfo().getLabel());

	}
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_SPAN);
	contentBuilder.append(HTML_END_TAG);

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_ANCHOR);
	contentBuilder.append(HTML_END_TAG);
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
