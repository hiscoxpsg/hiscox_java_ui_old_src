package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBDivWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;

/**
 * This class renders the DIV tag.
 * @author Cognizant
 * @version 1.0
 //* @created 25-Mar-2010 6:27:12 AM
 */
public class HsxSBDivRenderer extends HsxSBHTMLCompositeRenderer implements
	HsxSBUIBuilderConstants {

    /**
     * This method will return the String for rendering the HTML Div tag.
     * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws run time exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBDivWidget divWidget = (HsxSBDivWidget) widget;
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);

	if (null != divWidget.getWidgetInfo()
		&& null != divWidget.getWidgetInfo().getStyle()
		&& divWidget.getWidgetInfo().getStyle().trim().length() > 0) {
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(divWidget.getWidgetInfo().getStyle());
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	}

	if (null != divWidget.getWidgetInfo()
		&& null != divWidget.getWidgetInfo().getId()
		&& divWidget.getWidgetInfo().getId().trim().length() > 0) {
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_ID);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(divWidget.getWidgetInfo().getId());
	    contentBuilder.append(QUESTION_ID_SUFFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	}
	contentBuilder.append(HTML_END_TAG);

	Iterator<HsxSBWidget> widgetsListIterator = divWidget.getWidgetsList()
		.iterator();
	while (widgetsListIterator.hasNext()) {
	    HsxSBWidget innerWidget = widgetsListIterator.next();

	    HsxSBRenderer renderer = HsxSBHTMLRendererFactory
		    .getRendererInstance(innerWidget.getWidgetInfo().getType());
	    String rendererd = renderer.renderContent(innerWidget);
	    contentBuilder.append(rendererd);
	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }

}
