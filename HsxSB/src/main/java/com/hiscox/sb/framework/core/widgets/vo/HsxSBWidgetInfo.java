package com.hiscox.sb.framework.core.widgets.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is the value object which carries the config elements value under
 * each tag in Screen XML(e.g DataArea, QuestionGroup, ScreenQuestion), and also
 * holds the Screen XML element properties.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:48 AM
 */
public class HsxSBWidgetInfo implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = -8570957879347660973L;
	private String defaultValue;
    private String displayText;
    private boolean errorIndicator;
    private String id;
    private String code;
    private String isMandatory;
    private String allowMaxValue;
    private String allowMinValue;
    private String name;
    private int order;
    private String displayOnly;
    private String editLink;
    private String showBlank;
    private String validBlank;
    private String currency;
    private List<HsxSBPermitedValues> permitedVlaues = new ArrayList<HsxSBPermitedValues>();
    private List<HsxSBIPEQuestionReference> ipeQuestionReferenceList = new ArrayList<HsxSBIPEQuestionReference>();
    private String policyMap;
    private String savedValue;
    private String style;
    private String styleHint;
    private String type;
    private String subType;
    private String validationHint;
    private String tabIndex;
    private String target;
    public HashMap<String, String> configValueMap;
    private String label;
    private String errorText;
    private String helpText;
    private String pageErrorText;
    private String pageIntroText;
    private String nonJavascriptText;
    private String helpTitle;
    private String additionalText1;
    private String additionalText2;
    private String additionalText3;
    private String additionalText4;
    private String additionalText5;
    private String additionalText6;
  //Not In Use
  //private String additionalText7;
  //private String additionalText8;
  //private String additionalText9;
  //Not In Use
    private String editTitle;
    private String editText;
    private String yearRangeStart;
    private String yearRangeEnd;
    private String formAction;
    private String currentPage;
    private String errorStyle;
    private String dateFormat;
    private String columns;
    private String rows;
    private String assemblyMethod;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String addressLine4;
    private String addressLine5;
    private String optionalText;
    private String limit;
    private String showSize;
    private String useFilename;
    private String fileType;
    private String decimalPlaces;
    private String thousandSeperator;
    private String maxLength;
    private String action;
    // used for the period date validation
    private String allowPastDate;
    private String allowFutureDate;
    private String allowAnyDate;
    private String daysAllowedForward;
    private String daysAllowedBackward;
    private String isValidationRequired;
    private String isProgrssbarRequired;
    private String additionalValue;
    public HashMap<String, String> additionalValueMap;
    public HashMap<String, String> endorsementMap;
    private String formName;
    private String labelForId;
    private List<String> addressLine;
    private String questionTypeSpecificStyle;
    private String tMLookup;
    private Map<String, String> sessionMap;
    // this is the default action will get executed once the enter button is
    // clicked.
    private String defaultNavigator;
    private String showOptionalText;
    // this is particular to bypassing validation for a group which is dependent
    // on its parent question
    private String groupValidation;
    // this is used of the check box mandatory to mark yes.
    private String isCheckRequired;
    // this is used of the validate the date and time fields combined.
    private String dateDependency;
    // this is used to get the JS enabled status for the browser.
    private String isJSEnabled;
    // this is used to validate a field based on value of some other field.
    private String compareValidationRequired;
    private String compareQuestionCode;
    // this is used to validate based on comparing value
    private String compareOperator;
    // this is used to validate group questions based on value
    private String groupLimit;
    //Iframe related config elements
    private String sessionId;
    private String hostUrl;
    private String reference;
    private String returnUrl;
    private String expiryUrl;
    private String pageSetId;
    private String isRenderingRequired;
    private String height;

    public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getIsRenderingRequired() {
		return isRenderingRequired;
	}

	public void setIsRenderingRequired(String isRenderingRequired) {
		this.isRenderingRequired = isRenderingRequired;
	}

	public String getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getHostUrl() {
		return hostUrl;
	}

	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getExpiryUrl() {
		return expiryUrl;
	}

	public void setExpiryUrl(String expiryUrl) {
		this.expiryUrl = expiryUrl;
	}

	public String getPageSetId() {
		return pageSetId;
	}

	public void setPageSetId(String pageSetId) {
		this.pageSetId = pageSetId;
	}

	public String getCompareValidationRequired() {
	return compareValidationRequired;
    }

    public void setCompareValidationRequired(String compareValidationRequired) {
	this.compareValidationRequired = compareValidationRequired;
    }

    public String getCompareQuestionCode() {
	return compareQuestionCode;
    }

    public void setCompareQuestionCode(String compareQuestionCode) {
	this.compareQuestionCode = compareQuestionCode;
    }

    public String getGroupLimit() {
	return groupLimit;
    }

    public void setGroupLimit(String groupLimit) {
	this.groupLimit = groupLimit;
    }

    public String getCompareOperator() {
	return compareOperator;
    }

    public void setCompareOperator(String compareOperator) {
	this.compareOperator = compareOperator;
    }

    public String getIsJSEnabled() {
	return isJSEnabled;
    }

    public void setIsJSEnabled(String isJSEnabled) {
	this.isJSEnabled = isJSEnabled;
    }

    public String getDateDependency() {
	return dateDependency;
    }

    public void setDateDependency(String dateDependency) {
	this.dateDependency = dateDependency;
    }

    public String getIsCheckRequired() {
	return isCheckRequired;
    }

    public void setIsCheckRequired(String isCheckRequired) {
	this.isCheckRequired = isCheckRequired;
    }

    public String getShowOptionalText() {
	return showOptionalText;
    }

    public void setShowOptionalText(String showOptionalText) {
	this.showOptionalText = showOptionalText;
    }

    public String getDefaultNavigator() {
	return defaultNavigator;
    }

    public void setDefaultNavigator(String defaultNavigator) {
	this.defaultNavigator = defaultNavigator;
    }

    public Map<String, String> getSessionMap() {
	return sessionMap;
    }

    public void setSessionMap(Map<String, String> sessionMap)  {
	this.sessionMap = sessionMap;
    }

    public String getTMLookup() {
	return tMLookup;
    }

    public void setTMLookup(String lookup) {
	tMLookup = lookup;
    }

    public String getQuestionTypeSpecificStyle() {
	return questionTypeSpecificStyle;
    }

    public void setQuestionTypeSpecificStyle(String questionTypeSpecificStyle) {
	this.questionTypeSpecificStyle = questionTypeSpecificStyle;
    }

    public List<String> getAddressLine() {
	if (addressLine == null) {
	    addressLine = new ArrayList<String>();
	}
	return addressLine;
    }

    public void setAddressLine(List<String> addressLine) {
	this.addressLine = addressLine;
    }

    public String getLabelForId() {
	return labelForId;
    }

    public void setLabelForId(String labelForId) {
	this.labelForId = labelForId;
    }

    public String getFormName() {
	return formName;
    }

    public void setFormName(String formName) {
	this.formName = formName;
    }

    public HashMap<String, String> getAdditionalValueMap() {
	if (additionalValueMap == null) {
	    additionalValueMap = new HashMap<String, String>();
	}
	return additionalValueMap;
    }

    public void setAdditionalValueMap(HashMap<String, String> additionalValueMap){
	this.additionalValueMap = additionalValueMap;
    }

    public HashMap<String, String> getEndorsementMap() {
	if (endorsementMap == null) {
	    endorsementMap = new HashMap<String, String>();
	}
	return endorsementMap;
    }

    public void setEndorsementMap(HashMap<String, String> endorsementMap) {
	this.endorsementMap = endorsementMap;
    }

    public String getAdditionalValue() {
	return additionalValue;
    }

    public void setAdditionalValue(String additionalValue) {
	this.additionalValue = additionalValue;
    }

    public String getIsValidationRequired() {
	return isValidationRequired;
    }

    public void setIsValidationRequired(String isValidationRequired) {
	this.isValidationRequired = isValidationRequired;
    }

    public String getAllowPastDate() {
	return allowPastDate;
    }

    public void setAllowPastDate(String allowPastDate) {
	this.allowPastDate = allowPastDate;
    }

    public String getAllowFutureDate() {
	return allowFutureDate;
    }

    public void setAllowFutureDate(String allowFutureDate) {
	this.allowFutureDate = allowFutureDate;
    }

    public String getAllowAnyDate()  {
	return allowAnyDate;
    }

    public void setAllowAnyDate(String allowAnyDate) {
	this.allowAnyDate = allowAnyDate;
    }

    public String getDaysAllowedForward() {
	return daysAllowedForward;
    }

    public void setDaysAllowedForward(String daysAllowedForward) {
	this.daysAllowedForward = daysAllowedForward;
    }

    public String getDaysAllowedBackward() {
	return daysAllowedBackward;
    }

    public void setDaysAllowedBackward(String daysAllowedBackward) {
	this.daysAllowedBackward = daysAllowedBackward;
    }

    public String getEditLink() {
	return editLink;
    }

    public void setEditLink(String editLink) {
	this.editLink = editLink;
    }

    public String getShowBlank() {
	return showBlank;
    }

    public void setShowBlank(String showBlank) {
	this.showBlank = showBlank;
    }

    public String getValidBlank() {
	return validBlank;
    }

    public void setValidBlank(String validBlank) {
	this.validBlank = validBlank;
    }

    public String getCurrency() {
	return currency;
    }

    public void setCurrency(String currency) {
	this.currency = currency;
    }

    public String getDecimalPlaces()  {
	return decimalPlaces;
    }

    public void setDecimalPlaces(String decimalPlaces) {
	this.decimalPlaces = decimalPlaces;
    }

    public String getThousandSeperator()  {
	return thousandSeperator;
    }

    public void setThousandSeperator(String thousandSeperator) {
	this.thousandSeperator = thousandSeperator;
    }

    public String getMaxLength() {
	return maxLength;
    }

    public void setMaxLength(String maxLength) {
	this.maxLength = maxLength;
    }

    public String getAction() {
	return action;
    }

    public void setAction(String action) {
	this.action = action;
    }

    public String getShowSize() {
	return showSize;
    }

    public void setShowSize(String showSize) {
	this.showSize = showSize;
    }

    public String getUseFilename() {
	return useFilename;
    }

    public void setUseFilename(String useFilename) {
	this.useFilename = useFilename;
    }

    public String getFileType() {
	return fileType;
    }

    public void setFileType(String fileType) {
	this.fileType = fileType;
    }

    public String getLimit() {
	return limit;
    }

    public void setLimit(String limit) {
	this.limit = limit;
    }

    public String getOptionalText() {
	return optionalText;
    }

    public void setOptionalText(String optionalText) {
	this.optionalText = optionalText;
    }

    public String getAssemblyMethod() {
	return assemblyMethod;
    }

    public void setAssemblyMethod(String assemblyMethod) {
	this.assemblyMethod = assemblyMethod;
    }

    public String getAddressLine1() {
	return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
	this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
	return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
	this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
	return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
	this.addressLine3 = addressLine3;
    }

    public String getAddressLine4() {
	return addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
	this.addressLine4 = addressLine4;
    }

    public String getAddressLine5() {
	return addressLine5;
    }

    public void setAddressLine5(String addressLine5) {
	this.addressLine5 = addressLine5;
    }

    public String getColumns() {
	return columns;
    }

    public void setColumns(String columns) {
	this.columns = columns;
    }

    public String getRows() {
	return rows;
    }

    public void setRows(String rows) {
	this.rows = rows;
    }

    public String getDateFormat() {
	return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
	this.dateFormat = dateFormat;
    }

    public String getCurrentPage() {
	return currentPage;
    }

    public void setCurrentPage(String currentPage) {
	this.currentPage = currentPage;
    }

    public String getErrorStyle() {
	return errorStyle;
    }

    public void setErrorStyle(String errorStyle) {
	this.errorStyle = errorStyle;
    }

    public String getFormAction() {
	return formAction;
    }

    public void setFormAction(String formAction) {
	this.formAction = formAction;
    }

    public String getYearRangeStart() {
	return yearRangeStart;
    }

    public void setYearRangeStart(String yearRangeStart) {
	this.yearRangeStart = yearRangeStart;
    }

    public String getYearRangeEnd() {
	return yearRangeEnd;
    }

    public void setYearRangeEnd(String yearRangeEnd) {
	this.yearRangeEnd = yearRangeEnd;
    }

    public String getNonJavascriptText() {
	return nonJavascriptText;
    }

    public void setNonJavascriptText(String nonJavascriptText) {
	this.nonJavascriptText = nonJavascriptText;
    }

    public String getHelpTitle() {
	return helpTitle;
    }

    public void setHelpTitle(String helpTitle) {
	this.helpTitle = helpTitle;
    }

    public String getAdditionalText1() {
	return additionalText1;
    }

    public void setAdditionalText1(String additionalText1) {
	this.additionalText1 = additionalText1;
    }

    public String getAdditionalText2() {
	return additionalText2;
    }

    public void setAdditionalText2(String additionalText2) {
	this.additionalText2 = additionalText2;
    }

    public String getEditTitle() {
	return editTitle;
    }

    public void setEditTitle(String editTitle)  {
	this.editTitle = editTitle;
    }

    public String getEditText() {
	return editText;
    }

    public void setEditText(String editText) {
	this.editText = editText;
    }

    public String getPageErrorText() {
	return pageErrorText;
    }

    public void setPageErrorText(String pageErrorText) {
	this.pageErrorText = pageErrorText;
    }

    public String getPageIntroText() {
	return pageIntroText;
    }

    public void setPageIntroText(String pageIntroText) {
	this.pageIntroText = pageIntroText;
    }

    public String getErrorText() {
	return errorText;
    }

    public void setErrorText(String errorText) {
	this.errorText = errorText;
    }

    public String getHelpText() {
	return helpText;
    }

    public void setHelpText(String helpText) {
	this.helpText = helpText;
    }

    public String getLabel() {
	return label;
    }

    public void setLabel(String label) {
	this.label = label;
    }

    public String getSubType() {
	return subType;
    }

    public void setSubType(String subType) {
	this.subType = subType;
    }

    public String getStyleHint() {
	return styleHint;
    }

    public void setStyleHint(String styleHint) {
	this.styleHint = styleHint;
    }

    public HsxSBWidgetInfo() {

    }

    public String getDefaultValue() {
	return defaultValue;
    }

    public HashMap<String, String> getConfigValueMap() {
	return configValueMap;
    }

    public void setConfigValueMap(HashMap<String, String> configValueMap) {
	this.configValueMap = configValueMap;
    }

    public String getDisplayText() {
	return displayText;
    }

    public String getId() {
	return id;
    }

    public String getAllowMaxValue() {
	return allowMaxValue;
    }

    public String getAllowMinValue() {
	return allowMinValue;
    }

    public String getName() {
	return name;
    }

    public int getOrder() {
	return order;
    }

    public List<HsxSBPermitedValues> getPermitedVlaues() {
	return permitedVlaues;
    }

    public String getPolicyMap() {
	return policyMap;
    }

    public String getSavedValue() {
	return savedValue;
    }

    public String getStyle() {
	return style;
    }

    public String getType() {
	return type;
    }

    public String getValidationHint() {
	return validationHint;
    }

    public boolean isErrorIndicator() {
	return errorIndicator;
    }

    public String isMandatory() {
	return isMandatory;
    }

    /**
     * @param defaultValue
     */
    public void setDefaultValue(String defaultValue) {
	this.defaultValue = defaultValue;
    }

    /**
     *
     * @param displayText
     */
    public void setDisplayText(String displayText) {
	this.displayText = displayText;
    }

    /**
     *
     * @param errorIndicator
     */
    public void setErrorIndicator(boolean errorIndicator) {
	this.errorIndicator = errorIndicator;
    }

    /**
     *
     * @param code
     */
    public void setId(String code) {
	this.id = code;
    }

    /**
     *
     * @param isMandatory
     */
    public void setMandatory(String isMandatory) {
	this.isMandatory = isMandatory;
    }

    /**
     *
     * @param allowMaxValue
     */
    public void setAllowMaxValue(String allowMaxValue) {
	this.allowMaxValue = allowMaxValue;
    }

    /**
     *
     * @param allowMinValue
     */

    public void setAllowMinValue(String allowMinValue) {
	this.allowMinValue = allowMinValue;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     *
     * @param order
     */
    public void setOrder(int order) {
	this.order = order;
    }

    /**
     *
     * @param permitedVlaues
     */
    public void setPermitedVlaues(List<HsxSBPermitedValues> permitedVlaues) {
	this.permitedVlaues = permitedVlaues;
    }

    public String getDisplayOnly() {
	return displayOnly;
    }

    public void setDisplayOnly(String displayOnly) {
	this.displayOnly = displayOnly;
    }

    /**
     *
     * @param policyMap
     */
    public void setPolicyMap(String policyMap) {
	this.policyMap = policyMap;
    }

    /**
     *
     * @param savedValue
     */
    public void setSavedValue(String savedValue) {
	this.savedValue = savedValue;
    }

    /**
     *
     * @param style
     */
    public void setStyle(String style) {
	this.style = style;
    }

    /**
     *
     * @param type
     */
    public void setType(String type) {
	this.type = type;
    }

    /**
     *
     * @param validationHint
     */
    public void setValidationHint(String validationHint) {
	this.validationHint = validationHint;
    }

    public String getTabIndex() {
	return tabIndex;
    }

    public void setTabIndex(String tabIndex) {
	this.tabIndex = tabIndex;
    }

    public String getTarget() {
	return target;
    }

    public void setTarget(String target) {
	this.target = target;
    }

    public List<HsxSBIPEQuestionReference> getIpeQuestionReferenceList() {
	return ipeQuestionReferenceList;
    }

    public void setIpeQuestionReferenceList(
	    List<HsxSBIPEQuestionReference> ipeQuestionReferenceList) {
	this.ipeQuestionReferenceList = ipeQuestionReferenceList;
    }

    public String getAdditionalText3() {
	return additionalText3;
    }

    public void setAdditionalText3(String additionalText3) {
	this.additionalText3 = additionalText3;
    }

    public String getAdditionalText4() {
	return additionalText4;
    }

    public void setAdditionalText4(String additionalText4) {
	this.additionalText4 = additionalText4;
    }

    public String getAdditionalText5() {
	return additionalText5;
    }

    public void setAdditionalText5(String additionalText5) {
	this.additionalText5 = additionalText5;
    }

    public String getAdditionalText6() {
	return additionalText6;
    }

    public void setAdditionalText6(String additionalText6) {
	this.additionalText6 = additionalText6;
    }

//    public String getAdditionalText7()
//    {
//	return additionalText7;
//    }
//
//    public void setAdditionalText7(String additionalText7)
//    {
//	this.additionalText7 = additionalText7;
//    }
//
//    public String getAdditionalText8()
//    {
//	return additionalText8;
//    }
//
//    public void setAdditionalText8(String additionalText8)
//    {
//	this.additionalText8 = additionalText8;
//    }
//
//    public String getAdditionalText9()
//    {
//	return additionalText9;
//    }
//
//    public void setAdditionalText9(String additionalText9)
//    {
//	this.additionalText9 = additionalText9;
//    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getIsProgrssbarRequired() {
	return isProgrssbarRequired;
    }

    public void setIsProgrssbarRequired(String isProgrssbarRequired) {
	this.isProgrssbarRequired = isProgrssbarRequired;
    }

    public void setGroupValidation(String groupValidation) {
	this.groupValidation = groupValidation;
    }

    public String getGroupValidation() {
	return groupValidation;
    }

}
