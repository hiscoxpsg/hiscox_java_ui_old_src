package com.hiscox.sb.framework.core.renderer.htmlrenderers.composite;

import java.util.Iterator;

import com.hiscox.sb.framework.core.HsxSBRenderer;
import com.hiscox.sb.framework.core.HsxSBWidget;
import com.hiscox.sb.framework.core.renderer.htmlrenderers.HsxSBHTMLCompositeRenderer;
import com.hiscox.sb.framework.core.util.HsxSBHTMLRendererFactory;
import com.hiscox.sb.framework.core.widgets.composite.HsxSBScreenWidget;
import com.hiscox.sb.framework.exception.HsxSBUIBuilderRuntimeException;
import com.hiscox.sb.framework.util.HsxSBUIBuilderConstants;
import com.hiscox.sb.framework.util.HsxSBUtil;
import com.hiscox.sb.framework.util.HsxSBWidgetUtil;

/**
 * This class renders the HTML tags to display questions .
 * @author Cognizant
 * @version 1.0
// * @created 25-Mar-2010 6:27:29 AM
 */
public class HsxSBScreenRenderer extends HsxSBHTMLCompositeRenderer
	implements
	    HsxSBUIBuilderConstants {

    /**
     * This method returns the String for rendering the HTML tags to display the
     * screen questions. This method in turn calls the form renderer which calls
     * the question group renderer, which calls the question renderer.
     * @param widget contains widget data
     * @return content
     * @throws HsxSBUIBuilderRuntimeException which throws runtime exception
     */
    @Override
    public String renderContent(final HsxSBWidget widget)
	    throws HsxSBUIBuilderRuntimeException {
	StringBuilder contentBuilder = new StringBuilder();

	HsxSBScreenWidget screenWidget = (HsxSBScreenWidget) widget;
	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_DIV);

	if (null != screenWidget.getWidgetInfo()
		&& null != screenWidget.getWidgetInfo().getClass()
		&& screenWidget.getWidgetInfo().getClass().toString().trim()
			.length() > 0) {
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SCREEN_STYLE);
	    if (HsxSBUtil.isNotBlank(screenWidget.getWidgetInfo()
		    .getStyleHint())) {
		contentBuilder.append(HTML_SINGLESPACE);
		contentBuilder.append(screenWidget.getWidgetInfo()
			.getStyleHint());
	    }

	    contentBuilder.append(HTML_SINGLE_QUOTE);
	}

	if (null != screenWidget.getWidgetInfo()
		&& null != screenWidget.getWidgetInfo().getId()
		&& screenWidget.getWidgetInfo().getId().trim().length() > 0) {
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_ID);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(screenWidget.getWidgetInfo().getId());
	    contentBuilder.append(SCREEN_ID_SUFFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	}
	contentBuilder.append(HTML_END_TAG);

	boolean errorIndicator = screenWidget.getWidgetInfo()
		.isErrorIndicator();
	String errorStyle = screenWidget.getWidgetInfo().getErrorStyle();
	if (HsxSBUtil.isNotBlank(errorStyle)) {
	    errorStyle = HsxSBWidgetUtil.removehideUnHideStyles(errorStyle);
	    if (!errorIndicator) {
		errorStyle = errorStyle.trim() + STR_SINGLE_SPACE
			+ CSS_HIDE_CLASS;
	    }
	}
	if (errorIndicator) {
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(errorStyle);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_ID);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(screenWidget.getWidgetInfo().getId());
	    contentBuilder.append(SCREEN_ERROR_ID_SUFFIX);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_ERROR_STYLE);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);
	    // Error text from the Text manager
	    if (null != screenWidget.getWidgetInfo().getPageErrorText()) {
		contentBuilder.append(screenWidget.getWidgetInfo()
			.getPageErrorText());
	    }
	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_END_TAG);

	}

	// to display the screen level text
	if (HsxSBUtil.isNotBlank(screenWidget.getWidgetInfo()
		.getPageIntroText())) {

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_SINGLESPACE);
	    contentBuilder.append(HTML_CLASS);
	    contentBuilder.append(HTML_EQUALS);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_SCREEN_DIV_STYLE);
	    contentBuilder.append(HTML_SINGLE_QUOTE);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);

	    // page into text from the Text manager
	    contentBuilder.append(screenWidget.getWidgetInfo()
		    .getPageIntroText());

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_SPAN);
	    contentBuilder.append(HTML_END_TAG);

	    contentBuilder.append(HTML_START_TAG);
	    contentBuilder.append(HTML_FRONT_SLASH);
	    contentBuilder.append(HTML_DIV);
	    contentBuilder.append(HTML_END_TAG);

	}
	if (null != screenWidget.getWidgetsList()) {
	    Iterator<HsxSBWidget> widgetsListIterator = screenWidget
		    .getWidgetsList().iterator();
	    while (widgetsListIterator.hasNext()) {
		HsxSBWidget innerWidget = widgetsListIterator.next();
		HsxSBRenderer renderer = HsxSBHTMLRendererFactory
			.getRendererInstance(innerWidget.getWidgetInfo()
				.getType());
		if (renderer != null) {
		    contentBuilder.append(renderer.renderContent(innerWidget));
		}

	    }
	}

	contentBuilder.append(HTML_START_TAG);
	contentBuilder.append(HTML_FRONT_SLASH);
	contentBuilder.append(HTML_DIV);
	contentBuilder.append(HTML_END_TAG);

	return contentBuilder.toString();
    }
}
