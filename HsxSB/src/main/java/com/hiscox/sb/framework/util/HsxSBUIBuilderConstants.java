package com.hiscox.sb.framework.util;

/**
 * This is utility class for the whole Dynamic framework which carries the
 * constant values used across the Dynamic framework.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:41 AM
 */
public interface HsxSBUIBuilderConstants {

	int HTML_CONTENT_TYPE = 1;
	int JSF_CONTENT_TYPE = 2;
	String XML_ACTION = "Action";
	String XML_ACTION_LIST = "ActionList";
	String XML_TAG_LIST = "TagList";
	String XML_CONFIG = "Config";
	String XML_IPE_QUESTION_REFERENCE = "IPEQuestionReference";
	String XML_IS_MANDATORY = "isMandatory";
	String XML_ODE_SCREEN_RP = "ODEScreenRp";
	String XML_PERMITTED_VALUE = "PermittedValue";
	String XML_QUESTION_GROUP = "QuestionGroup";
	String XML_FORM = "Form";
	String XML_CODE = "code";
	String XML_ORDER = "order";
	String XML_QUESTION_ID = "id";
	String XML_QUESTION_PANEL = "QuestionPanel";
	String XML_QUESTION_TYPE = "type";
	String XML_QUESTION_SUBTYPE = "subtype";
	String XML_QUESTION_VALUE = "value";
	String XML_DATA_AREA = "DataArea";
	String XML_SCREEN_QUESTION = "ScreenQuestion";
	String XML_VALIDATION_HINT = "validationHint";
	String XML_XPATH = "xpath";
	String XML_REQUESTED_ACTION = "requestedAction";
	String XML_U1 = "u1";
	String XML_U2 = "u2";
	String XML_U3 = "u3";
	String XML_U4 = "u4";
	String XML_U5 = "u5";
	String HTML_START_TAG = "<";
	String HTML_END_TAG = ">";
	String HTML_CURRENCY = "currency";
	String HTML_NUMERIC = "numeric";
	String HTML_PERCENTAGE = "percentage";
	String HTML_CLOSE_END_TAG = "/>";
	String HTML_FRONT_SLASH = "/";
	String HTML_BACKSPACE = "\t";
	String HTML_SINGLESPACE = " ";
	String HTML_NEWLINE = "\n";
	String HTML_EQUALS = "=";
	String HTML_TYPE = "type";
	String HTML_ID = "id";
	String HTML_FOR = "for";
	String HTML_NAME = "name";
	String HTML_VALUE = "value";
	String HTML_STYLE = "style";
	String HTML_CLASS = "class";
	String HTML_SUMMARY = "summary";
	String HTML_CAPTION = "caption";
	String HTML_CELLSPACING = "cellspacing";
	String HTML_CELLPADDING = "cellpadding";
	String HTML_TITLE = "title";
	String HTML_NOSCRIPT = "noscript";
	String HTML_H3 = "h3";
	String HTML_DIV = "div";
	String HTML_OL = "ol";
	String HTML_FIELD_SET = "fieldset";
	String HTML_LI = "li";
	String HTML_UL = "ul";
	String HTML_QUESTION = "question";
	String HTML_ACTION_GROUP = "actiongroup";
	String HTML_SCREEN = "screen";
	String HTML_ACTION = "action";
	String HTML_ACTIONLIST = "actionlist";
	String HTML_SPAN = "span";
	String HTML_INS = "ins";
	String HTML_TABLE = "table";
	String HTML_TD = "td";
	String HTML_TR = "tr";
	String HTML_TH = "th";
	String HTML_TABLECOL = "tablecollabelentry";
	String HTML_TABLERADIOENTRY = "tableradioentry";
	String HTML_TABLEROW = "tablerow";
	String HTML_TABLEHEADER = "TableHeaderLabelEntry";
	String HTML_INPUT = "input";
	String HTML_LABEL = "label";
	String HTML_LEGEND = "legend";
	String HTML_FORM = "form";
	String HTML_METHOD = "method";
	String HTML_COMMAND_NAME = "commandname";
	String HTML_ANCHOR = "a";
	String HTML_HREF = "href";
	String HTML_TARGET = "target";
	String HTML_TABINDEX = "tabindex";
	String HTML_TEXT = "text";
	String HTML_TEXTAREA = "textarea";
	String HTML_PASSWORD = "password";
	String HTML_HIDDEN = "hidden";
	String HTML_BUTTON = "button";
	String HTML_SUBMIT = "submit";
	String HTML_SELECT = "select";
	String HTML_OPTION = "option";
	String HTML_OPTIONAL = "optional";
	String HTML_SELECTED = "selected";
	String HTML_CHECKED = "checked";
	String HTML_RADIO = "radio";
	String HTML_CHECKBOX = "checkbox";
	String HTML_QUESTION_ROW = "question_row";
	String HTML_DISABLED = "disabled";
	String HTML_ROWS = "rows";
	String HTML_COLS = "cols";
	String HTML_ANCHORHELP = "help";
	String HTML_MAXLENGTH = "maxlength";
	String GREATER_THAN = ">";
	String LESS_THAN = "<";
	String CLOSE_BRACKET = "]";
	String OPEN_BRACKET = "[";
	String DOUBLE_QUOTES = "\"";
	String BACK_SLASH = "\\";
	String COLON = ":";
	String SEMI_COLON = ";";
	String OPEN_BRACE = "(";
	String CLOSE_BRACE = ")";
	String AMPERSAND = "&";

	String HTML_IMAGE = "img";
	String HTML_SOURCE = "src";
	String HTML_ALT = "alt";
	String HTML_BORDER = "border";
	String HTML_READONLY = "readonly";
	String HTML_RETRIEVEQUOTE_ACTION = "RetrieveQuoteSecurityCheck";
	String HTML_YES = "yes";
	String HTML_PERCENTAGE_SYMBOL = "%";
	String HTML_DATE_FORMAT = "dd/MM/yyyy";
	String HTML_MONTH_YEAR_FORMAT = "MMMM, yyyy";
	String HTML_DATE_MONTH_YEAR = "MonthYear";
	String HTML_TRUE = "true";
	boolean HTML_BOOLEAN_TRUE = true;
	String HTML_SINGLE_QUOTE = "'";
	String HTML_DOUBLE_QUOTE = "\"";
	String HTML_SINGLE_QUOTES = "''";
	String HTML_HASH = "#";
	String HTML_IMAGE_SOURCE_URL = "img-source-url";
	String HTML_CALENDAR = "calendar";
	String HTML_CAL_HOLDER = "cal-holder";
	String HTML_READONLY_ESCAPED = "readonly";
	String HTML_BACKSLASH = "\"";
	String HTML_HYPHEN = "-";
	String HTML_UNDERSCORE = "_";
	String HTML_DOT = ".";
	String HTML_POST = "'post'";
	String HTML_BREAK = "<br/>";
	String HTML_PIPE_SYMBOL = "|";
	String STR_OR_SYMBOL = "||";
	String STR_DOUBLE_AMPERSAND_SYMBOL = "&&";
	String HTML_DATE_HREF_VALUE = "javascript:void(0)";
	String HTML_QUESTION_MARK_SYMBOL = "?";
	String HTML_HPS_SESSIONID = "HPS_SessionID";
	String HTML_HEIGHT = "height";
	String HTML_WIDTH = "width";
	String HTML_ALLOWTRANSPARENCY = "allowtransparency";
	String HTML_FRAMEBRODER = "frameborder";
	String HTML_IFRAMECSS = "pymntIframe";

	// tridirect tag constant
	String HTML_WIDTH_HEIGHT_FRAMEBORDER = " width=\"1\" height=\"1\" frameborder=\"0\"";
	String HTML_FRAME_SRC_URL = "https://fls.doubleclick.net/activityi;src=2723417;";
	String HTML_FRAME_SRC_URL_SECURE = "https://fls.doubleclick.net/activityi;src=2723417;";
	String HTML_JAVASCRIPT_START = "<script type=\"text/javascript\">var axel = Math.random() + \"\";var a = axel * 10000000000000;document.write('";
	String HTML_FRAME_SRC_URL_TYPE = "type=";
	String HTML_FRAME_SRC_URL_CATEGORY = ";cat=";
	String HTML_FRAME_SRC_URL_ORD_NUM = ";ord=1;num=' + a + '?\"";
	String HTML_FRAME_SRC_URL_ORD_NUM1 = "ord=1;num=' + a + '?\"";
	String HTML_FRAME_SRC_URL_ORD_NUM_2 = ";ord=1;num=1?\"";
	String HTML_FRAME_SRC_URL_ORD_NUM_3 = "ord=1;num=1?\" ";
	String HTML_IFRAME_START = "<iframe src=\"";
	String HTML_IFRAME_END = "></iframe>');";
	String HTML_IFRAME_END_2 = "></iframe>";
	String HTML_JAVASCRIPT_END = "</script>";
	String HTML_JAVA_NOSCRIPT_START = "<noscript>";
	String HTML_JAVA_NOSCRIPT_END = "</noscript>";
	String JS_PRINT = " onclick=window.print() ";
	String JS_BUTTON_DISABLE = " onclick= \"checkFormSubmitt('";

	// screen names and codes
	int USA_HOME_PAGE = 1;
	int BUSINESS_INSURANCE_HOME_PAGE = 2;
	int PROFESSIONAL_LIABILITY_E_AND_O_PAGE = 3;
	int BUSINESS_OWNERS_INSURANCE_PAGE = 4;
	int GENERAL_LIABILITY_PAGE = 5;
	int WORKERS_COMPENSATION_PAGE = 6;
	int IT_CONSULTING_PAGE = 7;
	int BUSINESS_CONSULTING_PAGE = 8;
	int MARKETING_CONSULTING_PAGE = 9;
	int OTHER_PROFESSIONS_PAGE = 10;
	int PRODUCT_SELECTION_I_KNOW_WHAT_I_WANT_PAGE = 11;
	int TELL_US_ABOUT_YOUR_BUSINESS_PAGE = 12;
	int HELP_YOU_CHOOSE_PRODUCT_OPTIONS_PAGE = 13;
	int ABOUT_YOU_REGISTRATION_1_PAGE = 14;
	int YOUR_BUSINESS_REGISTRATION_2_PAGE = 15;
	int YOUR_BUSINESS_REGISTRATION_3_PAGE = 16;
	int APPLICATION_DECLINED_REFFERED_PAGE = 17;
	int YOUR_QUOTE_REGISTRATION_4_PAGE = 18;
	int IMPORTANT_STATEMENTS_PAGE = 19;
	int APPLICATION_SUMMARY_PAGE = 20;
	int PAYMENT_DETAILS_PAGE = 21;
	int CONFIRMATION_PAGE = 22;
	int PAY_BY_PHONE_PAGE = 23;
	int SAVE_QUOTE_PAGE = 24;
	int CALL_BACK_REQUEST_PAGE = 25;
	int PRODUCT_SELECTION_HELP_ME_CHOOSE = 26;

	String JSP_PERCENT = "%";
	String JSP_DIRECTIVE = "%@";
	String JSP_INCLUDE = "include";
	String JSP_FILE = "file";

	String ERROR_QUESTION_ID_SUFFIX = "error_question_id";
	String QUESTION_ID_SUFFIX = "_question_id";
	String EDIT_ID_SUFFIX = "_edit_id";
	String EDIT_NAME_SUFFIX = "_edit_name";
	String ADDITIONAL1_ID_SUFFIX = "_additional1_id";
	String ADDITIONAL1_NAME_SUFFIX = "_additional1_name";
	String ADDITIONAL2_ID_SUFFIX = "_additional2_id";
	String ADDITIONAL2_NAME_SUFFIX = "_additional2_name";
	String HTML_ANCHOREDIT = "Edit";

	String DROPDOWN_ID_SUFFIX = "_dropdown_id";
	String CHECKBOX_ID_SUFFIX = "_checkbox_id";
	String TEXTBOX_ID_SUFFIX = "_text_id";
	String SUBGROUP_ID_SUFFIX = "_subgroup_id";
	String RADIO_BUTTON_SUFFIX = "_radio_";
	String CHECKBOX_BUTTON_SUFFIX = "_checkbox_";
	String LABEL_ID_SUFFIX = "_label_Id";
	String SPAN_ID_SUFFIX = "_span_id";
	String HIDDEN_PREFIX = "_hidden";
	String EMAIL_ADDRESS = "EmailAddress";
	String PASSWORD = "Password";
	String ACEGI_USERNAME = "j_username";
	String ACEGI_PASSWORD = "j_password";
	String TELEPHONE_NUMBER = "telephonenumber";
	String HELP_ID_SUFFIX = "_help_id";
	String VALUE_ID_SUFFIX = "_value_id";
	String ANCHOR_ID_SUFFIX = "_anchor_id";
	String HELP_NAME_SUFFIX = "_help_name";
	String TELEPHONE_ID_SUFFIX = "_textbox_id";
	String ID_SUFFIX = "_id";
	String BUTTON_ID_SUFFIX = "_button_id";
	String SCREEN_ID_SUFFIX = "_screen_id";
	String SCREEN_ERROR_ID_SUFFIX = "_screen_error_id";
	String GROUP_ID_SUFFIX = "_group_id";
	String FORM_ID_SUFFIX = "_form_id";
	String STR_DEFAULT = "default";
	String EXISTING_PASSWORD = "Password_Existing";
	String EXISTING_EMAIL_ADDRESS = "EmailAddress_Existing";
	String CSS_TOOL_TIP_ACHOR_TAG_CLASS = "showTip L3 HelpHref";
	// String Constants
	String STR_EMPTY = "";
	String STR_PRINT = "print";
	String STR_SINGLE_SPACE = " ";
	String STR_HIDE = "hide";
	String STR_SHOW = "show";
	String STR_COMPARISON = "comparison";
	String STR_QUESTION_ID = "questionId";
	String STR_CONFIRM = "confirm";
	String STR_OPTIONAL = "optional";
	String STR_FIRST_NAME = "applicantfirstname";
	String STR_LAST_NAME = "applicantlastname";

	String STR_DECIMAL_PATTERN = "##,###.";
	String STR_INTEGER_PATTERN = "##,###";
	String STR_ZERO_PATTERN = "0";

	// Hard coded Widget / Renderer Types
	String TYPE_ACTIONLIST = "ActionList";
	String TYPE_ACTION_HREF = "PopUpButton";
	String TYPE_HIDDEN = "hidden";
	String TYPE_TABLE = "table";
	String TYPE_ERROR = "error";
	String HTML_QUESTION_GROUP = "question_group";
	String TYPE_QUESTION_GROUP = "question_group";
	String TYPE_ANCHOR = "anchor";
	String TYPE_EDIT = "edit";
	String TYPE_TEXT = "text";
	String TYPE_DROPDOWN = "dropdown";
	String TYPE_STATIC = "static";
	String TYPE_CHECKBOX = "checkbox";
	String TYPE_RADIO = "radio";
	String TYPE_SCREEN = "screen";
	String TYPE_FORM = "form";
	String TYPE_ADDITIONALTEXT = "additionaltext";
	String TYPE_DATE = "date";
	String TYPE_LABEL = "label";
	String TYPE_OPTION = "option";
	String TYPE_ACTION = "action";
	String TYPE_SPAN = "span";
	String TYPE_LABEL_SPAN = "labelspan";
	String TYPE_TABLE_SPAN = "TableSpan";
	String TYPE_DIV = "div";
	String TYPE_QUESTION = "question";
	String TYPE_TEXTAREA = "textarea";
	String TYPE_TELEPHONE = "telephone";
	String TYPE_HELP = "help";
	String TYPE_TABLEROW = "tablerow";
	String TYPE_TABLEHEADER = "TABLEHEADERLABELENTRY";
	String TYPE_TABLECOLLABELENTRY = "TABLECOLLABELENTRY";
	String TYPE_TABLERADIOENTRY = "TABLERADIOENTRY";
	String TYPE_PASSWORD = "PASSWORD";
	String TYPE_ACTIONGROUP = "ACTIONGROUP";
	String TYPE_FILELINK = "FILELINK";
	String TYPE_SUBMIT = "SUBMIT";
	String TYPE_IMAGE = "IMAGE";
	String TYPE_LIMITSUMMARY = "LimitSummary";
	String TYPE_SURCHARGESUMMARY = "SurchargeSummary";
	String TYPE_ADDRESSSUMMARY = "AddressSummary";
	String TYPE_DEFAULT_NAVIGATOR = "DefaultNavigatorButton";
	String TYPE_IFRAME = "Iframe";

	// subtypes
	String SUBTYPE_CURRENCY = "Currency";
	String SUBTYPE_TWOCOLUMN = "TwoColumn";
	String SUBTYPE_REVERSE = "reverse";
	String SUBTYPE_TELEPHONE = "Telephone-US";
	String SUBTYPE_PASSWORD = "Password";
	String SUBTYPE_NARRATIVE = "Narrative";
	String SUBTYPE_EMAIL = "Email";
	String SUBTYPE_CREDIT_CARD = "CreditCard";
	String SUBTYPE_ZIPCODE5 = "ZIPcode5";
	String SUBTYPE_NUMERIC = "Numeric";
	String SUBTYPE_MONTHYEAR = "MonthYear";
	String SUBTYPE_PERCENTAGE = "Percentage";

	// String separators / tokens
	String STR_ASTERISK = "*";
	String STR_CURRENCY_KEY = "CurrencyText";
	String STR_UNDERSCORE = "_";
	String STR_HYPHEN = "-";
	String STR_FORWARD_SLASH = "/";
	String STR_DOT = ".";
	String STR_DOUBLE_DOT = "..";
	String STR_COMMA = ",";
	String STR_TILDE = "~";
	String STR_AT = "@";
	String STR_ESC_AT = "\"@";
	String STR_LOGICAL_AND = "&&";
	String SYMBOL_DUOBLE_LESSTHAN_START = "<<";
	String SYMBOL_DUOBLE_LESSTHAN_END = ">>";
	String SYMBOL_DUOBLE_HASH = "##";
	String SYMBOL_HASH = "#";
	String SYMBOL_DUOBLE_TILDE = "~~";
	String SYMBOL_CAPS = "^";
	String SYMBOL_ARROW = "->";

	// Time validation constants
	String TYPE_TIME = "time";
	String TIME_FORMAT = "hhmm";
	String TIME_STRING_EXCL = "Call me ASAP";
	String ENVIRONMENT = "Environment";
	String ENVIRONMENT_PROD = "prod";
	String ENVIRONMENT_NONPROD = "nonprod";

	String STR_REPLACE_ME = "replaceme";
	String STR_NAME = "name";
	String STR_VALUE = "value";
	String STR_RADIO = "Radio";
	String STR_CHECKBOX = "CheckBox";
	String STR_DROPDOWN = "Dropdown";
	String STR_LABEL = "Label";
	String STR_SMALL_LABEL = "label";
	String STR_LABEL_SPAN = "LabelSpan";
	String STR_YES = "Yes";
	String STR_NO = "No";
	String STR_ORDER = "order";
	String STR_EQUALS = "equals";
	String STR_BLANK = "_blank";
	String STR_DUMMY = "dummy";
	String STR_ZERO = "0";
	String STR_ONE = "1";
	String STR_TWO = "2";
	String STR_THREE = "3";
	String STR_FOUR = "4";
	String STR_ZERO_ONE = "01";
	String STR_ZERO_DOT = "0.";
	String STR_GREATER_THAN = "GREATER THAN";
	String STR_IS_ONE_OF = "IS ONE OF";

	String STR_DAY_SUFFIX = "_dropdown_day_name";
	String STR_MONTH_SUFFIX = "_dropdown_month_name";
	String STR_YEAR_SUFFIX = "_dropdown_year_name";

	// javascript event name strings
	String STR_ONCHANGE = "onChange";
	String STR_ONCLICK = "onClick";
	String STR_CHECKBOX_ONCLICK = "checkboxonclick";
	String STR_JS_HIDDEN_ACTION_NAMES_ID = "jsactionnameshiddenstringid";
	String STR_JS_HIDDEN_STRING_ID = "jsdependencyhiddenstringid";
	String STR_JS_TEXTBOX_HIDDEN_STRING_ID = "jstextboxhiddenstringid";
	String STR_JS_TOOLTIP_HIDDEN_STRING_ID = "jstooltiphiddenstringid";
	String STR_CALENDAR_JS_HIDDEN_STRING_ID = "jscalendarhiddenstringid";
	String STR_CONTEXT_PATH = "contextpath";
	String STR_JS_HIDE_UNHIDE_OR_CONDITION_STRING_ID = "jshideunhideorcondstringid";
	String STR_JS_HIDE_UNHIDE_AND_OR_CONDITION_STRING_ID = "jshideunhideandorcondstringid";
	String STR_RESOURCE_CONTEXT_PATH_VALUE = "resources";
	String STR_RESOURCE_DEV_CONTEXT_PATH_VALUE = "resources-usdirect";
	String STR_CALENDAR_IMAGE_PATH = "images/calendar-icon.gif";
	String STR_JS_BUTTON_ENABLE_DISABLE_STRING_ID = "jsbuttonenabledisablehiddenstring";
	String STR_JS_QUESTION_ENABLE_DISABLE_STRING_ID = "jsquestionenabledisablehiddenstring";
	String STR_JS_BUTTON_EN_DS_FLAG_ID = "jsbuttonendsflagid";
	String STR_JS_ISONEOF_AND_LOGIC_STRING_ID = "jsisoneofandlogicstringid";
	String STR_JS_DROPDOWN_AND_LOGIC_STRING_ID = "jsdropdownandlogicstringid";
	String STR_JS_AND_LOGIC_STRING_ID = "jsandlogicstringid";
	String STR_JS_ISONEOF_AND_VALIDATION_STRING_ID = "jsisoneofandvalidationhiddenid";
	String STR_JS_DEPENDENCY_VALIDATION_STRING_ID = "jsdependencyvalidationhiddenid";
	String STR_JS_TEXTBOX_PERCENT_STRING_ID = "jstextboxpercentagehiddenid";
	String STR_JS_TELEPHONE_AUTOTAB_HIDDEN_STRING_ID = "jstelephoneautotabhiddenstringid";
	String STR_JS_CHANGE_PAGESUBMIT_HIDDEN_STRING_ID = "jschangepagesubmithiddenstringid";
	String STR_JS_PS_QUESTION_VALUE_STRING_ID = "jspsquestionvaluehiddenstringid";
	String STR_JS_FILTER_BY_GROUPING_HIDDEN_STRING_ID = "jsfilterbygroupinghiddenstringid";
	// All possible Config Items
	String CONFIG_NAME_QID = "questionId";
	String CONFIG_SAVED_VALUE = "savedValue";
	String CONFIG_ERROR_INDICATOR = "errorIndicator";
	String CONFIG_DISPLAY_ONLY = "displayonly";
	String CONFIG_IS_MANDATORY = "isMandatory";
	String CONFIG_DEFAULT_VALUE = "defaultValue";
	String CONFIG_SUBTYPE = "subtype";
	String CONFIG_MAXIMUM = "allowMaxValue";
	String CONFIG_MINIMUM = "allowMinValue";
	String CONFIG_STYLE_HINT = "styleHint";
	String CONFIG_DISPLAY_TEXT = "DisplayText";
	String CONFIG_STYLE = "style";
	String CONFIG_FORM_ACTION = "formAction";
	String CONFIG_YEAR_RANGE_START = "yearRangeStart";
	String CONFIG_YEAR_RANGE_END = "yearRangeEnd";
	String CONFIG_DATE_FORMAT = "dateFormat";
	String CONFIG_ROWS = "rows";
	String CONFIG_COLUMNS = "columns";
	String CONFIG_ASSEMBLY_METHOD = "assemblyMethod";
	String CONFIG_ADDRESS_LINE_1 = "addressLine1";
	String CONFIG_ADDRESS_LINE_2 = "addressLine2";
	String CONFIG_ADDRESS_LINE_3 = "addressLine3";
	String CONFIG_ADDRESS_LINE_4 = "addressLine4";
	String CONFIG_ADDRESS_LINE_5 = "addressLine5";
	String CONFIG_ADDRESS_LINE = "addressField";
	String CONFIG_DEFAULT = "default";
	String CONFIG_LINEBREAK = "line-break";
	String CONFIG_LIMIT = "limit";
	String CONFIG_EDIT_LINK = "editLink";
	String CONFIG_SHOW_BLANK = "showBlank";
	String CONFIG_VALID_BLANK = "validBlank";
	String CONFIG_CURRENCY = "currency";
	String CONFIG_SHOW_SIZE = "showSize";
	String CONFIG_USE_FILE_NAME = "useFilename";
	String CONFIG_FILE_TYPE = "fileType";
	String CONFIG_DECIMAL_PLACES = "decimalPlaces";
	String CONFIG_THOUSAND_SEPERATOR = "thousandSeperator";
	String CONFIG_MAX_LENGTH = "maxLength";
	String CONFIG_TABLE_HEADER = "tableHeader";
	String CONFIG_ACTION = "action";
	String CONFIG_ALLOW_PAST_DATE = "allowPastDate";
	String CONFIG_ALLOW_FUTURE_DATE = "allowFutureDate";
	String CONFIG_ALLOW_ANY_DATE = "allowAnyDate";
	String CONFIG_DAYS_ALLOWED_FWD = "daysAllowedForward";
	String CONFIG_DAYS_ALLOWED_BWD = "daysAllowedBackward";
	String CONFIG_FORM_NAME = "formName";
	String CONFIG_ADDITIONAL_VALUE = "additionalValue";
	String CONFIG_ENDORSEMENT = "endorsement";
	String CONFIG_GROUP_ACTION = "validationTrigger";
	String CONFIG_DATEDEPEND = "dateDependency";
	String CONFIG_CONDITIONAL_FLAG = "conditionalFlag";
	String CONFIG_TYPE_SPECIFIC_STYLE = "questionTypeSpecificStyle";
	String CONFIG_TMLOOKUP = "TMLookup";
	String CONFIG_DEFAULT_NAVIGATOR = "defaultNavigator";
	String CONFIG_SHOW_OPTIONALTEXT = "showOptionalText";
	String CONFIG_ISCHECK_REQUIRED = "isCheckRequired";
	String CONFIG_COMPARE_VALIDATION_REQUIRED = "compareValidationRequired";
	String CONFIG_COMPARE_QUESTION_CODE = "compareQuestionCode";
	String CONFIG_COMPARE_OPERATOR = "compareOperator";
	String CONFIG_UPDATE_QUOTEID = "updateQuoteID";
	String CONFIG_GROUP_LIMIT = "groupLimit";
	String CONFIG_SESSION_ID = "sessionId";
	String CONFIG_HOST_URL = "hostUrl";
	String CONFIG_REFERENCE = "reference";
	String CONFIG_RETURN_URL = "returnUrl";
	String CONFIG_EXPIRY_URL = "expiryUrl";
	String CONFIG_PAGE_SETID = "pageSetId";
	String CONFIG_IS_RENDERING_REQUIRED = "isRenderingRequired";
	String CONFIG_HEIGHT = "height";

	String GROUPSTART_TAGS = "<div class=\"group-middle\">";

	String GROUPEND_TAGS = "</div>";

	// property files that are use in the screen builder
	String WIDGET_TYPE_PROPERTIES = "com.hiscox.sb.framework.properties.widgetsTypes";
	String VALIDATION_PROPERTIES = "com.hiscox.sb.framework.properties.validationHint";
	String OCCUPATION_VARIANT = "occupationVariant";
	String STATE_VARIANT = "stateVariant";

	// style classes
	String HTML_TEXT_STYLE = "input-type-text";
	String HTML_RADIO_STYLE = "input-type-radio";
	String HTML_CHECKBOX_STYLE = "input-type-checkbox";
	String HTML_TEXTAREA_STYLE = "input-type-textarea";
	String HTML_BUTTON_STYLE = "submit-input";
	String HTML_ERROR_STYLE = "error-text";
	String HTML_TEXT_DISABLED_STYLE = "text disabled";
	String HTML_LABEL_DIV_STYLE = "question-text";
	String HTML_CONTROL_DIV_STYLE = "control";
	String HTML_BUTTON_LI_STYLE = "question-container";
	String HTML_BUTTON_SPAN_STYLE = "submit-button-end";
	String HTML_BUTTONICON_SPAN_STYLE = "button-icon";
	String HTML_HELPICON_DIV_STYLE = "icon help";
	String HTML_EDITICON_DIV_STYLE = "icon edit";
	String HTML_CALENDARICON_DIV_STYLE = "icon calendar";
	String HTML_ADDITIONAL_TEXT_DIV_STYLE = "additional-text";
	String HTML_ADDITIONAL_TEXT_TWO_DIV_STYLE = "additional-text-2";
	String HTML_BUTTON_DIV_STYLE = "submit-button";
	String HTML_SCREEN_STYLE = "screen-container";
	String HTML_SCREEN_DIV_STYLE = "opening-message";
	String HTML_GROUP = "group ";
	String HTML_GROUP_STYLE = "group-container screen-reader-only";
	String HTML_BUTTON_SUBMIT_STYLE = "button-submit-input";
	String HTML_DROPDOWN_STYLE = "select-type-dropdown";
	String HTML_PASSWORD_STYLE = "input-type-password";
	String HTML_TELEPHONE_SUBTYPE_STYLE = "telephone-us";
	String HTML_TELEPHONE_STYLE = "input-type-telephone";
	String HTML_SAVEQUOTE_ACTION = "SaveQuoteSecurityCheck";
	String HTML_FIELDSET_RADIO_STYLE = "fs-type-radio";
	String HTML_SCREEN_READER_CSS_STYLE = "screen-reader-only";
	String HTML_INPUT_RADIO_STYLE = "input-type-radio";
	String HTML_INPUT_CHECKBOX_STYLE = "input-type-checkbox";
	String HTML_INPUT_STATIC_TWO_COLUMN_STYLE = "question-container static-two-column";
	String HTML_INPUT_STATIC_TWO_COLUMN_DISPLAYONLY_STYLE = "question-container static static-two-column display-only";
	String HTML_INPUT_STATIC_QUSTN_CNTR_STYLE = "question-container";
	String HTML_INPUT_STATIC_QUSTN_CNTR_CHQBX_STYLE = "question-container checkbox";
	String HTML_TR_GREY_STYLE = "grey-row";
	String HTML_TR_WHITE_STYLE = "white-row";
	String HTML_TABLE_STYLE = "quotes-table";

	// hard coded Style classes
	String CSS_HIDE_CLASS = "hide-question";
	String CSS_SHOW_CLASS = "show-style";
	String CSS_QUESTION_CONTAINER = "question-container";
	String CSS_STATIC_TWO_COLUMN = "static-two-column";
	String CSS_STATIC = "static";
	String CSS_DISPLAY_ONLY = "display-only";
	String CSS_CHECKBOX = "checkbox";
	String CSS_RADIO = "rdo";
	String CSS_ERROR_CLASS = "error";
	String CSS_NON_JS = "non-js";
	String CSS_OPTIONAL = "optional";
	String CSS_CURRENCY = "curr";
	String CSS_GROUP_TEXT = "group-text";
	String CSS_TEXT_DEFAULT = "txt";
	String CSS_TEXT_NUMERIC = "txt num";
	String CSS_TEXT_CURRENCY = "txt curr";
	String CSS_TEXT_ZIPCODE5 = "txt zip5";
	String CSS_TEXT_CREDITCARD = "txt ccrd";
	String CSS_TEXT_TELEPHONE_US = "txt tel";
	String CSS_TEXT_EMAIL = "txt email";
	String CSS_TEXT_NARRATIVE = "txt narr";
	String CSS_TEXT_PASSWORD = "txt pwd";
	String CSS_DROPDOWN_DEFAULT = "drp";
	String CSS_DROPDOWN_CURRENCY = "drp curr";
	String CSS_DATE_DEFAULT = "date";
	String CSS_DATE_MONTHYEAR = "date my";
	String CSS_FLINK_DEFAULT = "flnk";
	String CSS_SURCHARGE_SUMM_DEFAULT = "sur-sum";
	String CSS_ADDRESS_SUMM_DEFAULT = "add-sum";
	String CSS_LIMIT_SUMM_DEFAULT = "lim-sum";
	String CSS_DEFAULT_NAVIGATION_BUTTON = "default-navigation-button";
	String CSS_ERR = "err-msg";

	// Page level error message style class
	String PAGE_ERROR_STYLE_CLASS = "error-summary hide-question";

	char CHAR_COMMA = ',';

	String STR_OUTPUT = "Output";
	String TEXT_ITEM = "TextItem";
	String TEXT_ITEM_HELP_TEXT = "HelpText";
	String TEXT_ITEM_TEXT_CODE = "TextCode";
	String TEXT_ITEM_LABEL = "Label";
	String TEXT_ITEM_ERROR_TEXT = "ErrorText";
	String TEXT_ITEM_STYLE = "Style";
	String TEXT_ITEM_PURPOSE = "Purpose";
	String TEXT_ITEM_HELP_TITLE = "HelpTitle";
	String TEXT_ITEM_DEFAULT_VALUE = "DefaultValue";
	String TEXT_ITEM_ADD_TEXT1 = "AdditionalText1";
	String TEXT_ITEM_ADD_TEXT2 = "AdditionalText2";
	String TEXT_ITEM_ADD_TEXT3 = "AdditionalText3";
	String TEXT_ITEM_ADD_TEXT4 = "AdditionalText4";
	String TEXT_ITEM_ADD_TEXT5 = "AdditionalText5";
	String TEXT_ITEM_ADD_TEXT6 = "AdditionalText6";
	String TEXT_ITEM_ADD_TEXT7 = "AdditionalText7";
	String TEXT_ITEM_ADD_TEXT8 = "AdditionalText8";
	String TEXT_ITEM_ADD_TEXT9 = "AdditionalText9";
	String STR_ACTION = "action";
	String STR_BUTTON = "button";
	String STR_HOME = "home";
	String STR_IMAGE_BUTTON_ID = "_image_buttonid";
	String STR_INPUT_HIDDEN_ID = "_input_hiddenid";
	String STR_INPUT_HIDDEN_NAME = "_input_hiddenname";
	String STR_DROPDOWN_SUFFIX = "_dropdown_";
	String STR_NAME_SUFFIX = "_name";
	String STR_YEAR_RANGE_START_ID = "_yearrangestartid";
	String STR_YEAR_RANGE_END_ID = "_yearrangeendid";
	String STR_BLANK_VALUE = "Blank";
	String XML_APPLICATION_AREA = "ApplicationArea";
	String XML_STATUS = "Status";
	String XML_CORRELATION_ID = "CorrelationID";
	String STR_HELP_ID = "_help_id";
	String STR_HELP_NAME = "_help_name";
	String STR_NOT_EQUALS = "notEquals";
	String CONFIG_IS_VALIDATION_REQUIRED = "isValidationRequired";
	String CONFIG_IS_PROGRESS_BAR_REQUIRED = "isProgrssbarRequired";
	String STR_DISPLAY_HINT = "displayHint";
	String STR_DISABLE = "Disable";
	String STR_ENABLE = "Enable";
	String HTML_BUTTON_ENABLE_STYLE_HINT = "question-container red-button right-arrow-white float-right clear";
	String HTML_BUTTON_DISABLE_STYLE_HINT = "question-container grey-button right-arrow-black float-right disabled  clear";
	String HREF_APP_SUMMARY = "application-summary-terms-and-conditions";
	String POPUP_CALL = "return popup(this)";
	String STR_REFERENCE_NUMBER = "referencenumber";
	String STR_SCSECONDARY_COB  = "scsecondarycob";

	// regular expressions
	String REG_ONLY_NUMERIC = "[^0-9.]+";
	String REG_ONLY_NUMERIC_FORMATTED = "[0-9.,]+";
	String REG_ONLY_ALPHA = "[^A-z]+";
	String REG_ONLY_ALPHA_NUMERIC = "[^A-z0-9]+";
	String REG_NUMERIC = "[0-9]+";
	String REG_ONLY_CAPS_VAL = "[^A-Z]";
	String REG_ONLY_SPECIAL_VAL = "[A-z0-9]";
	String REG_NON_COMMA_VALUE = "[^,]+";
	String REG_ONLY_NUMERIC_WITHOUT_DECIMALS = "[^0-9.]+";

	// currency Codes
	String CURRENCY_USD = "USD";

	// currency Symbols
	String CURRENCY_SYMBOL_USD = "$";
	String STR_PRE_TICK = "Pre-tick";
	String STR_IS_JS_ENABLED = "isJSEnabled";
	String STR_HELP_TEXT = "help-text";
	String STR_QUESTION_MARK_SYMBOL = "?";
	String STR_PAGE_NAME = "pageName";
	String STR_QUESTION_CODE = "questionCode";
	String STR_OR = "OR";
	String STR_AND = "AND";
	String STR_AND_OR = "ANDOR";

	// Date Renderer Specific constants

	String DATE_DEF_FORMAT = "DMY";
	String MONTH_FORMAT_1 = "MMM";
	String MONTH_FORMAT_2 = "M";
	String DATE_FORMAT_MMYY = "MM/yyyy";
	char DAY_CHAR = 'D';
	char MONTH_CHAR = 'M';
	char YEAR_CHAR = 'Y';
	String STR_MONTH = "month";
	String STR_DAY = "day";
	String STR_YEAR = "year";
	String DATE_TOKENS = "/- \\";
	String STR_AM_LOWER = "am";
	String STR_PM_LOWER = "pm";
	String STR_AM_UPPER = "AM";
	String STR_PM_UPPER = "PM";

	String DATE_TIME_VAL_FORM = "d/M/y H";
	String SPACE_ZERO_VAL_FORM = " 00";
	String SPACE_TWELVE_VAL_FORM = " 12";
	String DATETIME_DEC_VAL_FORM = "##.00";
	String DATETIME_DATE_VAL_FORM = "H.mm";
	// Screen Reference constants used to retrieve CTM data

	public static final String[][] CTM_SCREEN_NAME_REF = {
			{"YourQuoteException", "YourQuote" },
			{"AdditionalQuestions", "YourBusiness" },
			{"AdditionalQuestions2", "YourBusiness2" },
			{"ProductOptions-ChangeCoverage", "ProductOptions" },
			{"UserSpecificBusinessInsurance-ChangeCoverage",
					"UserSpecificBusinessInsurance" }

	};
	public static final String[][] CTM_SCREEN_MIRROR_NAME_REF = {
	    {"YourBusiness", "AboutYou"},
	    {"AdditionalQuestions", "AboutYou" }
	};

	// Butoon Css Style for IE 6 and IE7
	String CSS_IE_BUTTON_STYLE = "lftbtns-movetp";
	String QUESTION_CODE_SECURITYCODE = "SecurityCode";

	// numeric comparator constants

	String GT = "GT";
	String LT = "LT";
	String GTEQ = "GTEQ";
	String LTEQ = "LTEQ";
	String EQ = "EQ";

	// ISONEOFAND Logic Strings

	String STR_LESSTHAN_GREATERTHAN_DELIMETER = "<>";
	String STR_SHOW_ON_LOGIC_STRING = "ShowOn--ISONEOF~~AND";
	String STR_SHOW_ON_AND_CASE = "ShowOn--AND--CASE";
	String STR_SHOW_ON_DD_AND_CASE = "ShowOn--DD-AND--CASE";
	String STR_EQUALLS_MAXLENGTH = "EQUALSMAXLENGTH";
	String STR_VALIDATE_ON_LOGIC_STRING = "ValidateOn--ISONEOF~~AND";
	String STR_DISPLAY_HINT_SHOW = "Show";
	String STR_DISPLAY_HINT_HIDE = "Hide";
	String STR_DOUBLE_CAP_DELIMETER = "^^";
	String ERRORS = "Errors";
	String STR_CHANGE_OPTION = "CHANGEOPTION";
	String STR_GROUPING = "GROUPING";
	String STR_LETTER_U = "U";
	String STR_ID = "Id";

	int PERCENTAGE_MIN_VALUE = 0;
	int PERCENTAGE_MAX_VALUE = 100;

	// Xpath Constants

	String XPATH_SCREEN_QUESTIONS_WITH_PERCENTAGE_SUBTYPE = "//ScreenQuestion/Config[@name='subtype'][@value='Percentage']";
	String XPATH_SCREEN_QUESTION_CONFIG_NAME_NOT_SAVED_VALUE = "//ScreenQuestion/Config[@name != 'savedValue' and @name !='returnUrl' and @name != 'expiryUrl' and @name !='pageSetId' and @name != 'datacashReference' and @name !='dynData1' and @name != 'dynData2' and @name !='dynData3' and @name !='dynData4' and @name !='dynData5' and @name !='dynData6' and @name !='dynData7' and @name !='dynData8' and @name !='dynData9']";
	String XPATH_SCREEN_QUESTION_PERMITTED_VALUE = "//ScreenQuestion/PermittedValue";
	String XPATH_SCREEN_QUESTION_IPEQUESTION_REFERENCE = "//ScreenQuestion/IPEQuestionReference";
	String XPATH_QUESTION_GROUP_CONFIG = "//QuestionGroup/Config";
	String XPATH_QUESTION_GROUP_IPEQUESTION_REFERENCE = "//QuestionGroup/IPEQuestionReference";
	String XPATH_FORM_CONFIG_FORM_ACTION_LIST = "//Form/Config|//Form/ActionList";
	String XPATH_DATA_AREA_CONFIG_NAME_UPDATE_QUOTE_ID = "//DataArea/Config[@name != 'updateQuoteID']";
	String XPATH_APPLICATION_AREA_ERRORS = "//ApplicationArea/Errors";
	String XPATH_APPLICATION_AREA_CORRELATION_ID = "//ApplicationArea/CorrelationID";
	String XPATH_APPLICATION_AREA_STATUS = "//ApplicationArea/Status";
	String XPATH_OR = "|";
	String XPATH_SCREEN_QUESTION_CONFIG_NAME_SAVED_VALUE = "//ScreenQuestion/Config[@name = 'savedValue']";
	String XPATH_ACTION = "//Action";
	String XPATH_SCREEN_QUESTION_CONFIG_NAME_TYPE_VALUE_ACTION = "//ScreenQuestion/Config[@name='type'][@value='Action']";
	String XPATH_ACTION_LIST_QUESTION_GROUP = "//ActionList/QuestionGroup";
	String XPATH_DATA_AREA_CONFIG_NAME_DEFAULT_NAVIGATOR = "//DataArea/Config[@name = 'defaultNavigator']";
	String XPATH_TAG_LIST_CONFIG = "//TagList/Config";
	/** Added for US25094-Add Accounting PL to DPD */
	String ENO_PRODUCT = "EnoProduct";
	String STR_GROUP_18="Group18";
	String STR_GROUP_YB_2_4="YB_2.4";
	String STR_GROUP_YB_2_6="YB_2.6";
	

}
