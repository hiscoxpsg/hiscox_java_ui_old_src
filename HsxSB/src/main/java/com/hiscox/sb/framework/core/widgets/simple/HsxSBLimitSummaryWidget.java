package com.hiscox.sb.framework.core.widgets.simple;

import com.hiscox.sb.framework.core.widgets.HsxSBBaseWidget;
import com.hiscox.sb.framework.core.widgets.vo.HsxSBWidgetInfo;

/**
 * This class holds the characteristics of Limit Summary, and implements the
 * basic functionality defined by the widget interface to create a widget.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:14 AM
 */
public class HsxSBLimitSummaryWidget extends HsxSBBaseWidget  {

    /**
	 *
	 */
	private static final long serialVersionUID = 5865047726568755803L;

	public HsxSBLimitSummaryWidget() {
	super();
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     * @param widgetInfo contains info of widget
     */
    public HsxSBLimitSummaryWidget(String id, String name,
	    HsxSBWidgetInfo widgetInfo) {
	super(id, name, widgetInfo);
    }

    /**
     *
     * @param id contains id
     * @param name contains name
     */
    public HsxSBLimitSummaryWidget(String id, String name) {
	super(id, name);
    }

    /**
     * Method to create widget by setting widget info, Style hint etc.
     *
     * @param widgetInfo contains info of widget
     */
    @Override
    public void createWidget(HsxSBWidgetInfo widgetInfo) {
	this.setWidgetInfo(widgetInfo);
	widgetInfo.setStyle("");

    }
}
