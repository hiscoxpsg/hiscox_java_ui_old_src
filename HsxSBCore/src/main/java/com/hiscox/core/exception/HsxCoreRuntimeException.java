package com.hiscox.core.exception;

import java.util.Calendar;

import com.hiscox.core.constants.HsxCoreConstants;

/**
 * This Class houses the functionalities required for exception handling. The
 * Exceptions could be originating from any of the USDC UI Components or from
 * one of the components in the Dynamic Screen Generation Framework, or the
 * Exceptions originating in Process Orchestration layer
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:13 AM
 */
public class HsxCoreRuntimeException extends Exception implements
	HsxCoreConstants {

    private String additionalInformation;
    private String applicationName;
    private String applicationServerName;
    private Calendar dateTime;
    private String errorCode;
    private String errorDescription;
    private String errorInstanceID;
    private String mainService;
    private static final long serialVersionUID = 1L;

    public HsxCoreRuntimeException() {

    }

    /**
     *
     * @param errorDescription
     */
    public HsxCoreRuntimeException(String errorDescription) {
	this.errorDescription = errorDescription;
    }

    /**
     *
     * @param errorCode
     * @param errorDescription
     * @param errorInstanceID
     * @param dateTime
     * @param additionalInformation
     */
    public HsxCoreRuntimeException(String errorCode, String errorDescription,
	    String errorInstanceID, Calendar dateTime,
	    String additionalInformation) {

    }

    public String getAdditionalInformation() {
	return additionalInformation;
    }

    public String getApplicationName() {
	return applicationName;
    }

    public String getApplicationServerName() {
	return applicationServerName;
    }

    public Calendar getDateTime() {
	return dateTime;
    }

    public String getErrorCode() {
	return errorCode;
    }

    public String getErrorDescription() {
	return errorDescription;
    }

    public String getErrorInstanceID() {
	return errorInstanceID;
    }

    public String getMainService() {
	return mainService;
    }

    /**
     *
     * @param additionalInformation
     */
    public void setAdditionalInformation(String additionalInformation) {
	this.additionalInformation = additionalInformation;
    }

    /**
     *
     * @param applicationName
     */
    public void setApplicationName(String applicationName) {
	this.applicationName = applicationName;
    }

    /**
     *
     * @param applicationServerName
     */
    public void setApplicationServerName(String applicationServerName) {
	this.applicationServerName = applicationServerName;
    }

    /**
     *
     * @param dateTime
     */
    public void setDateTime(Calendar dateTime) {
	this.dateTime = dateTime;
    }

    /**
     *
     * @param errorCode
     */
    public void setErrorCode(String errorCode) {
	this.errorCode = errorCode;
    }

    /**
     *
     * @param errorDescription
     */
    public void setErrorDescription(String errorDescription) {
	this.errorDescription = errorDescription;
    }

    /**
     *
     * @param errorInstanceID
     */
    public void setErrorInstanceID(String errorInstanceID) {
	this.errorInstanceID = errorInstanceID;
    }

    /**
     *
     * @param mainService
     */
    public void setMainService(String mainService) {
	this.mainService = mainService;
    }

}
