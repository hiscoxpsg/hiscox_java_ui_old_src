package com.hiscox.core.exception;

import com.hiscox.core.constants.HsxCoreConstants;

/**
 * This Class houses the functionalities required for exception handling. The
 * Exceptions could be originating from any of the USDC UI Components or from
 * one of the components in the Dynamic Screen Generation Framework, or the
 * Exceptions originating in Process Orchestration layer
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:13 AM
 */
public class HsxCoreException extends Exception implements HsxCoreConstants {

    private String additionalInformation;
    private String applicationName;
    private String applicationServerName;
    private String dateTime;
    private String errorCode;
    private String errorDescription;
    private String errorInstanceID;
    private String mainService;
    private static final long serialVersionUID = 1L;

    public HsxCoreException() {
	super();
    }

    /**
     *
     * @param errorDescription
     */
    public HsxCoreException(String errorDescription) {
	super();
	this.errorDescription = errorDescription;
    }

    /**
     *
     * @param errorCode
     * @param errorDescription
     * @param errorInstanceID
     * @param dateTime
     * @param additionalInformation
     */
    public HsxCoreException(String errorCode, String errorDescription,
	    String errorInstanceID, String dateTime,
	    String additionalInformation) {
	super();
	this.errorCode = errorCode;
	this.errorDescription = errorDescription;
	this.errorInstanceID = errorInstanceID;
	this.dateTime = dateTime;
	this.additionalInformation = additionalInformation;
    }

    public String getAdditionalInformation() {
	return additionalInformation;
    }

    public String getApplicationName() {
	return applicationName;
    }

    public String getApplicationServerName() {
	return applicationServerName;
    }



    public String getErrorCode() {
	return errorCode;
    }

    public String getErrorDescription() {
	return errorDescription;
    }

    public String getErrorInstanceID() {
	return errorInstanceID;
    }

    public String getMainService() {
	return mainService;
    }

    /**
     *
     * @param additionalInformation
     */
    public void setAdditionalInformation(String additionalInformation) {
	this.additionalInformation = additionalInformation;
    }

    /**
     *
     * @param applicationName
     */
    public void setApplicationName(String applicationName) {
	this.applicationName = applicationName;
    }

    /**
     *
     * @param applicationServerName
     */
    public void setApplicationServerName(String applicationServerName) {
	this.applicationServerName = applicationServerName;
    }


    /**
     *
     * @param errorCode
     */
    public void setErrorCode(String errorCode) {
	this.errorCode = errorCode;
    }

    /**
     *
     * @param errorDescription
     */
    public void setErrorDescription(String errorDescription) {
	this.errorDescription = errorDescription;
    }

    /**
     *
     * @param errorInstanceID
     */
    public void setErrorInstanceID(String errorInstanceID) {
	this.errorInstanceID = errorInstanceID;
    }

    /**
     *
     * @param mainService
     */
    public void setMainService(String mainService) {
	this.mainService = mainService;
    }
    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

}
