package com.hiscox.core.security.ldap;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import com.hiscox.core.constants.HsxCoreConstants;

/**
 * HsxCoreKeyDAOImplis the implementation class for KeyDAO interface. This class
 * the implementation methods for the retrieval of the key stored in the LDAP.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:14 AM
 */
public class HsxCoreKeyDAOImpl implements HsxCoreKeyDAO, HsxCoreConstants {

    private String base = null;
    private String dnsName = null;
    private String password = null;
    private String userDn = null;

    public HsxCoreKeyDAOImpl() {

    }

    /**
     * createAuthenticatedContext - create an authenticated context based on the
     * inputs provided.
     *
     * @return DirContext
     */
    private DirContext createAuthenticatedContext() {
	Hashtable<String, String> env = new Hashtable<String, String>();
	env.put(Context.SECURITY_AUTHENTICATION, "simple");
	env.put(Context.SECURITY_PRINCIPAL, userDn);
	env.put(Context.SECURITY_CREDENTIALS, password);
	return createContext(env);
    }

    /**
     * createContext - create an authenticated context based on the input
     * parameters.
     *
     * @return ctx
     *
     * @param env
     */
    private DirContext createContext(Hashtable<String, String> env) {
	env.put(Context.INITIAL_CONTEXT_FACTORY,
		"com.sun.jndi.ldap.LdapCtxFactory");

	env.put(Context.PROVIDER_URL, dnsName);
	DirContext ctx = null;
	try {
	    ctx = new InitialDirContext(env);
	} catch (NamingException e) {

	}
	return ctx;
    }

    /**
     * getKey is the method used to retrieve the symmetric key from the LDAP.
     *
     * @return encodedString
     *
     * @param appDn
     *            - this is application distinguished name (For example
     *            CN=CardPayment,OU=APP,DC=HISCOX,DC=CO,DC=UK)
     * @param attributeName
     *            - the name of the attribute where the key is stored.
     * @exception Exception
     */
    public String getKey(String appDn, String attributeName) throws Exception {
	/**
	 * create an authenticated context and retrieve the attribute.
	 */
	DirContext ctx = createAuthenticatedContext();
	Attributes attributes = ctx.getAttributes(appDn);
	Attribute attribute = attributes.get(attributeName);

	String encodedString = null;
	if (attribute.get() != null) {
	    encodedString = attribute.get().toString();
	} else {
	    throw new Exception("No data available for this attribute");
	}
	return encodedString;
    }

    /**
     * setBase is the method used to set the base DN for the server.
     *
     *
     *
     * @param base - Base DN
     *
     */
    public void setBase(String base) {
	this.base = base;
    }

    /**
     * getBase is the method used to get the base DN for the server.
     *
     * @return base
     */
    public String getBase() {
	return this.base;
    }

    /**
     * Method to set the key in LDAP.
     *
     * @param attributeName - the name of the attribute to set the key.
     * @param key
     *            - the secret key
     * @param attributeName
     * @exception Exception
     */
    public void setKey(String key, String attributeName) throws Exception {

    }

    /**
     * setPassword is the method used to set the password.
     *
     * @param password
     *            - password field
     */
    public void setPassword(String password) {
	this.password = password;
    }

    /**
     * setUrl is the method to set the LDAP URL.
     *
     * @param dnsName
     *            - LDAP URL.
     */
    public void setUrl(String dnsName) {
	this.dnsName = dnsName;
    }

    /**
     * setUserDn is the method used to set the user DN.
     *
     * @param userDn
     *            - user DN
     */
    public void setUserDn(String userDn) {
	this.userDn = userDn;
    }

}

