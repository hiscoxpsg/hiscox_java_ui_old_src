package com.hiscox.core.security.ldap;

/**
 * HsxCoreKeyDAOis the interface class which defines the utility LDAP methods
 * required for encryption and decryption operation.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:14 AM
 */
public interface HsxCoreKeyDAO {

    /**
     *
     * @param appDn
     * @param attributeName
     * @return encodedString
     * @exception Exception
     */
    String getKey(String appDn, String attributeName) throws Exception;

    /**
     *
     * @param base base
     */
    void setBase(String base);

    /**
     *
     * @param key
     * @param attributeName
     * @exception Exception
     */
    void setKey(String key, String attributeName) throws Exception;

    /**
     *
     * @param password
     */
    void setPassword(String password);

    /**
     *
     * @param dnsName
     */
    void setUrl(String dnsName);

    /**
     *
     * @param userDn
     */
    void setUserDn(String userDn);

}

