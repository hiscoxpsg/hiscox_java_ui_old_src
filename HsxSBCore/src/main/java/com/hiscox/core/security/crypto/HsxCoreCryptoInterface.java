package com.hiscox.core.security.crypto;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Interface : HsxCoreCryptoInterface is the interface specification for the
 * AESEngine class. This interfaces defines the methods which are required to
 * encrypt and decrypt the data.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:13 AM
 */
public interface HsxCoreCryptoInterface {

    void getAlgorithmName();

    void getBlockSize();

    void getProvider();

    /**
     *
     * @param aesCipher
     * @param MODE
     * @param key
     * @exception Exception
     * @return aesCipher
     */
    Cipher init(Cipher aesCipher, String MODE, SecretKeySpec key)
	    throws Exception;

    /**
     *
     * @param MODE
     * @param data
     * @param secretKey
     * @exception Exception
     * @return cipherString
     */
    String processBlock(String MODE, byte[] data, String secretKey)
	    throws Exception;

    void reset();

    void setAlgorithmName();

    void setBlockSize();

    void setProvider();

}

