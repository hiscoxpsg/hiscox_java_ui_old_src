package com.hiscox.core.security.crypto;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;

/**
 * HsxCoreAESEngineis the implementation class for HsxCoreCryptoInterface. This
 * class implements all the methods defined in the interface. This class
 * provides the implementation of encrypting and decrypting the text data
 * provided. The symmetric key for encryption will be stored in the LDAP. The
 * symmetric key will be used for both encryption and decryption. The key will
 * be encoded and stored in LDAP.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:12 AM
 */
public class HsxCoreAESEngine implements HsxCoreCryptoInterface {

    public HsxCoreAESEngine() {

    }

    /**
     * getAlgorithmName() is the method to retrieve the algorithm used for
     * encryption. This is not currently used and implemented.
     *
     *
     */
    public void getAlgorithmName() {

    }

    /**
     * getBlockSize() is the method used to retrieve the block size used for
     * encryption This is not currently used and implemented.
     *
     *
     */
    public void getBlockSize() {

    }

    /**
     * getProvider() is the method used to retrieve the provider which
     * implements the cryptography algorithm. This version of AES Engine uses
     * Sun JCE provider implementation. This is not currently used and
     * implemented.
     *
     *
     */
    public void getProvider() {

    }

    /**
     * init method is to initialize the encryption/decryption and generate the
     * Cipher. This method handles both encryption and decryption. The Cipher
     * will be generated using "AES" algorithm.
     *
     * @return Cipher - Return the Cipher after initialization.
     *
     * @param aesCipher
     * @param MODE
     *            - Mode represents whether the initialization is for ENCRYPT or
     *            DECRYPT
     * @param keySpec
     *            - Represents the keyspec used to encrypt/decrypt the data. The
     *            key spec will be retrieved from LDAP.
     * @exception Exception
     */
    public Cipher init(Cipher aesCipher, String MODE, SecretKeySpec keySpec)
	    throws Exception {
	aesCipher = Cipher.getInstance("AES");
	if (MODE != null && MODE.equalsIgnoreCase("ENCRYPT")) {
	    aesCipher.init(Cipher.ENCRYPT_MODE, keySpec);
	} else if (MODE != null && MODE.equalsIgnoreCase("DECRYPT")) {
	    aesCipher.init(Cipher.DECRYPT_MODE, keySpec);
	}
	return aesCipher;
    }

    /**
     * processBlock is the method invoked by the consumer to encrypt and decrypt
     * the data.
     *
     * @param MODE
     *            - Denotes whether the data has to be encrypted or decrypted.
     * @param data
     *            - Represents the data which needs to be encrypted.
     * @param keyString
     * @exception Exception
     *                Exception
     * @return cipherString
     */
    public String processBlock(String MODE, byte[] data, String keyString)
	    throws Exception {
	// cipher string to be returned to the consumer.
	String cipherString = null;
	try {
	    byte[] cipherData = data;

	    // decode the symmetric key string retrieved from LDAP store.
	    BASE64Decoder decoder = new BASE64Decoder();
	    // generate the key specification based on the data.
	    SecretKeySpec skeySpec = new SecretKeySpec(decoder
		    .decodeBuffer(keyString), "AES");

	    // initialize the cipher based on the requested mode.
	    Cipher aesCipher = null;
	    aesCipher = init(aesCipher, MODE, skeySpec);
	    // if the mode is decrypt - decode the data before decrypting
	    if (MODE != null && MODE.equalsIgnoreCase("DECRYPT")) {
		cipherData = decoder.decodeBuffer(new String(data));
	    }
	    // encrypt/decrypt the data and return the encrypted/decrypted data
	    byte[] ciphertext = aesCipher.doFinal(cipherData);

	    if (MODE != null && MODE.equalsIgnoreCase("ENCRYPT")) {
		BASE64Encoder en = new BASE64Encoder();
		cipherString = en.encode(ciphertext);
	    } else if (MODE != null && MODE.equalsIgnoreCase("DECRYPT")) {
		cipherString = new String(ciphertext);
	    }
	} catch (RuntimeException e) {
	    throw new HsxCoreRuntimeException(e.getMessage());
	} catch (Exception e) {
	    throw new HsxCoreException(e.getMessage());
	}
	return cipherString;
    }

    /**
     * reset method is to reset the encryption/decryption process. This is not
     * currently used and implemented.
     *
     *
     */
    public void reset() {

    }

    /**
     * reset method is to set the algorithm. This is not currently used and
     * implemented.
     *
     *
     */
    public void setAlgorithmName() {

    }

    /**
     * setBlockSize method is to set the block size for encryption or
     * decryption. This is not currently used and implemented.
     *
     *
     */
    public void setBlockSize() {

    }

    /**
     * setProvider method is to set the cryptography provider. Currently the
     * class uses the default Sun JCE. This is not currently used and
     * implemented.
     *
     *
     */
    public void setProvider() {

    }

}

