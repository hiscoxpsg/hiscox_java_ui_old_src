package com.hiscox.core.security.crypto;

import com.hiscox.core.constants.HsxCoreConstants;
import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.security.ldap.HsxCoreKeyDAO;
import com.hiscox.core.security.ldap.HsxCoreKeyDAOImpl;

/**
 * HsxCoreKeyGeneratoris a utility class to generate and retrieve keys for
 * encryption/decryption. This class will access the back end store to retrieve
 * the key information (LDAP).
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:15 AM
 */
public class HsxCoreKeyGenerator implements HsxCoreConstants {

	private String base = null;
	// private Cipher desCipher;
	// private SecretKey desKey;
	private String dnsName = null;
	// private byte key[];
	private String password = null;
	private String userDn = null;

	public HsxCoreKeyGenerator() {
		super();
	}

	public HsxCoreKeyGenerator(String userDn, String password, String base,
			String dnsName) {
		this.userDn = userDn;
		this.password = password;
		this.base = base;
		this.dnsName = dnsName;
	}

	/**
	 * createNewKey method creates a new AES 128 bit key and store this in the
	 * back end store. The key will be encoded before storing.
	 *
	 * @exception HsxCoreException
	 * @exception HsxCoreRuntimeException
	 */
	public void createNewKey() throws HsxCoreRuntimeException, HsxCoreException {

	}

	/**
	 * retrieveKey method is used to retrieve the key from the back end store
	 * (LDAP). The retrieved key will be used for encryption and decryption.
	 *
	 * @return encodedKey
	 *
	 * @param appDn
	 *            - the Distinguished Name for the application in LDAP
	 * @param attributeName
	 *            - the attribute name in which the key is stored.
	 * @exception IOException
	 * @exception CertificateException
	 * @exception UnrecoverableKeyException
	 * @exception KeyStoreException
	 * @exception NoSuchAlgorithmException
	 */
	public String retrieveKey(String appDn, String attributeName)
			throws HsxCoreRuntimeException, HsxCoreException {
		HsxCoreKeyDAO dao = new HsxCoreKeyDAOImpl();
		String encodedKey;
		try {
			dao.setBase(base);
			dao.setPassword(password);
			dao.setUrl(dnsName);
			dao.setUserDn(userDn);

			encodedKey = dao.getKey(appDn, attributeName);
		} catch (RuntimeException e) {
			throw new HsxCoreRuntimeException(e.getMessage());
		} catch (Exception e) {
			throw new HsxCoreException(e.getMessage());
		}

		return encodedKey;
	}

	/**
	 * Method to set the credentials in the KeyGenerator class.
	 *
	 * @param userDn
	 *            - User DN
	 * @param password
	 *            - Password
	 * @param base
	 *            - Base DB
	 * @param ldapUrl
	 *            - LDAP URL
	 */
	public void setCredentials(String userDn, String password, String base,
			String ldapUrl) {
		this.userDn = userDn;
		this.password = password;
		this.base = base;
		this.dnsName = ldapUrl;
	}

}

