package com.hiscox.core.constants;

/**
 * This is utility class for the Core Project which carries the constant values
 * used across the HsxCore.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:27:41 AM
 */
public interface HsxCoreConstants {

    String STR_EMPTY = "";
    String STR_SINGLE_SPACE = " ";
    String STR_NEW_LINE = "\n";
    String STR_ZERO = "0";
    String STR_NINE = "9";
    String STR_PERCENT = "%";
    String STR_CHAR_N = "n";
    String STR_CHAR_C = "c";
    String STR_SINGLE_QUOTE = "'";
    String STR_SINGLE_QUOTES = "''";
    String STR_QUOTE_ESCAPED = "\"";
    String STR_HYPHEN = "-";
    String STR_LESS = "<";
    String STR_GREATER = ">";
    String STR_GREATER_THAN = "&gt;";
    String STR_LESS_THAN = "&lt;";
    String STR_AMPERSAND = "&amp;";
    String STR_AND = "&";
    String STR_CHAR_ZERO = "\u0000";
    String STR_YES = "Yes";
    String STR_DOT = ".";
    String LOG_PROPERTIES = "com.hiscox.core.properties.logProperties";
    String CORE_LOGGER_CLASS = "HsxCoreLogger";
    String STR_ERROR_MESSAGE = "Error Message :";

    String STR_OUTPUT = "Output";
    String STR_SIMPLE = "simple";
    String STR_LDAP_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
    String STR_MAIL = "mail";
    String STR_OU_USER = "OU=USER";
    String STR_BAD_PASSWORD_COUNT = "badPwdCount";
    String CORE = "CORE";
    String INFO = "INFO";
    String ERROR = "ERROR";
    String DEBUG = "DEBUG";
    String STR_CN = "cn";
    String STR_GUID = "guid";

    String TEXT_ITEM = "TextItem";
    String TEXT_ITEM_HELP_TEXT = "HelpText";
    String TEXT_ITEM_TEXT_CODE = "TextCode";
    String TEXT_ITEM_LABEL = "Label";
    String TEXT_ITEM_ERROR_TEXT = "ErrorText";
    String TEXT_ITEM_STYLE = "Style";
    String TEXT_ITEM_PURPOSE = "Purpose";
    String TEXT_ITEM_HELP_TITLE = "HelpTitle";
    String TEXT_ITEM_DEFAULT_VALUE = "DefaultValue";
    String TEXT_ITEM_ADD_TEXT1 = "AdditionalText1";
    String TEXT_ITEM_ADD_TEXT2 = "AdditionalText2";
    String TEXT_ITEM_ADD_TEXT3 = "AdditionalText3";
    String TEXT_ITEM_ADD_TEXT4 = "AdditionalText4";
    String TEXT_ITEM_ADD_TEXT5 = "AdditionalText5";
    String TEXT_ITEM_ADD_TEXT6 = "AdditionalText6";
    String TEXT_ITEM_ADD_TEXT7 = "AdditionalText7";
    String TEXT_ITEM_ADD_TEXT8 = "AdditionalText8";
    String TEXT_ITEM_ADD_TEXT9 = "AdditionalText9";

    String SRVICE_BINDER = "ServiceBinder";
    String DEFAULT_URI = "DefaultUri";
    String REQUEST_XML = "RequestXml";

}

