package com.hiscox.core.logger;

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.hiscox.core.constants.HsxCoreConstants;

/**
 * This Class houses all the logging related activities. Log4j will be used for
 * logging activities in the USDC. Log4j enables logging at runtime without
 * modifying the application binary. Log4j needs to be configured up front using
 * a properties file.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:16 AM
 */
public class HsxCoreLogger extends Logger implements HsxCoreConstants {

	public HsxCoreLogger(String name) {
		super(name);
	}

	public static final Logger LOGGER = Logger.getLogger(HsxCoreLogger.class);
	public boolean loggerFlag;

	/**
	 *
	 * This method carries the application name, the method to be called i.e
	 * debug and the message as the input parameters. The properties file will
	 * be read and the value of the key will be retrieved. Based on the Key
	 * Value, the message will be displayed.
	 *
	 * @param packageName
	 * @param methodName
	 * @param message
	 */
	public void debug(String packageName, String methodName, String message) {
		boolean loggerValue;
		loggerValue = loggerCheck(packageName, methodName);
		if (loggerValue && LOGGER.isDebugEnabled()) {
			LOGGER.debug(message);
		} else {
			LOGGER.debug(STR_EMPTY);
		}
	}

	/**
	 *
	 * This method carries the application name, the method to be called i.e
	 * error and the message as the input parameters. The properties file will
	 * be read and the value of the key will be retrieved. Based on the Key
	 * Value, the message will be displayed.
	 *
	 * @param packageName
	 * @param methodName
	 * @param message
	 */
	public void error(String packageName, String methodName, String message) {
		boolean loggerValue;
		loggerValue = loggerCheck(packageName, methodName);
		if (loggerValue) {
			LOGGER.error(message);
		} else {
			LOGGER.error(STR_EMPTY);
		}
	}

	/**
	 *
	 * This method carries the application name, the method to be called i.e
	 * info and the message as the input parameters. The properties file will be
	 * read and the value of the key will be retrieved. Based on the Key Value,
	 * the message will be displayed.
	 *
	 * @param packageName
	 * @param methodName
	 * @param message
	 */
	public void info(String packageName, String methodName, String message) {
		boolean loggerValue;
		loggerValue = loggerCheck(packageName, methodName);
		if (loggerValue && LOGGER.isInfoEnabled()) {
			LOGGER.info(message);
		} else {
			LOGGER.info(STR_EMPTY);
		}
	}

	/**
	 *
	 * This method reads form the properties file and compares it with the input
	 * parameters. the Boolean value is returned
	 *
	 * @param packageName
	 * @param methodName
	 * @return loggerFlag
	 */
	public boolean loggerCheck(String packageName, String methodName) {
		ResourceBundle labels = ResourceBundle.getBundle(LOG_PROPERTIES);
		Enumeration<String> bundleKeys = labels.getKeys();

		while (bundleKeys.hasMoreElements()) {
			String loggerKey =  bundleKeys.nextElement();
			String loggerVal = labels.getString(loggerKey);
			String name = packageName.concat(STR_DOT).concat(methodName);
			if (loggerKey.contains(name)) {
				if (loggerVal.equalsIgnoreCase(STR_YES)) {
					loggerFlag = true;

				} else {
					loggerFlag = false;
				}
			}
		}
		return loggerFlag;
	}

	/**
	 *
	 * stack trace to determine the caller's class.
	 *
	 * @param packageName
	 * @param methodName
	 * @param message
	 * @return HsxCoreLogger or null
	 */
	public static HsxCoreLogger getLogger() {
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		int stackLen = stack.length;
		for (int i = 0; i < stackLen; i++) {
			if (stack[i].getClassName().endsWith(CORE_LOGGER_CLASS)) {
				return new HsxCoreLogger(stack[i + 1].getClassName());
			}
		}
		return null;
	}

}

