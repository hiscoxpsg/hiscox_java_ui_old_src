package com.hiscox.core.service;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Calendar;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.hiscox.core.constants.HsxCoreConstants;
import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.vo.HsxCoreRequest;

/**
 * This is the implementing class for the HsxWebServiceClient interface. This
 * class hosts all the necessary functionality required for making a web service
 * call.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:18 AM
 */
public class HsxWebServiceClientImpl
	implements
	    HsxWebServiceClient,
	    HsxCoreConstants {

    /**
     * This attribute will hold the default uri (service end point) information.
     */
    private WebServiceTemplate webServiceTemplate = null;

    public HsxWebServiceClientImpl(SoapVersion version) {
	org.springframework.ws.soap.axiom.AxiomSoapMessageFactory factory = new org.springframework.ws.soap.axiom.AxiomSoapMessageFactory();
	factory.setSoapVersion(version);
	webServiceTemplate = new WebServiceTemplate(factory);
    }

    public HsxWebServiceClientImpl() {
	org.springframework.ws.soap.axiom.AxiomSoapMessageFactory factory = new org.springframework.ws.soap.axiom.AxiomSoapMessageFactory();
	webServiceTemplate = new WebServiceTemplate(factory);
    }

    /**
     * This method will call the sendSourceAndReceiveToResult method of
     * WebServiceTemplate class.
     *
     * @param inputXML
     * @param serviceName
     * @throws HsxCoreRuntimeException
     * @throws HsxCoreException
     * @return responseXML
     */
    public StringBuilder callSendRecieve(HsxCoreRequest request)
	    throws HsxCoreRuntimeException, HsxCoreException {
	StringBuilder responseXML = null;
	try {
	    StreamSource source = new StreamSource(new StringReader(
		    (String) request.getAttribute(REQUEST_XML)));
	    Writer outWriter = new StringWriter();
	    StreamResult result = new StreamResult(outWriter);
	    webServiceTemplate.setDefaultUri((String) request
		    .getAttribute(DEFAULT_URI));
	    long a = Calendar.getInstance().getTimeInMillis();
	    // System.out.println("Service Call Started at :" + a);
	    webServiceTemplate.sendSourceAndReceiveToResult(source,
		    new SoapActionCallback((String) request
			    .getAttribute(SRVICE_BINDER)), result);
	    long b = Calendar.getInstance().getTimeInMillis();
	    // System.out.println("Service Call Ended at :" + b);
	    System.out.println("Time Taken for the Service Call :" + (b - a)
		    * 0.001 + " Seconds");
	    responseXML = new StringBuilder(outWriter.toString());
	} catch (RuntimeException e) {
	    e.printStackTrace();
	    throw new HsxCoreRuntimeException(e.getMessage());
	} catch (Exception e) {
	    throw new HsxCoreException(e.getMessage());
	}
	return responseXML;
    }
    // only for testing
    /*
     * public static void main(String[] args) {
     *
     * HsxWebServiceClientImpl impl = new HsxWebServiceClientImpl(); // impl //
     * .setDefaultUri(
     * "http://HXB31571.hiscox.nonprod:9005/ws/hsx.ctm.WSProviders:retieveDetailedText"
     * ); // String requestXML = //
     * "<retrieveTextInput>&lt;RetrieveTextInput&gt;&lt;Input&gt;&lt;Scheme&gt;USDC&lt;/Scheme&gt;&lt;Channel&gt;D&lt;/Channel&gt;&lt;Language/&gt;&lt;ActiveDate/&gt;&lt;EffectiveDate/&gt;&lt;TextCode/&gt;&lt;TextType/&gt;&lt;Format/&gt;&lt;/Input&gt;&lt;/RetrieveTextInput&gt;</retrieveTextInput>"
     * ; // String binderName = //
     * "hsx_ctm_WSProviders_retieveDetailedText_Binder_retrieveDetailedText";
     * HsxCoreRequest req = new HsxCoreRequest(); try { StringBuilder res =
     * impl.callSendRecieve(req); // System.out.println(res); } catch
     * (HsxCoreRuntimeException e) { // TODO Auto-generated catch block
     * e.printStackTrace(); } catch (HsxCoreException e) { // TODO
     * Auto-generated catch block e.printStackTrace(); } }
     */

    /**
     *
     * @param request
     * @return STR_EMPTY
     */
    public String callSendReceive(String request) {
	return STR_EMPTY;
    }

}
