package com.hiscox.core.service;

import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.vo.HsxCoreRequest;

/**
 * Client Interface for webservices.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:18 AM
 */
public interface HsxWebServiceClient {

    /**
     *
     * @param request
     * @return STR_EMPTY
     */
    public String callSendReceive(String request) throws HsxCoreException,
	    HsxCoreRuntimeException;

    /**
     *
     * @param inputXML
     * @param serviceName
     * @return responseXML
     */
    public StringBuilder callSendRecieve(HsxCoreRequest request)
	    throws HsxCoreException, HsxCoreRuntimeException;

}
