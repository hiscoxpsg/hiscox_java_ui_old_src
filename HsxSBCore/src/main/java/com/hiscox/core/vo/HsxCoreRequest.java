package com.hiscox.core.vo;

import java.util.HashMap;
import java.util.Map;

/**
 * This class will hold the information from one state to another state.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:20 AM
 */
public class HsxCoreRequest {

    private Map<Object, Object> attributes = new HashMap<Object, Object>();

    public Object getAttribute(String key) {
	return attributes.get(key);
    }

    public void setAttribute(Object key, Object value) {
	attributes.put(key, value);
    }

}

