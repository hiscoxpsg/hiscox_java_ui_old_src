package com.hiscox.core.vo;

/**
 * This class holds the error information.
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:22:20 AM
 */
public class HsxCoreError {

    private String errorType;
    private String errorCode;
    private String errorDescription;
    private String errorInstanceID;
    private String dateTime;
    private String additionalInformation;

    public String getErrorType() {
	return errorType;
    }

    public void setErrorType(String errorType) {
	this.errorType = errorType;
    }

    public String getErrorCode() {
	return errorCode;
    }

    public void setErrorCode(String errorCode) {
	this.errorCode = errorCode;
    }

    public String getErrorDescription() {
	return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
	this.errorDescription = errorDescription;
    }

    public String getErrorInstanceID() {
	return errorInstanceID;
    }

    public void setErrorInstanceID(String errorInstanceID) {
	this.errorInstanceID = errorInstanceID;
    }

    public String getDateTime() {
	return dateTime;
    }

    public void setDateTime(String dateTime) {
	this.dateTime = dateTime;
    }

    public String getAdditionalInformation() {
	return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
	this.additionalInformation = additionalInformation;
    }

}

