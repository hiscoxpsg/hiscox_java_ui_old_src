package com.hiscox.core.util;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import com.hiscox.core.constants.HsxCoreConstants;
import com.hiscox.core.exception.HsxCoreException;
import com.hiscox.core.exception.HsxCoreRuntimeException;
import com.hiscox.core.logger.HsxCoreLogger;

/**
 * This class hosts multiple utility operations that can be used to retrieve
 * meaningful information from LDAP such as badPwdCount or DOB etc..
 *
 * @author Cognizant
 * @version 1.0
 * created 25-Mar-2010 6:31:15 AM
 */
public class HsxCoreLDAPUtil implements HsxCoreConstants {

    public HsxCoreLDAPUtil() {

    }

    public static HsxCoreLogger logger = HsxCoreLogger.getLogger();
    private static String userDn = STR_EMPTY;
    private static String userPassword = STR_EMPTY;
    private static String dnsName = STR_EMPTY;

    /**
     *
     * This method will create an authenticated context based on the inputs
     * provided. The inputs such as ldap URL,managerDn and password are
     * retrieved from a property file.
     *
     * @param mailId
     * @return context
     */
    public static DirContext createAuthenticatedContext(String ldapName,
	    String managerDn, String password) {
	Hashtable<String, String> env = new Hashtable<String, String>();
	HsxCoreLDAPUtil.dnsName = ldapName;
	HsxCoreLDAPUtil.userPassword = password;
	HsxCoreLDAPUtil.userDn = managerDn;

	env.put(Context.SECURITY_AUTHENTICATION, STR_SIMPLE);
	env.put(Context.SECURITY_PRINCIPAL, userDn);
	env.put(Context.SECURITY_CREDENTIALS, userPassword);
	return createContext(env, dnsName);
    }

    /**
     * createContext - create an authenticated context based on the input
     * parameters.
     *
     * @param env
     * @return ctx
     */
    private static DirContext createContext(Hashtable<String, String> env,
	    String dnsName) {
	env.put(Context.INITIAL_CONTEXT_FACTORY, STR_LDAP_CONTEXT_FACTORY);

	env.put(Context.PROVIDER_URL, dnsName);
	DirContext ctx = null;
	try {
	    ctx = new InitialDirContext(env);
	} catch (NamingException e) {
	    // throw new RuntimeException(e);
	}
	return ctx;
    }

    /**
     *
     * This method retrieves the badPwdCount based on email id.
     *
     * @param mailId
     * @return badPwdCount
     * @throws HsxCoreRuntimeException
     * @throws HsxCoreException
     */
    public String getBadCredential(String mailId, String ldapName,
	    String managerDn, String password) throws HsxCoreRuntimeException,
	    HsxCoreException {
	String badPwdCount = STR_EMPTY;

	DirContext ctx = createAuthenticatedContext(ldapName, managerDn,
		password);

	SearchControls constraints = new SearchControls();
	constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
	Attributes matchAttrs = new BasicAttributes(true);
	matchAttrs.put(new BasicAttribute(STR_MAIL, mailId));

	try {
	    NamingEnumeration<SearchResult> results = ctx.search(STR_OU_USER, matchAttrs);

	    while (results != null && results.hasMore()) {
		SearchResult sr = (SearchResult) results.next();

		Attributes attrs = sr.getAttributes();
		for (NamingEnumeration<? extends Attribute> ne = attrs.getAll(); ne
			.hasMoreElements();) {
		    Attribute attr = (Attribute) ne.next();
		    String attrID = attr.getID();
		    if (attrID != null
			    && STR_BAD_PASSWORD_COUNT.equalsIgnoreCase(attrID)) {
			for (NamingEnumeration<?> ne1 = attr.getAll(); ne1
				.hasMoreElements();) {

			    badPwdCount = (String) ne1.nextElement();

			}
		    }
		}
	    }
	} catch (RuntimeException e) {
	    logger.error(CORE, ERROR, "Error Message :" + e.getMessage());
	    throw new HsxCoreRuntimeException(e.getMessage());
	} catch (Exception e) {
	    logger.error(CORE, ERROR, "Error Message :" + e.getMessage());
	    throw new HsxCoreException(e.getMessage());
	}

	logger.info(CORE, INFO, "Bad Pasword Count ::" + badPwdCount);
	return badPwdCount;
    }

    /**
     * This method retrieves the username based on GUID.
     *
     * @param guid
     * @return cn
     * @throws HsxCoreException
     * @throws HsxCoreRuntimeException
     */
    public String getVerificationDetails(String guid, String ldapName,
	    String managerDn, String password) throws HsxCoreException,
	    HsxCoreRuntimeException {
    	
	String cn = searchInLDAPwithKey("unlockaccountguid", guid,ldapName, managerDn, password);

	ModificationItem[] mods = new ModificationItem[1];
	Attribute a1 = new BasicAttribute(STR_GUID, " ");
	mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, a1);
	/*
	 * if (cn != null && !STR_EMPTY.equalsIgnoreCase(cn)) { // String userDN
	 * = "CN=" + cn + ",OU=USER"; }
	 */

	return cn;
    }

	

    /**
     * This method retrieves the username based on GUID.
     *
     * @param guid
     * @return cn
     * @throws HsxCoreException
     * @throws HsxCoreRuntimeException
     */
    public String getVerificationDetailsBasedonGuidType(String guidType,
	    String guid, String ldapName, String managerDn, String password)
	    throws HsxCoreException, HsxCoreRuntimeException {
    	
    String cn = searchInLDAPwithKey(guidType, guid,ldapName, managerDn, password);
		
	ModificationItem[] mods = new ModificationItem[1];
	Attribute a1 = new BasicAttribute(STR_GUID, " ");
	mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, a1);
	/*
	 * if (cn != null && !STR_EMPTY.equalsIgnoreCase(cn)) { // String userDN
	 * = "CN=" + cn + ",OU=USER"; }
	 */

	return cn;
    }
    
    //US10736 Starts
    /**
	 * This method checks if the Email Address is existing user or not by email address.
	 * @param userNameProvided
	 * @return isRegisteredUser
	 * @throws HsxCoreException
	 * @throws HsxCoreRuntimeException
	 */
	public boolean checkUserNameExistsInLDAP(String emailAddress,
			String ldapName,String managerDn,String password)
			throws HsxCoreException, HsxCoreRuntimeException {

		boolean isNewUser = false;
		String cn = searchInLDAPwithKey(STR_MAIL, emailAddress,ldapName, managerDn, password);

			if (!cn.isEmpty()) {
				logger.info(CORE, INFO, emailAddress
						+ " exists in LDAP as " + cn);
				isNewUser = false;

			} else if (cn.isEmpty()) {
				logger.info(CORE, INFO, emailAddress
						+ " doesn't exists in LDAP");
				isNewUser = true;
			}
		return isNewUser;

	}
	//US10736 Ends
	/**
	 * This method search in LDAP directory by using key value pair
	 * @param keyName
	 * @param keyValue
	 * @param ldapName
	 * @param managerDn
	 * @param password
	 * @return
	 * @throws HsxCoreRuntimeException
	 * @throws HsxCoreException
	 */
	private String searchInLDAPwithKey(String guidType, String guid, String ldapName,
			String managerDn, String password) throws HsxCoreRuntimeException,
			HsxCoreException {
		String cn = STR_EMPTY;

		DirContext ctx = createAuthenticatedContext(ldapName, managerDn,
			password);
		logger.info(CORE, INFO,"created authenticated context searchInLDAPwithKey");
		SearchControls constraints = new SearchControls();
		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
		Attributes matchAttrs = new BasicAttributes(true);
		matchAttrs.put(new BasicAttribute(guidType,guid));

		try {
		    NamingEnumeration<SearchResult> results = ctx.search(STR_OU_USER, matchAttrs);
		    logger.info(CORE, INFO,"search completed searchInLDAPwithKey");
		    while (results != null && results.hasMore()) {
			SearchResult sr = (SearchResult) results.next();

			Attributes attrs = sr.getAttributes();
			for (NamingEnumeration<? extends Attribute> ne = attrs.getAll(); ne
				.hasMoreElements();) {
			    Attribute attr = (Attribute) ne.next();
			    String attrID = attr.getID();
			    if (attrID != null && STR_CN.equalsIgnoreCase(attrID)) {

				for (NamingEnumeration<?> ne1 = attr.getAll(); ne1
					.hasMoreElements();) {
				    cn = (String) ne1.nextElement();

				}
			    }

			}
		    }
		} catch (RuntimeException e) {
			e.printStackTrace();
		    logger.error(CORE, ERROR, "Error Message :" + e.getMessage());
		    throw new HsxCoreRuntimeException(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		    logger.error(CORE, ERROR, "Error Message :" + e.getMessage());
		    throw new HsxCoreException(e.getMessage());
		}
		logger.info(CORE, INFO, "CN is  ::" + cn);
		return cn;
	}
	
    public static void main(String[] args) {
	HsxCoreLDAPUtil util = new HsxCoreLDAPUtil();
	try {
	    String emailAddress = util.getVerificationDetailsBasedonGuidType(
		    "unlockaccountguid", "f194cd5e7c2d62de12c639f61fe",
		    "ldap://HXS32226.hiscox.nonprod:389/DC=HiscoxSites",
		    "CN=SYSADMIN,OU=ADMINISTRATORS,DC=HiscoxSites",
		    "password-1");
	    // CN=SOA-US-DIRECTCOMMERCIAL,CN=CUSTOMER,OU=GROUPS,DC=HiscoxSites

	    logger.info(CORE, INFO, "Email Address :: " + emailAddress);
	    String count = util.getBadCredential(
		    "bradman.ramesh@hiscox.nonprod",
		    "ldap://HXS32226.hiscox.nonprod:389/DC=HiscoxSites",
		    "CN=SYSADMIN,OU=ADMINISTRATORS,DC=HiscoxSites",
		    "password-1");
	    logger.info(CORE, INFO, "Count :: " + count);

	} catch (Exception e) {
	    logger.error(CORE, ERROR, "Error Message :" + e.getMessage());
	}
    }

}
