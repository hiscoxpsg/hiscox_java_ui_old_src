
//To set focus on first input/select control on SB pages
document
		.observe(
				"dom:loaded",
				function setFocus() {
					var b = false;
					for (f = 0; f < document.forms.length; f++) {
						if (document.forms[f].name == "") {
							continue
						}
						for (i = 0; i < document.forms[f].length; i++) {
							if (document.forms[f][i].type != "hidden") {
								if (document.forms[f][i].disabled != true) {
									var c = document.forms[f][i].nodeName;
									if (c == "SELECT" || c == "INPUT") {
										if (document.forms[f][i].className != "hide-question default-navigation-button"
												&& document.forms[f][i].name != "action_Print_button") {
											document.forms[f][i].focus();
											window.scrollTo(0, 0);
											var b = true
										}
									}
								}
							}
							if (b == true) {
								break
							}
						}
						if (b == true) {
							break
						}
					}
				});


function showToolTipNew(helpTextId, helpImageId) {
	dw_Tooltip.content_vars.L3.helpTextId = helpTextId;
	dw_Tooltip.content_vars.L3.helpImageId = helpImageId;
}

function onClickOfHelpImageEvent(helpImageId, toolTipInputID) {
	if ($(helpImageId) != null) {
		$(helpImageId).observe("mouseup", function() {
			showToolTipNew(toolTipInputID, helpImageId);
			return false;
		});

		$(helpImageId).observe("keydown", function() {
			showToolTipNew(toolTipInputID, helpImageId);
			return false;
		});
	}
}

function wrapImageToWidthHelp(obj) {

	var helpTextId = obj['helpTextId'];
	var helpTextContent = document.getElementById(helpTextId).value;
	var rootPath = document.getElementById("contextpath").value;
	if (!obj)
		return '';
	dw_getImage(obj[rootPath + 'img']);
	var str = dw_Tooltip.wrapSticky(str, helpTextContent);
	// if (w) this.setTipWidth(w);
	return str;
}

function disableHelpHref() {
	$$(".HelpHref").each( function(e) {
		e.target = "_self";
		e.href = "#ToolTip";
		// e.disabled = "disabled";
		});
}

function setFlagOnsubmit() {
	//alert('JSENABLED FLAG MODIFIED');    
	$('isJSEnabled').value = 'YES';
	disableActionButtonsOnSubmitt();         
}

function disableActionButtonsOnSubmitt() {
	if ($('jsactionnameshiddenstringid') != null) {
		var jsActionNamesString = new String();
		jsActionNamesString = $('jsactionnameshiddenstringid').value;
		var actionNamesArray = jsActionNamesString.split('~');
		var actionLength = actionNamesArray.length;
		for ( var i = 0; i < actionLength; i++) {
			var buttonId = new String();
			buttonId = actionNamesArray[i] + '_button_id';
			buttonId = buttonId.toLowerCase();
			// alert('$(buttonId) :'+$(buttonId));
			if ($(buttonId) != null) {
				//alert('Disabled Boss  Correct :');             
				$(buttonId).disabled = true;
			}
		}
	}
}   

function showElement(elementId) {
	var cssClassName = $(elementId).className;
	cssClassName = cssClassName.replaceAll("hide-question", "");
	cssClassName = cssClassName.replaceAll("show-style", "");
	cssClassName = cssClassName.trim() + " " + "show-style";
	$(elementId).className = cssClassName;
}
function hideElement(elementId) {
	//alert('Element Id :'+elementId);
	var divElement = $(elementId);
	// if(divElement!=null){
	var cssClassName = divElement.className;
	cssClassName = cssClassName.replaceAll("show-style", "");
	cssClassName = cssClassName.replaceAll("hide-question", "");
	cssClassName = cssClassName.trim() + " " + "hide-question";
	divElement.className = cssClassName;
	// }
}

String.prototype.replaceAll = function(stringToFind, stringToReplace) {
	var temp = this;
	var index = temp.indexOf(stringToFind);
	while (index != -1) {
		temp = temp.replace(stringToFind, stringToReplace);
		index = temp.indexOf(stringToFind);
	}
	return temp;
};
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, "");
};
String.prototype.replaceAllSpecialChars = function() {
	var temp = this;
	var index = temp.search(/[^0-9.]/);
	alert(index);
	while (index != -1) {
		temp = temp.replace(/[^0-9.]/, "");
		index = temp.search(/[^0-9.]/);
	}
	return temp;
};
String.prototype.replaceNonNumericChars = function() {
	var temp = this;
	var index = temp.search(/[^0-9]+/);
	while (index != -1) {
		temp = temp.replace(/[^0-9.]+/, "");
		index = temp.search(/[^0-9.]+/);
	}
	if(!isNaN(temp)){
	temp = Math.round(temp).toString();
	}
	return temp;
};
