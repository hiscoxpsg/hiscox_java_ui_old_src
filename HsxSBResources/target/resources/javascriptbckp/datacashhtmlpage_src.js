//On Load show renewal check box
	$('renewalchk').style.display="block";
	
	//US9439 to update current month and current year+18 years
	var d = new Date();
	var n = d.getMonth();
	$('expiryMonth').selectedIndex=n;
	expyear=$('expiryYear');
	len=expyear.options.length;
	for(i=len;i>18;i--)
	{
		expyear.remove(i);
	}
	//Hiding Card Images on DataCash Error Messages Div Id 
	var ci=document.getElementsByTagName("div");
	for(var i=0;i<ci.length;i++)
	{
	if(ci[i].className=="error")
	{
		$('cardImages').hide();
		$('declineError').hide();
	break;
	}
	}	

function validate(form){
	var cardNo = $('cardNumber').value;
    var cardPattern = /^\d{6,16}?$/;
 
    var cv2No = $('securityNumber').value;
    var cv2Pattern = /^\d{3,4}?$/;
    
    var cardType = $('cardType').value;
    
    var cardName = $('cardName').value;
    var cardNamePattern = /^[a-zA-Z ]+$/;
    
    var expiryMonth = $('expiryMonth').value;
    var expiryYear = $('expiryYear').value;
    
    var hasCardExpired = false;
    
    var isRenewalCheckBoxChecked = true;
    var isValidCardFlag = true;
    var isValidCvFlag = true;
    var isValidExpiryDataFlag = true;
    var isCardTypeFlag = true;
    var isCardNameFlag = true;
    
	//Renewal check-box validation
    if (!document.cardcapture.capf3.checked){
    	$('dcError').hide();
    	$('cardImages').hide();
    	$('declineError').hide();
    	$('cardDetailsError').hide();
    	$('renewalError').show();
    	isRenewalCheckBoxChecked = false;
    }else{
    	$('dcError').hide();
    	$('renewalError').hide();
    	$('declineError').hide();
    	isRenewalCheckBoxChecked = true;
    }
	
    //Card number validation
    if (!cardNo.match(cardPattern)){
    	$('dcError').hide();
    	$('cardNumError').show();
    	$('renewalError').hide();
    	$('declineError').hide();
    	$('cardDetailsError').hide();
    	$('cardImages').hide();
    	isValidCardFlag = false;
    }else{
    	$('cardNumError').hide();    	
    	isValidCardFlag = true;
    }
    //Card Type validation
    if (!cardType){
    	$('dcError').hide();
    	$('cardTypeError').show();
    	$('renewalError').hide();
    	$('declineError').hide();
    	$('cardDetailsError').hide();
    	$('cardImages').hide();
    	isCardTypeFlag = false;
    }else{
    	$('cardTypeError').hide();    	
    	isCardTypeFlag = true;
    }
    
  //Card Name validation
    if (!cardName.match(cardNamePattern)){
    	$('dcError').hide();   
    	$('cardNameError').show();
    	$('renewalError').hide();
    	$('declineError').hide();
    	$('cardDetailsError').hide();
    	$('cardImages').hide();
    	isCardNameFlag = false;
    }else{
    	$('cardNameError').hide();    	
    	isCardNameFlag = true;
    }
  
    
    //Security number validation
    if(!cv2No.match(cv2Pattern)){
    	$('dcError').hide();
    	$('cardImages').hide();
    	$('renewalError').hide();
    	$('declineError').hide();
    	$('cardDetailsError').hide();
    	$('securityNumError').show();
    	isValidCvFlag = false;
    } else{
    	$('securityNumError').hide();    	
    	isValidCvFlag = true;
    }
	
    
    //Expiry date validation
    hasCardExpired = ValidateExpiryDate();
    
    if(!hasCardExpired){
    	$('dcError').hide();
    	//$('expiryMonth').value = "" ;
    	//$('expiryYear').value = "";
    	$('expiryDateError').show();
    	$('renewalError').hide();
    	$('cardDetailsError').hide();
    	$('cardImages').hide();
    	isValidExpiryDataFlag = false;    	
    }else{
    	$('expiryDateError').hide();    	
    	isValidExpiryDataFlag = true;
    } 

    if((isValidCardFlag==false) && (isValidCvFlag==false) &&
			(isValidExpiryDataFlag==false) && (isCardNameFlag==false) && (isCardTypeFlag==false)) {
    	$('cardTypeError').hide();
    	$('cardNameError').hide();
    	$('securityNumError').hide();
    	$('expiryDateError').hide();
    	$('cardNumError').hide();
    	$('cardDetailsError').show();
    }
    
	if(isRenewalCheckBoxChecked && isValidCardFlag && isValidCvFlag &&
    												isValidExpiryDataFlag && isCardNameFlag &&isCardTypeFlag) {
			document.form.submit;
			return true;
    	}else {
    		
    		return false;
		}   
}

//This function checks if the month and year are either the running month or in 
//future and gives error message if it is in the past.
function ValidateExpiryDate(){
	

	var ccExpYear = $('expiryYear').value;
	var ccExpMonth = $('expiryMonth').value;
	
	var expYearIndex = $('expiryYear').selectedIndex;
	
	/*if(ccExpMonth=="00" ||expYearIndex == 0){
		return false;
	} 
	else{*/
	var expDate=new Date();
	expDate.setFullYear(ccExpYear, ccExpMonth, 1);
	var today = new Date();
	if (expDate<today){
		 return false;
		}      
	else{
		
		return true;
		}  
	//} 
}
