/*couk_javascript.js :Start */

var revert = new Array();
var inames = new Array('hiscox01','hiscox02','hiscox03','hiscox04');
var flipped = new Array();
   
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}



function showRightImg(currId)
{
	var HPRightImgs = document.getElementById('HPRightImages').childNodes;
	var foundCount = 0;
	for (i = 0; i < HPRightImgs.length; ++i)
	{
		var rightImg = HPRightImgs[i];
		var id = rightImg.id;
		if (id != undefined) {
			if (id.substring(0,8) == 'rightImg')
			{
				foundCount++;
				if (foundCount == currId) {
					rightImg.style.display = 'block';
				} else {
					rightImg.style.display = 'none';
				}
			}
		}
	}
}

function mover(divId,newpath,iscouk)
{
	divId.style.position="relative";
	divId.style.zIndex="0";
	var img=divId.getElementsByTagName('img');
	var id=divId.id;
	//var rightImg=document.getElementById('rightImg');
	//rightImg.src=arr[id-1];
	showRightImg(id);
	
	//new code for USDC project
	//new code by AV - for brand image functionality change - co.uk 3.2
	img[0].src=""+newpath+"/images/hiscox_button_0"+id+"_overlay.png";

	if(document.getElementById('hiscox_button_01'))
	{
		var img1 = document.getElementById('hiscox_button_01');	
	}
	else
	{
		var img1 = document.getElementById('hiscox_button_01_overlay');	
	}
	if(document.getElementById('hiscox_button_02'))
	{
		var img2 = document.getElementById('hiscox_button_02');	
	}
	else
	{
		var img2 = document.getElementById('hiscox_button_02_overlay');	
	}	
	if(document.getElementById('hiscox_button_03'))
	{
		var img3 = document.getElementById('hiscox_button_03');
	}
	else
	{
		var img3 = document.getElementById('hiscox_button_03_overlay');
	}	
	if(document.getElementById('hiscox_button_04'))
	{
		var img4 = document.getElementById('hiscox_button_04');
	}
	else
	{
		var img4 = document.getElementById('hiscox_button_04_overlay');
	}
	
	if(id == 1)
	{
		img2.src=""+newpath+"/images/hiscox_button_02.png";	
		img3.src=""+newpath+"/images/hiscox_button_03.png";
		if(iscouk == 1)
		{
		img4.src=""+newpath+"/images/hiscox_button_04.png";
		}
	}
	if(id == 2)
	{
		img1.src=""+newpath+"/images/hiscox_button_01.png";	
		img3.src=""+newpath+"/images/hiscox_button_03.png";
		if(iscouk == 1)
		{
		img4.src=""+newpath+"/images/hiscox_button_04.png";
		}
	}
	if(id == 3)
	{
		img1.src=""+newpath+"/images/hiscox_button_01.png";	
		img2.src=""+newpath+"/images/hiscox_button_02.png";
		if(iscouk == 1)
		{
		img4.src=""+newpath+"/images/hiscox_button_04.png";
		}
	}
	if(id == 4)
	{
		img1.src=""+newpath+"/images/hiscox_button_01.png";	
		img2.src=""+newpath+"/images/hiscox_button_02.png";
		img3.src=""+newpath+"/images/hiscox_button_03.png";
	}
	//end of new code by AV - for brand image functionality change - co.uk 3.2
	//end of new code for USDC project
	
	img[0].src=""+newpath+"/images/hiscox_button_0"+id+"_overlay.png";
	
}

function mout(divId,newpath,iscouk)
{
	divId.style.position="relative";
	divId.style.zIndex="0";
	var img=divId.getElementsByTagName('img');
	var id=divId.id;
	
	//img[0].src=""+newpath+"/images/hiscox_button_0"+id+".png"; 
	
	//new code for USDC project
	//new code by AV - for brand image functionality change - co.uk 3.2
	img[0].src=""+newpath+"/images/hiscox_button_0"+id+"_overlay.png";
	
	if(document.getElementById('hiscox_button_01'))
	{
		var img1 = document.getElementById('hiscox_button_01');	
	}
	else
	{
		var img1 = document.getElementById('hiscox_button_01_overlay');	
	}
	if(document.getElementById('hiscox_button_02'))
	{
		var img2 = document.getElementById('hiscox_button_02');	
	}
	else
	{
		var img2 = document.getElementById('hiscox_button_02_overlay');	
	}	
	if(document.getElementById('hiscox_button_03'))
	{
		var img3 = document.getElementById('hiscox_button_03');
	}
	else
	{
		var img3 = document.getElementById('hiscox_button_03_overlay');
	}
	if(document.getElementById('hiscox_button_04'))
	{
		var img4 = document.getElementById('hiscox_button_04');
	}
	else
	{
		var img4 = document.getElementById('hiscox_button_04_overlay');
	}
	
	if(id == 1)
	{
		img2.src=""+newpath+"/images/hiscox_button_02.png";	
		img3.src=""+newpath+"/images/hiscox_button_03.png";
		if(iscouk == 1)
		{
		img4.src=""+newpath+"/images/hiscox_button_04.png";
		}
	}
	if(id == 2)
	{
		img1.src=""+newpath+"/images/hiscox_button_01.png";	
		img3.src=""+newpath+"/images/hiscox_button_03.png";
		if(iscouk == 1)
		{
		img4.src=""+newpath+"/images/hiscox_button_04.png";
		}
	}
	if(id == 3)
	{
		img1.src=""+newpath+"/images/hiscox_button_01.png";	
		img2.src=""+newpath+"/images/hiscox_button_02.png";
		if(iscouk == 1)
		{
		img4.src=""+newpath+"/images/hiscox_button_04.png";
		}
	}
	if(id == 4)
	{
		img1.src=""+newpath+"/images/hiscox_button_01.png";	
		img2.src=""+newpath+"/images/hiscox_button_02.png";
		img3.src=""+newpath+"/images/hiscox_button_03.png";
	}
	//end of new code by AV - for brand image functionality change - co.uk 3.2
	//end of new code for USDC project
	
	var rightImg=document.getElementById('rightImg');
	if(rightImg)
	{
		rightImg.src=arr[0];	
	}
}


//header tab
//Code for Menu
function setURL()
{
	//new code for USDC project
	//new code by AV - for brand image functionality change - co.uk 3.2
	if(document.getElementById('hiscox_button_01_overlay'))
	{
		showRightImg(1);
	}
	if(document.getElementById('hiscox_button_02_overlay'))
	{
		showRightImg(2);
	}	
	if(document.getElementById('hiscox_button_03_overlay'))
	{
		showRightImg(3);
	}	
	if(document.getElementById('hiscox_button_04_overlay'))
	{
		showRightImg(4);
	}	
	if(document.getElementById('hiscox_button_01_overlay'))
	{
		showRightImg(1);
	}
	if(document.getElementById('hiscox_button_02_overlay'))
	{
		showRightImg(2);
	}	
	if(document.getElementById('hiscox_button_03_overlay'))
	{
		showRightImg(3);
	}	
	//end of new code by AV - for brand image functionality change - co.uk 3.2
	//end of new code for USDC project
	
	//code for tridirect tag - AV
	if(document.getElementById('triDirectTag'))
	{
		var triDirectTag = document.getElementById('triDirectTag');
		if(triDirectTag)
		{
			var axel = Math.random()+"";
			var rand = axel * 10000000000000;
			var oldUrlWithRandomNumber = triDirectTag.src;
			var endOfRandUrl = rand + "?" 
			var newUrlWithRandomNumber = oldUrlWithRandomNumber.replace("?", endOfRandUrl);
			triDirectTag.src = newUrlWithRandomNumber;
		}
	}
	//end of code - AV

	//new code for null check - AV
	if(document.getElementById('hiddenForProdAZ'))
	{
		var prodAtoZCheck = document.getElementById('hiddenForProdAZ');
		if(prodAtoZCheck)
		{
			var mySplittedArray = document.getElementById('hiddenForProdAZ').value.split(",");
			var lengthOfArray = mySplittedArray.length - 1;
			for(count=0; count <= lengthOfArray; count++)
			{
				var classId = "head" + mySplittedArray[count];
				
				//new condition for null check - AV
				if(document.getElementById(classId))
				{
					document.getElementById(classId).href = "#" + classId;
				}
				if(mySplittedArray[count] == "A")
				{
		 
				 document.getElementById('tdForProdAZA').className="PAZ_class1";
				}
												
				if(mySplittedArray[count] == "B")
				{
				 document.getElementById('tdForProdAZB').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "C")
				{
				 document.getElementById('tdForProdAZC').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "D")
				{
				 document.getElementById('tdForProdAZD').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "E")
				{
				 document.getElementById('tdForProdAZE').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "F")
				{
				 document.getElementById('tdForProdAZF').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "G")
				{
				 document.getElementById('tdForProdAZG').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "H")
				{
				 document.getElementById('tdForProdAZH').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "I")
				{
				 document.getElementById('tdForProdAZI').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "J")
				{
				 document.getElementById('tdForProdAZJ').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "K")
				{
				 document.getElementById('tdForProdAZK').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "L")
				{
				 document.getElementById('tdForProdAZL').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "M")
				{
				 document.getElementById('tdForProdAZM').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "N")
				{
				 document.getElementById('tdForProdAZN').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "O")
				{
				 document.getElementById('tdForProdAZO').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "P")
				{
				 document.getElementById('tdForProdAZP').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "Q")
				{
				 document.getElementById('tdForProdAZQ').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "R")
				{
				 document.getElementById('tdForProdAZR').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "S")
				{
				 document.getElementById('tdForProdAZS').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "T")
				{
				 document.getElementById('tdForProdAZT').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "U")
				{
				 document.getElementById('tdForProdAZU').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "V")
				{
				 document.getElementById('tdForProdAZV').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "W")
				{
				 document.getElementById('tdForProdAZW').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "X")
				{
				 document.getElementById('tdForProdAZX').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "Y")
				{
				 document.getElementById('tdForProdAZY').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "Z")
				{
				 document.getElementById('tdForProdAZZ').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "A-Z")
				{
				 document.getElementById('tdForProdAZA-Z').className="PAZ_class1";
				}
				
				if(mySplittedArray[count] == "0-9")
				{
				 document.getElementById('tdForProdAZ0-9').className="PAZ_class1";
				}
			}
		}
	}				
}

function rotatingtext()
{
	if(document.getElementById('maxRotatingTextItems'))
	{
		var hiddentElement = document.getElementById('maxRotatingTextItems');
		var maxCount = hiddentElement.value;
		var u = maxCount;
		var l = 1;//lower limit will always be set to 1
		var randomId = (Math.floor((Math.random() * (u-l+1))+l));
		var elementIdToShow;
		var control;
		var elementIdToHide;
		for(i=1;i<=maxCount;i++)
		{
			if(randomId == i)
			{
				elementIdToShow = 'rotatingTextContainer' + i;
				control = document.getElementById(elementIdToShow);
				control.style.display = "block";
			}
			else
			{
				elementIdToHide = 'rotatingTextContainer' + i;
				control = document.getElementById(elementIdToHide);
				control.style.display = "none";
			}
		}
	}
	//added new section below for landing page tool tip
	//added new if condition to avoid js errors
	if(document.getElementById("primarybusiness_anchor_id"))
	{
		var anc = document.getElementById("primarybusiness_anchor_id"); 
		//NOTE- The id should match the id of the tooltip anchor tag
		anc.href="#ToolTip";
		anc.target="_self";	
	}
	
	//code for hiding non js text box on small business insurance landing page
	//US Factory FR1
	if(document.getElementById('primarybusiness_dropdown_id'))
	{
		var qc = document.getElementById('primarybusinessexplain_question_id');
		if(document.getElementById('primarybusiness_dropdown_id').value=="NONE OF THE ABOVE; Please explain") 
		{
			qc.className = "question-container txt narr lbl-max ctl-max show-style";
		} 
		else 
		{
			qc.className = "question-container txt narr lbl-max ctl-max hide-question";
		}
}
}
function openChosenURL(objSel)
{
	var chosenLink = objSel.value;
	window.open(chosenLink, 'open_window');
}
/*couk_javascript.js :End */

/*function.js :Start */
//Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
// Place any jQuery/helper plugins in here.
/*function.js :End */

/*detectmobilebrowser.js :Start */
/**
 * jQuery.browser.mobile (http://detectmobilebrowser.com/)
 *
 * jQuery.browser.mobile will be true if the browser is a mobile device
 *
 **/
(function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
/*detectmobilebrowser.js :End */
